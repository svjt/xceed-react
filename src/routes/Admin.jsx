import React, { Component } from "react";

import { Route, Switch, Redirect } from "react-router-dom";

import Login from "../components/admin/login/Login";
import AdminPageNotFound from "../components/404/AdminPageNotFound";
import Dashboard from "../components/admin/dashboard/Dashboard";
import Employees from "../components/admin/employee/Employee";
import Customer from "../components/admin/customer/Customers";
import AdminList from "../components/admin/adminlist/AdminList";
import Agent from "../components/admin/agent/Agent";
import Profile from "../components/admin/profile/Profile";
import Assigned from "../components/admin/assigned/Assigned";
import AssignedDetails from "../components/admin/assigned/AssignedDetails";
import Tasks from "../components/admin/tasks/Tasks";
import AllTasks from "../components/admin/tasks/AllTasks";
import Sla from "../components/admin/sla/Sla";
import NotificationSla from "../components/admin/notificationsla/NotificationSla";
import Company from "../components/admin/company/Company";
import MasterCompany from "../components/admin/company/MasterCompany";
import CompanyShipTo from "../components/admin/company/CompanyShipTo";
// import CompanySoldTo from "../components/admin/company/CompanySoldTo";
import TaskCOA from "../components/admin/taskcoa/TaskCOA";
import CompanyProduct from "../components/admin/company/CompanyProduct";
import Products from "../components/admin/products/Products";
import Region from "../components/admin/region/Region";
import Country from "../components/admin/country/Country";
import MasterCountry from "../components/admin/country/MasterCountry";
import Pharmacopeial from "../components/admin/pharmacopeial/Pharmacopeial.jsx";
import Emails from "../components/admin/emails/Emails";
import EmailLog from "../components/admin/emails/EmailLog";
import ProductEnquiries from "../components/admin/fa/ProductEnquiries";
import ProductSearch from "../components/admin/fa/ProductSearch";
import TextSuggestion from "../components/admin/text-suggestion/TextSuggestion";
import customerEscalation from "../components/admin/customer/CustomerEscalation";
import EmailGroup from "../components/admin/emails/EmailGroup";
import EmailGroupDetails from "../components/admin/emails/EmailGroupDetails";
import Reports from "../components/admin/reports/Reports";
import TaskRatings from "../components/admin/tasks/TaskRatings";
import TaskRatingQuestions from "../components/admin/tasks/TaskRatingQuestions";
import TaskRatingOptions from "../components/admin/tasks/TaskRatingOptions";

import FaRatings from "../components/admin/fa/FaRatings";
import FaRatingQuestions from "../components/admin/fa/FaRatingQuestions";
import FaRatingOptions from "../components/admin/fa/FaRatingOptions";


import PaymentTerms from "../components/admin/paymentterms/PaymentTerms";
import DeliveryTerms from "../components/admin/paymentterms/DeliveryTerms";
import IncreasedSla from "../components/admin/sla/IncreasedSla";
import PausedSla from "../components/admin/sla/PausedSla";
import TasksDetails from "../components/admin/tasks/TaskDetails";
import Group from "../components/admin/group/Group";
import Cscc from "../components/admin/cscc/Cscc";

import EventsListing from "../components/admin/events-listing/EventsListing";
import ErrLog from "../components/admin/error-log/ErrLog";
import ErrLogDetails from "../components/admin/error-log/ErrLogDetails";
import CrmErr from "../components/admin/error-log/CrmErr";

import "../assets/css/all.css";
import "../assets/css/admin-style.css";
import "../assets/css/admin-skin-blue.css";
import "react-bootstrap-table/dist/react-bootstrap-table.min.css";

// Private Route for inner component
const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      localStorage.getItem("admin_token") ? (
        <Component {...props} />
      ) : (
          <Redirect to="/admin" />
        )
    }
  />
);

class Admin extends Component {
  render() {
    //console.log('Admin',localStorage.getItem('token'));
    return (
      <Switch>
        <PrivateRoute
          exact
          path="/admin/dashboard"
          component={Dashboard}
          handler="Dashboard"
        />
        <PrivateRoute exact path="/admin/employees" component={Employees} />
        <PrivateRoute exact path="/admin/customers" component={Customer} />
        <PrivateRoute exact path="/admin/admin_list" component={AdminList} />
        <PrivateRoute exact path="/admin/agent" component={Agent} />
        <PrivateRoute exact path="/admin/profile" component={Profile} />
        <PrivateRoute exact path="/admin/assigned" component={Assigned} />
        <PrivateRoute
          exact
          path="/admin/assigned/:id"
          component={AssignedDetails}
        />
        <PrivateRoute
          exact
          path="/admin/employees/tasks/:id"
          component={Tasks}
        />
        <PrivateRoute exact path="/admin/tasks" component={AllTasks} />
        <PrivateRoute exact path="/admin/sla" component={Sla} />
        <PrivateRoute
          exact
          path="/admin/notificationsla"
          component={NotificationSla}
        />
        <PrivateRoute exact path="/admin/company" component={Company} />
        <PrivateRoute exact path="/admin/company_shipto/:id" component={CompanyShipTo} />
        {/* <PrivateRoute exact path="/admin/company_soldto/:id" component={CompanySoldTo} /> */}
        <PrivateRoute exact path="/admin/company_shipto/company_product/:id" component={CompanyProduct} />
        <PrivateRoute
          exact
          path="/admin/master_company"
          component={MasterCompany}
        />
        <PrivateRoute
          exact
          path="/admin/task_coa"
          component={TaskCOA}
        />
        <PrivateRoute exact path="/admin/products" component={Products} />
        <PrivateRoute exact path="/admin/region" component={Region} />
        <PrivateRoute exact path="/admin/country" component={Country} />
        <PrivateRoute exact path="/admin/master_country" component={MasterCountry} />
        <PrivateRoute exact path="/admin/pharmacopeial" component={Pharmacopeial} />
        <PrivateRoute exact path="/admin/email" component={Emails} />
        <PrivateRoute exact path="/admin/email_log" component={EmailLog} />
        <PrivateRoute exact path="/admin/product_enquiries" component={ProductEnquiries} />
        <PrivateRoute exact path="/admin/product_search" component={ProductSearch} />
        <PrivateRoute exact path="/admin/txt_suggestion" component={TextSuggestion} />
        <PrivateRoute exact path="/admin/customer_escalation" component={customerEscalation} />
        <PrivateRoute exact path="/admin/email_group" component={EmailGroup} />
        <PrivateRoute
          exact
          path="/admin/email_group/:id"
          component={EmailGroupDetails}
        />
        <PrivateRoute
          exact
          path="/admin/payment_terms"
          component={PaymentTerms}
        />
        <PrivateRoute
          exact
          path="/admin/delivery_terms"
          component={DeliveryTerms}
        />
        <PrivateRoute exact path="/admin/reports" component={Reports} />
        <PrivateRoute
          exact
          path="/admin/task_ratings"
          component={TaskRatings}
        />
        <PrivateRoute
          exact
          path="/admin/task_rating_questions"
          component={TaskRatingQuestions}
        />
        <PrivateRoute
          exact
          path="/admin/task_rating_options"
          component={TaskRatingOptions}
        />

        <PrivateRoute
          exact
          path="/admin/fa_ratings"
          component={FaRatings}
        />
        <PrivateRoute
          exact
          path="/admin/fa_rating_questions"
          component={FaRatingQuestions}
        />
        <PrivateRoute
          exact
          path="/admin/fa_rating_options"
          component={FaRatingOptions}
        />

        <PrivateRoute
          exact
          path="/admin/increased_sla"
          component={IncreasedSla}
        />
        <PrivateRoute exact path="/admin/paused_sla" component={PausedSla} />
        <PrivateRoute
          exact
          path="/admin/task_details/:id"
          component={TasksDetails}
        />
        <PrivateRoute exact path="/admin/group" component={Group} />
        <PrivateRoute
          exact
          path="/admin/commercial_checklist"
          component={Cscc}
        />

        <PrivateRoute exact path="/admin/events" component={EventsListing} />
        <PrivateRoute exact path="/admin/error_log" component={ErrLog} />
        <PrivateRoute
          exact
          path="/admin/error_log_details"
          component={ErrLogDetails}
        />
        <PrivateRoute exact path="/admin/crm_error" component={CrmErr} />

        <Route exact path="/admin" component={Login} />
        <Route from="*" component={AdminPageNotFound} />
      </Switch>
    );
  }
}

export default Admin;
