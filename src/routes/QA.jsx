import React, { Component } from 'react';

import {
    Route,
    Switch,
    Redirect,
    BrowserRouter as Router,
} from "react-router-dom";

import PageNotFound from '../components/404/PageNotFound';
import QALogin from "../components/QALogin/login";
import QAForgotPassword from "../components/QALogin/forgotPassword";
import QAResetPassword from "../components/QALogin/resetPassword";
import QAForm from  "../components/QALogin/form";
import QAUploads from "../components/QALogin/Uploads";
import QATasks from "../components/QALogin/Tasks";
import QATasksDetails from "../components/QALogin/TasksDetails";
import QAManageTypicalCoa from "../components/QALogin/ManageTypicalCoa";

import Layout from '../components/QALogin/Layout';
// import Layout from '../components/layout/Layout';
import { withRouter } from 'react-router';

import "../assets/css/all.css";
import "react-bootstrap-table/dist/react-bootstrap-table.min.css";
import "../assets/css/skin-blue.css";
import "../assets/css/style.css";


// Private Route for inner component
const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={
        (props) => (localStorage.getItem('qa_token') ? <Component {...props} /> : <Redirect to='/qa_login' />
    )} />
)

//basename={process.env.NODE_ENV === 'production' ? '/dr_reddy_xceed' : ''}

class QA extends Component {

    render(){
        return (
            <Router>
                <Layout>
                    <Switch>
                        <Route exact path='/qa_login' component={QALogin} />
                        <Route exact path='/qa_forgot_password' component={QAForgotPassword} />
                        <Route exact path='/qa_reset_password/:token' component={QAResetPassword} />
                        <PrivateRoute exact path='/qa_form' component={QAForm} />
                        <PrivateRoute exact path='/qa_uploads' component={QAUploads} />
                        <PrivateRoute exact path='/qa_coa_tasks' component={QATasks} />
                        <PrivateRoute exact path='/qa_task_details/:id/' component={QATasksDetails} />
                        <PrivateRoute exact path='/qa_manage_typical_coa/' component={QAManageTypicalCoa} />
                        <Route from='*' component={PageNotFound}/>
                    </Switch>                    
                </Layout>
            </Router>
        )
    }
}

export default withRouter(QA);