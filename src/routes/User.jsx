import React, { Component } from 'react';

import {
    Route,
    Switch,
    Redirect,
    BrowserRouter as Router,
} from "react-router-dom";

import ProductCatalog from "../components/product-catalog/ProductCatalog";
import ProductDisplay from "../components/product-display/productDisplay";
import Login from "../components/login/LogIn";
import Dashboard from "../components/dashboard/Dashboard";
import TaskDetails from "../components/dashboard/TaskDetails";
import TaskDetailsAny from "../components/dashboard/TaskDetailsAny";
import EditTaskDetails from "../components/dashboard/EditTaskDetails";
import EditTranslateTaskDetails from "../components/dashboard/EditTranslateTaskDetails";
import DiscussionDetails from '../components/dashboard/DiscussionDetails';

import MyCFTMembers from "../components/my-cft-members/MyCFTMembers";
import DocumentSearch from "../components/utlities/DocumentSearch";
import SearchByCustomer from "../components/utlities/SearchByCustomer";
import PricingGuidance from "../components/utlities/PricingGuidance";
import LeadTime from "../components/utlities/LeadTime";
import SlaReference from "../components/utlities/SlaReference";
import PageNotFound from '../components/404/PageNotFound';
import MyCustomer from "../components/dashboard/MyCustomer";
import MyUserList from "../components/dashboard/MyUserList";
import CustomerDashboard from '../components/dashboard/CustomerDashboard';
import Profile from '../components/profile/Profile';
import MyTeam from '../components/my-team/MyTeam';
import MyTeamGraphDetails from '../components/my-team/MyTeamGraphDetails';
import MyGraphDetails from '../components/dashboard/MyGraphDetails';

import ViewTeam from '../components/my-team/ViewTeam';

import MyTeamCustomer from '../components/my-team/MyTeamCustomer';

import EmailCustomer from "../components/dashboard/EmailCustomer";

import TeamDashboard from "../components/team-dashboard/TeamDashboard";
import Leave from "../components/leave/Leave";
import Proforma from "../components/proforma/CreateProforma";
import TeamTaskDetails from "../components/my-team/TeamTaskDetails";
import TeamEditTaskDetails from "../components/my-team/TeamEditTaskDetails";
import TeamEditTranslateTaskDetails from "../components/my-team/TeamEditTranslateTaskDetails";
import Ratings from "../components/ratings/Ratings";
import OverallExperience from "../components/ratings/OverallExperience";
import ExperienceRating from "../components/ratings/ExperienceRating";
import ThankYouForRating from "../components/ratings/ThankYouForRating";

import TypicalCOA from "../components/typicalcoa/TypicalCOA";

import CustomerApproval from '../components/process/CustomerApproval';

// import QALogin from "../components/QALogin/login";
// import QAForgotPassword from "../components/QALogin/forgotPassword";
// import QAResetPassword from "../components/QALogin/resetPassword";
// import QAForm from  "../components/QALogin/form";
// import Uploads from "../components/QALogin/Uploads";

//import DiscussionDetails from '../components/dashboard/DiscussionDetails';

import Tracker from "../tracker";        // SATYAJIT
import Layout from '../components/layout/Layout';    // SATYAJIT
import { withRouter } from 'react-router'   // SATYAJIT

import "../assets/css/all.css";
import "react-bootstrap-table/dist/react-bootstrap-table.min.css";
import "../assets/css/skin-blue.css";
import "../assets/css/style.css";


// Private Route for inner component
const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={
        (props) => (localStorage.getItem('token') ? <Component {...props} /> : <Redirect to='/' />
    )} />
    
)

//basename={process.env.NODE_ENV === 'production' ? '/dr_reddy_xceed' : ''}

class User extends Component {

    render(){
        //console.log('User',localStorage.getItem('admin_token'));
        return (
            <Router>
                <Layout>
                    <Switch>
                        <PrivateRoute exact path='/user/dashboard/review_request/:review_team_id' component={Dashboard}/>
                        <PrivateRoute exact path='/user/dashboard/outlook/:id' component={Dashboard}/>
                        <PrivateRoute exact path='/user/dashboard' component={Dashboard}/>
                        <PrivateRoute exact path='/user/customer-dashboard/:id' component={CustomerDashboard}/>               
                        <PrivateRoute exact path='/user/task_details/:id/:assign_id' component={TaskDetails} />
                        <PrivateRoute exact path='/user/user_task_details/:id' component={TaskDetailsAny} />
                        
                        <PrivateRoute exact path='/user/edit_task_details/:id/:assign_id' component={EditTaskDetails} />
                        <PrivateRoute exact path='/user/edit_translate_task_details/:id/:assign_id' component={EditTranslateTaskDetails} />
                        <PrivateRoute exact path='/user/discussion_details/:did' component={DiscussionDetails} />
                        <PrivateRoute exact path='/user/product_catalogue' component={ProductCatalog}/>
                        <PrivateRoute exact path='/user/product-details/:id' component={ProductDisplay}/>
                        <PrivateRoute exact path='/user/my_cft_members' component={MyCFTMembers}/>
                        <PrivateRoute exact path='/user/utilities/document_search' component={DocumentSearch}/>
                        <PrivateRoute exact path='/user/utilities/search_by_customer' component={SearchByCustomer}/>
                        <PrivateRoute exact path='/user/utilities/pricing_guidance' component={PricingGuidance}/>
                        <PrivateRoute exact path='/user/utilities/lead_time' component={LeadTime}/>
                        <PrivateRoute exact path='/user/utilities/sla_reference' component={SlaReference}/>
                        <PrivateRoute exact path='/user/my_customer' component={MyCustomer}/>
                        <PrivateRoute exact path='/user/my_user/:company_id' component={MyUserList}/>
                        <PrivateRoute exact path='/user/profile' component={Profile}/>
                        <PrivateRoute exact path='/user/my_team' component={MyTeam}/>
                        <PrivateRoute exact path='/user/my_team_details/:url_id/:bar_graph' component={MyTeamGraphDetails}/>
                        <PrivateRoute exact path='/user/my_analytics_details/:url_id/:bar_graph' component={MyGraphDetails}/>
                        <PrivateRoute exact path='/user/view_team' component={ViewTeam}/>
                        <PrivateRoute exact path='/user/team_dashboard/:id' component={TeamDashboard}/>
                        <PrivateRoute exact path='/user/leave' component={Leave}/>
                        <PrivateRoute exact path='/user/email_customer' component={EmailCustomer}/>
                        <PrivateRoute exact path='/user/my_team_customer/:emp_id/:comp_id' component={MyTeamCustomer}/>               
                        <PrivateRoute exact path='/user/team_task_details/:id/:assign_id/:posted_for' component={TeamTaskDetails} />
                        <PrivateRoute exact path='/user/team_edit_task_details/:id/:assign_id' component={EditTaskDetails} />
                        <PrivateRoute exact path='/user/team_edit_translate_task_details/:id/:assign_id/:posted_for' component={TeamEditTranslateTaskDetails} />
                        <PrivateRoute exact path='/user/team_discussion_details/:did' component={DiscussionDetails} />
                        <PrivateRoute exact path='/user/team_edit_task_details/:id/:assign_id/:posted_for' component={TeamEditTaskDetails} />
                        <PrivateRoute exact path='/user/create_proforma' component={Proforma}/>
                        <PrivateRoute exact path='/user/ratings' component={Ratings}/>
                        <PrivateRoute exact path='/user/overall_experience' component={OverallExperience}/>
                        <PrivateRoute exact path='/user/experience_rating' component={ExperienceRating}/>
                        <PrivateRoute exact path='/user/thank_you_for_rating' component={ThankYouForRating}/>
                        <PrivateRoute exact path='/user/typical_coa' component={TypicalCOA}/>

                        <Route exact path='/' component={Login}/>
                        <Route exact path='/process_customer_approval/:token' component={CustomerApproval} />
                        {/* <Route exact path='/qa_login' component={QALogin} />
                        <Route exact path='/qa_forgot_password' component={QAForgotPassword} />
                        <Route exact path='/qa_reset_password/:token' component={QAResetPassword} />
                        <Route exact path='/qa_form' component={QAForm} />
                        <Route exact path='/qa_uploads' component={Uploads} /> */}
                        <Route from='*' component={PageNotFound}/>
                    </Switch>                    
                    {/* <Tracker/> */}
                </Layout>
            </Router>
        )
        
    }
}

export default withRouter(User);