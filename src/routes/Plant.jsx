import React, { Component } from 'react';
import { Route, Switch, Redirect, BrowserRouter as Router } from 'react-router-dom';
import { withRouter } from 'react-router';

import Layout from '../components/plant-login/layout/Layout';
import PageNotFound from '../components/404/PageNotFound';
import PlantLogin from '../components/plant-login/login/Login';
import PlantForgotPassword from '../components/plant-login/forgot-password/ForgotPassword';
import PlantResetPassword from '../components/plant-login/reset-password/ResetPassword';
import PlantOrders from '../components/plant-login/plant-orders/PlantOrders';
import PlantOrderDetails from '../components/plant-login/plant-order-details/PlantOrderDetails';

import '../assets/css/all.css';
import 'react-bootstrap-table/dist/react-bootstrap-table.min.css';
import '../assets/css/skin-blue.css';
import '../assets/css/style.css';

// Private Route for inner component
const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={(props) =>
            localStorage.getItem('plant_token') ? <Component {...props} /> : <Redirect to='/plant_login' />
        }
    />
);

//basename={process.env.NODE_ENV === 'production' ? '/dr_reddy_xceed' : ''}

class Plant extends Component {
    render() {
        return (
            <Router>
                <Layout>
                    <Switch>
                        <Route exact path='/plant_login' component={PlantLogin} />
                        <Route exact path='/plant_forgot_password' component={PlantForgotPassword} />
                        <Route exact path='/plant_reset_password/:token' component={PlantResetPassword} />
                        <PrivateRoute exact path='/plant_orders' component={PlantOrders} />
                        <PrivateRoute exact path='/plant_task_details/:id' component={PlantOrderDetails} />
                        <Route from='*' component={PageNotFound} />
                    </Switch>
                </Layout>
            </Router>
        );
    }
}

export default withRouter(Plant);
