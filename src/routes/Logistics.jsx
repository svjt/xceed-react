import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { Route, Switch, Redirect, BrowserRouter as Router } from 'react-router-dom';

import Layout from '../components/logistics-login/layout/Layout';
import PageNotFound from '../components/404/PageNotFound';
import LogisticsLogin from '../components/logistics-login/login/Login';
import LogisticsForgotPassword from '../components/logistics-login/forgot-password/ForgotPassword';
import LogisticsResetPassword from '../components/logistics-login/reset-password/ResetPassword';
import LogisticsOrders from '../components/logistics-login/logistics-orders/LogisticsOrders';
import LogisticsOrderDetails from '../components/logistics-login/logistics-order-details/LogisticsOrderDetails';

import '../assets/css/all.css';
import 'react-bootstrap-table/dist/react-bootstrap-table.min.css';
import '../assets/css/skin-blue.css';
import '../assets/css/style.css';

// Private Route for inner component
const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={(props) =>
            localStorage.getItem('logistics_token') ? <Component {...props} /> : <Redirect to='/logistics_login' />
        }
    />
);

//basename={process.env.NODE_ENV === 'production' ? '/dr_reddy_xceed' : ''}

class Logistics extends Component {
    render() {
        return (
            <Router>
                <Layout>
                    <Switch>
                        <Route exact path='/logistics_login' component={LogisticsLogin} />
                        <Route exact path='/logistics_forgot_password' component={LogisticsForgotPassword} />
                        <Route exact path='/logistics_reset_password/:token' component={LogisticsResetPassword} />
                        <PrivateRoute exact path='/logistics_orders' component={LogisticsOrders} />
                        <PrivateRoute exact path='/logistics_order_details/:id' component={LogisticsOrderDetails} />
                        <Route from='*' component={PageNotFound} />
                    </Switch>
                </Layout>
            </Router>
        );
    }
}

export default withRouter(Logistics);
