import swal from "sweetalert";

export function showErrorMessage(err, props){
  var errText = '';
  if(localStorage.getItem('admin_token')){
    if(err && err.data){
      errText = err.data.message ? JSON.stringify(err.data.message) : (err.data.errors ? JSON.stringify(err.data.errors) : "");
    }else {
      errText = "Please check your internet connection."
    }
  }else{
    if(err && err.data){
      errText = err.data.message ? JSON.stringify(err.data.message) : (err.data.errors ? JSON.stringify(err.data.errors) : "")
    }else {
      errText = "Please check your internet connection."
    }
  }

  swal({
    closeOnClickOutside: false,
    title: "Error",
    text: errText,
    icon: "error"
  }).then(()=> {      
      if(localStorage.getItem('admin_token')){
        if(err.data && err.data.status === 5){
          localStorage.removeItem('admin_token')
        }
        //props.history.push('/admin');
        window.location.href="/admin";
      }else{
        //props.history.push('/');
        window.location.href="/admin";
      }
    });

}

