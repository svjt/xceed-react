import React, { Component } from 'react';
import {BootstrapTable,TableHeaderColumn} from 'react-bootstrap-table';
import { getMyDrupalId,localDate } from '../../shared/helper';
import base64 from 'base-64';
import {
  Row,
  Col,
  Tooltip,
  OverlayTrigger
} from "react-bootstrap";
import '../dashboard/Dashboard.css';

import StatusColumn from './StatusColumn';
import EmployeeClosedSubTaskTable from './EmployeeClosedSubTaskTable';

import { Link } from 'react-router-dom';
import API from "../../shared/axios";
import Pagination from "react-js-pagination";
import { showErrorMessageFront } from "../../shared/handle_error_front";

import dateFormat from "dateformat";
import swal from "sweetalert";

const portal_url = `${process.env.REACT_APP_PORTAL_URL}`; // SATYAJIT

const getStatusColumn = refObj => (cell,row) => {
    return <StatusColumn rowData={row} />
}

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const setDescription = refOBj => (cell,row) =>{
    if(row.parent_id > 0){
        return (
            <LinkWithTooltip
                tooltip={`${row.title}`}
                href="#"
                id="tooltip-1"
                clicked={e => refOBj.checkHandler(e)}
              >
              {row.title}
            </LinkWithTooltip>
        );
      }else{
        return (      
          <LinkWithTooltip
            tooltip={`${row.req_name}`}
            href="#"
            id="tooltip-1"
            clicked={e => refOBj.checkHandler(e)}
          >
            {row.req_name}
          </LinkWithTooltip>
          );
      }
}

const clickToShowTasks = refObj => (cell,row) =>{
    if(row.discussion === 1){
        return <Link to={{ pathname: '/user/discussion_details/'+row.task_id }} target="_blank" style={{cursor:'pointer'}}>{cell}</Link>
    }else{
        return <Link to={{ pathname: `/user/task_details/${row.task_id}/${row.assignment_id}`}} target="_blank" style={{cursor:'pointer'}}>{cell}</Link>
    }
}

const setCustomerName = refObj => (cell,row) =>{
    if(row.customer_id > 0){
      return (
          <LinkWithTooltip
              tooltip={`${row.first_name + " " + row.last_name}`}
              href="#"
              id="tooltip-1"
              clicked={e => refObj.redirectUrl(e, row.customer_drupal_id)}
            >
            {row.first_name + " " + row.last_name}
          </LinkWithTooltip>
      );
    }else{
        return "-";
    }
}

const setAssignedTo = refOBj => (cell,row) => {
    return (    
    <LinkWithTooltip
        tooltip={`${row.emp_first_name + " " + row.emp_last_name + " (" + row.desig_name + ")"}`}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {row.emp_first_name + " " + row.emp_last_name + " (" + row.desig_name + ")"}
    </LinkWithTooltip>
  );
}

const setDateFormat = refObj => (cell,row) => {
    var date = localDate(cell);
    return dateFormat(date, "dd/mm/yyyy");
}

class EmployeeClosedTasksTable extends Component {

    state = {
        tableData : [],
        
        activePage: 1,
        totalCount: 0,
        itemPerPage: 20,
    };

    checkHandler = (event) => {
      event.preventDefault();
    };

    redirectUrl = (event, id) => {
        event.preventDefault();
        //http://reddy.indusnet.cloud/customer-dashboard?source=Mi02NTE=
        var emp_drupal_id = getMyDrupalId(localStorage.token);
        var base_encode   = base64.encode(`${id}-${emp_drupal_id}`); 
        window.open( portal_url + "customer-dashboard?source="+base_encode, '_blank');
    };

    taskDetails = (row) => {
        this.setState({showTaskDetails:true,currRow:row});
    }
    
    showSubTaskPopup = (currRow) => {
        //console.log('Create Sub Task');
        if(currRow.po_no === null && currRow.request_type == 23){
            swal({
              closeOnClickOutside: false,
              title: "Warning !!",
              text: "Please set Purchase Order Number first.",
              icon: "warning",
            }).then(() => {
              
            });
        }else{
            this.setState({showCreateSubTask:true,currRow:currRow});
        }
    }

    componentDidMount(){
        //console.log('closed task table',this.props.countClosedTasks);
        this.setState({
            tableData:this.props.tableData,
            countClosedTasks: this.props.countClosedTasks,
            options : {
                clearSearch: true,
                expandBy: 'column',
                page: !this.state.start_page ? 1 : this.state.start_page,// which page you want to show as default
                sizePerPageList: [ {
                    text: '10', value: 10
                }, {
                    text: '20', value: 20
                }, {
                    text: 'All', value: !this.state.tableData ? 1 : this.state.tableData
                } ], // you can change the dropdown list for size per page
                sizePerPage: 10,  // which size per page you want to locate as default
                pageStartIndex: 1, // where to start counting the pages
                paginationSize: 3,  // the pagination bar size.
                prePage: '‹', // Previous page button text
                nextPage: '›', // Next page button text
                firstPage: '«', // First page button text
                lastPage: '»', // Last page button text
                //paginationShowsTotal: this.renderShowsTotal,  // Accept bool or function
                paginationPosition: 'bottom'  // default is bottom, top and both is all available
                // hideSizePerPage: true > You can hide the dropdown for sizePerPage
                // alwaysShowAllBtns: true // Always show next and previous button
                // withFirstAndLast: false > Hide the going to First and Last page button
            },
            cellEditProp:{
                mode: 'click',
                beforeSaveCell: this.onBeforeSetPriority, // a hook for before saving cell
                afterSaveCell: this.onAfterSetPriority // a hook for after saving cell
            }
        });
    }

    handlePageChange = (pageNumber) => {
        this.setState({ activePage: pageNumber });
        this.getMyTasks(pageNumber > 0 ? pageNumber  : 1);
    };
  
  getMyTasks(page = 1) {
      let url;
      if(this.props.queryString !== '' ){
        url = `/api/team/member/closed/${this.props.emp_id}?page=${page}&${this.props.queryString}`; 
      }else{
        url = `/api/team/member/closed/${this.props.emp_id}?page=${page}`;
      }
      API.get(url)
      .then(res => {
      this.setState({
          tableData: res.data.data, 
          countClosedTasks: res.data.count_closed_tasks});
      })
      .catch(err => { 
          showErrorMessageFront(err,this.props);
      });
  }

    getSubTasks = (row) => {
        return(
            <EmployeeClosedSubTaskTable tableData={row.sub_tasks} assignId={row.assignment_id} />
        );
    }

    checkSubTasks = (row) => {

        //console.log("subtask")

        if( typeof row.sub_tasks !== 'undefined' && row.sub_tasks.length > 0 ){
            return true;
        }else{
            return false;
        }
    }

    tdClassName = (fieldValue, row) =>{
        var dynamicClass = ' ';
        if(row.vip_customer === 1){
            dynamicClass += 'bookmarked-column ';
        }
        return dynamicClass;
    }

    trClassName = (row,rowIndex) =>{
        var ret = ' ';

        var selDueDate;
        if(row.assigned_by > 0){
            selDueDate = row.new_due_date;
        }else{
            selDueDate = row.due_date;
        }

        var dueDate = localDate(selDueDate);
        var today  = new Date();
        var timeDiff = dueDate.getTime() - today.getTime();

        if(timeDiff > 0){

        }else{
            if(row.vip_customer === 1){
                ret += 'tr-red';
            }
        }

        return ret;
    }

    expandColumnComponent({ isExpandableRow, isExpanded }) {
        let content = '';
    
        if (isExpandableRow) {
          content = (isExpanded ? '-' : '+' );
        } else {
          content = ' ';
        }
        return (
          <div> { content } </div>
        );
    }

    render(){
        return (
            <>  
                <div class="closed_task_table" >
                <BootstrapTable 
                    data={this.state.tableData}
                    /* options={ paginationOptions } 
                    pagination */ 
                    expandableRow={ this.checkSubTasks } 
                    expandComponent={ this.getSubTasks }
                    expandColumnOptions={ 
                        { 
                            expandColumnVisible: true,
                            expandColumnComponent: this.expandColumnComponent,
                            columnWidth: 25
                        } 
                    }
                    cellEdit={ this.state.cellEditProp }
                    >
                    <TableHeaderColumn isKey dataField='task_ref' columnClassName={ this.tdClassName } editable={ false } expandable={ false } dataFormat={ clickToShowTasks(this) } >Tasks</TableHeaderColumn>

                    <TableHeaderColumn dataField='request_type' editable={ false } expandable={ false } dataFormat={ setDescription(this) } >Description</TableHeaderColumn>
                    
                    <TableHeaderColumn dataField='dept_name' editable={ false } expandable={ false } >Department</TableHeaderColumn>

                    <TableHeaderColumn dataField='dept_name' editable={ false } expandable={ false } dataFormat={ setAssignedTo(this) } >Assigned To </TableHeaderColumn>
                    
                    {/* <TableHeaderColumn dataField='date_closed' editable={ false } expandable={ false } dataFormat={ setClosedDate(this) } >Closed On</TableHeaderColumn> */}

                    <TableHeaderColumn dataField='cust_name' editable={ false } expandable={ false } dataFormat={ setCustomerName(this) } >User</TableHeaderColumn>

                    
                    <TableHeaderColumn dataField='date_added' editable={ false } expandable={ false } dataFormat={ setDateFormat(this) } >Assigned Date</TableHeaderColumn>

                    <TableHeaderColumn dataField='display_responded_date' editable={ false } expandable={ false } >Closed Date</TableHeaderColumn>

                </BootstrapTable>
                </div>
                {this.state.countClosedTasks > 20 ? (
                <Row>
                    <Col md={12}>
                    <div className="paginationOuter text-right">
                        <Pagination
                        activePage={this.state.activePage}
                        itemsCountPerPage={this.state.itemPerPage}
                        totalItemsCount={this.state.countClosedTasks}
                        itemClass='nav-item'
                        linkClass='nav-link'
                        activeClass='active'
                        hideNavigation='false'
                        onChange={this.handlePageChange}
                        />
                    </div>
                    </Col>
                </Row>
                ) : null}
            </>
        );
    }
}

export default EmployeeClosedTasksTable;