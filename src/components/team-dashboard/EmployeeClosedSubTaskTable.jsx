import React, { Component } from 'react';
import {BootstrapTable,TableHeaderColumn} from 'react-bootstrap-table';

import StatusColumn from './StatusColumn';
import { Link } from 'react-router-dom';

import { htmlDecode,localDate } from '../../shared/helper';

import dateFormat from "dateformat"; 

import {
  Tooltip,
  OverlayTrigger
} from "react-bootstrap";
import '../dashboard/Dashboard.css';

const getStatusColumn = refObj => (cell,row) => {
    return <StatusColumn rowData={row} />
}

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const setDescription = refOBj => (cell,row) =>{
  if(row.parent_id > 0){
      let stripHtml = row.title.replace(/<[^>]+>/g, '');
      return (
          <LinkWithTooltip
              tooltip={`${htmlDecode(stripHtml)}`}
              href="#"
              id="tooltip-1"
              clicked={e => refOBj.checkHandler(e)}
            >
            {htmlDecode(stripHtml)}
          </LinkWithTooltip>
      );
  }else if(row.discussion === 1){
      let stripHtml = row.comment.replace(/<[^>]+>/g, '');
      return (
          <LinkWithTooltip
              tooltip={`${htmlDecode(stripHtml)}`}
              href="#"
              id="tooltip-1"
              clicked={e => refOBj.checkHandler(e)}
            >
            {htmlDecode(stripHtml)}
          </LinkWithTooltip>
      );
  }else{
      return (      
        <LinkWithTooltip
          tooltip={`${row.req_name}`}
          href="#"
          id="tooltip-1"
          clicked={e => refOBj.checkHandler(e)}
        >
          {row.req_name}
        </LinkWithTooltip>
      );
  }
}

const clickToShowTasks = refObj => (cell,row) =>{
    //return <span onClick={() => refObj.taskDetails(row)} style={{cursor:'pointer'}} >{cell}</span>
    
    if(row.discussion === 1){
        //return <Link to={{ pathname: '/user/discussion_details/'+row.task_id }} target="_blank" style={{cursor:'pointer'}}>{cell}</Link>
        /* return <Link to={{ pathname: `/user/task_details/${row.task_id}/${refObj.props.assignId}` }} target="_blank" style={{cursor:'pointer'}}>{cell}</Link> */
        return(
          <LinkWithTooltip
              tooltip={`${cell}`}
              href="#"
              id="tooltip-1"
              clicked={e => refObj.redirectUrlTask(e, row.task_id, refObj.props.assignId)}
            >
            {cell}
          </LinkWithTooltip>
      );
    }else{
        /* return <Link to={{ pathname: `/user/task_details/${row.task_id}/${row.assignment_id}` }} target="_blank" style={{cursor:'pointer'}}>{cell}</Link> */
        return(
          <LinkWithTooltip
              tooltip={`${cell}`}
              href="#"
              id="tooltip-1"
              clicked={e => refObj.redirectUrlTask(e, row.task_id, '', row.assignment_id)}
            >
            {cell}
          </LinkWithTooltip>
      );
    }
    
}

const setEmpCustName = refObj => (cell, row) => {
  if (row.customer_drupal_id > 0) {
    //hard coded customer id - SATYAJIT
    //row.customer_id = 2;
    return (
      /*<Link
        to={{
          pathname:
            portal_url + "customer-dashboard/" +
            row.customer_drupal_id
        }}
        target="_blank"
        style={{ cursor: "pointer" }}
      >*/
        <LinkWithTooltip
            tooltip={`${row.first_name + " " + row.last_name}`}
            href="#"
            id="tooltip-1"
            clicked={e => refObj.redirectUrl(e, row.customer_drupal_id)}
          >
          {row.first_name + " " + row.last_name}
        </LinkWithTooltip>        
      /*</Link>*/
    );
  } else {
    return "";
  }
};

const setAssignedTo = refOBj => (cell,row) => {
    //return row.emp_first_name+' '+row.emp_last_name+' ('+row.desig_name+')';
    if(row.discussion === 1){
        return '';
    }else{
        return (      
            <LinkWithTooltip
              tooltip={`${row.emp_first_name+' '+row.emp_last_name+' ('+row.desig_name+')'}`}
              href="#"
              id="tooltip-1"
              clicked={e => refOBj.checkHandler(e)}
            >
              {row.emp_first_name+' '+row.emp_last_name+' ('+row.desig_name+')'}
            </LinkWithTooltip>
            );
    }
}

const setDateFormat = refObj => (cell,row) => {
  if(row.discussion === 1){
    return '';
  }else{
    var date = localDate(cell);
    return dateFormat(date, "dd/mm/yyyy");
  }
}

class EmployeeClosedSubTaskTable extends Component {

    state = {
        showCreateSubTask   : false,
        showAssign          : false,
        showRespondBack     : false
    };

    checkHandler = (event) => {
        event.preventDefault();
    };

    tdClassName = (fieldValue, row) =>{
        var dynamicClass = ' ';
        if(row.vip_customer === 1){
            dynamicClass += 'bookmarked-column ';
        }
        return dynamicClass;
    }

    render(){

        const selectRowProp = {
            bgColor       : '#fff8f6'
        };

        return (
            <>
                <BootstrapTable 
                    data={this.props.tableData} 
                    selectRow={ selectRowProp } 
                    tableHeaderClass={"col-hidden"}
                    expandColumnOptions={ 
                        { 
                            expandColumnVisible: true,
                            expandColumnComponent: this.expandColumnComponent,
                            columnWidth: 25
                        } 
                    } 
                    trClassName="tr-expandable" 
                >
                    <TableHeaderColumn isKey dataField='task_ref' dataSort={ true } columnClassName={ this.tdClassName } editable={ false }  dataFormat={ clickToShowTasks(this) } >Tasks</TableHeaderColumn>

                    <TableHeaderColumn dataField='request_type' dataSort={ true } editable={ false }  dataFormat={ setDescription(this) } >Description</TableHeaderColumn>

                    <TableHeaderColumn dataField='dept_name' dataSort={ true } editable={ false } expandable={ false } >Department</TableHeaderColumn>

                    <TableHeaderColumn dataField='dept_name' dataSort={ true } editable={ false } expandable={ false } dataFormat={ setAssignedTo(this) } >Assigned To </TableHeaderColumn>

                    <TableHeaderColumn dataField='cust_name' dataSort={ true } editable={ false }  dataFormat={ setEmpCustName(this) } >Customer Name</TableHeaderColumn>

                    
                    <TableHeaderColumn dataField='date_added' editable={ false } expandable={ false } dataFormat={ setDateFormat(this) } >Assigned Date</TableHeaderColumn>

                    <TableHeaderColumn dataField='display_responded_date' editable={ false } expandable={ false } >Respond Date</TableHeaderColumn>
                </BootstrapTable>
            </>
        );
    }

}

export default EmployeeClosedSubTaskTable;