import React, { Component } from 'react';

import EmployeeTasksTable from './EmployeeTasksTable';
import EmployeeAllocatedTasksTable from './EmployeeAllocatedTasksTable';
import EmployeeClosedTasksTable from './EmployeeClosedTasksTable';

import '../dashboard/Dashboard.css';
import API from "../../shared/axios";
import whitelogo from '../../assets/images/drreddylogo_white.png';
import swal from "sweetalert";

class TeamDashboard extends Component {

    constructor(props){
        super(props);
    }

    state = {
        myTasks       : [],
        allocatedTasks: [],  
        closedTasks   : [],
        isLoading     : true,
        invalid_access: false,
        emp_id        : 0
    }

    componentDidMount(){
        this.allTaskDetails();
    }

    allTaskDetails = (page = 1) => {
       
        this.setState({
            myTasks            : [],
            allocatedTasks     : [],  
            closedTasks        : [],
            countMyTasks       : 0,  
            countAllocatedTasks: 0,  
            countClosedTasks   : 0, 
            invalid_access     : false, 
            isLoading          : true,
            emp_name           : ""
        });

        const emp_id = this.props.match.params.id;
        
        API.get( `/api/team/member/tasks/${emp_id}?page=${page}` ).then(res => {
            this.setState({ myTasks: res.data.data, countMyTasks: res.data.count_my_tasks,emp_name: res.data.emp_name});
            API.get( `/api/team/member/allocated/${emp_id}?page=${page}` ).then(res => {
                this.setState({ allocatedTasks: res.data.data, countAllocatedTasks: res.data.count_allocated_tasks});
                API.get( `/api/team/member/closed/${emp_id}?page=${page}` ).then(res => {
                    this.setState({ closedTasks: res.data.data, countClosedTasks: res.data.count_closed_tasks,isLoading:false});
                }).catch(err => {

                });
            }).catch(err => {

            });
        }).catch(err => {
            var errText = err.data.message;
            this.setState({invalid_access:true});
            swal({
                closeOnClickOutside: false,
                title: "Error",
                text: errText,
                icon: "error"
            }).then(()=> {
                this.props.history.push('/');
            });
        });            
    }

    handleTabs = (event) =>{
        
        if(event.currentTarget.className === "active" ){
            //DO NOTHING
        }else{

            var elems          = document.querySelectorAll('[id^="tab_"]');
            var elemsContainer = document.querySelectorAll('[id^="show_tab_"]');
            var currId         = event.currentTarget.id;

            for (var i = 0; i < elems.length; i++){
                elems[i].classList.remove('active');
            }

            for (var j = 0; j < elemsContainer.length; j++){
                elemsContainer[j].style.display = 'none';
            }

            event.currentTarget.classList.add('active');
            event.currentTarget.classList.add('active');
            document.querySelector('#show_'+currId).style.display = 'block';
        }
    }


    render(){

        console.log("===========>>>", this.state)

        if(this.state.isLoading){
            if(this.state.invalid_access){
                return (
                  <>
                    
                  </>
                );
            }else{
                return(
                    <>
                        <div className="loderOuter">
                        <div className="loading_reddy_outer">
                            <div className="loading_reddy" >
                                <img src={whitelogo} alt="logo" />
                            </div>
                            </div>
                        </div>
                    </>
                );
            }
        }else{
            return(
                <>
                    <div className="content-wrapper">
                        <section className="content-header">
                            <h1>
                                <small>Dashboard Of</small>
                                {this.state.emp_name}
                            </h1>
                            <div className="back-btn" onClick={()=>this.props.history.push('/user/view_team')} >Back</div>
                            
                        </section>
                        
                        <section className="content">

                            <div className="row">
                                <div className="col-xs-12">
                                    <div className="nav-tabs-custom">
                                        <ul className="nav nav-tabs">
                                            <li className="active" onClick = {(e) => this.handleTabs(e)} id="tab_1" >TASKS ({this.state.countMyTasks})</li>
                                            <li onClick = {(e) => this.handleTabs(e)}  id="tab_2">ALLOCATED TASKS ({this.state.countAllocatedTasks})</li>
                                            <li onClick = {(e) => this.handleTabs(e)}  id="tab_3">CLOSED TASKS ({this.state.countClosedTasks})</li>
                                        </ul>

                                        <div className="tab-content">

                                            <div className="tab-pane active" id="show_tab_1">
                                                
                                                {this.state.myTasks && this.state.myTasks.length > 0 && 
                                                <EmployeeTasksTable tableData={this.state.myTasks} countMyTasks={this.state.countMyTasks} emp_id={this.props.match.params.id} />}
                                                     
                                                {this.state.myTasks && this.state.myTasks.length === 0 && <div className="noData">No Data Found</div>}

                                            </div>

                                             <div className="tab-pane" id="show_tab_2">

                                                {this.state.allocatedTasks && this.state.allocatedTasks.length > 0 && <EmployeeAllocatedTasksTable tableData={this.state.allocatedTasks} countAllocatedTasks={this.state.countAllocatedTasks} emp_id={this.props.match.params.id} />}

                                                {this.state.allocatedTasks && this.state.allocatedTasks.length === 0 && <div className="noData">No Data Found</div>}
                                            </div>

                                            <div className="tab-pane" id="show_tab_3">
                                                {this.state.closedTasks && this.state.closedTasks.length > 0 && <EmployeeClosedTasksTable tableData={this.state.closedTasks} countClosedTasks={this.state.countClosedTasks} emp_id={this.props.match.params.id} />}

                                                {this.state.closedTasks && this.state.closedTasks.length === 0 && <div className="noData">No Data Found</div>}
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </>
            );
        }
    }
}

export default TeamDashboard;