import React, { Component } from 'react';
import {BootstrapTable,TableHeaderColumn} from 'react-bootstrap-table';

import {
  Row,
  Col,
  Tooltip,
  OverlayTrigger 
} from "react-bootstrap";
import '../dashboard/Dashboard.css';

import dateFormat from 'dateformat';

import StatusColumnAllocatedTask from './StatusColumnAllocatedTask';
import EmployeeAllocatedSubTaskTable from './EmployeeAllocatedSubTaskTable';

import { Link } from 'react-router-dom';

import { localDate } from '../../shared/helper';

import API from "../../shared/axios";
import Pagination from "react-js-pagination";
import { showErrorMessageFront } from "../../shared/handle_error_front";

const priority_arr = [
    {priority_id:1,priority_value:'Low'},
    {priority_id:2,priority_value:'Medium'},
    {priority_id:3,priority_value:'High'}
];

const getStatusColumn = refObj => (cell,row) => {
    return <StatusColumnAllocatedTask rowData={row} />
}

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const setDescription = refOBj => (cell,row) =>{
    if(row.parent_id > 0){
        return (
            <LinkWithTooltip
                tooltip={`${row.title}`}
                href="#"
                id="tooltip-1"
                clicked={e => refOBj.checkHandler(e)}
              >
              {row.title}
            </LinkWithTooltip>
        );
      }else{
        return (      
          <LinkWithTooltip
            tooltip={`${row.req_name}`}
            href="#"
            id="tooltip-1"
            clicked={e => refOBj.checkHandler(e)}
          >
            {row.req_name}
          </LinkWithTooltip>
          );
      }
}

const setCreateDate = refObj => cell =>{
    var date = localDate(cell);
    return dateFormat(date, "dd/mm/yyyy");
}

const setDaysPending = refObj => cell =>{
    var date = localDate(cell);
    return dateFormat(date, "dd/mm/yyyy");
}

const clickToShowTasks = refObj => (cell,row) =>{
    if(row.discussion === 1){
        //return <Link to={{ pathname: '/user/discussion_details/'+row.task_id }} target="_blank" style={{cursor:'pointer'}}>{cell}</Link>
        return(
            <LinkWithTooltip
                tooltip={`${cell}`}
                href="#"
                id="tooltip-1"
                clicked={e => refObj.redirectUrlTask(e, row.task_id)}
              >
              {cell}
            </LinkWithTooltip>
        );
    }else{
        //return <Link to={{ pathname: `/user/task_details/${row.task_id}/${row.assignment_id}` }} target="_blank" style={{cursor:'pointer'}}>{cell}</Link>
        return(
            <LinkWithTooltip
              tooltip={`${cell}`}
              href="#"
              id="tooltip-1"
              clicked={e => refObj.redirectUrlTask(e, row.task_id, row.assignment_id)}
            >
              {cell}
            </LinkWithTooltip>
        );
    }
}

const setCustomerName = refObj => (cell,row) =>{
    if(row.customer_id > 0){
        return (
            <LinkWithTooltip
                tooltip={`${row.first_name + " " + row.last_name}`}
                href="#"
                id="tooltip-1"
                clicked={e => refObj.redirectUrl(e, row.customer_drupal_id)}
              >
              {row.first_name + " " + row.last_name}
            </LinkWithTooltip>
        );
    }else{
        return "";
    }
}

const setPriorityName = refObj => (cell,row) =>{
    var ret = 'Set Priority';
    for (let index = 0; index < priority_arr.length; index++) {
        const element = priority_arr[index];
        if(element['priority_id'] === cell ){
            ret = element['priority_value'];
        }
    }

    return ret

}

const setAssignedTo = refOBj => (cell,row) => {
    return (    
    <LinkWithTooltip
        tooltip={`${row.emp_first_name + " " + row.emp_last_name + " (" + row.desig_name + ")"}`}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {row.emp_first_name + " " + row.emp_last_name + " (" + row.desig_name + ")"}
    </LinkWithTooltip>
  );
}

class EmployeeAllocatedTasksTable extends Component {

    state = {
        tableData  : [],
        activePage : 1,
        totalCount : 0,
        itemPerPage: 20,
    };

    constructor(props){
      super(props);
    }

    checkHandler = (event) => {
        event.preventDefault();
    };

    componentDidMount(){
        //console.log('>>>>>',this.props.countAllocatedTasks);
        this.setState({
            tableData:this.props.tableData,
            countAllocatedTasks: this.props.countAllocatedTasks,
            options : {
                clearSearch: true,
                expandBy: 'column',
                page: !this.state.start_page ? 1 : this.state.start_page,// which page you want to show as default
                sizePerPageList: [ {
                    text: '10', value: 10
                }, {
                    text: '20', value: 20
                }, {
                    text: 'All', value: !this.state.tableData ? 1 : this.state.tableData
                } ], // you can change the dropdown list for size per page
                sizePerPage: 10,  // which size per page you want to locate as default
                pageStartIndex: 1, // where to start counting the pages
                paginationSize: 3,  // the pagination bar size.
                prePage: '‹', // Previous page button text
                nextPage: '›', // Next page button text
                firstPage: '«', // First page button text
                lastPage: '»', // Last page button text
                //paginationShowsTotal: this.renderShowsTotal,  // Accept bool or function
                paginationPosition: 'bottom'  // default is bottom, top and both is all available
                // hideSizePerPage: true > You can hide the dropdown for sizePerPage
                // alwaysShowAllBtns: true // Always show next and previous button
                // withFirstAndLast: false > Hide the going to First and Last page button
            },
            cellEditProp:{
                mode: 'click',
                beforeSaveCell: this.onBeforeSetPriority, // a hook for before saving cell
                afterSaveCell: this.onAfterSetPriority // a hook for after saving cell
            }
        });
    }

    handlePageChange = (pageNumber) => {
        this.setState({ activePage: pageNumber });
        this.getMyTasks(pageNumber > 0 ? pageNumber  : 1);
    };
    
    getMyTasks(page = 1) {
        let url;
        if(this.props.queryString !== '' ){
            url = `/api/team/member/allocated/${this.props.emp_id}?page=${page}&${this.props.queryString}`; 
        }else{
            url = `/api/team/member/allocated/${this.props.emp_id}?page=${page}`;
        }
        API.get(url)
        .then(res => {
        this.setState({ 
            tableData: res.data.data, 
            countAllocatedTasks: res.data.count_allocated_tasks});
        })
        .catch(err => { 
            showErrorMessageFront(err,this.props);
        });
    }
    
    getSubTasks = (row) => {
        return(
            <EmployeeAllocatedSubTaskTable dashType={this.props.dashType} tableData={row.sub_tasks} reloadTaskSubTask={()=>this.props.allTaskDetails()} 
            assignId={row.assignment_id} />
        );
    }

    checkSubTasks = (row) => {

        //console.log("subtask")

        if( typeof row.sub_tasks !== 'undefined' && row.sub_tasks.length > 0 ){
            return true;
        }else{
            return false;
        }
    }

    handleClose = (closeObj) => {
        this.setState(closeObj);
    }

    tdClassName = (fieldValue, row) =>{
        var dynamicClass = 'width-150 ';
        if(row.vip_customer === 1){
            dynamicClass += 'bookmarked-column ';
        }
        return dynamicClass;
    }

    trClassName = (row,rowIndex) =>{

        var dueDate = localDate(row.due_date);
        var today  = new Date();
        var timeDiff = dueDate.getTime() - today.getTime();
        var ret;
        if(timeDiff > 0){

        }else{
            if(row.vip_customer === 1){
                ret += 'tr-red';
            }
        }

        return ret;
    }

    expandColumnComponent({ isExpandableRow, isExpanded }) {
        let content = '';
    
        if (isExpandableRow) {
          content = (isExpanded ? '-' : '+' );
        } else {
          content = ' ';
        }
        return (
          <div> { content } </div>
        );
    }

    refreshTableStatus = (currRow) => {
        currRow.status = 2;
    }

    render(){

        return (
            <>  
                <BootstrapTable 
                    data={this.state.tableData}
                    /* options={ paginationOptions } 
                    pagination */ 
                    expandableRow={ this.checkSubTasks } 
                    expandComponent={ this.getSubTasks }
                    expandColumnOptions={ 
                        { 
                            expandColumnVisible: true,
                            expandColumnComponent: this.expandColumnComponent,
                            columnWidth: 25
                        } 
                    }
                    trClassName={ this.trClassName }
                    >
                    <TableHeaderColumn isKey={true} dataField='task_ref' columnClassName={ this.tdClassName } editable={ false } expandable={ false } dataFormat={ clickToShowTasks(this) } >Tasks</TableHeaderColumn>

                    <TableHeaderColumn dataField='request_type' editable={ false } expandable={ false } dataFormat={ setDescription(this) } >Description</TableHeaderColumn>

                    <TableHeaderColumn dataField='date_added' dataFormat={ setCreateDate(this) } editable={ false } expandable={ false }>Created</TableHeaderColumn>

                    {/* <TableHeaderColumn dataField='rdd' dataFormat={ setCreateDate(this) } editable={ false } expandable={ false }>RDD</TableHeaderColumn> */}

                    <TableHeaderColumn dataField='new_due_date' editable={ false } expandable={ false } dataFormat={ setDaysPending(this) } >Due Date</TableHeaderColumn>

                    <TableHeaderColumn dataField='dept_name' editable={ false } expandable={ false } >Department</TableHeaderColumn>

                    <TableHeaderColumn dataField='dept_name' editable={ false } expandable={ false } dataFormat={ setAssignedTo(this) } >Assigned To </TableHeaderColumn>

                    <TableHeaderColumn dataField='priority' dataFormat={ setPriorityName(this) } expandable={ false } >Priority</TableHeaderColumn>

                    <TableHeaderColumn dataField='cust_name' editable={ false } expandable={ false } dataFormat={ setCustomerName(this) } >User</TableHeaderColumn>

                    <TableHeaderColumn dataField='status' dataFormat={ getStatusColumn(this) } expandable={ false } editable={ false } >Status</TableHeaderColumn>
                    
                </BootstrapTable>

                {this.state.countAllocatedTasks > 20 ? (
                <Row>
                    <Col md={12}>
                    <div className="paginationOuter text-right">
                        <Pagination
                        activePage={this.state.activePage}
                        itemsCountPerPage={this.state.itemPerPage}
                        totalItemsCount={this.state.countAllocatedTasks}
                        itemClass='nav-item'
                        linkClass='nav-link'
                        activeClass='active'
                        hideNavigation='false'
                        onChange={this.handlePageChange}
                        />
                    </div>
                    </Col>
                </Row>
                ) : null}

            </>
        );
    }
}

export default EmployeeAllocatedTasksTable;