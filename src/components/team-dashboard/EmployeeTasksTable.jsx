import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  Tooltip,
  OverlayTrigger
} from "react-bootstrap";
import '../dashboard/Dashboard.css';

import dateFormat from "dateformat";

import StatusColumn from "./StatusColumn";
import EmployeeSubTaskTable from "./EmployeeSubTaskTable";

import API from "../../shared/axios";

import { Link } from "react-router-dom";
import Pagination from "react-js-pagination";
import { showErrorMessageFront } from "../../shared/handle_error_front";

import { localDate } from '../../shared/helper';

const priority_arr = [
  { priority_id: 1, priority_value: "Low" },
  { priority_id: 2, priority_value: "Medium" },
  { priority_id: 3, priority_value: "High" }
];

const getStatusColumn = refObj => (cell, row) => {
  return <StatusColumn rowData={row} />;
};

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const setDescription = refOBj => (cell, row) => {

  if(row.parent_id > 0){
    let stripHtml = row.title.replace(/<[^>]+>/g, '');
    
    return (
        <LinkWithTooltip
            tooltip={`${stripHtml}`}
            href="#"
            id="tooltip-1"
            clicked={e => refOBj.checkHandler(e)}
          >
          {stripHtml}
        </LinkWithTooltip>
    );
  }else{

    let stripHtml = row.req_name.replace(/<[^>]+>/g, '');

    return (      
      <LinkWithTooltip
        tooltip={`${stripHtml}`}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {stripHtml}
      </LinkWithTooltip>
      );
  }
};


const setCreateDate = refObj => cell => {
  var date = localDate(cell);
  return dateFormat(date, "dd/mm/yyyy");
};

const setDaysPending = refObj => (cell, row) => {
  var date = localDate(cell);
  return dateFormat(date, "dd/mm/yyyy");
};

const clickToShowTasks = refObj => (cell, row) => {
  if (row.discussion === 1) {

    return(
      <LinkWithTooltip
          tooltip={`${cell}`}
          href="#"
          id="tooltip-1"
          clicked={e => refObj.checkHandler(e)}
        >
        {cell}
      </LinkWithTooltip>
    );
  } else {

    return(
      <LinkWithTooltip
        tooltip={`${cell}`}
        href="#"
        id="tooltip-1"
        clicked={e => refObj.checkHandler(e)}
      >
        {cell}
      </LinkWithTooltip>
    );
  }
};

const priorityEditor = refObj => (onUpdate, props) => {
  ///console.log(props.row.priority);
  //refObj.setState({ updt_priority:props.row.priority,updt_priority_value:props.row.priority_value });
  return (
    <>
      <select
        value={
          refObj.state.updt_priority > 0
            ? refObj.state.updt_priority
            : props.row.priority
        }
        onChange={ev => {
          refObj.setState({ updt_priority: ev.currentTarget.value });
        }}
      >
        {priority_arr.map(priority => (
          <option key={priority.priority_id} value={priority.priority_id}>
            {priority.priority_value}
          </option>
        ))}
      </select>
      <button
        className="btn btn-info btn-xs textarea-save-btn"
        onClick={() => {
          //AXIOS CALL TO UPDATE THE PRIORITY

          props.row.priority = refObj.state.updt_priority;
          onUpdate(refObj.state.updt_priority);
        }}
      >
        save
      </button>
    </>
  );
};

const setCustomerName = refObj => (cell, row) => {
  if (row.customer_drupal_id > 0) {
    //hard coded customer id - SATYAJIT
    //row.customer_id = 2;
    return (
      /*<Link
        to={{
          pathname:
            portal_url + "customer-dashboard/" +
            row.customer_drupal_id
        }}
        target="_blank"
        style={{ cursor: "pointer" }}
      >*/
        <LinkWithTooltip
            tooltip={`${row.first_name + " " + row.last_name}`}
            href="#"
            id="tooltip-1"
            clicked={e => refObj.checkHandler(e)}
          >
          {row.first_name + " " + row.last_name}
        </LinkWithTooltip>        
      /*</Link>*/
    );
  } else {
    return "";
  }
};

const setPriorityName = refObj => (cell, row) => {
  var ret = "Set Priority";
  for (let index = 0; index < priority_arr.length; index++) {
    const element = priority_arr[index];
    if (element["priority_id"] === cell) {
      ret = element["priority_value"];
    }
  }

  return ret;
};

const setAssignedTo = refOBj => (cell, row) => {
  return (    
    <LinkWithTooltip
        tooltip={`${row.emp_first_name + " " + row.emp_last_name + " (" + row.desig_name + ")"}`}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {row.emp_first_name + " " + row.emp_last_name + " (" + row.desig_name + ")"}
    </LinkWithTooltip>
  );
};

class EmployeeTasksTable extends Component {
  state = {
    showCreateSubTask: false,
    showAssign: false,
    showRespondBack: false,
    showTaskDetails: false,
    showRespondCustomer: false,
    showAuthorizeBack: false,
    changeData: true,
    task_id: 0,
    tableData: [],

    activePage: 1,
    totalCount: 0,
    itemPerPage: 20,
  };

  checkHandler = (event) => {
    event.preventDefault();
  };

  taskDetails = row => {
    this.setState({ showTaskDetails: true, currRow: row });
  };

  componentDidMount() {
    
    this.setState({
      tableData   : this.props.tableData,
      countMyTasks: this.props.countMyTasks,
      options: {
        clearSearch: true,
        expandBy   : "column",
        page       : !this.state.start_page ? 1: this.state.start_page,
        sizePerPageList: [
          {
            text : "10",
            value: 10
          },
          {
            text : "20",
            value: 20
          },
          {
            text : "All",
            value: !this.state.tableData ? 1: this.state.tableData
          }
        ], 
        sizePerPage   : 10, 
        pageStartIndex: 1, 
        paginationSize: 3, 
        prePage       : "‹", 
        nextPage      : "›", 
        firstPage     : "«", 
        lastPage      : "»", 
        paginationPosition: "bottom"
      },
      cellEditProp: {
        mode          : "click",
        beforeSaveCell: this.onBeforeSetPriority, 
        afterSaveCell : this.onAfterSetPriority 
      }
    });
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getMyTasks(pageNumber > 0 ? pageNumber  : 1);
  };

  getMyTasks(page = 1) {
    let url;
    if(this.props.queryString !== '' ){
      url = `/api/team/member/tasks/${this.props.emp_id}?page=${page}&${this.props.queryString}`; 
    }else{
      url = `/api/team/member/tasks/${this.props.emp_id}?page=${page}`;
    }
    API.get(url)
      .then(res => {
        this.setState({ 
          tableData: res.data.data, 
          countMyTasks: res.data.count_my_tasks});
      })
      .catch(err => { 
        showErrorMessageFront(err,this.props);
      });
  }

  getSubTasks = row => {
    return (
      <EmployeeSubTaskTable
        tableData={row.sub_tasks}
      />
    );
  };

  checkSubTasks = row => {
    if (typeof row.sub_tasks !== "undefined" && row.sub_tasks.length > 0) {
      return true;
    } else {
      return false;
    }
  };

  refreshTableStatus = currRow => {
    currRow.status = 2;
  };

  tdClassName = (fieldValue, row) => {
    var dynamicClass = "width-150 ";
    if (row.vip_customer === 1) {
      dynamicClass += "bookmarked-column ";
    }
    return dynamicClass;
  };

  trClassName = (row, rowIndex) => {
    var ret = " ";
    var selDueDate = row.due_date;
    var dueDate = localDate(selDueDate);
    var today = new Date();
    var timeDiff = dueDate.getTime() - today.getTime();

    if (timeDiff > 0) {
    } else {
      if (row.vip_customer === 1) {
        ret += "tr-red";
      }
    }

    return ret;
  };

  expandColumnComponent({ isExpandableRow, isExpanded }) {
    let content = "";

    if (isExpandableRow) {
      content = isExpanded ? "-" : "+";
    } else {
      content = " ";
    }
    return <div> {content} </div>;
  }

  render() {
    return (
      <>
        {this.state.changeData && (
          <BootstrapTable
            data={this.state.tableData}
            expandableRow={this.checkSubTasks}
            expandComponent={this.getSubTasks}
            expandColumnOptions={{
              expandColumnVisible: true,
              expandColumnComponent: this.expandColumnComponent,
              columnWidth: 25
            }}
            trClassName={this.trClassName}
            cellEdit={this.state.cellEditProp}
          >
            <TableHeaderColumn
              dataField="task_ref"
              isKey={true}
              columnClassName={this.tdClassName}
              editable={false}
              expandable={false}
              dataFormat={clickToShowTasks(this)}
            >
              Tasks
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="request_type"
              editable={false}
              expandable={false}
              dataFormat={setDescription(this)}
            >
              Description
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="date_added"
              dataFormat={setCreateDate(this)}
              editable={false}
              expandable={false}
            >
              Created
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="new_due_date"
              editable={false}
              expandable={false}
              dataFormat={setDaysPending(this)}
            >
              Due Date
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="dept_name"
              editable={false}
              expandable={false}
            >
              Department
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="dept_name"
              editable={false}
              expandable={false}
              dataFormat={setAssignedTo(this)}
            >
              Assigned To{" "}
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="priority"
              expandable={false}
              customEditor={{ getElement: priorityEditor(this) }}
              dataFormat={setPriorityName(this)}
            >
              Priority
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="cust_name"
              editable={false}
              expandable={false}
              dataFormat={setCustomerName(this)}
            >
              User
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="status"
              dataFormat={getStatusColumn(this)}
              expandable={false}
              editable={false}
            >
              Status
            </TableHeaderColumn>
          </BootstrapTable>
        )}

        {this.state.countMyTasks > 20 ? (
          <Row>
            <Col md={12}>
              <div className="paginationOuter text-right">
                <Pagination
                  activePage={this.state.activePage}
                  itemsCountPerPage={this.state.itemPerPage}
                  totalItemsCount={this.state.countMyTasks}
                  itemClass='nav-item'
                  linkClass='nav-link'
                  activeClass='active'
                  hideNavigation='false'
                  onChange={this.handlePageChange}
                />
              </div>
            </Col>
          </Row>
          ) : null}

        </>
    );
  }
}

export default EmployeeTasksTable;
