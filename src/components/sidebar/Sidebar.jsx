import React, { Component } from 'react';
import './Sidebar.css';
import { Link, withRouter } from 'react-router-dom';
import { Button, ButtonToolbar, Modal } from "react-bootstrap";

import DashboradImage from '../../assets/images/dashboard-icon.svg';
import MyTeamImage from '../../assets/images/team-icon.svg';
import Catlogmage from '../../assets/images/catalog-icon.svg';
import UtilitiesImage from '../../assets/images/utlities-icon.svg';
import LeaveImage from '../../assets/images/leave-icon.svg';
import DocumentImage from '../../assets/images/document-icon.svg';

import DashboradImageActive from '../../assets/images/dashboard-icon-active.svg';
import MyTeamImageActive from '../../assets/images/team-icon-active.svg';
import CatlogmageActive from '../../assets/images/catalog-icon-active.svg';
import LeaveImageActive from '../../assets/images/leave-icon-active.svg';
import UtilitiesImageActive from '../../assets/images/utlities-icon-active.svg';
import { displayLeaveForm, displayCOASearch } from "../../shared/helper";

// connect to store
import { connect } from 'react-redux';
import { authLogout, getNotifications, getNotificationCount } from "../../store/actions/auth";

class Sidebar extends Component {

  constructor() {
    super();
    this.state = {
      showModal: false
    }
  }

  modalShowHandler = (event) => {
    event.preventDefault();
    this.setState({ showModal: true });
  };
  modalCloseHandler = () => {
    this.setState({ showModal: false });
  };


  redirectEvent(event) {
    event.preventDefault();
    this.props.getNotificationCount();
  }
  render() {
    if (this.props.isLoggedIn === false) return null;

    const isDisplayLeave = displayLeaveForm();
    let leaveButton; let coaSearchLink;

    if (window.location.pathname === "/user/leave") {
      leaveButton = <li className="active">
        <Link to={'/user/leave'} >
          <img src={LeaveImage} alt="Leave" className="main-icon" />
          <img src={LeaveImageActive} alt="Leave" className="main-icon-active" />
          <span className="static-text">Leave <br />Management</span>
        </Link>
      </li>;
    } else {
      leaveButton = <li>
        <Link to={'/user/leave'} >
          <img src={LeaveImage} alt="Leave" className="main-icon" />
          <img src={LeaveImageActive} alt="Leave" className="main-icon-active" />
          <span className="static-text">Leave <br />Management</span>
        </Link>
      </li>;
    }

    if (window.location.pathname === "/user/typical_coa") {
      coaSearchLink = <li className="active">
        <Link to={'/user/typical_coa'} >
          <img src={LeaveImage} alt="Leave" className="main-icon" />
          <img src={LeaveImageActive} alt="Leave" className="main-icon-active" />
          <span className="static-text">Typical COA <br /></span>
        </Link>
      </li>;
    } else {
      coaSearchLink = <li>
        <Link to={'/user/typical_coa'} >
          <img src={LeaveImage} alt="Leave" className="main-icon" />
          <img src={LeaveImageActive} alt="Leave" className="main-icon-active" />
          <span className="static-text">Typical COA <br /></span>
        </Link>
      </li>;
    }



    return (
      <>
        <aside className="main-sidebar">
          {/*<!-- sidebar: style can be found in sidebar.less -->*/}
          <section className="sidebar">
            {/*<!-- sidebar menu: : style can be found in sidebar.less -->*/}
            <ul className="sidebar-menu">
              <li className="header">MAIN NAVIGATION</li>
              {/* <li className="treeview">
                    <Link to={'/user/dashboard'}>
                      <i className="fas fa-tachometer-alt main-icon"></i> <span className="static-text">Dashboard</span>
                    </Link>                    
                  </li> */}

              {window.location.pathname === "/user/dashboard" ?
                <li className="treeview active"> <Link to={'/user/dashboard'}>
                  <img src={DashboradImage} alt="Dashboard" className="main-icon" />
                  <img src={DashboradImageActive} alt="Dashboard" className="main-icon-active" />
                  <span className="static-text">Dashboard</span>
                </Link> </li> :
                <li className="treeview"> <Link to={'/user/dashboard'}>
                  <img src={DashboradImage} alt="Dashboard" className="main-icon" />
                  <img src={DashboradImageActive} alt="Dashboard" className="main-icon-active" />
                  <span className="static-text">Dashboard</span>
                </Link> </li>}
              {window.location.pathname === "/user/my_team" ?
                <li className="active">
                  <Link to={'/user/my_team'}>
                    <img src={MyTeamImage} alt="Team" className="main-icon" />
                    <img src={MyTeamImageActive} alt="Team" className="main-icon-active" />
                    <span className="static-text">My <br />Team</span>
                  </Link>
                </li> :
                <li>
                  <Link to={'/user/my_team'}>
                    <img src={MyTeamImage} alt="Team" className="main-icon" />
                    <img src={MyTeamImageActive} alt="Team" className="main-icon-active" />
                    <span className="static-text">My <br />Team</span>
                  </Link>
                </li>}

              {/* <li className=""> */}
              {/* <a href="javascript:void(0);" onClick={e => this.modalShowHandler()}>
                      <i className="fa fa-th main-icon"></i> <span className="static-text" >Product <br />Catalog</span>
                    </a> */}
              {/* <Link to="#" onClick={e => this.modalShowHandler(e)}>
                      <img src={Catlogmage} alt="Product Catalog" className="main-icon" />
                      <img src={CatlogmageActive} alt="Product Catalog" className="main-icon-active" />
                      <span className="static-text" >Product <br />Catalog</span>
                    </Link>
                  </li> */}
              {/* <li className="treeview">
                    <Link to={'/my_cft_members'}> 
                      <i className="fa fa-users main-icon"></i>
                      <span className="static-text"> My CFT <br />Members</span>
                      <span className="pull-right-container">
                        <i className="pull-right"></i>
                      </span>
                    </Link>
                  </li> */}

              {window.location.pathname === "/user/product_catalogue" ?
                <li className="active">
                  <Link to={'/user/product_catalogue'}>
                    <img src={Catlogmage} alt="Product Catalogue" className="main-icon" />
                    <img src={CatlogmageActive} alt="Product Catalogue" className="main-icon-active" />
                    <span className="static-text">Product <br />Catalogue</span>
                  </Link>
                </li> :
                <li>
                  <Link to={'/user/product_catalogue'}>
                    <img src={Catlogmage} alt="Product Catalogue" className="main-icon" />
                    <img src={CatlogmageActive} alt="Product Catalogue" className="main-icon-active" />
                    <span className="static-text">Product <br />Catalogue</span>
                  </Link>
                </li>
              }

             {displayCOASearch() && coaSearchLink} 

              {window.location.pathname === '/user/utilities/document_search' ?
                <li className="treeview active"><Link to="#" onClick={this.redirectEvent.bind(this)}>
                  <img src={UtilitiesImage} alt="Utlities" className="main-icon" />
                  <img src={UtilitiesImageActive} alt="Utlities" className="main-icon-active" />
                  <span className="static-text">Utlities</span>
                  <span className="pull-right-container">
                    <i className="fa fa-angle-right"></i>
                  </span>
                </Link>
                  <ul className="treeview-menu">
                    <li><Link to={'/user/utilities/document_search'}>
                      <img src={DocumentImage} alt="Product Catalog" />
                      Document Search</Link></li>
                  </ul>
                </li> :
                <li className="treeview"><Link to="#" onClick={this.redirectEvent.bind(this)}>
                  <img src={UtilitiesImage} alt="Utlities" className="main-icon" />
                  <img src={UtilitiesImageActive} alt="Utlities" className="main-icon-active" />
                  <span className="static-text">Utlities</span>
                  <span className="pull-right-container">
                    <i className="fa fa-angle-right"></i>
                  </span>
                </Link>
                  <ul className="treeview-menu">
                    <li><Link to={'/user/utilities/document_search'}>
                      <img src={DocumentImage} alt="Product Catalog" />
                      Document Search</Link></li>
                  </ul>
                </li>}


              {/* <li><Link to={'/user/utilities/search_by_customer'}><i className="fas fa-search"></i> Search by Customer</Link></li>
                      <li><Link to={'/user/utilities/pricing_guidance'}><i className="fas fa-tags"></i> Pricing Guidance</Link></li>
                      <li><Link to={'/user/utilities/lead_time'}><i className="fas fa-clock"></i> Lead Time</Link></li>
                      <li><Link to={'/user/utilities/sla_reference'}><i className="fas fa-copy"></i> SLA Reference</Link></li> */}

              {isDisplayLeave && leaveButton}

              {/* <li>
                      <Link to={'/user/create_proforma'}>
                        <img src={Catlogmage} alt="Create Proforma" className="main-icon" />
                        <img src={CatlogmageActive} alt="Create Proforma" className="main-icon-active" />
                        <span className="static-text">Create <br />Proforma</span>
                      </Link>
                    </li> */}              

            </ul>
          </section>
          {/*<!-- /.sidebar -->*/}
        </aside>
        <Modal show={this.state.showModal} onHide={() => this.modalCloseHandler()} backdrop="static">
          <Modal.Header>
            <Modal.Title>&nbsp;</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <div className="contBox">
              <span>Coming soon.</span>
            </div>
          </Modal.Body>

          <Modal.Footer>
            <button className={`btn-fill m-r-10`} type="button" onClick={e => this.modalCloseHandler()}>Close</button>

          </Modal.Footer>
        </Modal>
      </>

    );
  }
}


const mapStateToProps = state => {
  //console.log('state',state);
  return {
    ...state
  };
};

const mapDispatchToProps = dispatch => {
  //console.log("dispatch called")
  return {
    clickOpenNotification: (data, onSuccess, setErrors) => dispatch(getNotifications(data, onSuccess, setErrors)),
    getNotificationCount: (data, onSuccess, setErrors) => dispatch(getNotificationCount(data, onSuccess, setErrors)),
    logMeOut: () => dispatch(authLogout())

  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Sidebar));

//export default Sidebar;