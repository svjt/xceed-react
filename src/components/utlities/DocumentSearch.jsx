import React, { Component } from "react";
import { Button } from "react-bootstrap";

import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import API from "../../shared/axios";
import Select from "react-select";

//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
import { showErrorMessageFront } from "../../shared/handle_error_front";

const initialFileValues = {
  showLoader: false,
  keyword: "",
};

const initialProductValues = {
  showLoader: false,
  product_id: "",
};

const initialTicketValues = {
  showLoader: false,
  ticket_id: "",
};

class DocumentSearch extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showLoader: false,
      products: [],
      filenamesearch: [],
      isFileNameSearch: false,
      productfilesearch: [],
      isProductFileSearch: false,
      ticketfilesearch: [],
      isTicketFileSearch: false,
    };
  }

  handleSearchKeyword = (e) => {
    this.setState({ keyword: e.target.value });
  };

  searchFileName = (values, actions) => {
    this.setState({
      showLoader: true,
    });
    API.get(`/api/drl/filelist/${encodeURIComponent(values.keyword)}`)
      .then((res) => {
        this.setState({
          filenamesearch: res.data.data,
          isFileNameSearch: true,
          showLoader: false,
        });
        values.keyword = "";
        actions.setSubmitting(false);
      })
      .catch((error) => {
        this.setState({
          showLoader: false,
        });
        if (error.data.status === 3) {
          var token_rm = 2;
          showErrorMessageFront(error, token_rm, this.props);
        } else {
          actions.setErrors(error.data);
          actions.setSubmitting(false);
        }
      });
  };

  searchProductFileName = (values, actions) => {
    this.setState({
      showLoader: true,
    });
    API.get(`/api/drl/productfiles/${values.product_id}`)
      .then((res) => {
        this.setState({
          productfilesearch: res.data.data,
          isProductFileSearch: true,
          showLoader: false,
        });
        values.product_id = "";
        actions.setSubmitting(false);
      })
      .catch((error) => {
        this.setState({
          showLoader: false,
        });
        if (error.data.status === 3) {
          var token_rm = 2;
          showErrorMessageFront(error, token_rm, this.props);
        } else {
          actions.setErrors(error.data);
          actions.setSubmitting(false);
        }
      });
  };

  searchTicketFileName = (values, actions) => {
    this.setState({
      showLoader: true,
    });
    API.get(`/api/drl/ticketfiles/${values.ticket_id}`)
      .then((res) => {
        this.setState({
          ticketfilesearch: res.data.data,
          isTicketFileSearch: true,
          showLoader: false,
        });
        values.ticket_id = "";
        actions.setSubmitting(false);
      })
      .catch((error) => {
        this.setState({
          showLoader: false,
        });
        if (error.data.status === 3) {
          var token_rm = 2;
          showErrorMessageFront(error, token_rm, this.props);
        } else {
          actions.setErrors(error.data);
          actions.setSubmitting(false);
        }
      });
  };

  componentWillMount = () => {
    API.get(`/api/drl/products`)
      .then((res) => {
        this.setState({
          products: res.data.data,
        });
      })
      .catch((error) => {
        if (error.data.status === 3) {
          var token_rm = 2;
          showErrorMessageFront(error, token_rm, this.props);
        } else {
          console.log("error in fetching product", error);
        }
      });
  };

  searchWithProductId = (event, setFieldValue) => {
    if (event !== null) {
      setFieldValue("product_id", event.value);
    }
  };

  render() {
    const validateFileNameSearch = Yup.object().shape({
      keyword: Yup.string().required("Please enter file name"),
    });

    const validateProductFileSearch = Yup.object().shape({
      product_id: Yup.string().required("Please select product id"),
    });

    const validateTicketFileSearch = Yup.object().shape({
      ticket_id: Yup.number().required("Please enter ticket id"),
    });

    if (this.state.showLoader) {
      return (
        <div className="loderOuter">
          <div className="loader">
            <img src={loaderlogo} alt="logo" />
            <div className="loading">Loading...</div>
          </div>
        </div>
      );
    } else {
      return (
        <>
          <div className="content-wrapper">
            <section className="content-header">
              <h1>Document Search</h1>
            </section>
            <section className="content">
              <div className="row">
                {/* SEARCH WITH FILE NAME */}
                <div className="col-md-4">
                  <div className="col-main">
                    <Formik
                      initialValues={initialFileValues}
                      validationSchema={validateFileNameSearch}
                      onSubmit={this.searchFileName}
                    >
                      {({
                        values,
                        errors,
                        touched,
                        isValid,
                        isSubmitting,
                        setFieldValue,
                      }) => {
                        //console.log(errors);
                        return (
                          <Form encType="multipart/form-data">
                            {this.state.showLoader === true ? (
                              <div className="loderOuter">
                                <div className="loader">
                                  <img src={loaderlogo} alt="logo" />
                                  <div className="loading">Loading...</div>
                                </div>
                              </div>
                            ) : (
                              ""
                            )}

                            <div className="doc_ser_box">
                              <div className="form-group checText">
                                <label>
                                  File Name{" "}
                                  <span className="required-field">*</span>
                                </label>
                                <div className="form-group checText"></div>
                                <Field
                                  name="keyword"
                                  type="text"
                                  className={`selectArowGray form-control`}
                                  autoComplete="off"
                                ></Field>
                                {errors.keyword && touched.keyword ? (
                                  <span className="errorMsg">
                                    {errors.keyword}
                                  </span>
                                ) : null}
                              </div>

                              <Button
                                className={`btn-fill ${
                                  isValid ? "btn-custom-green" : "btn-disable"
                                } m-r-10`}
                                type="submit"
                                disabled={isValid ? false : true}
                              >
                                {this.state.stopflagId > 0
                                  ? isSubmitting
                                    ? "Updating..."
                                    : "Update"
                                  : isSubmitting
                                  ? "Submitting..."
                                  : "Submit"}
                              </Button>
                              <div className="clearFix"></div>
                            </div>
                          </Form>
                        );
                      }}
                    </Formik>
                    {this.state.isFileNameSearch === true ? (
                      this.state.filenamesearch.length > 0 ? (
                        this.state.filenamesearch.map((file, i) => (
                          <p key={i}>
                            <a href={file.filelink} target="_blank">
                              {file.filename}
                            </a>
                          </p>
                        ))
                      ) : (
                        <p>No file was found</p>
                      )
                    ) : (
                      ""
                    )}
                  </div>
                </div>

                {/* SEARCH WITH PRODUCT NAME */}

                <div className="col-md-4">
                  <div className="col-main">
                    <Formik
                      initialValues={initialProductValues}
                      validationSchema={validateProductFileSearch}
                      onSubmit={this.searchProductFileName}
                    >
                      {({
                        values,
                        errors,
                        touched,
                        isValid,
                        isSubmitting,
                        setFieldValue,
                      }) => {
                        return (
                          <Form encType="multipart/form-data">
                            {this.state.showLoader === true ? (
                              <div className="loderOuter">
                                <div className="loader">
                                  <img src={loaderlogo} alt="logo" />
                                  <div className="loading">Loading...</div>
                                </div>
                              </div>
                            ) : (
                              ""
                            )}

                            <div className="doc_ser_box">
                              <div className="form-group checText">
                                <label>Select Product</label>
                                <Select
                                  className="basic-single"
                                  classNamePrefix="select"
                                  isClearable={true}
                                  isSearchable={true}
                                  name="product_id"
                                  options={this.state.products}
                                  onChange={(e) =>
                                    this.searchWithProductId(e, setFieldValue)
                                  }
                                />
                                {errors.product_id && touched.product_id ? (
                                  <span className="errorMsg">
                                    {errors.product_id}
                                  </span>
                                ) : null}
                              </div>

                              <Button
                                className={`btn-fill ${
                                  isValid ? "btn-custom-green" : "btn-disable"
                                } m-r-10`}
                                type="submit"
                                disabled={isValid ? false : true}
                              >
                                {this.state.stopflagId > 0
                                  ? isSubmitting
                                    ? "Updating..."
                                    : "Update"
                                  : isSubmitting
                                  ? "Submitting..."
                                  : "Submit"}
                              </Button>
                              <div className="clearFix"></div>
                            </div>
                          </Form>
                        );
                      }}
                    </Formik>

                    {this.state.isProductFileSearch === true ? (
                      this.state.productfilesearch.length > 0 ? (
                        this.state.productfilesearch.map((file, i) => (
                          <p key={i}>
                            <a href={file.filelink} target="_blank">
                              {file.filename}
                            </a>
                          </p>
                        ))
                      ) : (
                        <p>No file was found</p>
                      )
                    ) : (
                      ""
                    )}
                  </div>
                </div>

                {/* SEARCH WITH TICKET ID */}

                <div className="col-md-4">
                  <div className="col-main">
                    <Formik
                      initialValues={initialTicketValues}
                      validationSchema={validateTicketFileSearch}
                      onSubmit={this.searchTicketFileName}
                    >
                      {({
                        values,
                        errors,
                        touched,
                        isValid,
                        isSubmitting,
                        setFieldValue,
                      }) => {
                        return (
                          <Form encType="multipart/form-data">
                            {this.state.showLoader === true ? (
                              <div className="loderOuter">
                                <div className="loader">
                                  <img src={loaderlogo} alt="logo" />
                                  <div className="loading">Loading...</div>
                                </div>
                              </div>
                            ) : (
                              ""
                            )}

                            <div className="doc_ser_box">
                              <div className="form-group checText">
                                <label>Ticket ID</label>
                                <Field
                                  name="ticket_id"
                                  type="text"
                                  className="form-control"
                                  autoComplete="off"
                                ></Field>
                                {errors.ticket_id && touched.ticket_id ? (
                                  <span className="errorMsg">
                                    {errors.ticket_id}
                                  </span>
                                ) : null}
                              </div>

                              <Button
                                className={`btn-fill ${
                                  isValid ? "btn-custom-green" : "btn-disable"
                                } m-r-10`}
                                type="submit"
                                disabled={isValid ? false : true}
                              >
                                {this.state.stopflagId > 0
                                  ? isSubmitting
                                    ? "Updating..."
                                    : "Update"
                                  : isSubmitting
                                  ? "Submitting..."
                                  : "Submit"}
                              </Button>
                              <div className="clearFix"></div>
                            </div>
                          </Form>
                        );
                      }}
                    </Formik>

                    {this.state.isTicketFileSearch === true ? (
                      this.state.ticketfilesearch.length > 0 ? (
                        this.state.ticketfilesearch.map((file, i) => (
                          <p key={i}>
                            <a href={file.filelink} target="_blank">
                              {file.filename}
                            </a>
                          </p>
                        ))
                      ) : (
                        <p>No file was found</p>
                      )
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              </div>
            </section>
          </div>
        </>
      );
    }
  }
}

export default DocumentSearch;
