import React, { Component } from 'react';
import API from "../../shared/axios";

class LeadTime extends Component {

    state = {
        files: "",
    };

    maxSelectFile=(event)=>{
        let files = event.target.files // create file object
        if (files.length > 3) { 
            const msg = 'Only 3 images can be uploaded at a time'
            event.target.value = null // discard selected file
            console.log(msg)
            return false;
        }
        return true;     
    }

    checkFileSize=(event)=>{
        let files = event.target.files
        let size = 15000000 
        let err = ""; 
        for(var x = 0; x < files.length; x++) {
            if (files[x].size > size) {
                err += files[x].type+' is too large( > 15 MB), please pick a smaller file\n';
            }
        };
        if (err !== '') {
            event.target.value = null
            console.log(err)
            return false
        }
        return true;   
   }

    onChangeHandler=event=>{

        if(this.maxSelectFile(event) && this.checkMimeType(event) && this.checkFileSize(event)){

           this.setState({
                files: event.target.files
           })
        }
    }
    
    onSubmitHandler = e => {
        e.preventDefault();
    
        let formData = new FormData();
    
        for (const file of this.state.files) {
            formData.append('file', file)
        }

       // console.log(this.state.files);
    
        API
          .post("/api/feed/upload_file", formData)
          .then(res => {
              console.log(res)
          })
          .catch(err => {
            console.log(err);
          });
    }

    checkMimeType=(event)=>{

        let files = event.target.files 
        let err = ''
        for(var x = 0; x < files.length; x++) {
            console.log(files[x].type);

            var allow_extension = ["image/png", "image/jpeg", "image/gif"];
            if(allow_extension.indexOf(files[x].type.toLowerCase()) === -1) {
                err += files[x].type+' is not a supported format\n';
            }
        };
      
        if (err !== '') {
            event.target.value = null
            console.log(err)
            return false; 
        }
        return true;      
    }
    

    render(){
        return(
            <>
            <div className="content-wrapper">
                <section className="content-header">
                    <h1>
                        Lead Time
                    </h1>

                    {/* satyajit */}
                    <form className="form-product" encType="multipart/form-data" onSubmit={this.onSubmitHandler} >

                        <div className="form-row">
                           
                            <div className="custom-file col-md-12">
                                <input type="file" multiple className="custom-file-input" id="customFile" name="files" onChange={this.onChangeHandler} />
                            </div>
                        </div>

                        <div className="col-md-12 text-center">
                            <button type="submit" className="btn btn-primary new-product-button">
                                Upload
                            </button>
                        </div>

                    </form>
                    {/* satyajit */}

                </section>
                <section className="content"></section>
            </div>
            </>
        );
    }
}


export default LeadTime;