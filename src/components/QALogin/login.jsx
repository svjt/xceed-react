import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import axios from "../../shared/axios";

import LoadingSpinner from "../loader/loadingSpinner";
import drreddylogo from "../../assets/images/drreddylogo_white.png";
import exceedlogo from "../../assets/images/Xceed_Logo_Purple_RGB.png";

const initialValues = {
  username: "",
  password: "",
};

var message = "";

class Login extends Component {
  constructor(props) {
    super(props);

    message = this.props.location.state
      ? this.props.location.state.message
      : "";
  }

  state = {
    errMsg: null,
    deniedPermissionCookies: false,
    errStatusCode: "",
    website_lang: "",
  };

  componentDidMount() {
    if (localStorage.qa_token !== null && typeof localStorage.qa_token != 'undefined' ){
      this.props.history.push("/qa_form");
    }

    if (message !== null) {
      this.props.history.push({
        pathname: "",
        state: "",
      });
      this.setState({ message : message})
    }
    document.title = `QA Login | Dr.Reddys API`;

    
  }

  handleSubmit = (values, actions) => {
    this.setState({ errMsg: "", errStatusCode: "" });

    axios
    .post("/api/tasks/qa_login", {"username": values.username,"password": values.password})
    .then(res => {
        localStorage.setItem('qa_token',res.data.token);
        this.props.history.push("/qa_form");
    })
    .catch(err => {
      actions.setErrors({
        password: err.data.errors.password
      });
      //actions.setErrors(error.data.errors);
      actions.setSubmitting(false);
    });

  };

  hideSuccessMsg = () => {
    message = "";
  };

  removeError = (setErrors) => {
    this.setState({ errMsg: "" });
  };

  render() {

    const loginvalidation = Yup.object().shape({
      username: Yup.string()
        .required(`Please enter email address`)
        .email(
          `Please enter a valid email address`
        ),
      password: Yup.string().required(
        `Please enter password`
      ),
    });

    const { loading } = this.state;

    return (
      <>
        <div className="login-box">
          {loading ? (
            <LoadingSpinner />
          ) : (
            <Formik
              initialValues={initialValues}
              validationSchema={loginvalidation}
              onSubmit={this.handleSubmit}
            >
              {({
                values,
                errors,
                touched,
                handleChange,
                isValid,
                handleBlur,
                setFieldValue,
              }) => {
                //console.log( errors );
                return (
                  <Form>
                    <div className="login-logo">
                      <Link to="/" className="logo">
                        <span className="logo-mini">
                          <img src={drreddylogo} alt="Dr.Reddy's" />
                        </span>
                      </Link>
                      <br />
                      <span className="logo-mini">
                        <img src={exceedlogo} alt="Dr.Reddy's" height="36px" />
                      </span>
                    </div>

                    <div className="login-box-body">
                      {/* invalid token format */}
                      {/* {this.props.auth.data.message && 
										<p><label>{this.props.auth.data.message}</label></p>} */}

                      {this.state.message && <h5 style={{color: '#1bb200',padding: '5px 10px',textAlign : "center"}}>{this.state.message}</h5>}
                      <h2>{errors.process}</h2>

                      <p className="login-box-msg">QA Login</p>
                      <label>Email:</label>
                      <div className="form-group has-feedback m-b-25">
                        
                        <Field
                          name="username"
                          type="text"
                          className="form-control"
                          autoComplete="off"
                          placeholder="Please enter Email"
                          onChange={handleChange}
                          autoFocus={this.state.enableFocus}
                        />

                        {errors.username && touched.username && (
                          <label>{errors.username}</label>
                        )}

                        {/* <span className="glyphicon glyphicon-user form-control-feedback"></span> */}
                        {/* <span class="help-block with-errors">Please correct the error</span> */}
                      </div>
                      <label>Password:</label>
                      <div className="form-group has-feedback">
                        
                        <Field
                          name="password"
                          type="password"
                          className="form-control"
                          autoComplete="off"
                          placeholder="Please enter Password"
                          onChange={handleChange}
                          autoFocus={this.state.enableFocus}
                        />
                        {errors.password && touched.password && (
                          <label>{errors.password}</label>
                        )}
                        {/* <span className="glyphicon glyphicon-lock form-control-feedback"></span> */}
                        {/* <span class="help-block with-errors">Please correct the error</span> */}
                      </div>
                      <div className="row">
                        <div className="col-xs-8">
                          <Link to={'/qa_forgot_password'}>Forgot password?</Link>
                        </div>
                        <div className="col-xs-4">
                          <button
                            className="btn-fill"
                            type="submit"
                          >
                            Login
                          </button>
                        </div>
                      </div>
                    </div>
                  </Form>
                );
              }}
            </Formik>
          )}{" "}
          {/* end of loader */}
        </div>
      </>
    );  
  }
}


export default Login;
