import React, { Component } from "react";
import { Row, Col, Tooltip, Modal } from "react-bootstrap";
import Pagination from "react-js-pagination";
import { Link } from "react-router-dom";
import dateFormat from 'dateformat';
import swal from "sweetalert";
import whitelogo from '../../assets/images/drreddylogo_white.png';
import API from "../../shared/axios_qa";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import { isMobile } from "react-device-detect";

const validatePaaword = Yup.object().shape({
  password: Yup.string().trim()
    .required("Please enter your new password")
    .min(8, "Password must be at least 8 characters long"),
  confirm_password: Yup.string().trim()
    .required("Please enter your confirm password")
    .test("match", "Confirm password do not match", function (
      confirm_password
    ) {
      return confirm_password === this.parent.password;
    })
});

class Tasks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      TaskList: [],
      count: 0,
      itemPerPage: 20,
      activePage: 1,
      showModal: false,
      search_sales_order_number: '',
      search_invoice_number: '',
      remove_search: false,
    };
  }

  componentDidMount() {
    document.title = `XCEED TASK LIST | Dr.Reddys API`;
    this.getCOATaskList();
  };

  getCOATaskList(page = 1) {
    const { search_invoice_number, search_sales_order_number } = this.state;
    API.get(`/api/employees/qa/coa_task_list?page=${page}&invoice_number=${encodeURIComponent(
          search_invoice_number
      )}&sales_order_number=${encodeURIComponent(
          search_sales_order_number
      )}`)
      .then(async (res) => {
        this.setState({
          TaskList: res.data.data,
          count: Number(res.data.count),
          isLoading: false,
        });
      })
      .catch((err) => {
        this.setState({
          isLoading: false,
        });
      });
  };

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getCOATaskList(pageNumber > 0 ? pageNumber : 1);
  };

  getSubmittedBy = (request) => {
    if (request.submitted_by > 0) {
      return `${request.agent_fname} ${request.agent_lname}`;
    } else if (request.ccp_posted_by > 0) {
      let posted_from = "";
      return `${request.emp_posted_by_fname} ${request.emp_posted_by_lname} ${posted_from}`;
    } else {
      return `${request.first_name} ${request.last_name}`;
    }
  };

  openMobileView = (onBehalf) => {
    this.setState({
      showMobileAlert: true,
      displayMobileText: `This request is raised on behalf of ${onBehalf} by Dr.Reddy’s`,
    });
  };

  isListingHover = (targetName) => {
    return this.state[targetName] ? this.state[targetName].hoverTip : false;
  };

  toggleListingTip = (targetName) => {
    if (!this.state[targetName]) {
      this.setState({
        [targetName]: {
          hoverTip: true,
        },
      });
    } else {
      this.setState({
        ...this.state,
        [targetName]: {
          hoverTip: !this.state[targetName].hoverTip,
        },
      });
    }
  };

  modalShowHandler = () => {
    this.setState({ showModal: true });
  };
  modalCloseHandler = () => {
    this.setState({ showModal: false });
  };

  handleSubmitEvent = (values, actions) => {
    const post_data = {
      password: values.password
    };

    API.put(`/api/employees/qa/changepassword`, post_data)
      .then(res => {
        this.modalCloseHandler();
        console.log("response", res);
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: "Password updated successfully.",
          icon: "success"
        }).then(() => {
          window.location.reload();
        });
      })
      .catch(err => {
        actions.setErrors(err.data.errors);
        actions.setSubmitting(false);
      });
  };

  handleSearch = (e) => {
    e.preventDefault();
    const {
        search_invoice_number,
        search_sales_order_number,
    } = this.state;

    if (
        search_invoice_number === '' &&
        search_sales_order_number === ''
    ) {
        return false;
    }

    API
        .get(
            `/api/employees/qa/coa_task_list?page=1&invoice_number=${encodeURIComponent(
                search_invoice_number
            )}&sales_order_number=${encodeURIComponent(
                search_sales_order_number
            )}`
        )
        .then((res) => {
            this.setState({
                TaskList: res.data.data,
                count: Number(res.data.count),
                remove_search: true,
                activePage: 1,
            });
        })
        .catch((err) => {
            // showErrorMessage(err, this.props);
            console.log("err",err);
        });
  };

  clearSearch = () => {
      this.setState(
          {
              search_invoice_number: '',
              search_sales_order_number: '',
              remove_search: false,
          },
          () => {
              this.getCOATaskList();
              this.setState({ activePage: 1 });
          }
      );
  };

  render() {

    const { search_invoice_number, search_sales_order_number } = this.state;
    
    if (this.state.isLoading) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy" >
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <>
          <div className="content-wrapper qaWrapper logisticsOrder">
            <section className="content-header">
              <div className="row">
                <div className="col-lg-12 col-sm-6 col-xs-12">
                  <h1>XCEED TASK LIST</h1>
                  <div className="customer-password-button btn-fill bdrRadius" onClick={e => this.modalShowHandler()}>
                    Change Password
                  </div>
                  <div className="customer-edit-button btn-fill bdrRadius" onClick={() => { localStorage.clear(); this.props.history.push("/qa_login"); }}
                  >
                    Log Out
                  </div>
                </div>
              </div>
            </section>
            <section className="content">
              <div className='row'>
                  <div className='col-lg-12 col-sm-12 col-xs-12 topSearchSection'>
                      <form className='form'>
                          <div className='clearfix serchapanel mb-0'>
                              <ul>
                                  <li>
                                      <div className=''>
                                          <input
                                              className='form-control'
                                              name='invoice_number'
                                              id='invoice_number'
                                              placeholder='Search Invoice Number'
                                              value={search_invoice_number}
                                              onChange={(e) =>
                                                  this.setState({ search_invoice_number: e.target.value })
                                              }
                                          />
                                      </div>
                                  </li>
                                  <li>
                                      <div className=''>
                                          <input
                                              className='form-control'
                                              name='sales_order_number'
                                              id='sales_order_number'
                                              placeholder='Search Sales Order Number'
                                              value={search_sales_order_number}
                                              onChange={(e) =>
                                                  this.setState({
                                                      search_sales_order_number: e.target.value,
                                                  })
                                              }
                                          />
                                      </div>
                                  </li>
                                  <li>
                                      <div className=''>
                                          <input
                                              type='submit'
                                              value='Search'
                                              className='btn btn-warning btn-sm orderListSearchBar'
                                              onClick={(e) => this.handleSearch(e)}
                                          />
                                          {this.state.remove_search ? (
                                              <a
                                                  onClick={() => this.clearSearch()}
                                                  className='btn btn-danger btn-sm orderListRemoveBtn'
                                              >
                                                  {' '}
                                                  Remove{' '}
                                              </a>
                                          ) : null}
                                      </div>
                                  </li>
                              </ul>
                          </div>
                      </form>
                  </div>
              </div>
              <div className="serchapanel">
                <div className="box-body">
                  <div className="listing-table table-responsive">
                    <table className="table table-hover table-bordered" style={{width:'2000px', maxWidth: 'inherit'}}>
                      <thead>
                        <tr>
                          <th width="120">Order No.</th>
                          <th width="150">Purchase Order (Customer PO number)</th>
                          <th width="150">Sales Order #</th>
                          <th width="150">Material Name</th>
                          <th width="150">Invoice #</th>
                          <th width="150">Invoice PDF</th>
                          <th width="150">Packing List PDF</th>
                          <th width="150">AWB #</th>
                          <th width="150">COA</th>
                          <th width="150">Payment Status</th>
                          <th width="150">Submitted By</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.state.TaskList.map((order, key) => (
                          <tr key={key}>
                            <td>
                              <Link
                                to={{
                                  pathname: `/qa_task_details/${order.task_id}`,
                                }}
                                style={{ cursor: "pointer" }}
                              >
                                {order.task_ref}
                              </Link>
                            </td>
                            <td>
                              <p>{order.po_number != null && order.po_number != '' ? order.po_number : '-'}</p>
                            </td>
                            <td>
                              <p>{order.sales_order_no != null && order.sales_order_no != '' ? order.sales_order_no : '-'}</p>
                              {order.sales_order_no != null && order.sales_order_no != '' ? `(${order.sales_order_date})` : ""}
                            </td>
                            <td>{order.product_name}</td>
                            <td>{order.invoice_number}</td>
                            <td>{order.invoice_number != null && order.invoice_number != '' ? order.invoice_file_name : ''}</td>
                            <td>{order.invoice_number != null && order.invoice_number != '' ? order.packing_file_name : ''}</td>
                            <td>{order.awb != null && order.awb != '' ? `${order.awb}${`\n`}( ${dateFormat(order.awb_date, "d mmm yyyy")} )` : "-"}</td>
                            <td>
                              {order.coa_file.length > 0 && (
                                order.coa_file
                                  .map((file, p) => {
                                    return (
                                      <p key={p}>
                                        <a href={file.url} target={"_blank"}>{file.actual_file_name}</a>
                                      </p>
                                    );
                                  })
                              )}
                            </td>
                            <td>{order.payment_status}</td>

                            <td>
                              {isMobile ? (
                                <span
                                  id={`tip-${order.task_id}`}
                                  onClick={() =>
                                    (order.submitted_by > 0 ||
                                      order.ccp_posted_by > 0) &&
                                    this.openMobileView(
                                      `${order.first_name} ${order.last_name}`
                                    )
                                  }
                                >
                                  {this.getSubmittedBy(order)}
                                </span>
                              ) : (
                                <>
                                  <span id={`tip-${order.task_id}`}>
                                    {this.getSubmittedBy(order)}
                                  </span>

                                  {(order.submitted_by > 0 ||
                                    order.ccp_posted_by > 0) && (
                                      <Tooltip
                                        placement="right"
                                        isOpen={this.isListingHover(
                                          `tip-${order.task_id}`
                                        )}
                                        autohide={false}
                                        target={`tip-${order.task_id}`}
                                        toggle={() =>
                                          this.toggleListingTip(
                                            `tip-${order.task_id}`
                                          )
                                        }
                                        className="listingTip"
                                      >
                                        This request is raised on behalf of {`${order.first_name} ${order.last_name}`} by {this.getSubmittedBy(order)}
                                      </Tooltip>
                                    )}
                                </>
                              )}
                            </td>

                          </tr>
                        ))}
                        {this.state.TaskList.length === 0 && (
                          <tr>
                            <td colSpan="11" align="center">
                              No Data to display
                            </td>
                          </tr>
                        )}
                      </tbody>
                    </table>
                  </div>

                  {this.state.count > this.state.itemPerPage ? (
                    <Row>
                      <Col md={12}>
                        <div className="paginationOuter text-right">
                          <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemPerPage}
                            totalItemsCount={this.state.count}
                            itemClass="nav-item"
                            linkClass="nav-link"
                            activeClass="active"
                            onChange={this.handlePageChange}
                          />
                        </div>
                      </Col>
                    </Row>
                  ) : null}
                </div>
              </div>
            </section>
          </div>

          <Modal show={this.state.showModal} onHide={() => this.modalCloseHandler()} backdrop="static" dialogClassName="profile-popup">
            <Modal.Header closeButton>
              <Modal.Title>Change Password</Modal.Title>
            </Modal.Header>

            <Modal.Body>
              <div className="contBox">
                <Row>
                  <Col xs={12} sm={12} md={12}>
                    <div className="profile">
                      <Formik
                        validationSchema={validatePaaword}
                        onSubmit={this.handleSubmitEvent}
                      >
                        {({ values, errors, touched, isValid, isSubmitting }) => {
                          return (
                            <Form>

                              <Row>
                                <Col xs={12} sm={12} md={12}>
                                  <div className="form-group">
                                    <label className="pull-left" style={{ float: "left" }}>Password</label>
                                    <Field
                                      name="password"
                                      type="password"
                                      className={`form-control`}
                                      placeholder="Enter password"
                                      autoComplete="off"
                                    />
                                    {errors.password && touched.password ? (
                                      <span className="errorMsg">
                                        {errors.password}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              </Row>
                              <Row>
                                <Col xs={12} sm={12} md={12}>
                                  <div className="form-group">
                                    <label className="pull-left">Confirm Password</label>
                                    <Field
                                      name="confirm_password"
                                      type="password"
                                      className={`form-control`}
                                      placeholder="Enter confirm password"
                                      autoComplete="off"
                                    />
                                    {errors.confirm_password &&
                                      touched.confirm_password ? (
                                      <span className="errorMsg">
                                        {errors.confirm_password}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              </Row>
                              <div className="button-footer btn-toolbar">
                                <button
                                  type="button"
                                  className="btn btn-success btn-sm"
                                  onClick={() => this.modalCloseHandler()}
                                >
                                  Cancel
                                </button>
                                <button
                                  className={`btn btn-danger btn-sm ${isValid ? "btn-custom-green" : "btn-disable"
                                    } m-r-10`}
                                  type="submit"
                                  disabled={isValid ? false : true}
                                >
                                  {isSubmitting ? "Updating..." : "Submit"}
                                </button>
                              </div>

                            </Form>
                          );
                        }}
                      </Formik>
                    </div>
                  </Col>
                </Row>
              </div>
            </Modal.Body>
          </Modal>
        </>
      )
    }


  }
}

export default Tasks;