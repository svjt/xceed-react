import React, { Component } from "react";
import whitelogo from '../../assets/images/drreddylogo_white.png';
import { withRouter } from 'react-router-dom';
import Pagination from "react-js-pagination";
import swal from "sweetalert";
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import dateFormat from "dateformat";

import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal
} from "react-bootstrap";

import { Link } from "react-router-dom";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import { htmlDecode, trimString } from "../../shared/helper";
import API from "../../shared/axios";
import axios from "../../shared/axios_qa";
import Select from "react-select";
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
import { showErrorMessage } from "../../shared/handle_error_qa";
import { Next } from "react-bootstrap/lib/Pagination";
import Autosuggest from "react-autosuggest";
import Dropzone from "react-dropzone";

var path = require("path");
const s3bucket_task_coa_path = `${process.env.REACT_APP_API_URL}/api/feed/download_typical_coa/`;

const validatePaaword = Yup.object().shape({
  password: Yup.string().trim()
    .required("Please enter your new password")
    .min(8, "Password must be at least 8 characters long"),
  confirm_password: Yup.string().trim()
    .required("Please enter your confirm password")
    .test("match", "Confirm password do not match", function (
      confirm_password
    ) {
      return confirm_password === this.parent.password;
    })
});

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
const removeDropZoneFiles = (fileName, objRef, setErrors, setFieldValue) => {
  var newArr = [];
  for (let index = 0; index < objRef.state.files.length; index++) {
    const element = objRef.state.files[index];

    if (fileName === element.name) {
    } else {
      newArr.push(element);
    }
  }

  var fileListHtml = newArr.map((file) => (
    <>
      <span onClick={() => removeDropZoneFiles(file.name, objRef, setErrors, setFieldValue)}>
        <i className="far fa-times-circle"></i>
      </span>{" "}
      {file.name}
    </>
  ));
  setErrors({ file_name: "" });
  setFieldValue("file_name", newArr);
  objRef.setState({
    isValidFile: true,
    files: newArr,
    filesHtml: fileListHtml,
  });
};
const actionFormatter = refObj => cell => {
  //console.log("this is ref obj ++++++++++++++++",refObj.state.typicalCoa);
  const current_coa = refObj.state.typicalCoa.filter((data) => cell == data.id);
  return (

    <div className="actionStyle">
      <LinkWithTooltip
        tooltip="Click to Edit"
        href="#"
        clicked={e => refObj.modalShowHandler(e, cell)}
        id="tooltip-1"
      >
        <i className="far fa-edit" />
      </LinkWithTooltip>
      {" "}
      <LinkWithTooltip
        tooltip="Click to Delete"
        href="#"
        clicked={e => refObj.confirmDelete(e, cell)}
        id="tooltip-1"
      >
        <i className="far fa-trash-alt" />
      </LinkWithTooltip>
      {" "}
      {current_coa[0].file_name != '' ?
        <LinkWithTooltip
          tooltip="Click to Download"
          href="#"
          clicked={(e) =>
            refObj.redirectUrlTask(
              e,
              `${s3bucket_task_coa_path}${cell}`
            )
          }
          id="tooltip-1"
        >
          <i className="fas fa-download" />
        </LinkWithTooltip>
        : null}
    </div>
  );
};

const custContent = () => cell => {
  //console.log(cell);
  return (<div data-toggle="tooltip" data-placement="top" title={htmlDecode(cell)}>{htmlDecode(cell)}</div>);
};
const custArr = () => cell => {
  const listItems = cell.map((d) => <li key={d} >{d}</li>);
  return (<div data-toggle="tooltip" data-placement="top" title={cell} className = "marketBullet">{listItems}</div>);
}
const custStatus = () => cell => {
  // this.setState({
  //   activation_status : cell
  // })
  if (cell === 1) {
    return (<div className="text-success">Active</div>)
  } else {
    return (<div className="text-danger">Inactive</div>)
  }


};
const initialValues = {
  product_code: '',
  country_code: '',
  specification_number: '',
  pharmacopeial_code: '',
  file_name: [],
  //date_expiry: ''
};

class ManageTypicalCoa extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showLoader: true,
      product_code: '',
      country: [],
      plant: [],
      qa_login: [],
      pharmacopeial: '',
      pharmacopeial_id: '',
      country_code: '',
      defaultValue_pharmacopeial: '',
      defaultValue_country: '',
      defaultValue_specification: '',
      defaultValue_product: '',
      product_code_auto_suggest: '',
      product_code_auto_suggest_popup: '',
      specification: '',
      typicalCoa: '',
      search_country_code: '',
      search_product_code: '',
      search_specification: '',
      search_pharmacopeial: '',
      search_status: '',
      count: '',
      remove_search: false,
      isListView: false,
      itemPerPage: 10,
      activePage: 1,
      showModalLoader: false,
      showModal: false,
      showModel_password: false,
      typicalCoaDetails: [],
      typicalCoaflagId: 0,
      selectStatus: [
        { value: "1", label: "Active" },
        { value: "0", label: "Inactive" }
        
      ],
      filesHtml: "",
      prevFilesHtml: "",
      files: [],
      rejectedFile: [],
      file_name: [],
      isValidFile: false,
      fileErrText: "",
      message: null,
      errMsg: null,
      startDate: new Date(),
      defaultValue_status: '',
      statuslist: [
        { value: "1", label: "Active" },
        { value: "0", label: "Inactive" }
        
      ]

    }
  }
  deleteFile = (e, setFieldValue) => {
    this.inputRef.current.value = null;
    e.preventDefault();
    var tempArray = [];

    this.state.files.map((file, fileIndex) => {
      if (file.checked === true) {
      } else {
        tempArray.push(file);
      }
    });

    setFieldValue("file_name", tempArray);

    this.setState({ files: tempArray });
  };
  componentDidMount() {
    axios.get(`/api/feed/get_sap_countries_qa`)
      .then((res) => {
        this.setState({ country: res.data.data });
      })
      .catch((err) => {
      });
    axios.get(`/api/feed/get_sap_pharmacopeial_qa`)
      .then((res) => {
        this.setState({ pharmacopeial: res.data.data });
      })
      .catch((err) => {
      });
    axios.get(`/api/feed/get_sap_plant_code_qa`)
      .then((res) => {
        this.setState({ plant: res.data.data });
      })
      .catch((err) => {
      });
    axios.get(`/api/feed/get_qa_logins`)
      .then((res) => {
        this.setState({ qa_login: res.data.data });
      })
      .catch((err) => {
      });
    this.getCOAList();

  }

  filterSearchPharmacopeial = (e) => {
    if (e != null) {
      this.setState({
        showLoader: false,
        defaultValue_pharmacopeial: e

      });
    } else {
      this.setState({
        showLoader: false,
        defaultValue_pharmacopeial: ''

      });
    }
  }
  filterSearchstatus = (e) => {
    //console.log("this is e ::",e);

    if (e != null) {
      this.setState({
        showLoader: false,
        defaultValue_status: e

      });
    } else {
      this.setState({
        showLoader: false,
        defaultValue_status: ''

      });
    }
  }
  filterSearchCountry = (e) => {
    // console.log("this is e",e);
    if (e != null) {
      this.setState({
        showLoader: false,
        defaultValue_country: e

      });
    } else {
      this.setState({
        showLoader: false,
        defaultValue_country: ''

      });
    }

  }
  setDropZoneFiles = (acceptedFiles, setErrors, setFieldValue, setFieldTouched) => {
    var rejectedFiles = [];
    var uploadFile = [];

    setErrors({ file_name: false });
    setFieldValue(this.state.files);

    // var prevFiles = this.state.files;
    var newFiles = acceptedFiles;

    // uncomment below code if multi file option is required
    // if (prevFiles.length > 0) {
    //     for (let index = 0; index < acceptedFiles.length; index++) {
    //         var remove = 0;

    //         for (let index2 = 0; index2 < prevFiles.length; index2++) {
    //             if (acceptedFiles[index].name === prevFiles[index2].name) {
    //                 remove = 1;
    //                 break;
    //             }
    //         }
    //         if (remove === 0) {
    //             prevFiles.push(acceptedFiles[index]);
    //         }
    //     }
    //     newFiles = prevFiles;
    // } else {
    //     newFiles = acceptedFiles;
    // }

    const totalfile = newFiles.length;
    for (let index = 0; index < totalfile; index++) {
      var error = 0;
      var filename = newFiles[index].name.toLowerCase();
      var extension_list = ['pdf'];
      var ext_with_dot = path.extname(filename);
      var file_extension = ext_with_dot.split('.').join('');

      var obj = {};

      var fileErrText = `Only files with the following extensions are allowed: pdf.`;

      if (extension_list.indexOf(file_extension) === -1) {
        error = error + 1;

        // let str_1 = `One or more files could not be uploaded.The specified file [file_name] could not be uploaded.`;
        // let str_2 = `The specified file [file_name] could not be uploaded.`;
        let str_1 = '';
        let str_2 = '';
        if (totalfile > 1) {
          if (index === 0) {
            obj['errorText'] = str_1.replace('[file_name]', filename) + fileErrText;
          } else {
            obj['errorText'] = str_2.replace('[file_name]', filename) + fileErrText;
          }
        } else {
          obj['errorText'] = str_2.replace('[file_name]', filename) + fileErrText;
        }
        rejectedFiles.push(obj);
      }

      if (newFiles[index].size > 50000000) {
        let err_txt = `The file [file_name] could not be saved because it exceeds 50 MB, the maximum allowed size for uploads.`;
        obj['errorText'] = err_txt.replace('[file_name]', filename);
        error = error + 1;
        rejectedFiles.push(obj);
      }

      if (error === 0) {
        uploadFile.push(newFiles[index]);
      } else {
        // setErrors({file_name: obj.errorText}); // This wont work as re render will clear it
        this.setState({ isValidFile: false, fileErrText: obj.errorText });
      }
    }

    if (rejectedFiles.length === 0) {
      setErrors({ file_name: false });
      this.setState({ isValidFile: true, fileErrText: '' });
    }

    setFieldValue('file_name', newFiles);
    setFieldTouched('file_name');

    this.setState({
      rejectedFile: rejectedFiles,
    });

    var fileListHtml = newFiles.map((file) => (
      <>
        <span
          onClick={() => {
            removeDropZoneFiles(file.name, this, setErrors, setFieldValue);
          }}
        >
          <i className='far fa-times-circle'></i>
        </span>{' '}
        {trimString(25, file.name)}
      </>
    ));

    this.setState({
      files: uploadFile,
      filesHtml: fileListHtml,
      prevFilesHtml: ''
    });
  };
  clearSearch = () => {
    document.getElementById('country').value = ''
    document.getElementById('pharmacopeial').value = ''
    document.getElementById('status').value = ''
    document.getElementById('specification').value = ''
    document.getElementById('product_code').value = ''
    this.setState({
      search_pharmacopeial: '',
      search_status: '',
      search_country_code: '',
      search_product_code: '',
      product_code: '',
      search_specification: '',
      remove_search: false
    }, () => {
      this.setState({ activePage: 1 })
      this.getCOAList()
    })

  }
  getCOAList(page = 1) {
    var country = this.state.search_country_code
    var pharmacopeial = this.state.search_pharmacopeial
    var status = this.state.search_status
    var specification = this.state.search_specification
    var product_code = this.state.search_product_code
    axios.get(`/api/feed/typical_coa_list?page=${page}&country_code=${encodeURIComponent(country)}&specification=${encodeURIComponent(specification)}&pharmacopeial_id=${encodeURIComponent(pharmacopeial)}&product_code=${encodeURIComponent(product_code)}&status=${encodeURIComponent(status)}`)
      .then(res => {
        this.setState({
          typicalCoa: res.data.data,
          count: res.data.count_typicalCoa,
          showLoader: false,
          search_pharmacopeial: pharmacopeial,
          search_country_code: country,
          search_status: status,
          search_product_code: product_code,
          search_specification: specification,
          remove_search: false
        });
      })
      .catch(err => {
        this.setState({
          showLoader: false
        });
        showErrorMessage(err, this.props);
      });
    this.setState({
      showLoader: false
    });
  }
  coaSearch = (e, page = 1) => {
    e.preventDefault();
    //   console.log("++++++",document.getElementById('product_code'));
    var country_code = document.getElementById('country').value
    var pharmacopeial_code = document.getElementById('pharmacopeial').value
    var status = document.getElementById('status').value;
    var specification = document.getElementById('specification').value
    var product_code = document.getElementById('product_code').value
    
    if (country_code === "" && pharmacopeial_code === "" && specification === "" && product_code === "" && status === "") {
      return false;
    }


    axios.get(`/api/feed/typical_coa_list?page=${page}&country_code=${encodeURIComponent(country_code)}&specification=${encodeURIComponent(specification)}&pharmacopeial_id=${encodeURIComponent(pharmacopeial_code)}&product_code=${encodeURIComponent(product_code)}&status=${encodeURIComponent(status)}`)
      .then(res => {
        this.setState({
          typicalCoa: res.data.data,
          count: res.data.count_typicalCoa,
          showLoader: false,
          search_pharmacopeial: pharmacopeial_code,
          search_country_code: country_code,
          search_product_code: product_code,
          search_specification: specification,
          search_status: status,
          remove_search: true,
          isListView: true,
          activePage : 1
        });
      })
      .catch(err => {
        this.setState({
          showLoader: false
        });
        showErrorMessage(err, this.props);
      });
  }
  modalShowHandler = (event, id) => {
    if (id) {
      event.preventDefault();
      axios.post(`/api/feed/get_typical_coa/${id}`)
        .then(res => {
          this.setState({
            typicalCoaDetails: res.data.data,
            typicalCoaflagId: id,
            showLoader: false,
            showModal: true
          });
          if (res.data.data.actual_file_name) {
            let prevFilesHtml =
              <>
                <span
                  onClick={() => { this.setState({ prevFilesHtml: '' }) }}
                >
                  <i className='far fa-times-circle'></i>
                </span>{' '}
                {trimString(25, res.data.data.actual_file_name)}
              </>
            this.setState({
              prevFilesHtml: prevFilesHtml,
            });
          }

        })
        .catch(err => {
          showErrorMessage(err, this.props);
        });
    } else {
      this.setState({
        typicalCoaDetails: [],
        typicalCoaflagId: 0,
        showModal: true

      });
    }
  };
  modalShowHandler_pass = () => {
    this.setState({ showModel_password: true });
  }
  confirmDelete = (event, id) => {
    event.preventDefault();
    swal({
      closeOnClickOutside: false,
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this!",
      icon: "warning",
      buttons: true,
      dangerMode: true
    }).then(willDelete => {
      if (willDelete) {
        this.deleteTypicalCoa(id);
      }
    });
  };

  deleteTypicalCoa = id => {
    if (id) {
      axios.put(`/api/feed/delete_typical_coa/${id}`).then(res => {
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: "Record deleted successfully.",
          icon: "success"
        }).then(() => {
          this.setState({ activePage: 1 });
          this.getCOAList(this.state.activePage);
        });
      }).catch(err => {
        if (err.data.status === 3) {
          this.setState({ closeModal: true });
          showErrorMessage(err, this.props);
        }
      });
    }
  };


  handleSubmitEvent = (values, actions) => {
    // this.setState({ showLoader: true });
    var formData = new FormData();
    var country_arr = [];
    this.setState({ message: "" });

    this.setState({ errMsg: "" });


    //alert(JSON.stringify(values))
    if (this.state.files && this.state.files.length > 0) {
      for (let index = 0; index < this.state.files.length; index++) {
        const element = this.state.files[index];

        formData.append("file", element);
      }
    } else {
      formData.append("file", []);
    }
    for (let i in values.country_code) {
      //country_arr.push(values.country_code[i].value)
      formData.append("country_code[]", values.country_code[i].value)
    }
    for (let i in values.plant_code) {
      //country_arr.push(values.plant_code[i].value)
      formData.append("plant[]", values.plant_code[i].value)
    }
    for (let i in values.employee_id) {
      //country_arr.push(values.country_code[i].value)
      formData.append("qa_logins[]", values.employee_id[i].value)
    }
    //console.log("this is value: ",values.country_code);
    formData.append("product_code", values.product_code);
    // formData.append("country_code", JSON.stringify(country_arr));
    formData.append("pharmacopeial_code", values.pharmacopeial_code);
    formData.append("specification_number", values.specification);
    formData.append("status", values.status)
    //formData.append("date_expiry", dateFormat(values.date_expiry, "yyyy-mm-dd HH:MM:ss"))

    if (this.state.typicalCoaflagId) {
      this.setState({ showModalLoader: true });
      const id = this.state.typicalCoaflagId;
      axios.put(`/api/feed/update_typical_coa/${id}`, formData)
        .then(res => {
          //console.log("this is responce ::::::::::::: ",res);
          this.modalCloseHandler();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record updated successfully.",
            icon: "success"
          }).then(() => {
            this.setState({ showModalLoader: false });
            this.getCOAList(this.state.activePage);
          });
        })
        .catch(err => {
          this.setState({ showModalLoader: false });
          actions.setSubmitting(false);
          if (err.data.errors.data_coa != '' && err.data.errors.data_coa != undefined) {
            swal({
              closeOnClickOutside: false,
              title: "Error",
              text: "Data already exists",
              icon: "Error",
              className: "qa-form"
            }).then(() => {
              window.location.reload();
            });
          }

          actions.setErrors({
            product_code: err.data.errors.product_code,
            country_code: err.data.errors.country_code,
            plant_code: err.data.errors.plant_code,
            employee_id: err.data.errors.qa_logins,
            pharmacopeial_code: err.data.errors.pharmacopeial_code
          });
        });
    } else {
      this.setState({ showModalLoader: true });
      axios.post("/api/feed/add_typical_coa", formData)
        .then(res => {
          //console.log("this is responce ::::::::::::: ",res);
          this.modalCloseHandler();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record added successfully.",
            icon: "success"
          }).then(() => {
            this.setState({ activePage: 1, showModalLoader: false });
            this.getCOAList(this.state.activePage);
          });
        })
        .catch(err => {
          // console.log("hello : ",err.data);
          this.setState({ showModalLoader: false });
          actions.setSubmitting(false);
          //console.log("====huiiiiii==",err.data.errors);
          if (err.data.errors.data_coa != '' && err.data.errors.data_coa != undefined) {
            swal({
              closeOnClickOutside: false,
              title: "Error",
              text: "Data already exists",
              icon: "Error",
              className: "qa-form"
            }).then(() => {
              window.location.reload();
            });
          }

          actions.setErrors({
            product_code: err.data.errors.product_code,
            country_code: err.data.errors.country_code,
            pharmacopeial_code: err.data.errors.pharmacopeial_code
          });
        });
    }




  };

  handleChange = (date) => {
    this.setState({
      startDate: date
    })
  }
  handleSubmitEvent_pass = (values, actions) => {
    const post_data = {
      password: values.password
    };

    API.put(`/api/employees/qa/changepassword`, post_data)
      .then(res => {
        this.modalCloseHandler();
        //console.log("response", res);
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: "Password updated successfully.",
          icon: "success"
        }).then(() => {
          window.location.reload();
        });
      })
      .catch(err => {
        console.log("this is error : ", err.data.errors);
        actions.setErrors(err.data.errors);
        actions.setSubmitting(false);
      });
  };


  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getCOAList(pageNumber > 0 ? pageNumber : 1);
  };
  modalCloseHandler = () => {
    this.setState({ typicalCoaflagId: 0 });
    this.setState({ showModal: false, showModel_password: false });
    this.setState({ filesHtml: "", prevFilesHtml: "" })
  };
  redirectUrlTask = (event, path) => {
    event.preventDefault();
    window.open(path, "_blank");
  }

  setMaterialID = (event) => {
    this.setState({ product_code: event.target.value.toString() });


    if (event.target.value && event.target.value.length > 2) {
      axios.post(`/api/feed/get_sap_Product_code_qa`, { material_id: event.target.value })
        .then((res) => {

          let ret_html = res.data.data.map((val, index) => {
            return (<li style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => this.setMaterialIDAutoSuggest(val.value)} >{val.label}<hr style={{ marginTop: '6px', marginBottom: '0px' }} /></li>)
          })
          this.setState({ product_code_auto_suggest: ret_html });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  setMaterialIDAutoSuggest = (event) => {
    this.setState({ product_code_auto_suggest: '', product_code: event.toString() });
  };

  setMaterialIDPopup = (event, setFieldValue) => {
    setFieldValue("product_code", event.target.value.toString());


    if (event.target.value && event.target.value.length > 2) {
      axios.post(`/api/feed/get_sap_Product_code_qa`, { material_id: event.target.value })
        .then((res) => {

          let ret_html = res.data.data.map((val, index) => {
            return (<li style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => this.setMaterialIDAutoSuggestPopup(val.value, setFieldValue)} >{val.label}<hr style={{ marginTop: '6px', marginBottom: '0px' }} /></li>)
          })
          this.setState({ product_code_auto_suggest_popup: ret_html });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  setMaterialIDAutoSuggestPopup = (event, setFieldValue) => {
    setFieldValue("product_code", event.toString());
    this.setState({ product_code_auto_suggest_popup: '' });
  };

  setCountry = (event, setFieldValue) => {

    setFieldValue("country_code", event);
  }
  setPlant = (event, setFieldValue) => {

    setFieldValue("plant_code", event);
  }
  setQaLogin = (event, setFieldValue) => {

    setFieldValue("employee_id", event);
  }

  render() {
    const { typicalCoaDetails } = this.state;
    const newInitialValues = Object.assign(initialValues, {
      product_code: typicalCoaDetails.product_code ? htmlDecode(typicalCoaDetails.product_code) : "",
      country_code: typicalCoaDetails.country ? typicalCoaDetails.country : "",
      plant_code: typicalCoaDetails.plant ? typicalCoaDetails.plant : "",
      employee_id: typicalCoaDetails.qa_user ? typicalCoaDetails.qa_user : "",
      specification: typicalCoaDetails.specification_number ? htmlDecode(typicalCoaDetails.specification_number) : "",
      pharmacopeial_code: typicalCoaDetails.pharmacopeial_code ? htmlDecode(typicalCoaDetails.pharmacopeial_code) : "",
      status: typicalCoaDetails.status || +typicalCoaDetails.status === 0 ? typicalCoaDetails.status.toString() : "",
      // date_expiry: typicalCoaDetails.date_expiry ? new Date(typicalCoaDetails.date_expiry) : "",

    });
    //console.log("this is file length :::",this.state.files.length);
    const validateStopFlag = Yup.object().shape({
      product_code: Yup.string()
        .min(2, 'Product code must be at least 2 characters')
        .required("Please enter Product code"),
      country_code: Yup.array()
        .required("Please enter country code"),
      plant_code: Yup.array()
        .required("Please enter Plant code"),
      employee_id: Yup.array()
        .required("Please enter valid User"),
      specification: Yup.string().trim()
        .max(20, 'specification number must be at most 10 characters')
        .required("Please enter specification number"),
      pharmacopeial_code: Yup.string().trim()
        .required("Please enter pharmacopeial code"),
      file_name: Yup.string()
        .test('file_name', 'Packing doc is required. Only .pdf file is allowed', () =>  this.state.files.length > 0 || this.state.prevFilesHtml )
        .test('file_name', this.state.fileErrText, () => this.state.isValidFile)
        .test('filecount', 'Maximum 1 file is allowed', () => this.state.files.length <= 1),
      status: Yup.string().trim()
        .required("Please select status")
        .matches(/^[0|1]$/, "Invalid status selected"),
      // date_expiry: Yup.string().required()
    });


    if (this.state.showLoader) {
      return (
        <div className="loderOuter">
          <div className="loading_reddy_outer">
            <div className="loading_reddy" >
              <img src={whitelogo} alt="logo" />
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className="content-wrapper qaWrapper">
          <section className="content-header">
            <div className="row">
              <div className="col-lg-12 col-xs-12">
                <h1>Typical COA</h1>
                <div className="customer-password-button btn-fill bdrRadius" onClick={e => this.modalShowHandler_pass()}>
                  Change Password
                </div>
                <div className="customer-edit-button btn-fill bdrRadius" onClick={() => { localStorage.clear(); this.props.history.push("/qa_login"); }}
                >
                  Log Out
                </div>
              </div>
              <div className="col-lg-12 col-sm-12 col-xs-12 topSearchSection" style={{ "marginTop": "10px" }}>

                <form className="form">
                  <div className="clearfix serchapanel">
                    <ul>
                      <li>
                        <div className="">
                          <button
                            type="button"
                            className="btn btn-primary"
                            onClick={e => this.modalShowHandler(e, "")}
                            style={{ "marginTop": "22px", "height": "40px" }}
                          >
                            <i className="fas fa-plus m-r-5" /> Add Typical COA
                          </button>
                        </div>
                      </li>
                      <li>
                        <div
                          className="form-group"
                          onMouseLeave={(e) => {
                            if (this.state.product_code_auto_suggest != '') {
                              document.getElementById(`product_code`).blur();
                              this.setState({ product_code_auto_suggest: '' });
                            }
                          }}
                        >
                          <label>Product</label>
                          <input
                            type="text"
                            name="product_code"
                            id="product_code"
                            className="form-control"
                            style={{ textTransform: "uppercase" }}

                            value={
                              this.state.product_code !== null &&
                                this.state.product_code !== ""
                                ? this.state.product_code
                                : ""
                            }

                            onChange={(e) => {
                              this.setMaterialID(e);
                            }}

                            onFocus={(e) => {
                              if (this.state.product_code != '' && this.state.product_code.length > 2) {
                                axios.post(`/api/feed/get_sap_Product_code_qa`, { material_id: this.state.product_code })
                                  .then((res) => {

                                    let ret_html = res.data.data.map((val, index) => {
                                      return (<li style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => this.setMaterialIDAutoSuggest(val.value)} >{val.label}<hr style={{ marginTop: '6px', marginBottom: '0px' }} /></li>)
                                    })
                                    this.setState({ product_code_auto_suggest: ret_html });
                                  })
                                  .catch((err) => {
                                    console.log(err);
                                  });
                              }
                            }}

                          />

                          {this.state.product_code_auto_suggest != '' && <ul id={`material_id_auto_suggect`} style={{ zIndex: '30', position: 'absolute', backgroundColor: '#d0d0d0', height: "100px", overflowY: 'auto', listStyleType: 'none', width: '100%' }} >
                            <li>{this.state.product_code_auto_suggest}</li>
                          </ul>}

                        </div>
                      </li>

                      {this.state.country && (
                        <li>
                          <div className="form-group  has-feedback">
                            <label>Market</label>
                            <select name="country" id="country" className="form-control">
                              <option value="">Select country</option>
                              {this.state.country.map(
                                (country, i) => (
                                  <option key={i} value={country.value}>
                                    {country.label}
                                  </option>
                                )
                              )}
                            </select>
                          </div>
                        </li>
                      )}
                      {this.state.pharmacopeial && (
                        <li>
                          <div className="form-group  has-feedback">
                            <label>Pharmacopeial</label>
                            <select name="pharmacopeial" id="pharmacopeial" className="form-control">
                              <option value="">Select pharmacopeial</option>
                              {this.state.pharmacopeial.map(
                                (pharmacopeial, i) => (
                                  <option key={i} value={pharmacopeial.value}>
                                    {pharmacopeial.label}
                                  </option>
                                )
                              )}
                            </select>
                          </div>
                        </li>
                      )}
                      <li>
                        <div className="form-group">
                          <label>Specification</label>
                          <input
                            type="text"
                            name="specification"
                            id="specification"
                            className="form-control"
                            style={{ textTransform: "uppercase" }}
                          //onChange={(e)=>this.searchSpecification(e)}

                          />
                        </div>
                      </li>
                      <li>
                        <div className="form-group  has-feedback">
                          <label>Status</label>
                          <select name="status" id="status" className="form-control">
                            <option value="">Select status</option>
                            {this.state.selectStatus.map(
                              (status, i) => (
                                <option key={i} value={status.value}>
                                  {status.label}
                                </option>
                              )
                            )}
                          </select>
                        </div>
                      </li>
                      <li>
                        <div class="searchBtnGroup">
                          <button type="button" className="btn btn-search" value="Search" onClick={(e) => this.coaSearch(e)}>Search</button>
                          {this.state.remove_search ? <button type="button" className="btn btn-remove" value="Search" onClick={() => this.clearSearch()}>Remove</button> : ''}
                        </div>
                      </li>

                    </ul>
                  </div>
                </form>

              </div>

            </div>
          </section>
          <section className="content">

            <div className="serchapanel">
              <div className="box-body">

                <BootstrapTable data={this.state.typicalCoa}>
                  <TableHeaderColumn isKey dataField="product" dataFormat={custContent(this)}>
                    Product
                  </TableHeaderColumn>
                  <TableHeaderColumn dataField="country" dataFormat={custArr(this)}>
                    Market
                  </TableHeaderColumn>
                  <TableHeaderColumn dataField="pharmacopeial" dataFormat={custContent(this)}>
                    Pharmacopeial
                  </TableHeaderColumn>
                  <TableHeaderColumn dataField="specification_number" dataFormat={custContent(this)}>
                    Specification
                  </TableHeaderColumn>
                  <TableHeaderColumn dataField="status" dataFormat={custStatus(this)}>
                    Status
                  </TableHeaderColumn>

                  {/* <TableHeaderColumn
                    dataField="date_expiry"
                    editable={false}
                    expandable={false}
                  >
                    Expiry Date
                  </TableHeaderColumn> */}

                  <TableHeaderColumn
                    dataField="id"
                    dataFormat={actionFormatter(this)}
                  >
                    Action
                  </TableHeaderColumn>

                </BootstrapTable>



                {/* ======= Add/Edit ======== */}
                <Modal
                  show={this.state.showModal}
                  onHide={() => this.modalCloseHandler()} backdrop="static"
                >
                  <Formik
                    initialValues={newInitialValues}
                    validationSchema={validateStopFlag}
                    onSubmit={this.handleSubmitEvent}
                  >
                    {({ values, errors, touched, isValid, isSubmitting, setErrors,
                      setFieldValue, setFieldTouched }) => {
                      return (
                        <Form>
                          {this.state.showModalLoader === true ? (
                            <div className="loading_reddy_outer">
                              <div className="loading_reddy" >
                                <img src={whitelogo} alt="loader" />
                              </div>
                            </div>
                          ) : ("")}
                          <Modal.Header closeButton>
                            <Modal.Title>
                              {this.state.typicalCoaflagId > 0 ? "Edit" : "Add"}{" "}
                              Typical COA
                            </Modal.Title>
                          </Modal.Header>
                          <Modal.Body>
                            <div className="contBox">
                              <Row>
                                <Col xs={12} sm={12} md={12}>
                                  <div
                                    className="form-group"
                                    onMouseLeave={(e) => {
                                      if (this.state.product_code_auto_suggest_popup != '') {
                                        document.getElementById(`product_code_popup`).blur();
                                        this.setState({ product_code_auto_suggest_popup: '' });
                                      }
                                    }}
                                  >
                                    <label>
                                      Product Code<span className="impField">*</span>
                                    </label>
                                    <Field
                                      name="product_code"
                                      id="product_code_popup"
                                      type="text"
                                      className={`form-control`}
                                      placeholder="Enter code"
                                      autoComplete="off"
                                      value={
                                        values.product_code !== null &&
                                          values.product_code !== ""
                                          ? values.product_code
                                          : ""
                                      }
                                      onChange={(e) => {
                                        this.setMaterialIDPopup(e, setFieldValue);
                                      }}
                                      onFocus={(e) => {
                                        if (values.product_code != '' && values.product_code.length > 2) {
                                          axios.post(`/api/feed/get_sap_Product_code_qa`, { material_id: values.product_code })
                                            .then((res) => {

                                              let ret_html = res.data.data.map((val, index) => {
                                                return (<li style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => this.setMaterialIDAutoSuggestPopup(val.value, setFieldValue)} >{val.label}<hr style={{ marginTop: '6px', marginBottom: '0px' }} /></li>)
                                              })
                                              this.setState({ product_code_auto_suggest_popup: ret_html });
                                            })
                                            .catch((err) => {
                                              console.log(err);
                                            });
                                        }
                                      }}

                                    />

                                    {this.state.product_code_auto_suggest_popup != '' && <ul id={`material_id_auto_suggect`} style={{ zIndex: '30', position: 'absolute', backgroundColor: '#d0d0d0', height: "100px", overflowY: 'auto', listStyleType: 'none', width: '100%' }} >
                                      {this.state.product_code_auto_suggest_popup}
                                    </ul>}

                                    {errors.product_code && touched.product_code ? (
                                      <span className="errorMsg">
                                        {errors.product_code}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              </Row>
                              <Row>
                                <Col xs={12} sm={12} md={12}>
                                  <div className="form-group">
                                    <label>
                                      Market<span className="impField">*</span>
                                    </label>
                                    <Select
                                      isMulti
                                      className=""
                                      classNamePrefix="select"
                                      defaultValue=""
                                      isClearable={true}
                                      isSearchable={true}
                                      name="country_code"
                                      value={values.country_code}
                                      options={this.state.country}
                                      onChange={(e) => {
                                        //console.log("++++++++this is :",e);
                                        this.setCountry(e, setFieldValue);
                                      }}
                                    >
                                    </Select>
                                    {errors.country_code && touched.country_code ? (
                                      <span className="errorMsg">
                                        {errors.country_code}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              </Row>
                              <Row>
                                <Col xs={12} sm={12} md={12}>
                                  <div className="form-group">
                                    <label>
                                      Pharmacopeial<span className="impField">*</span>
                                    </label>
                                    <Field
                                      className={`selectArowGray form-control`}
                                      component="select"
                                      autoComplete="off"
                                      name="pharmacopeial_code"
                                      value={values.pharmacopeial_code}
                                    >
                                      <option key="-1" value="">
                                        Select
                                      </option>
                                      {this.state.pharmacopeial.map(
                                        (pharmacopeial, i) => (
                                          <option key={i} value={pharmacopeial.value}>
                                            {pharmacopeial.label}
                                          </option>
                                        )
                                      )}
                                    </Field>
                                    {errors.pharmacopeial_code && touched.pharmacopeial_code ? (
                                      <span className="errorMsg">
                                        {errors.pharmacopeial_code}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              </Row>
                              <Row>
                                <Col xs={12} sm={12} md={12}>
                                  <div className="form-group">
                                    <label>
                                      Specification<span className="impField">*</span>
                                    </label>
                                    <Field
                                      name="specification"
                                      type="text"
                                      className={`form-control`}
                                      placeholder="Enter specification"
                                      autoComplete="off"
                                      value={values.specification}
                                    />
                                    {errors.specification && touched.specification ? (
                                      <span className="errorMsg">
                                        {errors.specification}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              </Row>
                              <Row>
                                <Col xs={12} sm={12} md={12}>
                                  <div className="form-group">
                                    <label>
                                      Plant<span className="impField">*</span>
                                    </label>
                                    <Select
                                      isMulti
                                      className=""
                                      classNamePrefix="select"
                                      defaultValue=""
                                      isClearable={true}
                                      isSearchable={true}
                                      name="plant_code"
                                      value={values.plant_code}
                                      options={this.state.plant}
                                      onChange={(e) => {
                                        //console.log("++++++++this is :",e);
                                        this.setPlant(e, setFieldValue);
                                      }}
                                    >
                                    </Select>
                                    {errors.plant_code && touched.plant_code ? (
                                      <span className="errorMsg">
                                        {errors.plant_code}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              </Row>
                              <Row>
                                <Col xs={12} sm={12} md={12}>
                                  <div className="form-group">
                                    <label>
                                      QA Users<span className="impField">*</span>
                                    </label>
                                    <Select
                                      isMulti
                                      className=""
                                      classNamePrefix="select"
                                      defaultValue=""
                                      isClearable={true}
                                      isSearchable={true}
                                      name="employee_id"
                                      value={values.employee_id}
                                      options={this.state.qa_login}
                                      onChange={(e) => {
                                        //console.log("++++++++this is :",e);
                                        this.setQaLogin(e, setFieldValue);
                                      }}
                                    >
                                    </Select>
                                    {errors.employee_id && touched.employee_id ? (
                                      <span className="errorMsg">
                                        {errors.employee_id}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              </Row>
                              {/* <Row>
                                <Col xs={6} sm={6} md={6}>
                                  <div className="form-group react-date-picker sub-task-area">
                                    <label>
                                      Expiry Date<span className="impField">*</span>
                                    </label>
                                    <div className="form-control">
                                      <DatePicker
                                        name='date_expiry'
                                        showMonthDropdown
                                        showYearDropdown
                                        dropdownMode='select'
                                        className="borderNone"
                                        minDate={new Date()}
                                        selected={values.date_expiry}
                                        placeholderText='Select date'
                                        onChange={(e) => {
                                          if (e === null) {
                                            setFieldValue('date_expiry', '');
                                          } else {
                                            setFieldValue('date_expiry', e);
                                          }
                                        }}
                                        dateFormat="dd/MM/yyyy"
                                        autoComplete="off"
                                      />
                                      {errors.date_expiry &&
                                        touched.date_expiry ? (
                                        <span className='errorMsg'>
                                          {errors.date_expiry}
                                        </span>
                                      ) : null}
                                    </div>
                                  </div>
                                </Col>
                              </Row> */}
                              <Row>
                                <Col xs={12} sm={12} md={12}>
                                  <div className="form-group">
                                    <label>
                                      Status<span className="impField">*</span>
                                    </label>
                                    <Field

                                      className={`selectArowGray form-control`}
                                      component="select"
                                      autoComplete="off"
                                      name="status"
                                      value={values.status}
                                    >
                                      <option key="-1" value="">
                                        Select
                                      </option>
                                      {this.state.selectStatus.map(
                                        (status, i) => (
                                          <option key={i} value={status.value}>
                                            {status.label}
                                          </option>
                                        )
                                      )}
                                    </Field>
                                    {errors.status && touched.status ? (
                                      <span className="errorMsg">
                                        {errors.status}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              </Row>
                              <Row>
                                <Col xs={12} sm={6} md={4}>
                                  <div className="form-group">
                                    <label>
                                      Upload COA doc{" "}
                                      <span class="required-field">*</span>
                                    </label>
                                    <Dropzone
                                      maxFiles={1}
                                      multiple={false}
                                      onDrop={(acceptedFiles) =>
                                        this.setDropZoneFiles(
                                          acceptedFiles,
                                          setErrors,
                                          setFieldValue,
                                          setFieldTouched
                                        )
                                      }
                                    >
                                      {({ getRootProps, getInputProps }) => (
                                        <section>
                                          <div
                                            {...getRootProps()}
                                            className="custom-file-upload-header"
                                          >
                                            <input {...getInputProps()} />
                                            <p>Upload file</p>
                                          </div>
                                          <div className="custom-file-upload-area">
                                            {this.state.filesHtml}
                                            {this.state.prevFilesHtml}
                                          </div>
                                        </section>
                                      )}
                                    </Dropzone>

                                    {errors.file_name &&
                                      touched.file_name ? (
                                      <span className="errorMsg">
                                        {errors.file_name}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              </Row>
                              {errors.message ? (
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <span className="errorMsg">
                                      {errors.message}
                                    </span>
                                  </Col>
                                </Row>

                              ) : ("")
                              }
                            </div>
                          </Modal.Body>
                          <Modal.Footer>
                            <button
                              className={`btn btn-success btn-sm ${isValid ? "btn-custom-green" : "btn-disable"
                                } mr-2`}
                              type="submit"
                              disabled={isValid ? false : false}
                            >
                              {this.state.typicalCoaflagId > 0
                                ? isSubmitting
                                  ? "Updating..."
                                  : "Update"
                                : isSubmitting
                                  ? "Submitting..."
                                  : "Submit"}
                            </button>
                            <button
                              onClick={e => this.modalCloseHandler()}
                              className={`btn btn-danger btn-sm`}
                              type="button"
                            >
                              Close
                            </button>
                          </Modal.Footer>
                        </Form>
                      );
                    }}
                  </Formik>
                </Modal>
                <Modal show={this.state.showModel_password} onHide={() => this.modalCloseHandler()} backdrop="static" dialogClassName="profile-popup">
                  <Modal.Header closeButton>
                    <Modal.Title>Change Password</Modal.Title>
                  </Modal.Header>

                  <Modal.Body>
                    <div className="contBox">
                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="profile">
                            <Formik
                              validationSchema={validatePaaword}
                              onSubmit={this.handleSubmitEvent_pass}
                            >
                              {({ values, errors, touched, isValid, isSubmitting }) => {
                                return (
                                  <Form>

                                    <Row>
                                      <Col xs={12} sm={12} md={12}>
                                        <div className="form-group">
                                          <label className="pull-left" style={{ float: "left" }}>Password</label>
                                          <Field
                                            name="password"
                                            type="password"
                                            className={`form-control`}
                                            placeholder="Enter password"
                                            autoComplete="off"
                                          />
                                          {errors.password && touched.password ? (
                                            <span className="errorMsg">
                                              {errors.password}
                                            </span>
                                          ) : null}
                                        </div>
                                      </Col>
                                    </Row>
                                    <Row>
                                      <Col xs={12} sm={12} md={12}>
                                        <div className="form-group">
                                          <label className="pull-left">Confirm Password</label>
                                          <Field
                                            name="confirm_password"
                                            type="password"
                                            className={`form-control`}
                                            placeholder="Enter confirm password"
                                            autoComplete="off"
                                          />
                                          {errors.confirm_password &&
                                            touched.confirm_password ? (
                                            <span className="errorMsg">
                                              {errors.confirm_password}
                                            </span>
                                          ) : null}
                                        </div>
                                      </Col>
                                    </Row>
                                    <div className="button-footer btn-toolbar">
                                      <button
                                        type="button"
                                        className="btn btn-success btn-sm"
                                        onClick={() => this.modalCloseHandler()}
                                      >
                                        Cancel
                                      </button>
                                      <button
                                        className={`btn btn-danger btn-sm ${isValid ? "btn-custom-green" : "btn-disable"
                                          } m-r-10`}
                                        type="submit"
                                        disabled={isValid ? false : true}
                                      >
                                        {isSubmitting ? "Updating..." : "Submit"}
                                      </button>
                                    </div>

                                  </Form>
                                );
                              }}
                            </Formik>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </Modal.Body>
                </Modal>

              </div>
            </div>
            {this.state.count > this.state.itemPerPage ? (
              <Row>
                <Col md={12}>
                  <div className="paginationOuter text-right">
                    <Pagination
                      activePage={this.state.activePage}
                      itemsCountPerPage={this.state.itemPerPage}
                      totalItemsCount={this.state.count}
                      itemClass='page-item'
                      linkClass='page-link'
                      activeClass='active'
                      onChange={this.handlePageChange}
                    />
                  </div>
                </Col>
              </Row>
            ) : null}

          </section>
        </div>
      )
    }
  }
}

export default withRouter(ManageTypicalCoa);