import React, { Component } from "react";
import { Row, Col } from "react-bootstrap";
import { Formik, Field, Form } from "formik";
import { Link } from "react-router-dom";
import editImage from "../../assets/images/image-edit.jpg";

import { getMyId,getMyPic } from "../../shared/helper";
import API from "../../shared/axios";
import * as Yup from "yup";
import swal from "sweetalert";

const initialValues = {  
};

class changePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      employeeDetails: []
    };
  }

  componentDidMount() {
    
  }

  handleSubmitEvent = (values, actions) => {
    var my_id = getMyId(localStorage.token);
    const post_data = {
      password: values.password
    };

  //   API.put(`/api/employees/${my_id}`, post_data)
  //     .then(res => {
  //       this.modalCloseHandler();
  //       swal({
  //         closeOnClickOutside: false,
  //         title: "Success",
  //         text: "Record updated successfully.",
  //         icon: "success"
  //       }).then(() => {
  //         this.getEmployeeList();
  //       });
  //     })
  //     .catch(err => {
  //       actions.setErrors(err.data.errors);
  //       actions.setSubmitting(false);
  //     });
  };

  render() {
    // const { employeeDetails } = this.state;

    const newInitialValues = Object.assign(initialValues, {
      
    });

    const validatePaaword = Yup.object().shape({
      password: Yup.string()
        .required("Please enter your new password")
        .min(8, "Password must be at least 8 characters long"),
      confirm_password: Yup.string()
        .required("Please enter your confirm password")
        .test("match", "Confirm password do not match", function(
          confirm_password
        ) {
          return confirm_password === this.parent.password;
        })
    });
    // console.log(this.state.employeeDetails);
    return (
      <>
        <div className="content-wrapper">
          {/*<!-- Content Header (Page header) -->*/}
          <section className="content-header">
            <div className="row">
              <div className="col-lg-9 col-sm-6 col-xs-12">
                <h1>
                  Change Password
                  <small />
                </h1>
              </div>
              <div className="col-lg-3 col-sm-6 col-xs-12" />
            </div>
          </section>

          {/*<!-- Main content -->*/}
          <section className="content">
            <div className="boxPapanel content-padding">
              <Row>                
                <Formik
                  initialValues={newInitialValues}
                  validationSchema={validatePaaword}
                  onSubmit={this.handleSubmitEvent}
                >
                  {({ values, errors, touched, isValid, isSubmitting }) => {
                    return (
                      <Form>
                        <Col xs={10} sm={10} md={10}>
                          <Row>  
                            <Col xs={12} sm={6} md={6}>
                              <div className="form-group">
                                <label>Password</label>
                                <Field
                                  name="password"
                                  type="password"
                                  className={`form-control`}
                                  placeholder="Enter password"
                                  autoComplete="off"
                                />
                                {errors.password && touched.password ? (
                                  <span className="errorMsg">
                                    {errors.password}
                                  </span>
                                ) : null}
                              </div>
                            </Col>
                            <Col xs={12} sm={6} md={6}>
                              <div className="form-group">
                                <label>Confirm Password</label>
                                <Field
                                  name="confirm_password"
                                  type="password"
                                  className={`form-control`}
                                  placeholder="Enter confirm password"
                                  autoComplete="off"
                                />
                                {errors.confirm_password &&
                                touched.confirm_password ? (
                                  <span className="errorMsg">
                                    {errors.confirm_password}
                                  </span>
                                ) : null}
                              </div>
                            </Col>
                          </Row>
                          <div className="button-footer btn-toolbar">
                            <button
                              type="button"
                              className="btn btn-danger btn-sm"
                            >
                              Cancel
                            </button>
                            <button
                              className={`btn btn-success btn-sm ${
                                isValid ? "btn-custom-green" : "btn-disable"
                              } m-r-10`}
                              type="submit"
                              disabled={isValid ? false : true}
                            >
                              {isSubmitting ? "Updating..." : "Submit"}
                            </button>
                          </div>
                        </Col>
                      </Form>
                    );
                  }}
                </Formik>
              </Row>
            </div>
          </section>
        </div>
      </>
    );
  }
}

export default changePassword;
