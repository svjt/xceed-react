import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import axios from "../../shared/axios";

import LoadingSpinner from "../loader/loadingSpinner";
import drreddylogo from "../../assets/images/drreddylogo_white.png";
import exceedlogo from "../../assets/images/Xceed_Logo_Purple_RGB.png";

const initialValues = {
  username: "",
  password: "",
};

var message = "";
var err_message = '';

class forgotPassword extends Component {
  constructor(props) {
    super(props);

    message = this.props.location.state
      ? this.props.location.state.message
      : "";
    err_message = this.props.location.state
      ? this.props.location.state.err_message
      : "";
  }

  state = {
    message : '',
    err_message: '',
  };

  componentDidMount() {

    if (localStorage.qa_token !== null && typeof localStorage.qa_token != 'undefined' ){
      this.props.history.push("/qa_form");
    }

    if (message !== null) {
      this.props.history.push({
        pathname: "",
        state: "",
      });
      this.setState({'message' : message})
    }
    if (err_message !== null) {
      this.props.history.push({
        pathname: "",
        state: "",
      });
      this.setState({'err_message' : err_message})
    }
    document.title = `QA Forgot Password | Dr.Reddys API`;
  }

  handleSubmit = (values, actions) => {
    axios
    .post("/api/employees/qa/forgotpassword", {"email": values.username.trim()})
    .then(res => {
        this.setState({      
            message: "Further instructions have been sent to your email address.",
        });             
        actions.setFieldValue("username", "");
        actions.setTouched("username",false);
    })
    .catch(err => { console.log(err)
      this.setState({      
        err_message: err.data.errors,
      }); 
      actions.setSubmitting(false);
    });

  };

  render() {

    const forgetvalidation = Yup.object().shape({
      username: Yup.string()
        .required(`Please enter email address`)
        .email(
          `Please enter a valid email address`
        ),      
    });

    const { loading } = this.state;

    return (
      <>
        <div className="login-box">
          {loading ? (
            <LoadingSpinner />
          ) : (
            <Formik
              initialValues={initialValues}
              validationSchema={forgetvalidation}
              onSubmit={this.handleSubmit}
            >
              {({
                values,
                errors,
                touched,
                handleChange,
                isValid,
                handleBlur,
                setFieldValue,
              }) => {
                //console.log( errors );
                return (
                  <Form>
                    <div className="login-logo">
                      <Link to="/" className="logo">
                        <span className="logo-mini">
                          <img src={drreddylogo} alt="Dr.Reddy's" />
                        </span>
                      </Link>
                      <br />
                      <span className="logo-mini">
                        <img src={exceedlogo} alt="Dr.Reddy's" height="36px" />
                      </span>
                    </div>

                    <div className="login-box-body">
                      
                      {this.state.message && <h5 style={{color: '#1bb200',padding: '5px 10px',textAlign : "center"}}>{this.state.message}</h5>}
                      {this.state.err_message && <h5 style={{color: '#c83a40',padding: '5px 10px',textAlign : "center"}}>{this.state.err_message}</h5>}
                      <p className="login-box-msg">QA Forget Password</p>

                      <label>Email:</label>
                      <div className="form-group has-feedback m-b-25">
                        
                        <Field
                          name="username"
                          type="text"
                          className="form-control"
                          autoComplete="off"
                          placeholder="Please enter email"
                          onChange={handleChange}
                          autoFocus={this.state.enableFocus}                          
                        />

                        {errors.username && touched.username && (
                          <label>{errors.username}</label>
                        )}

                      </div>
                      
                      <div className="row">
                        <div className="col-xs-8">
                          <Link to={'/qa_login'}>I already have an account</Link>
                        </div>
                        <div className="col-xs-4">
                          <button
                            className="btn-fill"
                            type="submit"
                          >
                            Submit
                          </button>
                        </div>
                      </div>
                    </div>
                  </Form>
                );
              }}
            </Formik>
          )}{" "}
          {/* end of loader */}
        </div>
      </>
    );  
  }
}


export default forgotPassword;
