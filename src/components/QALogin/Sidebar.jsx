import React, { Component } from 'react';
import { Link,withRouter } from 'react-router-dom';

import './Sidebar.css';

import DashboradImage from '../../assets/images/dashboard-icon.svg';
import MyTeamImage from '../../assets/images/team-icon.svg';

import DashboradImageActive from '../../assets/images/dashboard-icon-active.svg';
import MyTeamImageActive from '../../assets/images/team-icon-active.svg';


class Sidebar extends Component {   

  constructor(){
    super();
  }

  render() { 
    if( this.props.isLoggedIn === false) return null;
   
    return ( 
        <>
        <aside className="main-sidebar">
          <section className="sidebar">
            <ul className="sidebar-menu qa-sidebar">
              <li className="header">MAIN NAVIGATION</li>
              {
                window.location.pathname === "/qa_form" ? (
                <li className="treeview active"> 
                  <Link to={'/qa_form'}>
                    <img src={DashboradImageActive} alt="Dashboard" className="main-icon" />
                    <span className="static-text">Home</span>
                  </Link> 
                </li>
                ) : (
                <li className="treeview"> 
                  <Link to={'/qa_form'}>
                    <img src={DashboradImage} alt="Dashboard" className="main-icon" />
                    <span className="static-text">Home</span>
                  </Link> 
                </li>
                )
              }
              {
                window.location.pathname === "/qa_uploads" ? (
                <li className="treeview active"> 
                  <Link to={'/qa_uploads'}>
                    <img src={MyTeamImageActive} alt="Uploads" className="main-icon" />
                    <span className="static-text">Uploads</span>
                  </Link> 
                </li>
                ) : (
                <li className="treeview"> 
                  <Link to={'/qa_uploads'}>
                    <img src={MyTeamImage} alt="Uploads" className="main-icon" />
                    <span className="static-text">Uploads</span>
                  </Link> 
                </li>
                )
              }
              {
                window.location.pathname === "/qa_coa_tasks" ? (
                <li className="treeview active"> 
                  <Link to={'/qa_coa_tasks'}>
                    <img src={MyTeamImageActive} alt="COA_tasks" className="main-icon" />
                    <span className="static-text">Tasks</span>
                  </Link> 
                </li>
                ) : (
                <li className="treeview"> 
                  <Link to={'/qa_coa_tasks'}>
                    <img src={MyTeamImage} alt="COA_tasks" className="main-icon" />
                    <span className="static-text">Tasks</span>
                  </Link> 
                </li>
                )
              }
               {
                window.location.pathname === "/qa_manage_typical_coa" ? (
                <li className="treeview active"> 
                  <Link to={'/qa_manage_typical_coa'}>
                    <img src={MyTeamImageActive} alt="COA_tasks" className="main-icon" />
                    <span className="static-text">Typical COA</span>
                  </Link> 
                </li>
                ) : (
                <li className="treeview"> 
                  <Link to={'/qa_manage_typical_coa'}>
                    <img src={MyTeamImage} alt="COA_tasks" className="main-icon" />
                    <span className="static-text">Typical COA</span>
                  </Link> 
                </li>
                )
              } 
            </ul>
          </section>
        </aside>
        </>
      );
    }
}

export default withRouter(Sidebar);