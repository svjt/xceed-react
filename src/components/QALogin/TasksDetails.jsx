import React, { Component } from "react";
import { Row, Col, Modal, Tooltip, OverlayTrigger, } from "react-bootstrap";
import { Link } from "react-router-dom";
import dateFormat from 'dateformat';
import ReactHtmlParser from "react-html-parser";
import swal from "sweetalert";
import { htmlDecode, localDate, localDateOnly } from "../../shared/helper";
import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
import commenLogo from "../../assets/images/drreddylogosmall.png";
import commenLogo1 from "../../assets/images/drreddynoimg.png";
import downloadIcon from "../../assets/images/download-icon.svg";
import API from "../../shared/axios_qa";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import Switch from "react-switch";
import { Accordion, AccordionItem, AccordionItemHeading, AccordionItemButton, AccordionItemPanel } from 'react-accessible-accordion';
import 'react-accessible-accordion/dist/fancy-example.css';
const ship_download = `${process.env.REACT_APP_API_URL}/api/tasks/download_ship_doc/`;  // Deba
const s3bucket_task_diss_path_coa = `${process.env.REACT_APP_API_URL}/api/tasks/download_task_bucket_coa/`;

const req_category_arr = [
  { request_category_id: 1, request_category_value: "New Order" },
  { request_category_id: 2, request_category_value: "Other" },
  { request_category_id: 3, request_category_value: "Complaint" },
  { request_category_id: 4, request_category_value: "Forecast" },
  { request_category_id: 5, request_category_value: "Payment" },
  { request_category_id: 6, request_category_value: "Notification" },
  {
    request_category_id: 7,
    request_category_value: "Request for Proforma Invoice",
  },
];
const validatePaaword = Yup.object().shape({
  password: Yup.string().trim()
    .required("Please enter your new password")
    .min(8, "Password must be at least 8 characters long"),
  confirm_password: Yup.string().trim()
    .required("Please enter your confirm password")
    .test("match", "Confirm password do not match", function (
      confirm_password
    ) {
      return confirm_password === this.parent.password;
    })
});

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (

    <Link to={href} onClick={clicked}>
      {children}
    </Link>

  );
};

function LinkWithTooltipText({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={
        <Tooltip id={id} style={{ width: "100%", wordBreak: "break-all" }}>
          {tooltip}
        </Tooltip>
      }
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} style={{ cursor: "text" }}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

class TasksDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      invalid_access: false,
      show_lang: "EN",
      show_lang_diss: "EN",
      taskDetails: [],
      taskDetailsFiles: [],
      taskDetailsFilesCoa: [],
      taskDetailsFilesSTOCoa: [],
      taskDetailsFilesInvoice: [],
      taskDetailsFilesPacking: [],
      comments: [],
      discussDetails: [],
      discussFile: [],
      discussTaskRef: "",
      discussReqName: "",
      discussDetailsT: [],
      discussFileT: [],
      discussTaskRefT: "",
      discussReqNameT: "",
      for_review_task: "",
      for_review_comment: "",
      commentExists: "",
      discussionExists: "",
      activity_log: [],
      shippingInfo: [],
      shippingSTOInfo: [],
      task_id: this.props.match.params.id,
    };
  }

  componentDidMount() {
    document.title = `QA COA Tasks Details | Dr.Reddys API`;
    this.setState({
      isLoading: true,
    });
    this.getCOATaskDetails();
    if (this.props.location.hash === "#discussion") {
      this.setState({ taskDetailsClass: "", discussionClass: "active" });
    } else {
      this.setState({ taskDetailsClass: "active", discussionClass: "" });
    }
  };

  getCOATaskDetails() {
    this.setState({ invalid_access: false });
    API.get(`/api/employees/qa/tasks_details/${this.state.task_id}`)
      .then((res) => {
        this.setState({
          taskDetails: res.data.data.taskDetails,
          taskDetailsFiles: res.data.data.taskDetailsFiles,
          taskDetailsFilesCoa: res.data.data.taskDetailsFilesCoa,
          taskDetailsFilesSTOCoa: res.data.data.taskDetailsFilesSTOCoa,
          taskDetailsFilesInvoice: res.data.data.taskDetailsFilesInvoice,
          taskDetailsFilesPacking: res.data.data.taskDetailsFilesPacking,
          for_review_task: res.data.data.for_review_task,
          for_review_comment: res.data.data.for_review_comment,
          comments: res.data.data.comments,
          commentExists: res.data.data.commentExists,
          discussionExists: res.data.data.discussionExists,
          shippingInfo: res.data.data.shippingInfo,
          shippingSTOInfo: res.data.data.shippingSTOInfo,
        });

        if (res.data.data.discussionExists) {
          API.get(`api/employees/qa/discussions/${this.state.task_id}`)
            .then((res) => {
              this.setState({
                discussDetails: res.data.data.discussion,
                discussTaskRef: res.data.data.task_ref,
                discussReqName: res.data.data.req_name,
                discussFile: res.data.data.files,
              });
            })
            .catch((err) => {
              console.log(err);
            });

          if (res.data.data.taskDetails.language != "en" || (res.data.data.taskDetails.parent_id > 0 && res.data.data.taskDetails.parent_language != "en")) {
            API.get(`api/employees/qa/translated_discussions/${this.state.task_id}`)
              .then((res) => {
                this.setState({
                  discussDetailsT: res.data.data.discussion,
                  discussTaskRefT: res.data.data.task_ref,
                  discussReqNameT: res.data.data.req_name,
                  discussFileT: res.data.data.files,
                });
              })
              .catch((err) => {
                console.log(err);
              });
          }
        }
      })
      .catch((err) => {
        var errText = "Invalid Access To This Page.";
        this.setState({ invalid_access: true });
        swal({
          closeOnClickOutside: false,
          title: "Error in page",
          text: errText,
          icon: "error",
        }).then(() => {
          this.props.history.push('/qa_coa_tasks');
        });
      });

  };

  getActivityLog = () => {
    API.get(
      `/api/employees/qa/activity_log/${this.state.task_id}`
    )
      .then((res) => {
        this.setState({ activity_log: res.data.data });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  redirectUrlTask = (event, path) => {
    event.preventDefault();
    window.open(path, "_self");
  };

  setDateOnly(date_value) {
    var mydate = localDateOnly(date_value);
    return dateFormat(mydate, "dd/mm/yyyy");
  }

  checkHandler = (event) => {
    event.preventDefault();
  };

  handleTabs = (event) => {
    if (event.currentTarget.className === "active") {
      //DO NOTHING
    } else {
      var elems = document.querySelectorAll('[id^="tab_"]');
      var elemsContainer = document.querySelectorAll('[id^="show_tab_"]');
      var currId = event.currentTarget.id;

      for (var i = 0; i < elems.length; i++) {
        elems[i].classList.remove("active");
      }

      for (var j = 0; j < elemsContainer.length; j++) {
        elemsContainer[j].style.display = "none";
      }
      event.currentTarget.classList.add("active");
      event.currentTarget.classList.add("active");

      if (
        currId === "tab_1" &&
        this.state.taskDetails.language !== "en" &&
        this.state.taskDetails.parent_id === 0
      ) {
        if (this.state.show_lang === "EN") {
          document.querySelector("#show_tab_5").style.display = "block";
        } else {
          document.querySelector("#show_tab_1").style.display = "block";
        }
      } else {
        document.querySelector("#show_" + currId).style.display = "block";
      }

      if (currId === "tab_4") {
        this.getActivityLog();
      }

      this.setState({ files: [], filesHtml: "" });
    }
  };

  handleTabs2 = (event) => {
    if (event.currentTarget.className === "active") {
      //DO NOTHING
    } else {
      var elems = document.querySelectorAll('[id^="d_tab_"]');
      var elemsContainer = document.querySelectorAll('[id^="show_d_tab_"]');
      var currId = event.currentTarget.id;

      for (var i = 0; i < elems.length; i++) {
        elems[i].classList.remove("active");
      }

      for (var j = 0; j < elemsContainer.length; j++) {
        elemsContainer[j].style.display = "none";
      }

      event.currentTarget.classList.add("active");
      event.currentTarget.classList.add("active");

      if (
        currId === "d_tab_1" &&
        ((this.state.taskDetails.language !== "en" &&
          this.state.taskDetails.parent_id === 0) ||
          (this.state.taskDetails.parent_language !== "en" &&
            this.state.taskDetails.parent_id > 0))
      ) {
        if (this.state.show_lang_diss === "EN") {
          document.querySelector("#show_d_tab_3").style.display = "block";
        } else {
          document.querySelector("#show_d_tab_1").style.display = "block";
        }
      } else {
        document.querySelector("#show_" + currId).style.display = "block";
      }

      this.setState({ files: [], filesHtml: "" });
    }
  };

  setReqCategoryName = (req_cate_id) => {
    var ret = "Not Set";
    for (let index = 0; index < req_category_arr.length; index++) {
      const element = req_category_arr[index];
      if (element["request_category_id"] === req_cate_id) {
        ret = element["request_category_value"];
      }
    }

    return ret;
  };

  getComments = () => {
    if (this.state.comments.length > 0) {
      const comment_html = this.state.comments.map((comm, i) => (
        <div className="comment-list-wrap" key={i}>
          <div className="comment">
            <div className="row">
              <div className="col-md-10 col-sm-10 col-xs-12">
                <div className="imageArea">
                  <div className="imageBorder">
                    {comm.profile_pic != "" && comm.profile_pic != null ? (
                      <img alt="noimage" src={comm.profile_pic} />
                    ) : (
                      <img alt="noimage" src={commenLogo} />
                    )}
                  </div>
                </div>
                <div className="conArea">
                  <p>
                    {comm.posted_from == "S"
                      ? `${comm.first_name} ${comm.last_name} (Sales Force)`
                      : `${comm.first_name} ${comm.last_name}`}
                  </p>
                  <span>{ReactHtmlParser(htmlDecode(comm.comment))}</span>
                </div>
                <div className="clearfix" />
              </div>
              <div className="col-md-2 col-sm-2 col-xs-12">
                <div className="dateArea">
                  <p>
                    {dateFormat(localDate(comm.post_date), "ddd, mmm dS, yyyy")}
                  </p>
                </div>
              </div>
            </div>
            {this.getCommentFile(comm.uploads, comm.multi)}
          </div>
        </div>
      ));

      return (
        <>
          <div className="disHead">
            <label>Task Comments:</label>
          </div>
          <div className="comment-list-main-wrapper">{comment_html}</div>
        </>
      );
    }
  };

  getCommentFile = (uploads, multi) => {
    if (typeof uploads === "undefined") {
      return "";
    } else {
      return (
        <ul className="conDocList">
          {uploads.map((files, j) => (
            <li key={j}>
              {process.env.NODE_ENV === "development" ? (
                <LinkWithTooltip
                  href="#"
                  id="tooltip-1"
                >
                  {files.actual_file_name}
                </LinkWithTooltip>
              ) : (
                <LinkWithTooltip
                  href="#"
                  id="tooltip-1"
                >
                  {files.actual_file_name}
                </LinkWithTooltip>
              )}
            </li>
          ))}
        </ul>
      );
    }
  };

  getDiscussionFile = (uploads, multi) => {
    if (typeof uploads === "undefined") {
      return "";
    } else {
      return (
        <ul className="conDocList">
          {console.log("NODE ENV", process.env.NODE_ENV)}
          {uploads.map((files, j) => (
            <li key={j}>
              {process.env.NODE_ENV === "development" ? (
                <LinkWithTooltip
                  tooltip={`${files.new_file_name}`}
                  href="#"
                  id="tooltip-1"
                >
                  {files.actual_file_name}
                </LinkWithTooltip>
              ) : (
                <LinkWithTooltip
                  tooltip={`${files.new_file_name}`}
                  href="#"
                  id="tooltip-1"
                >
                  {files.actual_file_name}
                </LinkWithTooltip>
              )}
            </li>
          ))}

        </ul>
      );
    }
  };

  getConverTime = (time) => {
    let timeString = '';
    let H = time.substr(0, 2);
    let h = H % 12 || 12;
    let ampm = (H < 12 || H === 24) ? "AM" : "PM";
    timeString = h.toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false }) + ":" + time.substr(2, 3) + " " + ampm;
    return timeString;
  };

  getBlueDartDynamicContent = (sel_index, infoArr) => {

    let dynamic_html = infoArr.map((value, index) => {
      if (sel_index == index) {
        let obj = infoArr[sel_index][Object.keys(infoArr[sel_index])];

        let html_bottom = [];
        for (let props in obj.shipping_steps) {
          html_bottom.push(
            <tr>
              <td>{obj.shipping_steps[props].scan_date}</td>
              <td>{this.getConverTime(obj.shipping_steps[props].scan_time)}</td>
              <td>{obj.shipping_steps[props].scanned_location}</td>
              <td>
                {(obj.shipping_steps[props].cstat_desc) ? obj.shipping_steps[props].cstat_desc : obj.shipping_steps[props].scan}
                {(obj.shipping_steps[props].scan_code == 'POD' && obj.shipping_steps[props].file_path != '') && (<a href={`${ship_download + obj.shipping_steps[props].file_path}/bluedart`} target="_blank">
                  <img src={downloadIcon} width="28" height="28" id="bluedartdownload" type="button" />
                </a>)}
              </td>
            </tr>
          );
        }

        return (
          <Row>
            <Col sm="12">
              <div className="views-row">
                <div className="">
                  <strong>
                    Invoice Date
                  </strong>
                  <div className="field-content">
                    {obj.invoice_display_date}
                  </div>
                </div>
                <div className="">
                  <strong>
                    Invoice File
                  </strong>
                  <div className="field-content">
                    {obj.invoice_file_name}
                    {(obj.invoice_file_name != '') && (<a href={`${ship_download + obj.invoice_file_hash}/invoice`} target="_blank">
                      <img src={downloadIcon} width="28" height="28" id="invoicedownload" type="button" />
                    </a>)}
                  </div>
                </div>
                <div className="">
                  <strong>
                    Invoice Location
                  </strong>
                  <div className="field-content">
                    {obj.invoice_location}
                  </div>
                </div>
                <div className="">
                  <strong>
                    Packing Date
                  </strong>
                  <div className="field-content">
                    {obj.packing_display_date}
                  </div>
                </div>
                <div className="">
                  <strong>
                    Packing File
                  </strong>
                  <div className="field-content">
                    {obj.packing_file_name}
                    {(obj.packing_file_name != '') && (<a href={`${ship_download + obj.packing_file_hash}/package`} target="_blank">
                      <img src={downloadIcon} width="28" height="28" id="invoicedownload" type="button" />
                    </a>)}
                  </div>
                </div>
                <div className="">
                  <strong>
                    Packing Location
                  </strong>
                  <div className="field-content">
                    {obj.packing_location}
                  </div>
                </div>

                <div className="">
                  <strong>
                    Ref. No.
                  </strong>
                  <div className="field-content">
                    {obj.ref_no}
                  </div>
                </div>
                <div className="">
                  <strong>
                    Origin Location
                  </strong>
                  <div className="field-content">
                    {obj.origin}
                  </div>
                </div>
                <div className="">
                  <strong>
                    Destination Location
                  </strong>
                  <div className="field-content">
                    {obj.destination}
                  </div>
                </div>
                <div className="">
                  <strong>
                    Pick Up Date
                  </strong>
                  <div className="field-content">
                    {obj.pick_up_date}
                  </div>
                </div>
                <div className="">
                  <strong>
                    Pick Up Time
                  </strong>
                  <div className="field-content">
                    {(obj.pick_up_time != null && obj.pick_up_time != '') ? this.getConverTime(obj.pick_up_time) : null}
                  </div>
                </div>
                <div className="">
                  <strong>
                    Expected Delivery Date
                  </strong>
                  <div className="field-content">
                    {obj.expected_delivery_date}
                  </div>
                </div>
                <div className="">
                  <strong>
                    Original Delivery Date
                  </strong>
                  <div className="field-content">
                    {obj.dynamic_expected_delivery_date}
                  </div>
                </div>
              </div>
              <div className="table-responsive-lg">
                <table className="orderDetailsTable table table-striped">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Time</th>
                      <th>Location</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    {html_bottom}

                  </tbody>
                </table>
              </div>
            </Col>
          </Row>
        );
      }
    });
    return dynamic_html;
  };

  getDynamicContent = (sel_index = 0, infoArr) => {

    let dynamic_html = infoArr.map((value, index) => {
      if (sel_index == index) {
        let obj = infoArr[sel_index][Object.keys(infoArr[sel_index])];

        let html_bottom = [];
        let shipping_step = ['Approval Document', 'Approval Haz Clearance', 'FDA Approval', 'Customs Clearance'];
        for (let property in obj) {

          if (property == 'Shipping Progress') {
            for (let props in obj[property]) {
              if (obj[property][props].date != '' && obj[property][props].date != undefined && property != 'partner') {
                html_bottom.push(
                  <tr>
                    <td>{obj[property][props].date}</td>
                    <td>{obj[property][props].time}</td>
                    <td>{obj[property][props].location}</td>
                    <td>{shipping_step[props]} {obj[property][props].comment}</td>
                  </tr>
                );
              }
            }
          } else if (property != 'partner') {
            if (obj[property] && obj[property].date != '') {
              html_bottom.push(
                <tr>
                  <td>{obj[property].date}</td>
                  <td>{obj[property].time}</td>
                  <td>{obj[property].location}</td>
                  <td>
                    {property + ".  "}
                    {(property == 'Invoice Doc' && obj[property].comment != '') && (<a href={`${ship_download + obj[property].file_hash}/invoice`} target="_blank">
                      <img src={downloadIcon} width="28" height="28" id="invoicedownload" type="button" />
                    </a>)}
                    {(property == 'Packing List' && obj[property].comment != '') && (<a href={`${ship_download + obj[property].file_hash}/package`} target="_blank">
                      <img src={downloadIcon} width="28" height="28" id="invoicedownload" type="button" />
                    </a>)}
                    {(property == 'AWB Doc' && obj[property].comment != '') && (<a href={`${ship_download + obj[property].file_hash}/${obj[property].file_type}`} target="_blank">
                      <img src={downloadIcon} width="28" height="28" id="invoicedownload" type="button" />
                    </a>)}
                    {(property == 'HAWB Doc' && obj[property].comment != '') && (<a href={`${ship_download + obj[property].file_hash}/${obj[property].file_type}`} target="_blank">
                      <img src={downloadIcon} width="28" height="28" id="invoicedownload" type="button" />
                    </a>)}
                  </td>
                </tr>
              );
            }
          }
        }

        return (
          <Row>
            <Col sm="12">
              <div className="table-responsive-lg">
                <table className="orderDetailsTable table table-striped">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Time</th>
                      <th>Location</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    {html_bottom}
                  </tbody>
                </table>
              </div>
            </Col>
          </Row>);

      }
    });
    return dynamic_html;
    //this.setState({dynamic_html:dynamic_html});
  }


  activityLogTable = () => {
    if (this.state.activity_log.length > 0) {
      const comment_html = this.state.activity_log.map((act, i) => (
        <div className="comment-list-wrap" key={i}>
          <div className="comment">
            <div className="row">
              <div className="col-md-10 col-sm-10 col-xs-12">
                <div className="imageArea">
                  <div className="imageBorder">
                    <img alt="noimage" src={commenLogo} />
                  </div>
                </div>
                <div className="conArea">
                  <span>{act.description}</span>
                </div>
                <div className="clearfix" />
              </div>
              <div className="col-md-2 col-sm-2 col-xs-12">
                <div className="dateArea">
                  <p>{act.display_date_added}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      ));
      return (
        <>
          <div className="disHead">
            <label>Activity Logs:</label>
          </div>
          <div className="comment-list-main-wrapper">{comment_html}</div>
        </>
      );
    }
  };

  modalShowHandler = () => {
    this.setState({ showModal: true });
  };
  modalCloseHandler = () => {
    this.setState({ showModal: false });
  };

  handleSubmitEvent = (values, actions) => {
    const post_data = {
      password: values.password
    };

    API.put(`/api/employees/qa/changepassword`, post_data)
      .then(res => {
        this.modalCloseHandler();
        console.log("response", res);
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: "Password updated successfully.",
          icon: "success"
        }).then(() => {
          window.location.reload();
        });
      })
      .catch(err => {
        actions.setErrors(err.data.errors);
        actions.setSubmitting(false);
      });
  };

  render() {


    if (this.state.taskDetails.task_id > 0 && this.state.invalid_access == false) {
      return (
        <>
          <div className="content-wrapper qaWrapper">
            <section className="content-header">
              <div className="row">
                <div className="col-lg-12 col-sm-6 col-xs-12">
                  <h1>QA COA Tasks Details</h1>
                  <div className="customer-password-button btn-fill bdrRadius" onClick={e => this.modalShowHandler()}>
                    Change Password
                  </div>
                  <div className="customer-edit-button btn-fill bdrRadius" onClick={() => { localStorage.clear(); this.props.history.push("/qa_login"); }}
                  >
                    Log Out
                  </div>
                </div>
                <div className="col-lg-12 col-sm-12 col-xs-12 topSearchSection">
                </div>
              </div>

            </section>
            <section className="content">
              <div className="serchapanel">
                <div className="box-body">
                  <section className="content-header task-accept-decline-main-top">
                    <h1>
                      {this.state.taskDetails.req_name} | {this.state.taskDetails.task_ref}
                    </h1>
                  </section>
                  <section className="content switch-class-new">
                    <div className="nav-tabs-custom nav-tabs-custom2">
                      <ul className="nav nav-tabs">
                        <li
                          className={this.state.taskDetailsClass}
                          onClick={(e) => this.handleTabs(e)}
                          id="tab_1"
                        >
                          TASK DETAILS
                        </li>
                        {this.state.commentExists && (
                          <li onClick={(e) => this.handleTabs(e)} id="tab_2">
                            INTERNAL COMMENTS
                          </li>
                        )}
                        {this.state.discussionExists && (
                          <li
                            onClick={(e) => this.handleTabs(e)}
                            id="tab_3"
                            className={this.state.discussionClass}
                          >
                            CUSTOMER DISCUSSIONS
                          </li>
                        )}
                        <li onClick={(e) => this.handleTabs(e)} id="tab_4">
                          ACTIVITY LOG
                        </li>
                      </ul>
                    </div>

                    <div className="tab-content">
                      <div
                        className={`tab-pane ${(this.state.taskDetails.parent_id === 0 && this.state.discussionClass === "active") ? "" : "active"}`}
                        id="show_tab_1"
                      >
                        <div className="boxPapanel content-padding">
                          <div className="taskdetails">
                            <div className="clearfix tdBtmlist">
                              <Row className="task_details_complete">

                                {(this.state.taskDetails.request_type == 43 || this.state.taskDetails.request_type == 23) && this.state.taskDetails.curr_owner_fname !== "" && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>
                                        Current Owner:
                                      </label>{" "}
                                      {this.state.taskDetails.curr_owner_fname}{" "}
                                      {this.state.taskDetails.curr_owner_lname}{" "}
                                      ({this.state.taskDetails.curr_owner_desig})
                                    </div>
                                  </Col>
                                )}
                                {(this.state.taskDetails.request_type == 43 || this.state.taskDetails.request_type == 23) && this.state.taskDetails.company_name !==
                                  "" && (
                                    <Col xs={12} sm={6} md={4}>
                                      <div className="form-group">
                                        <label>Customer Name:</label>{" "}
                                        {
                                          this.state.taskDetails.company_name
                                        }{" "}
                                      </div>
                                    </Col>
                                  )}
                                {(this.state.taskDetails.request_type == 43 || this.state.taskDetails.request_type == 23) && (this.state.taskDetails.first_name !==
                                  "" ||
                                  this.state.taskDetails.last_name !==
                                  "") && (
                                    <Col xs={12} sm={6} md={4}>
                                      <div className="form-group">
                                        <label>User Name:</label>{" "}
                                        {htmlDecode(
                                          this.state.taskDetails
                                            .first_name
                                        )}{" "}
                                        {htmlDecode(
                                          this.state.taskDetails.last_name
                                        )}
                                      </div>
                                    </Col>
                                  )}
                                {(this.state.taskDetails.request_type == 43 || this.state.taskDetails.request_type == 23) && this.state.taskDetails.parent_id == 0 && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Market:</label>{" "}
                                      {htmlDecode(
                                        this.state.taskDetails.country_name
                                      )}
                                    </div>
                                  </Col>
                                )}
                                {this.state.taskDetails.request_type == 23 && this.state.taskDetails.parent_id == 0 && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Request Category:</label>{" "}
                                      {this.setReqCategoryName(
                                        this.state.taskDetails
                                          .request_category
                                      )}
                                    </div>
                                  </Col>
                                )}
                                {(this.state.taskDetails.request_type == 43 || this.state.taskDetails.request_type == 23) && this.state.taskDetails.parent_id == 0 && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Product:</label>{" "}
                                      {htmlDecode(
                                        this.state.taskDetails.product_name
                                      )}
                                    </div>
                                  </Col>
                                )}
                                {(this.state.taskDetails.request_type == 43 || this.state.taskDetails.request_type == 23) && this.state.taskDetails.parent_id == 0 && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Quantity:</label>{" "}
                                      {this.state.taskDetails.quantity}
                                    </div>
                                  </Col>
                                )}
                                {this.state.taskDetails.request_type == 23 && this.state.taskDetails.parent_id == 0 && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Requested Date of Delivery:</label>{" "}
                                      {this.state.taskDetails.rdd !== null &&
                                        this.setDateOnly(this.state.taskDetails.rdd)}
                                    </div>
                                  </Col>
                                )}

                                {this.state.taskDetails.request_type === 43 && this.state.taskDetails.sales_order_no != null && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group forecast-image">
                                      <label>Sales Order No:</label>{" "}
                                      {ReactHtmlParser(htmlDecode(this.state.taskDetails.sales_order_no))}
                                    </div>
                                  </Col>
                                )}

                                {this.state.taskDetails.request_type === 43 && this.state.taskDetails.sales_order_date != null && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group forecast-image">
                                      <label>
                                        Sales Order Date
                                      </label>{" "}
                                      {ReactHtmlParser(htmlDecode(this.state.taskDetails.sales_order_date))}
                                    </div>
                                  </Col>
                                )}

                                {(this.state.taskDetails.request_type == 43 || this.state.taskDetails.request_type == 23) && this.state.taskDetails.po_no != null && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group forecast-image">
                                      <label>
                                        Purchase Order No.
                                      </label>{" "}
                                      {ReactHtmlParser(htmlDecode(this.state.taskDetails.po_no))}
                                    </div>
                                  </Col>
                                )}
                                {this.state.taskDetails.request_type === 43 && this.state.taskDetails.SAPDetails.length > 0 && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group forecast-image">
                                      <label>
                                        ShipTo Address
                                      </label>{" "}
                                      <div>{ReactHtmlParser(htmlDecode(this.state.taskDetails.SAPDetails.shipto_name))}
                                      </div>
                                      {ReactHtmlParser(htmlDecode(this.state.taskDetails.SAPDetails.street)) + ', '}
                                      {ReactHtmlParser(htmlDecode(this.state.taskDetails.SAPDetails.city)) + ', '}
                                      {ReactHtmlParser(htmlDecode(this.state.taskDetails.SAPDetails.post_code)) + ', '}
                                      {ReactHtmlParser(htmlDecode(this.state.taskDetails.SAPDetails.country)) + ', '}
                                    </div>
                                  </Col>
                                )}
                                {this.state.taskDetails.request_type === 43 && this.state.taskDetails.po_no != null && this.state.taskDetails.SAPDetails.po_file_date != null && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group forecast-image">
                                      <label>
                                        PO Uploaded Date
                                      </label>{" "}
                                      {ReactHtmlParser(htmlDecode(this.state.taskDetails.SAPDetails.po_file_date))}
                                    </div>
                                  </Col>
                                )}
                                {this.state.taskDetails.request_type === 43 && this.state.taskDetails.po_no != null && this.state.taskDetails.SAPDetails.po_file_name != null && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group forecast-image">
                                      <label>
                                        PO File
                                      </label>{" "}
                                      {ReactHtmlParser(htmlDecode(this.state.taskDetails.SAPDetails.po_file_name))}

                                    </div>
                                  </Col>
                                )}
                                {this.state.taskDetails.parent_id ===
                                  0 && (
                                    <Col xs={12} sm={12} md={12}>
                                      <div className="form-group forecast-image">
                                        <label>Requirement:</label>{" "}
                                        {this.state.taskDetails.language == 'en' ? ReactHtmlParser(htmlDecode(this.state.taskDetails.task_content)) : ReactHtmlParser(htmlDecode(this.state.taskDetails.content))}
                                      </div>
                                    </Col>
                                  )}

                                {this.state.taskDetails.ccp_posted_by > 0 && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Submitted By:</label>{" "}
                                      {this.state.taskDetails.emp_posted_by_first_name}{" "}{this.state.taskDetails.emp_posted_by_last_name
                                      }{" "}
                                      ({this.state.taskDetails.emp_posted_by_desig_name})
                                    </div>
                                  </Col>
                                )}

                                {this.state.taskDetails.submitted_by > 0 && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Submitted By:</label>{" "}
                                      {this.state.taskDetails.agnt_first_name}{" "}{this.state.taskDetails.agnt_last_name}{" "}
                                      (Agent)
                                    </div>
                                  </Col>
                                )}

                                {this.state.taskDetailsFiles.length >
                                  0 && (
                                    <Col xs={12}>
                                      <div className="mb-20">
                                        <div className="form-group">
                                          <label>
                                            Attachment
                                            {this.state.taskDetailsFiles.length > 1 ? "s" : ""} : {" "}
                                          </label>
                                          <div className="cqtDetailsDeta-btm">
                                            <ul className="conDocList">
                                              <>
                                                {this.state.taskDetailsFiles.map((file, k) => (
                                                  <li key={k}>
                                                    {process.env.NODE_ENV === "development" ? (
                                                      <LinkWithTooltip
                                                        href="#"
                                                        id="tooltip-1"
                                                      >
                                                        {file.actual_file_name}
                                                      </LinkWithTooltip>
                                                    ) : (
                                                      <LinkWithTooltip
                                                        href="#"
                                                        id="tooltip-1"
                                                      >
                                                        {file.actual_file_name}
                                                      </LinkWithTooltip>
                                                    )}
                                                  </li>
                                                ))}
                                              </>
                                            </ul>
                                          </div>
                                        </div>
                                      </div>
                                    </Col>
                                  )}

                                {this.state.taskDetailsFilesCoa.length > 0 && (
                                  <Col xs={12}>
                                    <div className="mb-20">
                                      <div className="form-group">
                                        <label>
                                          COA Attachment
                                          {this.state.taskDetailsFilesCoa.length > 1
                                            ? "s"
                                            : ""}
                                          :{" "}
                                        </label>
                                        <div className="cqtDetailsDeta-btm">
                                          <ul className="conDocList">
                                            {this.state.taskDetailsFilesCoa.map((file, k) => (
                                              <li key={k}>
                                                {process.env.NODE_ENV === "development" ? (
                                                  <LinkWithTooltip
                                                    href="#"
                                                    id="tooltip-1"
                                                    style={{ cursor: 'pointer' }}
                                                    clicked={(e) =>
                                                      this.redirectUrlTask(
                                                        e,
                                                        `${s3bucket_task_diss_path_coa}${file.coa_id}`
                                                      )
                                                    }
                                                  >
                                                    {file.actual_file_name}
                                                  </LinkWithTooltip>
                                                ) : (
                                                  <LinkWithTooltip
                                                    href="#"
                                                    id="tooltip-1"
                                                    style={{ cursor: 'pointer' }}
                                                    clicked={(e) =>
                                                      this.redirectUrlTask(
                                                        e,
                                                        `${s3bucket_task_diss_path_coa}${file.coa_id}`
                                                      )
                                                    }
                                                  >
                                                    {file.actual_file_name}
                                                  </LinkWithTooltip>
                                                )}
                                              </li>
                                            ))}
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                  </Col>
                                )}

                              </Row>
                            </div>
                          </div>
                        </div>
                      </div>

                      {this.state.commentExists && (
                        <div className="tab-pane" id="show_tab_2">
                          <div className="boxPapanel content-padding">
                            <div className="taskdetails">{this.getComments()}</div>
                          </div>
                        </div>
                      )}

                      {this.state.discussionExists &&
                        this.state.discussDetails.length > 0 && (
                          <div
                            className={`tab-pane ${this.state.discussionClass}`}
                            id="show_tab_3"
                          >
                            <div className="row">
                              <div className="col-xs-12">
                                <div className="nav-tabs-custom discussion-tab">
                                  <ul className="nav nav-tabs">
                                    <li
                                      className={`active`}
                                      onClick={(e) => this.handleTabs2(e)}
                                      id="d_tab_1"
                                    >
                                      Discussion Details
                                    </li>

                                    <li
                                      onClick={(e) => this.handleTabs2(e)}
                                      id="d_tab_2"
                                    >
                                      Files
                                    </li>
                                    {this.state.discussDetailsT.length > 0 && (
                                      <li className="flrght">
                                        <Switch
                                          checked={this.state.switchCheckedDiss}
                                          onChange={(e) => this.toggleDiscussion(e)}
                                          onColor="#7145d3"
                                          uncheckedIcon={"EN"}
                                          checkedIcon={
                                            ((this.state.taskDetails.parent_id === 0) ? this.state.taskDetails.language.toUpperCase() : this.state.taskDetails.parent_language.toUpperCase()) ===
                                              "ZH"
                                              ? "MA"
                                              : ((this.state.taskDetails.parent_id === 0) ? this.state.taskDetails.language.toUpperCase() : this.state.taskDetails.parent_language.toUpperCase())
                                          }
                                          className="react-switch"
                                          id="icon-switch"
                                        />
                                      </li>
                                    )}
                                  </ul>

                                  <div className="tab-content">
                                    <div
                                      className={`tab-pane ${((this.state.discussDetailsT.length > 0 &&
                                        this.state.taskDetails.is_spoc === false && this.state.taskDetails.parent_id === 0) || (this.state.taskDetails.parent_id > 0 && this.state.show_lang_diss === "EN"))
                                        ? ""
                                        : "active"
                                        }`}
                                      id="show_d_tab_1"
                                    >
                                      <section className="content-header">
                                        <h1>
                                          Discussion Details
                                          {this.state.taskDetails.customer_status === 3 && (
                                            <span
                                              style={{
                                                color: "#e6021d",
                                                fontSize: "12px",
                                                paddingLeft: "12px",
                                              }}
                                            >
                                              (Messages will not reach customer since
                                              the account is inactive)
                                            </span>
                                          )}
                                        </h1>
                                        {this.state.taskDetails.is_spoc === true &&
                                          this.state.taskDetails.parent_id === 0 &&
                                          this.state.show_lang_diss !== "EN" && (
                                            <span
                                              className="task-details-language"
                                              style={{ color: "rgb(0, 86, 179)" }}
                                            >
                                              The below message will be sent to the
                                              customer without any review.
                                            </span>
                                          )}
                                      </section>
                                      <section className="content">
                                        <div className="boxPapanel content-padding">
                                          <div className="disHead">
                                            <h3>
                                              {this.state.discussReqName} |{" "}
                                              {this.state.discussTaskRef}
                                            </h3>
                                          </div>

                                          <div className="comment-list-main-wrapper">
                                            {this.state.discussDetails.map(
                                              (comments, i) => (
                                                <div key={i} className="comment">
                                                  <div className="row">
                                                    <div className="col-md-9 col-sm-9 col-xs-12">
                                                      <div className="imageArea">
                                                        <div className="imageBorder">
                                                          {comments.added_by_type ===
                                                            "E" &&
                                                            comments.emp_profile_pic !=
                                                            "" &&
                                                            comments.emp_profile_pic !=
                                                            null ? (
                                                            <img
                                                              alt="noimage"
                                                              src={
                                                                comments.emp_profile_pic
                                                              }
                                                            />
                                                          ) : (
                                                            ""
                                                          )}

                                                          {comments.added_by_type ===
                                                            "C" &&
                                                            comments.cust_profile_pic !=
                                                            "" &&
                                                            comments.cust_profile_pic !=
                                                            null ? (
                                                            <img
                                                              alt="noimage"
                                                              src={
                                                                comments.cust_profile_pic
                                                              }
                                                            />
                                                          ) : (
                                                            ""
                                                          )}

                                                          {comments.added_by_type ===
                                                            "A" &&
                                                            comments.agnt_profile_pic !=
                                                            "" &&
                                                            comments.agnt_profile_pic !=
                                                            null ? (
                                                            <img
                                                              alt="noimage"
                                                              src={
                                                                comments.agnt_profile_pic
                                                              }
                                                            />
                                                          ) : (
                                                            ""
                                                          )}

                                                          {(comments.emp_profile_pic ==
                                                            "" ||
                                                            comments.emp_profile_pic ==
                                                            null) &&
                                                            (comments.cust_profile_pic ==
                                                              "" ||
                                                              comments.cust_profile_pic ==
                                                              null) &&
                                                            (comments.agnt_profile_pic ==
                                                              "" ||
                                                              comments.agnt_profile_pic ==
                                                              null) && (
                                                              <img
                                                                alt="noimage"
                                                                src={commenLogo}
                                                              />
                                                            )}
                                                        </div>
                                                      </div>
                                                      <div className="conArea">
                                                        {comments.added_by_type ===
                                                          "C" && (
                                                            <p>{`${comments.cust_fname} ${comments.cust_lname}`}</p>
                                                          )}

                                                        {comments.added_by_type ===
                                                          "E" && (
                                                            <p>
                                                              {`${comments.emp_fname} ${comments.emp_lname} (DRL)`}{" "}
                                                            </p>
                                                          )}

                                                        {comments.added_by_type ===
                                                          "A" && (
                                                            <p>{`${comments.agnt_first_name} ${comments.agnt_last_name} (Agent)`}</p>
                                                          )}
                                                        <span>
                                                          {ReactHtmlParser(
                                                            ReactHtmlParser(
                                                              comments.comment
                                                            )
                                                          )}
                                                        </span>
                                                        {this.getDiscussionFile(
                                                          comments.uploads,
                                                          comments.multi
                                                        )}
                                                      </div>
                                                      <div className="clearfix" />
                                                    </div>

                                                    <div className="col-md-3 col-sm-3 col-xs-12 text-right">

                                                      <div className="dateArea">
                                                        <p>
                                                          {dateFormat(
                                                            localDate(
                                                              comments.date_added
                                                            ),
                                                            "ddd, mmm dS, yyyy"
                                                          )}
                                                        </p>


                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              )
                                            )}
                                          </div>
                                        </div>
                                      </section>
                                    </div>

                                    <div className="tab-pane" id="show_d_tab_2">
                                      <section className="content-header">
                                        <h1>File Details</h1>
                                      </section>
                                      <section className="content">
                                        <div className="boxPapanel content-padding">
                                          <div className="disHead">

                                          </div>

                                          <div className="comment-list-main-wrapper">
                                            {this.state.discussFile &&
                                              this.state.discussFile.length > 0
                                              ? this.state.discussFile.map(
                                                (file, i) => (
                                                  <div key={i} className="comment">
                                                    <div className="row">
                                                      <div className="col-md-10 col-sm-10 col-xs-12">
                                                        <div className="imageArea">
                                                          <div className="imageBorder">
                                                            <img
                                                              alt="noimage"
                                                              src={commenLogo1}
                                                            />
                                                          </div>
                                                        </div>
                                                        <div className="conArea">

                                                          {process.env.NODE_ENV ===
                                                            "development" ? (
                                                            <LinkWithTooltip
                                                              tooltip={`${file.new_file_name}`}
                                                              href="#"
                                                              id="tooltip-1"
                                                            >
                                                              {
                                                                file.actual_file_name
                                                              }
                                                            </LinkWithTooltip>
                                                          ) : (
                                                            <LinkWithTooltip
                                                              tooltip={`${file.new_file_name}`}
                                                              href="#"
                                                              id="tooltip-1"
                                                            >
                                                              {
                                                                file.actual_file_name
                                                              }
                                                            </LinkWithTooltip>
                                                          )}
                                                        </div>
                                                        <div className="clearfix" />
                                                      </div>

                                                      <div className="col-md-2 col-sm-2 col-xs-12">
                                                        <div className="dateArea">
                                                          <p>
                                                            {dateFormat(
                                                              localDate(
                                                                file.date_added
                                                              ),
                                                              "ddd, mmm dS, yyyy"
                                                            )}
                                                          </p>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                )
                                              )
                                              : `No Files Found`}
                                          </div>
                                        </div>
                                      </section>
                                    </div>

                                    {this.state.discussDetailsT.length > 0 && (
                                      <div
                                        className={`tab-pane ${((this.state.taskDetails.parent_id === 0 && this.state.taskDetails.is_spoc === true) || (this.state.taskDetails.parent_id > 0 && this.state.show_lang_diss !== "EN"))
                                          ? ""
                                          : "active"
                                          }`}
                                        id="show_d_tab_3"
                                      >
                                        <section className="content-header">
                                          <h1>
                                            Discussion Details
                                            {this.state.taskDetails.customer_status === 3 && (
                                              <span
                                                style={{
                                                  color: "#e6021d",
                                                  fontSize: "12px",
                                                  paddingLeft: "12px",
                                                }}
                                              >
                                                (Messages will not reach customer
                                                since the account is inactive)
                                              </span>
                                            )}
                                          </h1>
                                        </section>
                                        <section className="content">
                                          <div className="boxPapanel content-padding">
                                            <div className="disHead">
                                              <h3>
                                                {this.state.discussReqNameT} |{" "}
                                                {this.state.discussTaskRefT}
                                              </h3>
                                            </div>

                                            <div className="comment-list-main-wrapper">
                                              {this.state.discussDetailsT.map(
                                                (comments, i) => (
                                                  <div key={i} className="comment">
                                                    <div className="row">
                                                      <div className="col-md-9 col-sm-9 col-xs-12">
                                                        <div className="imageArea">
                                                          <div className="imageBorder">
                                                            {comments.added_by_type ===
                                                              "E" &&
                                                              comments.emp_profile_pic !=
                                                              "" &&
                                                              comments.emp_profile_pic !=
                                                              null ? (
                                                              <img
                                                                alt="noimage"
                                                                src={
                                                                  comments.emp_profile_pic
                                                                }
                                                              />
                                                            ) : (
                                                              ""
                                                            )}

                                                            {comments.added_by_type ===
                                                              "C" &&
                                                              comments.cust_profile_pic !=
                                                              "" &&
                                                              comments.cust_profile_pic !=
                                                              null ? (
                                                              <img
                                                                alt="noimage"
                                                                src={
                                                                  comments.cust_profile_pic
                                                                }
                                                              />
                                                            ) : (
                                                              ""
                                                            )}

                                                            {comments.added_by_type ===
                                                              "A" &&
                                                              comments.agnt_profile_pic !=
                                                              "" &&
                                                              comments.agnt_profile_pic !=
                                                              null ? (
                                                              <img
                                                                alt="noimage"
                                                                src={
                                                                  comments.agnt_profile_pic
                                                                }
                                                              />
                                                            ) : (
                                                              ""
                                                            )}

                                                            {(comments.emp_profile_pic ==
                                                              "" ||
                                                              comments.emp_profile_pic ==
                                                              null) &&
                                                              (comments.cust_profile_pic ==
                                                                "" ||
                                                                comments.cust_profile_pic ==
                                                                null) &&
                                                              (comments.agnt_profile_pic ==
                                                                "" ||
                                                                comments.agnt_profile_pic ==
                                                                null) && (
                                                                <img
                                                                  alt="noimage"
                                                                  src={commenLogo}
                                                                />
                                                              )}
                                                          </div>
                                                        </div>
                                                        <div className="conArea">
                                                          {comments.added_by_type ===
                                                            "C" && (
                                                              <p>{`${comments.cust_fname} ${comments.cust_lname}`}</p>
                                                            )}

                                                          {comments.added_by_type ===
                                                            "E" && (
                                                              <p>
                                                                {`${comments.emp_fname} ${comments.emp_lname} (DRL)`}{" "}
                                                              </p>
                                                            )}

                                                          {comments.added_by_type ===
                                                            "A" && (
                                                              <p>{`${comments.agnt_first_name} ${comments.agnt_last_name} (Agent)`}</p>
                                                            )}
                                                          <span>
                                                            {ReactHtmlParser(
                                                              ReactHtmlParser(
                                                                comments.comment
                                                              )
                                                            )}
                                                          </span>
                                                          {this.getDiscussionFile(
                                                            comments.uploads,
                                                            comments.multi
                                                          )}
                                                        </div>
                                                        <div className="clearfix" />
                                                      </div>

                                                      <div className="col-md-3 col-sm-3 col-xs-12 text-right">
                                                        <div className="dateArea">
                                                          <p>
                                                            {dateFormat(
                                                              localDate(
                                                                comments.date_added
                                                              ),
                                                              "ddd, mmm dS, yyyy"
                                                            )}
                                                          </p>


                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                )
                                              )}
                                            </div>

                                          </div>
                                        </section>
                                      </div>
                                    )}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        )}

                      <div className="tab-pane" id="show_tab_4">
                        <div className="boxPapanel content-padding">
                          <div className="taskdetails">{this.activityLogTable()}</div>
                        </div>
                      </div>
                    </div>
                  </section>

                  {this.state.shippingInfo.length > 0 ?
                    <>
                      <section className="content track-section">
                        <div className="boxPapanel content-padding">
                          {/* Accordion */}
                          <h2>Track Shipment Information</h2>
                          <Accordion allowZeroExpanded>
                            {this.state.shippingInfo.map((value, index) => {
                              return (
                                <AccordionItem>
                                  <AccordionItemHeading>
                                    <AccordionItemButton>
                                      Invoice #{Object.keys(this.state.shippingInfo[index])}
                                    </AccordionItemButton>
                                  </AccordionItemHeading>
                                  <AccordionItemPanel>
                                    <Row>
                                      <Col sm="12">
                                        {this.state.taskDetailsFilesCoa.length > 0 &&

                                          <div className="row coaDocumentsHead">
                                            <div className="col-md-2"><label className="custom-label"><strong> COA Document </strong></label></div>
                                            <div className="col-md-10 documentsFile">
                                              {this.state.taskDetailsFilesCoa.map((valuecoa, indexcoa) => {
                                                if (valuecoa.invoice_number == Object.keys(this.state.shippingInfo[index])) {
                                                  return (<span className="">
                                                    <a href={s3bucket_task_diss_path_coa + valuecoa.coa_id} target={'_blank'}>
                                                      {valuecoa.actual_file_name}{" "}
                                                      <img src={downloadIcon} width="28" height="28" /></a>
                                                  </span>)
                                                }
                                              })
                                              }
                                            </div>
                                          </div>
                                        }
                                        {this.state.shippingInfo[index][Object.keys(this.state.shippingInfo[index])].partner == 'bluedart' ? this.getBlueDartDynamicContent(index, this.state.shippingInfo) : this.getDynamicContent(index, this.state.shippingInfo)}
                                      </Col>
                                    </Row>
                                  </AccordionItemPanel>
                                </AccordionItem>
                              )
                            })}
                          </Accordion>
                        </div>
                      </section>
                    </> : ""
                  }

                  {this.state.shippingSTOInfo.length > 0 ?
                    <>
                      <section id="shipping_section" className="content track-section" style={{ minHeight: '200px' }}>
                        <div className="boxPapanel content-padding">
                          {/* Accordion */}
                          <h2>STO Invoice Information</h2>
                          <Accordion allowZeroExpanded>
                            {this.state.shippingSTOInfo.map((value, index) => {
                              return (
                                <AccordionItem>
                                  <AccordionItemHeading>
                                    <AccordionItemButton>
                                      Invoice #{Object.keys(this.state.shippingSTOInfo[index])}
                                    </AccordionItemButton>
                                  </AccordionItemHeading>
                                  <AccordionItemPanel>
                                    <Row>
                                      <Col sm="12">
                                        {this.state.taskDetailsFilesSTOCoa.length > 0 &&

                                          <div className="row coaDocumentsHead">
                                            <div className="col-md-2"><label className="custom-label"><strong> COA Document </strong></label></div>
                                            <div className="col-md-10 documentsFile">
                                              {this.state.taskDetailsFilesSTOCoa.map((valuecoa, indexcoa) => {
                                                if (valuecoa.invoice_number == Object.keys(this.state.shippingSTOInfo[index])) {
                                                  return (<span className="">
                                                    <a href={s3bucket_task_diss_path_coa + valuecoa.coa_id} target={'_blank'}> {valuecoa.actual_file_name}  <img src={downloadIcon} width="28" height="28" /></a>
                                                  </span>)
                                                }
                                              })
                                              }
                                            </div>
                                          </div>
                                        }
                                        {this.state.shippingSTOInfo[index][Object.keys(this.state.shippingSTOInfo[index])].partner == 'bluedart' ? this.getBlueDartDynamicContent(index, this.state.shippingSTOInfo) : this.getDynamicContent(index, this.state.shippingSTOInfo)}
                                      </Col>
                                    </Row>
                                  </AccordionItemPanel>
                                </AccordionItem>
                              )
                            })}
                          </Accordion>
                        </div>
                      </section>
                    </> : ""
                  }

                </div>
              </div>
            </section>
          </div>

          <Modal show={this.state.showModal} onHide={() => this.modalCloseHandler()} backdrop="static" dialogClassName="profile-popup">
            <Modal.Header closeButton>
              <Modal.Title>Change Password</Modal.Title>
            </Modal.Header>

            <Modal.Body>
              <div className="contBox">
                <Row>
                  <Col xs={12} sm={12} md={12}>
                    <div className="profile">
                      <Formik
                        validationSchema={validatePaaword}
                        onSubmit={this.handleSubmitEvent}
                      >
                        {({ values, errors, touched, isValid, isSubmitting }) => {
                          return (
                            <Form>

                              <Row>
                                <Col xs={12} sm={12} md={12}>
                                  <div className="form-group">
                                    <label className="pull-left" style={{ float: "left" }}>Password</label>
                                    <Field
                                      name="password"
                                      type="password"
                                      className={`form-control`}
                                      placeholder="Enter password"
                                      autoComplete="off"
                                    />
                                    {errors.password && touched.password ? (
                                      <span className="errorMsg">
                                        {errors.password}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              </Row>
                              <Row>
                                <Col xs={12} sm={12} md={12}>
                                  <div className="form-group">
                                    <label className="pull-left">Confirm Password</label>
                                    <Field
                                      name="confirm_password"
                                      type="password"
                                      className={`form-control`}
                                      placeholder="Enter confirm password"
                                      autoComplete="off"
                                    />
                                    {errors.confirm_password &&
                                      touched.confirm_password ? (
                                      <span className="errorMsg">
                                        {errors.confirm_password}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              </Row>
                              <div className="button-footer btn-toolbar">
                                <button
                                  type="button"
                                  className="btn btn-success btn-sm"
                                  onClick={() => this.modalCloseHandler()}
                                >
                                  Cancel
                                </button>
                                <button
                                  className={`btn btn-danger btn-sm ${isValid ? "btn-custom-green" : "btn-disable"
                                    } m-r-10`}
                                  type="submit"
                                  disabled={isValid ? false : true}
                                >
                                  {isSubmitting ? "Updating..." : "Submit"}
                                </button>
                              </div>

                            </Form>
                          );
                        }}
                      </Formik>
                    </div>
                  </Col>
                </Row>
              </div>
            </Modal.Body>
          </Modal>
        </>
      )
    } else {
      if (this.state.invalid_access) {
        return <></>;
      } else {
        return (
          <>
            <div className="loderOuter">
              <div className="loader">
                <img src={loaderlogo} alt="logo" />
                <div className="loading">Loading...</div>
              </div>
            </div>
          </>
        );
      }
    }


  }
}

export default TasksDetails;