import React, { Component } from "react";
import { Row, Col, Tooltip, OverlayTrigger, Modal } from "react-bootstrap";
import { Link } from "react-router-dom";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import Pagination from "react-js-pagination";
import dateFormat from 'dateformat';
import swal from "sweetalert";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";

import API from "../../shared/axios_qa";
import { showErrorMessage } from "../../shared/handle_error_qa";
import { htmlDecode } from "../../shared/helper";
import whitelogo from '../../assets/images/drreddylogo_white.png';

const s3bucket_task_coa_path = `${process.env.REACT_APP_API_URL}/api/feed/download_task_coa/`;

const validatePaaword = Yup.object().shape({
  password: Yup.string().trim()
    .required("Please enter your new password")
    .min(8, "Password must be at least 8 characters long"),
  confirm_password: Yup.string().trim()
    .required("Please enter your confirm password")
    .test("match", "Confirm password do not match", function (
      confirm_password
    ) {
      return confirm_password === this.parent.password;
    })
});

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href || ""} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

const actionFormatter = refObj => cell => {
  const current_coa = refObj.state.coaData.filter((data) => cell == data.coa_id);
  return (
    <div className="actionStyle">
      {current_coa[0].coa_file_path != '' ?
        <LinkWithTooltip
          tooltip="Click to Download"
          href="#"
          clicked={(e) =>
            refObj.redirectUrlTask(
              e,
              `${s3bucket_task_coa_path}${cell}`
            )
          }
          id="tooltip-1"
        >
          <i className="fas fa-download" />
        </LinkWithTooltip>
        : null}
    </div>
  );
};


class Uploads extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      count: 0,
      itemPerPage: 20,
      activePage: 1,
      showModal: false,
      search_customer_id: '',
      search_batch_number: '',
      remove_search: false,
    };
  }

  componentDidMount() {
    document.title = `QA Uploads | Dr.Reddys API`;
    this.getUploadsList();
  }

  getUploadsList(page = 1) {

    const { search_customer_id, search_batch_number } = this.state;

    API.get(`/api/tasks/qa_uploads/?page=${page}&customer_id=${encodeURIComponent(
          search_customer_id
      )}&batch_number=${encodeURIComponent(
          search_batch_number
      )}`)
      .then((res) => {
        this.setState({
          coaData: res.data.data,
          count: Number(res.data.count),
          isLoading: false,
        });
      })
      .catch((err) => {
        this.setState({
          isLoading: false,
        });
        // showErrorMessage(err, this.props);
      });
  }

  _htmlDecode = (refObj) => (cell) => {
    return htmlDecode(cell);
  };

  setDate = (refObj) => (date) => {
    console.log("data", date);
    if (date != '' && date != null) {
      var mydate = new Date(date);
      return dateFormat(mydate, "dd-mm-yyyy HH:MM:ss");
    } else {
      return '-';
    }
  }

  redirectUrlTask = (event, path) => {
    event.preventDefault();
    window.open(path, "_blank");
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getUploadsList(pageNumber > 0 ? pageNumber : 1);
  };

  modalShowHandler = () => {
    this.setState({ showModal: true });
  };
  modalCloseHandler = () => {
    this.setState({ showModal: false });
  };

  handleSubmitEvent = (values, actions) => {
    const post_data = {
      password: values.password
    };

    API.put(`/api/employees/qa/changepassword`, post_data)
      .then(res => {
        this.modalCloseHandler();
        console.log("response", res);
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: "Password updated successfully.",
          icon: "success"
        }).then(() => {
          window.location.reload();
        });
      })
      .catch(err => {
        actions.setErrors(err.data.errors);
        actions.setSubmitting(false);
      });
  };


  handleSearch = (e) => {
    e.preventDefault();
    const {
        search_customer_id,
        search_batch_number,
    } = this.state;

    if (
        search_customer_id === '' &&
        search_batch_number === ''
    ) {
        return false;
    }

    API
        .get(
            `/api/tasks/qa_uploads/?page=1&customer_id=${encodeURIComponent(
                search_customer_id
            )}&batch_number=${encodeURIComponent(
                search_batch_number
            )}`
        )
        .then((res) => {
            this.setState({
                coaData: res.data.data,
                count: Number(res.data.count),
                remove_search: true,
                activePage: 1,
            });
        })
        .catch((err) => {
            // showErrorMessage(err, this.props);
            console.log("err",err);
        });
  };

  clearSearch = () => {
      this.setState(
          {
              search_customer_id: '',
              search_batch_number: '',
              remove_search: false,
          },
          () => {
              this.getUploadsList();
              this.setState({ activePage: 1 });
          }
      );
  };

  render() {

    const { search_customer_id, search_batch_number } = this.state;
    if (this.state.isLoading) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy" >
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <>
          <div className="content-wrapper qaWrapper logisticsOrder">
            {/* <section className="content-header">
                <div className="row">
                <div className="col-lg-9 col-sm-6 col-xs-12">
                    <h1>
                    Uploads
                    <small />
                    </h1>
                </div>
                <div className="col-lg-12 col-sm-12 col-xs-12 topSearchSection">
                    <div className="">
                    <button
                        type="button"
                        className="btn btn-info btn-sm"
                        onClick={(e) => this.modalShowHandler(e)}
                    >
                        <i className="fas fa-plus m-r-5" /> Add 
                    </button>
                    </div> 
                    <div className="clearfix"></div>
                </div>
                </div>
            </section>*/}

            <section className="content-header">
              <div className="row">
                <div className="col-lg-12 col-sm-6 col-xs-12">
                  <h1>Uploads</h1>
                  <div className="customer-password-button btn-fill bdrRadius" onClick={e => this.modalShowHandler()}>
                    Change Password
                  </div>
                  <div className="customer-edit-button btn-fill bdrRadius" onClick={() => { localStorage.clear(); this.props.history.push("/qa_login"); }}
                  >
                    Log Out
                  </div>
                </div>
                <div className="col-lg-12 col-sm-12 col-xs-12 topSearchSection">
                  <div className="clearfix"></div>
                </div>
              </div>
            </section>
            <section className="content">
              <div className='row'>
                  <div className='col-lg-12 col-sm-12 col-xs-12 topSearchSection'>
                      <form className='form'>
                          <div className='clearfix serchapanel mb-0'>
                              <ul>
                                  <li>
                                      <div className=''>
                                          <input
                                              className='form-control'
                                              name='customer_id'
                                              id='customer_id'
                                              placeholder='Search Customer ID'
                                              value={search_customer_id}
                                              onChange={(e) =>
                                                  this.setState({ search_customer_id: e.target.value })
                                              }
                                          />
                                      </div>
                                  </li>
                                  <li>
                                      <div className=''>
                                          <input
                                              className='form-control'
                                              name='batch_number'
                                              id='batch_number'
                                              placeholder='Search Batch Number'
                                              value={search_batch_number}
                                              onChange={(e) =>
                                                  this.setState({
                                                      search_batch_number: e.target.value,
                                                  })
                                              }
                                          />
                                      </div>
                                  </li>
                                  <li>
                                      <div className=''>
                                          <input
                                              type='submit'
                                              value='Search'
                                              className='btn btn-warning btn-sm orderListSearchBar'
                                              onClick={(e) => this.handleSearch(e)}
                                          />
                                          {this.state.remove_search ? (
                                              <a
                                                  onClick={() => this.clearSearch()}
                                                  className='btn btn-danger btn-sm orderListRemoveBtn'
                                              >
                                                  {' '}
                                                  Remove{' '}
                                              </a>
                                          ) : null}
                                      </div>
                                  </li>
                              </ul>
                          </div>
                      </form>
                  </div>
              </div>
              <div className="serchapanel">                        
                <div className="box-body">
                  {/* //Table */}
                  <BootstrapTable data={this.state.coaData}>
                    <TableHeaderColumn
                      isKey
                      tdStyle={{ whiteSpace: "normal" }}
                      dataField="customer_id"
                      dataFormat={this._htmlDecode(this)}
                    >
                      Customer ID
                    </TableHeaderColumn>

                    <TableHeaderColumn
                      dataField="batch_no"
                      tdStyle={{ whiteSpace: "normal" }}
                      dataFormat={this._htmlDecode(this)}
                    >
                      Batch Number
                    </TableHeaderColumn>

                    <TableHeaderColumn
                      dataField="plant_id"
                    >
                      Plant
                    </TableHeaderColumn>

                    <TableHeaderColumn
                      dataField="emp_full_name"
                    >
                      Posted By
                    </TableHeaderColumn>

                    <TableHeaderColumn
                      dataField="date_added"
                      dataFormat={this.setDate(this)}
                    >
                      Posted On
                    </TableHeaderColumn>

                    <TableHeaderColumn
                      dataField="file_name"
                    >
                      File
                    </TableHeaderColumn>

                    <TableHeaderColumn
                      dataField="coa_id"
                      dataFormat={actionFormatter(this)}
                    >
                      Action
                    </TableHeaderColumn>
                  </BootstrapTable>
                </div>
              </div>
              {this.state.count > this.state.itemPerPage ? (
              <Row>
                <Col md={12}>
                  <div className="paginationOuter text-right">
                    <Pagination
                      activePage={this.state.activePage}
                      itemsCountPerPage={this.state.itemPerPage}
                      totalItemsCount={this.state.count}
                      itemClass='page-item'
                      linkClass='page-link'
                      activeClass='active'
                      onChange={this.handlePageChange}
                    />
                  </div>
                </Col>
              </Row>
            ) : null}
            </section>
          </div>

          <Modal show={this.state.showModal} onHide={() => this.modalCloseHandler()} backdrop="static" dialogClassName="profile-popup">
            <Modal.Header closeButton>
              <Modal.Title>Change Password</Modal.Title>
            </Modal.Header>

            <Modal.Body>
              <div className="contBox">
                <Row>
                  <Col xs={12} sm={12} md={12}>
                    <div className="profile">
                      <Formik
                        validationSchema={validatePaaword}
                        onSubmit={this.handleSubmitEvent}
                      >
                        {({ values, errors, touched, isValid, isSubmitting }) => {
                          return (
                            <Form>

                              <Row>
                                <Col xs={12} sm={12} md={12}>
                                  <div className="form-group">
                                    <label className="pull-left" style={{ float: "left" }}>Password</label>
                                    <Field
                                      name="password"
                                      type="password"
                                      className={`form-control`}
                                      placeholder="Enter password"
                                      autoComplete="off"
                                    />
                                    {errors.password && touched.password ? (
                                      <span className="errorMsg">
                                        {errors.password}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              </Row>
                              <Row>
                                <Col xs={12} sm={12} md={12}>
                                  <div className="form-group">
                                    <label className="pull-left">Confirm Password</label>
                                    <Field
                                      name="confirm_password"
                                      type="password"
                                      className={`form-control`}
                                      placeholder="Enter confirm password"
                                      autoComplete="off"
                                    />
                                    {errors.confirm_password &&
                                      touched.confirm_password ? (
                                      <span className="errorMsg">
                                        {errors.confirm_password}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              </Row>
                              <div className="button-footer btn-toolbar">
                                <button
                                  type="button"
                                  className="btn btn-success btn-sm"
                                  onClick={() => this.modalCloseHandler()}
                                >
                                  Cancel
                                </button>
                                <button
                                  className={`btn btn-danger btn-sm ${isValid ? "btn-custom-green" : "btn-disable"
                                    } m-r-10`}
                                  type="submit"
                                  disabled={isValid ? false : true}
                                >
                                  {isSubmitting ? "Updating..." : "Submit"}
                                </button>
                              </div>

                            </Form>
                          );
                        }}
                      </Formik>
                    </div>
                  </Col>
                </Row>
              </div>
            </Modal.Body>
          </Modal>
        </>
      );
    }
  }
}

export default Uploads;
