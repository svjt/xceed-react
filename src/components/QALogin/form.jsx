import React, { Component } from "react";
import { Link } from "react-router-dom";
import "react-datepicker/dist/react-datepicker.css";
import closeIcon from "../../assets/images/times-solid-red.svg";
import closeIconSelect from "../../assets/images/close.svg";
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
import Select from "react-select";
import swal from "sweetalert";
import dateFormat from "dateformat";

import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Modal,
  Alert,
  OverlayTrigger
} from "react-bootstrap";

import axios from "../../shared/axios_qa";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import {
  htmlDecode,
  supportedFileType,
  BasicUserData,
  trimString,
} from "../../shared/helper";
import Dropzone from "react-dropzone";
import { connect } from "react-redux";
import { isMobile } from "react-device-detect";

var path = require("path");
var cc_Customer = [];

var minDate = "";
var maxDate = "";
//validation
let initialValues = {
  customer_name: "",
  batch_number: "",
  material_id: "",
  customer_id: "",
  lot_no: "",
  file: "",
  id : [],
  plant_code : '',
  file_name: [],
  showModal : false,
};

const validationSchema = (refObj) =>
  Yup.object().shape({
    customer_name: Yup.string(),

    batch_number: Yup.string().required(
      'Batch number is required.'
    ),
    material_id: Yup.string().required(
      'Material number is required.'
    ),
    customer_id: Yup.string().required(
      'Customer ID is required.'
    ),

    lot_no: Yup.string(),
    file_name: Yup.string()
      // .required("Please upload determination file")
      // .test(
      //   "required",
      //   'Please attach COA doc.',
      //   function (value) {
      //     return value ? true : false;
      //   }
      // ),
      .required("COA doc is required. Only .pdf file is allowed")
      .test(
        "file_name",
        refObj.state.fileErrText,
        () => refObj.state.isValidFile
      )
      .test("filecount","Maximum 5 files are allowed",
      ()=> refObj.state.files.length <= 5),
      plant_code :Yup.string().required(
        'Plant code is required.'
      ), 

  });
 
  const validatePaaword = Yup.object().shape({
    password: Yup.string().trim()
      .required("Please enter your new password")
      .min(8, "Password must be at least 8 characters long"),
    confirm_password: Yup.string().trim()
      .required("Please enter your confirm password")
      .test("match", "Confirm password do not match", function(
        confirm_password
      ) {
        return confirm_password === this.parent.password;
      })
  });

  const removeDropZoneFiles = (fileName, objRef, setErrors, setFieldValue) => {
    var newArr = [];
    for (let index = 0; index < objRef.state.files.length; index++) {
      const element = objRef.state.files[index];
  
      if (fileName === element.name) {
      } else {
        newArr.push(element);
      }
    }
  
    var fileListHtml = newArr.map((file) => (
      <>
        <span onClick={() => removeDropZoneFiles(file.name, objRef, setErrors, setFieldValue)}>
          <i className="far fa-times-circle"></i>
        </span>{" "}
        {file.name}
      </>
    ));
    setErrors({ file_name: "" });
    setFieldValue("file_name", newArr);
    objRef.setState({
      isValidFile: true,
      files: newArr,
      filesHtml: fileListHtml,
    });
  };

class QAForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: "",
      tooltipOpen: false,
      showLoader: false,
      selectedOption: null,
      share_with_agent: true,
      message: null,
      products: [],
      units: [],
      countryList: [],
      selectedCountry: null,
      files: [],
      filesHtml: "",
      PlantList: [],
      rejectedFile: [],
      cc_Customers: [],
      selectedUnit: "",
      selectedDate: "",
      errMsg: null,
      customerList: [],
      ccAgentCustomerList: [],
      pre_selected: [],
      selUser: null,
      displayCCList: false,
      agent_customer_id: null,
      isValidFile: false,
      fileErrText: ""
    };
    this.inputRef = React.createRef();
  }

  deleteFile = (e, setFieldValue) => {
    this.inputRef.current.value = null; // clear input value - SATYAJIT
    e.preventDefault();
    var tempArray = [];

    this.state.files.map((file, fileIndex) => {
      if (file.checked === true) {
      } else {
        tempArray.push(file);
      }
    });

    setFieldValue("file_name", tempArray);

    this.setState({ files: tempArray });
  };

  componentDidMount() {
    axios
      .get("/api/tasks/coa_plant_list")
    .then(async (res) => {
      if(res.data.status == 1  && res.data.data.length > 0){
        //console.log("+++++++data : ",res.data.data);
       
        let temp = [];
        for(let i = 0 ; i < res.data.data.length ; i++){
         // console.log("+++++++data : ",res.data.data[i]);
          temp.push({
            'value' : res.data.data[i].id,
            'label' : res.data.data[i].plant_code});

        }
        this.setState({
          PlantList: temp
        });

      }

    })
    .catch((err) => {
      this.setState({
        isLoading: false,
      });
      //showErrorMessage(err, this.props);
    });
    this.setState({ files: [], filesHtml: "" });

    document.title = `QA FORM | Dr.Reddys API`;

    if (localStorage.qa_token == null || typeof localStorage.qa_token == 'undefined') {
      this.props.history.push("/qa_login");
    }
  }

  fileChangedHandler = (event) => {
    this.setState({ upload_file: event.target.files[0] });
  };
  
  setDropZoneFiles = (acceptedFiles, setErrors, setFieldValue, setFieldTouched,errors) => {
    var rejectedFiles = [];
    var uploadFile = [];

    setErrors({ file_name: false });
    setFieldValue(this.state.files);

    var prevFiles = this.state.files;
    var newFiles;
    
    if (prevFiles.length > 0) {
      for (let index = 0; index < acceptedFiles.length; index++) {
        var remove = 0;

        for (let index2 = 0; index2 < prevFiles.length; index2++) {
          if (acceptedFiles[index].name === prevFiles[index2].name) {
            remove = 1;
            break;
          }
        }
        if (remove === 0) {
          prevFiles.push(acceptedFiles[index]);
        }
      }
      newFiles = prevFiles;
    } else {
      newFiles = acceptedFiles;
    }
     
    const totalfile = newFiles.length;
    for (let index = 0; index < totalfile; index++) {
      var error = 0;
      var filename = newFiles[index].name.toLowerCase();
      var extension_list = ["pdf"];
      var ext_with_dot = path.extname(filename);
      var file_extension = ext_with_dot.split(".").join("");

      var obj = {};

      var fileErrText = `Only files with the following extensions are allowed: pdf.`;

      if (extension_list.indexOf(file_extension) === -1) {
        error = error + 1;

        // let str_1 = `One or more files could not be uploaded.The specified file [file_name] could not be uploaded.`;
        // let str_2 = `The specified file [file_name] could not be uploaded.`;
           let str_1 = "";
           let str_2 = "";
        if (totalfile > 1) {
          if (index === 0) {
            obj["errorText"] =
              str_1.replace(
                "[file_name]",
                filename
              ) + fileErrText;
          } else {
            obj["errorText"] =
              str_2.replace(
                "[file_name]",
                filename
              ) + fileErrText;
          }
        } else {
          obj["errorText"] =
            str_2.replace(
              "[file_name]",
              filename
            ) + fileErrText;
        }
        rejectedFiles.push(obj);
      }

      if (newFiles[index].size > 50000000) {
        let err_txt = `The file [file_name] could not be saved because it exceeds 50 MB, the maximum allowed size for uploads.`;
        obj[
          "errorText"
        ] = err_txt.replace(
          "[file_name]",
          filename
        );
        error = error + 1;
        rejectedFiles.push(obj);
      }

      if (error === 0) {
        uploadFile.push(newFiles[index]);
      }else{
        // setErrors({file_name: obj.errorText}); // This wont work as re render will clear it
        this.setState({isValidFile: false, fileErrText: obj.errorText});
      }
    }
    
    if(rejectedFiles.length === 0){
      setErrors({ file_name: false });
      this.setState({isValidFile: true, fileErrText: ""});
    }

    // //setErrors({ file_name: false });
    // errors.file_name = [];
    //setFieldValue(this.state.files);


    // this.setState({
    //   files: uploadFile,
    //   // filesHtml: fileListHtml
    // });
    // console.log("newFiles", acceptedFiles);

    setFieldValue("file_name", newFiles);
    setFieldTouched("file_name");

    this.setState({
      rejectedFile: rejectedFiles,
    });


    
    var fileListHtml = newFiles.map((file) => (
      <>
        <span onClick={() => {
            removeDropZoneFiles(file.name, this, setErrors, setFieldValue)}
          }
          >
          <i className="far fa-times-circle"></i>
        </span>{" "}
        {trimString(25, file.name)}
      </>
    ));

    this.setState({
      files: uploadFile,
      filesHtml: fileListHtml,
    });
  };

  handleSubmit = (values, actions) => {
    this.setState({ showLoader: true });

    this.setState({ message: "" });

    this.setState({ errMsg: "" });

    var formData = new FormData();


    //alert(JSON.stringify(values))
    if (this.state.files && this.state.files.length > 0) {
      for (let index = 0; index < this.state.files.length; index++) {
        const element = this.state.files[index];
        
        formData.append("file", element);
      }
    } else {
      formData.append("file", []);
    }

    formData.append("customer_name", values.customer_name);
    formData.append("batch_number", values.batch_number);
    formData.append("lot_no", values.lot_no);
    formData.append("material_id", values.material_id);
    formData.append("customer_id", values.customer_id);
    formData.append("plant", values.plant_code);


    axios
      .post("/api/tasks/qa_form", formData)
      .then((res) => {
        this.setState({
          showLoader: false
        });

        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: "Data Added Successfully.",
          icon: "success",
          className: "qa-form"
        }).then(() => {
          window.location.reload();
        });


      })
      .catch((err) => {
        this.setState({ showLoader: false });
        actions.setSubmitting(false);
        //console.log("====huiiiiii==",err.data.errors.customer_name);
        if(err.data.errors.customer_name != '' && err.data.errors.customer_name != undefined ){
          swal({
            closeOnClickOutside: false,
            title: "Error",
            text: "Data already exists",
            icon: "Error",
            className: "qa-form"
          }).then(() => {
            window.location.reload();
          });
        }
        
        actions.setErrors({
          batch_number: err.data.errors.batch_number,
          material_id: err.data.errors.material_id,
          customer_id: err.data.errors.customer_id,
          lot_no: err.data.errors.lot_no,
          file_name: err.data.errors.file_name,
          plant_code :  err.data.errors.plant
        });
      });
  };

  hideSuccessMsg = () => {
    this.setState({ message: "" });
  };

  removeError = (setErrors) => {
    setErrors({});
  };
  removeFile_Error = () => {
    this.setState({ rejectedFile: [] });
  };
  hideError = () => {
    this.setState({ errMsg: "" });
  };
 
  modalShowHandler = () => {       
    this.setState({ showModal: true});  
  };
  modalCloseHandler = () => {
    this.setState({ showModal: false });
  };

  handleSubmitEvent = (values, actions) => {    
    const post_data = {
      password: values.password
    };

    axios.put(`/api/employees/qa/changepassword`, post_data)
      .then(res => {
        this.modalCloseHandler();
        console.log("response",res);
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: "Password updated successfully.",
          icon: "success"
        }).then(() => {
          window.location.reload();
        });
      })
      .catch(err => {
        actions.setErrors(err.data.errors);
        actions.setSubmitting(false);
      });
  };

  render() {

    return (
      <>
        {this.state.showLoader && (
          <div className="loderOuter">
            <div className="loader">
              <img src={loaderlogo} alt="logo" />
              <div className="loading">Loading...</div>
            </div>
          </div>
        )}
        
        <div className="content-wrapper qaWrapper">
          <section className="content-header">
            <h1>Upload COA Document</h1>
            <div className="customer-password-button btn-fill bdrRadius" onClick={e => this.modalShowHandler()}> 
                Change Password
            </div>
            <div
              className="customer-edit-button btn-fill bdrRadius"
              onClick={() => { localStorage.clear(); this.props.history.push("/qa_login"); }}
            >
              Log Out
              </div>
          </section>
          <section className="content">
            <div className="boxPapanel content-padding form-min-hight">
              <div className="taskdetails">
                <div className="clearfix tdBtmlist">
                  <Formik
                    initialValues={initialValues}
                    validationSchema={validationSchema(this)}
                    onSubmit={this.handleSubmit}
                  >
                    {({
                      values,
                      errors,
                      touched,
                      setFieldTouched,
                      isValid,
                      isSubmitting,
                      setErrors,
                      setFieldValue,
                    }) => {
                      return (
                        <Form>
                          <Row className="eaditRow">
                            <Col xs={12} sm={6} md={4}>
                              <div className="form-group">
                                <label>
                                  Customer Name{" "}
                                </label>
                                <Field
                                  name="customer_name"
                                  type="text"
                                  className="form-control"
                                  autoComplete="off" placeholder={
                                    `Please enter Customer Name (Optional)`
                                  }
                                ></Field>

                                {errors.customer_name &&
                                  touched.customer_name ? (
                                  <span className="errorMsg">
                                    {errors.customer_name}
                                  </span>
                                ) : null}
                              </div>
                            </Col>

                            <Col xs={12} sm={6} md={4}>
                              <div className="form-group">
                                <label>
                                  Customer ID{" "}
                                  <span class="required-field">*</span>
                                </label>
                                <Field
                                  name="customer_id"
                                  type="text"
                                  className="form-control"
                                  autoComplete="off" placeholder={
                                    `Please enter Customer ID`
                                  }
                                ></Field>

                                {errors.customer_id &&
                                  touched.customer_id ? (
                                  <span className="errorMsg">
                                    {errors.customer_id}
                                  </span>
                                ) : null}
                              </div>
                            </Col>

                            <Col xs={12} sm={6} md={4}>
                              <div className="form-group">
                                <label>
                                  Batch Number{" "}
                                  <span class="required-field">*</span>
                                </label>
                                <Field
                                  name="batch_number"
                                  type="text"
                                  className="form-control"
                                  autoComplete="off" placeholder={
                                    `Please enter Batch Number *`
                                  }
                                ></Field>

                                {errors.batch_number &&
                                  touched.batch_number ? (
                                  <span className="errorMsg">
                                    {errors.batch_number}
                                  </span>
                                ) : null}
                              </div>
                            </Col>

                          </Row>
                          <Row className="eaditRow">
                            <Col xs={12} sm={6} md={4}>
                              <div className="form-group">
                                <label>
                                  Inspection Lot Number{" "}
                                  {/* <span class="required-field">*</span> */}
                                </label>
                                <Field
                                  name="lot_no"
                                  type="text"
                                  className="form-control"
                                  autoComplete="off" placeholder={
                                    `Please enter Inspection Lot Number (optional)`
                                  }
                                ></Field>

                                {errors.lot_no &&
                                  touched.lot_no ? (
                                  <span className="errorMsg">
                                    {errors.lot_no}
                                  </span>
                                ) : null}
                              </div>
                            </Col>

                            <Col xs={12} sm={6} md={4}>
                              <div className="form-group">
                                <label>
                                  Material Number{" "}
                                  <span class="required-field">*</span>
                                </label>
                                <Field
                                  name="material_id"
                                  type="text"
                                  className="form-control"
                                  autoComplete="off" placeholder={
                                    `Please enter Material Number *`
                                  }
                                ></Field>

                                {errors.material_id &&
                                  touched.material_id ? (
                                  <span className="errorMsg">
                                    {errors.material_id}
                                  </span>
                                ) : null}
                              </div>
                            </Col>
                            <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                                      <label>
                                      Plant
                                      <span class="required-field">*</span>
                                      </label>
                                      <Select
                                        name="plant_code"
                                        options={this.state.PlantList}
                                        className="basic-select"
                                        classNamePrefix="select"
                                        onChange={(evt) => {
                                          if (evt === null) {
                                            setFieldValue("plant_code", '');
                                          } else {
                                            setFieldValue(
                                              "plant_code", evt.label); 
                                          }
                                        }}
                                        placeholder="Select"
                                        onBlur={() => setFieldTouched("plant_code")}
                                      />
                                      {errors.plant_code && touched.plant_code ? (
                                        <span className="errorMsg">{errors.plant_code}</span>
                                      ) : null}
                                    </div>
                            </Col>

                          
                          </Row>
                          <Row>
                            <Col xs={12} sm={6} md={4}>
                              <div className="form-group">
                                <label>
                                  Upload COA doc{" "}
                                  <span class="required-field">*</span>
                                </label>
                                <Dropzone
                                  maxFiles={5}
                                  multiple={true}
                                  onDrop={(acceptedFiles) =>
                                    this.setDropZoneFiles(
                                      acceptedFiles,
                                      setErrors,
                                      setFieldValue,
                                      setFieldTouched
                                    )
                                  }
                                >
                                  {({ getRootProps, getInputProps }) => (
                                  <section>
                                    <div
                                      {...getRootProps()}
                                      className="custom-file-upload-header"
                                    >
                                      <input {...getInputProps()} />
                                      <p>Upload file</p>
                                    </div>
                                    <div className="custom-file-upload-area">
                                      {this.state.filesHtml}
                                    </div>
                                  </section>
                                )}
                                </Dropzone>

                                {errors.file_name &&
                                  touched.file_name ? (
                                  <span className="errorMsg">
                                    {errors.file_name}
                                  </span>
                                ) : null}
                              </div>
                            </Col>
                          </Row>
                          <Row>
                            <Col xs={12} className="spec-moa-customer">
                              <div className="customers-group">
                                <Button
                                  className="btn-fill customBut"
                                  type="submit"
                                >
                                  {`Submit`}
                                </Button>
                              </div>
                            </Col>
                          </Row>
                        </Form>
                      );
                    }}
                  </Formik>
                </div>
              </div>
            </div>
          </section>
        </div>

        <Modal show={this.state.showModal} onHide={() => this.modalCloseHandler()} backdrop="static" dialogClassName="profile-popup">
          <Modal.Header closeButton>
            <Modal.Title>Change Password</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <div className="contBox">
              <Row>
              <Col xs={12} sm={12} md={12}>
                <div className="profile">
                <Formik                  
                  validationSchema={validatePaaword}
                  onSubmit={this.handleSubmitEvent}
                >
                  {({ values, errors, touched, isValid, isSubmitting }) => {
                    return (
                      <Form>
                        
                          <Row>  
                            <Col xs={12} sm={12} md={12}>
                              <div className="form-group">
                                <label className="pull-left" style={{float:"left"}}>Password</label>
                                <Field
                                  name="password"
                                  type="password"
                                  className={`form-control`}
                                  placeholder="Enter password"
                                  autoComplete="off"
                                />
                                {errors.password && touched.password ? (
                                  <span className="errorMsg">
                                    {errors.password}
                                  </span>
                                ) : null}
                              </div>
                            </Col>
                          </Row>
                          <Row>
                            <Col xs={12} sm={12} md={12}>
                              <div className="form-group">
                                <label className="pull-left">Confirm Password</label>
                                <Field
                                  name="confirm_password"
                                  type="password"
                                  className={`form-control`}
                                  placeholder="Enter confirm password"
                                  autoComplete="off"
                                />
                                {errors.confirm_password &&
                                touched.confirm_password ? (
                                  <span className="errorMsg">
                                    {errors.confirm_password}
                                  </span>
                                ) : null}
                              </div>
                            </Col>
                          </Row>
                          <div className="button-footer btn-toolbar">
                            <button
                              type="button"
                              className="btn btn-success btn-sm"
                              onClick={() => this.modalCloseHandler()}
                            >
                              Cancel
                            </button>
                            <button
                              className={`btn btn-danger btn-sm ${
                                isValid ? "btn-custom-green" : "btn-disable"
                              } m-r-10`}
                              type="submit"
                              disabled={isValid ? false : true}
                            >
                              {isSubmitting ? "Updating..." : "Submit"}
                            </button>
                          </div>
                        
                      </Form>
                    );
                  }}
                </Formik>
                </div>
              </Col>
              </Row>
            </div>
          </Modal.Body>
        </Modal>
      </>
    );

  }
}

export default QAForm;
