import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import axios from "../../shared/axios";

import LoadingSpinner from "../loader/loadingSpinner";
import drreddylogo from "../../assets/images/drreddylogo_white.png";
import exceedlogo from "../../assets/images/Xceed_Logo_Purple_RGB.png";
import closeIcon from "../../assets/images/times-solid-red.svg";

const initialValues = {
  password: "",
  confirm_password: "",
};

var message = "";
var err_message = '';

class resetPassword extends Component {
  constructor(props) {
    super(props);

    message = this.props.location.state
      ? this.props.location.state.message
      : "";
    err_message = this.props.location.state
    ? this.props.location.state.err_message
    : "";
  }

  state = {
    message : '',
    err_message : '',
    token: "",
  };

  componentDidMount() {

    if (localStorage.qa_token !== null && typeof localStorage.qa_token != 'undefined' ){
      this.props.history.push("/qa_form");
    }
    this.setState({
      token: this.props.match.params.token,
    });
    if (message !== null) {
      this.props.history.push({
        pathname: "",
        state: "",
      });
      this.setState({ message : message})
    }
    if (err_message !== null) {
      this.props.history.push({
        pathname: "",
        state: "",
      });
      this.setState({ err_message : err_message})
    }

    document.title = `QA Reset Password | Dr.Reddys API`;

    axios
      .post("/api/employees/qa/check_token", {
        token: this.props.match.params.token,
      })
      .then((res) => {})
      .catch((err) => {
        if (err.data.status === 3) {
          this.props.history.push({
            pathname: "/qa_forgot_password",
            state: {
              err_message: "The link has expired or it has been used earlier or is no longer valid. Please raise a request again.",
            },
          });
        }
      });
  }

  handleSubmit = (values, actions) => {
    axios
    .post("/api/employees/qa/resetpassword", {token: this.state.token,password: values.password.trim()})
    .then(res => {        
        if (res.status === 200) {
          // this.setState({      
          //   message: "Password reset successfully.",
          // }); 
          this.props.history.push({
            pathname: "/qa_login",
            state: {
              message: "Password reset successfully.",
            },
          });            
          actions.setFieldValue("password", "");
          actions.setTouched("password",false);
          actions.setFieldValue("confirm_password", "");
          actions.setTouched("confirm_password",false);
        } else {  alert(123);
          this.setState({      
            err_message:"There is some problem please try again later"
          });
        }
    })
    .catch(err => {
      this.setState({      
        err_message:err.data.errors.password
      });
      actions.setSubmitting(false);
    });

  };

  removeError = () => {
    this.setState({ err_msg: "" });
  };

  render() {

    const resetPasswordvalidation = Yup.object().shape({
      password: Yup.string().trim().required("Please enter password" ).min(8,'Password must be at least 8 characters long'),
      confirm_password: Yup.string().trim().required("Please enter confirm password")
        .test(
          "passwords-match",
          "Password and Confirm password must match",
          function (value) {
            return this.parent.password === value;
          }
        ),
    });

    const { loading } = this.state;

    return (
      <>
        <div className="login-box">
          {loading ? (
            <LoadingSpinner />
          ) : (
            <Formik
              initialValues={initialValues}
              validationSchema={resetPasswordvalidation}
              onSubmit={this.handleSubmit}
            >
              {({
                values,
                errors,
                touched,
                handleChange,
                isValid,
                handleBlur,
                setFieldValue,
              }) => {
                //console.log( errors );
                return (
                  <Form>
                    <div className="login-logo">
                      <Link to="/" className="logo">
                        <span className="logo-mini">
                          <img src={drreddylogo} alt="Dr.Reddy's" />
                        </span>
                      </Link>
                      <br />
                      <span className="logo-mini">
                        <img src={exceedlogo} alt="Dr.Reddy's" height="36px" />
                      </span>
                    </div>

                    <div className="login-box-body">
                    {/* <div
                              className="messageserror"
                              style={{
                                display:
                                  this.state.err_msg &&
                                  this.state.err_msg !== null &&
                                  this.state.err_msg !== ""
                                    ? "block"
                                    : "none",
                              }}
                            >
                              <Link to="#" className="close">
                                <img
                                  src={closeIcon}
                                  width="17.69px"
                                  height="22px"
                                  onClick={(e) => this.removeError()}
                                />
                              </Link>
                              <div>
                                <span className="errorMsg">
                                  {this.state.err_msg}
                                </span>
                              </div>
                            </div> */}
                      
                      {this.state.message && <h5 style={{color: '#1bb200',padding: '5px 10px',textAlign : "center"}}>{this.state.message}</h5>}
                      {this.state.err_message && <h5 style={{color: '#c83a40',padding: '5px 10px',textAlign : "center"}}>{this.state.err_message}</h5>}
                      <p className="login-box-msg">QA Reset Password</p>

                      <label>Password:</label>
                      <div className="form-group has-feedback m-b-25">
                        
                        <Field
                          name="password"
                          type="password"
                          className="form-control"
                          autoComplete="off"
                          placeholder="Please enter password"                         
                        />

                        {errors.password && touched.password && (
                          <label>{errors.password}</label>
                        )}

                      </div>

                      <label>Confirm Password:</label>
                      <div className="form-group has-feedback m-b-25">
                        
                        <Field
                          name="confirm_password"
                          type="password"
                          className="form-control"
                          autoComplete="off"
                          placeholder="Please enter confirm password"                         
                        />

                        {errors.confirm_password && touched.confirm_password && (
                          <label>{errors.confirm_password}</label>
                        )}

                      </div>
                      
                      <div className="row">
                        <div className="col-xs-8">
                          <Link to={'/qa_login'}>I already have an account</Link>
                        </div>
                        <div className="col-xs-4">
                          <button
                            className="btn-fill"
                            type="submit"
                          >
                            Submit
                          </button>
                        </div>
                      </div>
                    </div>
                  </Form>
                );
              }}
            </Formik>
          )}{" "}
          {/* end of loader */}
        </div>
      </>
    );  
  }
}


export default resetPassword;
