import React, { Component, Fragment } from 'react';
import Sidebar from './Sidebar';
import { withRouter } from 'react-router-dom';

class Layout extends Component {
    constructor( props ) {
        super( props );
    }

    componentDidMount() {
        document.body.classList.add('skin-blue');
        document.body.classList.add('sidebar-mini');
    }

    render() {
        const isLoggedIn = (localStorage.getItem('qa_token') !== "" && localStorage.getItem('qa_token') !== null) ? true : false;
        const path_name = '';
        return  <Fragment>
                    <Sidebar isLoggedIn={isLoggedIn} path_name={path_name}/> 
                    {this.props.children}
                </Fragment>
    }
}

  
export default withRouter(Layout);