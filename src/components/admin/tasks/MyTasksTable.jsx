import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  Tooltip,
  OverlayTrigger
} from "react-bootstrap";
import dateFormat from "dateformat";

import { htmlDecode } from '../../../shared/helper';
import {showErrorMessage} from "../../../shared/handle_error";
import API from "../../../shared/admin-axios"; 


import SubTaskTable from "./SubTaskTable";
import StatusColumn from "./StatusColumn";

import { Link } from "react-router-dom";
import Pagination from "react-js-pagination";

const priority_arr = [
  { priority_id: 1, priority_value: "Low" },
  { priority_id: 2, priority_value: "Medium" },
  { priority_id: 3, priority_value: "High" }
];

const getStatusColumn = refObj => (cell, row) => {
  return <StatusColumn rowData={row} />;
};

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

const actionFormatter = refObj => cell => {
  return (
    <div className="actionStyle">
      <LinkWithTooltip
        tooltip="Click to view task details"
        href={`/admin/task_details/${cell}`}
        id="tooltip-1"
      >
        <i className="fas fa-eye" />
      </LinkWithTooltip>
    </div>
  )
}
/*For Tooltip*/

const setDescription = refOBj => (cell, row) => {

  if(row.parent_id > 0){
    return (
        <LinkWithTooltip
            tooltip={`${row.title}`}
            href="#"
            id="tooltip-1"
            clicked={e => refOBj.checkHandler(e)}
          >
          {row.title}
        </LinkWithTooltip>
    );
  }else{
    return (      
      <LinkWithTooltip
        tooltip={`${row.req_name}`}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {row.req_name}
      </LinkWithTooltip>
      );
  }
};

const setProduct = refOBj => (cell, row) => {

  if(row.parent_id > 0){
    return (
        <LinkWithTooltip
            tooltip={`${row.title}`}
            href="#"
            id="tooltip-1"
            clicked={e => refOBj.checkHandler(e)}
          >
          {row.title}
        </LinkWithTooltip>
    );
  }else{
    return (      
      <LinkWithTooltip
        tooltip={`${row.product_name}`}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {row.product_name}
      </LinkWithTooltip>
      );
  }
};

const setDate = refOBj => (cell,row) => {
  return (    
    <LinkWithTooltip
        tooltip={cell}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
      {cell}  
    </LinkWithTooltip>
  );
};

// const setCreateDate = refObj => cell => {
//   //var replacedDate = cell.split('T');
//   var mydate = new Date(cell);
//   //console.log(mydate);
//   return dateFormat(mydate, "dd/mm/yyyy");
// };

// const setDaysPending = refObj => (cell, row) => {
//   var selDueDate = cell;
//   var dueDate = new Date(selDueDate);
//   var today = new Date();
//   var timeDiff = dueDate.getTime() - today.getTime();

//   //console.log(timeDiff);
//   if (timeDiff > 0) {
//     var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
//     return diffDays;
//   } else {
//     return 0;
//   }
// };

const setAssignedBy = refOBj => (cell, row) => {
  if(row.assigned_by == '-1' ){
    return (    
      <LinkWithTooltip
          tooltip={`System`}
          href="#"
          id="tooltip-1"
          clicked={e => refOBj.checkHandler(e)}
        >
        System  
      </LinkWithTooltip>
    );
  }else{
    return (    
      <LinkWithTooltip
          tooltip={`${row.ab_emp_first_name + " " + row.ab_emp_last_name + " (" + row.ab_emp_desig_name + ")"}`}
          href="#"
          id="tooltip-1"
          clicked={e => refOBj.checkHandler(e)}
        >
          {row.ab_emp_first_name + " " + row.ab_emp_last_name + " (" + row.ab_emp_desig_name + ")"}
      </LinkWithTooltip>
    );
  }
};

const clickToShowTasks = refObj => (cell, row) => {
  return `${cell}`;
};

const setCustomerName = refOBj => (cell, row) => {
  //return `${row.first_name} ${row.last_name}`;
  return (    
    <LinkWithTooltip
        tooltip={`${row.first_name + " " + row.first_name }`}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {row.first_name + " " + row.first_name}
    </LinkWithTooltip>
  );
};

// const setPriorityName = refObj => (cell, row) => {
//   var ret = "Set Priority";
//   for (let index = 0; index < priority_arr.length; index++) {
//     const element = priority_arr[index];
//     if (element["priority_id"] === cell) {
//       ret = element["priority_value"];
//     }
//   }

//   return ret;
// };

const setCompanyName = refOBj => (cell, row) => {
  //return htmlDecode(cell);
  return (    
    <LinkWithTooltip
        tooltip={htmlDecode(cell)}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {htmlDecode(cell)}
    </LinkWithTooltip>
  );
}

const setAssignedTo = refOBj => (cell, row) => {
  return (    
    <LinkWithTooltip
        tooltip={`${row.at_emp_first_name + " " + row.at_emp_last_name + " (" + row.at_emp_desig_name + ")"}`}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {row.at_emp_first_name + " " + row.at_emp_last_name + " (" + row.at_emp_desig_name + ")"}
    </LinkWithTooltip>
  );
};

class MyTasksTable extends Component {
  state = {
    showCreateSubTask: false,
    showAssign: false,
    showRespondBack: false,
    showTaskDetails: false,
    showRespondCustomer: false,
    showAuthorizeBack: false,
    changeData: true,
    task_id: 0,
    activePage: 1,
    itemPerPage: 20,
    tableData: [],
    empID : 0,
    countMyTasks : 0
  };

  checkHandler = (event) => {
    event.preventDefault();
  };
  
  taskDetails = row => {
    this.setState({ showTaskDetails: true, currRow: row });
  };

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getMyTasks(pageNumber > 0 ? pageNumber  : 1);
  };

  getMyTasks(page = 1) {
    let emp_id = this.state.empID;
    //console.log('>>>>>',emp_id);
    let url;
    url = `/api/employees/my_tasks/${emp_id}?page=${page}`;     
    API.get(url)
      .then(res => {
        this.setState({ 
          tableData: res.data.data, 
          countMyTasks: res.data.count_my_tasks});
      })
      .catch(err => { 
        showErrorMessage(err,this.props);
      });
  };

  componentDidMount() {
    //console.log('>>>>>',this.props.countMyTasks);
    this.setState({
      tableData: this.props.tableData,
      countMyTasks: this.props.countMyTasks,
      empID: this.props.empID,
      options: {
        clearSearch: true,
        expandBy: "column",
        page: !this.state.start_page ? 1 : this.state.start_page, // which page you want to show as default
        sizePerPageList: [
          {
            text: "10",
            value: 10
          },
          {
            text: "20",
            value: 20
          },
          {
            text: "All",
            value: !this.state.tableData ? 1 : this.state.tableData
          }
        ], // you can change the dropdown list for size per page
        sizePerPage: 20, // which size per page you want to locate as default
        pageStartIndex: 1, // where to start counting the pages
        paginationSize: 3, // the pagination bar size.
        prePage: "‹", // Previous page button text
        nextPage: "›", // Next page button text
        firstPage: "«", // First page button text
        lastPage: "»", // Last page button text
        //paginationShowsTotal: this.renderShowsTotal,  // Accept bool or function
        paginationPosition: "bottom" // default is bottom, top and both is all available
        // hideSizePerPage: true > You can hide the dropdown for sizePerPage
        // alwaysShowAllBtns: true // Always show next and previous button
        // withFirstAndLast: false > Hide the going to First and Last page button
      }
    });
  }

  getSubTasks = row => {
    return (
      <SubTaskTable
        tableData={row.sub_tasks}
        dashType={this.props.dashType}
        reloadTaskSubTask={() => this.props.allTaskDetails()}
      />
    );
  };

  checkSubTasks = row => {
    //console.log("subtask")

    if (typeof row.sub_tasks !== "undefined" && row.sub_tasks.length > 0) {
      return true;
    } else {
      return false;
    }
  };

  tdClassName = (fieldValue, row) => {
    var dynamicClass = "width-150 ";
    if (row.vip_customer === 1) {
      dynamicClass += "bookmarked-column ";
    }
    // if(row.sub_tasks.length > 0 && typeof row.sub_tasks !== 'undefined' && typeof row.bookmarked !== 'undefined' ){
    //     return 'sub-task-column bookmarked-column';
    // }else if( typeof row.bookmarked !== 'undefined' ){
    //     return 'bookmarked-column';
    // }else if(row.sub_tasks.length > 0 && typeof row.sub_tasks !== 'undefined'){
    //     return 'sub-task-column';
    // }else{
    //     return '';
    // }
    return dynamicClass;
  };

  trClassName = (row, rowIndex) => {
    var ret = " ";
    var selDueDate = row.due_date;
    var dueDate = new Date(selDueDate);
    var today = new Date();
    var timeDiff = dueDate.getTime() - today.getTime();

    if (timeDiff > 0) {
    } else {
      if (row.vip_customer === 1) {
        ret += "tr-red";
      }
    }

    return ret;
  };

  expandColumnComponent({ isExpandableRow, isExpanded }) {
    let content = "";

    if (isExpandableRow) {
      content = isExpanded ? "-" : "+";
    } else {
      content = " ";
    }
    return <div> {content} </div>;
  }

  render() {
   
    const paginationOptions = {
      page: 1, // which page you want to show as default
      sizePerPageList: [
        {
          text: "10",
          value: 10
        },
        {
          text: "20",
          value: 20
        },
        {
          text: "All",
          value: this.props.tableData.length > 0 ? this.props.tableData.length : 1
        }
      ], // you can change the dropdown list for size per page
      sizePerPage: 20, // which size per page you want to locate as default
      hideSizePerPage: true,
      pageStartIndex: 1, // where to start counting the pages
      paginationSize: 3, // the pagination bar size.
      prePage: '‹', // Previous page button text
      nextPage: '›', // Next page button text
      firstPage: '«', // First page button text
      lastPage: '»', // Last page button text
      //paginationShowsTotal: this.renderShowsTotal, // Accept bool or function
      paginationPosition: "bottom" // default is bottom, top and both is all available
      // hideSizePerPage: true //> You can hide the dropdown for sizePerPage
      // alwaysShowAllBtns: true // Always show next and previous button
      // withFirstAndLast: false //> Hide the going to First and Last page button
    };
    return (
      <>
        {this.state.changeData && (
          <BootstrapTable
            data={this.state.tableData}
            /*options={paginationOptions}
            pagination*/
            expandableRow={this.checkSubTasks}
            expandComponent={this.getSubTasks}
            expandColumnOptions={{
              expandColumnVisible: true,
              expandColumnComponent: this.expandColumnComponent,
              columnWidth: 25
            }}
            trClassName={this.trClassName}
          >
            <TableHeaderColumn
              dataField="task_ref"
              isKey
              
              columnClassName={this.tdClassName}
              editable={false}
              expandable={false}
              dataFormat={clickToShowTasks(this)}
            >
              Tasks
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="request_type"
              
              editable={false}
              expandable={false}
              dataFormat={setDescription(this)}
            >
              Description
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="product_name"
              
              editable={false}
              expandable={true}
              dataFormat={setProduct(this)}
            >
              Product Name
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="display_assign_date"
              
              editable={false}
              expandable={false}
              dataFormat={setDate(this)}
            >
              Assigned Date
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="display_date_added"
              
              editable={false}
              expandable={false}
              dataFormat={setDate(this)}
            >
              Created
            </TableHeaderColumn>

            {/* <TableHeaderColumn dataField='rdd' dataFormat={ setCreateDate(this) } editable={ false } expandable={ false }>RDD</TableHeaderColumn> */}

            <TableHeaderColumn
              dataField="display_new_due_date"
              
              editable={false}
              expandable={false}
              dataFormat={setDate(this)}
            >
              Due Date
            </TableHeaderColumn>

            {/* <TableHeaderColumn dataField='assigned_to' editable={ false } expandable={ false } >Assigned To</TableHeaderColumn> */}

            <TableHeaderColumn
              dataField="dept_name"
              
              editable={false}
              expandable={false}
              dataFormat={setAssignedBy(this)}
            >
              Assigned By
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="dept_name"
              
              editable={false}
              expandable={false}
              dataFormat={setAssignedTo(this)}
            >
              Assigned To{" "}
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="company_name"
              
              expandable={false}
              dataFormat={setCompanyName(this)}
            >
              Customer
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="cust_name"
              
              editable={false}
              expandable={false}
              dataFormat={setCustomerName(this)}
            >
              User
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="status"
              dataFormat={getStatusColumn(this)}
              expandable={false}
              editable={false}
            >
              Status
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="task_id"
              width="5%"
              dataFormat={actionFormatter(this)}
            >
              Task Details
            </TableHeaderColumn>            
          </BootstrapTable>
        )}
        {this.state.countMyTasks > 20 ? (
        <Row>
          <Col md={12}>
            <div className="paginationOuter text-right">
              <Pagination
                activePage={this.state.activePage}
                itemsCountPerPage={this.state.itemPerPage}
                totalItemsCount={this.state.countMyTasks}
                itemClass='nav-item'
                linkClass='nav-link'
                activeClass='active'
                hideNavigation='false'
                onChange={this.handlePageChange}
              />
            </div>
          </Col>
        </Row>
        ) : null}
        </>
    );
  }
}

export default MyTasksTable;
