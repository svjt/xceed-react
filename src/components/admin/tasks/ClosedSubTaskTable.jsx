import React, { Component } from 'react';
import {BootstrapTable,TableHeaderColumn} from 'react-bootstrap-table';
import { htmlDecode } from '../../../shared/helper';

import StatusColumn from './StatusColumn';
import { Link } from 'react-router-dom';

import {
  Tooltip,
  OverlayTrigger
} from "react-bootstrap";



const getStatusColumn = refObj => (cell,row) => {
    return <StatusColumn rowData={row} />
}

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const setDescription = refOBj => (cell,row) =>{
    if(row.parent_id > 0){
        return (
            <LinkWithTooltip
                tooltip={`${row.title}`}
                href="#"
                id="tooltip-1"
                clicked={e => refOBj.checkHandler(e)}
              >
              {row.title}
            </LinkWithTooltip>
        );
      }else{
        return (      
          <LinkWithTooltip
            tooltip={`${row.req_name}`}
            href="#"
            id="tooltip-1"
            clicked={e => refOBj.checkHandler(e)}
          >
            {row.req_name}
          </LinkWithTooltip>
          );
      }
}

const clickToShowTasks = refObj => (cell,row) =>{
  return `${cell}`;
    
}

const setBlank = refObj => (cell,row) => {
  return "";    
};

const setEmpCustName = refObj => (cell, row) => {
  return `${row.first_name} ${row.last_name}`;
};

const setAssignedBy = refOBj => (cell, row) => {
  if(row.assigned_by == '-1' ){
    return (    
      <LinkWithTooltip
          tooltip={`System`}
          href="#"
          id="tooltip-1"
          clicked={e => refOBj.checkHandler(e)}
        >
        System  
      </LinkWithTooltip>
    );
  }else{
    return (    
      <LinkWithTooltip
          tooltip={`${row.ab_emp_first_name + " " + row.ab_emp_last_name + " (" + row.ab_emp_desig_name + ")"}`}
          href="#"
          id="tooltip-1"
          clicked={e => refOBj.checkHandler(e)}
        >
          {row.ab_emp_first_name + " " + row.ab_emp_last_name + " (" + row.ab_emp_desig_name + ")"}
      </LinkWithTooltip>
    );
  }
};

const setAssignedTo = refOBj => (cell,row) => {
    //return row.emp_first_name+' '+row.emp_last_name+' ('+row.desig_name+')';
    return (      
      <LinkWithTooltip
        tooltip={`${row.at_emp_first_name+' '+row.at_emp_last_name+' ('+row.at_desig_name+')'}`}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {row.at_emp_first_name+' '+row.at_emp_last_name+' ('+row.at_desig_name+')'}
      </LinkWithTooltip>
      );
}

const setCompanyName = refOBj => (cell, row) => {
  //return htmlDecode(cell);
  return (    
    <LinkWithTooltip
        tooltip={htmlDecode(cell)}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {htmlDecode(cell)}
    </LinkWithTooltip>
  );
}

const setDate = refOBj => (cell,row) => {
  return (    
    <LinkWithTooltip
        tooltip={cell}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
      {cell}  
    </LinkWithTooltip>
  );
};

const actionFormatter = refObj => cell => {
  return (
    <div className="actionStyle">
      <LinkWithTooltip
        tooltip="Click to view task details"
        href={`/admin/task_details/${cell}`}
        id="tooltip-1"
      >
        <i className="fas fa-eye" />
      </LinkWithTooltip>
    </div>
  );
};

class ClosedSubTaskTable extends Component {

    state = {
        showCreateSubTask   : false,
        showAssign          : false,
        showRespondBack     : false
    };

    checkHandler = (event) => {
        event.preventDefault();
    };

    tdClassName = (fieldValue, row) =>{
        var dynamicClass = 'width-150 ';
        if(row.vip_customer === 1){
            dynamicClass += 'bookmarked-column ';
        }
        return dynamicClass;
    }

    render(){

        const selectRowProp = {
            bgColor       : '#fff8f6'
        };

        return (
            <>
                <BootstrapTable 
                    data={this.props.tableData} 
                    selectRow={ selectRowProp } 
                    tableHeaderClass={"col-hidden"}
                    expandColumnOptions={ 
                        { 
                            expandColumnVisible: true,
                            expandColumnComponent: this.expandColumnComponent,
                            columnWidth: 25
                        } 
                    } 
                    trClassName="tr-expandable" 
                >
                    <TableHeaderColumn isKey dataField='task_ref' columnClassName={ this.tdClassName } editable={ false }  dataFormat={ clickToShowTasks(this) } >Tasks</TableHeaderColumn>

                    <TableHeaderColumn dataField='request_type' editable={ false }  dataFormat={ setDescription(this) } >Description</TableHeaderColumn>
                    <TableHeaderColumn dataField='request_type' editable={ false }  dataFormat={ setBlank(this) } >Product Name</TableHeaderColumn>

                    <TableHeaderColumn dataField='dept_name' editable={ false } expandable={ false } dataFormat={ setAssignedBy(this) } >Assigned BY</TableHeaderColumn>

                    {/* <TableHeaderColumn dataField='dept_name' editable={ false } expandable={ false } >Department</TableHeaderColumn> */}

                    <TableHeaderColumn dataField='dept_name' editable={ false } expandable={ false } dataFormat={ setAssignedTo(this) } >Assigned To </TableHeaderColumn>

                    <TableHeaderColumn dataField='company_name'  dataFormat={ setCompanyName(this) } >Customer</TableHeaderColumn> 

                    <TableHeaderColumn dataField='cust_name' editable={ false }  dataFormat={ setEmpCustName(this) } >User Name</TableHeaderColumn>

                    <TableHeaderColumn dataField='display_date_added' editable={ false } dataFormat={setDate(this)} >Created</TableHeaderColumn>

                    <TableHeaderColumn dataField='display_due_date' editable={ false }  dataFormat={ setDate(this) } >Due Date</TableHeaderColumn>

                    <TableHeaderColumn dataField='display_date_added' editable={ false } dataFormat={setDate(this)} >Assigned Date</TableHeaderColumn>
                    
                    <TableHeaderColumn dataField='display_responded_date' editable={ false } dataFormat={setDate(this)} >Closed Date</TableHeaderColumn>

                    <TableHeaderColumn dataField='status' dataFormat={ getStatusColumn(this) }  editable={ false } >Status</TableHeaderColumn>
                    
                    <TableHeaderColumn
                      dataField="task_id"
                      width="5%"
                      dataFormat={actionFormatter(this)}
                      expandable={false}
                    >
                      Task Details
                    </TableHeaderColumn>
                </BootstrapTable>

            </>
        );
    }

}

export default ClosedSubTaskTable;