import React, { Component } from "react";
import Layout from "../layout/Layout";
import API from "../../../shared/admin-axios";

import AllTasksTable from './AllTasksTable';
import AllClosedTasksTable from './AllClosedTasksTable';
import whitelogo from '../../../assets/images/drreddylogo_white.png';
import { showErrorMessage } from "../../../shared/handle_error";
import { getSuperAdmin, getAdminGroup } from '../../../shared/helper';

import {
  Tooltip,
  OverlayTrigger
} from "react-bootstrap";
import { Link } from 'react-router-dom';

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

class Tasks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openTasks: [],
      countOpenTasks: '',
      closedTasks: [],
      isLoading: true,
      tabsClicked: 'tab_1',
      get_access_data: false,
      request_type: [],
      search_task_ref: "",
      search_request_type: "",
      search_cust_name: "",
      search_user_name: "",
      search_status: "",
    };
  }

  componentDidMount = () => {
    //this.getAllTask();
    const superAdmin = getSuperAdmin(localStorage.admin_token);

    if (superAdmin === 1) {
      this.setState({
        access: {
          view: true,
          add: true,
          edit: true,
          delete: true
        },
        get_access_data: true
      });
      this.getAllTask();
      this.getServiceRequestList();
    } else {
      const adminGroup = getAdminGroup(localStorage.admin_token);
      API.get(`/api/adm_group/single_access/${adminGroup}/${'TASK_MANAGEMENT'}`)
        .then(res => {
          this.setState({
            access: res.data.data,
            get_access_data: true
          });

          if (res.data.data.view === true) {
            this.getAllTask();
          } else {
            this.props.history.push('/admin/dashboard');
          }

        })
        .catch(err => {
          showErrorMessage(err, this.props);
        });
    }
  };

  getAllTask = (page = 1) => {
    let task_ref = this.state.search_task_ref;
    let request_type = this.state.search_request_type;
    let cust_name = this.state.search_cust_name;
    let user_name = this.state.search_user_name;
    let status = this.state.search_status;

    API.get(`/api/tasks/open_tasks?page=${page}&task_ref=${encodeURIComponent(task_ref)}&request_type=${encodeURIComponent(request_type)}&cust_name=${encodeURIComponent(cust_name)}&user_name=${encodeURIComponent(user_name)}&status=${encodeURIComponent(status)}`).then(res => {
      this.setState({ openTasks: res.data.data, countOpenTasks: res.data.count_open_tasks });
      API.get(`/api/tasks/closed_tasks?page=${page}&task_ref=${encodeURIComponent(task_ref)}&request_type=${encodeURIComponent(request_type)}&cust_name=${encodeURIComponent(cust_name)}&user_name=${encodeURIComponent(user_name)}&status=${encodeURIComponent(status)}`).then(res => {
        this.setState({ closedTasks: res.data.data, countClosedTasks: res.data.count_closed_tasks, isLoading: false });
      }).catch(err => { });
    }).catch(err => { });
  }

  getServiceRequestList() {
    API.get(`/api/feed/request_type_adm`).then((res) => {
      var all_request_type = [];
      for (let index = 0; index < res.data.data.length; index++) {
        const element = res.data.data[index];
        all_request_type.push(element);
      }
      this.setState({
        request_type: all_request_type,
        isLoading: false,
      });
    });
  }

  handleTabs = (event) => {

    if (event.currentTarget.className === "active") {
      //DO NOTHING
    } else {

      var elems = document.querySelectorAll('[id^="tab_"]');
      var elemsContainer = document.querySelectorAll('[id^="show_tab_"]');
      var currId = event.currentTarget.id;

      for (var i = 0; i < elems.length; i++) {
        elems[i].classList.remove('active');
      }

      for (var j = 0; j < elemsContainer.length; j++) {
        elemsContainer[j].style.display = 'none';
      }

      event.currentTarget.classList.add('active');
      event.currentTarget.classList.add('active');
      document.querySelector('#show_' + currId).style.display = 'block';
      this.setState({ tabsClicked: currId });
    }
  }

  downloadXLSX = (e) => {
    e.preventDefault();
    let task_ref = this.state.search_task_ref;
    let request_type = this.state.search_request_type;
    let cust_name = this.state.search_cust_name;
    let user_name = this.state.search_user_name;
    let status = this.state.search_status;

    if (this.state.tabsClicked === 'tab_1') {
      API.get(`/api/tasks/open_tasks_download?page=1&task_ref=${encodeURIComponent(task_ref)}&request_type=${encodeURIComponent(request_type)}&cust_name=${encodeURIComponent(cust_name)}&user_name=${encodeURIComponent(user_name)}&status=${encodeURIComponent(status)}`, { responseType: 'blob' })
        .then(res => {
          let url = window.URL.createObjectURL(res.data);
          let a = document.createElement('a');
          a.href = url;
          a.download = 'open_tasks.xlsx';
          a.click();

        }).catch(err => {
          showErrorMessage(err, this.props);
        });
    } else if (this.state.tabsClicked === 'tab_2') {
      API.get(`/api/tasks/closed_tasks_download?page=1&task_ref=${encodeURIComponent(task_ref)}&request_type=${encodeURIComponent(request_type)}&cust_name=${encodeURIComponent(cust_name)}&user_name=${encodeURIComponent(user_name)}&status=${encodeURIComponent(status)}`, { responseType: 'blob' })
        .then(res => {
          let url = window.URL.createObjectURL(res.data);
          let a = document.createElement('a');
          a.href = url;
          a.download = 'closed_tasks.xlsx';
          a.click();

        }).catch(err => {
          showErrorMessage(err, this.props);
        });
    }
  }
  checkHandler = (event) => {
    event.preventDefault();
  };

  custSearch = (e) => {
    e.preventDefault();
    var task_ref = document.getElementById('task_ref').value;
    var request_type = document.getElementById('request_type').value;
    var cust_name = document.getElementById('cust_name').value;
    var user_name = document.getElementById('user_name').value;
    var status = document.getElementById('status').value;


    if (task_ref === "" && request_type === "" && cust_name === "" && user_name === "" && status === "") {
      return false;
    }

    this.setState({
      openTasks: "",
      countOpenTasks: "",
      closedTasks: "",
      countClosedTasks: "",
      search_task_ref: "",
      search_request_type: "",
      search_cust_name: "",
      search_user_name: "",
      search_status: "",
      activePage: 1,
    });

    API.get(`/api/tasks/open_tasks?page=1&task_ref=${encodeURIComponent(task_ref)}&request_type=${encodeURIComponent(request_type)}&cust_name=${encodeURIComponent(cust_name)}&user_name=${encodeURIComponent(user_name)}&status=${encodeURIComponent(status)}`).then(res => {
      this.setState({ openTasks: res.data.data, countOpenTasks: res.data.count_open_tasks, activePage: 1 });
      API.get(`/api/tasks/closed_tasks?page=1&task_ref=${encodeURIComponent(task_ref)}&request_type=${encodeURIComponent(request_type)}&cust_name=${encodeURIComponent(cust_name)}&user_name=${encodeURIComponent(user_name)}&status=${encodeURIComponent(status)}`).then(res => {
        this.setState({
          closedTasks: res.data.data, countClosedTasks: res.data.count_closed_tasks, isLoading: false,
          search_task_ref: task_ref,
          search_request_type: request_type,
          search_cust_name: cust_name,
          search_user_name: user_name,
          search_status: status,
          remove_search: true,
          activePage: 1,
        });

      }).catch(err => {
        showErrorMessage(err, this.props);
      });
    }).catch(err => {
      showErrorMessage(err, this.props);
    });
  }

  clearSearch = () => {
    this.setState({
      openTasks: "",
      countOpenTasks: "",
      closedTasks: "",
      countClosedTasks: "",
      search_task_ref: "",
      search_request_type: "",
      search_cust_name: "",
      search_user_name: "",
      search_status: "",
      activePage: 1,
    });

    document.getElementById('task_ref').value = "";
    document.getElementById('request_type').value = "";
    document.getElementById('cust_name').value = "";
    document.getElementById('user_name').value = "";
    document.getElementById('status').value = "";

    this.setState({
      search_task_ref: "",
      search_request_type: "",
      search_cust_name: "",
      search_user_name: "",
      search_status: "",
      remove_search: false
    }, () => {
      this.getAllTask();
      this.setState({ activePage: 1 })
    })
  }

  render() {
    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy" >
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (

        <Layout {...this.props}>
          <div className="content-wrapper">
            {/* <section className="content-header">
              <h1>Manage Tasks<small></small></h1>
            </section> */}
            <section className="content-header">
              <div className="row">
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <h1>
                    Manage Tasks
                    <small />
                  </h1>
                </div>
                <div className="col-lg-12 col-sm-12 col-xs-12 topSearchSection">
                  <form className="form">
                    <div className="">
                      <input
                        className="form-control"
                        name="task_ref"
                        id="task_ref"
                        placeholder="Filter by Task Ref"
                      />
                    </div>

                    <div>
                      <select
                        name="request_type"
                        id="request_type"
                        className="form-control"
                      >
                        <option value="">Select Request</option>
                        {this.state.request_type.map((request, i) => (
                          <option key={i} value={request.type_id}>
                            {request.req_name}
                          </option>
                        ))}
                      </select>
                    </div>

                    <div className="">
                      <input
                        className="form-control"
                        name="cust_name"
                        id="cust_name"
                        placeholder="Filter by customer name"
                      />
                    </div>
                    <div className="">
                      <input
                        className="form-control"
                        name="user_name"
                        id="user_name"
                        placeholder="Filter by user name"
                      />
                    </div>
                    <div style={{ display: 'none' }}>
                      <select
                        name="status"
                        id="status"
                        className="form-control"
                        onChange={this.filterSearchComp}
                      >
                        <option value="">Status</option>
                        <option value="0">Due</option>
                        <option value="1">Assigned</option>
                        <option value="2">In Progress</option>
                        <option value="3">Responded</option>
                      </select>
                    </div>


                    <div className="">
                      <input
                        type="submit"
                        value="Search"
                        className="btn btn-warning btn-sm"
                        onClick={(e) => this.custSearch(e)}
                      />
                      {this.state.remove_search ? <a onClick={() => this.clearSearch()} className="btn btn-danger btn-sm"> Remove </a> : null}
                    </div>
                    <div className="clearfix"></div>
                  </form>
                  <div className="clearfix"></div>

                </div>
              </div>
            </section>
            <section className="content">
              <div className="box">
                <div className="box-body">
                  <div className="row">
                    <div className="col-xs-12">
                      <div className="nav-tabs-custom">
                        <ul className="nav nav-tabs">
                          <li className="active" onClick={(e) => this.handleTabs(e)} id="tab_1" >OPEN TASKS ({this.state.countOpenTasks && this.state.countOpenTasks > 0 ? this.state.countOpenTasks : 0})</li>
                          <li onClick={(e) => this.handleTabs(e)} id="tab_2">CLOSED TASKS ({this.state.countClosedTasks && this.state.countClosedTasks > 0 ? this.state.countClosedTasks : 0})</li>
                          <li className="tabButtonSec pull-right">
                            {(this.state.tabsClicked === 'tab_1' && this.state.countOpenTasks > 0) &&
                              <span onClick={(e) => this.downloadXLSX(e)} >
                                <LinkWithTooltip
                                  tooltip={`Click here to download excel`}
                                  href="#"
                                  id="tooltip-my"
                                  clicked={e => this.checkHandler(e)}
                                >
                                  <i className="fas fa-download"></i>
                                </LinkWithTooltip>
                              </span>}

                            {(this.state.tabsClicked === 'tab_2' && this.state.countClosedTasks > 0) &&
                              <span onClick={(e) => this.downloadXLSX(e)} >
                                <LinkWithTooltip
                                  tooltip={`Click here to download excel`}
                                  href="#"
                                  id="tooltip-allocated"
                                  clicked={e => this.checkHandler(e)}
                                >
                                  <i className="fas fa-download"></i>
                                </LinkWithTooltip>
                              </span>}

                            {(this.state.tabsClicked === 'tab_3' && this.state.countClosedTasks > 0) &&
                              <span onClick={(e) => this.downloadXLSX(e)} >
                                <LinkWithTooltip
                                  tooltip={`Click here to download excel`}
                                  href="#"
                                  id="tooltip-close"
                                  clicked={e => this.checkHandler(e)}
                                >
                                  <i className="fas fa-download"></i>
                                </LinkWithTooltip>
                              </span>}
                          </li>
                        </ul>

                        <div className="tab-content">

                          <div className="tab-pane active" id="show_tab_1">

                            {this.state.openTasks && this.state.openTasks.length > 0 && <AllTasksTable tableData={this.state.openTasks} countOpenTasks={this.state.countOpenTasks} search_task_ref={this.state.search_task_ref} search_request_type={this.state.search_request_type} search_cust_name={this.state.search_cust_name} search_user_name={this.state.search_user_name} search_status={this.state.search_status} />}

                            {this.state.openTasks && this.state.openTasks.length === 0 && <div className="noData">No Data Found</div>}

                          </div>

                          <div className="tab-pane" id="show_tab_2">

                            {this.state.closedTasks && this.state.closedTasks.length > 0 && <AllClosedTasksTable closedTableData={this.state.closedTasks} countClosedTasks={this.state.countClosedTasks} search_task_ref={this.state.search_task_ref} search_request_type={this.state.search_request_type} search_cust_name={this.state.search_cust_name} search_user_name={this.state.search_user_name} search_status={this.state.search_status} />}

                            {this.state.closedTasks && this.state.closedTasks.length === 0 && <div className="noData">No Data Found</div>}
                          </div>

                        </div>
                      </div>
                    </div>
                  </div></div></div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}

export default Tasks;
