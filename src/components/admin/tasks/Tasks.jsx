import React, { Component } from "react";

import { Redirect } from "react-router-dom";
import Layout from "../layout/Layout";
import API from "../../../shared/admin-axios";

import MyTasksTable from './MyTasksTable';
import AllocatedTasksTable from './AllocatedTasksTable';
import ClosedTasksTable from './ClosedTasksTable';
import whitelogo from '../../../assets/images/drreddylogo_white.png';
import { getSuperAdmin, getAdminGroup } from '../../../shared/helper';
import { showErrorMessage } from "../../../shared/handle_error";

import {
  Tooltip,
  OverlayTrigger
} from "react-bootstrap"; 
import { Link } from 'react-router-dom';

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

class Tasks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      myTasks: [],
      countMyTasks: 0,
      allocatedTasks: [],
      countAllocatedTasks: 0,
      closedTasks: [],
      countClosedTasks: 0,
      isLoading: true,
      tabsClicked        : 'tab_1',
      get_access_data:false
    };
  }

  checkHandler = (event) => {
      event.preventDefault();
  };

  componentDidMount = () => {
    /* let emp_id = this.props.match.params.id;
    if (emp_id > 0) {
      this.getEmployeeTask();
    } else {
      return <Redirect to='/admin/employees' />
    } */

    const superAdmin  = getSuperAdmin(localStorage.admin_token);
    
    if(superAdmin === 1){
      this.setState({
        access: {
           view : true,
           add : true,
           edit : true,
           delete : true
         },
         get_access_data:true
     });
      let emp_id = this.props.match.params.id;
      if (emp_id > 0) {
        this.getEmployeeTask();
      } else {
        return <Redirect to='/admin/employees' />
      }
    }else{
      const adminGroup  = getAdminGroup(localStorage.admin_token);
      API.get(`/api/adm_group/single_access/${adminGroup}/${'EMPLOYEES_MANAGEMENT'}`)
      .then(res => {
        this.setState({
          access: res.data.data,
          get_access_data:true
        }); 

        if(res.data.data.view === true){
          let emp_id = this.props.match.params.id;
          if (emp_id > 0) {
            this.getEmployeeTask();
          } else {
            return <Redirect to='/admin/employees' />
          }
        }else{
          this.props.history.push('/admin/dashboard');
        }
        
      })
      .catch(err => {
        showErrorMessage(err,this.props);
      });
    }

  };

  getEmployeeTask = () => {
    let emp_id = this.props.match.params.id;
    let page = 1;
    this.setState({ emp_id : emp_id});
    /* this.setState({
      myTasks: [],
      allocatedTasks: [],
      closedTasks: [],
      isLoading: true
    }) */
  
    API.get( `/api/employees/my_tasks/${emp_id}?page=${page}` ).then(res => {
        this.setState({ myTasks: res.data.data, countMyTasks: res.data.count_my_tasks});
        API.get( `/api/employees/allocated_tasks/${emp_id}?page=${page}` ).then(res => {
            this.setState({ allocatedTasks: res.data.data, countAllocatedTasks: res.data.count_my_allocated_tasks});
            API.get( `/api/employees/closed_tasks/${emp_id}?page=${page}` ).then(res => {
                this.setState({ closedTasks: res.data.data, countClosedTasks: res.data.count_my_closed_tasks, isLoading:false});
            }).catch(err => {});
        }).catch(err => {});
    }).catch(err => {});
  }

  handleTabs = (event) =>{
        
    if(event.currentTarget.className === "active" ){
        //DO NOTHING
    }else{

        var elems          = document.querySelectorAll('[id^="tab_"]');
        var elemsContainer = document.querySelectorAll('[id^="show_tab_"]');
        var currId         = event.currentTarget.id;

        for (var i = 0; i < elems.length; i++){
            elems[i].classList.remove('active');
        }

        for (var j = 0; j < elemsContainer.length; j++){
            elemsContainer[j].style.display = 'none';
        }

        event.currentTarget.classList.add('active');
        event.currentTarget.classList.add('active');
        document.querySelector('#show_'+currId).style.display = 'block';
        this.setState({tabsClicked:currId});
    }
    // this.tabElem.addEventListener("click",function(event){
    //     alert(event.target);
    // }, false);
}

downloadXLSX = (e) => {
  let emp_id = this.props.match.params.id;
  let page = 1;
  e.preventDefault();

  if(this.state.tabsClicked === 'tab_1'){
      API.get( `/api/employees/my_tasks_excel/${emp_id}?page=${page}`,{responseType: 'blob'}).then(res => {
          //console.log(res);
          let url    = window.URL.createObjectURL(res.data);
          let a      = document.createElement('a');
          a.href     = url;
          a.download = 'my_tasks.xlsx';
          a.click();

      }).catch(err => {});
  }else if(this.state.tabsClicked === 'tab_2'){
      API.get( `/api/employees/allocated_tasks_excel/${emp_id}?page=${page}`,{responseType: 'blob'}).then(res => {
          //console.log(res);
          let url    = window.URL.createObjectURL(res.data);
          let a      = document.createElement('a');
          a.href     = url;
          a.download = 'allocated_tasks.xlsx';
          a.click();

      }).catch(err => {});
  }else if(this.state.tabsClicked === 'tab_3'){
      API.get( `/api/employees/closed_tasks_excel/${emp_id}?page=${page}`,{responseType: 'blob'}).then(res => {
          //console.log(res);
          let url    = window.URL.createObjectURL(res.data);
          let a      = document.createElement('a');
          a.href     = url;
          a.download = 'close_tasks.xlsx';
          a.click();

      }).catch(err => {});
  }
}

  render() {
    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy" >
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <h1>Tasks<small></small></h1>
            </section>
            <section className="content">
            <div className="box">
            <div className="box-body">
              <div className="row">
                <div className="col-xs-12">
                  <div className="nav-tabs-custom">
                    <ul className="nav nav-tabs">
                      <li className="active" onClick={(e) => this.handleTabs(e)} id="tab_1" >MY TASKS ({this.state.countMyTasks && this.state.countMyTasks > 0?this.state.countMyTasks:0})</li>
                      <li onClick={(e) => this.handleTabs(e)} id="tab_2">ALLOCATED TASKS ({this.state.countAllocatedTasks && this.state.countAllocatedTasks > 0?this.state.countAllocatedTasks:0})</li>
                      <li onClick={(e) => this.handleTabs(e)} id="tab_3">CLOSED TASKS ({this.state.countClosedTasks && this.state.countClosedTasks > 0?this.state.countClosedTasks:0})</li>

                      <li className="tabButtonSec pull-right">
                        {(this.state.tabsClicked === 'tab_1' && this.state.countMyTasks > 0) && 
                        <span onClick={(e) => this.downloadXLSX(e)} >
                            <LinkWithTooltip
                                tooltip={`Click here to download excel`}
                                href="#"
                                id="tooltip-my"
                                clicked={e => this.checkHandler(e)}
                            >
                                <i className="fas fa-download"></i>
                            </LinkWithTooltip>
                        </span>}

                        {(this.state.tabsClicked === 'tab_2' && this.state.countAllocatedTasks > 0) && 
                        <span onClick={(e) => this.downloadXLSX(e)} >
                            <LinkWithTooltip
                                tooltip={`Click here to download excel`}
                                href="#"
                                id="tooltip-allocated"
                                clicked={e => this.checkHandler(e)}
                            >
                                <i className="fas fa-download"></i>
                            </LinkWithTooltip>
                        </span>}

                        {(this.state.tabsClicked === 'tab_3' && this.state.countClosedTasks > 0) && 
                        <span onClick={(e) => this.downloadXLSX(e)} >
                            <LinkWithTooltip
                                tooltip={`Click here to download excel`}
                                href="#"
                                id="tooltip-close"
                                clicked={e => this.checkHandler(e)}
                            >
                                <i className="fas fa-download"></i>
                            </LinkWithTooltip>
                        </span>}
                        </li>

                      {/* <li className="pull-right pull-right-off">
                        <span className="text-muted pull-right"><i className="fas fa-calendar-alt"></i></span>

                        <div className="search-form pull-left">
                          <div className="form-group has-feedback">
                            <label htmlFor="search" className="sr-only">Search</label>
                            <input type="text" className="form-control" name="search" ></input>
                            <span className="glyphicon glyphicon-search form-control-feedback"></span>
                          </div>
                        </div>
                      </li> */}
                    </ul>

                    <div className="tab-content">

                      <div className="tab-pane active" id="show_tab_1">

                        {this.state.myTasks && this.state.myTasks.length > 0 && <MyTasksTable tableData={this.state.myTasks} countMyTasks={this.state.countMyTasks} empID = {this.state.emp_id} allTaskDetails={(e) => this.allTaskDetails()} />}

                        {this.state.myTasks && this.state.myTasks.length === 0 && <div className="noData">No Data Found</div>}

                      </div>

                      <div className="tab-pane" id="show_tab_2">

                        {this.state.allocatedTasks && this.state.allocatedTasks.length > 0 && <AllocatedTasksTable tableData={this.state.allocatedTasks} countMyTasks={this.state.countAllocatedTasks} empID = {this.state.emp_id} allTaskDetails={(e) => this.allTaskDetails()} />}

                        {this.state.allocatedTasks && this.state.allocatedTasks.length === 0 && <div className="noData">No Data Found</div>}
                      </div>

                      <div className="tab-pane" id="show_tab_3">
                        {this.state.closedTasks && this.state.closedTasks.length > 0 && <ClosedTasksTable tableData={this.state.closedTasks} countMyTasks={this.state.countClosedTasks} empID = {this.state.emp_id} allTaskDetails={(e) => this.allTaskDetails()} />}

                        {this.state.closedTasks && this.state.closedTasks.length === 0 && <div className="noData">No Data Found</div>}
                      </div>

                    </div>
                  </div>
                </div>
              </div></div></div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}

export default Tasks;
