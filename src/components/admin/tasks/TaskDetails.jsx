import React, { Component } from "react";
import { Row, Col, Panel, PanelGroup, OverlayTrigger, Tooltip } from "react-bootstrap";
import { Redirect, Link } from "react-router-dom";
import dateFormat from "dateformat";
import swal from "sweetalert";
import ReactHtmlParser from 'react-html-parser';
import Layout from "../layout/Layout";
import API from "../../../shared/admin-axios";
import { htmlDecode,localDate,localDateOnly } from "../../../shared/helper";
import { showErrorMessage } from "../../../shared/handle_error";

import exclamationImage from '../../../assets/images/exclamation-icon-black.svg';
import whitelogo from '../../../assets/images/drreddylogo_white.png';
import commenLogo from "../../../assets/images/drreddylogosmall.png";
import commenLogo1 from "../../../assets/images/drreddynoimg.png";
import downloadIcon from '../../../assets/images/download-icon.svg';

const s3bucket_diss_path     = `${process.env.REACT_APP_API_URL}/api/tasks/download_discussion_bucket/`;
const s3bucket_comment_diss_path     = `${process.env.REACT_APP_API_URL}/api/tasks/download_comment_bucket/`;
const s3bucket_task_diss_path     = `${process.env.REACT_APP_API_URL}/api/tasks/download_task_bucket/`;
const download_path = `${process.env.REACT_APP_API_URL}/api/drl/download/`;
const priority_arr = [
  { priority_id: 1, priority_value: "Low" },
  { priority_id: 2, priority_value: "Medium" },
  { priority_id: 3, priority_value: "High" }
];

const req_category_arr = [
  { request_category_id: 1, request_category_value: "New Order" },
  { request_category_id: 2, request_category_value: "Other" },
  { request_category_id: 3, request_category_value: "Complaint" },
  { request_category_id: 4, request_category_value: "Forecast" },
  { request_category_id: 5, request_category_value: "Payment" },
  { request_category_id: 6, request_category_value: "Notification" },
  { request_category_id: 7, request_category_value: "Request for Proforma Invoice" }
];

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
    return (        
        <Link to={href} onClick={clicked}>
          {children}
        </Link>      
    );    
}

function LinkWithTooltipText({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

class Tasks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      task: [],
      activity_log : [],
      CQT_details : [],
      comments : [],
      commentExists : false,
      discussDetails: [],
      discussTaskRef: '',
      discussReqName: '',
      discussFile: [],
      invalid_access:false,
      CQT_loader    : false,
      isLoading: true
    };
  }

  componentDidMount = () => {
    let task_id = this.props.match.params.id;
    if (task_id > 0) {
      this.getTaskDetails();
    } else {
      return <Redirect to='/admin/tasks' />
    }    
  };

  createAccordion = () => {

    let accordion = [];
    var sub_tasks = this.state.task.subTasks;

    for (let index = 0; index < sub_tasks.length; index++) {

        accordion.push(
            <Panel eventKey={sub_tasks[index].task_id} key={index}>
                <Panel.Heading>
                <Panel.Title toggle>{sub_tasks[index].task_ref}</Panel.Title>
                </Panel.Heading>
                <Panel.Body collapsible>
                    <div className="form-group">
                        <label>
                            Task: 
                        </label>
                        {sub_tasks[index].discussion === 1 &&
                            <Link to={{ pathname: '/user/discussion_details/'+sub_tasks[index].parent_id }} target="_blank" style={{cursor:'pointer'}}>{sub_tasks[index].task_ref}</Link>
                        }
                        {sub_tasks[index].discussion !== 1 &&
                            <Link to={{ pathname: `/user/task_details/${sub_tasks[index].task_id}/${sub_tasks[index].assignment_id}` }} target="_blank" style={{cursor:'pointer'}}>{sub_tasks[index].task_ref}</Link>
                        }
                    </div>
                    
                    <div className="form-group">
                        <label>
                            Description:
                        </label>

                        {sub_tasks[index].parent_id > 0 && ReactHtmlParser(sub_tasks[index].title) }
                        {sub_tasks[index].parent_id === 0 && ReactHtmlParser(sub_tasks[index].req_name) }
                    </div>
                    <div className="form-group">
                        <label>
                            Created:
                        </label>
                            {dateFormat(localDate(sub_tasks[index].date_added), "dd/mm/yyyy")}
                    </div>
                    {sub_tasks[index].first_name !== '' && <div className="form-group">
                        <label>
                        User Name:
                        </label>
                          {`${sub_tasks[index].first_name + " " + sub_tasks[index].last_name}`}
                    </div>}
                    {sub_tasks[index].assigned_by === -1 && <div className="form-group">
                        <label>
                            Assigned By:
                        </label>
                            System
                    </div>}
                    {sub_tasks[index].assigned_by > 0 && <div className="form-group">
                        <label>
                            Assigned By:
                        </label>
                            {`${sub_tasks[index].assigned_by_first_name+' '+sub_tasks[index].assigned_by_last_name+' ('+sub_tasks[index].assign_by_desig_name+')'}`}
                    </div>}
                    <div className="form-group">
                        <label>
                            Assigned To:
                        </label>
                            {`${sub_tasks[index].assigned_to_first_name+' '+sub_tasks[index].assigned_to_last_name+' ('+sub_tasks[index].assign_to_desig_name+')'}`}
                    </div>
                    
                </Panel.Body>
            </Panel>
        );
    }

    return accordion;
  }

  getComments = () => {
    if (this.state.comments.length > 0) {
      const comment_html = this.state.comments.map((comm, i) => (
        <div className="comment-list-wrap" key={i}>
          <div className="comment">
            <div className="row">
              <div className="col-md-10 col-sm-10 col-xs-12">
                <div className="imageArea">
                  <div className="imageBorder">
                    {(comm.profile_pic!='' && comm.profile_pic!=null) ? 
                      <img alt="noimage" src={comm.profile_pic} /> 
                      : 
                      <img alt="noimage" src={commenLogo} />
                    }
                  </div>
                </div>
                <div className="conArea">
                  <p>
                    {comm.first_name} {comm.last_name}
                  </p>
                  <span>{ReactHtmlParser(htmlDecode(comm.comment))}</span>
                </div>
                <div className="clearfix" />
              </div>
              <div className="col-md-2 col-sm-2 col-xs-12">
                <div className="dateArea">
                  
                  <p>{dateFormat(
                                                localDate(comm.post_date),
                                                "ddd, mmm dS, yyyy"
                                              )}</p>
                </div>
              </div>
            </div>
            {this.getCommentFile(comm.uploads)}
          </div>
        </div>
      ));

      return (
        <>
          <div className="disHead">
            <label>Task Comments:</label>
          </div>
          <div className="comment-list-main-wrapper">{comment_html}</div>
        </>
      );
    }
  };

  getCommentFile = (uploads) => {
    if(typeof uploads === 'undefined'){
      return '';
    }else{
      return (
        <ul className="conDocList">
          {uploads.map((files, j) => (
              <li key={j} >
                {/* <a href={`${comm_path}${files.tcu_id}`} target="_blank" download rel="noopener noreferrer">
                  {files.actual_file_name}
                </a> */}
                {process.env.NODE_ENV === 'development' ? 
                <LinkWithTooltip
                  // tooltip={`/emp_uploads/${files.new_file_name}`}
                  href="#"
                  id="tooltip-1"
                  clicked={e => this.redirectUrlTask(e,`${s3bucket_comment_diss_path}${files.tcu_id}`)}
                >
                  {files.actual_file_name}
                </LinkWithTooltip>
                :
                <LinkWithTooltip
                  // tooltip={`/emp_uploads/${files.new_file_name}`}
                  href="#"
                  id="tooltip-1"
                  clicked={e => this.redirectUrlTask(e,`${s3bucket_comment_diss_path}${files.tcu_id}`)}
                >
                  {files.actual_file_name}
                </LinkWithTooltip>
                }

              </li>
          ))}
        </ul>
      );
    }
  }

  getDiscussionFile = (uploads) => {
    if(typeof uploads === 'undefined'){
      return '';
    }else{
      return (
        <ul className="conDocList">
          {console.log("NODE ENV",process.env.NODE_ENV)}
          {uploads.map((files, j) => (
                <li key={j} >
                  {process.env.NODE_ENV === 'development' ? 
                  <LinkWithTooltip                    
                    href="#"
                    id="tooltip-1"                    
                    clicked={e => this.redirectUrlTask(e,`${s3bucket_diss_path}${files.upload_id}`)}                    
                  >
                    {files.actual_file_name}
                  </LinkWithTooltip>
                  : <LinkWithTooltip                    
                    href="#"
                    id="tooltip-1"                    
                    clicked={e => this.redirectUrlTask(e,`${s3bucket_diss_path}${files.upload_id}`)} 
                  >
                    {files.actual_file_name}
                  </LinkWithTooltip>
                  }
                </li>
          ))}
        </ul>
      );
    }
  }

  openCQTattachment = ( filepath ) => {
    API.get(filepath)
    .then(res => {
        window.open( res.data.data, '_blank');
    })
    .catch(err => {
        console.log(err);
    });
  }

  // getCQT = () => {

  //   if(this.state.CQT_details && this.state.CQT_details !== "" && this.state.CQT_loader === false){

  //       const { CQT_details } = this.state;

  //       return (
  //         <>
  //         <div className="boxPapanel content-padding">
  //          <div className="taskdetails">
  //          <div className="taskdetailsHeader">

  //           <div className="disHead">
  //             <h4>CQT Details:</h4>
  //           </div>
  //           <div className="cqtDetailsDeta-btm"> <div className="row">

  //             <div className="col-md-4 col-sm-6 col-xs-12">
  //                 <div className="form-group">
  //                   <label>Task Reference Number :  </label>
  //                   {CQT_details.task_reference}
  //                 </div>
  //               </div>
                
  //               <div className="col-md-4 col-sm-6 col-xs-12">
  //                 <div className="form-group">
  //                   <label>Title :  </label>
  //                     {CQT_details.title}
  //                 </div>
  //               </div>

  //               <div className="col-md-4 col-sm-6 col-xs-12">
  //                 <div className="form-group">
  //                   <label>Product Name : </label>
  //                     {CQT_details.product_name}
  //                 </div>
  //               </div>

  //               <div className="col-md-4 col-sm-6 col-xs-12">
  //                 <div className="form-group">
  //                   <label>Country Name : </label>
  //                     {CQT_details.country_name}
  //                 </div>
  //               </div>

  //               <div className="col-md-4 col-sm-6 col-xs-12">
  //                 <div className="form-group">
  //                   <label>Query Category : </label>
  //                     {CQT_details.query_category}
  //                 </div>
  //               </div>

  //               <div className="col-md-4 col-sm-6 col-xs-12">
  //                 <div className="form-group">
  //                   <label>Query Description :  </label>
  //                     { CQT_details.query_description }
  //                 </div>
  //               </div>

  //               <div className="col-md-4 col-sm-6 col-xs-12">
  //                 <div className="form-group">
  //                   <label>Criticality :  </label>
  //                     {CQT_details.criticality}
  //                 </div>
  //               </div>
               
  //               <div className="col-md-4 col-sm-6 col-xs-12">
  //                 <div className="form-group">
  //                   <label>Response Trail :  </label>
  //                     <p dangerouslySetInnerHTML={{__html: CQT_details.response_trail}}/>
  //                 </div>
  //               </div> 
  //           </div></div>
                     
                  
  //                 {CQT_details.files && CQT_details.files.length > 0 &&   
  //                     <>
  //                     <hr/>
  //                     <div className="disHead">
  //                       <h4>CQT Attachment:</h4>
  //                     </div>
  //                     <div className="cqtDetailsDeta-attachment">
  //                       <div className="row">
                          
  //                         {CQT_details.files[0] && CQT_details.files[0].length > 0 &&
                              
  //                             <div className="col-md-6 col-sm-6 col-xs-12">
  //                             <div className="form-group"><label>Uploaded from XCEED</label></div>
  //                             <ul className='cqtAttachment'>
  //                                 {CQT_details.files[0].map((ccp_file, w) => (
  //                                     <li key={w}>
  //                                       <div className="file-base">
  //                                         <div className="file-baseName">{ccp_file.OriginalFileName}</div>
  //                                         <div className="file-attactment">                                           

  //                                             <span onClick={(e) => this.openCQTattachment(`${download_path}${ccp_file.ID}`)}>
  //                                              <img alt="Download Icon" src={downloadIcon} />
  //                                             </span>
                                            
  //                                         </div>
  //                                       </div>
  //                                     </li> 
  //                                 ))}
  //                             </ul>
  //                             </div>
  //                         }

  //                         {CQT_details.files[1] && CQT_details.files[1].length > 0 &&
                              
  //                             <div className="col-md-6 col-sm-6 col-xs-12">
  //                               <div className="form-group"><label>Uploaded from CQT</label></div>
  //                               <ul className='cqtAttachment'>
  //                                 {CQT_details.files[1].map((drl_file, q) => (
  //                                     <li key={q}>
  //                                       <div className="file-base">
  //                                         <div className="file-baseName">{drl_file.name} </div>
  //                                         <div className="file-attactment">                                                
  //                                           <a href={drl_file.link} target="_blank" download rel="noopener noreferrer">
  //                                             <span>
  //                                               <img alt="Download Icon" src={downloadIcon} />
  //                                             </span>
  //                                           </a>
  //                                         </div>
  //                                       </div>
  //                                     </li>
  //                                   ))}
  //                               </ul>
  //                             </div>
  //                         }    

  //                     </div>
  //                     </div>    
  //                     </>                 
  //                 }

  //         </div>
  //         </div>
  //         </div> 
  //         </>
  //       );
  //   } else {
  //         return (
  //             <div className="loderOuter">
  //               <div className="loading_reddy_outer">
  //                   <div className="loading_reddy" >
  //                       <img src={whitelogo} alt="logoImage"/>
  //                   </div>
  //               </div>
  //             </div>
  //         );
  //     }
  // }

  getCCCust = () => {
    if(this.state.apiCompleted === true && this.state.custCCArr && this.state.custCCArr.length > 0 ){
      return (
        <Col xs={12}>
          <div className="cc_customer mb-20"> 
              <label>External Email notification{` `}
                <LinkWithTooltipText
                  tooltip={`Select list of users within Customer’s organization who will receive email notification`}
                  href="#"
                  id={`tooltip-menu`}
                  clicked={e => this.checkHandler(e)}
                >
                  <img src={exclamationImage} alt="exclamation" />
                </LinkWithTooltipText>
              </label>
              
              {this.state.custCCArr.map((cust, index) => (
                  <p key={index} >{cust.label}</p>
              ))}                                                                      
          </div>
      </Col>
      );
    }
  }

  redirectUrlTask = (event, path) => {
    event.preventDefault();
    window.open( path, '_self');
  }

  checkHandler = (event) => {
    event.preventDefault();
  };

  getTaskDetails = () => {
    let task_id = this.props.match.params.id;
    API.get( `/api/tasks/task_details/${task_id}` ).then(res => {
      this.setState({ task: res.data.data, commentExists: res.data.data.commentExists, comments: res.data.data.comments, isLoading:false});
      
      if(res.data.data.discussionExists){

        API.get(`api/tasks/task_discussions/${task_id}`)
        .then(res => {
          this.setState({
            discussDetails: res.data.data.discussion,
            discussTaskRef: res.data.data.task_ref,
            discussReqName: res.data.data.req_name,
            discussFile: res.data.data.files
          });
        })
        .catch(err => {
          console.log(err);
        });
      }
      
      // if(res.data.data.taskDetails.cqt > 0){   // CQT Show in details(need to create a new route) 

      //     this.setState({ CQT_loader : true });

      //     let taskId = res.data.data.taskDetails.task_id;
      //     let cqtId = res.data.data.taskDetails.cqt;

      //     API.get(`/api/drl/${taskId}/${cqtId}`)
      //     .then(cqtResponse => {
      //         this.setState({
      //             CQT_details : cqtResponse.data.data,
      //             CQT_loader : false
      //         })                       
      //     })
      //     .catch(err => {
      //       console.log(err);
      //     });
      // }

      API.get(`/api/customers/cc_cust_admtask/${task_id}`)
      .then(res => {
          var selectedCCTeam = [];
          for (let index = 0; index < res.data.data.length; index++) {
              const element = res.data.data[index];
              selectedCCTeam.push({
                value: element["customer_id"],
                label: element["first_name"] +" "+ element["last_name"]
              });
          }
          this.setState({custCCArr:selectedCCTeam,apiCompleted:true});

          
      })
      .catch(err => {
          console.log(err);
      });
    }).catch(err => {
      var errText = 'Invalid Access To This Page.';
      this.setState({invalid_access:true, isLoading:false}); 

      swal({
        closeOnClickOutside: false,
        title: "Error in page",
        text: errText,
        icon: "error"
      }).then(()=> {
        //this.props.history.push('/');
        this.props.history.push("/admin/tasks");
      });
    });
  };

  checkProductRequest(request_type, parent_id) {    
    if(request_type === null){
      return false;
    }else{
        if((request_type !== 24) && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkMarketRequest(request_type, parent_id) {
    if(request_type === null){
      return false;
    }else{
        if((request_type !== 24 && request_type !== 25 && request_type !== 26 && request_type !== 40) && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkGMPRequest(request_type, parent_id) {
    if(request_type === null){
      return false;
    }else{
        if((request_type === 40) && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkTGAEmail(request_type, parent_id) {
    if(request_type === null){
      return false;
    }else{
        if((request_type === 40) && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkApplicant(request_type, parent_id) {
    if(request_type === null){
      return false;
    }else{
        if((request_type === 40) && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkDocRequired(request_type, parent_id) {
    if(request_type === null){
      return false;
    }else{
        if((request_type === 40) && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkSamplesWorkingImpureRequest(request_type, parent_id){
    if(request_type === null){
        return false;
    }else{
        if(request_type === 1 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  setCreateDate = date_added => {
    var mydate = localDate(date_added);
    return dateFormat(mydate, "dd/mm/yyyy");
  };

  setDaysPending = due_date => {
    var dueDate = localDate(due_date);
    var today = new Date();
    var timeDiff = dueDate.getTime() - today.getTime();
    if (timeDiff > 0) {
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
      return diffDays;
    } else {
      return 0;
    }
  };

  setPriorityName = priority_id => {
    var ret = "Not Set";
    for (let index = 0; index < priority_arr.length; index++) {
      const element = priority_arr[index];
      if (element["priority_id"] === priority_id) {
        ret = element["priority_value"];
      }
    }

    return ret;
  };

  splitQuantity = (quantity) => {
    if(quantity){
      return quantity.split(" ");
    }
  }

  setDateOnly(date_value) {
    var mydate = localDateOnly(date_value);
    return dateFormat(mydate, "dd/mm/yyyy");
  }

  checkPharmacopoeia(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (
        ( request_type === 35 ||
          request_type === 36 ||
          request_type === 37 ||
          request_type === 22 ||
          request_type === 21 ||
          request_type === 19 ||
          request_type === 18 ||
          request_type === 17 ||
          request_type === 2 ||
          request_type === 3 ||
          request_type === 4 ||
          request_type === 10 ||
          request_type === 12 ||
          request_type === 1 ||
          request_type === 30 ||
          request_type === 32 ||
          request_type === 33 ||
          request_type === 41) &&
        parent_id === 0
      ) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkDMFNumber = (request_type, parent_id) => {
    if(request_type === null){
        return false;
    }else{
        if((request_type === 27 || request_type === 39) && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkNotificationNumber = (request_type, parent_id) => {
    if(request_type === null){
        return false;
    }else{
        if(request_type === 29 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkPolymorphicForm(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if ((request_type === 2 || request_type === 3) && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkAposDocumentType = (request_type, parent_id) => {
    if(request_type === null){
        return false;
    }else{
        if(request_type === 38 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  setReqCategoryName = req_cate_id => {
    var ret = "Not Set";
    for (let index = 0; index < req_category_arr.length; index++) {
      const element = req_category_arr[index];
      if (element["request_category_id"] === req_cate_id) {
        ret = element["request_category_value"];
      }
    }

    return ret;
  };

  checkStabilityDataType(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 13 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkAuditSiteName(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 9 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkAuditVistDate(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 9 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkQuantity(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if ((
        request_type === 7 
        || request_type === 34 
        || request_type === 23 
        || request_type === 25 
        || request_type === 31 
        || request_type === 41) 
        && parent_id === 0){
        return true;
      } else {
        return false;
      }
    }
  }

  checkBatchNo(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if ((request_type === 7 || request_type === 34) && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkForecastNotificationDate(request_type, parent_id) {
    if(request_type === null){
      return false;
    }else{
        if(request_type === 24 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkNatureOfIssue(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if ((request_type === 7 || request_type === 34) && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkRDD(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if ((request_type === 23 || request_type === 39 || request_type === 40 || request_type === 41) && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkRDORC = (request_type, parent_id) => {
    if(request_type === null){
        return false;
    }else{
        if(request_type === 28 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkAmountPending( request_type, parent_id ) {
    if(request_type === null){
      return false;
    }else{
        if(request_type === 25 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkDaysRemaining( request_type, parent_id ) {
    if(request_type === null){
      return false;
    }else{
        if(request_type === 25 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkPaymentStatus( request_type, parent_id ) {
    if(request_type === null){
      return false;
    }else{
        if(request_type === 25 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkPaymentDueDate( request_type, parent_id ) {
    if(request_type === null){
      return false;
    }else{
        if(request_type === 25 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }
  
  checkNotificationRequestType( request_type, parent_id ) {
    if(request_type === null){
      return false;
    }else{
        if(request_type === 26 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkNotificationTargetApproval( request_type, parent_id ) {
    if(request_type === null){
      return false;
    }else{
        if(request_type === 26 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkNotificationTargetImplementation ( request_type, parent_id ){
    if(request_type === null){
      return false;
    }else{
        if(request_type === 26 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  getActivityLog = () => {
    API.get(`/api/tasks/task_activity_log/${this.state.task.taskDetails.task_id}`)
    .then(res => {
      this.setState({activity_log:res.data.data});
    })
    .catch(err => {
      console.log(err);
    });
  }

  activityLogTable = () => {
    if (this.state.activity_log.length > 0) {
      const comment_html = this.state.activity_log.map((act, i) => (
        <div className="comment-list-wrap" key={i}>
          <div className="comment">
            <div className="row">
              <div className="col-md-10 col-sm-10 col-xs-12">
                <div className="imageArea">
                  <div className="imageBorder">
                    <img alt="noimage" src={commenLogo} />
                  </div>
                </div>
                <div className="conArea">
                  <span>{act.description}</span>
                </div>
                <div className="clearfix" />
              </div>
              <div className="col-md-2 col-sm-2 col-xs-12">
                <div className="dateArea">
                  <p>{act.display_date_added}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      ));
      return (
        <>
          <div className="disHead">
            <label>Activity Logs:</label>
          </div>
          <div className="comment-list-main-wrapper">{comment_html}</div>
        </>
      );
    }
  }

  handleTabs = (event) =>{
        
    if(event.currentTarget.className === "active" ){
        
    }else{
        var elems          = document.querySelectorAll('[id^="tab_"]');
        var elemsContainer = document.querySelectorAll('[id^="show_tab_"]');
        var currId         = event.currentTarget.id;

        for (var i = 0; i < elems.length; i++){
            elems[i].classList.remove('active');
        }

        for (var j = 0; j < elemsContainer.length; j++){
            elemsContainer[j].style.display = 'none';
        }

        event.currentTarget.classList.add('active');
        event.currentTarget.classList.add('active');
        document.querySelector('#show_'+currId).style.display = 'block';

        if(currId === 'tab_4'){
          this.getActivityLog();
        }
    }    
  }

  handleTabs2 = (event) =>{
        
    if(event.currentTarget.className === "active" ){
        //DO NOTHING
    }else{

        var elems          = document.querySelectorAll('[id^="d_tab_"]');
        var elemsContainer = document.querySelectorAll('[id^="show_d_tab_"]');
        var currId         = event.currentTarget.id;

        for (var i = 0; i < elems.length; i++){
            elems[i].classList.remove('active');
        }

        for (var j = 0; j < elemsContainer.length; j++){
            elemsContainer[j].style.display = 'none';
        }

        event.currentTarget.classList.add('active');
        event.currentTarget.classList.add('active');
        document.querySelector('#show_'+currId).style.display = 'block';

        this.setState({ files:[],filesHtml:""});
    }  
  }

  changeDisplayDiscussion = (e) => {
    this.setState({loaderCCemp : true});
    API.put(`/api/tasks/display_discussion/${this.state.taskDetails.task_id}`)
    .then(resp => {
        this.setState({loaderCCemp : false});
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: resp.data.data,
            icon: "success"
          })                  
    })
    .catch(err => {
        showErrorMessage(err,2,this.props);
    });
  }

  getDisplayDiscussionForm = () => {
    return (
        <>
        <label class="customCheckBox">Display Customer Discussions <input
              type="checkbox"
              name="display_discussion"
              value="1"
              onChange={(e) => this.changeDisplayDiscussion(e)}
              defaultChecked={(this.state.task.taskDetails.display_discussion === 1) ? true:false}
          /><span class="checkmark-check"></span></label>
        </>  
    );
  }

  render() {
    if (this.state.isLoading) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy" >
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {

      if(this.state.invalid_access=== false){  
        if (this.state.task.taskDetailsFiles.length > 0) {
          var fileList = this.state.task.taskDetailsFiles.map((file,k) => (
            <li key={k}>           
              {process.env.NODE_ENV === 'development' ? 
              <LinkWithTooltip                
                  href="#"
                  id="tooltip-1"
                  clicked={e => this.redirectUrlTask(e,`${s3bucket_task_diss_path}${file.upload_id}`)}
                >
                {file.actual_file_name}
              </LinkWithTooltip>
              :
              <LinkWithTooltip                
                  href="#"
                  id="tooltip-1"
                  clicked={e => this.redirectUrlTask(e,`${s3bucket_task_diss_path}${file.upload_id}`)}
                >
                {file.actual_file_name}
              </LinkWithTooltip>
              }
            </li>
          ));
        }
        return (
          <Layout {...this.props}>
            <div className="content-wrapper">
              <section className="content-header">
                <h1>Tasks Details<small></small></h1>
                <input
                  type="button"
                  value="Go Back"
                  className="btn btn-warning btn-sm"
                  onClick={() => {
                    window.history.go(-1);
                    return false;
                  }}
                  style={{right:'9px',position:'absolute',top:'13px'}}
                />
              </section>
              <section className="content">
                <div className="box">
                  <div className="box-body">
                    <div className="row">
                      <div className="col-xs-12">
                        <h3> {this.state.task.taskDetails.req_name} | {this.state.task.taskDetails.task_ref}</h3>
                        <div className="nav-tabs-custom">
                          <ul className="nav nav-tabs">
                            <li className="active" onClick={(e) =>this.handleTabs(e)} id="tab_1" >TASK DETAILS</li>
                            {this.state.commentExists && (this.state.task.taskDetails.cqt === null || this.state.task.taskDetails.cqt === 0) && <li onClick={(e) =>this.handleTabs(e)} id="tab_2">INTERNAL COMMENTS</li>}
                            {this.state.task.discussionExists && <li onClick = {(e) => this.handleTabs(e)}  id="tab_3">CUSTOMER DISCUSSIONS</li>}
                            <li onClick={(e) =>this.handleTabs(e)} id="tab_4">ACTIVITY LOG</li>                          
                          </ul>

                        <div className="tab-content">

                          <div className="tab-pane active" id="show_tab_1">
                          {this.state.task.taskDetails.parent_id > 0 &&
                            <div className="boxPapanel content-padding task-accept-decline-main">
                                <div className="taskdetails">
                                  <div className="taskdetailsHeader">
                                      <div className="commentBox1">
                                        <div className="row">
                                          <div className="col-sm-12 col-xs-12">
                                          {/* taskdetails Panel Top Listing*/}
                                            <div className="tdTplist">
                                              <ul>
                                                <li>
                                                  <div className="form-group">
                                                    <label>Task:</label> {this.state.task.taskDetails.task_ref}
                                                  </div>
                                                </li>
                                                <li>
                                                  <div className="form-group">
                                                    <label>Created On:</label>{" "}
                                                    {this.setCreateDate(
                                                      this.state.task.taskDetails.date_added
                                                    )}
                                                  </div>
                                                </li>
                                                <li>
                                                  <div className="form-group">
                                                    <label>Days Pending:</label>{" "}
                                                    {this.setDaysPending(this.state.task.taskDetails.assign_due_date)}
                                                  </div>
                                                </li>
                                                <li>
                                                  <div className="form-group">
                                                    <label>Priority:</label>{" "}
                                                    {this.setPriorityName(
                                                      this.state.task.taskDetails.priority
                                                    )}
                                                  </div>
                                                </li>
                                                {this.state.task.taskDetails.parent_id > 0 && this.state.task.taskDetails.discussion !== 1 && this.state.task.taskDetails.sla_status !== 0 &&
                                                  <li>
                                                    <div className="form-group">
                                                      <label>SLA Status:</label> {this.getSLAStatus(this.state.task.taskDetails.sla_status)}
                                                    </div>
                                                  </li>
                                                }
                                                {this.state.task.showDisplayCheckBox === 1 &&
                                                  <li>
                                                    <div className="form-group">
                                                      {this.getDisplayDiscussionForm()}
                                                    </div>
                                                  </li>
                                                }
                                              </ul>
                                            </div>
                                            {/* taskdetails Panel Top Listing*/}
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                          </div>}
                            {this.state.task.taskDetails && this.state.task.taskDetails!= '' && 
                              (<div className= "task_details_tab_1"> 

                                  <Row className="row">
                                    {(this.state.task.taskDetails.company_name !== "") && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Customer Name:</label>{" "}
                                          <span>{this.state.task.taskDetails.company_name}</span>
                                        </div>
                                      </Col>
                                    )}
                                    {(this.state.task.taskDetails.first_name !== "" || this.state.task.taskDetails.last_name !== "" ) && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>User Name:</label>{" "}
                                          <span>{htmlDecode(this.state.task.taskDetails.first_name)} {" "} {htmlDecode(this.state.task.taskDetails.last_name)}</span>
                                        </div>
                                      </Col>
                                    )}
                                    {this.checkProductRequest(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id) 
                                      &&
                                        <Col xs={12} sm={6} md={4}>
                                          <div className="form-group">
                                            <label>Product:</label>{" "}
                                            {htmlDecode(this.state.task.taskDetails.product_name)}
                                          </div>
                                        </Col>
                                    }
                                    {this.checkMarketRequest(this.state.task.taskDetails.request_type,this.state.task.taskDetails.parent_id) 
                                      &&
                                        <Col xs={12} sm={6} md={4}>
                                          <div className="form-group">
                                            <label>Market:</label>{" "}
                                            {htmlDecode(this.state.task.taskDetails.country_name)}
                                          </div>
                                        </Col>
                                    }

                                    {this.checkGMPRequest(this.state.task.taskDetails.request_type,this.state.task.taskDetails.parent_id) 
                                      && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>GMP Clearance ID:</label>{" "}
                                          {htmlDecode(this.state.task.taskDetails.gmp_clearance_id)}
                                        </div>
                                      </Col>
                                    )}                                

                                    {this.checkTGAEmail(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id) 
                                      && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Email ID:</label>{" "}
                                          {htmlDecode(this.state.task.taskDetails.tga_email_id)}
                                        </div>
                                      </Col>
                                    )}

                                    {this.checkApplicant(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id) 
                                      && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Applicant's Name and Address:</label>{" "}
                                          {htmlDecode(this.state.task.taskDetails.applicant_name)}
                                        </div>
                                      </Col>
                                    )}

                                    {this.checkDocRequired(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id) 
                                      && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Documents Required:</label>{" "}
                                          {htmlDecode(this.state.task.taskDetails.doc_required)}
                                        </div>
                                      </Col>
                                    )}

                                    {this.checkSamplesWorkingImpureRequest(
                                      this.state.task.taskDetails.request_type,
                                      this.state.task.taskDetails.parent_id
                                      ) === true && this.state.task.taskDetails.service_request_type.indexOf('Samples') !== -1 && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                        
                                          <label>Samples:</label>
                                          <p>Number of batches : {this.state.task.taskDetails.number_of_batches}</p>
                                          {this.state.task.taskDetails.quantity && 
                                            <p>Quantity : {this.splitQuantity(this.state.task.taskDetails.quantity)[0]} {this.splitQuantity(this.state.task.taskDetails.quantity)[1]}</p>
                                          }
                                          
                                        </div>
                                      </Col>
                                    )}

                                    {this.checkSamplesWorkingImpureRequest(
                                      this.state.task.taskDetails.request_type,
                                      this.state.task.taskDetails.parent_id
                                      ) === true && this.state.task.taskDetails.service_request_type.indexOf('Impurities') !== -1 && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Impurities:</label>
                                          <p>Required impurity : {htmlDecode(this.state.task.taskDetails.specify_impurity_required)}</p>

                                          {this.state.task.taskDetails.impurities_quantity && 
                                            <p>Quantity : {this.splitQuantity(this.state.task.taskDetails.impurities_quantity)[0]} {this.splitQuantity(this.state.task.taskDetails.impurities_quantity)[1]} </p>
                                          }
                                        </div>
                                      </Col>
                                    )}

                                    {this.checkSamplesWorkingImpureRequest(
                                      this.state.task.taskDetails.request_type,
                                      this.state.task.taskDetails.parent_id
                                      ) === true && this.state.task.taskDetails.service_request_type.indexOf('Working Standards') !== -1 && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Working Standards:</label>
                                          {this.state.task.taskDetails.working_quantity && 
                                            <p>Quantity : {this.splitQuantity(this.state.task.taskDetails.working_quantity)[0]} {this.splitQuantity(this.state.task.taskDetails.working_quantity)[1]} </p>
                                          }
                                        </div>
                                      </Col>
                                    )}

                                    {this.checkSamplesWorkingImpureRequest(
                                      this.state.task.taskDetails.request_type,
                                      this.state.task.taskDetails.parent_id
                                      ) === true && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Shipping Address:</label>
                                          <p>{ReactHtmlParser(htmlDecode(this.state.task.taskDetails.shipping_address))}</p>

                                          
                                        </div>
                                      </Col>
                                    )}

                                    {this.checkPharmacopoeia(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id
                                    ) === true && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Pharmacopoeia:</label>{" "}
                                          {htmlDecode(this.state.task.taskDetails.pharmacopoeia)}
                                        </div>
                                      </Col>
                                    )}

                                    {this.checkPolymorphicForm(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id
                                    ) === true && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Polymorphic Form:</label>{" "}
                                          {htmlDecode(this.state.task.taskDetails.polymorphic_form)}
                                        </div>
                                      </Col>
                                    )}

                                    {this.checkDMFNumber(this.state.task.taskDetails.request_type,this.state.task.taskDetails.parent_id
                                    ) === true && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>{this.state.task.taskDetails.request_type === 39 ? "DMF/CEP:" : "DMF Number:"}</label>{" "}
                                          {htmlDecode(this.state.task.taskDetails.dmf_number)}
                                        </div>
                                      </Col>
                                    )}

                                    {this.checkNotificationNumber(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id
                                    ) === true && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Notification Number:</label>{" "}
                                          {htmlDecode(this.state.task.taskDetails.notification_number)}
                                        </div>
                                      </Col>
                                    )}

                                    {this.checkAposDocumentType(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id
                                    ) === true && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Document Type:</label>{" "}
                                          {htmlDecode(this.state.task.taskDetails.apos_document_type)}
                                        </div>
                                      </Col>
                                    )}

                                    {this.state.task.taskDetails.parent_id === 0 && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Request Category:</label>{" "}
                                          {this.setReqCategoryName(
                                            this.state.task.taskDetails.request_category
                                          )}
                                        </div>
                                      </Col>
                                    )}
                                    
                                    {this.checkStabilityDataType(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id
                                    ) === true && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Stability Data Type:</label>{" "}
                                          {this.state.task.taskDetails.stability_data_type}
                                        </div>
                                      </Col>
                                    )}

                                  
                                    {this.checkAuditSiteName(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id
                                    ) === true && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Site Name:</label>{" "}
                                          {htmlDecode(this.state.task.taskDetails.audit_visit_site_name)}
                                        </div>
                                      </Col>
                                    )}

                                    
                                    {this.checkAuditVistDate(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id
                                    ) === true && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Audit/Visit Date:</label>{" "}
                                          {this.state.task.taskDetails.request_audit_visit_date !== null && this.setDateOnly(this.state.task.taskDetails.request_audit_visit_date)}
                                        </div>
                                      </Col>
                                    )}

                                    {this.checkQuantity(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id
                                    ) === true && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Quantity:</label>{" "}
                                          {this.state.task.taskDetails.quantity}
                                        </div>
                                      </Col>
                                    )}

                                    {this.checkBatchNo(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id
                                    ) === true && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Batch No:</label>{" "}
                                          {this.state.task.taskDetails.batch_number}
                                        </div>
                                      </Col>
                                    )}

                                    {this.checkForecastNotificationDate(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id
                                    ) === true && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Notification Date:</label>{" "}
                                          {this.setCreateDate(
                                            this.state.task.taskDetails.date_added
                                          )}
                                        </div>
                                      </Col>
                                    )}

                                    {this.checkNatureOfIssue(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id
                                    ) === true && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Nature Of Issue:</label>{" "}
                                          {htmlDecode(this.state.task.taskDetails.nature_of_issue)}
                                        </div>
                                      </Col>
                                    )}      

                                    {this.checkRDD(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id
                                    ) === true && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>
                                            {(this.state.task.taskDetails.request_type === 39 || this.state.task.taskDetails.request_type === 40)
                                            ?
                                              'Requested Deadline:'
                                            :
                                              'Required Date Of Delivery:'
                                            }
                                          </label>{" "}
                                          {this.state.task.taskDetails.rdd !== null && this.setDateOnly(this.state.task.taskDetails.rdd) }
                                        </div>
                                      </Col>
                                    )}

                                    {this.checkRDORC(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id
                                    ) === true && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Requested date of response/closure:</label>{" "}
                                          {this.state.task.taskDetails.rdfrc && this.state.task.taskDetails.rdfrc !== null && this.state.task.taskDetails.rdfrc }
                                        </div>
                                      </Col>
                                    )}            

                                    {this.state.task.taskDetails.parent_id > 0 && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Title:</label> {ReactHtmlParser(this.state.task.taskDetails.title)}
                                        </div>
                                      </Col>
                                    )}

                                    {this.state.task.taskDetails.parent_id > 0 && (
                                      <Col xs={12} sm={12} md={12}>
                                        <div className="form-group">
                                          <label>Content:</label>{" "}
                                          {ReactHtmlParser(htmlDecode(this.state.task.taskDetails.content))}
                                        </div>
                                      </Col>
                                    )}

                                    {this.checkAmountPending(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id
                                    ) === true && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Amount Pending:</label>{" "}
                                          {this.state.task.taskDetails.payment_pending}
                                        </div>
                                      </Col>
                                    )}

                                    {this.checkDaysRemaining(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id
                                    ) === true && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Days Remaining:</label>{" "}
                                          {this.state.task.taskDetails.days_remaining}
                                        </div>
                                      </Col>
                                    )}

                                    {this.checkPaymentStatus(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id
                                    ) === true && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>status:</label>{" "}
                                          {this.state.task.taskDetails.payment_status}
                                        </div>
                                      </Col>
                                    )}

                                    {this.checkPaymentDueDate(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id
                                    ) === true && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Due Date:</label>{" "}
                                          {this.setCreateDate(
                                            this.state.task.taskDetails.due_date
                                          )}
                                        </div>
                                      </Col>
                                    )}

                                    {this.checkNotificationRequestType(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id
                                    ) === true && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Request Type:</label>{" "}
                                          {this.state.task.taskDetails.notification_type}
                                        </div>
                                      </Col>
                                    )}
                                    
                                    {this.checkNotificationTargetApproval(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id
                                    ) === true && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Target for Approval/Feedback:</label>{" "}
                                          {this.state.task.taskDetails.target_for_approval !== null && this.setDateOnly( this.state.task.taskDetails.target_for_approval)}
                                        </div>
                                      </Col>
                                    )}

                                    {this.checkNotificationTargetImplementation(this.state.task.taskDetails.request_type, this.state.task.taskDetails.parent_id
                                    ) === true && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Target for Implementation:</label>{" "}
                                          {this.state.task.taskDetails.target_for_implementation !== null && this.setDateOnly(this.state.task.taskDetails.target_for_implementation)}
                                        </div>
                                      </Col>
                                    )}

                                    {this.state.task.taskDetails.parent_id === 0 && (
                                      <Col xs={12} sm={12} md={12}>
                                        <div className="form-group forecast-image">
                                          <label>Requirement:</label>{" "}
                                          {ReactHtmlParser(htmlDecode(this.state.task.taskDetails.content))}
                                        </div>
                                      </Col>
                                    )}


                                    {(this.state.task.taskDetails.ccp_posted_by > 0) && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Submitted By:</label>{" "}
                                          <p>{this.state.task.taskDetails.emp_posted_by_first_name}{" "}
                                          {this.state.task.taskDetails.emp_posted_by_last_name}{" "}
                                          ({this.state.task.taskDetails.emp_posted_by_desig_name})
                                          </p>
                                        </div>
                                      </Col>
                                    )}

                                    {(this.state.task.taskDetails.submitted_by > 0) && (
                                      <Col xs={12} sm={6} md={4}>
                                        <div className="form-group">
                                          <label>Submitted By:</label>{" "}
                                          {this.state.task.taskDetails.agnt_first_name}{" "}
                                          {this.state.task.taskDetails.agnt_last_name}{" "}
                                          (Agent)
                                        </div>
                                      </Col>
                                    )}

                                    {this.getCCCust()}

                                    {this.state.task.taskDetailsFiles.length > 0 &&
                                      <Col xs={12}>
                                          <div className="mb-20"> 
                                              <div className="form-group"><label>Attachment</label>
                                                <div className="cqtDetailsDeta-btm">
                                                  <ul className='conDocList'>{fileList}</ul>  
                                                </div>  
                                              </div>                                                                      
                                          </div>
                                      </Col>
                                    }
                                  </Row>

                                  {this.state.task.subTasks.length > 0 && 
                                    <>
                                    <div className="clearfix"></div>
                                    <div className="form-group m-t-15 m-b-0"><label>Sub Tasks:</label></div>

                                    {this.state.task.taskDetails.parent_id === 0 && 
                                    this.state.has_discussion === 1 &&
                                    <Row>
                                      
                                    <Col xs={12} sm={6} md={12}>
                                        <div className="form-group">
                                        <Link
                                          to={{ pathname: '/user/discussion_details/'+this.state.task.taskDetails.task_id }} target="_blank" style={{cursor:'pointer'}}
                                          className="plus-request edit-button"
                                        >                                        
                                          Discussions
                                        </Link>
                                        </div>
                                      </Col>
                                      </Row>
                                      
                                    }   
                        
                                    
                                    <PanelGroup accordion id="task_panel" className="task_panel_accordian" defaultActiveKey={this.state.task.subTasks[0].task_id} >
                                        {this.createAccordion()}
                                    </PanelGroup>
                                    </>
                              }
                              </div>)
                            }
                          </div>

                          {this.state.commentExists && (this.state.task.taskDetails.cqt === null || this.state.task.taskDetails.cqt === 0) && 
                          <div className="tab-pane" id="show_tab_2">
                            <div className="boxPapanel content-padding">
                              <div className="taskdetails">
                                {this.getComments()}
                              </div>
                            </div>
                          </div>}

                          {this.state.task.discussionExists && this.state.discussDetails.length > 0 && <div className="tab-pane" id="show_tab_3">
                            <div className="row">
                              <div className="col-xs-12">

                                <div className="nav-tabs-custom discussion-tab">

                                  <ul className="nav nav-tabs">
                                    <li className="active" onClick={(e) => this.handleTabs2(e)} id="d_tab_1" >Discussion Details</li>
                                    <li onClick={(e) => this.handleTabs2(e)} id="d_tab_2">Files</li>
                                  </ul>

                                  <div className="tab-content">

                                    <div className="tab-pane active" id="show_d_tab_1">
                                      {/* <section className="content-header">
                                        <h1>Discussion Details
                                          {this.state.task.taskDetails.customer_status === 3 && <span style={{color: '#e6021d',fontSize: '12px',paddingLeft: '12px'}}>
                                            (Messages will not reach customer since the account is inactive)
                                          </span>}
                                        </h1>
                                      </section> */}
                                      <section className="content">
                                        <div className="boxPapanel content-padding">
                                          {/* <div className="disHead">
                                            <h3>
                                              {this.state.discussReqName} | {this.state.discussTaskRef}
                                            </h3>
                                          </div> */}

                                          <div className="comment-list-main-wrapper">
                                            {this.state.discussDetails.map((comments, i) => (
                                              <div key={i} className="comment">
                                                <div className="row">
                                                  <div className="col-md-10 col-sm-10 col-xs-12">
                                                    <div className="imageArea">
                                                      <div className="imageBorder">

                                                        {(comments.added_by_type === "E" && comments.emp_profile_pic!='' && comments.emp_profile_pic!=null) ? <img alt="noimage" src={comments.emp_profile_pic} /> : ''}

                                                        {(comments.added_by_type === "C" && comments.cust_profile_pic!='' && comments.cust_profile_pic!=null) ? <img alt="noimage" src={comments.cust_profile_pic} /> : ''}

                                                        {(comments.added_by_type === "A" && comments.agnt_profile_pic!='' && comments.agnt_profile_pic!=null) ? <img alt="noimage" src={comments.agnt_profile_pic} /> : ''}

                                                        {(comments.emp_profile_pic =='' || comments.emp_profile_pic==null) && (comments.cust_profile_pic =='' || comments.cust_profile_pic==null) && 
                                                        (comments.agnt_profile_pic =='' || comments.agnt_profile_pic==null) && <img alt="noimage" src={commenLogo} />}
                                                      </div>
                                                    </div>
                                                    <div className="conArea">
                                                      {comments.added_by_type === "C" && <p>{`${comments.cust_fname} ${comments.cust_lname}`}</p> }

                                                      {comments.added_by_type === "E" && <p>{`${comments.emp_fname} ${comments.emp_lname} (DRL)`}</p> }

                                                      {comments.added_by_type === "A" && <p>{`${comments.agnt_first_name} ${comments.agnt_last_name} (Agent)`}</p> }
                                                      <span>{ReactHtmlParser(ReactHtmlParser(comments.comment))}</span>
                                                      {this.getDiscussionFile(comments.uploads)}
                                                    </div>
                                                    <div className="clearfix" />
                                                  </div>

                                                  <div className="col-md-2 col-sm-2 col-xs-12">
                                                    <div className="dateArea">
                                                      <p>
                                                        {dateFormat(
                                                          localDate(comments.date_added),
                                                          "ddd, mmm dS, yyyy"
                                                        )}
                                                      </p>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            ))}
                                          </div>
                                        </div>
                                      </section>
                                    </div>

                                    <div className="tab-pane" id="show_d_tab_2">
                                      {/* <section className="content-header">
                                        <h1>File Details</h1>
                                      </section> */}
                                      <section className="content">
                                        <div className="boxPapanel content-padding">
                                          <div className="disHead">                                           
                                          </div>

                                          <div className="comment-list-main-wrapper">

                                            {this.state.discussFile &&
                                              this.state.discussFile.length > 0 ?
                                              this.state.discussFile.map((file, i) => (
                                                <div key={i} className="comment">
                                                  <div className="row">
                                                    <div className="col-md-10 col-sm-10 col-xs-12">
                                                      <div className="imageArea">
                                                        <div className="imageBorder">
                                                          <img alt="noimage" src={commenLogo1} />
                                                        </div>
                                                      </div>
                                                      <div className="conArea">
                                                        {process.env.NODE_ENV === 'development' ?       
                                                        <LinkWithTooltip                                                          
                                                          href="#"
                                                          id="tooltip-1"
                                                          clicked={e => this.redirectUrlTask(e,`${s3bucket_diss_path}${file.upload_id}`)}
                                                        >
                                                          {file.actual_file_name}
                                                        </LinkWithTooltip>
                                                        :
                                                        <LinkWithTooltip                                                          
                                                          href="#"
                                                          id="tooltip-1"
                                                          clicked={e => this.redirectUrlTask(e,`${s3bucket_diss_path}${file.upload_id}`)}
                                                        >
                                                          {file.actual_file_name}
                                                        </LinkWithTooltip>
                                                        }

                                                      </div>
                                                      <div className="clearfix" />
                                                    </div>

                                                    <div className="col-md-2 col-sm-2 col-xs-12">
                                                      <div className="dateArea">
                                                        <p>
                                                          {dateFormat(
                                                            localDate(file.date_added),
                                                            "ddd, mmm dS, yyyy"
                                                          )}
                                                        </p>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              )): `No Files Found`}
                                          </div>
                                        </div>
                                      </section>

                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>}

                          <div className="tab-pane" id="show_tab_4">
                            <div className="boxPapanel content-padding">
                              <div className="taskdetails">
                                {this.activityLogTable()}
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </Layout>
        );
      }else{
        return (
          <>
            
          </>
        );
      }
    
    }
  }
}

export default Tasks;
