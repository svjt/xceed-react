import React, { Component } from 'react';
import {BootstrapTable,TableHeaderColumn} from 'react-bootstrap-table';
import { htmlDecode } from '../../../shared/helper';

//import dateFormat from 'dateformat';

import StatusColumn from './StatusColumn';
import { Link } from 'react-router-dom';


import {
  Tooltip,
  OverlayTrigger
} from "react-bootstrap";


const priority_arr = [
    {priority_id:1,priority_value:'Low'},
    {priority_id:2,priority_value:'Medium'},
    {priority_id:3,priority_value:'High'}
];


const getStatusColumn = refObj => (cell,row) => {
    if(row.discussion !== 1){
        return <StatusColumn rowData={row} />
    }else{
        return "";
    }
}

const setDate = refOBj => (cell,row) => {
    return (    
      <LinkWithTooltip
          tooltip={cell}
          href="#"
          id="tooltip-1"
          clicked={e => refOBj.checkHandler(e)}
        >
        {cell}  
      </LinkWithTooltip>
    );
  };

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const setDescription = refOBj => (cell,row) =>{
    if(row.parent_id > 0){
        return (
            <LinkWithTooltip
                tooltip={`${row.title}`}
                href="#"
                id="tooltip-1"
                clicked={e => refOBj.checkHandler(e)}
              >
              {row.title}
            </LinkWithTooltip>
        );
      }else{
        return (      
          <LinkWithTooltip
            tooltip={`${row.req_name}`}
            href="#"
            id="tooltip-1"
            clicked={e => refOBj.checkHandler(e)}
          >
            {row.req_name}
          </LinkWithTooltip>
          );
      }
}

// const setCreateDate = refObj => cell =>{
//     //var replacedDate = cell.split('T');
//     //var mydate = new Date(cell);
//     //console.log(mydate);
//     //return dateFormat(mydate, "dd/mm/yyyy");
//     return cell
// }

// const setDaysPending = refObj => (cell,row) =>{
//     var selDueDate;
//     if(row.assigned_by > 0){
//         selDueDate = row.new_due_date;
//     }else{
//         selDueDate = cell;
//     }
    
//     var dueDate = new Date(selDueDate);
//     var today  = new Date();
//     var timeDiff = dueDate.getTime() - today.getTime();
//     //console.log(timeDiff);
//     if(timeDiff > 0 ){
//         var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
//         return diffDays;
//     }else{
//         return 0;
//     }
    
// }

const clickToShowTasks = refObj => (cell,row) =>{
  return `${cell}`;
}

const setEmpCustName = refOBj => (cell, row) => {
  //return `${row.first_name} ${row.last_name}`;
  return (    
    <LinkWithTooltip
        tooltip={`${row.first_name + " " + row.first_name }`}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {row.first_name + " " + row.first_name}
    </LinkWithTooltip>
  );
};

// const setPriorityName = refObj => (cell,row) =>{
//     var ret = 'Set Priority';
//     for (let index = 0; index < priority_arr.length; index++) {
//         const element = priority_arr[index];
//         if(element['priority_id'] === cell ){
//             ret = element['priority_value'];
//         }
//     }

//     return ret

// }

const setCompanyName = refOBj => (cell, row) => {
  //return htmlDecode(cell);
  return (    
    <LinkWithTooltip
        tooltip={htmlDecode(cell)}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {htmlDecode(cell)}
    </LinkWithTooltip>
  );
};

const setAssignedTo = refOBj => (cell,row) => {
    //return row.emp_first_name+' '+row.emp_last_name+' ('+row.desig_name+')';
    return (      
      <LinkWithTooltip
        tooltip={`${row.emp_first_name+' '+row.emp_last_name+' ('+row.desig_name+')'}`}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {row.emp_first_name+' '+row.emp_last_name+' ('+row.desig_name+')'}
      </LinkWithTooltip>
      );
}

const setAssignedBy = refOBj => (cell, row) => {
  if(row.assigned_by == '-1' ){
    return (    
      <LinkWithTooltip
          tooltip={`System`}
          href="#"
          id="tooltip-1"
          clicked={e => refOBj.checkHandler(e)}
        >
        System  
      </LinkWithTooltip>
    );
  }else{
    return (    
      <LinkWithTooltip
          tooltip={`${row.ab_emp_first_name + " " + row.ab_emp_last_name + " (" + row.ab_emp_desig_name + ")"}`}
          href="#"
          id="tooltip-1"
          clicked={e => refOBj.checkHandler(e)}
        >
          {row.ab_emp_first_name + " " + row.ab_emp_last_name + " (" + row.ab_emp_desig_name + ")"}
      </LinkWithTooltip>
    );
  }
};

class CloseSubTaskTable extends Component {

    state = {
        showCreateSubTask   : false,
        showAssign          : false,
        showReAssign        : false,
        showRespondBack     : false
    };

    checkHandler = (event) => {
        event.preventDefault();
    };

    redirectUrl = (event, id) => {
        event.preventDefault();
        window.open( "http://reddy.indusnet.cloud/customer-dashboard/"+id, '_blank');
    };

    tdClassName = (fieldValue, row) =>{
        var dynamicClass = 'width-150 ';
        if(row.vip_customer === 1){
            dynamicClass += 'bookmarked-column ';
        }
        return dynamicClass;
    }

    render(){

        const selectRowProp = {
            bgColor       : '#fff8f6'
        };

        return (
            <>
                <BootstrapTable 
                    data={this.props.tableData} 
                    selectRow={ selectRowProp } 
                    tableHeaderClass={"col-hidden"} 
                    expandColumnOptions={ 
                        { 
                            expandColumnVisible: true,
                            expandColumnComponent: this.expandColumnComponent,
                            columnWidth: 25
                        } 
                    }
                    trClassName="tr-expandable" 
                >
                    
                    <TableHeaderColumn isKey dataField='task_ref' columnClassName={ this.tdClassName } editable={ false }  dataFormat={ clickToShowTasks(this) } width="10%">Tasks</TableHeaderColumn>

                    <TableHeaderColumn dataField='request_type' editable={ false }  dataFormat={ setDescription(this) } >Description</TableHeaderColumn>

                    <TableHeaderColumn dataField='product_name' editable={ false }>Product Name</TableHeaderColumn>

                    <TableHeaderColumn dataField='dept_name' editable={ false } expandable={ false } dataFormat={ setAssignedBy(this) } >Assigned BY</TableHeaderColumn>

                    <TableHeaderColumn dataField='dept_name' editable={ false } expandable={ false } dataFormat={ setAssignedTo(this) } >Assigned To </TableHeaderColumn>

                    <TableHeaderColumn dataField='company_name'  dataFormat={ setCompanyName(this) } >Customer</TableHeaderColumn>      

                    <TableHeaderColumn dataField='cust_name' editable={ false }  dataFormat={ setEmpCustName(this) } >User Name</TableHeaderColumn>

                    <TableHeaderColumn dataField='display_date_added' editable={ false } dataFormat={setDate(this)} >Created</TableHeaderColumn>

                    <TableHeaderColumn dataField='display_due_date' editable={ false }  dataFormat={ setDate(this) } >Due Date</TableHeaderColumn>

                    <TableHeaderColumn dataField='display_date_added' editable={ false } dataFormat={setDate(this)} >Assigned Date</TableHeaderColumn>
                    
                    <TableHeaderColumn dataField='display_responded_date' editable={ false } dataFormat={setDate(this)} >Closed Date</TableHeaderColumn>
                    
                    
                    

                    <TableHeaderColumn dataField='status' dataFormat={ getStatusColumn(this) }  editable={ false } >Status</TableHeaderColumn>
                    <TableHeaderColumn dataField=""> Action </TableHeaderColumn>

                </BootstrapTable>
            </>
        );
    }

}

export default CloseSubTaskTable;