import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  Tooltip,
  OverlayTrigger
} from "react-bootstrap";
import { htmlDecode } from '../../../shared/helper';

//import dateFormat from "dateformat"; 


import AllCloseSubTaskTable from "./AllCloseSubTaskTable";
import StatusColumn from "./StatusColumn";

import { Link } from "react-router-dom";
import Pagination from "react-js-pagination";
import API from "../../../shared/admin-axios";
import { showErrorMessage } from "../../../shared/handle_error";

// const priority_arr = [
//   { priority_id: 1, priority_value: "Low" },
//   { priority_id: 2, priority_value: "Medium" },
//   { priority_id: 3, priority_value: "High" }
// ];

const getStatusColumn = refObj => (cell, row) => {
  return <StatusColumn rowData={row} />;
};

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const setDescription = refOBj => (cell, row) => {

  if (row.parent_id > 0) {
    return (
      <LinkWithTooltip
        tooltip={`${row.title}`}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {row.title}
      </LinkWithTooltip>
    );
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${row.req_name}`}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {row.req_name}
      </LinkWithTooltip>
    );
  }
};

const setProduct = refOBj => (cell, row) => {
  if (row.parent_id > 0) {
    return (
      <LinkWithTooltip
        tooltip={`${row.title}`}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {row.title}
      </LinkWithTooltip>
    );
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${row.product_name}`}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {row.product_name}
      </LinkWithTooltip>
    );
  }
}

const setAssignedBy = refOBj => (cell, row) => {
  if (row.assigned_by == '-1') {
    return (
      <LinkWithTooltip
        tooltip={`System`}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        System
      </LinkWithTooltip>
    );
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${row.ab_emp_first_name + " " + row.ab_emp_last_name + " (" + row.ab_emp_desig_name + ")"}`}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {row.ab_emp_first_name + " " + row.ab_emp_last_name + " (" + row.ab_emp_desig_name + ")"}
      </LinkWithTooltip>
    );
  }
};

const setDate = refOBj => (cell, row) => {
  return (
    <LinkWithTooltip
      tooltip={cell}
      href="#"
      id="tooltip-1"
      clicked={e => refOBj.checkHandler(e)}
    >
      {cell}
    </LinkWithTooltip>
  );
};

// const setCreateDate = refObj => cell => {
//   //var mydate = new Date(cell);
//   return cell;
// };

// const setDaysPending = refObj => (cell, row) => {
//   var selDueDate = cell;
//   var dueDate = new Date(selDueDate);
//   var today = new Date();
//   var timeDiff = dueDate.getTime() - today.getTime();

//   //console.log(timeDiff);
//   if (timeDiff > 0) {
//     var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
//     return diffDays;
//   } else {
//     return 0;
//   }
// };

const clickToShowTasks = refObj => (cell, row) => {
  return `${cell}`;
};

const setCustomerName = refOBj => (cell, row) => {
  //return `${row.first_name} ${row.last_name}`;

  return (
    <LinkWithTooltip
      tooltip={`${row.first_name + " " + row.last_name}`}
      href="#"
      id="tooltip-1"
      clicked={e => refOBj.checkHandler(e)}
    >
      {row.first_name + " " + row.last_name}
    </LinkWithTooltip>
  );
};

// const setPriorityName = refObj => (cell, row) => {
//   var ret = "Set Priority";
//   for (let index = 0; index < priority_arr.length; index++) {
//     const element = priority_arr[index];
//     if (element["priority_id"] === cell) {
//       ret = element["priority_value"];
//     }
//   }

//   return ret;
// };

const setAssignedTo = refOBj => (cell, row) => {
  return (
    <LinkWithTooltip
      tooltip={`${row.emp_first_name + " " + row.emp_last_name + " (" + row.desig_name + ")"}`}
      href="#"
      id="tooltip-1"
      clicked={e => refOBj.checkHandler(e)}
    >
      {row.emp_first_name + " " + row.emp_last_name + " (" + row.desig_name + ")"}
    </LinkWithTooltip>
  );
};

const setCompanyName = refOBj => (cell, row) => {
  //return htmlDecode(cell);

  return (
    <LinkWithTooltip
      tooltip={htmlDecode(cell)}
      href="#"
      id="tooltip-1"
      clicked={e => refOBj.checkHandler(e)}
    >
      {htmlDecode(cell)}
    </LinkWithTooltip>
  );
};

const actionFormatter = refObj => cell => {
  return (
    <div className="actionStyle">
      <LinkWithTooltip
        tooltip="Click to view task details"
        href={`/admin/task_details/${cell}`}
        id="tooltip-1"
      >
        <i className="fas fa-eye" />
      </LinkWithTooltip>
    </div>
  );
};

class AllTasksTable extends Component {
  state = {
    showCreateSubTask: false,
    showAssign: false,
    showRespondBack: false,
    showTaskDetails: false,
    showRespondCustomer: false,
    showAuthorizeBack: false,
    changeData: true,
    task_id: 0,
    closedTableData: [],

    activePage: 1,
    totalCount: 0,
    itemPerPage: 20,
  };

  checkHandler = (event) => {
    event.preventDefault();
  };

  redirectUrl = (event, id) => {
    event.preventDefault();
    window.open("http://reddy.indusnet.cloud/customer-dashboard/" + id, '_blank');
  };

  taskDetails = row => {
    this.setState({ showTaskDetails: true, currRow: row });
  };

  componentDidMount() {
    this.setState({
      closedTableData: this.props.closedTableData,
      countClosedTasks: this.props.countClosedTasks
    });
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getOpenTasks(pageNumber > 0 ? pageNumber : 1);
  };

  getOpenTasks(page = 1) {
    API.get(`/api/tasks/closed_tasks?page=${page}&task_ref=${encodeURIComponent(this.props.search_task_ref)}&request_type=${encodeURIComponent(
      this.props.search_request_type
    )}&cust_name=${encodeURIComponent(this.props.search_cust_name)}&user_name=${encodeURIComponent(
      this.props.search_user_name)}&status=${encodeURIComponent(this.props.search_status)}`)
      .then(res => {
        this.setState({
          closedTableData: res.data.data,
          countClosedTasks: res.data.count_closed_tasks
        });
      })
      .catch(err => {
        showErrorMessage(err, this.props);
      });
  }

  getSubTasks = row => {
    return (
      <AllCloseSubTaskTable
        tableData={row.sub_tasks}
        dashType={this.props.dashType}
        reloadTaskSubTask={() => this.props.allTaskDetails()}
      />
    );
  };

  checkSubTasks = row => {
    if (typeof row.sub_tasks !== "undefined" && row.sub_tasks.length > 0) {
      return true;
    } else {
      return false;
    }
  };

  tdClassName = (fieldValue, row) => {
    var dynamicClass = "width-150 ";
    if (row.vip_customer === 1) {
      dynamicClass += "bookmarked-column ";
    }
    return dynamicClass;
  };

  trClassName = (row, rowIndex) => {
    var ret = " ";
    var selDueDate = row.due_date;
    var dueDate = new Date(selDueDate);
    var today = new Date();
    var timeDiff = dueDate.getTime() - today.getTime();

    if (timeDiff > 0) {
    } else {
      if (row.vip_customer === 1) {
        ret += "tr-red";
      }
    }
    return ret;
  };

  expandColumnComponent({ isExpandableRow, isExpanded }) {
    let content = "";
    if (isExpandableRow) {
      content = isExpanded ? "-" : "+";
    } else {
      content = " ";
    }
    return <div> {content} </div>;
  }

  downloadXLSX = (e) => {
    e.preventDefault();

    API.get(`/api/tasks/closed_tasks_download?page=1`, { responseType: 'blob' })
      .then(res => {
        let url = window.URL.createObjectURL(res.data);
        let a = document.createElement('a');
        a.href = url;
        a.download = 'closed_tasks.xlsx';
        a.click();

      }).catch(err => {
        showErrorMessage(err, this.props);
      });
  }

  checkHandler = (event) => {
    event.preventDefault();
  };

  render() {

    return (
      <>
        {/* <div>
          {this.state.countClosedTasks > 0 ? 
              <span onClick={(e) => this.downloadXLSX(e)} >
                  <LinkWithTooltip
                      tooltip={`Click here to download excel`}
                      href="#"
                      id="tooltip-my"
                      clicked={e => this.checkHandler(e)}
                  >
                      <i className="fas fa-download"></i>
                  </LinkWithTooltip>
              </span>
            : null }
          </div> */}
        <div>
          <BootstrapTable
            data={this.state.closedTableData}
            expandableRow={this.checkSubTasks}
            expandComponent={this.getSubTasks}
            expandColumnOptions={{
              expandColumnVisible: true,
              expandColumnComponent: this.expandColumnComponent,
              columnWidth: 25
            }}
            trClassName={this.trClassName}
          >
            <TableHeaderColumn
              dataField="task_ref"
              isKey
              columnClassName={this.tdClassName}
              editable={false}
              expandable={false}
              dataFormat={clickToShowTasks(this)}
            >
              Tasks
              </TableHeaderColumn>

            <TableHeaderColumn
              dataField="request_type"
              editable={false}
              expandable={false}
              dataFormat={setDescription(this)}
            >
              Description
              </TableHeaderColumn>

            <TableHeaderColumn
              dataField='product_name'
              editable={false}
              expandable={false}
              dataFormat={setProduct(this)}
            >
              Product Name
              </TableHeaderColumn>

            <TableHeaderColumn
              dataField='dept_name'
              editable={false}
              expandable={false}
              dataFormat={setAssignedBy(this)}
            >
              Assigned By
              </TableHeaderColumn>

            <TableHeaderColumn
              dataField="dept_name"
              editable={false}
              expandable={false}
              dataFormat={setAssignedTo(this)}
            >
              Assigned To
              </TableHeaderColumn>

            <TableHeaderColumn
              dataField='company_name'
              expandable={false}
              dataFormat={setCompanyName(this)}
            >
              Customer
              </TableHeaderColumn>

            <TableHeaderColumn
              dataField="cust_name"
              editable={false}
              expandable={false}
              dataFormat={setCustomerName(this)}
            >
              User
              </TableHeaderColumn>

            <TableHeaderColumn
              dataField="display_date_added"
              editable={false}
              expandable={false}
              dataFormat={setDate(this)}
            >
              Created
              </TableHeaderColumn>

            {/* <TableHeaderColumn
                dataField="due_date"
                editable={false}
                expandable={false}
                dataFormat={setDaysPending(this)}
              >
                Days Pending
              </TableHeaderColumn>

              <TableHeaderColumn
                dataField="dept_name"
                editable={false}
                expandable={false}
              >
                Department
              </TableHeaderColumn>              

              <TableHeaderColumn
                dataField="priority"
                expandable={false}
                dataFormat={setPriorityName(this)}
              >
                Priority
              </TableHeaderColumn> */}

            <TableHeaderColumn
              dataField='display_due_date'
              editable={false}
              expandable={false}
              dataFormat={setDate(this)}
            >
              Due Date
              </TableHeaderColumn>

            <TableHeaderColumn
              dataField='display_date_added'
              editable={false}
              expandable={false}
              dataFormat={setDate(this)}
            >
              Assigned Date
              </TableHeaderColumn>

            <TableHeaderColumn
              dataField='display_responded_date'
              editable={false}
              expandable={false}
              dataFormat={setDate(this)}
            >
              Closed Date
              </TableHeaderColumn>

            <TableHeaderColumn
              dataField="status"
              dataFormat={getStatusColumn(this)}
              expandable={false}
              editable={false}
            >
              Status
              </TableHeaderColumn>
            <TableHeaderColumn
              dataField="task_id"
              dataFormat={actionFormatter(this)}
            >
              Task Details
              </TableHeaderColumn>
          </BootstrapTable>
          {this.state.countClosedTasks > 20 ? (
            <Row>
              <Col md={12}>
                <div className="paginationOuter text-right">
                  <Pagination
                    activePage={this.state.activePage}
                    itemsCountPerPage={this.state.itemPerPage}
                    totalItemsCount={this.state.countClosedTasks}
                    itemClass='nav-item'
                    linkClass='nav-link'
                    activeClass='active'
                    onChange={this.handlePageChange}
                  />
                </div>
              </Col>
            </Row>
          ) : null}
        </div>

      </>
    );
  }
}

export default AllTasksTable;
