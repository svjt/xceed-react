import React, { Component } from "react";
import Pagination from "react-js-pagination";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal,
  Table,
} from "react-bootstrap";
//import { Label } from 'reactstrap';
import { Link } from "react-router-dom";
import { Formik, Field, Form } from "formik";
import swal from "sweetalert";
import * as Yup from "yup";

import Layout from "../layout/Layout";
import whitelogo from "../../../assets/images/drreddylogo_white.png";
import API from "../../../shared/admin-axios";
import { showErrorMessage } from "../../../shared/handle_error";
import { htmlDecode } from "../../../shared/helper";
import { getSuperAdmin, getAdminGroup, inArray } from "../../../shared/helper";
import StatusColumn from "./StatusColumn";
import { Chart } from "react-google-charts";
import Rating from "react-rating";

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
const actionFormatter = (refObj) => (cell, row) => {
  //console.log("===========", row);
  if (row.rating > 0) {
    return (
      <div className="actionStyle">
        <LinkWithTooltip
          tooltip="Click to View"
          href="#"
          clicked={(e) =>
            refObj.modalShowHandler(
              e,
              cell,
              row.rating_options,
              row.rating_comment,
              row.rating
            )
          }
          id="tooltip-1"
        >
          <i className="fas fa-eye" />
        </LinkWithTooltip>
      </div>
    );
  }
};

const clickToShowTasks = () => (cell) => {
  return `${cell}`;
};

const setRating = (refOBj) => (cell, row) => {
  if (row.rating > 0 && row.rating_skipped == 0) {
    return `${row.rating}`;
  } else if (row.rating == 0 && row.rating_skipped == 0) {
    return "Not rated";
  } else if (row.rating_skipped == 1 && row.rating == 0) {
    return "Skipped";
  }
};

const setDescription = (refOBj) => (cell, row) => {
  return `${row.req_name}`;
};

const setProduct = (refOBj) => (cell, row) => {
  if (row.product_name != null) {
    return `${row.product_name}`;
  } else {
    return "-";
  }
};
const setCompany = (refOBj) => (cell, row) => {
  if (row.company_name != null) {
    return `${row.company_name}`;
  } else {
    return "-";
  }
};
const setCustomer = (refOBj) => (cell, row) => {
  return `${row.first_name + " " + row.last_name}`;
};

const setSubmittedBy = (refOBj) => (cell, row) => {
  if (row.rated_by > 0) {
    if (row.rated_by_type == 2) {
      return `${row.first_name_rated_by_agent} ${row.last_name_rated_by_agent} (Agent)`;
    } else {
      return `${row.first_name_rated_by} ${row.last_name_rated_by}`;
    }
  } else {
    return `-`;
  }
};

class TaskRatings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      get_access_data: false,
      task_ratings: [],
      request_type: [],
      companyList: [],
      productList: [],
      productCustomerList: [],
      activePage: 1,
      totalCount: 0,
      itemPerPage: 20,
      remove_search: false,
      search_task_ref: "",
      search_rating_type: "",
      search_rating_total: "",
      search_compid: "",
      search_cid: "",
      search_prodid: "",
      search_rating_request_type: "",
      showModal: false,
      showCustomerBlock: false,
    };
  }

  modalShowHandler = (
    event,
    id,
    rating_options,
    rating_comment,
    rating_stars
  ) => {
    event.preventDefault();
    //console.log("rating_options", rating_options.split());
    if (rating_options && rating_options.length > 0) {
      var rating_options = rating_options.split(",");
      API.get(`api/tasks/task_ratings_options`)
        .then((res) => {
          var taskOptionArr = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            if (inArray(element.o_id, rating_options)) {
              taskOptionArr.push([element.option_name_en]);
            }
            // for (let j = 0; j < rating_options.length; j++) {
            //   const element1 = rating_options[j];
            //   if (element1 == element.o_id) {
            //     taskOptionArr.push([element.option_name_en]);
            //   }
            // }
          }
          var r_comment = rating_comment;

          this.setState({
            taskOptionArr: taskOptionArr,
            comment: r_comment,
            rating_stars: rating_stars,
            showModal: true,
          });
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      var taskOptionArr = [];
      var r_comment = rating_comment;
      this.setState({
        taskOptionArr: taskOptionArr,
        comment: r_comment,
        rating_stars: rating_stars,
        showModal: true,
      });
    }
  };

  modalCloseHandler = () => {
    this.setState({ showModal: false });
  };

  componentDidMount() {
    const superAdmin = getSuperAdmin(localStorage.admin_token);

    if (superAdmin === 1) {
      this.setState({
        access: {
          view: true,
          add: true,
          edit: true,
          delete: true,
        },
        get_access_data: true,
      });
      this.getTaskRatingsList();
      this.getServiceRequestList();
      this.getCompanyList();
      this.getProductList();
    } else {
      const adminGroup = getAdminGroup(localStorage.admin_token);
      API.get(`/api/adm_group/single_access/${adminGroup}/${"TASK_MANAGEMENT"}`)
        .then((res) => {
          this.setState({
            access: res.data.data,
            get_access_data: true,
          });

          if (res.data.data.view === true) {
            this.getTaskRatingsList();
            this.getServiceRequestList();
            this.getCompanyList();
            this.getProductList();
          } else {
            this.props.history.push("/admin/dashboard");
          }
        })
        .catch((err) => {
          showErrorMessage(err, this.props);
        });
    }
  }

  getTaskRatingsList(page = 1) {
    let task_ref = this.state.search_task_ref;
    let rating_type = this.state.search_rating_type;
    let rating_total = this.state.search_rating_total;
    let compid = this.state.search_compid;
    let cid = this.state.search_cid;
    let prodid = this.state.search_prodid;
    let rating_request_type = this.state.search_rating_request_type;

    //==================MONOSOM=================//
    API.get(
      `/api/tasks/get_task_ratings_total?task_ref=${encodeURIComponent(
        task_ref
      )}&rating_type=${encodeURIComponent(
        rating_type
      )}&rating_total=${encodeURIComponent(
        rating_total
      )}&compid=${encodeURIComponent(compid)}&cid=${encodeURIComponent(
        cid
      )}&prodid=${encodeURIComponent(
        prodid
      )}&rating_request_type=${encodeURIComponent(rating_request_type)}`
    )
      .then((res) => {
        this.setState({
          tasks_total: res.data.tasks_total,
          tasks_rated: res.data.tasks_rated,
          task_rate_pending: res.data.task_rate_pending,
          task_rate_skipped: res.data.task_rate_skipped,
        });
      })
      .catch((err) => {
        console.log(err);
      });

    //==================MONOSOM=================//
    API.get(
      `/api/tasks/get_task_ratings_review?task_ref=${encodeURIComponent(
        task_ref
      )}&compid=${encodeURIComponent(compid)}&cid=${encodeURIComponent(
        cid
      )}&prodid=${encodeURIComponent(
        prodid
      )}&rating_request_type=${encodeURIComponent(rating_request_type)}`
    )
      .then((res) => {
        this.setState({
          total_riview:
            res.data.tasks_positive +
            res.data.task_neutral +
            res.data.task_negative,
          tasks_positive: res.data.tasks_positive,
          task_neutral: res.data.task_neutral,
          task_negative: res.data.task_negative,
        });
      })
      .catch((err) => {
        console.log(err);
      });

    //======================First graph================Type====================
    API.get(
      `/api/tasks/get_task_ratings_graph?rating_type=${encodeURIComponent(
        rating_type
      )}&rating_request_type=${encodeURIComponent(
        rating_request_type
      )}&task_ref=${encodeURIComponent(task_ref)}&compid=${encodeURIComponent(
        compid
      )}&cid=${encodeURIComponent(cid)}&prodid=${encodeURIComponent(prodid)}`
    )
      .then((res) => {
        this.setState({
          dataArrGraph: res.data.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });

    //======================Second Graph====================================
    API.get(
      `/api/tasks/get_avg_task_ratings_graph?rating_request_type=${encodeURIComponent(
        rating_request_type
      )}&task_ref=${encodeURIComponent(task_ref)}&compid=${encodeURIComponent(
        compid
      )}&cid=${encodeURIComponent(cid)}&prodid=${encodeURIComponent(prodid)}`
    )
      .then((res) => {
        this.setState({
          dataAvgArrGraph: res.data.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });

    //======================Third Graph==============Score======================
    API.get(
      `/api/agents/get_max_avg_task_ratings_graph?rating_total=${encodeURIComponent(
        rating_total
      )}&rating_request_type=${encodeURIComponent(
        rating_request_type
      )}&task_ref=${encodeURIComponent(task_ref)}&compid=${encodeURIComponent(
        compid
      )}&cid=${encodeURIComponent(cid)}&prodid=${encodeURIComponent(prodid)}`
    )
      .then((res) => {
        this.setState({
          dataMaxAvgArrGraph: res.data.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });

    /* API.get(`/api/tasks/get_task_ratings_piechart`)
      .then((res) => {
        console.log(res.data);
        var dataPie = [
          ["Task", "Rating Status"],
          ["Rated", res.data.tasks_rated],
          ["Yet To Rete", res.data.task_rate_pending],
          ["Skipped", res.data.task_rate_skipped],
        ];
        this.setState({
          dataArrPie: dataPie,
        });
      })
      .catch((err) => {
        console.log(err);
      }); */

    /*  API.get(`/api/tasks/get_task_rating_option_graph`)
        .then((res) => {
          console.log(res.data.data);
          var dataPie = [
            ['Task', 'Rating Status'],
            ['Rated', res.data.tasks_rated],
            ['Yet To Rete', res.data.task_rate_pending],
            ['Skipped', res.data.task_rate_skipped]
          ];
          this.setState({
            dataArrPie: dataPie
          });
        })
        .catch((err) => {
          console.log(err);
        }); */

    API.get(
      `/api/tasks/task_ratings?page=${page}&task_ref=${encodeURIComponent(
        task_ref
      )}&rating_type=${encodeURIComponent(
        rating_type
      )}&rating_total=${encodeURIComponent(
        rating_total
      )}&compid=${encodeURIComponent(compid)}&cid=${encodeURIComponent(
        cid
      )}&prodid=${encodeURIComponent(
        prodid
      )}&rating_request_type=${encodeURIComponent(rating_request_type)}`
    )
      .then((res) => {
        this.setState({
          task_ratings: res.data.data,
          count: res.data.count_task_ratings,
          search_task_ref: task_ref,
          search_rating_type: rating_type,
          search_rating_total: rating_total,
          search_compid: compid,
          search_cid: cid,
          search_prodid: prodid,
          search_rating_request_type: rating_request_type,
        });
      })
      .catch((err) => {
        this.setState({
          isLoading: false,
        });
        showErrorMessage(err, this.props);
      });
    this.setState({
      isLoading: false,
    });
  }

  getServiceRequestList() {
    API.get(`/api/feed/request_type_adm`).then((res) => {
      var all_request_type = [];
      for (let index = 0; index < res.data.data.length; index++) {
        const element = res.data.data[index];
        all_request_type.push(element);
      }
      this.setState({
        request_type: all_request_type,
        isLoading: false,
      });
    });
  }

  getCompanyList() {
    API.get("/api/customers/company")
      .then((res) => {
        this.setState({
          companyList: res.data.data,
        });
      })
      .catch((err) => {
        showErrorMessage(err, this.props);
      });
  }

  getProductList() {
    API.get("/api/products/admin_all_products")
      .then((res) => {
        this.setState({
          productList: res.data.data,
        });
      })
      .catch((err) => {
        showErrorMessage(err, this.props);
      });
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getTaskRatingsList(pageNumber > 0 ? pageNumber : 1);
  };

  taskRatingsListSearch = (e) => {
    e.preventDefault();

    var task_ref = document.getElementById("task_ref").value;
    var rating_type = document.getElementById("rating_type").value;
    var rating_total = document.getElementById("rating_total").value;
    var compid = document.getElementById("compid").value;
    if (compid > 0) {
      var cid = document.getElementById("cid").value;
    } else {
      var cid = "";
    }

    var prodid = document.getElementById("prodid").value;
    var rating_request_type = document.getElementById("rating_request_type")
      .value;

    if (
      task_ref === "" &&
      rating_type === "" &&
      rating_total === "" &&
      compid === "" &&
      cid === "" &&
      prodid === "" &&
      rating_request_type === ""
    ) {
      return false;
    }

    //==================MONOSOM=================//
    API.get(
      `/api/tasks/get_task_ratings_total?task_ref=${encodeURIComponent(
        task_ref
      )}&rating_type=${encodeURIComponent(
        rating_type
      )}&rating_total=${encodeURIComponent(
        rating_total
      )}&compid=${encodeURIComponent(compid)}&cid=${encodeURIComponent(
        cid
      )}&prodid=${encodeURIComponent(
        prodid
      )}&rating_request_type=${encodeURIComponent(rating_request_type)}`
    )
      .then((res) => {
        this.setState({
          tasks_total: res.data.tasks_total,
          tasks_rated: res.data.tasks_rated,
          task_rate_pending: res.data.task_rate_pending,
          task_rate_skipped: res.data.task_rate_skipped,
        });
      })
      .catch((err) => {
        console.log(err);
      });

    //==================MONOSOM=================//
    API.get(
      `/api/tasks/get_task_ratings_review?task_ref=${encodeURIComponent(
        task_ref
      )}&compid=${encodeURIComponent(compid)}&cid=${encodeURIComponent(
        cid
      )}&prodid=${encodeURIComponent(
        prodid
      )}&rating_request_type=${encodeURIComponent(rating_request_type)}`
    )
      .then((res) => {
        this.setState({
          total_riview:
            res.data.tasks_positive +
            res.data.task_neutral +
            res.data.task_negative,
          tasks_positive: res.data.tasks_positive,
          task_neutral: res.data.task_neutral,
          task_negative: res.data.task_negative,
        });
      })
      .catch((err) => {
        console.log(err);
      });

    //======================First graph====================================
    API.get(
      `/api/tasks/get_task_ratings_graph?rating_type=${encodeURIComponent(
        rating_type
      )}&rating_request_type=${encodeURIComponent(
        rating_request_type
      )}&task_ref=${encodeURIComponent(task_ref)}&compid=${encodeURIComponent(
        compid
      )}&cid=${encodeURIComponent(cid)}&prodid=${encodeURIComponent(prodid)}`
    )
      .then((res) => {
        this.setState({
          dataArrGraph: res.data.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
    //======================Second Graph====================================
    API.get(
      `/api/tasks/get_avg_task_ratings_graph?rating_request_type=${encodeURIComponent(
        rating_request_type
      )}&task_ref=${encodeURIComponent(task_ref)}&compid=${encodeURIComponent(
        compid
      )}&cid=${encodeURIComponent(cid)}&prodid=${encodeURIComponent(prodid)}`
    )
      .then((res) => {
        this.setState({
          dataAvgArrGraph: res.data.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });

    //======================Third Graph====================================
    API.get(
      `/api/agents/get_max_avg_task_ratings_graph?rating_total=${encodeURIComponent(
        rating_total
      )}&rating_request_type=${encodeURIComponent(
        rating_request_type
      )}&task_ref=${encodeURIComponent(task_ref)}&compid=${encodeURIComponent(
        compid
      )}&cid=${encodeURIComponent(cid)}&prodid=${encodeURIComponent(prodid)}`
    )
      .then((res) => {
        this.setState({
          dataMaxAvgArrGraph: res.data.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });

    API.get(
      `/api/tasks/task_ratings?page=1&task_ref=${encodeURIComponent(
        task_ref
      )}&rating_type=${encodeURIComponent(
        rating_type
      )}&rating_total=${encodeURIComponent(
        rating_total
      )}&compid=${encodeURIComponent(compid)}&cid=${encodeURIComponent(
        cid
      )}&prodid=${encodeURIComponent(
        prodid
      )}&rating_request_type=${encodeURIComponent(rating_request_type)}`
    )
      .then((res) => {
        this.setState({
          task_ratings: res.data.data,
          count: res.data.count_task_ratings,
          isLoading: false,
          search_task_ref: task_ref,
          search_rating_type: rating_type,
          search_rating_total: rating_total,
          search_compid: compid,
          search_cid: cid,
          search_prodid: prodid,
          search_rating_request_type: rating_request_type,
          remove_search: true,
          activePage: 1,
        });
      })
      .catch((err) => {
        this.setState({
          isLoading: false,
        });
        showErrorMessage(err, this.props);
      });
  };

  clearSearch = () => {
    document.getElementById("task_ref").value = "";
    document.getElementById("rating_type").value = "";
    document.getElementById("rating_total").value = "";
    document.getElementById("compid").value = "";
    if (this.state.showCustomerBlock === true) {
      document.getElementById("cid").value = "";
    }
    document.getElementById("prodid").value = "";
    document.getElementById("rating_request_type").value = "";
    this.setState(
      {
        search_task_ref: "",
        search_rating_type: "",
        search_rating_total: "",
        search_compid: "",
        search_cid: "",
        search_prodid: "",
        search_rating_request_type: "",
        remove_search: false,
        showCustomerBlock: false,
      },
      () => {
        this.getTaskRatingsList();
        this.setState({ activePage: 1 });
      }
    );
  };

  downloadXLSX = (e) => {
    e.preventDefault();

    var rating_type = document.getElementById("rating_type").value;
    var rating_total = document.getElementById("rating_total").value;
    var rating_request_type = document.getElementById("rating_request_type")
      .value;

    API.get(
      `/api/agents/download_rating?page=1&rating_type=${encodeURIComponent(
        rating_type
      )}&rating_total=${encodeURIComponent(
        rating_total
      )}&rating_request_type=${encodeURIComponent(rating_request_type)}`,
      { responseType: "blob" }
    )
      .then((res) => {
        let url = window.URL.createObjectURL(res.data);
        let a = document.createElement("a");
        a.href = url;
        a.download = "ratings.xlsx";
        a.click();
      })
      .catch((err) => {
        showErrorMessage(err, this.props);
      });
  };

  checkHandler = (event) => {
    event.preventDefault();
  };

  filterSearchCust = (event) => {
    if (event.target.value > 0) {
      this.companyCustomers(event.target.value);
    } else {
      this.setState({
        showCustomerBlock: false,
        productCustomerList: [],
      });
    }
  };

  companyCustomers(id) {
    API.get(`/api/agents/company_customers/${id}`)
      .then((res) => {
        this.setState({
          productCustomerList: res.data.data,
          showCustomerBlock: true,
        });
      })
      .catch((err) => {
        showErrorMessage(err, this.props);
      });
  }

  render() {
    //alert(isNaN(10));
    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy">
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      const { valueCompid } = this.state;
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="topSearchSection">
                <div className="searchfield">
                  <form className="form">
                    <div>
                      <input
                        className="form-control"
                        name="task_ref"
                        id="task_ref"
                        placeholder="Task Ref"
                      />
                    </div>
                    <div>
                      <select
                        name="type"
                        id="rating_type"
                        className="form-control"
                      >
                        <option value="">Type</option>
                        <option value="1">Submitted</option>
                        <option value="3">Pending</option>
                        <option value="2">Denied</option>
                      </select>
                    </div>
                    <div>
                      <select
                        name="total"
                        id="rating_total"
                        className="form-control"
                      >
                        <option value="">Score</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                        <option value="4">Four</option>
                        <option value="5">Five</option>
                      </select>
                    </div>
                    <div>
                      <select
                        name="custcompany"
                        id="compid"
                        className="form-control w-150"
                        onChange={this.filterSearchCust}
                        value={valueCompid}
                      >
                        <option value="">Select Customer</option>
                        {this.state.companyList.map((company, i) => (
                          <option key={i} value={company.company_id}>
                            {htmlDecode(company.company_name)}
                          </option>
                        ))}
                      </select>
                    </div>
                    <div>
                      {this.state.showCustomerBlock && (
                        <select
                          name="product"
                          id="cid"
                          className="form-control w-150"
                        >
                          <option value="">Select User</option>
                          {this.state.productCustomerList.map((customer, i) => (
                            <option key={i} value={customer.customer_id}>
                              {htmlDecode(customer.first_name)}{" "}
                              {htmlDecode(customer.last_name)}
                            </option>
                          ))}
                        </select>
                      )}
                    </div>

                    <div>
                      <select
                        name="product"
                        id="prodid"
                        className="form-control w-150"
                      >
                        <option value="">Select Products</option>
                        {this.state.productList.map((product, i) => (
                          <option key={i} value={product.product_id}>
                            {htmlDecode(product.product_name)}
                          </option>
                        ))}
                      </select>
                    </div>

                    <div>
                      <select
                        name="request_type"
                        id="rating_request_type"
                        className="form-control"
                      >
                        <option value="">Select Request</option>
                        {this.state.request_type.map((request, i) => (
                          <option key={i} value={request.type_id}>
                            {request.req_name}
                          </option>
                        ))}
                      </select>
                    </div>
                    <div className="reset">
                      <input
                        type="submit"
                        value="Search"
                        className="btn btn-warning btn-sm"
                        onClick={(e) => this.taskRatingsListSearch(e)}
                      />
                      <a
                        onClick={() => this.clearSearch()}
                        className="btn btn-danger btn-sm"
                      >
                        {" "}
                        Reset{" "}
                      </a>
                      {/* {this.state.remove_search ? (
                        <a
                          onClick={() => this.clearSearch()}
                          className="btn btn-danger btn-sm"
                        >
                          {" "}
                          Reset{" "}
                        </a>
                      ) : null} */}
                    </div>
                    <div className="clearfix"></div>
                  </form>
                </div>
              </div>

              <div className="pd-15">
                <div className="chartbg">
                  <h2 className="chrthd">Customer Feedback</h2>

                  <div className="ratngbg">
                    <div className="admnbox">
                      <div className="fdbgbox">
                        <img
                          src={require("../../../assets/images/feed-1.png")}
                        />
                      </div>
                      <div className="info">
                        <p>Total Closed Task</p>
                        <p className="nmbr">{this.state.tasks_total}</p>
                      </div>
                    </div>

                    <div className="admnbox">
                      <div className="fdbgbox">
                        <img
                          src={require("../../../assets/images/feed-2.png")}
                        />
                      </div>
                      <div className="info">
                        <p>Submitted Feedback</p>
                        <p className="nmbr">{this.state.tasks_rated}</p>
                        <h6>Response Rate</h6>
                        {this.state.tasks_rated > 0 &&
                          this.state.tasks_total > 0 ? (
                            <p>
                              {(
                                (this.state.tasks_rated /
                                  this.state.tasks_total) *
                                100
                              ).toFixed(2)}
                            %
                            </p>
                          ) : (
                            <p>0%</p>
                          )}
                      </div>
                    </div>

                    <div className="admnbox">
                      <div className="fdbgboxPen">
                        <img src={require("../../../assets/images/sand.png")} />
                      </div>
                      <div className="info">
                        <p>Pending Feedback</p>
                        <p className="nmbr">{this.state.task_rate_pending}</p>
                        <h6>Average Feedback Pending</h6>
                        {this.state.task_rate_pending > 0 &&
                          this.state.tasks_total > 0 ? (
                            <p>
                              {(
                                (this.state.task_rate_pending /
                                  this.state.tasks_total) *
                                100
                              ).toFixed(2)}
                            %
                            </p>
                          ) : (
                            <p>0%</p>
                          )}
                      </div>
                    </div>

                    <div className="admnbox brdrrght">
                      <div className="fdbgboxDen">
                        <img src={require("../../../assets/images/den.png")} />
                      </div>
                      <div className="info">
                        <p>Denied Feedback</p>
                        <p className="nmbr">{this.state.task_rate_skipped}</p>
                        <h6>Average Denial</h6>
                        {this.state.task_rate_skipped > 0 &&
                          this.state.tasks_total > 0 ? (
                            <p>
                              {(
                                (this.state.task_rate_skipped /
                                  this.state.tasks_total) *
                                100
                              ).toFixed(2)}
                            %
                            </p>
                          ) : (
                            <p>0%</p>
                          )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="pd-15">
                <div className="bar-canvus-area">
                  <div className="canvus-side-head">
                    <p>Feedback Overview</p>
                  </div>
                  <div className="canvus-area">
                    <div className="chartbg">
                      <Chart
                        width={"100%"}
                        height={"500px"}
                        chartType="ColumnChart"
                        loader={<div>Loading Chart</div>}
                        data={this.state.dataArrGraph}
                        options={{
                          chartArea: { width: "90%" },
                          colors: ["#01b350", "#fbc201", "#ef0c00"],
                          isStacked: true,
                          bars: "vertical",
                          legend: { position: "bottom", alignment: "center" },
                        }}
                        // For tests
                        rootProps={{ "data-testid": "2" }}
                      />
                    </div>
                  </div>
                </div>
              </div>
              {/* <div className="svgfootr">
                  <ul class="bar-description">
                    <li>
                      <span className="green"></span>Rated
                    </li>
                    <li>
                      <span className="yellow"></span>Yet To Rate
                    </li>
                    <li>
                      <span className="red"></span>Skipped
                    </li>
                  </ul>
                </div> */}

              {/* <Chart
                width={"300px"}
                height={"300px"}
                chartType="PieChart"
                loader={<div>Loading Chart</div>}
                data={this.state.dataArrPie}
                options={{
                  title: "Task Rating Status",
                  // Just add this option
                  pieHole: 0.65,
                }}
                rootProps={{ "data-testid": "3" }}
              />
              <div>Total : {this.state.count}</div> */}

              <div className="row">
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  {/* <h1 className="m-b-25">
                    Task Average
                    <small />
                  </h1> */}
                  <div className="pd-15">
                    <div class="canvus-side-head">
                      <p>Average Request Wise Feedback</p>
                    </div>

                    <div className="chartbg">
                      <Chart
                        width={"100%"}
                        height={"500px"}
                        marginBottom={"25px"}
                        chartType="ColumnChart"
                        loader={<div>Loading Chart</div>}
                        data={this.state.dataAvgArrGraph}
                        options={{
                          chartArea: { width: "90%" },
                          colors: ["#01b350", "#fad264", "#ff5959"],
                          bars: "vertical",
                          legend: { position: "none" },
                          axes: {
                            x: {
                              0: { label: "" },
                            },
                          },
                          vAxis: {
                            title: "Average Feedback",
                          },
                        }}
                        // For tests
                        rootProps={{ "data-testid": "2" }}
                      />
                    </div>
                  </div>

                  <div className="pd-15">
                    <div class="canvus-side-head">
                      <p>Request Wise Feedback</p>
                    </div>
                    <div className="chartbg">
                      <Chart
                        width={"100%"}
                        height={"500px"}
                        marginBottom={"25px"}
                        chartType="ColumnChart"
                        loader={<div>Loading Chart</div>}
                        data={this.state.dataMaxAvgArrGraph}
                        options={{
                          chartArea: { width: "90%" },
                          colors: ["#01b350", "#fad264", "#ff5959"],
                          bars: "vertical",
                          legend: { position: "none" },
                          axes: {
                            x: {
                              0: { label: "" },
                            },
                          },
                          vAxis: {
                            title: "Response Count",
                          },
                        }}
                        // For tests
                        rootProps={{ "data-testid": "2" }}
                      />
                    </div>
                  </div>
                </div>

                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <div className="pd-15">
                    <div className="chartbg">
                      <div className="d-flex justify-content-center align-item-center midhead">
                        <strong> CSAT score </strong> {""} ={" "}
                        {this.state.tasks_positive > 0 &&
                          this.state.total_riview > 0 ? (
                            <div>
                              {(
                                (this.state.tasks_positive /
                                  this.state.total_riview) *
                                100
                              ).toFixed(2)}
                            %
                            </div>
                          ) : (
                            <div>0%</div>
                          )}
                      </div>
                      <Row>
                        <Col sm={12} md={2}></Col>
                        <Col sm={12} md={8}>
                          <Col sm={12} md={4}>
                            <div className="smilebox">
                              <img
                                src={require("../../../assets/images/smile-1.svg")}
                                alt=""
                              />
                              <h3>{this.state.tasks_positive}</h3>
                              {this.state.tasks_positive > 0 &&
                                this.state.total_riview > 0 ? (
                                  <span>
                                    {(
                                      (this.state.tasks_positive /
                                        this.state.total_riview) *
                                      100
                                    ).toFixed(2)}
                                  %
                                  </span>
                                ) : (
                                  <span>0%</span>
                                )}
                              <p>Satisfied Customer</p>
                            </div>
                          </Col>

                          <Col sm={12} md={4}>
                            <div className="smilebox">
                              <img
                                src={require("../../../assets/images/smile-2.svg")}
                                alt=""
                              />
                              <h3>{this.state.task_neutral}</h3>
                              {this.state.task_neutral > 0 &&
                                this.state.total_riview > 0 ? (
                                  <span>
                                    {(
                                      (this.state.task_neutral /
                                        this.state.total_riview) *
                                      100
                                    ).toFixed(2)}
                                  %
                                  </span>
                                ) : (
                                  <span>0%</span>
                                )}
                              <p>Neutral Customer</p>
                            </div>
                          </Col>

                          <Col sm={12} md={4}>
                            <div className="smilebox">
                              <img
                                src={require("../../../assets/images/smile-3.svg")}
                                alt=""
                              />
                              <h3>{this.state.task_negative}</h3>
                              {this.state.task_negative > 0 &&
                                this.state.total_riview > 0 ? (
                                  <span>
                                    {(
                                      (this.state.task_negative /
                                        this.state.total_riview) *
                                      100
                                    ).toFixed(2)}
                                  %
                                  </span>
                                ) : (
                                  <span>0%</span>
                                )}
                              <p>Unsatisfied Customer</p>
                            </div>
                          </Col>
                        </Col>
                        <Col sm={12} md={2}></Col>

                        {/* <Col sm={12} md={6}></Col>

                        <Col sm={12} md={6}>
                          <div className="ratngcount">
                            <p>5 Positive</p>
                            <p>4 Neutral</p>
                            <p>1-3 Negative</p>
                          </div>
                        </Col> */}
                      </Row>

                      <div className="ratingtabl">
                        <div className="bggrey">
                          <span className="greenclr m-r-10">5 Satisfied</span>
                          <span className="orangeclr m-r-10">4 Neutral</span>
                          <span className="redclr">3 Unsatisfied</span>
                        </div>
                      </div>

                      <div className="ratingtabl">
                        <Table striped bordered hover>
                          <thead>
                            <tr>
                              <th>Unsatisfied</th>
                              <th>Neutral</th>
                              <th>Satisfied</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td className="redclr">
                                <strong>
                                  {" "}
                                  <span className="mr-10"> 1 </span>
                                  <span className="mr-10"> 2 </span>
                                  <span className="mr-10"> 3 </span>
                                </strong>
                              </td>

                              <td className="orangeclr">
                                <strong> 4 </strong>
                              </td>

                              <td className="greenclr">
                                <strong> 5</strong>
                              </td>
                            </tr>

                            {/* <tr>

                            <td colSpan="3" className="partly">
                             
                            </td>
                            </tr> */}
                          </tbody>
                        </Table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <div className="pd-15">
              <section className="content">
                <div className="box">
                  <div className="box-body">
                    <div className="nav-tabs-custom">
                      <ul className="nav nav-tabs">
                        <li className="tabButtonSec pull-right">
                          {" "}
                          {this.state.count > 0 ? (
                            <span onClick={(e) => this.downloadXLSX(e)}>
                              <LinkWithTooltip
                                tooltip={`Click here to download excel`}
                                href="#"
                                id="tooltip-my"
                                clicked={(e) => this.checkHandler(e)}
                              >
                                <i className="fas fa-download"></i>
                              </LinkWithTooltip>
                            </span>
                          ) : null}
                        </li>
                      </ul>
                    </div>
                    <BootstrapTable data={this.state.task_ratings}>
                      <TableHeaderColumn
                        isKey
                        dataField="task_ref"
                        dataFormat={clickToShowTasks(this)}
                      >
                        Name
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        dataField="rated_by"
                        dataFormat={setSubmittedBy(this)}
                      >
                        Submitted By
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        dataField="req_name"
                        dataFormat={setDescription(this)}
                      >
                        Description
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        dataField="product_name"
                        dataFormat={setProduct(this)}
                      >
                        Product Name
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        dataField="company_name"
                        dataFormat={setCompany(this)}
                      >
                        Company Name
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        dataField="cust_name"
                        dataFormat={setCustomer(this)}
                      >
                        User Name
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        dataField="rating"
                        dataFormat={setRating(this)}
                      >
                        Rating
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        dataField="task_id"
                        dataFormat={actionFormatter(this)}
                      >
                        View
                      </TableHeaderColumn>
                    </BootstrapTable>

                    {this.state.count > this.state.itemPerPage ? (
                      <Row>
                        <Col md={12}>
                          <div className="paginationOuter text-right">
                            <Pagination
                              activePage={this.state.activePage}
                              itemsCountPerPage={this.state.itemPerPage}
                              totalItemsCount={this.state.count}
                              itemClass="nav-item"
                              linkClass="nav-link"
                              activeClass="active"
                              onChange={this.handlePageChange}
                            />
                          </div>
                        </Col>
                      </Row>
                    ) : null}
                    <Modal
                      show={this.state.showModal}
                      onHide={() => this.modalCloseHandler()}
                      backdrop="static"
                      className="feedbckProv"
                    >
                      <Formik>
                        {({ }) => {
                          return (
                            <Form>
                              <Modal.Header>
                                <Modal.Title>Feedback provided</Modal.Title>
                              </Modal.Header>
                              <Modal.Body>
                                <div className="contBox">
                                  {this.state.rating_stars &&
                                    this.state.rating_stars != null ? (
                                      <Row>
                                        <Col xs={12} sm={12} md={12}>
                                          <div className="form-group">
                                            <h2> Rating</h2>
                                            <div className="ratinfo starimg">
                                              <span>
                                                {" "}
                                                <Rating
                                                  initialRating={
                                                    this.state.rating_stars
                                                  }
                                                  readonly
                                                  emptySymbol={
                                                    <img
                                                      src={require("../../../assets/images/uncheck-star.svg")}
                                                      alt=""
                                                      className="icon"
                                                    />
                                                  }
                                                  placeholderSymbol={
                                                    <img
                                                      src={require("../../../assets/images/uncheck-stars.svg")}
                                                      alt=""
                                                      className="icon"
                                                    />
                                                  }
                                                  fullSymbol={
                                                    <img
                                                      src={require("../../../assets/images/uncheck-stars.svg")}
                                                      alt=""
                                                      className="icon"
                                                    />
                                                  }
                                                />
                                              </span>
                                            </div>
                                          </div>
                                        </Col>
                                      </Row>
                                    ) : null}

                                  <div className="opsn">
                                    {this.state.taskOptionArr &&
                                      this.state.taskOptionArr != "" ? (
                                        <div>
                                          <p>Options selected</p>
                                          {this.state.taskOptionArr.map(
                                            (option, i) => {
                                              return (
                                                <Row>
                                                  <Col xs={12} sm={12} md={12}>
                                                    <div className="form-group">
                                                      <div className="checkboxfeedbck">
                                                        {/* <label class="customCheckBoxbtn">
                                          <input type="checkbox"/>
                                            <span class="checkmark ">{option}</span>
                                            </label> */}

                                                        <input
                                                          type="checkbox"
                                                          checked="checked"
                                                        />
                                                        <span> {option}</span>
                                                      </div>
                                                    </div>
                                                  </Col>
                                                </Row>
                                              );
                                            }
                                          )}
                                        </div>
                                      ) : null}
                                    {this.state.comment &&
                                      this.state.comment != null ? (
                                        <Row>
                                          <Col xs={12} sm={12} md={12}>
                                            <div className="form-group">
                                              <label>Comment</label>
                                              <span> {this.state.comment}</span>
                                            </div>
                                          </Col>
                                        </Row>
                                      ) : null}
                                  </div>
                                </div>
                              </Modal.Body>
                              <Modal.Footer>
                                <button
                                  onClick={(e) => this.modalCloseHandler()}
                                  className={`btn btn-danger btn-sm`}
                                  type="button"
                                >
                                  Close
                                </button>
                              </Modal.Footer>
                            </Form>
                          );
                        }}
                      </Formik>
                    </Modal>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </Layout>
      );
    }
  }
}
export default TaskRatings;
