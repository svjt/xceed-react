import React, { Component } from 'react';
import {BootstrapTable,TableHeaderColumn} from 'react-bootstrap-table';
import {
  Row,
  Col,
  Tooltip,
  OverlayTrigger 
} from "react-bootstrap";

//import dateFormat from 'dateformat';
import { htmlDecode } from '../../../shared/helper';
import {showErrorMessage} from "../../../shared/handle_error";
import API from "../../../shared/admin-axios";

import StatusColumnAllocatedTask from './StatusColumnAllocatedTask';
import AllocatedSubTaskTable from './AllocatedSubTaskTable';

import { Link } from 'react-router-dom';
import Pagination from "react-js-pagination";

const priority_arr = [
    {priority_id:1,priority_value:'Low'},
    {priority_id:2,priority_value:'Medium'},
    {priority_id:3,priority_value:'High'}
];

const getStatusColumn = refObj => (cell,row) => {
    return <StatusColumnAllocatedTask rowData={row} />
}

const actionFormatter = refObj => cell => {
  return (
    <div className="actionStyle">
      <LinkWithTooltip
        tooltip="Click to view task details"
        href={`/admin/task_details/${cell}`}
        id="tooltip-1"
      >
        <i className="fas fa-eye" />
      </LinkWithTooltip>
    </div>
  )
}

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const setDescription = refOBj => (cell,row) =>{
    if(row.parent_id > 0){
        return (
            <LinkWithTooltip
                tooltip={`${row.title}`}
                href="#"
                id="tooltip-1"
                clicked={e => refOBj.checkHandler(e)}
              >
              {row.title}
            </LinkWithTooltip>
        );
      }else{
        return (      
          <LinkWithTooltip
            tooltip={`${row.req_name}`}
            href="#"
            id="tooltip-1"
            clicked={e => refOBj.checkHandler(e)}
          >
            {row.req_name}
          </LinkWithTooltip>
          );
      }
}

const setProduct = refOBj => (cell,row) =>{
  if(row.parent_id > 0){
      return (
          <LinkWithTooltip
              tooltip={`${row.title}`}
              href="#"
              id="tooltip-1"
              clicked={e => refOBj.checkHandler(e)}
            >
            {row.title}
          </LinkWithTooltip>
      );
    }else{
      return (      
        <LinkWithTooltip
          tooltip={`${row.product_name}`}
          href="#"
          id="tooltip-1"
          clicked={e => refOBj.checkHandler(e)}
        >
          {row.product_name}
        </LinkWithTooltip>
        );
    }
}

const setDate = refOBj => (cell,row) => {
  return (    
    <LinkWithTooltip
        tooltip={cell}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
      {cell}  
    </LinkWithTooltip>
  );
};

const setAssignedBy = refOBj => (cell, row) => {
  if(row.assigned_by == '-1' ){
    return (    
      <LinkWithTooltip
          tooltip={`System`}
          href="#"
          id="tooltip-1"
          clicked={e => refOBj.checkHandler(e)}
        >
        System  
      </LinkWithTooltip>
    );
  }else{
    return (    
      <LinkWithTooltip
          tooltip={`${row.ab_emp_first_name + " " + row.ab_emp_last_name + " (" + row.ab_emp_desig_name + ")"}`}
          href="#"
          id="tooltip-1"
          clicked={e => refOBj.checkHandler(e)}
        >
          {row.ab_emp_first_name + " " + row.ab_emp_last_name + " (" + row.ab_emp_desig_name + ")"}
      </LinkWithTooltip>
    );
  }
};

const setCompanyName = refOBj => (cell, row) => {
  //return htmlDecode(cell);
  return (    
    <LinkWithTooltip
        tooltip={htmlDecode(cell)}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {htmlDecode(cell)}
    </LinkWithTooltip>
  );
}

//const setCreateDate = refObj => cell =>{
    //var replacedDate = cell.split('T');
    //var mydate = new Date(cell);
    //console.log(mydate);
    //return dateFormat(mydate, "dd/mm/yyyy");
    //return cell
//}

/*const setDaysPending = refObj => cell =>{
    if(cell !== null){
        //var replacedDate = cell.split('T');
        var dueDate = new Date(cell);
        var today  = new Date();
        var timeDiff = dueDate.getTime() - today.getTime();
        //console.log(timeDiff);
        if(timeDiff > 0 ){
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
            return diffDays;
        }else{
            return 0;
        }
    }else{
        return 0;
    }
}*/

const clickToShowTasks = refObj => (cell,row) =>{
  return `${cell}`;
}

const setCustomerName = refOBj => (cell,row) =>{
  //return `${row.first_name} ${row.last_name}`;

  return (    
    <LinkWithTooltip
        tooltip={`${row.first_name + " " + row.first_name }`}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {row.first_name + " " + row.first_name}
    </LinkWithTooltip>
  );
}

const setPriorityName = refObj => (cell,row) =>{
    var ret = 'Set Priority';
    for (let index = 0; index < priority_arr.length; index++) {
        const element = priority_arr[index];
        if(element['priority_id'] === cell ){
            ret = element['priority_value'];
        }
    }

    return ret

}

const setAssignedTo = refOBj => (cell,row) => {
    return (    
    <LinkWithTooltip
        tooltip={`${row.at_emp_first_name + " " + row.at_emp_last_name + " (" + row.at_emp_desig_name + ")"}`}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {row.at_emp_first_name + " " + row.at_emp_last_name + " (" + row.at_emp_desig_name + ")"}
    </LinkWithTooltip>
  );
}

class AllocatedTasksTable extends Component {

    state = {
        showCreateSubTask   : false,
        showAssign          : false,
        showRespond         : false,
        showTaskDetails     : false,
        showRespondCustomer : false,
        showRespondBack     : false,
        showAuthorizeBack   : false,
        showReAssign        : false,
        task_id             : 0,
        activePage          : 1,
        itemPerPage         : 20,
        tableData           : [],
        empID               : 0,
        countMyTasks        : 0
    };

    checkHandler = (event) => {
        event.preventDefault();
    };

    handlePageChange = (pageNumber) => {
      this.setState({ activePage: pageNumber });
      this.getMyAlocatedTasks(pageNumber > 0 ? pageNumber  : 1);
    };
  
    getMyAlocatedTasks(page = 1) {
      let emp_id = this.state.empID;
      //console.log('>>>>>',emp_id);
      let url;
      url = `/api/employees/allocated_tasks/${emp_id}?page=${page}`;     
      API.get(url)
        .then(res => {
          this.setState({ 
            tableData: res.data.data, 
            countMyTasks: res.data.count_my_allocated_tasks});
        })
        .catch(err => { 
          showErrorMessage(err,this.props);
        });
    };

    componentDidMount(){
        //console.log("===>",this.props.countMyTasks);
        //console.log(this.props.tableData);
        this.setState({
            tableData:this.props.tableData,
            countMyTasks: this.props.countMyTasks,
            empID: this.props.empID,
            options : {
                clearSearch: true,
                expandBy: 'column',
                page: !this.state.start_page ? 1 : this.state.start_page,// which page you want to show as default
                sizePerPageList: [ {
                    text: '10', value: 10
                }, {
                    text: '20', value: 20
                }, {
                    text: 'All', value: !this.state.tableData ? 1 : this.state.tableData
                } ], // you can change the dropdown list for size per page
                sizePerPage: 20,  // which size per page you want to locate as default
                pageStartIndex: 1, // where to start counting the pages
                paginationSize: 3,  // the pagination bar size.
                prePage: '‹', // Previous page button text
                nextPage: '›', // Next page button text
                firstPage: '«', // First page button text
                lastPage: '»', // Last page button text
                //paginationShowsTotal: this.renderShowsTotal,  // Accept bool or function
                paginationPosition: 'bottom'  // default is bottom, top and both is all available
                // hideSizePerPage: true > You can hide the dropdown for sizePerPage
                // alwaysShowAllBtns: true // Always show next and previous button
                // withFirstAndLast: false > Hide the going to First and Last page button
            },
            cellEditProp:{
                mode: 'click',
                beforeSaveCell: this.onBeforeSetPriority, // a hook for before saving cell
                afterSaveCell: this.onAfterSetPriority // a hook for after saving cell
            }
        });
    }

    getSubTasks = (row) => {
        return(
            <AllocatedSubTaskTable dashType={this.props.dashType} tableData={row.sub_tasks} reloadTaskSubTask={()=>this.props.allTaskDetails()} />
        );
    }

    checkSubTasks = (row) => {

        //console.log("subtask")

        if( typeof row.sub_tasks !== 'undefined' && row.sub_tasks.length > 0 ){
            return true;
        }else{
            return false;
        }
    }

    handleClose = (closeObj) => {
        this.setState(closeObj);
    }

    tdClassName = (fieldValue, row) =>{
        var dynamicClass = 'width-150 ';
        if(row.vip_customer === 1){
            dynamicClass += 'bookmarked-column ';
        }
        return dynamicClass;
    }

    trClassName = (row,rowIndex) =>{
        var ret = '';
        // if( typeof row.notResponded !== 'undefined'){
        //     ret = 'tr-bold';
        // }else if( typeof row.responseExpired !== 'undefined'){
        //     ret =  'tr-red';
        // }

        //var replacedDate = row.due_date.split('T');
        var dueDate = new Date(row.due_date);
        var today  = new Date();
        var timeDiff = dueDate.getTime() - today.getTime();

        if(timeDiff > 0){

        }else{
            if(row.vip_customer === 1){
                ret += 'tr-red';
            }
        }

        return ret;
    }

    expandColumnComponent({ isExpandableRow, isExpanded }) {
        let content = '';
    
        if (isExpandableRow) {
          content = (isExpanded ? '-' : '+' );
        } else {
          content = ' ';
        }
        return (
          <div> { content } </div>
        );
    }

    render(){
        const paginationOptions = {
          page: 1, // which page you want to show as default
          sizePerPageList: [
            {
              text: "10",
              value: 10
            },
            {
              text: "20",
              value: 20
            },
            {
              text: "All",
              value: this.props.tableData.length > 0 ? this.props.tableData.length : 1
            }
          ], // you can change the dropdown list for size per page
          hideSizePerPage: true,
          sizePerPage: 20, // which size per page you want to locate as default
          pageStartIndex: 1, // where to start counting the pages
          paginationSize: 3, // the pagination bar size.
          prePage: '‹', // Previous page button text
          nextPage: '›', // Next page button text
          firstPage: '«', // First page button text
          lastPage: '»', // Last page button text
          //paginationShowsTotal: this.renderShowsTotal, // Accept bool or function
          paginationPosition: "bottom" // default is bottom, top and both is all available
          // hideSizePerPage: true //> You can hide the dropdown for sizePerPage
          // alwaysShowAllBtns: true // Always show next and previous button
          // withFirstAndLast: false //> Hide the going to First and Last page button
        };
        return (
            <>  
                <BootstrapTable 
                    data={this.state.tableData}
                    /*options={ paginationOptions } 
                    pagination */
                    expandableRow={ this.checkSubTasks } 
                    expandComponent={ this.getSubTasks }
                    expandColumnOptions={ 
                        { 
                            expandColumnVisible: true,
                            expandColumnComponent: this.expandColumnComponent,
                            columnWidth: 25
                        } 
                    }
                    trClassName={ this.trClassName }
                    >
                    <TableHeaderColumn isKey dataField='task_ref' columnClassName={ this.tdClassName } editable={ false } expandable={ false } dataFormat={ clickToShowTasks(this) } >Tasks</TableHeaderColumn>

                    <TableHeaderColumn dataField='request_type' editable={ false } expandable={ false } dataFormat={ setDescription(this) } >Description</TableHeaderColumn>

                    <TableHeaderColumn dataField='product_name' editable={ false } expandable={ false } dataFormat={ setProduct(this) } >Product Name</TableHeaderColumn>

                    <TableHeaderColumn dataField='display_assign_date' editable={ false } expandable={ false } dataFormat={setDate(this)}>Assigned Date</TableHeaderColumn>

                    <TableHeaderColumn dataField='display_date_added' editable={ false } expandable={ false } dataFormat={setDate(this)}>Created</TableHeaderColumn>

                    {/* <TableHeaderColumn dataField='rdd' dataFormat={ setCreateDate(this) } editable={ false } expandable={ false }>RDD</TableHeaderColumn> */}

                    <TableHeaderColumn dataField='display_new_due_date' editable={ false } expandable={ false } dataFormat={ setDate(this) } >Due Date</TableHeaderColumn>

                    <TableHeaderColumn dataField='dept_name' editable={ false } expandable={ false } dataFormat={ setAssignedBy(this) }>Assigned By</TableHeaderColumn>

                    <TableHeaderColumn dataField='dept_name' editable={ false } expandable={ false } dataFormat={ setAssignedTo(this) } >Assigned To </TableHeaderColumn>

                    <TableHeaderColumn dataField='company_name' dataFormat={ setCompanyName(this) } expandable={ false } >Customer</TableHeaderColumn>

                    <TableHeaderColumn dataField='cust_name' editable={ false } expandable={ false } dataFormat={ setCustomerName(this) } >User</TableHeaderColumn>

                    <TableHeaderColumn dataField='status' dataFormat={ getStatusColumn(this) } expandable={ false } editable={ false } >Status</TableHeaderColumn>

                    <TableHeaderColumn dataField="task_id" width="5%" dataFormat={actionFormatter(this)}>Task Details</TableHeaderColumn>
                     
                </BootstrapTable>
                {this.state.countMyTasks > 20 ? (
                  <Row>
                    <Col md={12}>
                      <div className="paginationOuter text-right">
                        <Pagination
                          activePage={this.state.activePage}
                          itemsCountPerPage={this.state.itemPerPage}
                          totalItemsCount={this.state.countMyTasks}
                          itemClass='nav-item'
                          linkClass='nav-link'
                          activeClass='active'
                          hideNavigation='false'
                          onChange={this.handlePageChange}
                        />
                      </div>
                    </Col>
                  </Row>
                  ) : null}
            </>
        );
    }
}

export default AllocatedTasksTable;