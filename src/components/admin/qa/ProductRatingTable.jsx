import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import API from "../../../shared/admin-axios";
import { showErrorMessage } from "../../../shared/handle_error";
import { getSuperAdmin, getAdminGroup, inArray, htmlDecode } from "../../../shared/helper";
import { Link } from "react-router-dom";
import Pagination from "react-js-pagination";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal,
  Table,
} from "react-bootstrap";
import { Formik, Field, Form } from "formik";
import Rating from "react-rating";


/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const actionFormatter = (refObj) => (cell, row) => {
  //console.log("===========", row);
  if (row.rating_product > 0) {
    return (
      <div className="actionStyle">
        <LinkWithTooltip
          tooltip="Click to View"
          href="#"
          clicked={(e) =>
            refObj.modalShowHandler(
              e,
              cell,
              row.rating_options,
              row.rating_comment,
              row.rating_product
            )
          }
          id="tooltip-1"
        >
          <i className="fas fa-eye" />
        </LinkWithTooltip>
      </div>
    );
  }
};

const setSubmittedBy = (refOBj) => (cell, row) => {
  if (row.rated_by > 0) {
    if (row.rated_by_type == 2) {
      return `${row.first_name_rated_by_agent} ${row.last_name_rated_by_agent} (Agent)`;
    } else {
      return `${row.first_name_rated_by} ${row.last_name_rated_by}`;
    }
  } else {
    return `-`;
  }
};

const setCompany = (refOBj) => (cell, row) => {

  if (row.rated_by_type == 2) {
    return `${row.agent_company}`;
  } else {
    if (row.company_name != null) {
      return `${row.company_name}`;
    } else {
      return "-";
    }
  }
};

const setCustomer = (refOBj) => (cell, row) => {
  if (row.rated_by_type == 2) {
    return `${row.first_name_rated_by_agent} ${row.last_name_rated_by_agent}`;
  } else {
    return `${row.first_name_rated_by} ${row.last_name_rated_by}`;
  }
};

const setRating = (refOBj) => (cell, row) => {
  if (row.rating_product > 0 && row.rating_skipped == 0) {
    return `${row.rating_product}`;
  } else if (row.rating_product == 0 && row.rating_skipped == 0) {
    return "Not rated";
  } else if (row.rating_skipped == 1 && row.rating_product == 0) {
    return "Skipped";
  }
};

const setProduct = (refOBj) => (cell, row) => {
  return htmlDecode(row.product_name);
};

class ProductRatingTable extends Component {
  state = {
    tableData: [],
    activePage: 1,
    totalCount: 0,
    itemPerPage: 20,
  };

  componentDidMount() {
    this.setState({
      tableData: this.props.tableData,
      count: this.props.count_product,
      /* rating_score: this.props.search_rating_score,
      type: this.props.search_type,
      compid: this.props.search_compid,
      cid: this.props.search_cid, */
    });
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getTaskRatings(pageNumber > 0 ? pageNumber : 1);
  };

  getTaskRatings(page = 1) {
    API.get(`/api/fa/task_ratings_product?page=${page}&type=${encodeURIComponent(this.props.search_type)}&rating_score=${encodeURIComponent(
      this.props.search_rating_score
    )}&compid=${encodeURIComponent(this.props.search_compid)}&cid=${encodeURIComponent(
      this.props.search_cid
    )}`)
      .then(res => {
        this.setState({
          tableData: res.data.data,
          count: res.data.count_task_ratings_product
        });
      })
      .catch(err => {
        showErrorMessage(err, this.props);
      });
  }

  modalShowHandler = (
    event,
    id,
    rating_options,
    rating_comment,
    rating_stars
  ) => {
    event.preventDefault();
    if (rating_options && rating_options.length > 0) {
      var rating_options = rating_options.split(",");
      API.get(`api/fa/task_ratings_options`)
        .then((res) => {
          var taskOptionArr = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            if (inArray(element.o_id, rating_options)) {
              taskOptionArr.push([element.option_name_en]);
            }
          }
          var r_comment = rating_comment;

          this.setState({
            taskOptionArr: taskOptionArr,
            comment: r_comment,
            rating_stars: rating_stars,
            showModal: true,
          });
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      var taskOptionArr = [];
      var r_comment = rating_comment;
      this.setState({
        taskOptionArr: taskOptionArr,
        comment: r_comment,
        rating_stars: rating_stars,
        showModal: true,
      });
    }
  };

  modalCloseHandler = () => {
    this.setState({ showModal: false });
  };

  render() {
    return (
      <>
        <div>
          <BootstrapTable data={this.state.tableData}>
            <TableHeaderColumn
              isKey
              dataField="rated_by"
              dataFormat={setSubmittedBy(this)}
            >
              Submitted By
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="company_name"
              dataFormat={setCompany(this)}
            >
              Company Name
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="cust_name"
              dataFormat={setCustomer(this)}
            >
              User Name
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="rating"
              dataFormat={setRating(this)}
            >
              Rating
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="product_name"
              dataFormat={setProduct(this)}
            >
              Product Name
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="task_id"
              dataFormat={actionFormatter(this)}
            >
              View
           </TableHeaderColumn>
          </BootstrapTable>
          {this.state.count > this.state.itemPerPage ? (
            <Row>
              <Col md={12}>
                <div className="paginationOuter text-right">
                  <Pagination
                    activePage={this.state.activePage}
                    itemsCountPerPage={this.state.itemPerPage}
                    totalItemsCount={this.state.count}
                    itemClass='nav-item'
                    linkClass='nav-link'
                    activeClass='active'
                    onChange={this.handlePageChange}
                  />
                </div>
              </Col>
            </Row>
          ) : null}
          <Modal
            show={this.state.showModal}
            onHide={() => this.modalCloseHandler()}
            backdrop="static"
            className="feedbckProv"
          >
            <Formik>
              {({ }) => {
                return (
                  <Form>
                    <Modal.Header>
                      <Modal.Title>Feedback provided</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <div className="contBox">
                        {this.state.rating_stars &&
                          this.state.rating_stars != null ? (
                            <Row>
                              <Col xs={12} sm={12} md={12}>
                                <div className="form-group">
                                  <h2> Rating</h2>
                                  <div className="ratinfo starimg">
                                    <span>
                                      {" "}
                                      <Rating
                                        initialRating={
                                          this.state.rating_stars
                                        }
                                        readonly
                                        emptySymbol={
                                          <img
                                            src={require("../../../assets/images/uncheck-star.svg")}
                                            alt=""
                                            className="icon"
                                          />
                                        }
                                        placeholderSymbol={
                                          <img
                                            src={require("../../../assets/images/uncheck-stars.svg")}
                                            alt=""
                                            className="icon"
                                          />
                                        }
                                        fullSymbol={
                                          <img
                                            src={require("../../../assets/images/uncheck-stars.svg")}
                                            alt=""
                                            className="icon"
                                          />
                                        }
                                      />
                                    </span>
                                  </div>
                                </div>
                              </Col>
                            </Row>
                          ) : null}

                        <div className="opsn">
                          {this.state.taskOptionArr &&
                            this.state.taskOptionArr != "" ? (
                              <div>
                                <p>Options selected</p>
                                {this.state.taskOptionArr.map(
                                  (option, i) => {
                                    return (
                                      <Row>
                                        <Col xs={12} sm={12} md={12}>
                                          <div className="form-group">
                                            <div className="checkboxfeedbck">
                                              <input
                                                type="checkbox"
                                                checked="checked"
                                              />
                                              <span> {option}</span>
                                            </div>
                                          </div>
                                        </Col>
                                      </Row>
                                    );
                                  }
                                )}
                              </div>
                            ) : null}
                          {this.state.comment &&
                            this.state.comment != null ? (
                              <Row>
                                <Col xs={12} sm={12} md={12}>
                                  <div className="form-group">
                                    <label>Comment</label>
                                    <span> {this.state.comment}</span>
                                  </div>
                                </Col>
                              </Row>
                            ) : null}
                        </div>
                      </div>
                    </Modal.Body>
                    <Modal.Footer>
                      <button
                        onClick={(e) => this.modalCloseHandler()}
                        className={`btn btn-danger btn-sm`}
                        type="button"
                      >
                        Close
                                </button>
                    </Modal.Footer>
                  </Form>
                );
              }}
            </Formik>
          </Modal>
        </div>

      </>
    );
  }
}

export default ProductRatingTable;
