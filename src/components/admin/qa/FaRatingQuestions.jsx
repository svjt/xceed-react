import React, { Component } from "react";
import Pagination from "react-js-pagination";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal,
} from "react-bootstrap";
//import { Label } from 'reactstrap';
import { Link } from "react-router-dom";
import { Formik, Field, Form } from "formik";
import swal from "sweetalert";
import * as Yup from "yup";

import Layout from "../layout/Layout";
import whitelogo from "../../../assets/images/drreddylogo_white.png";
import API from "../../../shared/admin-axios";
import { showErrorMessage } from "../../../shared/handle_error";
import { htmlDecode } from "../../../shared/helper";
import { getSuperAdmin, getAdminGroup } from "../../../shared/helper";
import Select from "react-select";

const rating_options_select = [
  { value: "1", label: "One" },
  { value: "2", label: "Two" },
  { value: "3", label: "Three" },
  { value: "4", label: "Four" },
  { value: "5", label: "Five" },
];

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

const actionFormatter = (refObj) => (cell) => {
  //console.log(refObj.state.access)
  return (
    <div className="actionStyle">
      {refObj.state.access.edit === true ? (
        <LinkWithTooltip
          tooltip="Click to Edit"
          href="#"
          clicked={(e) => refObj.modalShowHandler(e, cell)}
          id="tooltip-1"
        >
          <i className="far fa-edit" />
        </LinkWithTooltip>
      ) : null}
      {refObj.state.access.delete === true ? (
        <LinkWithTooltip
          tooltip="Click to Delete"
          href="#"
          clicked={(e) => refObj.confirmDelete(e, cell)}
          id="tooltip-1"
        >
          <i className="far fa-trash-alt" />
        </LinkWithTooltip>
      ) : null}
    </div>
  );
};

const custContent = () => (cell) => {
  return htmlDecode(cell);
};

const initialValues = {};

class TaskRatingQuestions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      get_access_data: false,
      activePage: 1,
      totalCount: 0,
      itemPerPage: 20,
      ratingQuestionsDetails: [],
      ratingQuestionsflagId: 0,
      showModal: false,
      showModalLoader: false,
    };
  }

  componentDidMount() {
    const superAdmin = getSuperAdmin(localStorage.admin_token);

    if (superAdmin === 1) {
      this.setState({
        access: {
          view: true,
          add: true,
          edit: true,
          delete: true,
        },
        get_access_data: true,
      });
      this.getTaskQuestionList();
    } else {
      const adminGroup = getAdminGroup(localStorage.admin_token);
      API.get(
        `/api/adm_group/single_access/${adminGroup}/${"TASK_RATING_QUESTIONS_MANAGEMENT"}`
      )
        .then((res) => {
          this.setState({
            access: res.data.data,
            get_access_data: true,
          });

          if (res.data.data.view === true) {
            this.getTaskQuestionList();
          } else {
            this.props.history.push("/admin/dashboard");
          }
        })
        .catch((err) => {
          showErrorMessage(err, this.props);
        });
    }
  }

  getTaskQuestionList(page = 1) {
    API.get(`/api/fa/fa_ratings_questions?page=${page}`)
      .then((res) => {
        this.setState({
          ratings_questions: res.data.data,
          count: res.data.count_fa_ratings_questions,
          isLoading: false,
        });
      })
      .catch((err) => {
        this.setState({
          isLoading: false,
        });
        showErrorMessage(err, this.props);
      });
    this.setState({
      isLoading: false,
    });
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getTaskQuestionList(pageNumber > 0 ? pageNumber : 1);
  };

  modalShowHandler = (event, id) => {
    if (id) {
      event.preventDefault();
      console.log(id);
      API.get(`/api/fa/ratings_questions/${id}`)
        .then((res) => {
          var rating = res.data.data.rating.split(",");
          let questions_select_arr = [];
          let questions_select_id = [];

          for (let index = 0; index < rating.length; index++) {
            if (rating[index] == 1) {
              questions_select_arr.push({ value: "1", label: "One" });
              questions_select_id.push(1);
            } else if (rating[index] == 2) {
              questions_select_arr.push({ value: "2", label: "Two" });
              questions_select_id.push(2);
            } else if (rating[index] == 3) {
              questions_select_arr.push({ value: "3", label: "Three" });
              questions_select_id.push(3);
            } else if (rating[index] == 4) {
              questions_select_arr.push({ value: "4", label: "Four" });
              questions_select_id.push(4);
            } else if (rating[index] == 5) {
              questions_select_arr.push({ value: "5", label: "Five" });
              questions_select_id.push(5);
            }
          }

          this.setState({
            ratingQuestionsDetails: res.data.data,
            ratingQuestionsflagId: id,
            selectedQuestionList: questions_select_arr,
            selectedQuestion: questions_select_id,
            isLoading: false,
            showModal: true,
          });
        })
        .catch((err) => {
          showErrorMessage(err, this.props);
        });
    } else {
      this.setState({
        ratingQuestionsDetails: [],
        ratingQuestionsflagId: 0,
        selectedQuestionList: [],
        selectedQuestion: [],
        showModal: true,
      });
    }
  };

  modalCloseHandler = () => {
    this.setState({ ratingQuestionsflagId: 0 });
    this.setState({ showModal: false });
  };

  handleSubmitEvent = (values, actions) => {
    const post_data = {
      question_name_en: values.question_name_en,
      question_name_zh: values.question_name_zh,
      question_name_pt: values.question_name_pt,
      question_name_es: values.question_name_es,
      question_name_ja: values.question_name_ja,
      option_list: values.option_list,
    };

    if (this.state.ratingQuestionsflagId) {
      this.setState({ showModalLoader: true });
      const id = this.state.ratingQuestionsflagId;
      API.put(`/api/fa/fa_ratings_questions/${id}`, post_data)
        .then((res) => {
          this.modalCloseHandler();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record updated successfully.",
            icon: "success",
          }).then(() => {
            this.setState({ showModalLoader: false });
            this.getTaskQuestionList(this.state.activePage);
          });
        })
        .catch((err) => {
          this.setState({ showModalLoader: false });
          if (err.data.status === 3) {
            this.setState({
              showModal: false,
            });
            showErrorMessage(err, this.props);
          } else {
            actions.setErrors(err.data.errors);
            actions.setSubmitting(false);
          }
        });
    } else {
      this.setState({ showModalLoader: true });
      API.post("/api/fa/fa_ratings_questions", post_data)
        .then((res) => {
          this.modalCloseHandler();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record added successfully.",
            icon: "success",
          }).then(() => {
            this.setState({ activePage: 1, showModalLoader: false });
            this.getTaskQuestionList(this.state.activePage);
          });
        })
        .catch((err) => {
          this.setState({ showModalLoader: false });
          if (err.data.status === 3) {
            this.setState({
              showModal: false,
            });
            showErrorMessage(err, this.props);
          } else {
            actions.setErrors(err.data.errors);
            actions.setSubmitting(false);
          }
        });
    }
  };

  confirmDelete = (event, id) => {
    event.preventDefault();
    swal({
      closeOnClickOutside: false,
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.deleteTaskRatings(id);
      }
    });
  };

  deleteTaskRatings = (id) => {
    if (id) {
      API.delete(`/api/fa/fa_ratings_questions/${id}`)
        .then((res) => {
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record deleted successfully.",
            icon: "success",
          }).then(() => {
            this.setState({ activePage: 1 });
            this.getTaskQuestionList(this.state.activePage);
          });
        })
        .catch((err) => {
          if (err.data.status === 3) {
            this.setState({ closeModal: true });
            showErrorMessage(err, this.props);
          }
        });
    }
  };

  render() {
    const { ratingQuestionsDetails } = this.state;
    const newInitialValues = Object.assign(initialValues, {
      question_name_en: ratingQuestionsDetails.question_name_en
        ? htmlDecode(ratingQuestionsDetails.question_name_en)
        : "",
      question_name_zh: ratingQuestionsDetails.question_name_zh
        ? htmlDecode(ratingQuestionsDetails.question_name_zh)
        : "",
      question_name_pt: ratingQuestionsDetails.question_name_pt
        ? htmlDecode(ratingQuestionsDetails.question_name_pt)
        : "",
      question_name_es: ratingQuestionsDetails.question_name_es
        ? htmlDecode(ratingQuestionsDetails.question_name_es)
        : "",
      question_name_ja: ratingQuestionsDetails.question_name_ja
        ? htmlDecode(ratingQuestionsDetails.question_name_ja)
        : "",
      option_list: this.state.selectedQuestion
        ? this.state.selectedQuestion
        : "",
    });
    const validateStopFlag = Yup.object().shape({
      question_name_en: Yup.string().required("Please enter question name"),
      question_name_zh: Yup.string().required("Please enter question name"),
      question_name_pt: Yup.string().required("Please enter question name"),
      question_name_es: Yup.string().required("Please enter question name"),
      question_name_ja: Yup.string().required("Please enter question name"),
      option_list: Yup.array()
        .ensure()
        .min(1, "Please add at least one rating options.")
        .of(Yup.string().ensure().required("Rating options cannot be empty")),
    });

    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy">
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <h1>
                    FA Question Options
                    <small />
                  </h1>
                </div>
                <div className="col-lg-12 col-sm-12 col-xs-12 topSearchSection">
                  {this.state.access.add === true ? (
                    <div className="">
                      <button
                        type="button"
                        className="btn btn-info btn-sm"
                        onClick={(e) => this.modalShowHandler(e, "")}
                      >
                        <i className="fas fa-plus m-r-5" /> Add Question Options
                      </button>
                    </div>
                  ) : null}
                  <div className="clearfix"></div>
                </div>
              </div>
            </section>
            <section className="content">
              <div className="box">
                <div className="box-body">
                  <BootstrapTable data={this.state.ratings_questions}>
                    <TableHeaderColumn
                      isKey
                      dataField="question_name_en"
                      dataFormat={custContent(this)}
                    >
                      Question Name
                    </TableHeaderColumn>
                    {/* <TableHeaderColumn
                      dataField="question_name_zh"
                      dataFormat={custContent(this)}
                    >
                      Option Name Mandarin
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="question_name_pt"
                      dataFormat={custContent(this)}
                    >
                      Option Name Portuguese
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="question_name_es"
                      dataFormat={custContent(this)}
                    >
                      Option Name Spanish
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="question_name_ja"
                      dataFormat={custContent(this)}
                    >
                      Option Name Japanese
                    </TableHeaderColumn> */}
                    <TableHeaderColumn dataField="rating">
                      Rating
                    </TableHeaderColumn>

                    {this.state.access.edit === true ||
                      this.state.access.delete === true ? (
                        <TableHeaderColumn
                          dataField="q_id"
                          dataFormat={actionFormatter(this)}
                        >
                          Action
                      </TableHeaderColumn>
                      ) : null}
                  </BootstrapTable>

                  {this.state.count > this.state.itemPerPage ? (
                    <Row>
                      <Col md={12}>
                        <div className="paginationOuter text-right">
                          <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemPerPage}
                            totalItemsCount={this.state.count}
                            itemClass="nav-item"
                            linkClass="nav-link"
                            activeClass="active"
                            onChange={this.handlePageChange}
                          />
                        </div>
                      </Col>
                    </Row>
                  ) : null}

                  {/* ======= Add/Edit ======== */}
                  <Modal
                    show={this.state.showModal}
                    onHide={() => this.modalCloseHandler()}
                    backdrop="static"
                  >
                    <Formik
                      initialValues={newInitialValues}
                      validationSchema={validateStopFlag}
                      onSubmit={this.handleSubmitEvent}
                    >
                      {({
                        values,
                        errors,
                        touched,
                        isValid,
                        isSubmitting,
                        setFieldValue,
                        setFieldTouched,
                      }) => {
                        return (
                          <Form>
                            {this.state.showModalLoader === true ? (
                              <div className="loading_reddy_outer">
                                <div className="loading_reddy">
                                  <img src={whitelogo} alt="loader" />
                                </div>
                              </div>
                            ) : (
                                ""
                              )}
                            <Modal.Header closeButton>
                              <Modal.Title>
                                {this.state.ratingQuestionsflagId > 0
                                  ? "Edit"
                                  : "Add"}{" "}
                                Rating Questions
                              </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="contBox">
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>
                                        Question English
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="question_name_en"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter name"
                                        autoComplete="off"
                                        value={values.question_name_en}
                                      />
                                      {errors.question_name_en &&
                                        touched.question_name_en ? (
                                          <span className="errorMsg">
                                            {errors.question_name_en}
                                          </span>
                                        ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>
                                        Question Mandarin
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="question_name_zh"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter name"
                                        autoComplete="off"
                                        value={values.question_name_zh}
                                      />
                                      {errors.question_name_zh &&
                                        touched.question_name_zh ? (
                                          <span className="errorMsg">
                                            {errors.question_name_zh}
                                          </span>
                                        ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>
                                        Question Portuguese
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="question_name_pt"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter name"
                                        autoComplete="off"
                                        value={values.question_name_pt}
                                      />
                                      {errors.question_name_pt &&
                                        touched.question_name_pt ? (
                                          <span className="errorMsg">
                                            {errors.question_name_pt}
                                          </span>
                                        ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>
                                        Question Spanish
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="question_name_es"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter name"
                                        autoComplete="off"
                                        value={values.question_name_es}
                                      />
                                      {errors.question_name_es &&
                                        touched.question_name_es ? (
                                          <span className="errorMsg">
                                            {errors.question_name_es}
                                          </span>
                                        ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>
                                        Question Japanese
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="question_name_ja"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter name"
                                        autoComplete="off"
                                        value={values.question_name_ja}
                                      />
                                      {errors.question_name_ja &&
                                        touched.question_name_ja ? (
                                          <span className="errorMsg">
                                            {errors.question_name_ja}
                                          </span>
                                        ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>Options</label>

                                      <Select
                                        isMulti
                                        name="option_list[]"
                                        options={rating_options_select}
                                        className="basic-multi-select"
                                        classNamePrefix="select"
                                        onChange={(evt) =>
                                          setFieldValue(
                                            "option_list",
                                            [].slice
                                              .call(evt)
                                              .map((val) => val.value)
                                          )
                                        }
                                        placeholder="Options"
                                        onBlur={() =>
                                          setFieldTouched("option_list")
                                        }
                                        defaultValue={
                                          this.state.selectedQuestionList
                                        }
                                      />
                                      {errors.option_list ? (
                                        <span className="errorMsg">
                                          {errors.option_list}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                {errors.message ? (
                                  <Row>
                                    <Col xs={12} sm={12} md={12}>
                                      <span className="errorMsg">
                                        {errors.message}
                                      </span>
                                    </Col>
                                  </Row>
                                ) : (
                                    ""
                                  )}
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                              <button
                                className={`btn btn-success btn-sm ${
                                  isValid ? "btn-custom-green" : "btn-disable"
                                  } mr-2`}
                                type="submit"
                                disabled={isValid ? false : false}
                              >
                                {this.state.ratingQuestionsflagId > 0
                                  ? isSubmitting
                                    ? "Updating..."
                                    : "Update"
                                  : isSubmitting
                                    ? "Submitting..."
                                    : "Submit"}
                              </button>
                              <button
                                onClick={(e) => this.modalCloseHandler()}
                                className={`btn btn-danger btn-sm`}
                                type="button"
                              >
                                Close
                              </button>
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>
                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}
export default TaskRatingQuestions;
