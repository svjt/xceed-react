import React, { Component } from "react";
import Pagination from "react-js-pagination";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import { Formik, Field, Form } from "formik";
import swal from "sweetalert";

import Layout from "../layout/Layout";
import whitelogo from "../../../assets/images/drreddylogo_white.png";
import API from "../../../shared/admin-axios";
import { showErrorMessage } from "../../../shared/handle_error";
import { htmlDecode } from "../../../shared/helper";
import { getSuperAdmin, getAdminGroup } from "../../../shared/helper";
import ReactHtmlParser from "react-html-parser";



const custContent = () => (cell) => {
  return htmlDecode(cell);
};

const customerContent = () => (cell, row) => {
  if (cell == 1) {
    return row.cust_name;
  } else {
    return row.agent_name;
  }
};

const customerCompany = () => (cell, row) => {
  if (cell == 1) {
    return row.company_name;
  } else {
    return row.agent_company;
  }
};

class Emails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      get_access_data: false,
      activePage: 1,
      totalCount: 0,
      itemPerPage: 20,
      emailLogDetails: [],
      emailflagId: 0,
      showModal: false,
      showModalLoader: false,
      search_product_name: '',
      search_is_enquiry: '',
      requested_for_list: [
        {
          id: 1,
          question: `Want to know more Orange Book details of the particular product?`,
          title: `USFDA`,
          //answer   : `Retrieve details from the USFDA Orange Book Details for a product. This will provide information on the player market, NDC (National Drug Code), and the relevant application numbers. If you want more details on the dosage forms, pill details
          //(like color, shape, size), strength, whether RLD is available, please click on Request for more information.`
          answer: `The USFDA Orange Book details information on the market players, NDC (National Drug Code), and the relevant application numbers. For more information on the dosage forms, pill details (color, shape, size), strength, or whether a RLD is available, please click on Request further information`
        },
        {
          id: 3,
          question: 'Want to know the EMA details of the product?',
          title: `EMA Product Details`,
          //answer: `Retrieve details from the European Medicenes Agency for a product. If you require information on last modified date or indications it is approved for, please click on Request for more information.`
          answer: `If you require information on the last modified date or approved indications, please click on Request further information.`
        },
        {
          id: 2,
          question: 'Want to know the TGA details of the product (Australia)?',
          title: `TGA Product Details`,
          //answer: `Retrieve details from the Therapeutics Goods Administration (Australia) for a product.`
          answer: `Approvals by the Therapeutics Goods Administration (Australia).`
        },
        {
          id: 4,
          question: 'What are the details of the API of the product?',
          title: `API Details`,
          //answer: `Retrieve details from the API DrugBank for a particular API. This will provide information on the product structure & identification, experimental properties, calculated properties, and its pharmcology.`
          answer: `Get more information on the API structure and identification, experimental properties, calculated properties, and pharmacology. Source: API Drugbank`
        },
        {
          id: 5,
          question: 'What are the various patents associated with a product?',
          title: `Patent Details`,
          //answer: `Retrieve list of all patents associated with a product along with a patent expiry date, with links to the patent offices (USPTO) for dossiers. If you require more information on claims on the patents (independent or dependent claims), please
          //click on Request for more information`
          answer: `Here you can find a list of all patents associated with a product along with a patent expiry date, with links to the patent offices (USPTO) for dossiers. If you require more information on claims on the patents (independent or dependent claims), please write to us by clicking on Request further information.`,
        },
        {
          id: 6,
          question: 'For a particular product, what are the various excipients used?',
          title: `Excipient Information`,
          //answer: `Retrieve information on a product (please provide either the specific NDC code or the organization). You will receive all the Orange book details of the product, various excipients used with the following details for each excipient: Name, functional
          //category, daily dosage limit, regulatory status, incompatibilities, links for each to Inxight: Drugs`
          answer: `Search by the NDC code or the organization to get further information on for each excipient: Name, functional category, daily dosage limit, regulatory status, incompatibilities, links for each to Inxight: Drugs. Source: Orange Book`
        },
        {
          id: 7,
          question: 'For a particular product, what are the various alternate excipients used?',
          title: `Alternate Excipient Information`,
          answer: `Find an alternate excipient for the required formulation based on a proprietary AI / ML algorithm based on the functional use of the excipient.`
        }
        // ,
        // {
        //   id: 7,
        //   question: 'What are alternate excipients that can be used?',
        //   answer:`Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`
        // }
      ]
    };
  }

  componentDidMount() {
    const superAdmin = getSuperAdmin(localStorage.admin_token);

    if (superAdmin === 1) {
      this.setState({
        access: {
          view: true,
          add: true,
          edit: true,
          delete: true,
        },
        get_access_data: true,
      });
      this.getEmailLogList();
    } else {
      const adminGroup = getAdminGroup(localStorage.admin_token);
      API.get(
        `/api/adm_group/single_access/${adminGroup}/${"EMAIL_LOG_MANAGEMENT"}`
      )
        .then((res) => {
          this.setState({
            access: res.data.data,
            get_access_data: true,
          });

          if (res.data.data.view === true) {
            this.getEmailLogList();
          } else {
            this.props.history.push("/admin/dashboard");
          }
        })
        .catch((err) => {
          showErrorMessage(err, this.props);
        });
    }
  }

  getEmailLogList(page = 1) {
    let is_enquiry = this.state.search_is_enquiry;
    let product_name = this.state.search_product_name;

    API.get(`/api/fa/task_search?page=${page}&is_enquiry=${encodeURIComponent(is_enquiry)}&product_name=${encodeURIComponent(product_name)}`)
      .then((res) => {
        this.setState({
          emails: res.data.data,
          count: res.data.cnt,
          isLoading: false,
          search_is_enquiry: is_enquiry,
          search_product_name: product_name,
        });
      })
      .catch((err) => {
        this.setState({
          isLoading: false,
        });
        showErrorMessage(err, this.props);
      });
    this.setState({
      isLoading: false,
    });
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getEmailLogList(pageNumber > 0 ? pageNumber : 1);
  };


  productSearch = (e) => {
    e.preventDefault();

    var is_enquiry = document.getElementById('is_enquiry').value;
    var product_name = document.getElementById('product_name').value;

    if (is_enquiry === "" && product_name === "") {
      return false;
    }

    API.get(`/api/fa/task_search?page=1&is_enquiry=${encodeURIComponent(is_enquiry)}&product_name=${encodeURIComponent(product_name)}`)
      .then(res => {
        this.setState({
          emails: res.data.data,
          count: res.data.cnt,
          isLoading: false,
          search_is_enquiry: is_enquiry,
          search_product_name: product_name,
          remove_search: true,
          activePage: 1
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        showErrorMessage(err, this.props);
      });
  }

  clearSearch = () => {
    document.getElementById('is_enquiry').value = "";
    document.getElementById('product_name').value = "";

    this.setState({
      search_is_enquiry: "",
      search_product_name: "",
      remove_search: false
    }, () => {
      this.getEmailLogList();
      this.setState({ activePage: 1 })
    })
  }

  custEnquiryType = () => (cell) => {
    let ret = ''
    for (let index = 0; index < this.state.requested_for_list.length; index++) {
      const element = this.state.requested_for_list[index];
      if (element.id === cell) {
        ret = element.title;
      }
    }
    return ret;
  };

  render() {

    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy">
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <h1>
                    Product Search
                    <small />
                  </h1>
                </div>
                <div className="col-lg-12 col-sm-12 col-xs-12 topSearchSection">
                  <form className="form">
                    <div className="">
                      {/* <input
                        className="form-control"
                        name="is_enquiry"
                        id="is_enquiry"
                        placeholder="Enquiry"
                      /> */}
                      <select
                        name="is_enquiry"
                        id="is_enquiry"
                        className="form-control"
                      >
                        <option value="">Enquiry</option>
                        <option value="1">USFDA</option>
                        <option value="2">TGA Product Details</option>
                        <option value="3">EMA Product Details</option>
                        <option value="4">API Details</option>
                        <option value="5">Patent Details</option>
                        <option value="6">Excipient Information</option>
                        <option value="7">Alternate Excipient Information</option>
                      </select>
                    </div>
                    <div className="">
                      <input
                        className="form-control"
                        name="product_name"
                        id="product_name"
                        placeholder="Product name"
                      />
                    </div>
                    <div className="">
                      <input
                        type="submit"
                        value="Search"
                        className="btn btn-warning btn-sm"
                        onClick={(e) => this.productSearch(e)}
                      />
                      {this.state.remove_search ? <a onClick={() => this.clearSearch()} className="btn btn-danger btn-sm"> Remove </a> : null}
                    </div>
                  </form>
                  <div className="clearfix"></div>
                </div>
              </div>
            </section>
            <section className="content">
              <div className="box">
                <div className="box-body">
                  <BootstrapTable data={this.state.emails}>
                    <TableHeaderColumn
                      isKey
                      dataField="enquiry_type"
                      width="40%"
                      dataFormat={this.custEnquiryType(this)}
                    >
                      Enquiry
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="product_name"
                      width="22%"
                      dataFormat={custContent(this)}
                    >
                      Product Name
                    </TableHeaderColumn>

                    <TableHeaderColumn dataField="date_added">
                      Search Date
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="customer_type"
                      dataFormat={customerContent(this)}
                    >
                      Customer Name
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="customer_type"
                      dataFormat={customerCompany(this)}
                    >
                      Company Name
                    </TableHeaderColumn>

                  </BootstrapTable>

                  {this.state.count > this.state.itemPerPage ? (
                    <Row>
                      <Col md={12}>
                        <div className="paginationOuter text-right">
                          <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemPerPage}
                            totalItemsCount={this.state.count}
                            itemClass="nav-item"
                            linkClass="nav-link"
                            activeClass="active"
                            onChange={this.handlePageChange}
                          />
                        </div>
                      </Col>
                    </Row>
                  ) : null}

                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}
export default Emails;
