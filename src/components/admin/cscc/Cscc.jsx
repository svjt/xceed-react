import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal
} from "react-bootstrap";
import { Link } from "react-router-dom";
import API from "../../../shared/admin-axios";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import Select from "react-select";
import Layout from "../layout/Layout";
//import DashboardSearch from "./DashboardSearch";
import whitelogo from '../../../assets/images/drreddylogo_white.png';
import {showErrorMessage} from "../../../shared/handle_error";
import Pagination from "react-js-pagination";
import { htmlDecode } from '../../../shared/helper';
import { getSuperAdmin, getAdminGroup } from '../../../shared/helper';

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const actionFormatter = refObj => (cell,row) => {
  return (
     
    <div className="actionStyle">
    {/* {refObj.state.access.edit === true ?
      <LinkWithTooltip
        tooltip="Click to Edit"
        href="#"
        clicked={e => refObj.modalShowHandler(e, cell)}
        id="tooltip-1"
      >
        <i className="far fa-edit" />
      </LinkWithTooltip>
      : null } */}

      {row.field_type === 'dropdown' && <LinkWithTooltip
        tooltip="Click to Add"
        href="#"
        clicked={e => refObj.modalShowHandler(e, cell)}
        id="tooltip-1"
      >
        <i className="fa fa-plus" />
      </LinkWithTooltip>}
      {`  `}

      {row.field_type === 'dropdown' && row.values != null  ?
      <LinkWithTooltip
        tooltip="Click to View"
        href="#"
        clicked={e => refObj.showDisplayModal(e, cell)}
        id="tooltip-1"
      >
        <i className="far fas fa-eye" />
      </LinkWithTooltip>
      : null }

      {row.field_type === 'dropdown' ?
      <LinkWithTooltip
        tooltip="Click to Delete"
        href="#"
        clicked={e => refObj.showDeleteModal(e, cell)}
        id="tooltip-1"
      >
        <i className="far fa-trash-alt" />
      </LinkWithTooltip>
      : null }

    </div>
  );
};

const __htmlDecode = refObj => cell => {
  return htmlDecode(cell);
}

const initialValues = {
  title: ''
};

class Cscc extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      checklist: [],
      csccDetails: [],
      activePage      : 1,
      totalCount      : 0,
      itemPerPage     : 20,
      csccFlagId: 0,
      showModal: false,
      showModalLoader : false,
      search_title: '',
      search_type: '',
      search_required: '',
      remove_search: false,
      selectStatus: [
        { id: "0", name: "Inactive" },
        { id: "1", name: "Active" }
      ],
      get_access_data:false,
      delete_err_msg:''
    }
  }

  componentDidMount() {
    const superAdmin  = getSuperAdmin(localStorage.admin_token);
    if(superAdmin === 1){
      this.setState({
        access: {
           view : true,
           add : true,
           edit : true,
           delete : true
         },
         get_access_data:true
     });
     this.getCheckList();
    }else{
      const adminGroup  = getAdminGroup(localStorage.admin_token);
      API.get(`/api/adm_group/single_access/${adminGroup}/${'COMMERCIAL_CHECKLIST_MANAGEMENT'}`)
      .then(res => {
        this.setState({
          access: res.data.data,
          get_access_data:true
        }); 

        if(res.data.data.view === true){
          this.getCheckList();
        }else{
          this.props.history.push('/admin/dashboard');
        }
        
      })
      .catch(err => {
        showErrorMessage(err,this.props);
      });
    } 
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getCheckList(pageNumber > 0 ? pageNumber  : 1);
  };

  getCheckList(page = 1) {

    let f_title = this.state.search_title;
    let f_type = this.state.search_type;
    let f_required = this.state.search_required;

    API.get(`/api/feed/commercial_checklist?page=${page}&f_title=${encodeURIComponent(f_title)}&f_type=${encodeURIComponent(f_type)}&f_required=${encodeURIComponent(f_required)}`)
      .then(res => {
        this.setState({
          checklist: res.data.data,
          count_checklist: res.data.count_commercial_checklist,
          isLoading: false,
          search_title: f_title,
          search_type: f_type,
          search_required: f_required
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        showErrorMessage(err,this.props);
      });
  }

  checkListSearch = (e) => {
    e.preventDefault();

    var f_title = document.getElementById('f_title').value;
    var f_type = document.getElementById('f_type').value;
    var f_required = document.getElementById('f_required').value;

    if(f_title === "" && f_type === "" && f_required === ""){
      return false;
    }

    API.get(`/api/feed/commercial_checklist?page=1&f_title=${encodeURIComponent(f_title)}&f_type=${encodeURIComponent(f_type)}&f_required=${encodeURIComponent(f_required)}`)
    .then(res => {
      this.setState({
        checklist: res.data.data,
        count_checklist: res.data.count_commercial_checklist,
        isLoading: false,
        search_title: f_title,
        search_type: f_type,
        search_required: f_required,
        remove_search: true,
        activePage: 1
      });
    })
    .catch(err => {
      this.setState({
        isLoading: false
      });
      showErrorMessage(err,this.props);
    });
  }

  clearSearch = () => {
    document.getElementById('f_title').value = "";
    document.getElementById('f_type').value = "";
    document.getElementById('f_required').value = "";

    this.setState({
      search_title: "",
      search_type: "",
      search_required: "",
      remove_search: false
    }, () => {
      this.getCheckList();
      this.setState({activePage: 1})
    })
  }

  downloadXLSX = (e) => {
    e.preventDefault();

    var f_title = document.getElementById('f_title').value;    

    API.get(`/api/feed/commercial_checklist/download?page=1&f_title=${encodeURIComponent(f_title)}`,{responseType: 'blob'})
    .then(res => { 
      let url    = window.URL.createObjectURL(res.data);
      let a      = document.createElement('a');
      a.href     = url;
      a.download = 'commercial_checklist.xlsx';
      a.click();

    }).catch(err => {
      showErrorMessage(err,this.props);
    });
  }

  checkHandler = (event) => {
    event.preventDefault();
  };

  statusFunction(cell, row) {
    
    if (row.required === 1) {        
        return 'Yes';
    } else {
        return 'No';
    }
    
  };

  modalShowHandler = (event, id) => { 
    if(id){
      event.preventDefault();
      this.setState({
        csccFlagId: id,
        showModal: true 
      }); 
    } else {
      this.setState({        
        csccDetails: [],
        csccFlagId: 0,
        showModal: true
      });
    }
  };

  modalCloseHandler = () => {
    this.setState({ csccFlagId: 0 });
    this.setState({ showModal: false });
  };

  modalCloseDisplayHandler = () => {
    this.setState({ csccFlagId: 0 });
    this.setState({ field_options:[],showDisplayPopup: false });
  };

  modalCloseDeleteHandler = () => {
    this.setState({ csccFlagId: 0 });
    this.setState({ delete_field_options:[],delete_id:0,showDeletePopup: false });
  };

  submitDeleteHandler = (e) => {
    e.preventDefault();

    const post_data = {
      options_arr: JSON.stringify(this.state.delete_field_options)
    };

    API.put(`/api/feed/delete_checklist/${this.state.delete_id}`,post_data)
    .then(res => { 

      this.modalCloseDeleteHandler();
      swal({
        closeOnClickOutside: false,
        title: "Success",
        text: "Record updated successfully.",
        icon: "success"
      }).then(() => {
        this.setState({ showModalLoader: false });
        this.getCheckList(this.state.activePage);
      });

    }).catch(err => {
      showErrorMessage(err,this.props);
    });
  }


  showDisplayModal = (e,id) => {
    e.preventDefault();
    API.get(`/api/feed/get_checklist/${id}`)
    .then(res => { 

      this.setState({ field_options:JSON.parse(res.data.data),showDisplayPopup: true });

    }).catch(err => {
      showErrorMessage(err,this.props);
    });

  };

  showDeleteModal = (e,id) => {
    e.preventDefault();
    API.get(`/api/feed/get_checklist/${id}`)
    .then(res => { 

      this.setState({ delete_field_options:JSON.parse(res.data.data),showDeletePopup: true,delete_err_msg:'',delete_id:id });

    }).catch(err => {
      showErrorMessage(err,this.props);
    });

  };

  

  handleSubmitEvent = (values, actions) => {
    const post_data = {
      title: values.title
    };

    if (this.state.csccFlagId) {
      this.setState({ showModalLoader: true });
      const id = this.state.csccFlagId;
      API.put(`/api/feed/update_checklist/${id}`, post_data)
        .then(res => {
          this.modalCloseHandler();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record updated successfully.",
            icon: "success"
          }).then(() => {
            this.setState({ showModalLoader: false });
            this.getCheckList(this.state.activePage);
          });
        })
        .catch(err => {
          this.setState({ showModalLoader: false });
          if (err.data.status === 3) {
            this.setState({
              showModal: false
            });
            showErrorMessage(err, this.props);
          } else {
            actions.setErrors(err.data.errors);
            actions.setSubmitting(false);
          }
        });
    }
  }; 
  
  confirmDelete = (event, id) => {
    event.preventDefault();
    swal({
      closeOnClickOutside: false,
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this!",
      icon: "warning",
      buttons: true,
      dangerMode: true
    }).then(willDelete => {
      if (willDelete) {
        this.deleteCheckList(id);
      }
    });
  };

  deleteCheckList = id => {
    if (id) {
      API.delete(`/api/feed/delete_checklist/${id}`).then(res => {
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: "Record deleted successfully.",
          icon: "success"
        }).then(() => {
          this.setState({ activePage: 1 });
          this.getCheckList(this.state.activePage);
        });
      }).catch(err => {
        if (err.data.status === 3) {
          this.setState({ closeModal: true });
          showErrorMessage(err, this.props);
        }
      });
    }
  };

  updateFieldOptions = (e,index) => {
    e.preventDefault();
    if(this.state.delete_field_options.length === 1){
      this.setState({delete_err_msg:'Action cannot be performed as this is the last option.'});
    }else{
      let new_arr = []; 
      for (let j = 0; j < this.state.delete_field_options.length; j++) {
        let element = this.state.delete_field_options[j];
        if(index == j){

        }else{
          new_arr.push(element);
        }
      }
      this.setState({delete_field_options:new_arr});
    }
  }

  render() {

    const { csccDetails } = this.state;

    const newInitialValues = Object.assign(initialValues, {
      title: ""
    });
    const validateStopFlag = Yup.object().shape({
      title: Yup.string()
        .min(2, 'Option name must be at least 2 characters')
        .max(50, 'Option name must be at most 50 characters')  
        .required("Please enter option")
    });

    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
            <div className="loderOuter">
            <div className="loading_reddy_outer">
                <div className="loading_reddy" >
                    <img src={whitelogo} alt="logo" />
                </div>
                </div>
            </div>
        </>        
      );
    } else {
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <h1>
                    CSC Commercial Checklist
                    <small />
                  </h1>
                  
                </div>
                {/* <div className="col-lg-6 col-sm-6 col-xs-12 topSearchSection">
                {this.state.access.add === true ? 
                <div className="">
                  <button
                      type="button"
                      className="btn btn-info btn-sm"
                     onClick={e => this.modalShowHandler(e, "")}
                    >
                      <i className="fas fa-plus m-r-5" /> Add Checklist
                    </button>
                  </div>
                  : null}
                  <div className="clearfix"></div>
                </div> */}

                {/*  */}
                <div className="col-lg-12 col-sm-12 col-xs-12 topSearchSection">
                  <form className="form">
                          <div className="">
                          <input
                              className="form-control"
                              name="f_title"
                              id="f_title"
                              placeholder="Title"
                            />
                          </div>
                          <div className="">
                            <select name="type" id="f_type" className="form-control">
                              <option value="">Type</option>
                              <option value="dropdown">Dropdown</option>
                              <option value="text">Text</option>
                              <option value="textarea">Textarea</option>
                              <option value="date">Date</option>
                            </select>
                          </div>
                          <div className="">
                            <select name="required" id="f_required" className="form-control">
                              <option value="">Required</option>
                              <option value="1">Yes</option>
                              <option value="0">No</option>
                            </select>
                          </div>

                          <div className="">
                          <input
                            type="submit"
                            value="Search"
                            className="btn btn-warning btn-sm"
                            onClick={(e) => this.checkListSearch( e )}
                          />
                          {this.state.remove_search ? <a onClick={() => this.clearSearch()} className="btn btn-danger btn-sm"> Remove </a> : null}
                          </div>
                          <div className="clearfix"></div>
                    </form>
                  </div>
                  {/*  */}

              </div>
            </section>
            {/* <DashboardSearch groupList={this.state.groupList} /> */}
            <section className="content">
              <div className="box">

                <div className="box-body">

                {/* <div className="nav-tabs-custom">
                  <ul className="nav nav-tabs">
                    <li className="tabButtonSec pull-right">
                      {this.state.count_checklist > 0 ? 
                        <span onClick={(e) => this.downloadXLSX(e)} >
                            <LinkWithTooltip
                                tooltip={`Click here to download excel`}
                                href="#"
                                id="tooltip-my"
                                clicked={e => this.checkHandler(e)}
                            >
                                <i className="fas fa-download"></i>
                            </LinkWithTooltip>
                        </span>
                      : null }
                    </li>
                  </ul>
                </div> */}

                  <BootstrapTable
                    data={this.state.checklist}
                  >
                    <TableHeaderColumn isKey dataField="field_title"  dataFormat={__htmlDecode(this)} width={'40%'} >
                     Title
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="field_type"  dataFormat={__htmlDecode(this)}>
                     Type
                    </TableHeaderColumn>

                    <TableHeaderColumn dataField="required" dataFormat={this.statusFunction}>
                      Required
                    </TableHeaderColumn>
                    {this.state.access.edit === true || this.state.access.delete === true ?
                    <TableHeaderColumn
                      dataField="id"
                      dataFormat={actionFormatter(this)}
                    >
                      Action
                    </TableHeaderColumn>
                    : null}
                   
                  </BootstrapTable>
                  {this.state.count_checklist > this.state.itemPerPage ? (
                    <Row>
                      <Col md={12}>
                        <div className="paginationOuter text-right">
                          <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemPerPage}
                            totalItemsCount={this.state.count_checklist}
                            itemClass='nav-item'
                            linkClass='nav-link'
                            activeClass='active'
                            onChange={this.handlePageChange}
                          />
                        </div>
                      </Col>
                    </Row>
                  ) : null}

                  {/* ======= Add/Edit ======== */}
                  <Modal
                      show={this.state.showModal}
                      onHide={() => this.modalCloseHandler()} backdrop="static"
                    >
                      <Formik
                        initialValues={newInitialValues}
                        validationSchema={validateStopFlag}
                        onSubmit={this.handleSubmitEvent}
                      >
                        {({ values, errors, touched, isValid, isSubmitting }) => {
                          return (
                            <Form>
                              {this.state.showModalLoader === true ? (
                                <div className="loading_reddy_outer">
                                  <div className="loading_reddy" >
                                    <img src={whitelogo} alt="loader" />
                                  </div>
                                </div>
                              ) : ("")}
                              <Modal.Header closeButton>
                                <Modal.Title>
                                  {`Add Options`}
                                </Modal.Title>
                              </Modal.Header>
                              <Modal.Body>
                                <div className="contBox listSection">                              
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>
                                      Option<span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="title"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter option"
                                        autoComplete="off"
                                        value={values.title}
                                      />
                                      {errors.title && touched.title ? (
                                        <span className="errorMsg">
                                          {errors.title}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                </div>
                              </Modal.Body>
                              <Modal.Footer>
                                  <button
                                    className={`btn btn-success btn-sm ${
                                      isValid ? "btn-custom-green" : "btn-disable"
                                      } mr-2`}
                                    type="submit"
                                    disabled={isValid ? false : false}
                                  >
                                    {this.state.groupFlagId > 0
                                      ? isSubmitting
                                        ? "Updating..."
                                        : "Update"
                                      : isSubmitting
                                        ? "Submitting..."
                                        : "Submit"}
                                  </button>
                                  <button
                                    onClick={e => this.modalCloseHandler()}
                                    className={`btn btn-danger btn-sm`}
                                    type="button"
                                  >
                                    Close
                                  </button>
                              </Modal.Footer>
                            </Form>
                          );
                        }}
                      </Formik>
                    </Modal>

                    {this.state.field_options && <Modal
                      show={this.state.showDisplayPopup}
                      onHide={() => this.modalCloseDisplayHandler()} backdrop="static"
                    >
                      
                      <Modal.Header closeButton>
                        <Modal.Title>
                          {`Options List`}
                        </Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        <div className="contBox listSection">     

                        {this.state.field_options.map((val,index)=>{
                          return(
                            <Row>
                              <Col xs={12} sm={12} md={12}>
                                {htmlDecode(val.value)}
                              </Col>
                            </Row>
                          );  
                        })}
                        </div>
                      </Modal.Body>
                      <Modal.Footer>
                          <button
                            onClick={e => this.modalCloseDisplayHandler()}
                            className={`btn btn-danger btn-sm`}
                            type="button"
                          >
                            Close
                          </button>
                      </Modal.Footer>
                            
                    </Modal>}

                    {this.state.delete_field_options && <Modal
                      show={this.state.showDeletePopup}
                      onHide={() => this.modalCloseDeleteHandler()} backdrop="static"
                    >
                      
                      <Modal.Header closeButton>
                        <Modal.Title>
                          {`Options List`}
                        </Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        <div className="contBox listSection">     

                        {this.state.delete_field_options.map((val,index)=>{
                          return(
                            <Row>
                              <Col xs={12} sm={12} md={12}>
                                <LinkWithTooltip
                                            tooltip="Click to Delete"
                                            href="#"
                                            clicked={e => this.updateFieldOptions(e, index)}
                                            id="tooltip-1"
                                          >
                                            <i className="far fa-trash-alt" />
                                          </LinkWithTooltip>
                                        &nbsp;  {htmlDecode(val.value)} 
                              </Col>
                            </Row>
                          );  
                        })}

                        {this.state.delete_err_msg && this.state.delete_err_msg != '' && <span class="errorMsg">{this.state.delete_err_msg}</span>}

                        </div>
                      </Modal.Body>
                      <Modal.Footer>
                          <button
                            onClick={e => this.submitDeleteHandler(e)}
                            className={`btn btn-info btn-sm`}
                            type="button"
                          >
                            Update Options
                          </button>
                      </Modal.Footer>
                            
                    </Modal>}

                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}

export default Cscc;