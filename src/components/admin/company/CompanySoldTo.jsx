import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  Tooltip,
  OverlayTrigger,
  Modal
} from "react-bootstrap";
import { Link } from "react-router-dom";
import API from "../../../shared/admin-axios";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import Select from "react-select";
import Layout from "../layout/Layout";
//import DashboardSearch from "./DashboardSearch";
import whitelogo from '../../../assets/images/drreddylogo_white.png';
import {showErrorMessage} from "../../../shared/handle_error";
import Pagination from "react-js-pagination";
import { htmlDecode } from '../../../shared/helper';
import { getSuperAdmin, getAdminGroup } from '../../../shared/helper';

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const actionFormatter = refObj => (cell, row) => {
  return (
    <div className="actionStyle">
      {refObj.state.access.edit === true ?
      <LinkWithTooltip
        tooltip="Click to Edit"
        href="#"
        clicked={e => refObj.modalEditShowHandler(e, cell)}
        id="tooltip-1"
      >
        <i className="far fa-edit" />
      </LinkWithTooltip>
      : null }

      {refObj.state.access.delete === true ?
        <LinkWithTooltip
          tooltip="Click to Delete"
          href="#"
          clicked={e => refObj.confirmDelete(e, cell)}
          id="tooltip-1"
        >
          <i className="far fa-trash-alt" />        
          </LinkWithTooltip>
        : null}   

    </div>
  );
};

const addInitialValues = {
  soldto_id: "",
  country: "",
  city: "",
  post_code: "",
  street: ''
}

const updateInitialValues = {
  soldto_id: "",
  country: "",
  city: "",
  post_code: "",
  street: ''
}

const __htmlDecode = refObj => cell => {
  return htmlDecode(cell);
}
const custStatus = () => cell => {
  return cell === 1 ? "Active" : "Inactive";
};

class CompanySoldTo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      company_id: this.props.match.params.id,
      isLoading: true,
      soldto:[],
      company_name: '',
      count_soldto : 0,
      activePage      : 1,
      totalCount      : 0,
      itemPerPage     : 20,
      showModalLoader : false,
      search_name: '',
      remove_search: false,
      child_details : false,
      master_list : [],
      selectedParent: "",
      selectedOther : "",      
      get_access_data:false,
      selectStatus: [
        { id: "0", name: "Inactive" },
        { id: "1", name: "Active" }
      ],
    }
  }

  componentDidMount() {
    
    const superAdmin  = getSuperAdmin(localStorage.admin_token);
    
    if(superAdmin === 1){
      this.setState({
        access: {
           view : true,
           add : true,
           edit : true,
           delete: true           
         },
         get_access_data:true         
     });
     this.getCompanyDetails();
     this.getSoldToList();
    }
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getSoldToList(pageNumber > 0 ? pageNumber  : 1);
  };

  getCompanyDetails(){
    let company_id = this.state.company_id;

    API.get(`/api/company/child_details/${company_id}`)
    .then(res => {

          this.setState({
              company_name : res.data.data.child_name
          })
    })
    .catch(err => {
      if(err.data.status === 3){
        this.setState({ closeModal: true });
        showErrorMessage(err,this.props);
      }
    });
  }

  getSoldToList(page = 1) {
    let soldto_name = this.state.search_name;
    let company_id = this.state.company_id;

    API.get(`/api/company/soldto_list/${company_id}?page=${page}&soldto_name=${encodeURIComponent(soldto_name)}`)
      .then(res => {
        this.setState({
          soldto: res.data.data,
          count_soldto: res.data.count,
          isLoading: false,
          search_name: soldto_name
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        showErrorMessage(err,this.props);
      });
  }  

  closeEditModal = () => {
    this.setState({  });
    this.setState({ sold_id: 0,showEditModal: false });
  };

  closeCustomerModal = () => {
    this.setState({ sold_id: 0,showCustomerModal: false, addCustomerModal: false });
  }

  modalEditShowHandler = (event, id) => {
    event.preventDefault();
    this.setState({ sold_id: 0 });
    this.getSoldTo(id);
    this.setState({ sold_id: id, showEditModal: true });
  };

  getSoldTo = id => {
    API.get(`/api/company/soldto/${id}`)
    .then(res => {

          this.setState({
              child_details : res.data.data,
          }, () => {
            updateInitialValues.soldto_id = res.data.data.soldto_id ? htmlDecode(res.data.data.soldto_id) : "";
            updateInitialValues.country = res.data.data.country ? res.data.data.country : "";
            updateInitialValues.city = res.data.data.city ? res.data.data.city : "";
            updateInitialValues.post_code = res.data.data.post_code ? res.data.data.post_code : "";
            updateInitialValues.street = res.data.data.street ? res.data.data.street : "";
            updateInitialValues.status  = res.data.data.status ? res.data.data.status.toString() : "0";
          })
    })
    .catch(err => {
      if(err.data.status === 3){
        this.setState({ closeModal: true });
        showErrorMessage(err,this.props);
      }
    });
  }

  soldtoSearch = (e) => {
    e.preventDefault();

    let soldto_name = document.getElementById('soldto_name').value
    let company_id = this.state.company_id;

    if(soldto_name === ""){
      return false;
    }

    API.get(`/api/company/soldto_list/${company_id}?page=1&soldto_name=${encodeURIComponent(soldto_name)}`)
    .then(res => {
      this.setState({
        soldto: res.data.data,
        count_soldto : res.data.count,
        isLoading: false,
        search_name: soldto_name,
        remove_search: true
      });
    })
    .catch(err => {
      this.setState({
        isLoading: false
      });
      showErrorMessage(err,this.props);
    });
  }

  clearSearch = () => {
    document.getElementById('soldto_name').value     = "";

    this.setState({
      search_name: "",
      remove_search: false
    }, () => {
      this.getSoldToList();
      this.setState({activePage: 1})
    })
  }


  handleUpdateSoldTo = ( values,action ) => {
    this.setState({ showModalLoader : true })
    let postData = {
      soldto_id : values.soldto_id,
      country : values.country,
      city : values.city,
      post_code : values.post_code,
      street: values.street,
      status: values.status
    }

    API.put(`/api/company/soldto/${this.state.sold_id}`, postData)
    .then(res => {
      this.setState({ sold_id: 0,showEditModal: false, showModalLoader: false });
      swal({
        closeOnClickOutside: false,
        title:"Success",
        text:"SoldTo has been updated successfully",
        icon: "success"
      }).then(() => {
        this.getSoldToList();
      });
    })
    .catch(err => {
      this.setState({ closeModal: true, showModalLoader: false });
      if(err.data.status === 3){
        showErrorMessage(err,this.props);
      }else{
        action.setErrors(err.data.errors)
      }
    });
  }
 


  handleAddSoldTo = (values, action) => {
    this.setState({ showModalLoader : true })
    let postData = {
      soldto_id  : values.soldto_id,
      country : values.country,
      company_id : this.state.company_id,
      city : values.city,
      street: values.street,
      post_code : values.post_code
    }

    API.post(`/api/company/soldto/`, postData)
    .then(res => {
      this.setState({ sold_id: 0, addCustomerModal: false, showModalLoader : false });
      swal({
        closeOnClickOutside: false,
        title:"Success",
        text: "SoldTo Id has been added successfully",
        icon: "success"
      }).then(() => {
        this.getSoldToList();
      });
    })
    .catch(err => {
      this.setState({ closeModal: true, showModalLoader : false });
      if(err.data.status === 3){
        showErrorMessage(err,this.props);
      }else{
        action.setErrors(err.data.errors)
      }
    });
  }


  openAddSoldTo = () => {    
    this.setState({ addCustomerModal : true});
  }

  // downloadXLSX = (e) => {
  //   e.preventDefault();

  //   var company_name = document.getElementById('company_name').value;    

  //   API.get(`/api/company/child_list/download?page=1&company_name=${encodeURIComponent(company_name)}`,{responseType: 'blob'})
  //   .then(res => { 
  //     let url    = window.URL.createObjectURL(res.data);
  //     let a      = document.createElement('a');
  //     a.href     = url;
  //     a.download = 'customers.xlsx';
  //     a.click();

  //   }).catch(err => {
  //     showErrorMessage(err,this.props);
  //   });
  // }

  checkHandler = (event) => {
    event.preventDefault();
  };

  confirmDelete = (event, id) => {
    event.preventDefault();
    swal({
      closeOnClickOutside: false,
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.deleteShipTo(id);
      }
    });
  };

  deleteShipTo = (id) => {
    if (id) {
      API.delete(`/api/company/soldto/${id}`)
        .then((res) => {
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record deleted successfully.",
            icon: "success",
          }).then(() => {
            this.setState({ activePage: 1 });
            this.getSoldToList(this.state.activePage);
          });
        })
        .catch((err) => {
          if (err.data.status === 3) {
            this.setState({ closeModal: true });
            showErrorMessage(err, this.props);
          }
        });
    }
  };
  
  render() { 

    let validateSoldTo = Yup.object().shape({
      soldto_id: Yup.string().required('Please enter SoldTo Id')
    });


    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
            <div className="loderOuter">
            <div className="loading_reddy_outer">
                <div className="loading_reddy" >
                    <img src={whitelogo} alt="logo" />
                </div>
                </div>
            </div>
        </>        
      );
    } else {
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">

                  <div className="col-lg-12 col-sm-12 col-xs-12">
                    <h1>
                        Manage SoldTo of "{this.state.company_name}"
                      <small />
                    </h1>
                    <input
                        type="button"
                        value="Go Back"
                        className="btn btn-warning btn-sm"
                        onClick={() => {
                          window.history.go(-1);
                          return false;
                        }}
                        style={{ right: "18px", position: "absolute", top: "13px" }}
                      />
                  </div>

                  <div className="col-lg-12 col-sm-12 col-xs-12 topSearchSection">
                  {this.state.access.add === true ?
                    <div className="">
                    <button
                        type="button"
                        className="btn btn-info btn-sm"
                        onClick={() => this.openAddSoldTo()}
                      >
                        <i className="fas fa-plus m-r-5" /> Add SoldTo
                      </button>
                    </div>
                  : null }
                      {/*  */}
                      <form className="form">
                            <div className="">
                            <input
                                className="form-control"
                                name="soldto_name"
                                id="soldto_name"
                                placeholder="SoldTo ID"
                              />
                            </div>

                            <div className="">
                            <input
                              type="submit"
                              value="Search"
                              className="btn btn-warning btn-sm"
                              onClick={(e) => this.soldtoSearch( e )}
                            />
                            {this.state.remove_search ? <a onClick={() => this.clearSearch()} className="btn btn-danger btn-sm"> Remove </a> : null}
                            </div>
                      </form>                      
                      {/*  */}

                                      </div>
 
              </div>
            </section>
            {/* <DashboardSearch groupList={this.state.groupList} /> */}
            <section className="content">
              <div className="box">

                <div className="box-body">





                  {/* <div className="nav-tabs-custom">
                    <ul className="nav nav-tabs">
                      <li className="tabButtonSec pull-right">
                      {this.state.count_soldto > 0 ? 
                    <span onClick={(e) => this.downloadXLSX(e)} >
                        <LinkWithTooltip
                            tooltip={`Click here to download excel`}
                            href="#"
                            id="tooltip-my"
                            clicked={e => this.checkHandler(e)}
                        >
                            <i className="fas fa-download"></i>
                        </LinkWithTooltip>
                    </span>
                  : null }
                      </li>
                    </ul>
                  </div> */}






                  
                  <BootstrapTable
                    data={this.state.soldto}
                  >
                    <TableHeaderColumn isKey dataField="soldto_id"  dataFormat={__htmlDecode(this)}>
                      SoldTo Id
                    </TableHeaderColumn>

                    <TableHeaderColumn dataField="country">
                      Country
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="city">
                      City
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="post_code">
                      Post Code
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="status" dataFormat={custStatus(this)}>
                      Status
                    </TableHeaderColumn>
                    {this.state.access.edit === true || this.state.access.delete === true ?
                    <TableHeaderColumn
                      dataField="soldtoid"
                      dataFormat={actionFormatter(this)}
                      dataAlign=""
                    >
                      Action
                    </TableHeaderColumn>
                    : null}
                  </BootstrapTable>
                  {this.state.count_soldto > this.state.itemPerPage ? (
                    <Row>
                      <Col md={12}>
                        <div className="paginationOuter text-right">
                          <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemPerPage}
                            totalItemsCount={this.state.count_soldto}
                            itemClass='nav-item'
                            linkClass='nav-link'
                            activeClass='active'
                            onChange={this.handlePageChange}
                          />
                        </div>
                      </Col>
                    </Row>
                  ) : null}

                  {/* ======= Add/Edit Admin ======== */}

                  <Modal
                    show={this.state.showEditModal}
                    onHide={() => this.closeEditModal()} backdrop="static"
                  >
                    <Formik
                      initialValues={updateInitialValues}
                      validationSchema={validateSoldTo}
                      onSubmit={this.handleUpdateSoldTo}
                    >
                      {({ values, errors, touched, isValid, isSubmitting, setFieldValue}) => {
                        console.log('values', values)
                        return (
                          <Form>
                            {this.state.showModalLoader === true ? ( 
                                <div className="loading_reddy_outer">
                                    <div className="loading_reddy" >
                                        <img src={whitelogo} alt="loader"/>
                                    </div>
                                </div>
                              ) : ( "" )}
                            <Modal.Header closeButton>
                              <Modal.Title>
                                Update SoldTo Details
                              </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="contBox">                              
                                
                                <Row>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        SoldTo Id<span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="soldto_id"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter SoldTo Id"
                                        autoComplete="off"
                                        value={values.soldto_id}
                                      />
                                      {errors.soldto_id && touched.soldto_id ? (
                                        <span className="errorMsg">
                                          {errors.soldto_id}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>                                
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>Country</label>
                                      <Field
                                        name="country"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter Country"
                                        autoComplete="off"
                                        value={values.country}
                                      />
                                      {errors.country ? (
                                        <span className="errorMsg">
                                          {errors.country}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                <Row>                                  
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>City</label>
                                      <Field
                                        name="city"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter City"
                                        autoComplete="off"
                                        value={values.city}
                                      />
                                      {errors.city ? (
                                        <span className="errorMsg">
                                          {errors.city}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>Post Code</label>
                                      <Field
                                        name="post_code"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter Post Code"
                                        autoComplete="off"
                                        value={values.post_code}
                                      />
                                      {errors.post_code ? (
                                        <span className="errorMsg">
                                          {errors.post_code}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>Street</label>
                                      <Field
                                        name="street"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter Street"
                                        autoComplete="off"
                                        value={values.street}
                                      />
                                      {errors.street ? (
                                        <span className="errorMsg">
                                          {errors.street}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                    <Col xs={12} sm={12} md={12}>
                                      <div className="form-group">
                                        <label>
                                          Status
                                          <span className="impField">*</span>
                                        </label>
                                        <Field
                                          name="status"
                                          component="select"
                                          className={`selectArowGray form-control`}
                                          autoComplete="off"
                                          value={values.status}
                                        >
                                          <option key="-1" value="">
                                            Select
                                          </option>
                                          {this.state.selectStatus.map(
                                            (status, i) => (
                                              <option key={i} value={status.id}>
                                                {status.name}
                                              </option>
                                            )
                                          )}
                                        </Field>
                                        {errors.status && touched.status ? (
                                          <span className="errorMsg">
                                            {errors.status}
                                          </span>
                                        ) : null}
                                      </div>
                                    </Col>
                                  </Row>  
                                {errors.message ? (
                                    <Row>
                                      <Col xs={12} sm={12} md={12}>
                                        <span className="errorMsg">
                                          {errors.message}
                                        </span>
                                      </Col>
                                    </Row>
                                  ) : ( "" )
                                }                               
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <button
                                  className={`btn btn-success btn-sm ${isValid ? "btn-custom-green" : "btn-disable"} m-r-10`}
                                  type="submit"
                                  disabled={isValid ? false : true}
                                >
                                  Update
                                </button>
                                <button
                                  onClick={e => this.closeEditModal()}
                                  className={`btn btn-danger btn-sm`}
                                  type="button"
                                >
                                  Close
                                </button>
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>


                  <Modal
                    show={this.state.addCustomerModal}
                    onHide={() => this.closeCustomerModal()} backdrop="static"
                  >
                    <Formik
                      initialValues={addInitialValues}
                      validationSchema={validateSoldTo}
                      onSubmit={this.handleAddSoldTo}
                    >
                      {({ values, errors, touched, isValid, isSubmitting, setFieldValue,setFieldTouched}) => {
                        
                        return (
                          <Form>
                            {this.state.showModalLoader === true ? ( 
                                <div className="loading_reddy_outer">
                                    <div className="loading_reddy" >
                                        <img src={whitelogo} alt="loader"/>
                                    </div>
                                </div>
                              ) : ( "" )}
                            <Modal.Header closeButton>
                              <Modal.Title>
                                Add SoldTo Details
                              </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                            <div className="contBox">
                                                            
                              <Row>
                                <Col xs={12} sm={6} md={6}>
                                  <div className="form-group">
                                    <label>
                                      SoldTo Id<span className="impField">*</span>
                                    </label>
                                    <Field
                                      name="soldto_id"
                                      type="text"
                                      className={`form-control`}
                                      placeholder="Enter SoldTo Id"
                                      autoComplete="off"
                                    />
                                    {errors.soldto_id && touched.soldto_id ? (
                                      <span className="errorMsg">
                                        {errors.soldto_id}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>                             
                                <Col xs={12} sm={6} md={6}>
                                  <div className="form-group">
                                    <label>Country</label>
                                    <Field
                                      name="country"
                                      type="text"
                                      className={`form-control`}
                                      placeholder="Enter Country"
                                      autoComplete="off"
                                    />
                                    {errors.country && touched.country ? (
                                      <span className="errorMsg">
                                        {errors.country}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              </Row>
                              <Row>                                  
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>City</label>
                                      <Field
                                        name="city"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter City"
                                        autoComplete="off"
                                        value={values.city}
                                      />
                                      {errors.city ? (
                                        <span className="errorMsg">
                                          {errors.city}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>Post Code</label>
                                      <Field
                                        name="post_code"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter Post Code"
                                        autoComplete="off"
                                        value={values.post_code}
                                      />
                                      {errors.post_code ? (
                                        <span className="errorMsg">
                                          {errors.post_code}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>Street</label>
                                      <Field
                                        name="street"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter Street"
                                        autoComplete="off"
                                        value={values.street}
                                      />
                                      {errors.street ? (
                                        <span className="errorMsg">
                                          {errors.street}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row> 
                              {errors.message ? (
                                  <Row>
                                    <Col xs={12} sm={12} md={12}>
                                      <span className="errorMsg">
                                        {errors.message}
                                      </span>
                                    </Col>
                                  </Row>
                                ) : ( "" )
                              }                               
                            </div>
                            </Modal.Body>
                            <Modal.Footer>
                              
                              <button
                                  className={`btn btn-success btn-sm ${isValid ? "btn-custom-green" : "btn-disable"} m-r-10`}
                                  type="submit"
                                  disabled={isValid ? false : true}
                                >
                                  Add
                                </button>
                                <button
                                  onClick={e => this.closeCustomerModal()}
                                  className={`btn btn-danger btn-sm`}
                                  type="button"
                                >
                                  Close
                                </button>
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>

                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}

export default CompanySoldTo;
