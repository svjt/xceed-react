import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal
} from "react-bootstrap";
import { Link } from "react-router-dom";
import API from "../../../shared/admin-axios";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import Layout from "../layout/Layout";
import whitelogo from '../../../assets/images/drreddylogo_white.png';
import {showErrorMessage} from "../../../shared/handle_error";
import Pagination from "react-js-pagination";
import { htmlDecode } from '../../../shared/helper';
import Select from "react-select";
import { getSuperAdmin, getAdminGroup } from '../../../shared/helper';


function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

const actionFormatter = refObj => cell => {
  return (
    <div className="actionStyle">
      {refObj.state.access.edit === true ?
      <LinkWithTooltip
        tooltip="Click to Edit"
        href="#"
        clicked={e => refObj.openEditCompany(e, cell)}
        id="tooltip-1"
      >
        <i className="far fa-edit" />
      </LinkWithTooltip>
      : null }
    {refObj.state.access.delete === true ?
      <LinkWithTooltip
        tooltip="Click to Delete"
        href="#"
        clicked={e => refObj.openDeleteCompany(e, cell)}
        id="tooltip-1"
      >
        <i className="far fa-trash-alt" />
      </LinkWithTooltip>
      : null }
    </div>
  );
};

const initialValues = {
    company_name:""
};

const __htmlDecode = refObj => cell => {
  return htmlDecode(cell);
}

class Company extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      master_company_list: [],
      total_master_company: 0,

      activePage      : 1,
      totalCount      : 0,
      itemPerPage     : 20,
      showModalLoader : false,
     
      addCompanyModalOpen : false,
      updateCompanyModalOpen: false,
      deleteCompanyModalOpen: false,
      childCompanyModalOpen : false,
      company_name: "",
      childCompanyList : [],
      selectedChildCompanies: [],
      get_access_data:false
    }
  }

  componentDidMount() {
    //this.getMasterCompanyList();
    const superAdmin  = getSuperAdmin(localStorage.admin_token);
    
    if(superAdmin === 1){
      this.setState({
        access: {
           view : true,
           add : true,
           edit : true,
           delete : true
         },
         get_access_data:true
     });
     this.getMasterCompanyList();
    }else{
      const adminGroup  = getAdminGroup(localStorage.admin_token);
      API.get(`/api/adm_group/single_access/${adminGroup}/${'PARENT_CUSTOMER_MANAGEMENT'}`)
      .then(res => {
        this.setState({
          access: res.data.data,
          get_access_data:true
        }); 

        if(res.data.data.view === true){
          this.getMasterCompanyList();
        }else{
          this.props.history.push('/admin/dashboard');
        }
        
      })
      .catch(err => {
        showErrorMessage(err,this.props);
      });
    }
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getMasterCompanyList(pageNumber > 0 ? pageNumber  : 1);
  };

  getMasterCompanyList(page = 1) {
    API.get(`/api/company/master_list?page=${page}`)
      .then(res => {
        this.setState({
          master_company_list: res.data.data,
          total_master_company: res.data.count_master_company,
          isLoading: false
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        showErrorMessage(err,this.props);
      });
  }

  getCompanyDetails(id){
    API.get(`/api/company/parent_details/${id}`)
      .then(res => {
        API.get(`/api/company/get_all_child`)
        .then( result => {
          var temp_child = [];
          result.data.data.map((ch, k ) => {
            temp_child.push({ value: ch.company_id, label : htmlDecode(ch.company_name)})
          })
          this.setState({childCompanyList: temp_child});

          var childList = [];
          res.data.data.child.map((values) => {
            childList.push({ value : values.company_id, label : htmlDecode(values.company_name) })
          });

          this.setState({
            company_id : res.data.data.id,
            company_name : htmlDecode(res.data.data.name),
            selectedChildCompanies: childList,
            updateCompanyModalOpen: true
          }, () => {
            initialValues.company_name = res.data.data.name;
          });
        })
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        showErrorMessage(err,this.props);
      });
  }
  
  getChildOfParent = (id) => {
    API.get(`/api/company/child/${id}`)
      .then(res => {
        this.setState({
          child_company : res.data.data
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        showErrorMessage(err,this.props);
      });
  }
  
  closeModal = () => {
    this.setState({
      company_list:[],
      company_id: 0,
      addCompanyModalOpen: false,
      deleteCompanyModalOpen: false, 
      updateCompanyModalOpen: false,
      childCompanyModalOpen: false 
    });
  }

  openDeleteCompany = (event, id) => {
    event.preventDefault();
    this.setState({ company_id: id, deleteCompanyModalOpen: true });
  };

  openEditCompany = (event, id) => {
    event.preventDefault();
    this.getCompanyDetails(id);
  };

  getCompany = id => {
    API.delete(`/api/feed/other_company/${id}`).then(res => {
      this.setState({company_list:res.data.data});
    })
    .catch(err => {          
      if(err.data.status === 3){
        this.setState({ closeModal: true });
        showErrorMessage(err,this.props);
      }
    });
  };

  openChildList = ( e, parent_company_id ) => {
    e.preventDefault();
    this.getChildOfParent( parent_company_id );
    this.setState({ childCompanyModalOpen : true })
  }

  linkChildCompany = (cell, row) => {

    if(row.count_company > 0){
      return (
        <Link to='#' onClick={(e) => this.openChildList(e, row.parent_company_id)}>
          {cell}
        </Link>
      )
    }else{
      return (row.count_company) 
    }        
  }

  openAddCompany = () => {
    API.get(`/api/company/get_all_child`)
    .then( result => {
      var temp_child = [];
      result.data.data.map((ch, k ) => {
        temp_child.push({ value: ch.company_id, label : htmlDecode(ch.company_name)})
      })
      this.setState({childCompanyList: temp_child});

      this.setState({
        company_id : 0,
        company_name : "",
        selectedChildCompanies: [],
        addCompanyModalOpen: true
      }, () => {
        initialValues.company_name = "";
        initialValues.child_companies = [];
      });
    })
  }

  handleUpdateCompany = ( values, action ) => {
    
    let child_arr = [];
    values.child_companies.length > 0 && values.child_companies.map((c, r) => {
      child_arr.push({ company_id : c});
    })

    let postData = {
      company_name  : values.company_name,
      child_company : child_arr
    }
    API.put(`/api/company/parent/${this.state.company_id}`, postData)
    .then(res => {
        this.setState({
          updateCompanyModalOpen: false
        })
        swal({
          closeOnClickOutside: false,
          title:"Success",
          text:"Customer has been updated successfully",
          icon: "success"
        }).then(() => {
          this.getMasterCompanyList();
        });
    })
    .catch(err => {          
      if(err.data.status === 3){
        this.setState({ closeModal: true });
        showErrorMessage(err,this.props);
      } else {
        action.setErrors(err.data.errors);
      }
    });
  }

  handleAddCompany = ( values, action ) => {
    let child_arr = [];
    values.child_companies.length > 0 && values.child_companies.map((c, r) => {
      child_arr.push({ company_id : c});
    })

    let postData = {
      company_name : values.company_name,
      child_company : child_arr
    }
    API.post(`/api/company/parent`, postData)
    .then(res => {
      this.setState({
        addCompanyModalOpen: false
      })
      swal({
        closeOnClickOutside: false,
        title:"Success",
        text:"Customer has been added successfully",
        icon: "success"
      }).then(() => {        
        this.getMasterCompanyList();
      });
    })
    .catch(err => {          
      if(err.data.status === 3){
        this.setState({ closeModal: true });
        showErrorMessage(err,this.props);
      }else{
        action.setErrors(err.data.errors)
      }
    });
  }

  handleDeleteCompany = (values, action) => {    
    if( this.state.company_id > 0){
      API.delete(`/api/company/parent/${this.state.company_id}`)
      .then(res => {
        this.setState({
          deleteCompanyModalOpen: false
        })
        swal({
          closeOnClickOutside: false,
          title:"Success",
          text:"Customer has been deleted successfully",
          icon: "success"
        }).then(() => {        
          this.getMasterCompanyList();
        });
      })
      .catch(err => {
        this.setState({
          deleteCompanyModalOpen: false
        })          
        if(err.data.status === 3){
          this.setState({ closeModal: true });
          showErrorMessage(err,this.props);
        }else{
          action.setErrors(err.data.errors)
        }
      });
    }
  }

  downloadXLSX = (e) => {
    e.preventDefault();

    API.get(`/api/company/master_list/download`,{responseType: 'blob'})
    .then(res => { 
      let url    = window.URL.createObjectURL(res.data);
      let a      = document.createElement('a');
      a.href     = url;
      a.download = 'parent_customers.xlsx';
      a.click();

    }).catch(err => {
      showErrorMessage(err,this.props);
    });
  }

  checkHandler = (event) => {
    event.preventDefault();
  };

  render() {
    
    let validateCompany = Yup.object().shape({
      company_name: Yup.string().trim()
        .required("Please enter company name")
        .min(2, "Customer name can be minimum 2 characters long")     
        .max(100, "Customer name can be maximum 100 characters long")
    });
    
    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
            <div className="loderOuter">
            <div className="loading_reddy_outer">
                <div className="loading_reddy" >
                    <img src={whitelogo} alt="logo" />
                </div>
                </div>
            </div>
        </>        
      );
    } else {
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">

                <div className="col-lg-9 col-sm-6 col-xs-12">
                  <h1>
                    Manage Parent Customers
                    <small />
                  </h1>
                  
                </div>
                {this.state.access.add === true ?
                <div className="col-lg-3 col-sm-6 col-xs-12">
                  <button
                    type="button"
                    className="btn btn-warning btn-sm btn-custom-green pull-right"
                    onClick={() => this.openAddCompany()}
                  >
                    <i className="fas fa-plus m-r-5" /> Add Parent Customer
                  </button>
                </div>
                : null }
                
              </div>
            </section>
            {/* <DashboardSearch groupList={this.state.groupList} /> */}
            <section className="content">
              <div className="box">

                <div className="box-body">

                <div className="nav-tabs-custom">
<ul className="nav nav-tabs">
<li className="tabButtonSec pull-right">
{this.state.total_master_company > 0 ? 
                    <span onClick={(e) => this.downloadXLSX(e)} >
                        <LinkWithTooltip
                            tooltip={`Click here to download excel`}
                            href="#"
                            id="tooltip-my"
                            clicked={e => this.checkHandler(e)}
                        >
                            <i className="fas fa-download"></i>
                        </LinkWithTooltip>
                    </span>
                  : null }
</li>
</ul>
</div>



                  <BootstrapTable
                    data={this.state.master_company_list}
                  >
                    <TableHeaderColumn isKey dataField="parent_company_name"  dataFormat={__htmlDecode(this)}>
                      Parent Customer Name
                    </TableHeaderColumn>

                    <TableHeaderColumn dataField="count_company" dataFormat={ this.linkChildCompany }>
                      Child Customers
                    </TableHeaderColumn>

                    {this.state.access.edit === true || this.state.access.delete === true ?
                    <TableHeaderColumn
                      dataField="parent_company_id"
                      dataFormat={actionFormatter(this)}
                      dataAlign=""
                    >
                      Action
                    </TableHeaderColumn>
                    : null}
                  </BootstrapTable>
                  {this.state.total_master_company > this.state.itemPerPage ? (
                    <Row>
                      <Col md={12}>
                        <div className="paginationOuter text-right">
                          <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemPerPage}
                            totalItemsCount={this.state.total_master_company}
                            itemClass='nav-item'
                            linkClass='nav-link'
                            activeClass='active'
                            onChange={this.handlePageChange}
                          />
                        </div>
                      </Col>
                    </Row>
                  ) : null}

                  {/* ======= Add/Edit Admin ======== */}

                  <Modal
                    show={this.state.updateCompanyModalOpen}
                    onHide={() => this.closeModal()} backdrop="static"
                  >
                    <Formik
                      initialValues={initialValues}
                      validationSchema={validateCompany}
                      onSubmit={this.handleUpdateCompany}
                    >
                      {({ values, errors, touched, isValid, isSubmitting, setErrors, setFieldValue, setFieldTouched}) => {
                        return (
                          <Form>
                            {this.state.showModalLoader === true ? ( 
                                <div className="loading_reddy_outer">
                                    <div className="loading_reddy" >
                                        <img src={whitelogo} alt="loader"/>
                                    </div>
                                </div>
                              ) : ( "" )}
                            <Modal.Header closeButton>
                              <Modal.Title>
                                Update Customer
                              </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="contBox">
                                <Row>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Customer Name<span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="company_name"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter customer name"
                                        autoComplete="off"
                                        value={values.company_name}
                                      />
                                      {errors.company_name && touched.company_name ? (
                                        <span className="errorMsg">
                                          {errors.company_name}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row> 
                                {errors.message ? (
                                    <Row>
                                      <Col xs={12} sm={12} md={12}>
                                        <span className="errorMsg">
                                          {errors.message}
                                        </span>
                                      </Col>
                                    </Row>
                                  ) : ( "" )
                                }  

                                <Row>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                    <label>Child Customer</label>

                                <Select
                                  isMulti
                                  name="child_companies[]"
                                  options={this.state.childCompanyList}
                                  className="basic-multi-select"
                                  classNamePrefix="select"
                                  onChange={evt => setFieldValue("child_companies",[].slice.call(evt).map(val => val.value))}
                                  placeholder="Enter child customer name"
                                  onBlur={() => setFieldTouched("child_companies")}
                                  defaultValue={this.state.selectedChildCompanies}
                                />
                                {errors.child_companies && touched.child_companies ? (
                                  <span className="errorMsg">
                                    {errors.child_companies}
                                  </span>
                                ) : null}

                                    </div>
                                  </Col>
                                </Row>                               
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                             
                                <button
                                  className={`btn btn-success btn-sm ${
                                    isValid ? "btn-custom-green" : "btn-disable"
                                  } m-r-10`}
                                  type="submit"
                                  disabled={isValid ? false : true}
                                >
                                  Update
                                </button>
                                <button
                                  onClick={e => this.closeModal()}
                                  className={`btn btn-danger btn-sm`}
                                  type="button"
                                >
                                  Close
                                </button>
                             
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>

                  <Modal
                    show={this.state.deleteCompanyModalOpen}
                    onHide={() => this.closeModal()} backdrop="static"
                  >
                    <Formik
                      initialValues={initialValues}
                      validationSchema={""}
                      onSubmit={this.handleDeleteCompany}
                    >
                      {({ values, errors, touched, isValid, isSubmitting, setErrors}) => {
                        console.log( "errors", errors );
                        return (
                          <Form>
                            {this.state.showModalLoader === true ? ( 
                                <div className="loading_reddy_outer">
                                    <div className="loading_reddy" >
                                        <img src={whitelogo} alt="loader"/>
                                    </div>
                                </div>
                              ) : ( "" )}
                            <Modal.Header closeButton>
                              <Modal.Title>
                                Delete Customer
                              </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="contBox">
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>
                                        Do you really want to delete this customer?
                                      </label>
                                    </div>
                                  </Col>
                                </Row>                             
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <button
                                  className={`btn btn-success btn-sm ${
                                    isValid ? "btn-custom-green" : "btn-disable"
                                  } m-r-10`}
                                  type="submit"
                                >
                                  Yes
                                </button>
                                <button
                                  onClick={e => this.closeModal()}
                                  className={`btn btn-danger btn-sm`}
                                  type="button"
                                >
                                  No
                                </button>
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>


                  <Modal
                    show={this.state.childCompanyModalOpen}
                    onHide={() => this.closeModal()} backdrop="static"
                  >                   
                          <Modal.Header closeButton>
                            <Modal.Title>
                              Child Customer List
                            </Modal.Title>
                          </Modal.Header>
                          <Modal.Body>
                          <div className="contBox">
                            <Row>
                              <Col xs={12} sm={6} md={6}>
                                <div className="form-group">
                                  {this.state.child_company && this.state.child_company.map( (values, index) => {
                                    return (
                                      <div key={index}>
                                        {htmlDecode(values.company_name)}
                                      </div> 
                                    )
                                  })}
                                                                   
                                </div>
                              </Col>
                            </Row>                             
                          </div>
                        </Modal.Body>
                        <Modal.Footer>                           
                            <button
                              onClick={e => this.closeModal()}
                              className={`btn btn-danger btn-sm`}
                              type="button"
                            >
                              Close
                            </button>
                        </Modal.Footer>
                  </Modal>
                  

                  <Modal
                    show={this.state.addCompanyModalOpen}
                    onHide={() => this.closeModal()} backdrop="static"
                  >
                    <Formik
                      initialValues={initialValues}
                      validationSchema={validateCompany}
                      onSubmit={this.handleAddCompany}
                    >
                      {({ values, errors, touched, isValid, isSubmitting, setFieldValue, setFieldTouched}) => {
                        return (
                          <Form>
                            {this.state.showModalLoader === true ? ( 
                                <div className="loading_reddy_outer">
                                    <div className="loading_reddy" >
                                        <img src={whitelogo} alt="loader"/>
                                    </div>
                                </div>
                              ) : ( "" )}
                            <Modal.Header closeButton>
                              <Modal.Title>
                                Add Parent Customer
                              </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="contBox">
                                <Row>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Customer Name<span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="company_name"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter customer name"
                                        autoComplete="off"
                                        value={values.company_name}
                                      />
                                      {errors.company_name && touched.company_name ? (
                                        <span className="errorMsg">
                                          {errors.company_name}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row> 
                                {errors.message ? (
                                    <Row>
                                      <Col xs={12} sm={12} md={12}>
                                        <span className="errorMsg">
                                          {errors.message}
                                        </span>
                                      </Col>
                                    </Row>
                                  ) : ( "" )
                                } 

                                  {/*  */}
                                  <Row>
                                    <Col xs={12} sm={6} md={6}>
                                      <div className="form-group">
                                      <label>Child Customer</label>

                                    <Select
                                      isMulti
                                      name="child_companies[]"
                                      options={this.state.childCompanyList}
                                      className="basic-multi-select"
                                      classNamePrefix="select"
                                      onChange={evt => setFieldValue("child_companies",[].slice.call(evt).map(val => val.value))}
                                      placeholder="Enter child customer name"
                                      onBlur={() => setFieldTouched("child_companies")}
                                      //defaultValue={this.state.selectedChildCompanies}
                                    />
                                  {errors.child_companies && touched.child_companies ? (
                                    <span className="errorMsg">
                                      {errors.child_companies}
                                    </span>
                                  ) : null}

                                      </div>
                                    </Col>
                                  </Row> 
                                  {/*  */}

                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <button
                                  className={`btn btn-success btn-sm ${
                                    isValid ? "btn-custom-green" : "btn-disable"
                                  } m-r-10`}
                                  type="submit"
                                  disabled={isValid ? false : true}
                                >
                                  Add
                                </button>
                                <button
                                  onClick={e => this.closeModal()}
                                  className={`btn btn-danger btn-sm`}
                                  type="button"
                                >
                                  Close
                                </button>
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>

                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}

export default Company;
