import React, { Component } from 'react'
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  Tooltip,
  OverlayTrigger,
  Modal
} from "react-bootstrap";
import { Link } from "react-router-dom";
import API from "../../../shared/admin-axios";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import swal from "sweetalert"
import whitelogo from '../../../assets/images/drreddylogo_white.png';
import { showErrorMessage } from "../../../shared/handle_error";
import Pagination from "react-js-pagination";
import { getSuperAdmin, getAdminGroup } from '../../../shared/helper';
import Layout from "../layout/Layout";
import { htmlDecode } from '../../../shared/helper';


const initialValues = {
  product_name: '',
  product_sku: '',
  status: ''
};

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const actionFormatter = refObj => (cell, row) => {
  return (
    <div className="actionStyle">
      {/* {refObj.state.access.edit === true ?
        <LinkWithTooltip
          tooltip="Click to Edit"
          href="#"
          clicked={e => refObj.handleModal(e, cell)}
          id="tooltip-1"
        >
          <i className="far fa-edit" />
         </LinkWithTooltip>
        : null}
      {refObj.state.access.delete ? (
        <LinkWithTooltip
          tooltip="Click to Delete"
          href="#"
          clicked={(e) => refObj.confirmDelete(e, cell)}
          id="tooltip-1"
        >
          <i className="far fa-trash-alt" />
        </LinkWithTooltip>
      ) : null} */}
    </div>
  );
};

const custStatus = () => (cell) => {
  return cell === 1 ? "Active" : "Inactive";
};

class CompanyProduct extends Component {

  constructor(props) {
    super(props);
    this.state = {
      shipto_id: this.props.match.params.id,
      isLoading: true,
      product: [],
      product_id: 0,
      productDetails: {},
      count_product: 0,
      activePage: 1,
      totalCount: 0,
      itemPerPage: 20,
      showModalLoader: false,
      search_name: '',
      remove_search: false,
      child_details: false,
      master_list: [],
      selectedParent: "",
      selectedOther: "",
      get_access_data: false,
      selectStatus: [
        { id: "0", name: "Inactive" },
        { id: "1", name: "Active" }
      ],
    }
  }

  componentDidMount() {

    const superAdmin = getSuperAdmin(localStorage.admin_token);

    if (superAdmin === 1) {
      this.setState({
        access: {
          view: true,
          add: true,
          edit: true,
          delete: true
        },
        get_access_data: true
      });
      this.getShipToProduct();
    }
  }

  getShipToProduct(page = 1) {
    let product_name = this.state.search_name;
    let shipto_id = this.state.shipto_id;

    API.get(`/api/company/sap_product_list/${shipto_id}?page=${page}&product_name=${encodeURIComponent(product_name)}`)
      .then(res => {
        this.setState({
          product: res.data.data,
          count_product: res.data.count,
          isLoading: false,
          search_name: product_name
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        showErrorMessage(err, this.props);
      });
  }

  handleModal = (event, id) => {
    if (id) {
      event.preventDefault();
      API.get(`/api/company/sap_product/${id}`).then(res => {
        this.setState({
          showModal: true,
          product_id: id,
          productDetails: res.data.data
        });
      }).catch((err) => {
        showErrorMessage(err, this.props);
      });
    } else {

      this.setState({
        showModal: true,
        product_id: 0,
        productDetails: {}
      });

    }
  };
  modalCloseHandler = () => {
    this.setState({
      showModal: false,
      product_id: 0,
      productDetails: {}
    });
  };

  productSearch = (e) => {
    e.preventDefault();

    let product_name = document.getElementById('product_name').value
    let shipto_id = this.state.shipto_id;

    if (product_name === "") {
      return false;
    }

    API.get(`/api/company/sap_product_list/${shipto_id}?page=1&product_name=${encodeURIComponent(product_name)}`)
      .then(res => {
        this.setState({
          product: res.data.data,
          count_product: res.data.count,
          isLoading: false,
          search_name: product_name,
          remove_search: true
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        showErrorMessage(err, this.props);
      });
  }

  clearSearch = () => {
    document.getElementById('product_name').value = "";

    this.setState({
      search_name: "",
      remove_search: false
    }, () => {
      this.getShipToProduct();
      this.setState({ activePage: 1 })
    })
  }

  handleSubmitEvent = (values, actions) => {
    let post_data = {};
    let method = '';
    let url = '';
    if (this.state.product_id > 0) {
      post_data = {
        product_name: values.product_name,
        product_sku: values.product_sku,
        status: values.status
      };
      method = 'PUT';
      url = `/api/company/sap_product/${this.state.product_id}`;
    } else {
      post_data = {
        shipto_id: this.state.shipto_id,
        product_name: values.product_name,
        product_sku: values.product_sku,
      };
      method = 'POST';
      url = `/api/company/sap_product`;
    }
    API({
      url: url,
      data: post_data,
      method: method,

    }).then((res) => {
      this.setState({ showModal: false });
      swal({
        closeOnClickOutside: false,
        title: "Success",
        text: method === 'POST' ? "Record added successfully." : "Record updated successfully.",
        icon: "success",
      }).then(() => {
        this.setState({ activePage: 1 });
        this.getShipToProduct(this.state.activePage);
      });
    })
      .catch((err) => {
        this.setState({ showModalLoader: false });
        if (err.data.status === 3) {
          this.setState({
            showModal: false,
          });
          showErrorMessage(err, this.props);
        } else {
          actions.setErrors(err.data.errors);
          actions.setSubmitting(false);
        }
      });

  };

  confirmDelete = (event, id) => {
    event.preventDefault();
    swal({
      closeOnClickOutside: false,
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.deleteShipToProduct(id);
      }
    });
  };

  deleteShipToProduct = (id) => {
    if (id) {
      API.delete(`/api/company/sap_product/${id}`)
        .then((res) => {
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record deleted successfully.",
            icon: "success",
          }).then(() => {
            this.setState({ activePage: 1 });
            this.getShipToProduct(this.state.activePage);
          });
        })
        .catch((err) => {
          if (err.data.status === 3) {
            this.setState({ closeModal: true });
            showErrorMessage(err, this.props);
          }
        });
    }
  };



  render() {

    const { productDetails } = this.state;
    let newInitialValues;
    let validateStopFlag;
    if (this.state.product_id > 0) {
      newInitialValues = Object.assign(initialValues, {
        product_name: productDetails.product_name ? htmlDecode(productDetails.product_name) : "",
        product_sku: productDetails.product_sku ? htmlDecode(productDetails.product_sku) : "",
        status: productDetails.status || + productDetails.status === 0
          ? productDetails.status.toString()
          : "",
      });
      validateStopFlag = Yup.object().shape({
        product_name: Yup.string().min(2).required("Please enter the product name"),
        product_sku: Yup.string().required('Please enter the product sku').min(5).max(200),
        status: Yup.string().trim().required("Please select status").matches(/^[0|1]$/, "Invalid status selected"),
      });
    } else {
      newInitialValues = Object.assign(initialValues, {
        product_name: productDetails.product_name ? htmlDecode(productDetails.product_name) : "",
        product_sku: productDetails.product_sku ? htmlDecode(productDetails.product_sku) : "",
      });
      validateStopFlag = Yup.object().shape({
        product_name: Yup.string().min(2).required("Please enter the product name"),
        product_sku: Yup.string().required('Please enter the product sku').min(5).max(200),
      });
    }



    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy" >
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">

                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <h1>
                    Manage ShipTo Product
                            <small />
                  </h1>
                  <input
                      type="button"
                      value="Go Back"
                      className="btn btn-warning btn-sm"
                      onClick={() => {
                        window.history.go(-1);
                        return false;
                      }}
                      style={{ right: "18px", position: "absolute", top: "13px" }}
                    />
                </div>

                <div className="col-lg-12 col-sm-12 col-xs-12 topSearchSection">
                  {/* {this.state.access.add === true ?
                    <div className="">
                      <button
                        type="button"
                        className="btn btn-info btn-sm"
                        onClick={(e) => this.handleModal(e, "")}
                      >
                        <i className="fas fa-plus m-r-5" /> Add Product
                            </button>
                    </div>
                    : null} */}
                  
                  <form className="form">
                      <div className="">
                          <input
                              className="form-control"
                              name="product_name"
                              id="product_name"
                              placeholder="Product SKU"
                          />
                      </div>

                      <div className="">
                          <input
                              type="submit"
                              value="Search"
                              className="btn btn-warning btn-sm"
                              onClick={(e) => this.productSearch(e)}
                          />
                          {this.state.remove_search ? <a onClick={() => this.clearSearch()} className="btn btn-danger btn-sm"> Remove </a> : null}
                      </div>
                  </form>
                 

                </div>

              </div>
            </section>
            {/* <DashboardSearch groupList={this.state.groupList} /> */}
            <section className="content">
              <div className="box">

                <div className="box-body">





                  {/* <div className="nav-tabs-custom">
                          <ul className="nav nav-tabs">
                            <li className="tabButtonSec pull-right">
                            {this.state.count_shipto > 0 ? 
                          <span onClick={(e) => this.downloadXLSX(e)} >
                              <LinkWithTooltip
                                  tooltip={`Click here to download excel`}
                                  href="#"
                                  id="tooltip-my"
                                  clicked={e => this.checkHandler(e)}
                              >
                                  <i className="fas fa-download"></i>
                              </LinkWithTooltip>
                          </span>
                        : null }
                            </li>
                          </ul>
                        </div> */}







                  <BootstrapTable
                    data={this.state.product}
                  >
                    <TableHeaderColumn dataField="product_sku">
                      Product SKU
                          </TableHeaderColumn>
                    <TableHeaderColumn isKey dataField="product_name">
                      Product Name
                          </TableHeaderColumn>                    
                    {/* <TableHeaderColumn dataField="status" dataFormat={custStatus(this)}>
                      Status
                          </TableHeaderColumn> */}
                    {/* {this.state.access.edit === true?
                      <TableHeaderColumn
                        dataField="product_id"
                        dataFormat={actionFormatter(this)}
                      >
                        Action
                          </TableHeaderColumn>
                      : null} */}
                  </BootstrapTable>
                  {this.state.count_shipto > this.state.itemPerPage ? (
                    <Row>
                      <Col md={12}>
                        <div className="paginationOuter text-right">
                          <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemPerPage}
                            totalItemsCount={this.state.count_product}
                            itemClass='nav-item'
                            linkClass='nav-link'
                            activeClass='active'
                            onChange={this.handlePageChange}
                          />
                        </div>
                      </Col>
                    </Row>
                  ) : null}
                  <Modal
                    show={this.state.showModal}
                    onHide={() => this.modalCloseHandler()}
                    backdrop="static"
                  >
                    <Formik
                      initialValues={newInitialValues}
                      validationSchema={validateStopFlag}
                      onSubmit={this.handleSubmitEvent}
                    >
                      {({
                        values,
                        errors,
                        touched,
                        isValid,
                        isSubmitting,
                        setFieldValue,
                        setFieldTouched,
                      }) => {
                        return (
                          <Form>
                            {this.state.showModalLoader === true ? (
                              <div className="loading_reddy_outer">
                                <div className="loading_reddy">
                                  {/* <img src={whitelogo} alt="loader" /> */}
                                </div>
                              </div>
                            ) : (
                                ""
                              )}
                            <Modal.Header closeButton>
                              <Modal.Title>
                                {this.state.product_id > 0 ? "Edit" : "Add"} Product
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="contBox">
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>
                                        Product Name
                                          <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="product_name"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter product name"
                                        autoComplete="off"
                                        value={values.product_name}
                                      />
                                      {errors.product_name && touched.product_name ? (
                                        <span className="errorMsg">{errors.product_name}</span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>
                                        Product SKU
                                          <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="product_sku"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter the product sku"
                                        autoComplete="off"
                                        value={values.product_sku}
                                      />
                                      {errors.product_sku && touched.product_sku ? (
                                        <span className="errorMsg">{errors.product_sku}</span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                {
                                  this.state.product_id > 0 ?
                                    <Row>
                                      <Col xs={12} sm={12} md={12}>
                                        <div className="form-group">
                                          <label>
                                            Status
                                          <span className="impField">*</span>
                                          </label>
                                          <Field
                                            name="status"
                                            component="select"
                                            className={`selectArowGray form-control`}
                                            autoComplete="off"
                                            value={values.status}
                                          >
                                            <option key="-1" value="">
                                              Select
                                          </option>
                                            {this.state.selectStatus.map((status, i) => (
                                              <option key={i} value={status.id}>
                                                {status.name}
                                              </option>
                                            ))}
                                          </Field>
                                          {errors.status && touched.status ? (
                                            <span className="errorMsg">{errors.status}</span>
                                          ) : null}
                                        </div>
                                      </Col>
                                    </Row> : null
                                }

                                {errors.message ? (
                                  <Row>
                                    <Col xs={12} sm={12} md={12}>
                                      <span className="errorMsg">{errors.message}</span>
                                    </Col>
                                  </Row>
                                ) : (
                                    ""
                                  )}
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                              <button
                                className={`btn btn-success btn-sm ${isValid ? "btn-custom-green" : "btn-disable"
                                  } mr-2`}
                                type="submit"
                                disabled={isValid ? false : false}
                              >
                                {this.state.regionflagId > 0
                                  ? isSubmitting
                                    ? "Updating..."
                                    : "Update"
                                  : isSubmitting
                                    ? "Submitting..."
                                    : "Submit"}
                              </button>
                              <button
                                onClick={(e) => this.modalCloseHandler()}
                                className={`btn btn-danger btn-sm`}
                                type="button"
                              >
                                Close
                                </button>
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>
                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}

export default CompanyProduct;
