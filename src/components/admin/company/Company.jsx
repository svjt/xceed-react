import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  Alert,
  OverlayTrigger,
  Modal
} from "react-bootstrap";
import { Link } from "react-router-dom";
import API from "../../../shared/admin-axios";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import Select from "react-select";
import Layout from "../layout/Layout";
//import DashboardSearch from "./DashboardSearch";
import whitelogo from '../../../assets/images/drreddylogo_white.png';
import {showErrorMessage} from "../../../shared/handle_error";
import Pagination from "react-js-pagination";
import { htmlDecode } from '../../../shared/helper';
import { getSuperAdmin, getAdminGroup } from '../../../shared/helper';

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const actionFormatter = refObj => (cell, row) => {
  return (
    <div className="actionStyle">
      {refObj.state.access.edit === true ?
      <LinkWithTooltip
        tooltip="Click to Edit"
        href="#"
        clicked={e => refObj.modalEditShowHandler(e, cell)}
        id="tooltip-1"
      >
        <i className="far fa-edit" />
      </LinkWithTooltip>
      : null }

    {refObj.state.access.delete === true ?
      <LinkWithTooltip
        tooltip="Click to Delete"
        href="#"
        clicked={e => refObj.modalShowDeleteHandler(e, cell)}
        id="tooltip-1"
      >
        <i className="far fa-trash-alt" />
      </LinkWithTooltip>
      : null }

      {refObj.state.access.edit === true && row.count_customer > 0 ? 
        <LinkWithTooltip
          tooltip="Click to Exchange"
          href="#"
          clicked={e => refObj.modalShowExchangeHandler(e, cell)}
          id="tooltip-1"
        >
          <i className="fas fa-exchange-alt" />
        </LinkWithTooltip>
        : null }

      {refObj.state.access.edit === true ? 
        <LinkWithTooltip
          tooltip="Click to View ShipTo List"
          href={`company_shipto/${row.company_id}`}
          id="tooltip-shipto"          
        >
          <i className="fa fa-list" style={{padding : "0 10px"}} />
        </LinkWithTooltip>
        : null }      
      
      
      {refObj.state.access.edit === true && row.count_active_customer > 0 && row.sales_company_id === null ? 
        <LinkWithTooltip
          tooltip="Add Sales Company ID"
          href="#"
          clicked={e => refObj.modalShowAddSalesIdHandler(e, cell)}
          id="tooltip-1"
        >
          {`  `}<i className="fa fa-plus" style={{paddingRight : "10px"}} />
        </LinkWithTooltip>
        : null }

      {refObj.state.access.edit === true && row.sales_company_id !== null ? 
        <LinkWithTooltip
          tooltip="Edit Sales Company ID"
          href="#"
          clicked={e => refObj.modalShowEditSalesIdHandler(e, cell,row.sales_company_id)}
          id="tooltip-1"
        >
          {`  `}<i className="fas fa-crop" style={{paddingRight : "10px"}} />
        </LinkWithTooltip>
        : null }

      {refObj.state.access.edit === true && row.sap_ref_no !== '' && row.info_avail === 'Available' && row.count_shipto > 0  ? 
        <LinkWithTooltip
          tooltip="Click to Re-sync ShipTo List"          
          id="tooltip-soldto"
          href="#"
          clicked={e => refObj.syncShipTo(e,row.sap_ref_no)}          
        >
          <i className="fa fa-sync" style={{paddingRight : "10px",  color: "green"}} />
        </LinkWithTooltip>
        : null }

      {refObj.state.access.edit === true && row.sap_ref_no != '' && row.info_avail === 'Available' && row.count_shipto === null ? 
        <LinkWithTooltip
          tooltip="Click to Sync ShipTo List"          
          id="tooltip-soldto"
          href="#"
          clicked={e => refObj.syncShipTo(e,row.sap_ref_no)}
        >
          <i className="fa fa-sync"  style={{ color: "red"}} />
        </LinkWithTooltip>
        : null }

      

      

    </div>
  );
};

const initialValues = {
  child_company_name: "",
  parent_company_name: "",
  other_company_name: ""
}

const addInitialValues = {
  child_company_name: "",
  parent_company_name: "",
  sap_ref_no: ""
}

const updateInitialValues = {
  child_company_name: "",
  parent_company_name: "",
  sap_ref_no: "",
  sap_customer_id : ""
}

const addSalesIdInitialValues = {
  sales_customer_id: ""
}

const editSalesIdInitialValues = {
  sales_customer_id: ""
}

const __htmlDecode = refObj => cell => {
  return htmlDecode(cell);
}

class Company extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      company: [],
      company_list:[],
      soldtoDetails: [],
      showDetails: 0,
      // groupList: [],
      // roleList: [],      
      showDeleteModal : false,
      showEditModal   : false,
      activePage      : 1,
      totalCount      : 0,
      itemPerPage     : 20,
      showModalLoader : false,
      search_name: '',
      remove_search: false,
      child_details : false,
      master_list : [],
      selectedParent: "",
      selectedOther : "",
      otherCompany: [],
      exchangeCompany : true,
      get_access_data:false,
      addButtonid: '',
      buyer_sold_auto_suggest:"",
      selected_sold_to:[]
    }
  }

  componentDidMount() {
    //this.getCompanyList();
    const superAdmin  = getSuperAdmin(localStorage.admin_token);
    
    if(superAdmin === 1){
      this.setState({
        access: {
           view : true,
           add : true,
           edit : true,
           delete : true
         },
         get_access_data:true
     });
     this.getCompanyList();
    }else{
      const adminGroup  = getAdminGroup(localStorage.admin_token);
      API.get(`/api/adm_group/single_access/${adminGroup}/${'CUSTOMER_MANAGEMENT'}`)
      .then(res => {
        this.setState({
          access: res.data.data,
          get_access_data:true
        }); 

        if(res.data.data.view === true){
          this.getCompanyList();
        }else{
          this.props.history.push('/admin/dashboard');
        }
        
      })
      .catch(err => {
        showErrorMessage(err,this.props);
      });
    } 
  };

  handleChange = (event) =>{
    event.preventDefault();
    let valid = event.target.value;
    this.setState({addButtonid : valid});
  } 

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getCompanyList(pageNumber > 0 ? pageNumber  : 1);
  };

  getCompanyList = (page = 1) => {

    let company_name = this.state.search_name;

    API.get(`/api/company/child_list?page=${page}&company_name=${encodeURIComponent(company_name)}`)
      .then(res => {
        this.setState({
          activePage: page,
          company: res.data.data,
          count_company: res.data.count_company,
          isLoading: false,
          search_name: company_name
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        showErrorMessage(err,this.props);
      });
  }

  closeDeleteModal = () => {
    this.setState({ company_id: 0 });
    this.setState({ showDeleteModal: false, exchangeCompany: true });
  };

  modalShowDeleteHandler = (event, id) => {
    event.preventDefault();
    this.setState({ company_id: id, showDeleteModal: true,  exchangeCompany: true });
  };

  // exchange icon ==========
  modalShowExchangeHandler = (event, id) => {
    event.preventDefault();
    this.setState({ company_id: id, showDeleteModal: true, exchangeCompany: false });
  }

  modalShowAddSalesIdHandler = (event, id) => {
    event.preventDefault();
    this.setState({ company_id: id, showAddSalesModal: true, exchangeCompany: false });
  }

  modalShowEditSalesIdHandler = (event, id,sales_id) => {
    event.preventDefault();
    editSalesIdInitialValues.sales_customer_id = sales_id;
    this.setState({ company_id: id, showEditSalesModal: true,sales_comp_id_db:sales_id, exchangeCompany: false });
  }
  // excahnge icon ==========

  closeEditModal = () => {
    this.setState({ company_list:[],company_id: 0,showEditModal: false, child_details: false });
    updateInitialValues.child_company_name = '';
    updateInitialValues.parent_company_name = '';
    updateInitialValues.sap_customer_id = '';
    updateInitialValues.sap_ref_no = '';
  };

  closeAddSalesIdModal = () => {
    this.setState({  });
    this.setState({ company_id: 0,showAddSalesModal: false });
  };

  closeEditSalesIdModal = () => {
    this.setState({  });
    this.setState({ company_id: 0,showEditSalesModal: false,sales_comp_id_db:"" });
  };

  closeCustomerModal = () => {
    this.setState({ company_list:[],company_id: 0,showCustomerModal: false, addCustomerModal: false, otherCompany: [], showDetails : 0, soldtoDetails: [], addButtonid : '' });
  }

  modalEditShowHandler = (event, id) => {
    event.preventDefault();
    this.setState({ company_id: 0, showDetails : 0, isLoading:true, soldtoDetails: [] });
    this.getParentCompany();
    this.getCompany(id);
    this.setState({ company_id: id, showEditModal: true , isLoading: false});
  };

  makeDynamicHtml = () => {
    let details_state = this.state.soldtoDetails;
    //console.log(details_state);
    return (
      <>
        {details_state ? (
          <>
            
            <Row>
                     
              <Col xs={12} sm={12} md={12}>
                <div className="form-group">
                  <label>
                    Country Name
                  </label>
                  <Field                  
                    type="text"
                    className={`form-control`}
                    placeholder="Country Name"
                    autoComplete="off"
                    disabled                  
                    value={details_state.cty_name}
                  />
                </div>
                <Field                  
                    type="hidden"
                    name="sap_customer_id"
                    className={`form-control`}                    
                    autoComplete="off"
                    disabled                  
                    value={details_state.customer_id}
                  />
              </Col>
            </Row>
            <Row>
              <Col xs={12} sm={6} md={6}>
                <div className="form-group">
                  <label>
                    City
                  </label>
                  <Field                  
                    type="text"
                    className={`form-control`}
                    placeholder="City"
                    autoComplete="off"
                    disabled                  
                    value={details_state.city}
                  />
                </div>
              </Col>            
              <Col xs={12} sm={6} md={6}>
                <div className="form-group">
                  <label>
                    Postal Code
                  </label>
                  <Field                  
                    type="text"
                    className={`form-control`}
                    placeholder=" Postal Code"
                    autoComplete="off"
                    disabled                  
                    value={details_state.postalcode}
                  />
                </div>
              </Col>
            </Row>
            <Row>
              <Col xs={12} sm={12} md={12}>
                <div className="form-group">
                  <label>
                    Street
                  </label>
                  <Field                  
                    type="text"
                    className={`form-control`}
                    placeholder="Street"
                    autoComplete="off"
                    disabled                  
                    value={details_state.street}
                  />
                </div>
              </Col> 
            </Row>
          </>
        ) : ( 
          <Row>
            <Col xs={12} sm={6} md={6}>
              <div className="form-group">
                <p>No Such data found</p>
              </div>
            </Col>
          </Row> 
        )}
      </>
    );
  };

  populateSoldTo = (event, soldto_id) => {
    if(soldto_id){

      API.get(`/api/company/soldto_details/${soldto_id}`)
      .then(res => {
        //console.log(res.data.data);
        this.setState({soldtoDetails : res.data.data, showDetails: 1});        
      })
      .catch(err => {
        if(err.data.status === 3){
          this.setState({ closeModal: true });
          showErrorMessage(err,this.props);
        }
      });
    }else{
      this.setState({showDetails: 1});      
    }
  }

  syncShipTo = (event, refsoldtoid) => { 
    event.preventDefault();
    this.setState({company:[],count_company:0});
    API.post(`/api/company/shiptosync/`,{soldto_id : refsoldtoid})
      .then(res => {        
        swal({
          closeOnClickOutside: false,
          title: (res.data.is_sync=='0') ? "Failed" : "Success",
          text: (res.data.message!='') ? res.data.message : "ShipTo & Product successfully synced",
          icon: (res.data.is_sync=='0') ? "error" :"success"
        }).then(() => {
          this.setState({
            search_name: "",
            remove_search: false
          }, () => {
            this.getCompanyList(1);
            this.setState({activePage: 1})
          })
        });
      })
      .catch(err => {  
          showErrorMessage(err, this.props);
      });
  }

  syncAllShipTo = (event) => { 
    event.preventDefault();
    this.setState({company:[],count_company:0});
    API.get(`/api/company/shiptosyncall/`)
      .then(res => {        
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: "ShipTo & Product successfully synced",
          icon: "success"
        }).then(() => {
          this.setState({
            search_name: "",
            remove_search: false
          }, () => {
            this.getCompanyList(1);
            this.setState({activePage: 1})
          })
        });
      })
      .catch(err => {  
          showErrorMessage(err, this.props);
      });
  }

  getParentCompany = () => {
    API.get(`/api/company/get_all_parent`)
    .then(res => {
      const parent_list = [];
      res.data.data.map((values, index) => {
        parent_list.push({ value : values.parent_company_id, label: htmlDecode(values.parent_company_name)})
      })
      this.setState({master_list : parent_list});
    })
    .catch(err => {
      if(err.data.status === 3){
        this.setState({ closeModal: true });
        showErrorMessage(err,this.props);
      }
    });
  }

  getCompany = id => {
    // Edit company auto fill

    this.setState({selected_sold_to:[],selectedParent:""});

    API.get(`/api/company/child_details/${id}`)
    .then(res => {
          let soldtoDetails = {}
          // if(res.data.data.city || res.data.data.country || res.data.data.street || res.data.data.postalcode ){
          //   soldtoDetails.city = res.data.data.city;
          //   soldtoDetails.cty_name = res.data.data.country;
          //   soldtoDetails.postalcode = res.data.data.postalcode;
          //   soldtoDetails.street = res.data.data.street;
          // }

          let setStateOBJ = {
            selected_sold_to:res.data.data
          };

          if(res.data.company_data && res.data.company_data.parent_id != null && res.data.company_data.parent_name != null){
            setStateOBJ.selectedParent = {
              value :  res.data.company_data.parent_id,
              label :  htmlDecode(res.data.company_data.parent_name)
            }
          }

          updateInitialValues.parent_company_name = (this.state.selectedParent && this.state.selectedParent.value) > 0 ? 
          this.state.selectedParent : '';
          updateInitialValues.child_company_name = res.data.company_data.child_name ? htmlDecode(res.data.company_data.child_name) : "";

          this.setState(setStateOBJ);

          // this.setState({
          //   selected_sold_to
          //     child_details : res.data.data, 
          //     selectedParent : {
          //       value :  res.data.data.data  ? res.data.data.data.parent_id : '',
          //       label :  res.data.data.data  ? htmlDecode(res.data.data.data.parent_name) : '',
          //     },
          //     // soldtoDetails : soldtoDetails,
          //     // showDetails: Object.keys(soldtoDetails).length ? 1 : 0
          //     soldtoDetails : res.data.data.ship_to_data,
          //     showDetails   : res.data.data.ship_to_data.length ? 1 : 0
          // }, () => {
          //   updateInitialValues.parent_company_name = (this.state.selectedParent && this.state.selectedParent.value) > 0 ? 
          //                                             this.state.selectedParent : '';
          //   updateInitialValues.child_company_name = res.data.data.child_name ? htmlDecode(res.data.data.child_name) : "";
          //   updateInitialValues.sap_ref_no = res.data.data.sap_ref_no ? res.data.data.sap_ref_no : "";

          // })
    })
    .catch(err => {
      console.log({err})
      if(err.data && err.data.status === 3){
        this.setState({ closeModal: true });
        showErrorMessage(err,this.props);
      }
    });
  }

  companySearch = (e) => {
    e.preventDefault();

    var company_name = document.getElementById('company_name').value

    if(company_name === ""){
      return false;
    }

    API.get(`/api/company/child_list?page=1&company_name=${encodeURIComponent(company_name)}`)
    .then(res => {
      this.setState({
        activePage: 1,
        company: res.data.data,
        count_company: res.data.count_company,
        isLoading: false,
        search_name: company_name,
        remove_search: true
      });
    })
    .catch(err => {
      this.setState({
        isLoading: false
      });
      showErrorMessage(err,this.props);
    });
  }

  clearSearch = () => {
    document.getElementById('company_name').value     = "";

    this.setState({
      search_name: "",
      remove_search: false
    }, () => {
      this.getCompanyList();
      this.setState({activePage: 1})
    })
  }

  handleDeleteCompany = (values, action) => {
    this.setState({ showModalLoader : true })
    API.delete(`/api/company/child/${this.state.company_id}`)
    .then(res => {
      this.setState({ showDeleteModal: false, showModalLoader: false });
      if(res.data.customer && res.data.customer === true){

        let otherCompArr = res.data.data.map((values, index) => {
          return {value: values.company_id, label : htmlDecode(values.company_name)}
        })
        this.setState({showCustomerModal: true, otherCompany: otherCompArr, selectedOther: ""});
      }else{
          swal({
            closeOnClickOutside: false,
            title:"Success",
            text: "Company has been deleted successfully",
            icon: "success"
          }).then(() => {
            this.getCompanyList();
          });
      }      
    })
    .catch(err => {
      if(err.data.status === 3){
        this.setState({ closeModal: true, showModalLoader: false });
        showErrorMessage(err,this.props);
      }else{
        this.setState({ showDeleteModal: false, showModalLoader: false });
        swal({
          closeOnClickOutside: false,
          title:"Warning",
          text: err.data.errors.child_company_name,
          icon: "error"
        }).then(() => {
          this.getCompanyList();
        });
      }
    });
  }

  setSOLDTO = (event, setFieldValue) => {

    setFieldValue("sap_ref_no", event.target.value.toString());
    
    if(event.target.value && event.target.value.length > 2){
      // API.post(`/api/feed/find_sap_soldTo_admin`,{soldto_id:event.target.value,exclude:JSON.stringify(this.state.selected_sold_to.map(val=>{return val.value}))})
      API.post(`/api/feed/find_sap_soldTo_admin`,{soldto_id:event.target.value,exclude:JSON.stringify([])})
      .then((res) => {

        
        let ret_html = res.data.data.map((val,index)=>{
          return (<li style={{cursor:'pointer',marginTop:'6px'}} onClick={()=>this.setSOLDTOAutoSuggest(val,setFieldValue)} >{val.label}<hr style={{marginTop:'6px',marginBottom:'0px'}} /></li>)
        })
        this.setState({buyer_sold_auto_suggest:ret_html});



      })
      .catch((err) => {
        console.log(err);
      });
    }
  };

  setSOLDTOAutoSuggest = (event, setFieldValue) => {

    setFieldValue("sap_ref_no", '');
    this.setState({buyer_sold_auto_suggest:'' });

    let prev_state = this.state.selected_sold_to;
    const soldTo_exists = prev_state.some(item => item.value == event.value); 
    // console.log("soldto",event.value);
    // console.log("customer",event.customer);
    if(!soldTo_exists){
      // console.log("soldTo_exists",soldTo_exists);
      API.get(`/api/company/soldto_details/${event.value}/${event.customer}`)
      .then((res) => {
  
        event.ship_to_data = res.data.data;
  
        prev_state = [event, ...prev_state]; // Add to the top
        // prev_state.push(event); // Add to the bottom
  
        this.setState({selected_sold_to:prev_state});
        
      })
      .catch((err) => {
        console.log(err);
      });
    }
  };

  handleUpdateCompany = ( values,action ) => {

    let sap_ref_no_arr = this.state.selected_sold_to.map((val)=>{return val.value});
    let customer_id_arr = this.state.selected_sold_to.map((val)=>{return val.ship_to_data.customer_id});

    this.setState({ showModalLoader : true })
    let postData = {
      parent_id: values.parent_company_name.value,
      company_name : values.child_company_name,
      //sap_ref_no : values.sap_ref_no,
      //sap_customer_id : this.state.soldtoDetails.customer_id,
      sap_customer_id : customer_id_arr.join(','),
      sap_ref_no_arr  : sap_ref_no_arr.join(',')
    }

    API.put(`/api/company/child/${this.state.company_id}`, postData)
    .then(res => {
      this.setState({ company_list:[],company_id: 0,showEditModal: false, showModalLoader: false });
      swal({
        closeOnClickOutside: false,
        title:"Success",
        text:"Company has been updated successfully",
        icon: "success"
      }).then(() => {
        this.getCompanyList();
      });
    })
    .catch(err => {
      this.setState({ closeModal: true, showModalLoader: false });
      if(err.data.status === 3){
        showErrorMessage(err,this.props);
      }else{
        action.setErrors(err.data.errors)
        console.log("errors",err.data.errors);
      }
    });
  }

  handleAddSalesCompanyId = ( values,action ) => {
    this.setState({ showModalLoader : true })
    let postData = {
      sales_customer_id : values.sales_customer_id
    }

    API.put(`/api/company/sales_id/${this.state.company_id}`, postData)
    .then(res => {
      this.setState({ company_id: 0,showAddSalesModal: false, showModalLoader: false });
      swal({
        closeOnClickOutside: false,
        title:"Success",
        text:"Company Sales ID has been updated successfully",
        icon: "success"
      }).then(() => {
        this.getCompanyList();
      });
    })
    .catch(err => {
      this.setState({ closeModal: true, showModalLoader: false });
      if(err.data.status === 3){
        showErrorMessage(err,this.props);
      }else{
        action.setErrors(err.data.errors)
      }
    });
  }

  handleEditSalesCompanyId = ( values,action ) => {
    this.setState({ showModalLoader : true })
    let postData = {
      sales_customer_id : values.sales_customer_id
    }

    API.put(`/api/company/sales_id_edit/${this.state.company_id}`, postData)
    .then(res => {
      this.setState({ company_id: 0,showEditSalesModal: false, showModalLoader: false });
      swal({
        closeOnClickOutside: false,
        title:"Success",
        text:"Company Sales ID has been updated successfully",
        icon: "success"
      }).then(() => {
        this.getCompanyList();
      });
    })
    .catch(err => {
      this.setState({ closeModal: true, showModalLoader: false });
      if(err.data.status === 3){
        showErrorMessage(err,this.props);
      }else{
        action.setErrors(err.data.errors)
      }
    });
  }

  handleOtherCompany = (values, action) => {
    this.setState({ showModalLoader : true })
    let postData = {
      new_company_id: values.other_company_name.value,
      delete : this.state.exchangeCompany
    }

    API.put(`/api/company/change_company/${this.state.company_id}`, postData)
    .then(res => {
      this.setState({ otherCompany:[], company_id: 0, showCustomerModal: false, showModalLoader : false });
      swal({
        closeOnClickOutside: false,
        title:"Success",
        text: "Company has been swapped successfully",
        icon: "success"
      }).then(() => {
        this.getCompanyList();
      });
    })
    .catch(err => {
      this.setState({ closeModal: true,showModalLoader : false });
      if(err.data.status === 3){
        showErrorMessage(err,this.props);
      }else{
        action.setErrors(err.data.errors)
      }
    });
  }

  handleAddCompany = (values, action) => {

    let sap_ref_no_arr = this.state.selected_sold_to.map((val)=>{return val.value});
    let customer_id_arr = this.state.selected_sold_to.map((val)=>{return val.ship_to_data.customer_id});
    //console.log({selected_sold_to : this.state.selected_sold_to});
    this.setState({ showModalLoader : true })
    let postData = {
      parent_id : values.parent_company_name ? values.parent_company_name.value : "",
      company_name  : values.child_company_name,
      // sap_ref_no : this.state.addButtonid,
      // sap_customer_id : this.state.soldtoDetails.customer_id,
      sap_customer_id : customer_id_arr.join(','),
      sap_ref_no_arr  : sap_ref_no_arr.join(',')
    }

    API.post(`/api/company/child/`, postData)
    .then(res => {
      this.setState({ master_list:[], company_id: 0, addCustomerModal: false, showModalLoader : false , showDetails : 0, soldtoDetails: [], addButtonid : '' });
      swal({
        closeOnClickOutside: false,
        title:"Success",
        text: "Company has been added successfully",
        icon: "success"
      }).then(() => {
        this.getCompanyList();
      });
    })
    .catch(err => {
      this.setState({ closeModal: true, showModalLoader : false });
      if(err.data.status === 3){
        showErrorMessage(err,this.props);
      }else{
        action.setErrors(err.data.errors);
      }
    });
  }

  changeParentCompany = (event,setFieldValue) => {
    setFieldValue("parent_company_name", event === null ? "" : event);
    this.setState({selectedParent :  event === null ? "" : event})
  }

  changeOtherCompany = (event,setFieldValue,setFieldTouched) => {
    setFieldTouched('other_company_name');
    setFieldValue("other_company_name", event === null ? "" : event);    
    this.setState({selectedOther :  event === null ? "" : event}) 
  }

  openAddCompany = () => {
    this.getParentCompany();
    this.setState({ addCustomerModal : true,selected_sold_to:[]});
    initialValues.child_company_name  = ""
    initialValues.parent_company_name = ""
  }

  downloadXLSX = (e) => {
    e.preventDefault();

    var company_name = document.getElementById('company_name').value;    

    API.get(`/api/company/child_list/download?page=1&company_name=${encodeURIComponent(company_name)}`,{responseType: 'blob'})
    .then(res => { 
      let url    = window.URL.createObjectURL(res.data);
      let a      = document.createElement('a');
      a.href     = url;
      a.download = 'customers.xlsx';
      a.click();

    }).catch(err => {
      showErrorMessage(err,this.props);
    });
  }

  checkHandler = (event) => {
    event.preventDefault();
  };

  generateSelectedSoldTo = (setErrors) => {
    let fileListHtml = "";
    if (this.state.selected_sold_to && this.state.selected_sold_to.length > 0) {
      fileListHtml = this.state.selected_sold_to.map((sold_to) => (
        <Alert key={sold_to.value}>
          <span onClick={() => {setErrors({}); this.removeSoldTo(sold_to);}}>
            <i
              className="far fa-times-circle"
              style={{ cursor: "pointer" }}
            ></i>
          </span>{" "}
          {sold_to.label}
          {sold_to.ship_to_data ? (
            <>
            <Row>   
                <Col xs={12} sm={12} md={12}>
                <div className="form-group">
                    <label>
                    Country Name
                    </label>
                    <Field                  
                    type="text"
                    className={`form-control`}
                    placeholder="Country Name"
                    autoComplete="off"
                    disabled                  
                    value={sold_to.ship_to_data.cty_name}
                    />
                </div>
                <Field                  
                    type="hidden"
                    name="sap_customer_id"
                    className={`form-control`}                    
                    autoComplete="off"
                    disabled                  
                    value={sold_to.ship_to_data.customer_id}
                    />
                </Col>
            </Row>
            <Row>
                <Col xs={12} sm={6} md={6}>
                <div className="form-group">
                    <label>
                    City
                    </label>
                    <Field                  
                    type="text"
                    className={`form-control`}
                    placeholder="City"
                    autoComplete="off"
                    disabled                  
                    value={sold_to.ship_to_data.city}
                    />
                </div>
                </Col>            
                <Col xs={12} sm={6} md={6}>
                <div className="form-group">
                    <label>
                    Postal Code
                    </label>
                    <Field                  
                    type="text"
                    className={`form-control`}
                    placeholder=" Postal Code"
                    autoComplete="off"
                    disabled                  
                    value={sold_to.ship_to_data.postalcode}
                    />
                </div>
                </Col>
            </Row>
            <Row>
                <Col xs={12} sm={12} md={12}>
                <div className="form-group">
                    <label>
                    Street
                    </label>
                    <Field                  
                    type="text"
                    className={`form-control`}
                    placeholder="Street"
                    autoComplete="off"
                    disabled                  
                    value={sold_to.ship_to_data.street}
                    />
                </div>
                </Col> 
            </Row>
            </>
        ) : ( 
            <Row>
            <Col xs={12} sm={6} md={6}>
                <div className="form-group">
                <p>No Such data found</p>
                </div>
            </Col>
            </Row> 
        )}
        </Alert>
      ));
    }

    return fileListHtml;
  }

  removeSoldTo = (soldTo) => {
    let prevState = this.state.selected_sold_to;
    let newState = [];
    for (let index = 0; index < prevState.length; index++) {
      const element = prevState[index];
      // console.log("soldTo",soldTo)
      // console.log("value",soldTo.value,element.value)
      // console.log("customer",soldTo.customer,element.customer)
      if (soldTo.value !== element.value || soldTo.customer !== element.customer) {
        newState.push(element);
      }
    }

    this.setState({ selected_sold_to: newState });
  };
  
  render() {
    const newInitialValues = Object.assign(initialValues, {
      child_company_name : this.state.child_details.child_name ? htmlDecode(this.state.child_details.child_name) : '',
      parent_company_name: this.state.selectedParent && this.state.selectedParent.value > 0 ? this.state.selectedParent : '',
      other_company_name : this.state.selectedOther ? this.state.selectedOther : ''
    });

    let validateCompany = Yup.object().shape({
      child_company_name: Yup.string().required('Please enter company name')
    });

    let validateAddSalesId = Yup.object().shape({
      sales_customer_id: Yup.string().required('Please enter sales company id')
    });

    let validateOther = Yup.object().shape({
      other_company_name: Yup.string().required('Please select the alternate company')
    });
    

    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
            <div className="loderOuter">
            <div className="loading_reddy_outer">
                <div className="loading_reddy" >
                    <img src={whitelogo} alt="logo" />
                </div>
                </div>
            </div>
        </>        
      );
    } else {
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">

                  <div className="col-lg-12 col-sm-12 col-xs-12">
                    <h1>
                        Manage Customers
                      <small />
                    </h1>
                    
                  </div>

                  <div className="col-lg-12 col-sm-12 col-xs-12 topSearchSection">
                  {this.state.access.add === true ?
                    <div className="">
                    <button
                        type="button"
                        className="btn btn-info btn-sm"
                        onClick={() => this.openAddCompany()}
                      >
                        <i className="fas fa-plus m-r-5" /> Add Customer
                      </button>

                      <button
                        type="button"
                        className="btn btn-info btn-sm"
                        onClick={e => this.syncAllShipTo(e)}
                        //style={{padding : "0 10px"}} 
                      >
                        <i className="fas fa-sync m-r-5" /> Sync All
                      </button>
                    </div>
                  : null }
                      {/*  */}
                      <form className="form">
                            <div className="">
                            <input
                                className="form-control"
                                name="company_name"
                                id="company_name"
                                placeholder="Customer name"
                              />
                            </div>

                            <div className="">
                            <input
                              type="submit"
                              value="Search"
                              className="btn btn-warning btn-sm"
                              onClick={(e) => this.companySearch( e )}
                            />
                            {this.state.remove_search ? <a onClick={() => this.clearSearch()} className="btn btn-danger btn-sm"> Remove </a> : null}
                            </div>
                      </form>
                      {/*  */}

                                      </div>
 
              </div>
            </section>
            {/* <DashboardSearch groupList={this.state.groupList} /> */}
            <section className="content">
              <div className="box">

                <div className="box-body">
                  <div className="nav-tabs-custom">
                    <ul className="nav nav-tabs">
                      <li className="tabButtonSec pull-right">
                      {this.state.count_company > 0 ? 
                    <span onClick={(e) => this.downloadXLSX(e)} >
                        <LinkWithTooltip
                            tooltip={`Click here to download excel`}
                            href="#"
                            id="tooltip-my"
                            clicked={e => this.checkHandler(e)}
                        >
                            <i className="fas fa-download"></i>
                        </LinkWithTooltip>
                    </span>
                  : null }
                      </li>
                    </ul>
                  </div>
                  
                  <BootstrapTable
                    data={this.state.company}
                  >
                    <TableHeaderColumn isKey dataField="company_name"  dataFormat={__htmlDecode(this)}>
                      Customers
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="sap_ref_no"  dataFormat={__htmlDecode(this)}>
                      SoldTo
                    </TableHeaderColumn>
                    {/* <TableHeaderColumn dataField="info_avail">
                      SoldTo Address
                    </TableHeaderColumn> */}
                    <TableHeaderColumn dataField="count_customer">
                      Users
                    </TableHeaderColumn>
                    {this.state.access.edit === true || this.state.access.delete === true ?
                    <TableHeaderColumn
                      dataField="company_id"
                      dataFormat={actionFormatter(this)}
                      dataAlign=""
                    >
                      Action
                    </TableHeaderColumn>
                    : null}
                  </BootstrapTable>
                  {this.state.count_company > this.state.itemPerPage ? (
                    <Row>
                      <Col md={12}>
                        <div className="paginationOuter text-right">
                          <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemPerPage}
                            totalItemsCount={this.state.count_company}
                            itemClass='nav-item'
                            linkClass='nav-link'
                            activeClass='active'
                            onChange={this.handlePageChange}
                          />
                        </div>
                      </Col>
                    </Row>
                  ) : null}

                  {/* ======= Add/Edit Admin ======== */}

                  <Modal
                    show={this.state.showDeleteModal}
                    onHide={() => this.closeDeleteModal()} 
                    backdrop="static"
                  >
                    <Formik
                      initialValues={newInitialValues}
                      validationSchema={""}
                      onSubmit={this.handleDeleteCompany}
                    >
                      {({ values, errors, touched, isValid, isSubmitting, setFieldValue, setFieldTouched}) => {                
                        return (
                          <Form>
                            {this.state.showModalLoader === true ? ( 
                                <div className="loading_reddy_outer">
                                    <div className="loading_reddy" >
                                        <img src={whitelogo} alt="loader"/>
                                    </div>
                                </div>
                              ) : ( "" )}
                            <Modal.Header closeButton>
                              <Modal.Title>
                              {this.state.exchangeCompany === false ? "Swap Customer" : "Delete Customer"}
                              </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="contBox">
                                <Row>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Are you sure to {this.state.exchangeCompany === false ? "swap" : "delete"} this customer?
                                      </label>
                                    </div>
                                  </Col>
                                </Row>                    
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                              
                                <button
                                  className={`btn btn-success btn-sm btn-custom-green m-r-10`}
                                  type="submit"
                                >
                                  Yes
                                </button>
                                <button
                                  onClick={e => this.closeDeleteModal()}
                                  className={`btn btn-danger btn-sm`}
                                  type="button"
                                >
                                  No
                                </button>
                              
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>

                  
                  <Modal
                    show={this.state.showCustomerModal}
                    onHide={() => this.closeCustomerModal()} 
                    backdrop="static"
                  >
                    <Formik
                      initialValues={newInitialValues}
                      validationSchema={validateOther}
                      onSubmit={this.handleOtherCompany}
                    >
                      {({ values, errors, touched, isValid, isSubmitting, setFieldValue,setFieldTouched}) => {
                        
                        return (
                          <Form>
                            {this.state.showModalLoader === true ? ( 
                                <div className="loading_reddy_outer">
                                    <div className="loading_reddy" >
                                        <img src={whitelogo} alt="loader"/>
                                    </div>
                                </div>
                              ) : ( "" )}
                            <Modal.Header closeButton>
                              <Modal.Title>
                                Change Customer
                              </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="contBox">
                                <Row>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Please select alternate company for existing customers
                                      </label>

                                      <Select
                                        className="basic-single"
                                        classNamePrefix="select"
                                        isClearable={true}
                                        isSearchable={true}
                                        name="other_company_name"
                                        options={this.state.otherCompany}
                                        defaultValue={values.other_company_name}
                                        value={values.other_company_name}
                                        onChange={e => { this.changeOtherCompany(e,setFieldValue,setFieldTouched) }} 
                                      />
                                    {errors.other_company_name && touched.other_company_name ? (
                                      <span className="errorMsg">
                                        {errors.other_company_name}
                                      </span>
                                    ) : null}
                                    
                                    </div>
                                  </Col>
                                </Row>                    
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                              
                              <button
                                  className={`btn btn-success btn-sm ${isValid ? "btn-custom-green" : "btn-disable"} m-r-10`}
                                  type="submit"
                                >
                                  Update
                                </button>
                                <button
                                  onClick={e => this.closeCustomerModal()}
                                  className={`btn btn-danger btn-sm`}
                                  type="button"
                                >
                                  No
                                </button>
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>

                {
                  this.state.showEditModal && (
                 
                  <Modal
                    show={this.state.showEditModal}
                    onHide={() => this.closeEditModal()} backdrop="static"
                  >
                    <Formik
                      initialValues={updateInitialValues}
                      validationSchema={validateCompany}
                      onSubmit={this.handleUpdateCompany}
                    >
                      {({ values, errors, touched, isValid, isSubmitting, setFieldValue, setErrors}) => {
                        //console.log('values', values)
                        return (
                          <Form>
                            {this.state.showModalLoader === true ? ( 
                                <div className="loading_reddy_outer">
                                    <div className="loading_reddy" >
                                        <img src={whitelogo} alt="loader"/>
                                    </div>
                                </div>
                              ) : ( "" )}
                            <Modal.Header closeButton>
                              <Modal.Title>
                                Update Customer
                              </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="contBox">
                              
                                <Row>
                                    <Col xs={12} sm={6} md={6}>
                                      <div className="form-group">
                                        <label>
                                          Parent Customer Name
                                        </label>

                                        <Select
                                            className="basic-single"
                                            classNamePrefix="select"
                                            isClearable={true}
                                            isSearchable={true}
                                            name="parent_company_name"
                                            options={this.state.master_list}
                                            defaultValue={values.parent_company_name}
                                            value={values.parent_company_name}
                                            onChange={e => { this.changeParentCompany(e,setFieldValue) }}                                            
                                          />

                                        {errors.parent_company_name && touched.parent_company_name ? (
                                          <span className="errorMsg">
                                            {errors.parent_company_name}
                                          </span>
                                        ) : null}
                                      </div>
                                    </Col>
                                  
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Customer Name<span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="child_company_name"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter company name"
                                        autoComplete="off"
                                        value={values.child_company_name}
                                      />
                                      {errors.child_company_name && touched.child_company_name ? (
                                        <span className="errorMsg">
                                          {errors.child_company_name}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>

                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group" onMouseLeave={(e)=>{
                                      if(this.state.buyer_sold_auto_suggest != ''){
                                        document.getElementById(`buyer_sold`).blur();
                                        this.setState({buyer_sold_auto_suggest:''});
                                      }
                                    }} >
                                      <label>
                                        SAP SoldTo Id's
                                        {/* <span className="required-field">*</span> */}
                                      </label>
                                      <Field
                                        name={`sap_ref_no`}
                                        type="text"
                                        id={`buyer_sold`}
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        placeholder="Enter SoldTo Id's"
                                        value={
                                          values.sap_ref_no !== null &&
                                          values.sap_ref_no !== ""
                                            ? values.sap_ref_no
                                            : ""
                                        }
                                        onChange={(e) => this.setSOLDTO(e, setFieldValue)}
                                        onFocus={(e)=>{
                                          if(values.sap_ref_no != '' && values.sap_ref_no.length > 2){
                                            // API.post(`/api/feed/find_sap_soldTo_admin`,{soldto_id:values.sap_ref_no,exclude:JSON.stringify(this.state.selected_sold_to.map(val=>{return val.value}))})
                                            API.post(`/api/feed/find_sap_soldTo_admin`,{soldto_id:values.sap_ref_no,exclude:JSON.stringify([])})
                                            .then((res) => {
                                              
                                              let ret_html = res.data.data.map((val,index)=>{
                                                return (<li style={{cursor:'pointer',marginTop:'6px'}} onClick={()=>this.setSOLDTOAutoSuggest(val, setFieldValue)} >{val.label}<hr style={{marginTop:'6px',marginBottom:'0px'}} /></li>)
                                              })
                                              this.setState({buyer_sold_auto_suggest:ret_html});
                                            })
                                            .catch((err) => {
                                              console.log(err);
                                            });
                                          }
                                        }}

                                      ></Field>
                                      {this.state.buyer_sold_auto_suggest != '' && <ul id={`material_id_auto_suggect`} style={{zIndex:'30',position:'absolute',backgroundColor:'#d0d0d0',height:"100px",overflowY:'auto',listStyleType:'none',width:'100%'}} >
                                        {this.state.buyer_sold_auto_suggest}
                                      </ul>}
                                    </div>

                                    {errors.sap_ref_no ? (
                                        <span className="errorMsg">
                                          {errors.sap_ref_no}
                                        </span>
                                      ) : null}

                                    {this.state.selected_sold_to.length > 0 && <div className="custom-file-upload-area">
                                      {this.generateSelectedSoldTo(setErrors)}
                                    </div>}
                                        
                                  </Col>
                                </Row>

                                {/* <Row>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>SAP SoldTo Id</label>
                                      <Field
                                        name="sap_ref_no"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter SAP SoldTo Id"
                                        autoComplete="off"
                                        value={values.sap_ref_no}
                                      />
                                      {errors.sap_ref_no ? (
                                        <span className="errorMsg">
                                          {errors.sap_ref_no}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  {values.sap_ref_no ? (
                                    <Col xs={12} sm={6} md={6}>
                                      <button
                                        className={`btn btn-default btn-sm`}
                                        type="button"
                                        onClick={e => this.populateSoldTo(e, values.sap_ref_no)}  
                                      >
                                      {Object.keys(this.state.soldtoDetails).length > 0 ? "Re-populate SAP SoldTo Details" : "Populate SAP SoldTo Details" }
                                      </button>
                                    </Col>
                                  ) : null }
                                </Row>
                                {this.state.soldtoDetails && this.state.showDetails===1 && this.makeDynamicHtml()}                                 
                                {errors.message ? (
                                    <Row>
                                      <Col xs={12} sm={12} md={12}>
                                        <span className="errorMsg">
                                          {errors.message}
                                        </span>
                                      </Col>
                                    </Row>
                                  ) : ( "" )
                                }                                */}
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <button
                                  className={`btn btn-success btn-sm ${isValid ? "btn-custom-green" : "btn-disable"} m-r-10`}
                                  type="submit"
                                  //disabled={isValid ? false : true}
                                >
                                  Update
                                </button>
                                <button
                                  onClick={e => this.closeEditModal()}
                                  className={`btn btn-danger btn-sm`}
                                  type="button"
                                >
                                  Close
                                </button>
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>

                  )
                }
                  

                  <Modal
                    show={this.state.showAddSalesModal}
                    onHide={() => this.closeAddSalesIdModal()} backdrop="static"
                  >
                    <Formik
                      initialValues={addSalesIdInitialValues}
                      validationSchema={validateAddSalesId}
                      onSubmit={this.handleAddSalesCompanyId}
                    >
                      {({ values, errors, touched, isValid, isSubmitting, setFieldValue}) => {
                        // console.log('values', values)
                        return (
                          <Form>
                            {this.state.showModalLoader === true ? ( 
                                <div className="loading_reddy_outer">
                                    <div className="loading_reddy" >
                                        <img src={whitelogo} alt="loader"/>
                                    </div>
                                </div>
                              ) : ( "" )}
                            <Modal.Header closeButton>
                              <Modal.Title>
                                Update Customers
                              </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="contBox">
                                <Row>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Sales Customer ID<span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="sales_customer_id"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter sales customer ID"
                                        autoComplete="off"
                                        value={values.sales_customer_id}
                                      />
                                      {errors.sales_customer_id && touched.sales_customer_id ? (
                                        <span className="errorMsg">
                                          {errors.sales_customer_id}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row> 
                                {errors.message ? (
                                    <Row>
                                      <Col xs={12} sm={12} md={12}>
                                        <span className="errorMsg">
                                          {errors.message}
                                        </span>
                                      </Col>
                                    </Row>
                                  ) : ( "" )
                                }                               
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <button
                                  className={`btn btn-success btn-sm ${isValid ? "btn-custom-green" : "btn-disable"} m-r-10`}
                                  type="submit"
                                  disabled={isValid ? false : true}
                                >
                                  Update
                                </button>
                                <button
                                  onClick={e => this.closeAddSalesIdModal()}
                                  className={`btn btn-danger btn-sm`}
                                  type="button"
                                >
                                  Close
                                </button>
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>

                  <Modal
                    show={this.state.showEditSalesModal}
                    onHide={() => this.closeEditSalesIdModal()} backdrop="static"
                  >
                    <Formik
                      initialValues={editSalesIdInitialValues}
                      validationSchema={validateAddSalesId}
                      onSubmit={this.handleEditSalesCompanyId}
                    >
                      {({ values, errors, touched, isValid, isSubmitting, setFieldValue}) => {
                        // console.log('values', values)
                        return (
                          <Form>
                            {this.state.showModalLoader === true ? ( 
                                <div className="loading_reddy_outer">
                                    <div className="loading_reddy" >
                                        <img src={whitelogo} alt="loader"/>
                                    </div>
                                </div>
                              ) : ( "" )}
                            <Modal.Header closeButton>
                              <Modal.Title>
                                Update Customer
                              </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="contBox">
                                <Row>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Sales Customer ID<span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="sales_customer_id"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter sales customer ID"
                                        autoComplete="off"
                                        value={values.sales_customer_id}
                                      />
                                      {errors.sales_customer_id && touched.sales_customer_id ? (
                                        <span className="errorMsg">
                                          {errors.sales_customer_id}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row> 
                                {errors.message ? (
                                    <Row>
                                      <Col xs={12} sm={12} md={12}>
                                        <span className="errorMsg">
                                          {errors.message}
                                        </span>
                                      </Col>
                                    </Row>
                                  ) : ( "" )
                                }                               
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <button
                                  className={`btn btn-success btn-sm ${isValid ? "btn-custom-green" : "btn-disable"} m-r-10`}
                                  type="submit"
                                  disabled={isValid ? false : true}
                                >
                                  Update
                                </button>
                                <button
                                  onClick={e => this.closeEditSalesIdModal()}
                                  className={`btn btn-danger btn-sm`}
                                  type="button"
                                >
                                  Close
                                </button>
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>

                  {/* ADD COMPANY - SATYAJIT */}

                  <Modal
                    show={this.state.addCustomerModal}
                    onHide={() => this.closeCustomerModal()} backdrop="static"
                  >
                    <Formik
                      initialValues={addInitialValues}
                      validationSchema={validateCompany}
                      onSubmit={this.handleAddCompany}
                    >
                      {({ values, errors, touched, isValid, isSubmitting, setFieldValue,setFieldTouched, setErrors}) => {
                        
                        return (
                          <Form>
                            {this.state.showModalLoader === true ? ( 
                                <div className="loading_reddy_outer">
                                    <div className="loading_reddy" >
                                        <img src={whitelogo} alt="loader"/>
                                    </div>
                                </div>
                              ) : ( "" )}
                            <Modal.Header closeButton>
                              <Modal.Title>
                                Add Customer
                              </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                            <div className="contBox">
                              
                              <Row>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Parent Customer Name
                                      </label>

                                      <Select
                                          className="basic-single"
                                          classNamePrefix="select"
                                          isClearable={true}
                                          isSearchable={true}
                                          name="parent_company_name"
                                          options={this.state.master_list}
                                          onChange={e => { this.changeParentCompany(e,setFieldValue) }}                                            
                                      />

                                      {errors.parent_company_name && touched.parent_company_name ? (
                                        <span className="errorMsg">
                                          {errors.parent_company_name}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                              {/* </Row> 
                              <Row> */}
                                <Col xs={12} sm={6} md={6}>
                                  <div className="form-group">
                                    <label>
                                      Customer Name<span className="impField">*</span>
                                    </label>
                                    <Field
                                      name="child_company_name"
                                      type="text"
                                      className={`form-control`}
                                      placeholder="Enter company name"
                                      autoComplete="off"
                                    />
                                    {errors.child_company_name && touched.child_company_name ? (
                                      <span className="errorMsg">
                                        {errors.child_company_name}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              </Row>

                              <Row>
                                <Col xs={12} sm={12} md={12}>
                                  <div className="form-group" onMouseLeave={(e)=>{
                                    if(this.state.buyer_sold_auto_suggest != ''){
                                      document.getElementById(`buyer_sold`).blur();
                                      this.setState({buyer_sold_auto_suggest:''});
                                    }
                                  }} >
                                    <label>
                                      SAP SoldTo Id's
                                      {/* <span className="required-field">*</span> */}
                                    </label>
                                    <Field
                                      name={`sap_ref_no`}
                                      type="text"
                                      id={`buyer_sold`}
                                      className={`selectArowGray form-control`}
                                      autoComplete="off"
                                      placeholder="Enter SoldTo Id's"
                                      value={
                                        values.sap_ref_no !== null &&
                                        values.sap_ref_no !== ""
                                          ? values.sap_ref_no
                                          : ""
                                      }
                                      onChange={(e) => this.setSOLDTO(e, setFieldValue)}
                                      onFocus={(e)=>{
                                        if(values.sap_ref_no != '' && values.sap_ref_no.length > 2){
                                          // API.post(`/api/feed/find_sap_soldTo_admin`,{soldto_id:values.sap_ref_no,exclude:JSON.stringify(this.state.selected_sold_to.map(val=>{return val.value}))})
                                          API.post(`/api/feed/find_sap_soldTo_admin`,{soldto_id:values.sap_ref_no,exclude:JSON.stringify([])})
                                          .then((res) => {
                                            
                                            let ret_html = res.data.data.map((val,index)=>{
                                              return (<li style={{cursor:'pointer',marginTop:'6px'}} onClick={()=>this.setSOLDTOAutoSuggest(val, setFieldValue)} >{val.label}<hr style={{marginTop:'6px',marginBottom:'0px'}} /></li>)
                                            })
                                            this.setState({buyer_sold_auto_suggest:ret_html});
                                          })
                                          .catch((err) => {
                                            console.log(err);
                                          });
                                        }
                                      }}

                                    ></Field>
                                    {this.state.buyer_sold_auto_suggest != '' && <ul id={`material_id_auto_suggect`} style={{zIndex:'30',position:'absolute',backgroundColor:'#d0d0d0',height:"100px",overflowY:'auto',listStyleType:'none',width:'100%'}} >
                                      {this.state.buyer_sold_auto_suggest}
                                    </ul>}
                                  </div>

                                  {errors.sap_ref_no ? (
                                        <span className="errorMsg">
                                          {errors.sap_ref_no}
                                        </span>
                                      ) : null}
                                      
                                  {this.state.selected_sold_to.length > 0 && <div className="custom-file-upload-area">
                                    {this.generateSelectedSoldTo(setErrors)}
                                  </div>}
                                      
                                </Col>
                              </Row>

                              {/* <Row>
                                <Col xs={12} sm={6} md={6}>
                                  <div className="form-group">
                                    <label>SAP SoldTo Id</label>
                                    <Field
                                      name="sap_ref_no"
                                      type="text"
                                      className={`form-control`}
                                      placeholder="Enter SAP SoldTo Id"
                                      autoComplete="off"
                                      value={this.state.addButtonid}
                                      onChange={e => this.handleChange(e)}
                                    />
                                    {errors.sap_ref_no && touched.sap_ref_no ? (
                                      <span className="errorMsg">
                                        {errors.sap_ref_no}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>                                
                                {this.state.addButtonid ? (    
                                  <Col xs={12} sm={6} md={6}>
                                    <button
                                      className={`btn btn-default btn-sm`}
                                      type="button"
                                      onClick={e => this.populateSoldTo(e, this.state.addButtonid)}  
                                    >
                                    Populate SAP SoldTo Details
                                    </button>
                                  </Col>
                                ) : null }                                
                              </Row> 
                              {this.state.soldtoDetails && this.state.showDetails===1 && this.makeDynamicHtml()}
                              {errors.message ? (
                                  <Row>
                                    <Col xs={12} sm={12} md={12}>
                                      <span className="errorMsg">
                                        {errors.message}
                                      </span>
                                    </Col>
                                  </Row>
                                ) : ( "" )
                              }                                */}
                            </div>
                            </Modal.Body>
                            <Modal.Footer>
                              
                              <button
                                  className={`btn btn-success btn-sm ${isValid ? "btn-custom-green" : "btn-disable"} m-r-10`}
                                  type="submit"
                                  disabled={isValid ? false : true}
                                >
                                  Add
                                </button>
                                <button
                                  onClick={e => this.closeCustomerModal()}
                                  className={`btn btn-danger btn-sm`}
                                  type="button"
                                >
                                  Close
                                </button>
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>

                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}

export default Company;