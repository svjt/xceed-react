import React, { Component } from 'react';
import Pagination from "react-js-pagination";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal
} from "react-bootstrap";
//import { Label } from 'reactstrap';
import { Link } from "react-router-dom";
import { Formik, Field, Form } from "formik";
import swal from "sweetalert";
import * as Yup from "yup";


import Layout from "../layout/Layout";
import whitelogo from '../../../assets/images/drreddylogo_white.png';
import API from "../../../shared/admin-axios";
import { showErrorMessage } from "../../../shared/handle_error";
import { htmlDecode, inArray } from '../../../shared/helper';

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

const actionFormatter = refObj => cell => {
  return (
    <div className="actionStyle">
      <LinkWithTooltip
        tooltip="Click to Edit"
        href="#"
        clicked={e => refObj.modalShowHandler(e, cell)}
        id="tooltip-1"
      >
        <i className="far fa-edit" />
      </LinkWithTooltip>
      <LinkWithTooltip
        tooltip="Click to Delete"
        href="#"
        clicked={e => refObj.confirmDelete(e, cell)}
        id="tooltip-1"
      >
        <i className="far fa-trash-alt" />
      </LinkWithTooltip>
    </div>
  );
};
;

const custContent = () => cell => {
  return htmlDecode(cell);
};

const initialValues = {
  group_name: ''
};

class Group extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      activePage: 1,
      totalCount: 0,
      itemPerPage: 20,
      groupDetails: [],
      groupFlagId: 0,
      selectStatus: [
        { id: "0", name: "Inactive" },
        { id: "1", name: "Active" }
      ],
      showModal: false,
      showModalLoader: false,
      search_name: ""
    };
  }

  componentDidMount() {
    this.getGroupList();
  }

  getGroupList(page = 1) {
    let name = this.state.search_name;

    API.get(`/api/adm_group?page=${page}&name=${encodeURIComponent(name)}`)
      .then(res => {
        this.setState({
          group: res.data.data,
          count: res.data.count_group,
          isLoading: false,
          search_name: name,
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        showErrorMessage(err, this.props);
      });
    this.setState({
      isLoading: false
    });
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getGroupList(pageNumber > 0 ? pageNumber : 1);
  };


  confirmDelete = (event, id) => {
    event.preventDefault();
    swal({
      closeOnClickOutside: false,
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this!",
      icon: "warning",
      buttons: true,
      dangerMode: true
    }).then(willDelete => {
      if (willDelete) {
        this.deleteGroup(id);
      }
    });
  };

  deleteGroup = id => {
    API.get(`/api/adm_group/cnt_grp_adm/${id}`)
      .then(res => {
        if (res.data.data > 0) {
          swal({
            icon: 'info',
            title: '',
            text: 'Admin exists in this group'
          });
        } else {
          API.delete(`/api/adm_group/${id}`).then(res => {
            swal({
              closeOnClickOutside: false,
              title: "Success",
              text: "Record deleted successfully.",
              icon: "success"
            }).then(() => {
              this.setState({ activePage: 1 });
              this.getGroupList(this.state.activePage);
            });
          })
            .catch(err => {
              if (err.data.status === 3) {
                this.setState({ closeModal: true });
                showErrorMessage(err, this.props);
              }
            });
        }
      })
      .catch(err => {
        showErrorMessage(err, this.props);
      });


    /* return false;
    API.delete(`/api/adm/${id}`).then(res => {
      swal({
        closeOnClickOutside: false,
        title: "Success",
        text: "Record deleted successfully.",
        icon: "success"
      }).then(() => {
        this.setState({ activePage: 1 });
        this.getAdminList(this.state.activePage);
      });
    })
    .catch(err => {          
      if(err.data.status === 3){
        this.setState({ closeModal: true });
        showErrorMessage(err,this.props);
      }
    }); */
  };

  modalShowHandler = (event, id) => {
    if (id) {
      event.preventDefault();
      API.get(`/api/adm_group/sections/${id}`)
        .then(res => {

          this.setState({
            sectionList: res.data.data.access_permission,
            groupName: res.data.data.group_name,
            groupFlagId: id,
            showModal: true
          });
        })
        .catch(err => {
          showErrorMessage(err, this.props);
        });
    } else {

      API.get("/api/adm_group/sections")
        .then(res => {
          this.setState({
            sectionList: res.data.data,
            groupName: '',
            groupFlagId: 0,
            showModal: true
          });
        })
        .catch(err => {
          showErrorMessage(err, this.props);
        });
    }
  };

  toggleChange = (event, value, index) => {
    let prevState = this.state.sectionList;

    let indexOF = prevState[index].checkBoxes.findIndex((person) => {
      return person.key == value
    });

    prevState[index].checkBoxes[indexOF].value = event.target.checked;

    //AUTO CHECK VIEW

    let indexOFView = prevState[index].checkBoxes.findIndex((person) => {
      return person.key == 'V'
    });
    let indexOFAdd = prevState[index].checkBoxes.findIndex((person) => {
      return person.key == 'A'
    });
    let indexOFEdit = prevState[index].checkBoxes.findIndex((person) => {
      return person.key == 'E'
    });
    let indexOFDelete = prevState[index].checkBoxes.findIndex((person) => {
      return person.key == 'D'
    });

    if (inArray(value, ['A', 'E', 'D'])) {
      if (prevState[index].checkBoxes[indexOFAdd].value === true || prevState[index].checkBoxes[indexOFEdit].value === true || prevState[index].checkBoxes[indexOFDelete].value === true) {
        prevState[index].checkBoxes[indexOFView].value = true;
      }
    }
    if (value == 'V') {
      if (prevState[index].checkBoxes[indexOFView].value === false) {
        prevState[index].checkBoxes[indexOFAdd].value = false;
        prevState[index].checkBoxes[indexOFEdit].value = false;
        prevState[index].checkBoxes[indexOFDelete].value = false;
      }
    }

    this.setState({ sectionList: prevState });

  }

  modalCloseHandler = () => {
    this.setState({ groupFlagId: 0 });
    this.setState({ showModal: false });
  };

  handleSubmitEvent = (values, actions) => {
    const post_data = {
      group_name: values.group_name,
      access_permission: JSON.stringify(this.state.sectionList)
    };

    if (this.state.groupFlagId) {
      this.setState({ showModalLoader: true });
      const id = this.state.groupFlagId;
      API.put(`/api/adm_group/${id}`, post_data)
        .then(res => {
          this.modalCloseHandler();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record updated successfully.",
            icon: "success"
          }).then(() => {
            this.setState({ showModalLoader: false });
            this.getGroupList(this.state.activePage);
          });
        })
        .catch(err => {
          this.setState({ showModalLoader: false });
          if (err.data.status === 3) {
            this.setState({
              showModal: false
            });
            showErrorMessage(err, this.props);
          } else {
            actions.setErrors(err.data.errors);
            actions.setSubmitting(false);
          }
        });
    } else {
      this.setState({ showModalLoader: true });
      API.post("/api/adm_group", post_data)
        .then(res => {
          this.modalCloseHandler();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record added successfully.",
            icon: "success"
          }).then(() => {
            this.setState({ activePage: 1, showModalLoader: false });
            this.getGroupList(this.state.activePage);
          });
        })
        .catch(err => {
          this.setState({ showModalLoader: false });
          if (err.data.status === 3) {
            this.setState({
              showModal: false
            });
            showErrorMessage(err, this.props);
          } else {
            actions.setErrors(err.data.errors);
            actions.setSubmitting(false);
          }
        });
    }
  };

  getHtml = () => {
    let html = [];

    for (let index = 0; index < this.state.sectionList.length; index++) {
      let checkbox = [];
      const element = this.state.sectionList[index];
      let checkbox_arr = element.access_param.split(',');

      for (let j = 0; j < checkbox_arr.length; j++) {
        const elementNew = checkbox_arr[j];
        let indexOF = element.checkBoxes.findIndex((person) => {
          return person.key == elementNew
        });
        checkbox.push(
          <div className="form-group checText" key={`${j}${index}`}>
            <input
              type="checkbox"
              name="view_region"
              value="1"
              checked={element.checkBoxes[indexOF].value}
              onChange={e => { this.toggleChange(e, elementNew, index) }}
            />
            <label>{elementNew === 'V' && element.view_ac_name}{elementNew === 'A' && element.add_ac_name}{elementNew === 'E' && element.edit_ac_name}{elementNew === 'D' && element.delete_ac_name}</label>
          </div>
        );

      }

      html.push(
        <Row key={index}>
          <Col xs={12} sm={12} md={12}>
            <div className="form-group listBox">
              <label className="listHead">
                {element.section_desc}
                <span className="impField">*</span>
              </label>
              {checkbox}
            </div>
          </Col>
        </Row>
      );
    }
    return html;
  }

  customSearch = (e) => {
    e.preventDefault();
    var name = document.getElementById('name').value;

    if (name === "") {
      return false;
    }

    API.get(`/api/adm_group?page=1&name=${encodeURIComponent(name)}`)
      .then(res => {
        this.setState({
          group: res.data.data,
          count: res.data.count_group,
          isLoading: false,
          search_name: name,
          remove_search: true,
          activePage: 1,
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        showErrorMessage(err, this.props);
      });
  }

  clearSearch = () => {
    document.getElementById('name').value = "";

    this.setState({
      search_name: "",
      remove_search: false
    }, () => {
      this.getGroupList();
      this.setState({ activePage: 1 })
    })
  }

  render() {
    const { groupName } = this.state;
    const newInitialValues = Object.assign(initialValues, {
      group_name: groupName ? htmlDecode(groupName) : ""
    });
    const validateStopFlag = Yup.object().shape({
      group_name: Yup.string()
        .min(2, 'Group name must be at least 2 characters')
        .max(200, 'Group name must be at most 200 characters')
        .required("Please enter group name")
    });

    if (this.state.isLoading === true) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy" >
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <h1>
                    Manage Group
                  <small />
                  </h1>
                </div>
                <div className="col-lg-12 col-sm-12 col-xs-12 topSearchSection">
                  <form className="form">
                    <div className="">
                      <input
                        className="form-control"
                        name="name"
                        id="name"
                        placeholder="Filter by name"
                      />
                    </div>

                    <div className="">
                      <input
                        type="submit"
                        value="Search"
                        className="btn btn-warning btn-sm"
                        onClick={(e) => this.customSearch(e)}
                      />
                      {this.state.remove_search ? <a onClick={() => this.clearSearch()} className="btn btn-danger btn-sm"> Remove </a> : null}
                    </div>
                    <div className="clearfix"></div>
                  </form>
                  <div className="">
                    <button
                      type="button"
                      className="btn btn-info btn-sm"
                      onClick={e => this.modalShowHandler(e, "")}
                    >
                      <i className="fas fa-plus m-r-5" /> Add Group
                    </button>
                  </div>
                  <div className="clearfix"></div>
                </div>
              </div>
            </section>
            <section className="content">
              <div className="box">
                <div className="box-body">
                  <BootstrapTable data={this.state.group}>
                    <TableHeaderColumn isKey dataField="group_name" dataFormat={custContent(this)}>
                      Name
                    </TableHeaderColumn>
                    {/* <TableHeaderColumn dataField="status" dataFormat={custStatus(this)}>
                      Status
                    </TableHeaderColumn> */}
                    {/* <TableHeaderColumn
                      dataField="display_add_date"
                      editable={false}
                      expandable={false}
                    >
                      Date Added
                    </TableHeaderColumn> */}
                    <TableHeaderColumn
                      dataField="group_id"
                      dataFormat={actionFormatter(this)}
                    >
                      Action
                    </TableHeaderColumn>
                  </BootstrapTable>

                  {this.state.count > this.state.itemPerPage ? (
                    <Row>
                      <Col md={12}>
                        <div className="paginationOuter text-right">
                          <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemPerPage}
                            totalItemsCount={this.state.count}
                            itemClass='nav-item'
                            linkClass='nav-link'
                            activeClass='active'
                            onChange={this.handlePageChange}
                          />
                        </div>
                      </Col>
                    </Row>
                  ) : null}

                  {/* ======= Add/Edit ======== */}
                  <Modal
                    show={this.state.showModal}
                    onHide={() => this.modalCloseHandler()} backdrop="static"
                  >
                    <Formik
                      initialValues={newInitialValues}
                      validationSchema={validateStopFlag}
                      onSubmit={this.handleSubmitEvent}
                    >
                      {({ values, errors, touched, isValid, isSubmitting }) => {
                        return (
                          <Form>
                            {this.state.showModalLoader === true ? (
                              <div className="loading_reddy_outer">
                                <div className="loading_reddy" >
                                  <img src={whitelogo} alt="loader" />
                                </div>
                              </div>
                            ) : ("")}
                            <Modal.Header closeButton>
                              <Modal.Title>
                                {this.state.groupFlagId > 0 ? "Edit" : "Add"}{" "}
                                Group
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="contBox listSection">
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>
                                        Group Name<span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="group_name"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter name"
                                        autoComplete="off"
                                        value={values.group_name}
                                      />
                                      {errors.group_name && touched.group_name ? (
                                        <span className="errorMsg">
                                          {errors.group_name}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                <div className="listBody">{this.state.sectionList && this.state.sectionList.length > 0 && this.getHtml()}</div>
                                {errors.message ? (
                                  <Row>
                                    <Col xs={12} sm={12} md={12}>
                                      <span className="errorMsg">
                                        {errors.message}
                                      </span>
                                    </Col>
                                  </Row>
                                ) : ("")
                                }
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                              <button
                                className={`btn btn-success btn-sm ${
                                  isValid ? "btn-custom-green" : "btn-disable"
                                  } mr-2`}
                                type="submit"
                                disabled={isValid ? false : false}
                              >
                                {this.state.groupFlagId > 0
                                  ? isSubmitting
                                    ? "Updating..."
                                    : "Update"
                                  : isSubmitting
                                    ? "Submitting..."
                                    : "Submit"}
                              </button>
                              <button
                                onClick={e => this.modalCloseHandler()}
                                className={`btn btn-danger btn-sm`}
                                type="button"
                              >
                                Close
                                  </button>
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>
                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}
export default Group;