import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import Loader from "react-loader-spinner";
import {
  Tooltip,
  OverlayTrigger
} from "react-bootstrap";
import { Link } from "react-router-dom";
import API from "../../../shared/admin-axios";
import Layout from "../layout/Layout";
//import DashboardSearch from "./DashboardSearch";

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const actionFormatter = refObj => cell => {
  return (
    <div className="actionStyle">
      <LinkWithTooltip
        tooltip="Click to View"
        href="#"
        clicked={e => refObj.assignedHandler(cell)}
        id="tooltip-1"
      >
        <i className="fas fa-arrows-alt" />
      </LinkWithTooltip>

      <LinkWithTooltip
        tooltip="Click to View"
        href="#"
        clicked={e => refObj.inProgressHandler(cell)}
        id="tooltip-1"
      >
        <i className="fas fa-bolt" />
      </LinkWithTooltip>

      <LinkWithTooltip
        tooltip="Click to View"
        href="#"
        clicked={e => refObj.closedHandler(cell)}
        id="tooltip-1"
      >
        <i className="fab fa-blackberry" />
      </LinkWithTooltip>      
    </div>
  );
};


class Assigned extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      admins: []
    };
  }

  componentDidMount() {
    this.getAdminList();
  }

  getAdminList() {
    API.get("/api/adm")
      .then(res => {
        this.setState({
          admins: res.data.data,
          isLoading: false
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  assignedHandler = (id) => {  
    this.props.history.push(`/admin/assigned/${id}`)
  };
  inProgressHandler = (id) => {  
    this.props.history.push(`/admin/assigned/${id}`)
  };
  closedHandler = (id) => {  
    this.props.history.push(`/admin/assigned/${id}`)
  };


  renderShowsTotal = (start, to, total) => {
    return (
      <span className="pageShow">
        Showing {start} to {to}, of {total} records
      </span>
    );
  };

  render() {
    const paginationOptions = {
      page: 1, // which page you want to show as default
      sizePerPageList: [
        {
          text: "5",
          value: 5
        },
        {
          text: "10",
          value: 10
        },
        {
          text: "All",
          value: this.state.admins.length > 0 ? this.state.admins.length : 1
        }
      ], // you can change the dropdown list for size per page
      sizePerPage: 5, // which size per page you want to locate as default
      pageStartIndex: 1, // where to start counting the pages
      paginationSize: 3, // the pagination bar size.
      prePage: "Prev", // Previous page button text
      nextPage: "Next", // Next page button text
      firstPage: "First", // First page button text
      lastPage: "Last", // Last page button text
      paginationShowsTotal: this.renderShowsTotal, // Accept bool or function
      paginationPosition: "bottom" // default is bottom, top and both is all available
      // hideSizePerPage: true //> You can hide the dropdown for sizePerPage
      // alwaysShowAllBtns: true // Always show next and previous button
      // withFirstAndLast: false //> Hide the going to First and Last page button
    };
    
    if (this.state.isLoading === true) {
      return (
        <div className="loading_reddy_outer">
          <div className="loading_reddy" >
            <Loader 
            type="Puff"
            color="#00BFFF"
            height="50" 
            width="50"
            verticalAlign="middle"
            />
          </div>
        </div>
      );
    } else {

      return (
        <Layout>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">
                <div className="col-lg-9 col-sm-6 col-xs-12">
                  <h1>
                    Assigned List
                    <small />
                  </h1>
                </div>
              </div>
            </section>
            {/* <DashboardSearch groupList={this.state.groupList} /> */}
            <section className="content">
              <div className="box">
                <div className="box-body">
                  <BootstrapTable
                    data={this.state.admins}
                    pagination={true}
                    options={paginationOptions}
                    striped={true}
                    hover={true}
                    ignoreSinglePage
                  >
                    <TableHeaderColumn isKey dataField="first_name">
                      First Name
                    </TableHeaderColumn>

                    <TableHeaderColumn dataField="last_name">
                      Last Name
                    </TableHeaderColumn>

                    <TableHeaderColumn dataField="email">
                      Email
                    </TableHeaderColumn>

                    <TableHeaderColumn dataField="group_name">
                      Group
                    </TableHeaderColumn>

                    <TableHeaderColumn
                      dataField="id"
                      dataFormat={actionFormatter(this)}
                      dataAlign=""
                    >
                      Action
                    </TableHeaderColumn>
                  </BootstrapTable>                  
                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}

export default Assigned;
