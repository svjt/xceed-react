import React, { Component } from 'react';
import { Row, Col, Modal, Alert } from 'react-bootstrap';
import { Formik, Field, Form } from 'formik';
import API from "../../../shared/admin-axios";
import * as Yup from 'yup';
import whitelogo from "../../../assets/images/drreddylogo_white.png";
import { showErrorMessage } from '../../../shared/handle_error';
import swal from "sweetalert";


const initialValues = {
    sap_ref_no: '',
};

class CustomerSoldToPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            company: [],
            company_list: [],
            soldtoDetails: [],
            showDetails: 0,
            showModalLoader: false,
            child_details: false,
            master_list: [],
            selectedParent: '',
            buyer_sold_auto_suggest: '',
            selected_sold_to: [],
        };
    }

    componentDidMount() {
        if(this.props.company_id){
            this.getCompany(this.props.company_id)
        }
    }

    
    // Auto Populate soldTo details
    getCompany = (company_name) => {
        this.setState({selected_sold_to:[]});
        API.get(`/api/company/child_details/${company_name}`)
        .then(res => {
              let setStateOBJ = {
                selected_sold_to:res.data.data
              };
    
            //   if(res.data.company_data && res.data.company_data.parent_id != null && res.data.company_data.parent_name != null){
            //     setStateOBJ.selectedParent = {
            //       value :  res.data.company_data.parent_id,
            //       label :  htmlDecode(res.data.company_data.parent_name)
            //     }
            //   }
              this.setState(setStateOBJ);
            //   this.setState(setStateOBJ,()=>{
            //     updateInitialValues.parent_company_name = (this.state.selectedParent && this.state.selectedParent.value) > 0 ? 
            //                                             this.state.selectedParent : '';
            //     updateInitialValues.child_company_name = res.data.company_data.child_name ? htmlDecode(res.data.company_data.child_name) : "";
    
            //   });
        })
        .catch(err => {
          console.log({err})
          if(err.data && err.data.status === 3){
            this.setState({ closeModal: true });
            showErrorMessage(err,this.props);
          }
        });
    }

    setSOLDTO = (event, setFieldValue) => {

        setFieldValue("sap_ref_no", event.target.value.toString());
        
        if(event.target.value && event.target.value.length > 2){
          // API.post(`/api/feed/find_sap_soldTo_employee`,{soldto_id:event.target.value,exclude:JSON.stringify(this.state.selected_sold_to.map(val=>{return val.value}))})
          API.post(`/api/feed/find_sap_soldTo_admin`,{soldto_id:event.target.value,exclude:JSON.stringify([])})
          .then((res) => {
    
            
            let ret_html = res.data.data.map((val,index)=>{
              return (<li style={{cursor:'pointer',marginTop:'6px'}} onClick={()=>this.setSOLDTOAutoSuggest(val,setFieldValue)} >{val.label}<hr style={{marginTop:'6px',marginBottom:'0px'}} /></li>)
            })
            this.setState({buyer_sold_auto_suggest:ret_html});
    
    
    
          })
          .catch((err) => {
            console.log(err);
          });
        }
    };
    
    setSOLDTOAutoSuggest = (event, setFieldValue) => {
    
        setFieldValue("sap_ref_no", '');
        this.setState({buyer_sold_auto_suggest:'' });
    
        let prev_state = this.state.selected_sold_to;
        const soldTo_exists = prev_state.some(item => item.value == event.value);
        // console.log("soldto",event.value);
        // console.log("customer",event.customer);
        if(!soldTo_exists){
        //   console.log("soldTo_exists",soldTo_exists);
          API.get(`/api/company/soldto_details/${event.value}/${event.customer}`)
          .then((res) => {
      
            event.ship_to_data = res.data.data;
      
            prev_state = [event, ...prev_state]; // Add to the top
            // prev_state.push(event); // Add to the bottom
      
            this.setState({selected_sold_to:prev_state});
            
          })
          .catch((err) => {
            console.log(err);
          });
        }
    };

    handleAddCompanySoldTo = ( values, action ) => {

        let sap_ref_no_arr = this.state.selected_sold_to.map((val)=>{return val.value});
        let customer_id_arr = this.state.selected_sold_to.map((val)=>{return val.ship_to_data.customer_id});
        const { company_name, company_id } = this.props;

        this.setState({ showModalLoader : true })
        let postData = {
        //   parent_id: values.parent_company_name.value,
          company_name : company_name,
          //sap_ref_no : values.sap_ref_no,
          //sap_customer_id : this.state.soldtoDetails.customer_id,
          sap_customer_id : customer_id_arr.join(','),
          sap_ref_no_arr  : sap_ref_no_arr.join(',')
        }
    
        API.put(`/api/company/child/${company_id}`, postData)
        .then(res => {
          this.setState({ showEditModal: false, showModalLoader: false });
          swal({
            closeOnClickOutside: false,
            title:"Success",
            text:"",
            icon: "success"
          }).then(() => {
            this.props.handleCloseCustomerSoldToPopup();
          });
        })
        .catch(err => {
            action.setSubmitting(false);
            this.setState({ showModalLoader: false });
            if(err.data.status === 3){
                showErrorMessage(err,this.props);
            }else{
                action.setErrors(err.data.errors)
                console.log("errors",err.data.errors);
            }
        });
    }

    generateSelectedSoldTo = (setErrors) => {
        let fileListHtml = "";
        if (this.state.selected_sold_to && this.state.selected_sold_to.length > 0) {
          fileListHtml = this.state.selected_sold_to.map((sold_to) => (
            <Alert key={sold_to.value}>
              <span onClick={() => {setErrors({}); this.removeSoldTo(sold_to);}}>
                <i
                  className="far fa-times-circle"
                  style={{ cursor: "pointer" }}
                ></i>
              </span>{" "}
              {sold_to.label}
              {sold_to.ship_to_data ? (
                <>
                <Row>   
                    <Col xs={12} sm={12} md={12}>
                    <div className="form-group">
                        <label>
                        Country Name
                        </label>
                        <Field                  
                        type="text"
                        className={`form-control`}
                        placeholder="Country Name"
                        autoComplete="off"
                        disabled                  
                        value={sold_to.ship_to_data.cty_name}
                        />
                    </div>
                    <Field                  
                        type="hidden"
                        name="sap_customer_id"
                        className={`form-control`}                    
                        autoComplete="off"
                        disabled                  
                        value={sold_to.ship_to_data.customer_id}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={6} md={6}>
                    <div className="form-group">
                        <label>
                        City
                        </label>
                        <Field                  
                        type="text"
                        className={`form-control`}
                        placeholder="City"
                        autoComplete="off"
                        disabled                  
                        value={sold_to.ship_to_data.city}
                        />
                    </div>
                    </Col>            
                    <Col xs={12} sm={6} md={6}>
                    <div className="form-group">
                        <label>
                        Postal Code
                        </label>
                        <Field                  
                        type="text"
                        className={`form-control`}
                        placeholder=" Postal Code"
                        autoComplete="off"
                        disabled                  
                        value={sold_to.ship_to_data.postalcode}
                        />
                    </div>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={12}>
                    <div className="form-group">
                        <label>
                        Street
                        </label>
                        <Field                  
                        type="text"
                        className={`form-control`}
                        placeholder="Street"
                        autoComplete="off"
                        disabled                  
                        value={sold_to.ship_to_data.street}
                        />
                    </div>
                    </Col> 
                </Row>
                </>
            ) : ( 
                <Row>
                <Col xs={12} sm={6} md={6}>
                    <div className="form-group">
                    <p>No Such data found</p>
                    </div>
                </Col>
                </Row> 
            )}
            </Alert>
          ));
        }
    
        return fileListHtml;
    }
    
    removeSoldTo = (soldTo) => {
        let prevState = this.state.selected_sold_to;
        let newState = [];
        for (let index = 0; index < prevState.length; index++) {
          const element = prevState[index];
          // console.log("soldTo",soldTo)
          // console.log("value",soldTo.value,element.value)
          // console.log("customer",soldTo.customer,element.customer)
          if (soldTo.value !== element.value || soldTo.customer !== element.customer) {
            newState.push(element);
          }
        }
    
        this.setState({ selected_sold_to: newState });
    };


    render() {

        let validateCompanySoldTo = Yup.object().shape({
            // sap_ref_no: Yup.string(),
        });

        return (
            <>
                <Modal 
                    show={this.props.showCustomerSoldToPopup} 
                    onHide={this.props.handleCloseCustomerSoldToPopup} 
                    backdrop='static'
                >
                    <Formik
                        initialValues={initialValues}
                        validationSchema={validateCompanySoldTo}
                        onSubmit={this.handleAddCompanySoldTo}
                    >
                        {({
                            values,
                            errors,
                            touched,
                            isSubmitting,
                            setFieldValue,
                            setFieldTouched,
                            setErrors,
                        }) => {
                            return (
                                <Form>
                                    {/* {console.log({errors})} */}
                                    {/* {console.log("values",this.state.selected_sold_to)} */}
                                    {this.state.showModalLoader && (
                                        <div className='loderOuter'>
                                            <div className="loading_reddy_outer">
                                                <div className="loading_reddy" >
                                                    <img src={whitelogo} alt="loader"/>
                                                </div>
                                            </div>
                                        </div>
                                    )}
                                    <Modal.Header closeButton>
                                        <Modal.Title>Add Sold To</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>
                                        <div className='contBox taskcontBox'>
                                            <Row>
                                                <Col xs={12} sm={12} md={12}>
                                                <div className="form-group" onMouseLeave={(e)=>{
                                                    if(this.state.buyer_sold_auto_suggest != ''){
                                                        document.getElementById(`buyer_sold`).blur();
                                                        this.setState({buyer_sold_auto_suggest:''});
                                                    }
                                                }} >
                                                    <label>
                                                        SAP SoldTo Id's
                                                        {/* <span className="required-field">*</span> */}
                                                    </label>
                                                    <Field
                                                        name={`sap_ref_no`}
                                                        type="text"
                                                        id={`buyer_sold`}
                                                        className={`selectArowGray form-control`}
                                                        autoComplete="off"
                                                        placeholder="Enter SoldTo Id's"
                                                        value={
                                                            values.sap_ref_no !== null &&
                                                            values.sap_ref_no !== ""
                                                            ? values.sap_ref_no
                                                            : ""
                                                        }
                                                        onChange={(e) => this.setSOLDTO(e, setFieldValue)}
                                                        onFocus={(e)=>{
                                                            if(values.sap_ref_no != '' && values.sap_ref_no.length > 2){
                                                                // API.post(`/api/feed/find_sap_soldTo_employee`,{soldto_id:values.sap_ref_no,exclude:JSON.stringify(this.state.selected_sold_to.map(val=>{return val.value}))})
                                                                API.post(`/api/feed/find_sap_soldTo_employee`,{soldto_id:values.sap_ref_no,exclude:JSON.stringify([])})
                                                                .then((res) => {
                                                                    
                                                                    let ret_html = res.data.data.map((val,index)=>{
                                                                    return (<li style={{cursor:'pointer',marginTop:'6px'}} onClick={()=>this.setSOLDTOAutoSuggest(val, setFieldValue)} >{val.label}<hr style={{marginTop:'6px',marginBottom:'0px'}} /></li>)
                                                                    })
                                                                    this.setState({buyer_sold_auto_suggest:ret_html});
                                                                })
                                                                .catch((err) => {
                                                                    console.log(err);
                                                                });
                                                            }
                                                        }}
                                                    ></Field>
                                                    {this.state.buyer_sold_auto_suggest != '' && <ul id={`material_id_auto_suggect`} style={{zIndex:'30',position:'absolute',backgroundColor:'#d0d0d0',height:"100px",overflowY:'auto',listStyleType:'none',width:'100%'}} >
                                                    {this.state.buyer_sold_auto_suggest}
                                                    </ul>}
                                                </div>
                                                {errors.sap_ref_no ? (
                                                        <span className="errorMsg">
                                                        {errors.sap_ref_no}
                                                        </span>
                                                    ) : null}
                                                    
                                                {this.state.selected_sold_to.length > 0 && <div className="custom-file-upload-area">
                                                    {this.generateSelectedSoldTo(setErrors)}
                                                </div>}
                                                </Col>
                                            </Row>
                                        </div>
                                    </Modal.Body>
                                    <Modal.Footer>
                                        <button
                                            className={`btn btn-success btn-sm btn-custom-green m-r-10`}
                                            type='submit'
                                        >
                                            {isSubmitting ? 'Submitting...' : 'Submit'}
                                        </button>
                                        <button 
                                            className={`btn btn-danger btn-sm`} 
                                            onClick={this.props.handleCloseCustomerSoldToPopup} 
                                            type='button'
                                        >
                                            Close
                                        </button>
                                    </Modal.Footer>
                                </Form>
                            );
                        }}
                    </Formik>
                </Modal>
            </>
        );
    }
}

export default CustomerSoldToPopup;
