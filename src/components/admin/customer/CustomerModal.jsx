
import React, { Component } from 'react';

class CustomerModal extends Component {

  
 

  render() {
    return (
      <Modal
      show={this.props.showModal}
      onHide={() => this.props.modalCloseHandler()}
    >
      <Formik
        initialValues={this.props.newInitialValues}
        validationSchema={validateStopFlag}
        onSubmit={this.handleSubmitEvent}
      >
        {({
          values,
          errors,
          touched,
          isValid,
          isSubmitting,
          setFieldValue,
          setFieldTouched
        }) => {                        
          return (                          
            <Form>
              <Modal.Header closeButton>
                <Modal.Title>
                  {this.state.customerflagId > 0 ? "Edit" : "Add"} Customer
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <div className="contBox">
                  <Row>
                    <Col xs={12} sm={6} md={6}>
                      <div className="form-group">
                        <label>
                          First Name
                          <span className="impField">*</span>
                        </label>
                        <Field
                          name="first_name"
                          type="text"
                          className={`form-control`}
                          placeholder="Enter first name"
                          autoComplete="off"
                        />
                        {errors.first_name &&
                        touched.first_name ? (
                          <span className="errorMsg">
                            {errors.first_name}
                          </span>
                        ) : null}
                      </div>
                    </Col>
                    <Col xs={12} sm={6} md={6}>
                      <div className="form-group">
                        <label>
                          Last Name
                          <span className="impField">*</span>
                        </label>
                        <Field
                          name="last_name"
                          type="text"
                          className={`form-control`}
                          placeholder="Enter last name"
                          autoComplete="off"
                        />
                        {errors.last_name && touched.last_name ? (
                          <span className="errorMsg">
                            {errors.last_name}
                          </span>
                        ) : null}
                      </div>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={6} md={6}>
                      <div className="form-group">
                        <label>
                          Email
                          <span className="impField">*</span>
                        </label>
                        <Field
                          name="email"
                          type="text"
                          className={`form-control`}
                          placeholder="Enter customer reference"
                          autoComplete="off"
                        />
                        {errors.email && touched.email ? (
                          <span className="errorMsg">
                            {errors.email}
                          </span>
                        ) : null}
                      </div>
                    </Col>
                    <Col xs={12} sm={6} md={6}>
                      <div className="form-group">
                        <label>
                          Phone Number
                          <span className="impField">*</span>
                        </label>
                        <Field
                          name="phone_no"
                          type="text"
                          className={`form-control`}
                          placeholder="Enter customer reference"
                          autoComplete="off"
                        />
                        {errors.phone_no && touched.phone_no ? (
                          <span className="errorMsg">
                            {errors.phone_no}
                          </span>
                        ) : null}
                      </div>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={6} md={6}>
                      <div className="form-group">
                        <label>
                          Country
                          <span className="impField">*</span>
                        </label>

                        <Field
                          name="country_id"
                          component="select"
                          className={`selectArowGray form-control`}
                          autoComplete="off"
                          value={values.country_id}
                        >
                          <option key="-1" value="">
                            Select
                          </option>
                          {this.state.countryList.map(
                            (country, i) => (
                              <option
                                key={i}
                                value={country.country_id}
                              >
                                {country.country_name}
                              </option>
                            )
                          )}
                        </Field>
                        {errors.country && touched.country ? (
                          <span className="errorMsg">
                            {errors.country}
                          </span>
                        ) : null}
                      </div>
                    </Col>
                    <Col xs={12} sm={6} md={6}>
                      <div className="form-group">
                        <label>
                          Vip Customer
                          <span className="impField">*</span>
                        </label>
                        <Field
                          name="vip_customer"
                          component="select"
                          className={`selectArowGray form-control`}
                          autoComplete="off"
                          value={values.vip_customer}
                        >
                          <option key="-1" value="">
                            Select
                          </option>
                          {this.state.vipStat.map(
                            (status, i) => (
                              <option key={i} value={status.id}>
                                {status.name}
                              </option>
                            )
                          )}
                        </Field>
                        {errors.vip_customer &&
                        touched.vip_customer ? (
                          <span className="errorMsg">
                            {errors.vip_customer}
                          </span>
                        ) : null}
                      </div>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} sm={6} md={6}>
                      <div className="form-group">
                        <label>
                          Status
                          <span className="impField">*</span>
                        </label>
                        <Field
                          name="status"
                          component="select"
                          className={`selectArowGray form-control`}
                          autoComplete="off"
                          value={values.status}
                        >
                          <option key="-1" value="">
                            Select
                          </option>
                          {this.state.selectStatus.map(
                            (status, i) => (
                              <option key={i} value={status.id}>
                                {status.name}
                              </option>
                            )
                          )}
                        </Field>
                        {errors.status && touched.status ? (
                          <span className="errorMsg">
                            {errors.status}
                          </span>
                        ) : null}
                      </div>
                    </Col>
                    </Row>
                    <Row>
                      <Col xs={12} sm={12} md={12}>
                        <div className="form-group">
                          <label>Team</label>

                          <Select
                            isMulti
                            name="teams[]"
                            options={this.state.teamList}
                            className="basic-multi-select"
                            classNamePrefix="select"
                            onChange={evt =>
                              setFieldValue(
                                "teams",
                                [].slice
                                  .call(evt)
                                  .map(val => val.value)
                              )
                            }
                            placeholder="Teams"
                            onBlur={() => setFieldTouched("teams")}
                            defaultValue={
                              this.state.selectedTeamList
                            }
                          />
                          {errors.teams && touched.teams ? (
                            <span className="errorMsg">
                              {errors.teams}
                            </span>
                          ) : null}
                        </div>
                      </Col>
                    </Row>
                  
                </div>
              </Modal.Body>
              <Modal.Footer>
               
                  <button
                    className={`btn btn-success btn-sm ${
                      isValid ? "btn-custom-green" : "btn-disable"
                    } m-r-10`}
                    type="submit"
                    disabled={isValid ? false : true}
                  >
                    {this.state.customerflagId > 0
                      ? isSubmitting
                        ? "Updating..."
                        : "Update"
                      : isSubmitting
                      ? "Submitting..."
                      : "Submit"}
                  </button>
                  <button
                    onClick={e => this.modalCloseHandler()}
                    className={`btn btn-danger btn-sm`}
                    type="button"
                  >
                    Close
                  </button>
              </Modal.Footer>
            </Form>
          );
        }}
      </Formik>
    </Modal>
    )
  }



}