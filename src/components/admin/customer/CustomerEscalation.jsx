import React, { Component } from "react";
import Pagination from "react-js-pagination";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import { Formik, Field, Form } from "formik";
import swal from "sweetalert";

import Layout from "../layout/Layout";
import whitelogo from "../../../assets/images/drreddylogo_white.png";
import API from "../../../shared/admin-axios";
import { showErrorMessage } from "../../../shared/handle_error";
import { htmlDecode } from "../../../shared/helper";
import { getSuperAdmin, getAdminGroup } from "../../../shared/helper";
import ReactHtmlParser from "react-html-parser";

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

const actionFormatter = (refObj) => (cell) => {
  //console.log(refObj.state.access)
  return (
    <div className="actionStyle">
      {refObj.state.access.view === true ? (
        <LinkWithTooltip
          tooltip="Click to view"
          href="#"
          clicked={(e) => refObj.modalShowHandler(e, cell)}
          id="tooltip-1"
        >
          <i className="fas fa-eye" />
        </LinkWithTooltip>
      ) : null}
    </div>
  );
};
const custName = () => (cell, row) => {
  if (row.user_type === 1) {
    return `${htmlDecode(row.cust_first_name)} ${htmlDecode(row.cust_last_name)}`;
  } else {
    return `${htmlDecode(row.agt_first_name)} ${htmlDecode(row.agt_last_name)}`;
  }
};

const custContent = () => (cell) => {
  return htmlDecode(cell);
};

class CustomerEscalation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      get_access_data: false,
      activePage: 1,
      totalCount: 0,
      itemPerPage: 20,
      custEscalationDetails: [],
      emailflagId: 0,
      showModal: false,
      showModalLoader: false,
      request_type: [],
      search_task_ref: "",
      search_cust_name: "",
      search_request_type: ""

    };
  }

  componentDidMount() {
    const superAdmin = getSuperAdmin(localStorage.admin_token);

    if (superAdmin === 1) {
      this.setState({
        access: {
          view: true,
          add: true,
          edit: true,
          delete: true,
        },
        get_access_data: true,
      });
      this.getCustEscalationList();
      this.getServiceRequestList();
    } else {
      const adminGroup = getAdminGroup(localStorage.admin_token);
      API.get(
        `/api/adm_group/single_access/${adminGroup}/${"EMAIL_LOG_MANAGEMENT"}`
      )
        .then((res) => {
          this.setState({
            access: res.data.data,
            get_access_data: true,
          });

          if (res.data.data.view === true) {
            this.getCustEscalationList();
          } else {
            this.props.history.push("/admin/dashboard");
          }
        })
        .catch((err) => {
          showErrorMessage(err, this.props);
        });
    }
  }

  getCustEscalationList(page = 1) {
    let task_ref = this.state.search_task_ref;
    let cust_name = this.state.search_cust_name;
    let request_type = this.state.search_request_type;

    API.get(`/api/agents/customer_escalation?page=${page}&task_ref=${encodeURIComponent(task_ref)}&cust_name=${encodeURIComponent(cust_name)}&request_type=${encodeURIComponent(request_type)}`)
      .then((res) => {
        this.setState({
          custEscalation: res.data.data,
          count: res.data.count_cust_escalation,
          isLoading: false,
          search_task_ref: task_ref,
          search_cust_name: cust_name,
          search_request_type: request_type,
        });
      })
      .catch((err) => {
        this.setState({
          isLoading: false,
        });
        showErrorMessage(err, this.props);
      });
    this.setState({
      isLoading: false,
    });
  }

  getServiceRequestList() {
    API.get(`/api/feed/request_type_adm`).then((res) => {
      var all_request_type = [];
      for (let index = 0; index < res.data.data.length; index++) {
        const element = res.data.data[index];
        all_request_type.push(element);
      }
      this.setState({
        request_type: all_request_type,
        isLoading: false,
      });
    });
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getCustEscalationList(pageNumber > 0 ? pageNumber : 1);
  };

  modalShowHandler = (event, id) => {
    if (id) {
      event.preventDefault();
      API.get(`/api/agents/view_customer_escalation/${id}`)
        .then((res) => {
          this.setState({
            custEscalationDetails: res.data.data,
            emailflagId: id,
            isLoading: false,
            showModal: true,
          });
        })
        .catch((err) => {
          showErrorMessage(err, this.props);
        });
    }
  };

  modalCloseHandler = () => {
    this.setState({ emailflagId: 0 });
    this.setState({ showModal: false });
  };

  custSearch = (e) => {
    e.preventDefault();
    var task_ref = document.getElementById('task_ref').value;
    var cust_name = document.getElementById('cust_name').value;
    var request_type = document.getElementById('request_type').value;

    if (task_ref === "" && cust_name === "" && request_type === "") {
      return false;
    }

    API.get(`/api/agents/customer_escalation?page=1&task_ref=${encodeURIComponent(task_ref)}&cust_name=${encodeURIComponent(cust_name)}&request_type=${encodeURIComponent(request_type)}`)
      .then(res => {
        this.setState({
          custEscalation: res.data.data,
          count: res.data.count_cust_escalation,
          isLoading: false,
          search_task_ref: task_ref,
          search_cust_name: cust_name,
          search_request_type: request_type,
          remove_search: true,
          activePage: 1,
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        showErrorMessage(err, this.props);
      });
  }

  clearSearch = () => {
    document.getElementById('task_ref').value = "";
    document.getElementById('cust_name').value = "";
    document.getElementById('request_type').value = "";

    this.setState({
      search_task_ref: "",
      search_cust_name: "",
      search_request_type: "",
      remove_search: false
    }, () => {
      this.getCustEscalationList();
      this.setState({ activePage: 1 })
    })
  }

  render() {
    const { custEscalationDetails } = this.state;

    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy">
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <h1>
                    Customer Escalation
                    <small />
                  </h1>
                </div>
                <div className="col-lg-12 col-sm-12 col-xs-12 topSearchSection">
                  <form className="form">
                    <div className="">
                      <input
                        className="form-control"
                        name="task_ref"
                        id="task_ref"
                        placeholder="Filter by Task Ref"
                      />
                    </div>

                    <div className="">
                      <input
                        className="form-control"
                        name="cust_name"
                        id="cust_name"
                        placeholder="Filter by customer name"
                      />
                    </div>

                    <div>
                      <select
                        name="request_type"
                        id="request_type"
                        className="form-control"
                      >
                        <option value="">Select Request</option>
                        {this.state.request_type.map((request, i) => (
                          <option key={i} value={request.type_id}>
                            {request.req_name}
                          </option>
                        ))}
                      </select>
                    </div>

                    <div className="">
                      <input
                        type="submit"
                        value="Search"
                        className="btn btn-warning btn-sm"
                        onClick={(e) => this.custSearch(e)}
                      />
                      {this.state.remove_search ? <a onClick={() => this.clearSearch()} className="btn btn-danger btn-sm"> Remove </a> : null}
                    </div>
                    <div className="clearfix"></div>
                  </form>
                  {/*  */}
                  <div className="clearfix"></div>

                </div>
              </div>
            </section>
            <section className="content">
              <div className="box">
                <div className="box-body">
                  <BootstrapTable data={this.state.custEscalation}>
                    <TableHeaderColumn
                      isKey
                      dataField="task_ref"
                      dataFormat={custContent(this)}
                    >
                      Task Ref
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="name"
                      dataFormat={custName(this)}
                    >
                      Customer Name
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="req_name"
                      dataFormat={custContent(this)}
                    >
                      Request Type
                    </TableHeaderColumn>

                    {/* <TableHeaderColumn
                      dataField="content"
                      width="30%"
                      dataFormat={custContent(this)}
                    >
                      Content
                    </TableHeaderColumn> */}
                    <TableHeaderColumn dataField="display_date_added">
                      Added On
                    </TableHeaderColumn>
                    {this.state.access.view === true ? (
                      <TableHeaderColumn
                        dataField="id"
                        width="10%"
                        dataFormat={actionFormatter(this)}
                      >
                        Action
                      </TableHeaderColumn>
                    ) : null}
                  </BootstrapTable>

                  {this.state.count > this.state.itemPerPage ? (
                    <Row>
                      <Col md={12}>
                        <div className="paginationOuter text-right">
                          <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemPerPage}
                            totalItemsCount={this.state.count}
                            itemClass="nav-item"
                            linkClass="nav-link"
                            activeClass="active"
                            onChange={this.handlePageChange}
                          />
                        </div>
                      </Col>
                    </Row>
                  ) : null}

                  {/* ======= Add/Edit ======== */}
                  <Modal
                    show={this.state.showModal}
                    onHide={() => this.modalCloseHandler()}
                    backdrop="static"
                  >
                    <Formik>
                      {() => {
                        return (
                          <Form>
                            {this.state.showModalLoader === true ? (
                              <div className="loading_reddy_outer">
                                <div className="loading_reddy">
                                  <img src={whitelogo} alt="loader" />
                                </div>
                              </div>
                            ) : (
                                ""
                              )}
                            <Modal.Header closeButton>
                              <Modal.Title>View Content</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="contBox">
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>Content</label>
                                      <div>
                                        {ReactHtmlParser(htmlDecode(this.state.custEscalationDetails[0].content))}
                                      </div>
                                    </div>
                                  </Col>
                                </Row>
                                {this.state.custEscalationDetails[0].actual_file_name !== '' && this.state.custEscalationDetails[0].actual_file_name !== null ?
                                  <Row>
                                    <label>Attachment </label>
                                    {this.state.custEscalationDetails.map((escalation, key) => (
                                      <tr key={key}>
                                        <Col xs={12} sm={12} md={12} key={key} >

                                          <div>{escalation.actual_file_name}</div>
                                          {/* <i class="fas fa-arrow-down"></i>{` `} */}
                                        </Col>
                                      </tr>
                                    ))}
                                  </Row>
                                  : null}
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                              <button
                                onClick={(e) => this.modalCloseHandler()}
                                className={`btn btn-danger btn-sm`}
                                type="button"
                              >
                                Close
                              </button>
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>
                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}
export default CustomerEscalation;
