import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import Autosuggest from "react-autosuggest";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import API from "../../../shared/admin-axios";
import { Formik, Field, Form } from "formik"; // for add/edit only
import * as Yup from "yup"; // for add/edit only
import swal from "sweetalert";
import Select from "react-select";

import Layout from "../layout/Layout";
import { htmlDecode } from "../../../shared/helper";
import whitelogo from "../../../assets/images/drreddylogo_white.png";

import exclamationImage from '../../../assets/images/exclamation-icon-black.svg';
import Pagination from "react-js-pagination";
import { showErrorMessage } from "../../../shared/handle_error";
import dateFormat from "dateformat";
import { getSuperAdmin, getAdminGroup } from "../../../shared/helper";
import CustomerSoldToPopup from './CustomerSoldToPopup';

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const actionFormatter = (refObj) => (cell, row) => {
  // var html = [];

  // html.push(<LinkWithTooltip
  //   tooltip="Click to Edit"
  //   href="#"
  //   clicked={e => refObj.modalShowHandler(e, cell)}
  //   id="tooltip-1"
  // >
  //   <i className="far fa-edit" />
  // </LinkWithTooltip>);

  // html.push(<LinkWithTooltip
  //   tooltip="Click to Delete"
  //   href="#"
  //   clicked={e => refObj.confirmDelete(e, cell)}
  //   id="tooltip-1"
  // >
  //   <i className="far fa-trash-alt" />
  // </LinkWithTooltip>);

  // if(row.status === 1 && row.pass_update_date === null){
  //   html.push(<LinkWithTooltip
  //         tooltip="Click to Send Mail"
  //         href="#"
  //         clicked={e => refObj.sendPassword(e, cell)}
  //         id="tooltip-1"
  //       >
  //         <i className="far fa-envelope" />
  //   </LinkWithTooltip>);
  // }

  return (
    <div className="actionStyle">
      {refObj.state.access.edit === true ? (
        <LinkWithTooltip
          tooltip="Click to Edit"
          href="#"
          clicked={(e) => refObj.modalShowHandler(e, cell)}
          id="tooltip-1"
        >
          <i className="far fa-edit" />
        </LinkWithTooltip>
      ) : null}
      {refObj.state.access.delete === true ? (
        <LinkWithTooltip
          tooltip="Click to Delete"
          href="#"
          clicked={(e) => refObj.confirmDelete(e, cell)}
          id="tooltip-1"
        >
          <i className="far fa-trash-alt" />
        </LinkWithTooltip>
      ) : null}

      {row.status === 1 &&
        row.pass_update_date === null &&
        refObj.state.access.edit === true ? (
        <LinkWithTooltip
          tooltip="Click to Send Mail"
          href="#"
          clicked={(e) => refObj.sendPassword(e, cell)}
          id="tooltip-1"
        >
          <i className="far fa-envelope" />
        </LinkWithTooltip>
      ) : null}

      {row.show_icon ? (
        <LinkWithTooltip
          tooltip="Check team approval"
          href="#"
          clicked={(e) => refObj.getTeam(e, cell)}
          id="tooltip-1"
        >
          <i className="fas fa-user" />
        </LinkWithTooltip>
      ) : null}
    </div>
  );
};

const vipStatus = (refObj) => (cell) => {
  return cell === 1 ? "Yes" : "No";
};

const __htmlDecode = (refObj) => (cell) => {
  return htmlDecode(cell);
};

const __salesCompany = (refObj) => (cell, row) => {
  if (row.sales_company_id === null && (row.status == 1 || row.status == 3)) {
    //let return_str = [];
    //return_str.push(`${htmlDecode(cell)}<br>`);
    // return_str.push(<LinkWithTooltip
    //   tooltip="Company Not Registered In SF"
    //   href="#"
    //   clicked={e => refObj.modalShowHandler(e, cell)}
    //   id="tooltip-1"
    // >
    //   <i className="fa fa-exclamation " />
    // </LinkWithTooltip>);

    //let return_str = `${cell}`;

    return (
      <>
        {htmlDecode(cell)}{" "}
        <span style={{ float: 'right' }} >
          <LinkWithTooltip
            tooltip="Company Not Registered In SF"
            href="#"
            clicked={(e) => refObj.modalShowHandler(e, cell)}
            id="tooltip-1"
          >
            <img src={exclamationImage} alt="exclamation" />
          </LinkWithTooltip>
        </span>
      </>
    );
  } else {
    return htmlDecode(cell);
  }
};

const custStatus = (refObj) => (cell, row) => {
  //return cell === 1 ? "Active" : "Inactive";
  let ret_str = '';
  if (cell === 1) {
    ret_str = "Active";
  } else if (cell === 0) {
    ret_str = "Inactive";
  } else if (cell === 3) {
    ret_str = "Dummy User";
  }

  if (row.approved_to_admin == true) {
    ret_str = `${ret_str} <i class="fas fa-star" style="color:#fb0808;" ></i>`;
  }

  return ret_str;

};

const loginDate = (refObj) => (cell) => {
  if (cell === null) {
    return "-";
  } else {
    return cell;
  }
};

const setDND = (refObj) => (cell) => {
  if (cell === 1) {
    return "Yes";
  } else {
    return "No";
  }
};

const initialValues = {
  first_name: "",
  last_name: "",
  company_name: "",
  email: "",
  phone_no: "",
  status: "",
  vip_customer: "",
  fa_access: "",
  country_id: "",
  language_code: "",
  teams: "",
  customer_type: "",
};

const setCreateDate = (refObj) => (cell) => {
  // if(cell != '' && cell != null){
  //   var mydate = new Date(cell);
  //   return dateFormat(mydate, "dd/mm/yyyy");
  // }else{
  //   return '-';
  // }

  if (cell === null) {
    return "-";
  } else {
    return cell;
  }
};

const arrayEquals = (a, b) => {
  return Array.isArray(a) &&
      Array.isArray(b) &&
      a.length === b.length &&
      a.every((val, index) => val === b[index]);
}

class Customer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customerTeam: [],
      customerID: 0,
      employeeID: 0,
      customers: [],
      customerDetails: [],
      countryList: [],
      languageList: [],
      companyList: [],
      productList: [],
      selectedTeamList: [],
      prevTeam:[],
      selectedRoleList: [],
      selectedTeams: {},
      selectedRole: {},
      isLoading: true,
      isValidState:false,
      customerflagId: 0,
      showModal: false,
      teamPopup: false,
      teamList: [],
      selectStatus: [
        { id: "0", name: "Inactive" },
        { id: "1", name: "Active" },
        { id: "3", name: "Dummy" },
      ],
      vipStat: [
        { id: "0", name: "No" },
        { id: "1", name: "Yes" },
      ],
      faAccess: [
        { id: "0", name: "No" },
        { id: "1", name: "Yes" },
      ],
      isAdmin: false,
      isAdminType: [
        { id: "0", name: "No" },
        { id: "1", name: "Yes" },
      ],
      roleType: [
        { id: "1", name: "Admin" },
        { id: "2", name: "Regular" },
      ],

      activePage: 1,
      totalCount: 0,
      itemPerPage: 20,
      showModalLoader: false,
      search_name: "",
      search_email: "",
      search_company: "",
      search_approval: "",
      search_status: "",
      remove_search: false,
      get_access_data: false,
      country_id: '',
      value: "", // for auto-suggest
      suggestions: [], // for auto-suggest
      company_has_soldTo: '',
      showCustomerSoldToPopup: false,
    };
  }

  componentDidMount() {
    //this.getCustomerList();
    //this.getCompanyList();
    const superAdmin = getSuperAdmin(localStorage.admin_token);

    if (superAdmin === 1) {
      this.setState({
        access: {
          view: true,
          add: true,
          edit: true,
          delete: true,
        },
        get_access_data: true,
      });
      this.getCustomerList();
      this.getCompanyList();
    } else {
      const adminGroup = getAdminGroup(localStorage.admin_token);
      API.get(
        `/api/adm_group/single_access/${adminGroup}/${"USERS_MANAGEMENT"}`
      )
        .then((res) => {
          this.setState({
            access: res.data.data,
            get_access_data: true,
          });

          if (res.data.data.view === true) {
            this.getCustomerList();
            this.getCompanyList();
          } else {
            this.props.history.push("/admin/dashboard");
          }
        })
        .catch((err) => {
          showErrorMessage(err, this.props);
        });
    }
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getCustomerList(pageNumber > 0 ? pageNumber : 1);
  };

  getCustomerList(page = 1) {
    let custname = this.state.search_name;
    let email = this.state.search_email;
    let custcompany = this.state.search_company;
    let custapproval = this.state.search_approval;
    let status = this.state.search_status;

    API.get(
      `/api/customers?page=${page}&custname=${encodeURIComponent(
        custname
      )}&email=${encodeURIComponent(email)}&custcompany=${encodeURIComponent(
        custcompany
      )}&status=${encodeURIComponent(status)}&custapproval=${encodeURIComponent(
        custapproval
      )}`
    )
      .then((res) => {
        this.setState({
          customers: res.data.data,
          count_user: res.data.count_user,
          isLoading: false,
          search_name: custname,
          search_email: email,
          search_company: custcompany,
          search_approval: custapproval,
          search_status: status,
        });
      })
      .catch((err) => {
        this.setState({
          isLoading: false,
        });
        showErrorMessage(err, this.props);
      });
  }

  getCountryTeam = (country_id) => {
    API.get(`/api/customers/country_team/${country_id}`)
      .then((res) => {
        if (res.data && res.data.data && res.data.data.length > 0) {
          const { emp_bm, emp_cscc, emp_csct, emp_ra } = res.data.data[0];
          const team_ids = [emp_bm, emp_cscc, emp_csct, emp_ra];
          const { teamList } = this.state;
          if (teamList && teamList.length > 0) {
            const country_team = [];
            const country_team_ids = [];
            team_ids.forEach(id => {
              const team = teamList.find((team) => id && team.value == id);
              if (team && Object.keys(team).length > 0) {
                country_team.push(team);
                country_team_ids.push(team.value);
              }
            });
            this.setState({ selectedTeams: country_team_ids, selectedTeamList: country_team });
          }
        }
      })
      .catch((err) => {
        showErrorMessage(err, this.props);
      });
  }

  setCountry = (event, setFieldValue) => {
    const { value } = event.target;
    if (value) {
      this.getCountryTeam(value);
      setFieldValue("country_id", value);
      this.setState({ country_id: value });
    } else {
      setFieldValue("country_id", "");
      this.setState({ country_id: "" });
    }
  };

  selectTeam = (option, setFieldValue,isValid) => {
    let new_team = [];
    if (option && (option.length === 0 || option === null)) {
      setFieldValue("teams", []);
      this.setState({ selectedTeamList: [], selectedTeams: [] });
    } else {
      let teams = [];
      let teamIds = [];
      option.forEach(team => {
        teams.push(team);
        teamIds.push(team.value);
      });
      setFieldValue("teams", teams);
      this.setState({ selectedTeamList: teams, selectedTeams: teamIds });
      new_team = teamIds;
    }
    let prev_team = this.state.prevTeam;

    console.log('team',prev_team,new_team);

    if(arrayEquals(prev_team,new_team)){
      isValid = false;
    }else{
      isValid = true;
    }
    this.setState({isValidState:isValid});
    console.log(isValid);

  };

  customerSearch = (e) => {
    e.preventDefault();

    var custname = document.getElementById("custname").value;
    var email = document.getElementById("email").value;
    var custcompany = document.getElementById("custcompany").value;
    var custapproval = document.getElementById("custapproval").value;
    var status = document.getElementById("status").value;

    if (
      custname === "" &&
      email === "" &&
      custcompany === "" &&
      custapproval === "" &&
      status === ""
    ) {
      return false;
    }

    API.get(
      `/api/customers?page=1&custname=${encodeURIComponent(
        custname
      )}&email=${encodeURIComponent(email)}&custcompany=${encodeURIComponent(
        custcompany
      )}&status=${encodeURIComponent(status)}&custapproval=${encodeURIComponent(
        custapproval
      )}`
    )
      .then((res) => {
        this.setState({
          customers: res.data.data,
          count_user: res.data.count_user,
          isLoading: false,
          search_name: custname,
          search_email: email,
          search_company: custcompany,
          search_approval: custapproval,
          search_status: status,
          remove_search: true,
        });
      })
      .catch((err) => {
        this.setState({
          isLoading: false,
        });
        showErrorMessage(err, this.props);
      });
  };

  getProductList() {
    API.get("/api/feed/adm_products")
      .then((res) => {
        var prodArr = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          prodArr.push({
            value: element["product_id"],
            label: htmlDecode(element["product_name"]),
          });
        }
        this.setState({
          productList: prodArr,
        });
      })
      .catch((err) => {
        showErrorMessage(err, this.props);
      });
  }

  getCountryList() {
    API.get("/api/customers/country")
      .then((res) => {
        this.setState({
          countryList: res.data.data,
        });
      })
      .catch((err) => {
        showErrorMessage(err, this.props);
      });
  }

  getLanguageList() {
    API.get("/api/feed/adm_language")
      .then((res) => {
        this.setState({
          languageList: res.data.data,
        });
      })
      .catch((err) => {
        showErrorMessage(err, this.props);
      });
  }

  getCompanyList() {
    API.get("/api/customers/company")
      .then((res) => {
        this.setState({
          companyList: res.data.data,
        });
      })
      .catch((err) => {
        showErrorMessage(err, this.props);
      });
  }

  getEmployeeList() {
    API.get("/api/employees/all")
      .then((res) => {
        console.log(res.data);
        var myTeam = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          if (element["desig_name"] == 'SPOC') {
            console.log(element["lang_code"]);
            let prefix = '';
            if (element["lang_code"] === 'zh') {
              prefix = 'M-'
            } else if (element["lang_code"] === 'es') {
              prefix = 'S-'
            } else if (element["lang_code"] === 'pt') {
              prefix = 'P-'
            } else if (element["lang_code"] === 'ja') {
              prefix = 'J-'
            }
            myTeam.push({
              value: element["employee_id"],
              label:
                element["first_name"] +
                " " +
                element["last_name"] +
                " (" +
                prefix +
                element["desig_name"] +
                ")",
            });
          } else {
            myTeam.push({
              value: element["employee_id"],
              label:
                element["first_name"] +
                " " +
                element["last_name"] +
                " (" +
                element["desig_name"] +
                ")",
            });
          }

        }
        this.setState({
          teamList: myTeam,
        });
      })
      .catch((err) => {
        showErrorMessage(err, this.props);
      });
  }

  getIndividualCustomer(id) {
    API.get(`/api/customers/${id}`)
      .then((res) => {
        this.setState({
          customerDetails: res.data.data,
          value: res.data.data.company_name
        });
        this.checkCompanySoldto(res.data.data.company_name);
      })
      .catch((err) => {
        showErrorMessage(err, this.props);
      });
  }

  getCustomerTeam(id, callBack) {
    var selMyTeam = [];
    var selTeams = [];
    var selMyRole = [];
    var selRole = [];
    var selMyProducts = [];
    var selProducts = [];
    API.get(`/api/customers/${id}`)
      .then((res) => {
        for (let index = 0; index < res.data.data.teams.length; index++) {
          const element = res.data.data.teams[index];
          selMyTeam.push({
            value: element["employee_id"],
            label:
              element["first_name"] +
              " " +
              element["last_name"] +
              " (" +
              element["desig_name"] +
              ")",
          });
          selTeams.push(element["employee_id"]);
        }

        for (let index = 0; index < res.data.data.role.length; index++) {
          const element = res.data.data.role[index];
          selMyRole.push({
            value: element["role_id"],
            label: element["role_name"],
          });
          selRole.push(element["role_id"]);
        }

        for (let index = 0; index < res.data.data.products.length; index++) {
          const element = res.data.data.products[index];
          selMyProducts.push({
            value: element["product_id"],
            label: htmlDecode(element["product_name"]),
          });
          selProducts.push(element["product_id"]);
        }

        if (selRole[0] === 1) {
          this.setState({
            prevTeam:selTeams,
            selectedTeamList: selMyTeam,
            selectedTeams: selTeams,
            selectedRoleList: "",
            selectedRole: "",
            selectedProductsList: selMyProducts,
            selectedProducts: selProducts,
            is_admin: 1,
            dnd: res.data.data.dnd,
            ignore_spoc: res.data.data.ignore_spoc,
            multi_lang_access: res.data.data.multi_lang_access,
            isAdmin: false,
          });
        } else {
          this.setState({
            prevTeam:selTeams,
            selectedTeamList: selMyTeam,
            selectedTeams: selTeams,
            selectedRoleList: selMyRole,
            selectedRole: selRole,
            selectedProductsList: selMyProducts,
            selectedProducts: selProducts,
            is_admin: 0,
            dnd: res.data.data.dnd,
            ignore_spoc: res.data.data.ignore_spoc,
            multi_lang_access: res.data.data.multi_lang_access,
            isAdmin: true,
          });
        }
        // console.log(this.state);
        callBack();
      })
      .catch((err) => {
        showErrorMessage(err, this.props);
      });
  }

  roleList() {
    API.get("/api/customers/roles")
      .then((res) => {
        var myRole = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          myRole.push({
            value: element["role_id"],
            label: element["role_name"],
          });
        }
        this.setState({
          roleList: myRole,
        });
      })
      .catch((err) => {
        showErrorMessage(err, this.props);
      });
  }

  enterPressed = (event) => {
    var code = event.keyCode || event.which;
    if (code === 13) {
      //13 is the enter keycode

      this.setState({
        searchkey: event.target.value,
      });
      //this.getRequests(1, event.target.value);
    }
  };

  onReferenceChange = (event, { newValue }, setFieldValue) => {
    this.setState({ value: newValue }, () =>
      setFieldValue("company_name", newValue)
    );
  };

  onSuggestionsFetchRequested = ({ value, reason }) => {
    const inputValue = value.toLowerCase();
    const inputLength = inputValue.length;

    if (inputLength === 0) {
      return [];
    } else {
      if (inputLength > 0) {
        this.checkCompanySoldto(value);
        API
          .get(`/api/employees/company-search/${value}`)
          .then((res) => {
            this.setState({
              suggestions: res.data.data,
            });
            return this.state.suggestions;
          })
          .catch((err) => {
            console.log("==========", err);
          });
      }
    }
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
    });
  };

  setSuggesstionValue = (suggestion, setFieldValue) => {
    setFieldValue("company_name", suggestion.company_name);
    this.checkCompanySoldto(suggestion.company_name);
    return suggestion.company_name;
  };

  renderSuggestion = (suggestion) => {
    return (
      <div>
        <p style={{ cursor: "pointer" }}>{suggestion.company_name}</p>
      </div>
    );
  };

  modalCloseHandler = () => {
    this.setState({ customerflagId: 0 });
    this.setState({ showModal: false, isAdmin: false, is_admin: "", company_has_soldTo: '' });
  };

  modalCloseHandlerTeam = () => {
    this.setState({ customerTeam: [], customerID: 0, teamPopup: false });
    for (let index = 0; index < this.state.customerTeam.length; index++) {
      this.setState({ [`show_button_${index}`]: false })
      
    }
  };

  modalShowHandler = (event, id) => {
    this.getEmployeeList();
    this.roleList();
    this.getCountryList();
    this.getCompanyList();
    this.getProductList();
    this.getLanguageList();

    if (id) {
      event.preventDefault();
      this.setState({ customerflagId: id });
      this.getIndividualCustomer(id);
      this.getCustomerTeam(id, () => this.setState({ showModal: true }));
    } else {
      this.setState({
        customerDetails: "",
        prevTeam:"",
        selectedTeamList: "",
        selectedTeams: "",
        selectedRoleList: "",
        selectedRole: "",
        selectedProductsList: "",
        selectedProducts: "",
        company_has_soldTo: "",
        showModal: true,
      });
    }
  };

  handleSubmitEvent = (values, actions) => {
    var role_type;
    if (values.role_type > 0) {
      role_type = values.role_type;
    } else {
      role_type = "0";
    }

    const post_data = {
      first_name: values.first_name,
      last_name: values.last_name,
      country_id: values.country_id,
      language_code: values.language_code,
      company_name: values.company_name,
      status: values.status,
      email: values.email,
      vip_customer: values.vip_customer,
      fa_access: values.fa_access,
      phone_no: values.phone_no,
      teams: this.state.selectedTeams,
      products: values.products,
      sap_ref: values.sap_ref,
      customer_type: values.customer_type,
      is_admin: values.is_admin,
      role: values.role,
      role_type: role_type,
      dnd: values.dnd,
      ignore_spoc: values.ignore_spoc,
      multi_lang_access: values.multi_lang_access
    };

    if (this.state.customerflagId) {
      this.setState({ showModalLoader: true });
      const id = this.state.customerflagId;
      API.put(`/api/customers/${id}`, post_data)
        .then((res) => {
          this.modalCloseHandler();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record updated successfully.",
            icon: "success",
          }).then(() => {
            this.setState({ showModalLoader: false });
            this.getCustomerList(this.state.activePage);
          });
        })
        .catch((err) => {
          this.setState({ showModalLoader: false });
          if (err.data.status === 3) {
            this.setState({
              showModal: false,
            });
            showErrorMessage(err, this.props);
          } else {
            actions.setErrors(err.data.errors);
            actions.setSubmitting(false);
          }
        });
    } else {
      this.setState({ showModalLoader: true });
      API.post("/api/customers", post_data)
        .then((res) => {
          this.modalCloseHandler();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record added successfully.",
            icon: "success",
          }).then(() => {
            this.setState({ activePage: 1, showModalLoader: false });
            this.getCustomerList(this.state.activePage);
          });
        })
        .catch((err) => {
          this.setState({ showModalLoader: false });
          if (err.data.status === 3) {
            this.setState({
              showModal: false,
            });
            showErrorMessage(err, this.props);
          } else {
            actions.setErrors(err.data.errors);
            actions.setSubmitting(false);
          }
        });
    }
  };

  confirmDelete = (event, id) => {
    event.preventDefault();
    swal({
      closeOnClickOutside: false,
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.deleteCustomer(id);
      }
    });
  };

  sendPassword = (event, id) => {
    event.preventDefault();
    API.get(`/api/customers/set_password/${id}`)
      .then((res) => {
        console.log("res", res);
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: res.data.data,
          icon: "success",
        }).then(() => {
          this.setState({ activePage: 1 });
          this.getCustomerList(this.state.activePage);
        });
      })
      .catch((err) => {
        if (err.data.status === 3) {
          this.setState({ closeModal: true });
          showErrorMessage(err, this.props);
        }
      });
  };

  getTeam = (event, id) => {    // SVJT popup closed for all approved
    //this.getEmployeeList();
    event.preventDefault();
    this.setState({ customerTeam: [], customerID: 0 });
    API.get(`/api/feed/get_customer_team/${id}`)
      .then((res) => {
        console.log("res", res.data.data);

        let teams_arr = res.data.data.filter(val => {
          return val.employee_approved != 1
        })

        if (teams_arr.length > 0) {
          this.setState({ customerTeam: res.data.data, customerID: id, teamPopup: true });
        } else {
          this.setState({ teamPopup: false, customerTeam: [], customerID: 0 });
          this.getCustomerList();
        }

      })
      .catch((err) => {
        if (err.data.status === 3) {
          this.setState({ closeModal: true });
          showErrorMessage(err, this.props);
        }
      });
  };

  approveCustomer = (event, assign_id, id) => {
    //alert(assign_id)
    event.preventDefault();
    swal({
      closeOnClickOutside: false,
      title: "Customer Approval Request",
      text: "Are you sure, you want to approve the customer?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.setState({ showModalLoader: true });
        API.post(`/api/customers/approve_customer_approval_by_admin/`, { assign_id: assign_id })
          .then((res) => {
            swal({
              closeOnClickOutside: false,
              title: "Success",
              text: "User successfully approved.",
              icon: "success",
            }).then(() => {
              this.setState({ showModalLoader: false });
              this.getTeam(event, id)
            });
          })
          .catch((err) => {
            showErrorMessage(err, this.props);
          });
      }

    });
  };

  assignCustomer = (event, assign_id, employee_id, id, index) => {
    event.preventDefault();
    swal({
      closeOnClickOutside: false,
      title: "Customer Assign Request",
      text: "Are you sure you want to assign this team member?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.setState({ showModalLoader: true });
        API.post(`/api/customers/assign_customer_approval_by_admin/`, { assign_id: assign_id, employee_id: employee_id })
          .then((res) => {
            swal({
              closeOnClickOutside: false,
              title: "Success",
              text: "User successfully assigned.",
              icon: "success",
            }).then(() => {
              this.setState({ showModalLoader: false, [`show_button_${index}`]: false });
              this.getTeam(event, id)
            });
          })
          .catch((err) => {
            showErrorMessage(err, this.props);
          });
      }

    });
  };

  deleteCustomer = (id) => {
    API.delete(`/api/customers/${id}`)
      .then((res) => {
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: "Record deleted successfully.",
          icon: "success",
        }).then(() => {
          this.setState({ activePage: 1 });
          this.getCustomerList(this.state.activePage);
        });
      })
      .catch((err) => {
        if (err.data.status === 3) {
          this.setState({ closeModal: true });
          showErrorMessage(err, this.props);
        }
      });
  };

  onChangeAdminType = (event) => {
    if (event.target.value === "0") {
      this.setState({ isAdmin: true });
    } else {
      this.setState({ isAdmin: false });
      this.setState({ selectedRoleList: "", selectedRole: "" });
    }
  };

  renderShowsTotal = (start, to, total) => {
    return (
      <span className="pageShow">
        Showing {start} to {to}, of {total} records
      </span>
    );
  };

  clearSearch = () => {
    document.getElementById("custname").value = "";
    document.getElementById("email").value = "";
    document.getElementById("custcompany").value = "";
    document.getElementById("custapproval").value = "";
    document.getElementById("status").value = "";

    this.setState(
      {
        search_name: "",
        search_email: "",
        search_company: "",
        search_approval: "",
        search_status: "",
        remove_search: false,
      },
      () => {
        this.getCustomerList();
        this.setState({ activePage: 1 });
      }
    );
  };

  downloadXLSX = (e) => {
    e.preventDefault();

    let custname = this.state.search_name;
    let email = this.state.search_email;
    let custcompany = this.state.search_company;
    let status = this.state.search_status;

    API.get(
      `/api/customers/download?page=1&custname=${encodeURIComponent(
        custname
      )}&email=${encodeURIComponent(email)}&custcompany=${encodeURIComponent(
        custcompany
      )}&status=${encodeURIComponent(status)}`,
      { responseType: "blob" }
    )
      .then((res) => {
        let url = window.URL.createObjectURL(res.data);
        let a = document.createElement("a");
        a.href = url;
        a.download = "users.xlsx";
        a.click();
      })
      .catch((err) => {
        showErrorMessage(err, this.props);
      });
  };

  checkHandler = (event) => {
    event.preventDefault();
  };

  getTeamStatus = (approval_status) => {
    if (approval_status == 0) {
      return `Pending`;
    } else if (approval_status == 1) {
      return `Approved`;
    } else if (approval_status == 2) {
      return `Assigned To Other Employee`;
    } else if (approval_status == 3) {
      return `Assigned To Admin`;
    }
  }

  checkCompanySoldto = (company_name) => {
    API
      .get(`/api/company/company_has_soldto/${company_name}`)
      .then((res) => {
        if (res.data && Object.keys(res.data).length > 0) {
          const { company_id, has_soldto } = res.data;
          this.setState({ company_has_soldTo: has_soldto, company_id: company_id });
        } else {
          this.setState({ company_has_soldTo: '', company_id: '' });
        }
      })
      .catch((err) => {
        this.setState({ company_has_soldTo: '' });
        console.log("==========", err);
      });
  }

  handleShowCustomerSoldToPopup = () => {
    this.setState({ showCustomerSoldToPopup: true });
  }

  handleCloseCustomerSoldToPopup = () => {
    const company_name = this.state.value;
    this.checkCompanySoldto(company_name);
    this.setState({ showCustomerSoldToPopup: false });
  }

  render() {
    const { customerDetails, value, suggestions, selectedTeams } = this.state;

    const newInitialValues = Object.assign(initialValues, {
      first_name: customerDetails.first_name
        ? htmlDecode(customerDetails.first_name)
        : "",
      last_name: customerDetails.last_name
        ? htmlDecode(customerDetails.last_name)
        : "",
      company_name: customerDetails.company_name ? htmlDecode(customerDetails.company_name) : "",
      email: customerDetails.email ? htmlDecode(customerDetails.email) : "",
      phone_no: customerDetails.phone_no
        ? htmlDecode(customerDetails.phone_no)
        : "",
      sap_ref: customerDetails.sap_ref
        ? htmlDecode(customerDetails.sap_ref)
        : "",
      status:
        customerDetails.status || +customerDetails.status === 0
          ? customerDetails.status.toString()
          : "",
      vip_customer:
        customerDetails.vip_customer || +customerDetails.vip_customer === 0
          ? customerDetails.vip_customer.toString()
          : "",
      fa_access:
        customerDetails.new_feature_fa || +customerDetails.new_feature_fa === 0
          ? customerDetails.new_feature_fa.toString()
          : "",
      country_id: customerDetails.country_id
        ? customerDetails.country_id.toString()
        : "",
      language_code: customerDetails.language_code
        ? customerDetails.language_code.toString()
        : "",

      company_id: customerDetails.company_id
        ? customerDetails.company_id.toString()
        : "",
      teams: this.state.selectedTeamList ? this.state.selectedTeamList : "",
      customer_type: customerDetails.customer_type
        ? customerDetails.customer_type.toString()
        : "",
      is_admin:
        this.state.is_admin || +this.state.is_admin === 0
          ? this.state.is_admin.toString()
          : "",
      dnd:
        this.state.dnd || +this.state.dnd === 0
          ? this.state.dnd.toString()
          : "2",
      ignore_spoc:
        this.state.ignore_spoc || +this.state.ignore_spoc === 0
          ? this.state.ignore_spoc.toString()
          : "2",
      multi_lang_access:
        this.state.multi_lang_access || +this.state.multi_lang_access === 0
          ? this.state.multi_lang_access.toString()
          : "2",
      role: this.state.selectedRole ? this.state.selectedRole : "[]",
      role_type: customerDetails.role_type
        ? customerDetails.role_type.toString()
        : "",
      products: this.state.selectedProducts
        ? this.state.selectedProducts
        : "[]",
    });

    const validateStopFlag = Yup.object().shape({
      first_name: Yup.string()
        .trim()
        .required("Please enter first name")
        .min(1, "First name can be minimum 1 characters long")
        /*.matches(/^[A-Za-z0-9\s]*$/, "Invalid first name format! Only alphanumeric and spaces are allowed")*/

        .max(30, "First name can be maximum 30 characters long"),
      last_name: Yup.string()
        .trim()
        .required("Please enter last name")
        .min(1, "Last name can be minimum 1 characters long")
        /*.matches(/^[A-Za-z0-9\s]*$/, "Invalid last name format! Only alphanumeric and spaces are allowed")*/

        .max(30, "Last name can be maximum 30 characters long"),
      email: Yup.string()
        .trim()
        .required("Please enter email")
        .email("Invalid email")
        .max(80, "Email can be maximum 80 characters long"),
      phone_no: Yup.string()
        .trim()
        .required("Please enter phone number")
        .max(30, "Phone number cannot be more than 30 characters long"),
      status: Yup.string()
        .trim()
        .required("Please select status")
        .matches(/^[0|1|3]$/, "Invalid status selected"),
      country_id: Yup.string().trim().required("Please select country"),
      language_code: Yup.string().trim().required("Please select language"),
      // company_id: Yup.string().trim().required("Please select customer"),
      company_name: Yup.string().trim().required("Please select customer"),
      vip_customer: Yup.string()
        .trim()
        .required("Please select option")
        .matches(/^[0|1]$/, "Invalid option selected"),
      fa_access: Yup.string()
        .trim()
        .required("Please select option")
        .matches(/^[0|1]$/, "Invalid option selected"),
      // teams: Yup.array()
      //   .ensure()
      //   .min(1, "Please add at least one team.")
      //   .of(Yup.string().ensure().required("Team cannot be empty")),
      teams: Yup.array()
        .of(
          Yup.object().shape({
            label: Yup.string(),
            value: Yup.string()
          })
        )
        .test("teamSelected", "Please add at least one team.", () => this.state.selectedTeamList && this.state.selectedTeamList.length > 0),
      customer_type: Yup.string()
        .trim()
        .required("Please select the type of user"),
      is_admin: Yup.string()
        .trim()
        .required("Please select is admin")
        .matches(/^[0|1]$/, "Invalid admin selected"),
      dnd: Yup.string()
        .trim()
        .required("Please select status for do not disturb")
        .matches(/^[1|2]$/, "Invalid status for do not disturb"),
      ignore_spoc: Yup.string()
        .trim()
        .required("Please select Ignore SPOC")
        .matches(/^[1|2]$/, "Invalid selection for Ignore SPOC"),
      multi_lang_access: Yup.string()
        .trim()
        .required("Please select Multi Language Access")
        .matches(/^[1|2]$/, "Invalid selection for  Multi Language Access")
      /* role: Yup.array().when('is_admin',
        {
          is: "0",
          then: Yup.array().ensure().min(1, "Please add at least one role.")
            .of(Yup.string().ensure().required("Role cannot be empty"))
        }),
      role_type: Yup.string().when('is_admin',
        {
          is: "0",
          then: Yup.string().required("Please select is role type")
        }), */
    });

    //console.log(this.state.customers);

    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy">
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <h1>
                    Manage Users
                    <small />
                  </h1>
                </div>

                <div className="col-lg-12 col-sm-12 col-xs-12  topSearchSection">
                  {this.state.access.add === true ? (
                    <div className="">
                      <button
                        type="button"
                        className="btn btn-info btn-sm"
                        onClick={(e) => this.modalShowHandler(e, "")}
                      >
                        <i className="fas fa-plus m-r-5" /> Add User
                      </button>
                    </div>
                  ) : null}
                  {/*  */}
                  <form className="form">
                    <div className="">
                      <input
                        className="form-control"
                        name="custname"
                        id="custname"
                        placeholder="Filter by name"
                      />
                    </div>

                    <div className="">
                      <input
                        className="form-control"
                        name="email"
                        id="email"
                        placeholder="Filter by email"
                      />
                    </div>

                    <div className="">
                      <select
                        name="custcompany"
                        id="custcompany"
                        className="form-control w-150"
                      >
                        <option value="">Select Customer</option>
                        {this.state.companyList.map((company, i) => (
                          <option key={i} value={company.company_id}>
                            {htmlDecode(company.company_name)}
                          </option>
                        ))}
                      </select>
                    </div>

                    <div className="">
                      <select
                        name="custapproval"
                        id="custapproval"
                        className="form-control w-150"
                      >
                        <option value="">Select Approval Request</option>
                        <option value="1">Admin approval request</option>
                      </select>
                    </div>

                    <div className="">
                      <select
                        name="status"
                        id="status"
                        className="form-control"
                      >
                        <option value="">Select status</option>
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                        <option value="3">Dummy</option>
                      </select>
                    </div>

                    <div className="">
                      <input
                        type="submit"
                        value="Search"
                        className="btn btn-warning btn-sm"
                        onClick={(e) => this.customerSearch(e)}
                      />
                      {this.state.remove_search ? (
                        <a
                          onClick={() => this.clearSearch()}
                          className="btn btn-danger btn-sm"
                        >
                          {" "}
                          Remove{" "}
                        </a>
                      ) : null}
                    </div>
                    <div className="clearfix"></div>
                  </form>
                  {/*  */}
                </div>
                {/* <div className="clearfix"></div>
                <div className="col-lg-12 col-sm-12 col-xs-12 mt-10">
                <button
                    type="button"
                    className="btn btn-info btn-sm"
                    onClick={e => this.modalShowHandler(e, "")}
                  >
                    <i className="fas fa-plus m-r-5" /> Add Customer
                  </button>
                  </div> */}
              </div>
            </section>
            <section className="content">
              <div className="box">
                <div className="box-body">
                  <div className="nav-tabs-custom">
                    <ul className="nav nav-tabs">
                      <li className="tabButtonSec pull-right">
                        {this.state.count_user > 0 ? (
                          <span onClick={(e) => this.downloadXLSX(e)}>
                            <LinkWithTooltip
                              tooltip={`Click here to download excel`}
                              href="#"
                              id="tooltip-my"
                              clicked={(e) => this.checkHandler(e)}
                            >
                              <i className="fas fa-download"></i>
                            </LinkWithTooltip>
                          </span>
                        ) : null}
                      </li>
                    </ul>
                  </div>

                  <BootstrapTable data={this.state.customers}>
                    <TableHeaderColumn
                      isKey
                      dataField="first_name"
                      dataFormat={__htmlDecode(this)}
                    >
                      First Name
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="last_name"
                      dataFormat={__htmlDecode(this)}
                    >
                      Last Name
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="company_name"
                      dataFormat={__salesCompany(this)}
                      width={"17%"}
                    >
                      Customer Name
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="language"
                      dataFormat={__htmlDecode(this)}
                      width={"8%"}
                    >
                      Language
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="email"
                      dataFormat={__htmlDecode(this)}
                      width={"16%"}
                    >
                      Email
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="status"
                      dataFormat={custStatus(this)}
                      width={"10%"}
                    >
                      Status
                    </TableHeaderColumn>
                    {/* <TableHeaderColumn
                      dataField="vip_customer"
                      dataFormat={vipStatus(this)}                     
                    >
                      Key Customer
                    </TableHeaderColumn> */}

                    <TableHeaderColumn
                      dataField="activated"
                      dataFormat={vipStatus(this)}
                    >
                      Password Set
                    </TableHeaderColumn>

                    <TableHeaderColumn
                      dataField="pass_update_date"
                      dataFormat={setCreateDate(this)}
                      editable={false}
                      expandable={false}
                    >
                      Pass Update Date
                    </TableHeaderColumn>

                    <TableHeaderColumn
                      dataField="display_login_date"
                      dataFormat={loginDate(this)}
                    >
                      Last Login Time
                    </TableHeaderColumn>

                    <TableHeaderColumn
                      dataField="dnd"
                      dataFormat={setDND(this)}
                    >
                      Disable Notifications
                    </TableHeaderColumn>

                    {this.state.access.edit === true ||
                      this.state.access.delete === true ? (
                      <TableHeaderColumn
                        dataField="customer_id"
                        dataFormat={actionFormatter(this)}
                        dataAlign=""
                        width={"8%"}
                      >
                        Action
                      </TableHeaderColumn>
                    ) : null}
                  </BootstrapTable>

                  {this.state.count_user > 20 ? (
                    <Row>
                      <Col md={12}>
                        <div className="paginationOuter text-right">
                          <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemPerPage}
                            totalItemsCount={this.state.count_user}
                            itemClass="nav-item"
                            linkClass="nav-link"
                            activeClass="active"
                            onChange={this.handlePageChange}
                          />
                        </div>
                      </Col>
                    </Row>
                  ) : null}

                  {/* ======= Add/Edit Customer modal ======== */}
                  {this.state.teamPopup === true && <Modal
                    show={this.state.teamPopup}
                    onHide={() => this.modalCloseHandlerTeam()}
                    backdrop="static"
                    className="employeeApprovalPopup"
                  >
                    <Modal.Header closeButton>
                      <Modal.Title>
                        Employee Approval Status
                      </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <div className="contBox">


                        <table width="100%" cellpadding="5" cellspacing="5" className="table">
                          <tr>
                            <th>Current team</th>
                            <th>Status</th>
                            <th width="150">Assign to</th>
                            <th width="150">Action</th>
                          </tr>

                          {this.state.customerTeam.length > 0 && this.state.customerTeam.map((teams, index) => {
                            return <tr>
                              <td><label>{teams.employee_name}</label></td>
                              <td>                                <label>
                                {this.getTeamStatus(teams.employee_approved)}
                              </label></td>
                              <td width="150">
                                {this.getTeamStatus(teams.employee_approved) !='Approved' && <Select
                                name="teams"
                                options={teams.other_team_list}
                                className="basic-multi-select"
                                classNamePrefix="select"
                                onChange={(e) => {
                                  if (e != null) {
                                    this.setState({ [`show_button_${index}`]: true, employeeID: e.value });
                                  } else {
                                    this.setState({ [`show_button_${index}`]: false, employeeID: 0 });
                                  }
                                }}
                              />}
                              </td>
                              <td width="150">
                                <div className="d-flex">
                                  {teams.employee_approved == 0 &&
                                    <button
                                      className={`btn btn-sm btn-success`}
                                      type="button"
                                      onClick={(e) => { this.approveCustomer(e, teams.team_id, this.state.customerID) }}
                                    >
                                      Approve
                                    </button>}

                                  {this.state[`show_button_${index}`] && this.state[`show_button_${index}`] === true &&

                                    <button
                                      className={`btn btn-sm btn-info`}
                                      type="button"
                                      onClick={(e) => { this.assignCustomer(e, teams.team_id, this.state.employeeID, this.state.customerID, index) }}
                                    >
                                      Assign
                                    </button>
                                  }
                                </div>
                              </td>
                            </tr>
                          })}

                        </table>







                        {/* {this.state.customerTeam.length > 0 && this.state.customerTeam.map((teams, index) => {
                          return <Row>
                            <Col xs={12} sm={4} md={4}>
                              <div className="form-group">
                                <label>
                                  {teams.employee_name}
                                </label>
                              </div>
                            </Col>
                            <Col xs={12} sm={2} md={2}>
                              <div className="form-group">
                                <label>
                                  {this.getTeamStatus(teams.employee_approved)}
                                </label>
                              </div>
                            </Col>
                            {teams.employee_approved != 1 && <Col xs={12} sm={3} md={3}>
                              <div className="form-group">
                                <label>
                                  Employee
                                </label>

                                <Select
                                  name="teams"
                                  options={teams.other_team_list}
                                  className="basic-multi-select"
                                  classNamePrefix="select"
                                  onChange={(e) => {
                                    if (e != null) {
                                      this.setState({ [`show_button_${index}`]: true, employeeID : e.value });
                                    } else {
                                      this.setState({ [`show_button_${index}`]: false, employeeID : 0 });
                                    }
                                  }}
                                />



                              </div>
                            </Col>}
                          
                              <Col xs={12} sm={3} md={3}>
                              <div className="form-group">

                              {teams.employee_approved == 0 && 
                                <button
                                  className={`btn btn-sm`}
                                  type="button"
                                  onClick= {(e) => {this.approveCustomer(e,teams.team_id,this.state.customerID)}}
                                >
                                  Approve
                                </button>}

                                {this.state[`show_button_${index}`] && this.state[`show_button_${index}`] === true &&

                                  <button
                                    className={`btn btn-sm`}
                                    type="button"
                                    onClick= {(e) => {this.assignCustomer(e,teams.team_id,this.state.employeeID,this.state.customerID,index)}}
                                  >
                                    Assign
                                  </button>
                                }
                                </div>
                              </Col>
                             

                          </Row>
                        })} */}
                      </div>
                    </Modal.Body>
                    <Modal.Footer>
                      {/* <button
                        onClick={(e) => this.modalCloseHandlerTeam()}
                        className={`btn btn-danger btn-sm`}
                        type="button"
                      >
                        Close
                      </button> */}
                    </Modal.Footer>
                  </Modal>}

                  <Modal
                    show={this.state.showModal}
                    onHide={() => this.modalCloseHandler()}
                    backdrop="static"
                  >
                    <Formik
                      initialValues={newInitialValues}
                      validationSchema={validateStopFlag}
                      onSubmit={this.handleSubmitEvent}
                    >
                      {({
                        values,
                        errors,
                        touched,
                        isValid,
                        dirty,
                        isSubmitting,
                        setFieldValue,
                        setFieldTouched,
                        handleChange,
                      }) => {
                        return (
                          <Form>
                            {/* {console.log({ errors })}
                            {console.log({isValid})}
                            {console.log({ values })} */}
                            {this.state.showModalLoader === true ? (
                              <div className="loading_reddy_outer">
                                <div className="loading_reddy">
                                  <img src={whitelogo} alt="loader" />
                                </div>
                              </div>
                            ) : (
                              ""
                            )}
                            <Modal.Header closeButton>
                              <Modal.Title>
                                {this.state.customerflagId > 0 ? "Edit" : "Add"}{" "}
                                User
                              </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="contBox">
                                <Row>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        First Name
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="first_name"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter first name"
                                        autoComplete="off"
                                      />
                                      {errors.first_name &&
                                        touched.first_name ? (
                                        <span className="errorMsg">
                                          {errors.first_name}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Last Name
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="last_name"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter last name"
                                        autoComplete="off"
                                      />
                                      {errors.last_name && touched.last_name ? (
                                        <span className="errorMsg">
                                          {errors.last_name}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>
                                        Customer
                                        <span className="impField">*</span>
                                      </label>
                                      {/* <Field
                                        name="company_id"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.company_id}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        {this.state.companyList.map(
                                          (company, i) => (
                                            <option
                                              key={i}
                                              value={company.company_id}
                                            >
                                              {htmlDecode(company.company_name)}
                                            </option>
                                          )
                                        )}
                                      </Field> */}
                                      <Autosuggest
                                        suggestions={suggestions}
                                        onSuggestionsFetchRequested={
                                          this.onSuggestionsFetchRequested
                                        }
                                        onSuggestionsClearRequested={
                                          this.onSuggestionsClearRequested
                                        }
                                        getSuggestionValue={(suggestion) =>
                                          this.setSuggesstionValue(
                                            suggestion,
                                            setFieldValue
                                          )
                                        }
                                        renderSuggestion={this.renderSuggestion}
                                        inputProps={{
                                          placeholder: "Customer",
                                          value: values.company_name,
                                          type: "search",
                                          onChange: (event, object) => {
                                            return this.onReferenceChange(
                                              event,
                                              object,
                                              setFieldValue
                                            );
                                          },
                                          onKeyPress: this.enterPressed.bind(this),
                                          onBlur: () => setFieldTouched("company_name"),
                                          className: "form-control customInput",
                                        }}
                                        name="company_name"
                                      />
                                      {
                                        this.state.value && this.state.company_has_soldTo !== '' ? (
                                          this.state.company_has_soldTo ? (
                                            <span className="task-details-language"
                                              style={{
                                                color: "rgb(0, 86, 179)",
                                                fontWeight: "700",
                                              }} >To map new Sold To Party, click <button type="button" onClick={this.handleShowCustomerSoldToPopup}>here</button>.</span>
                                          ) : (
                                            <span className="task-details-language"
                                              style={{
                                                color: "rgb(0, 86, 179)",
                                                fontWeight: "700",
                                              }} >No Sold To Party mapped with above customer. Click <button type="button" onClick={this.handleShowCustomerSoldToPopup}>here</button> to map one.</span>
                                          )
                                        ) : (null)
                                      }
                                      {errors.company_id ? (
                                        <span className="errorMsg">
                                          {errors.company_id}
                                        </span>
                                      ) : null}
                                      {errors.company_name &&
                                        touched.company_name ? (
                                        <span className="errorMsg">
                                          {errors.company_name}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>
                                        Customer Email
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="email"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter customer email"
                                        autoComplete="off"
                                      />
                                      {errors.email && touched.email ? (
                                        <span className="errorMsg">
                                          {errors.email}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>
                                        Phone Number
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="phone_no"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter phone number"
                                        autoComplete="off"
                                      />
                                      {errors.phone_no && touched.phone_no ? (
                                        <span className="errorMsg">
                                          {errors.phone_no}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>

                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>
                                        Country
                                        <span className="impField">*</span>
                                      </label>

                                      <Field
                                        name="country_id"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.country_id}
                                        onChange={(e) => {
                                          this.setCountry(e, setFieldValue);
                                        }}
                                        onBlur={() => setFieldTouched('country_id')}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        {this.state.countryList.map(
                                          (country, i) => (
                                            <option
                                              key={i}
                                              value={country.country_id}
                                            >
                                              {htmlDecode(country.country_name)}
                                            </option>
                                          )
                                        )}
                                      </Field>
                                      {errors.country_id &&
                                        touched.country_id ? (
                                        <span className="errorMsg">
                                          {errors.country_id}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>

                                <Row>
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>
                                        Language
                                        <span className="impField">*</span>
                                      </label>

                                      <Field
                                        name="language_code"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.language_code}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        {this.state.languageList.map(
                                          (lang, i) => (
                                            <option
                                              key={i}
                                              value={lang.code}
                                            >
                                              {htmlDecode(lang.language)}
                                            </option>
                                          )
                                        )}
                                      </Field>
                                      {errors.language_code &&
                                        touched.language_code ? (
                                        <span className="errorMsg">
                                          {errors.language_code}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>
                                        Key Account
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="vip_customer"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.vip_customer}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        {this.state.vipStat.map((status, i) => (
                                          <option key={i} value={status.id}>
                                            {status.name}
                                          </option>
                                        ))}
                                      </Field>
                                      {errors.vip_customer &&
                                        touched.vip_customer ? (
                                        <span className="errorMsg">
                                          {errors.vip_customer}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>

                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>
                                        Status
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="status"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.status}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        {this.state.selectStatus.map(
                                          (status, i) => (
                                            <option key={i} value={status.id}>
                                              {status.name}
                                            </option>
                                          )
                                        )}
                                      </Field>
                                      {errors.status && touched.status ? (
                                        <span className="errorMsg">
                                          {errors.status}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  {/* <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>SAP ref number</label>
                                      <Field
                                        name="sap_ref"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter SAP reference"
                                        autoComplete="off"
                                      />
                                      {errors.sap_ref && touched.sap_ref ? (
                                        <span className="errorMsg">
                                          {errors.sap_ref}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col> */}
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        User Type
                                        <span className="impField">*</span>
                                      </label>

                                      <Field
                                        name="customer_type"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.customer_type}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        <option key="1" value="API">
                                          API
                                        </option>
                                        <option key="2" value="CPS">
                                          CPS
                                        </option>
                                        <option key="3" value="BOTH">
                                          BOTH
                                        </option>
                                      </Field>
                                      {errors.customer_type &&
                                        touched.customer_type ? (
                                        <span className="errorMsg">
                                          {errors.customer_type}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Formulation Assistant Access
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="fa_access"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.fa_access}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        {this.state.faAccess.map((status, i) => (
                                          <option key={i} value={status.id}>
                                            {status.name}
                                          </option>
                                        ))}
                                      </Field>
                                      {errors.fa_access &&
                                        touched.fa_access ? (
                                        <span className="errorMsg">
                                          {errors.fa_access}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Assign Team
                                        <span className="impField">*</span>
                                      </label>
                                      <Select
                                        isMulti
                                        name="teams"
                                        options={this.state.teamList}
                                        className="basic-multi-select"
                                        classNamePrefix="select"
                                        onChange={(option, e) => this.selectTeam(option, setFieldValue,isValid)}
                                        placeholder="Teams"
                                        onBlur={() => setFieldTouched("teams")}
                                        value={
                                          this.state.selectedTeamList
                                        }
                                      />
                                      {errors.teams && touched.teams ? (
                                        <span className="errorMsg">
                                          {errors.teams}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={12} md={6}>
                                    <div className="form-group">
                                      <label>Products</label>

                                      <Select
                                        isMulti
                                        name="products[]"
                                        options={this.state.productList}
                                        className="basic-multi-select"
                                        classNamePrefix="select"
                                        onChange={(evt) =>
                                          setFieldValue(
                                            "products",
                                            [].slice
                                              .call(evt)
                                              .map((val) => val.value)
                                          )
                                        }
                                        placeholder="Products"
                                        onBlur={() =>
                                          setFieldTouched("products")
                                        }
                                        defaultValue={
                                          this.state.selectedProductsList
                                        }
                                      />
                                      {errors.products && touched.products ? (
                                        <span className="errorMsg">
                                          {errors.products}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>


                                <Row>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Is Admin
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="is_admin"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.is_admin}
                                        onChange={(e) => {
                                          handleChange(e);
                                          this.onChangeAdminType(e);
                                        }}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        {this.state.isAdminType.map(
                                          (status, i) => (
                                            <option key={i} value={status.id}>
                                              {status.name}
                                            </option>
                                          )
                                        )}
                                      </Field>
                                      {errors.is_admin && touched.is_admin ? (
                                        <span className="errorMsg">
                                          {errors.is_admin}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Disable Notifications
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="dnd"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.dnd}
                                      >
                                        <option key="3" value="">
                                          Select
                                        </option>
                                        <option key="2" value="2">
                                          No
                                        </option>
                                        <option key="1" value="1">
                                          Yes
                                        </option>
                                      </Field>
                                      {errors.dnd && touched.dnd ? (
                                        <span className="errorMsg">
                                          {errors.dnd}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Exclude SPOC
                                        <span className="impField">*</span>
                                        <LinkWithTooltip
                                          tooltip="By excluding no request shall be sent to SPOC while reviewing"
                                          href="#"
                                          clicked={(e) => e.preventDefault()}
                                          id="tooltip-testdata"
                                        >
                                          {`  `}<i className="fas fa-exclamation-circle" />
                                        </LinkWithTooltip>
                                      </label>
                                      <Field
                                        name="ignore_spoc"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.ignore_spoc}
                                      >
                                        <option key="3" value="">
                                          Select
                                        </option>
                                        <option key="2" value="2">
                                          No
                                        </option>
                                        <option key="1" value="1">
                                          Yes
                                        </option>
                                      </Field>
                                      {errors.ignore_spoc && touched.ignore_spoc ? (
                                        <span className="errorMsg">
                                          {errors.ignore_spoc}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Multi Language Access
                                        <span className="impField">*</span>
                                        <LinkWithTooltip
                                          tooltip="By selecting 'NO' language selection feature will be disabled for this customer."
                                          href="#"
                                          clicked={(e) => e.preventDefault()}
                                          id="tooltip-testdata-2"
                                        >
                                          {`  `}<i className="fas fa-exclamation-circle" />
                                        </LinkWithTooltip>
                                      </label>
                                      <Field
                                        name="multi_lang_access"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.multi_lang_access}
                                      >
                                        <option key="3" value="">
                                          Select
                                        </option>
                                        <option key="2" value="2">
                                          No
                                        </option>
                                        <option key="1" value="1">
                                          Yes
                                        </option>
                                      </Field>
                                      {errors.multi_lang_access && touched.multi_lang_access ? (
                                        <span className="errorMsg">
                                          {errors.multi_lang_access}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                {this.state.isAdmin === true ? (
                                  <div>
                                    <Row>
                                      <Col xs={12} sm={12} md={6}>
                                        <div className="form-group">
                                          <label>
                                            Role
                                            <span className="impField">*</span>
                                          </label>
                                          <Select
                                            isMulti
                                            name="role[]"
                                            options={this.state.roleList}
                                            className="basic-multi-select"
                                            classNamePrefix="select"
                                            onChange={(evt) =>
                                              setFieldValue(
                                                "role",
                                                [].slice
                                                  .call(evt)
                                                  .map((val) => val.value)
                                              )
                                            }
                                            placeholder="Role"
                                            onBlur={() =>
                                              setFieldTouched("role")
                                            }
                                            defaultValue={
                                              this.state.selectedRoleList
                                            }
                                          />
                                          {errors.role && touched.role ? (
                                            <span className="errorMsg">
                                              {errors.role}
                                            </span>
                                          ) : null}
                                        </div>
                                        {/* <Field
                                            name="role"
                                            component="select"
                                            className={`selectArowGray form-control`}
                                            autoComplete="off"
                                            value={values.role}
                                          >
                                            <option key="-1" value="">
                                              Select
                                            </option>
                                            {this.state.roleList.map(
                                              (role, i) => (
                                                <option
                                                  key={i}
                                                  value={role.role_id}
                                                >
                                                  {role.role_name}
                                                </option>
                                              )
                                            )}
                                          </Field> 
                                          {errors.role && touched.role ? (
                                            <span className="errorMsg">
                                              {errors.role}
                                            </span>
                                          ) : null}*/}
                                      </Col>
                                      <Col xs={12} sm={6} md={6}>
                                        <div className="form-group">
                                          <label>
                                            Role Type
                                            <span className="impField">*</span>
                                          </label>
                                          <Field
                                            name="role_type"
                                            component="select"
                                            className={`selectArowGray form-control`}
                                            autoComplete="off"
                                            value={values.role_type}
                                          >
                                            <option key="-1" value="">
                                              Select
                                            </option>
                                            {this.state.roleType.map(
                                              (role_type, i) => (
                                                <option
                                                  key={i}
                                                  value={role_type.id}
                                                >
                                                  {role_type.name}
                                                </option>
                                              )
                                            )}
                                          </Field>
                                          {errors.role_type &&
                                            touched.role_type ? (
                                            <span className="errorMsg">
                                              {errors.role_type}
                                            </span>
                                          ) : null}
                                        </div>
                                      </Col>
                                    </Row>
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                              <button
                                className={`btn btn-success btn-sm ${(isValid || this.state.isValidState) ? "btn-custom-green" : "btn-disable"
                                  } m-r-10`}
                                type="submit"
                                disabled={(isValid || this.state.isValidState) ? false : true}
                              >
                                {this.state.customerflagId > 0
                                  ? isSubmitting
                                    ? "Updating..."
                                    : "Update"
                                  : isSubmitting
                                    ? "Submitting..."
                                    : "Submit"}
                              </button>
                              <button
                                onClick={(e) => this.modalCloseHandler()}
                                className={`btn btn-danger btn-sm`}
                                type="button"
                              >
                                Close
                              </button>
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>

                </div>
              </div>
            </section>
          </div>
          {
            this.state.showCustomerSoldToPopup && (
              <CustomerSoldToPopup
                showCustomerSoldToPopup={this.state.showCustomerSoldToPopup}
                handleCloseCustomerSoldToPopup={this.handleCloseCustomerSoldToPopup}
                company_id={this.state.company_id}
                company_name={this.state.value}
              />
            )
          }
        </Layout>
      );
    }
  }
}

export default Customer;
