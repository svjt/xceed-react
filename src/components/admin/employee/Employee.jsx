import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal
} from "react-bootstrap";
import { Link } from "react-router-dom";
import API from "../../../shared/admin-axios";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import Layout from "../layout/Layout";
import whitelogo from '../../../assets/images/drreddylogo_white.png';
import {showErrorMessage} from "../../../shared/handle_error";
import Pagination from "react-js-pagination";
import Select from "react-select";
import { htmlDecode } from '../../../shared/helper';
import jsonpath from "jsonpath";
import { getSuperAdmin, getAdminGroup } from '../../../shared/helper';

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const actionFormatter = refObj => cell => {
  return (
    <div className="actionStyle">
      <LinkWithTooltip
        tooltip="Click to view Employee’s tasks"
        href={`/admin/employees/tasks/${cell}`}
        id="tooltip-1"
      >
        <i className="fas fa-tasks" />
      </LinkWithTooltip>  
      {refObj.state.access.edit === true ?
      <LinkWithTooltip
        tooltip="Change Designation"
        href="#"
        clicked={e => refObj.changeDesignation(e, cell)}
        id="tooltip-1"
      >
        <i className="fa fa-redo" />
      </LinkWithTooltip> 
      : null }
      {refObj.state.access.edit === true ?
      <LinkWithTooltip
        tooltip="Click to Edit"
        href="#"
        clicked={e => refObj.modalShowHandler(e, cell)}
        id="tooltip-1"
      >
        <i className="far fa-edit" />
      </LinkWithTooltip>
      : null }
      
      {refObj.state.access.delete === true ?
      <LinkWithTooltip
        tooltip="Click to Delete"
        href="#"
        clicked={e => refObj.confirmDelete(e, cell)}
        id="tooltip-1"
      >
        <i className="far fa-trash-alt" />
      </LinkWithTooltip>
      : null }
         
    </div>
  );
};

const initialValues = {
  first_name   : "",
  last_name    : "",
  emp_ref      : "",
  email        : "",
  desig_id     : "",
  dept_id      : "",
  manager_id   : "",
  abilityToReopen : "",
  language_code:"",
  password     : "",
  conf_password: "",
  country_id: "",
  plant_code : ""
};

const custStatus = refObj => cell => {
  return cell === 1 ? "Active" : "Inactive";
};

const loginDate = refObj => cell => {
  if(cell === null){
    return '-';
  }else{
    return cell;
  }
}

const isSpoc = refObj => cell => {
  if(cell === 1){
    return 'Yes';
  }else{
    return 'No';
  }
}


const __htmlDecode = refObj => cell => {
  return htmlDecode(cell);
}

class Employee extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading      : true,
      employees      : [],
      employeeDetails: [],
      desigList      : [],
      deptList       : [],
      plantList : [] ,
      OtherEmployee  : [],
      languageList:[],
      employeeflagId : 0,
      selectStatus   : [
        { id: "0", name: "Inactive" },
        { id: "1", name: "Active" }
      ],
      selectQaLogin   : [
        { id: "0", name: "No" },
        { id: "1", name: "Yes" }
      ],
      selectXceedLogin   : [
        { id: "0", name: "No" },
        { id: "1", name: "Yes" }
      ],
      selectLevel     : [
        { id: "1", name: "General" },
        { id: "2", name: "L+1" },
        { id: "3", name: "L+2" }
      ],
      abilityToRepopen     : [
        { id: "1", name: "Yes" },
        { id: "0", name: "No" }
      ],
      showModal      : false,
      activePage     : 1,
      totalCount     : 0,
      itemPerPage    : 20,
      showModalLoader: false,
      search_name: '',
      search_email: '',
      search_desig: '',
      search_status: '',
      remove_search: false,
      showMyCustomerPopup:false,
      selected_customers:[],
      posted_customers:[],
      my_cust_emp_id:0,
      regionList: [],
      get_access_data:false,
      show_lang:false,
      show_plant : false ,
      countryList: [],
      bm_arr:[],
      myCustomerBMHTML:'',
      show_bm_tab:false,
      filter_search:false
    };
  }

  componentDidMount() {
    //this.getEmployeeList(); 
    //this.designationList();  
    const superAdmin  = getSuperAdmin(localStorage.admin_token);
    
    if(superAdmin === 1){
      this.setState({
        access: {
           view : true,
           add : true,
           edit : true,
           delete : true
         },
         get_access_data:true
     });
     this.getEmployeeList(); 
     this.designationList();  
    }else{
      const adminGroup  = getAdminGroup(localStorage.admin_token);
      API.get(`/api/adm_group/single_access/${adminGroup}/${'EMPLOYEES_MANAGEMENT'}`)
      .then(res => {
        this.setState({
          access: res.data.data,
          get_access_data:true
        }); 

        if(res.data.data.view === true){
          this.getEmployeeList(); 
          this.designationList();  
        }else{
          this.props.history.push('/admin/dashboard');
        }
        
      })
      .catch(err => {
        showErrorMessage(err,this.props);
      });
    }  
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getEmployeeList(pageNumber > 0 ? pageNumber  : 1);
  };

  getEmployeeList(page = 1) {

    let empname     = this.state.search_name
    let email       = this.state.search_email
    let designation = this.state.search_desig
    let status      = this.state.search_status

    API.get(`/api/employees?page=${page}&empname=${encodeURIComponent(empname)}&email=${encodeURIComponent(email)}&designation=${encodeURIComponent(designation)}&status=${encodeURIComponent(status)}`)
      .then(res => {
        this.setState({
          employees: res.data.data,
          count_user: res.data.count_user,
          isLoading: false,
          search_name: empname,
          search_email: email,
          search_desig: designation,
          search_status: status
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        showErrorMessage(err,this.props);
      });
  }

  getAllEmployeeList(){
    API.get("/api/employees/all")
      .then(res => {
        var empDropdown = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          empDropdown.push({
            value: element["employee_id"],
            label: element["first_name"] +" "+ element["last_name"] +" ("+ element["desig_name"] +")"
          });
        }
        this.setState({
          empDropdown: empDropdown
        });
      })
      .catch(err => {
        showErrorMessage(err,this.props);
      });
  } 

  designationList() {    
    API.get("/api/employees/designations")
      .then(res => {
        var all_designation = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          if(element['desig_id']!=6){
            all_designation.push(element);
          }
        }
        this.setState({
          desigList: all_designation,
          isLoading: false
        });
      })
      .catch(err => {
        showErrorMessage(err,this.props);
      });
  }
  getPlantList() {    
    API.get("/api/employees/coa_plant_list")
      .then(res => {
        var all_plants = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
            all_plants.push({
              value: element["id"],
              label: element["plant_code"]
            }
            
            );
        }
        this.setState({
          plantList: all_plants,
          isLoading: false
        });
      })
      .catch(err => {
        showErrorMessage(err,this.props);
      });
  }

  departmentList() {
    API.get("/api/employees/departments")
      .then(res => {
        this.setState({
          deptList: res.data.data,
          isLoading: false
        });
      })
      .catch(err => {
        showErrorMessage(err,this.props);
      });
  }

  getOtherEmployeeList = id => {  
    var empDropdown = [];  
    API.get(`/api/employees/other/${id}`)
      .then(res => { 
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          empDropdown.push({
            value: element["employee_id"],
            label: element["first_name"] +" "+ element["last_name"] +" ("+ element["desig_name"] +")" 
          });
        }       
        this.setState({
          empDropdown: empDropdown,
          isLoading: false
        });
      })
      .catch(err => {
        showErrorMessage(err,this.props);
      });
  };

  getRegionList(){
    API.get("/api/feed/adm_region_list")
    .then(res => {
      var regionArr = [];
      for (let index = 0; index < res.data.data.length; index++) {
        const element = res.data.data[index];
        regionArr.push({
          value: element["region_id"],
          label: htmlDecode(element["region_name"])
        });
      }
      this.setState({
        regionList: regionArr
      });
    })
    .catch(err => {
      showErrorMessage(err,this.props);
    });
  }

  getCountryList() {
    API.get("/api/customers/country")
      .then((res) => {
        this.setState({
          countryList: res.data.data,
        });
      })
      .catch((err) => {
        showErrorMessage(err, this.props);
      });
  }

  modalCloseHandler = () => {
    this.setState({ employeeflagId: 0 });
    this.setState({ showModal: false });
  };

  getLanguageList() {
    API.get("/api/feed/adm_language_eng_exclude")
      .then((res) => {
        this.setState({
          languageList: res.data.data,
        });
      })
      .catch((err) => {
        showErrorMessage(err, this.props);
      });
  }

  closeMyCustomerPopup = () => {
    this.setState({ showMyCustomerPopup: false });
  };

  modalShowHandler = (event, id) => {
    this.getPlantList();
    this.getAllEmployeeList();
    this.designationList();
    this.departmentList();
    this.getRegionList();
    this.getLanguageList();
    this.getCountryList();
    if (id) {
      event.preventDefault();
      this.getOtherEmployeeList(id);
      var selectedDropdown = [];
      var selMyRegion = [];
      var selRegion = [];
      API.get(`/api/employees/${id}`)
        .then(res => {
          if(res.data.data.manager_id > 0){
            selectedDropdown.push({
              value: res.data.data.manager_id,
              label: res.data.data.manager_fname+' '+res.data.data.manager_lname+' ('+ res.data.data.desig_name +')'
            });
          }

          for (let index = 0; index < res.data.data.region.length; index++) {
            const element = res.data.data.region[index];
            selMyRegion.push({
              value: element["region_id"],
              label: htmlDecode(element["region_name"])
            });
            selRegion.push( element["region_id"] );
          }
          
          this.setState({
            employeeflagId  : id,
            employeeDetails : res.data.data,
            selectedDropdown: selectedDropdown,
            selectedRegionList: selMyRegion, 
            selectedRegion: selRegion,
            isLoading       : false,
            showModal       : true
          });

          if(res.data.data.is_spoc == 1){
            this.setState({show_lang:true});
          }else{
            this.setState({show_lang:false});
          }
          if(res.data.data.desig_id == 7){
            this.setState({show_plant:true});
          }else{
            this.setState({show_plant:false});
          }

        })
        .catch(err => {
          showErrorMessage(err,this.props);
        });
    } else {
      this.setState({ 
        employeeDetails: [], 
        selectedDropdown: '',
        selectedRegionList: '', 
        selectedRegion: '',
        showModal: true,
        show_lang:false,
        show_plant : false
      });
    }
  };

  changeDesignation = (event, id) => {
    event.preventDefault();
    this.getCustomerList(id);
  }

  changeTabDesignation = (id) => {
    this.getCustomerList(id);
  }

  getCustomerList = (id) => {
    this.setState({myCustomerHTML:'',myCustomerBMHTML:'',bm_arr:[],selected_customers:[],posted_customers:[],bm_id:0,filter_search:false});
    
    API.get(`/api/employees/my_customers/${id}`)
    .then(res => {
      if(res.data.data.length > 0){
        this.setState({my_cust_emp_id:id,designation:res.data.designation,show_bm_tab:res.data.show_tab});
        this.getCustomerHtml(res.data.data,res.data.employees);
      }else{
        swal({
          closeOnClickOutside: false,
          title: "Sorry",
          text: "No records found!",
          icon: "error"
        });
      }
    })
    .catch(err => {
      showErrorMessage(err,this.props);
    });
  }

  getCustomerHtml = (customer_list,employee_list) => {
    let html = customer_list.map((cust,i)=>{
      return (
        <>
          <tr>
            <td width="30">
              <Field
                type="checkbox"
                name="samplesCheck"
                id={`chk_${cust.customer_id}`}
                defaultChecked={true}
                onChange={(e) => this.toggleCheckBox(cust.customer_id)}
              />
            </td>
            <td>{`${cust.first_name} ${cust.last_name} (${cust.company_name})`}</td>
          </tr>
          <tr id={`sel_emp_${cust.customer_id}`} style={{display:'none'}}>
            <td colSpan="2">
              <div className="form-group">
                <label>
                  Select Employee
                </label>

                <Select
                  name={`replace_employee_${cust.customer_id}`}
                  id={`replace_employee_${cust.customer_id}`}
                  options={employee_list}
                  className="basic-multi-select"
                  classNamePrefix="select"
                  onChange={(e) => this.selectCustEmployee(e,cust.customer_id)}
                />
              </div>
            </td>
          </tr>
        </>
      );
    });

    let new_html = <Row>
      <Col xs={12} sm={12} md={12}>
        <table class="table">
          <tbody>
            <tr>
              <td width="30">
                <Field
                  type="checkbox"
                  name="allCheck"
                  id="allCheck"
                  defaultChecked={true}
                  onChange={(e) => this.toggleCheckBoxAll(customer_list)}
                />
              </td>
              <td>All</td>
            </tr>
            <tr id={`sel_emp_all`} style={{display:'none'}}>
              <td colSpan="2">
                <div className="form-group">
                  <label>
                    Select Employee
                  </label>

                  <Select
                    name={`replace_employee_all`}
                    id={`replace_employee_all`}
                    options={employee_list}
                    className="basic-multi-select"
                    classNamePrefix="select"
                    onChange={(e) => this.selectCustEmployeeAll(e,customer_list)}
                  />
                </div>
              </td>
            </tr>
            {html}
          </tbody>
        </table>
      </Col>
    </Row>;

    this.setState({myCustomerHTML:new_html,showMyCustomerPopup:true});
  }

  getCustomerBMList = (id) => {
    this.setState({myCustomerHTML:'',selected_customers:[],posted_customers:[]});
    
    API.get(`/api/employees/my_customers_bm/${id}/${this.state.my_cust_emp_id}`)
    .then(res => {
      console.log(res.data.data.length);
      if(res.data.data.length > 0){
        this.getCustomerBMHtml(res.data.data,res.data.employees);
      }else{
        swal({
          closeOnClickOutside: false,
          title: "Sorry",
          text: "No records found!",
          icon: "error"
        });
      }
    })
    .catch(err => {
      showErrorMessage(err,this.props);
    });
  }

  getCustomerBMHtml = (customer_list,employee_list) => {
    console.log(customer_list,employee_list);
    let html = customer_list.map((cust,i)=>{
      return (
        <>
          <tr>
            <td width="30">
              <Field
                type="checkbox"
                name="samplesCheck"
                id={`chk_${cust.customer_id}`}
                defaultChecked={true}
                onChange={(e) => this.toggleCheckBox(cust.customer_id)}
              />
            </td>
            <td>{cust.first_name} {cust.last_name}{`  `}</td>
          </tr>
          <tr id={`sel_emp_${cust.customer_id}`} style={{display:'none'}}>
            <td colSpan="2">
              <div className="form-group">
                <label>
                  Select Employee
                </label>

                <Select
                  name={`replace_employee_${cust.customer_id}`}
                  id={`replace_employee_${cust.customer_id}`}
                  options={employee_list}
                  className="basic-multi-select"
                  classNamePrefix="select"
                  onChange={(e) => this.selectCustEmployee(e,cust.customer_id)}
                />
              </div>
            </td>
          </tr>
        </>
      );
    });

    let new_html = <Row>
      <Col xs={12} sm={12} md={12}>
        <table class="table">
          <tbody>
            <tr>
              <td width="30">
                <Field
                  type="checkbox"
                  name="allCheck"
                  id="allCheck"
                  defaultChecked={true}
                  onChange={(e) => this.toggleCheckBoxAll(customer_list)}
                />
              </td>
              <td>All</td>
            </tr>
            <tr id={`sel_emp_all`} style={{display:'none'}}>
              <td colSpan="2">
                <div className="form-group">
                  <label>
                    Select Employee
                  </label>

                  <Select
                    name={`replace_employee_all`}
                    id={`replace_employee_all`}
                    options={employee_list}
                    className="basic-multi-select"
                    classNamePrefix="select"
                    onChange={(e) => this.selectCustEmployeeAll(e,customer_list)}
                  />
                </div>
              </td>
            </tr>
            {html}
          </tbody>
        </table>
      </Col>
    </Row>;
    this.setState({myCustomerBMHTML:new_html});
  }
  
  toggleCheckBoxAll = (customer_list) => {
    var x = document.getElementById(`sel_emp_all`);
    if (window.getComputedStyle(x).display === "none") {
      x.style.display = '';
      document.getElementById(`replace_employee_all`).value = '';
      let arr = [];
      for (let index = 0; index < customer_list.length; index++) {
        const element = customer_list[index];
        document.getElementById(`chk_${element.customer_id}`).checked = false;
        document.getElementById(`chk_${element.customer_id}`).disabled = true;  
        arr.push(element.customer_id);      
      }
      this.setState({selected_customers:arr,posted_customers:[]});
    }else{
      x.style.display = 'none';
      for (let index = 0; index < customer_list.length; index++) {
        const element = customer_list[index];
        document.getElementById(`chk_${element.customer_id}`).checked = true;
        document.getElementById(`chk_${element.customer_id}`).disabled = false;
        
      }
      this.setState({posted_customers:[],selected_customers:[]});
    }

  }

  selectCustEmployeeAll = (event,customer_list) => {
    let arr = [];
    for (let index = 0; index < customer_list.length; index++) {
      const element = customer_list[index];
      arr.push({customer_id:element.customer_id,employee_id:event.value}); 
    }
    this.setState({posted_customers:arr},()=>{
      console.log('posted_customers',this.state.posted_customers);
    });
  }

  toggleCheckBox = (customer_id) => {
    var x = document.getElementById(`sel_emp_${customer_id}`);
    if (window.getComputedStyle(x).display === "none") {
      x.style.display = '';
      this.state.selected_customers.push(customer_id);
    }else{
      x.style.display = 'none';
      let index = this.state.selected_customers.indexOf(customer_id);

      if (index > -1) {
        this.state.selected_customers.splice(index, 1);
      }

      if(this.state.posted_customers.length > 0){
        for (let index2 = 0; index2 < this.state.posted_customers.length; index2++) {
          const element = this.state.posted_customers[index2];
          if(element.customer_id === customer_id){
            this.state.posted_customers.splice(index2, 1);
          }
        }
      }
      //console.log(document.getElementsByName(`replace_employee_${customer_id}`).value);
      document.getElementsByName(`replace_employee_${customer_id}`).value = '';
    }
  }

  selectCustEmployee = (event,customer_id) => {
    if(this.state.posted_customers.length > 0){
      for (let index = 0; index < this.state.posted_customers.length; index++) {
        const element = this.state.posted_customers[index];
        if(element.customer_id === customer_id){
          this.state.posted_customers.splice(index, 1);
        }
      }
    }
    this.state.posted_customers.push({customer_id:customer_id,employee_id:event.value});
    //console.log(this.state.posted_customers);
  }

  changeManager = (event,setFieldValue) => {
    if(event === null){
      setFieldValue("manager_id", "0");
    }else{
      setFieldValue("manager_id", event.value.toString());
    }      
  }

  changeBM = (event,setFieldValue) => {
    if(event === null){
      setFieldValue("bm_id", "0");
      this.setState({myCustomerHTML:'',myCustomerBMHTML:'',selected_customers:[],posted_customers:[],bm_id:0,filter_search:false});
    }else{
      setFieldValue("bm_id", event.value.toString());
      this.setState({myCustomerHTML:'',myCustomerBMHTML:'',selected_customers:[],posted_customers:[],bm_id:event.value,filter_search:false});
      this.getCustomerBMList(event.value);
    }      
  }

  handleSubmitEvent = (values, actions) => {
    let manager_id;
    if(values.manager_id.value > 0 ){
      manager_id = values.manager_id.value;
    }else{
      manager_id = values.manager_id;
    }
    // if(values.qa_login == 0){
    //   values.plant_code = 0
    // }
    if(values.desig_id != 7){
      values.plant_code = 0
    }
    
    const post_data = {
      first_name      : values.first_name,
      last_name       : values.last_name,
      emp_ref         : values.emp_ref,
      language_code   : values.language_code,
      manager_id      : manager_id,
      email           : values.email,
      status          : values.status,
      level           : values.level,
      desig_id        : values.desig_id,
      is_spoc         : values.is_spoc,
      abilityToReopen : values.abilityToReopen,
      dept_id         : values.dept_id,
      region          : values.region,
      country_id      : values.country_id,
      plant           : values.plant_code
    };    
    
    if(this.state.employeeflagId){      

      if(values.password !== '' && values.password !== values.conf_password){
        actions.setErrors({conf_password:'Confirm password do not match'});
        actions.setSubmitting(false);
      }else{
        this.setState({ showModalLoader: true });
        if(values.password !== ''){
            post_data.password = values.password;
        }
        const id = this.state.employeeflagId;
        API.put(`/api/employees/${id}`, post_data)
        .then(res => {
          this.modalCloseHandler();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record updated successfully.",
            icon: "success"
          }).then(() => {
            this.setState({ showModalLoader: false });
            this.getEmployeeList(this.state.activePage);
          });
        })
        .catch(err => {
          this.setState({ showModalLoader: false });
          if(err.data.status === 3){
            this.setState({
              showModal: false
            });
            showErrorMessage(err,this.props);
          }else{
            actions.setErrors(err.data.errors);
            actions.setSubmitting(false);
          }
        });
      }
    }else{   

      if(values.password !== values.conf_password){
        actions.setErrors({conf_password:'Confirm password do not match'});
        actions.setSubmitting(false);
      }else{
        this.setState({ showModalLoader: true });
        post_data.password = values.password;
        API.post("/api/employees", post_data)
        .then(res => {
          this.modalCloseHandler();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record added successfully.",
            icon: "success"
          }).then(() => {
            this.setState({ activePage: 1, showModalLoader: false });
            this.getEmployeeList(this.state.activePage);
          });
        })
        .catch(err => {
          this.setState({ showModalLoader: false });
          if(err.data.status === 3){
            this.setState({
              showModal: false
            });
            showErrorMessage(err,this.props);
          }else{
            actions.setErrors(err.data.errors);
            actions.setSubmitting(false);
          }
        });
      }
    }
  };

  handleSubmitCustomerPopup = (values, actions) => {
    if(this.state.selected_customers.length > 0){
      var post_data = {
        selected_customers : this.state.selected_customers,
        posted_customers   : JSON.stringify(this.state.posted_customers),
        employee_id        : this.state.my_cust_emp_id
      };
      API.post(`/api/employees/change_team/`, post_data)
        .then(res => {
          this.closeMyCustomerPopup();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Records updated successfully.",
            icon: "success"
          }).then(() => {
            this.setState({ activePage: 1, showModalLoader: false,my_cust_emp_id:0});
            this.getEmployeeList(this.state.activePage);
          });
        })
        .catch(err => {
          this.setState({ showModalLoader: false });
          if(err.data.status === 3){
            this.setState({
              showModal: false
            });
            showErrorMessage(err,this.props);
          }else{
            actions.setErrors(err.data.errors);
            actions.setSubmitting(false);
          }
        });
    }else{
      actions.setErrors({message:'Please un-check atleast one customer'});
    }

  }

  handleSubmitBMPopup = (values, actions) => {
    if(this.state.selected_customers.length > 0){
      var post_data = {
        selected_customers : this.state.selected_customers,
        posted_customers   : JSON.stringify(this.state.posted_customers),
        employee_id        : this.state.my_cust_emp_id,

      };
      API.post(`/api/employees/change_team/`, post_data)
        .then(res => {
          this.closeMyCustomerPopup();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Records updated successfully.",
            icon: "success"
          }).then(() => {
            this.setState({ activePage: 1, showModalLoader: false,my_cust_emp_id:0});
            this.getEmployeeList(this.state.activePage);
          });
        })
        .catch(err => {
          this.setState({ showModalLoader: false });
          if(err.data.status === 3){
            this.setState({
              showModal: false
            });
            showErrorMessage(err,this.props);
          }else{
            actions.setErrors(err.data.errors);
            actions.setSubmitting(false);
          }
        });
    }else{
      actions.setErrors({message:'Please un-check atleast one customer'});
    }

  }

  confirmDelete = (event, id) => {
    event.preventDefault();
    swal({
      closeOnClickOutside: false,
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this!",
      icon: "warning",
      buttons: true,
      dangerMode: true
    }).then(willDelete => {
      if (willDelete) {
        this.deleteEmployee(id);
      }
    });
  };

  deleteEmployee = id => {
    if (id) {
      API.delete(`/api/employees/${id}`).then(res => {
        swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record deleted successfully.",
            icon: "success"
          }).then(() => {
          this.setState({ activePage: 1 });
          this.getEmployeeList(this.state.activePage);
        });
      }).catch(err => {          
        if(err.data.status === 3){
          this.setState({ closeModal: true });
          showErrorMessage(err,this.props);
        }
      });
    }
  };

  renderShowsTotal = (start, to, total) => {
    return (
      <span className="pageShow">
        Showing {start} to {to}, of {total} records
      </span>
    );
  };  

  employeeSearch = ( e ) => {
    e.preventDefault();
    var empname = document.getElementById('empname').value
    var email = document.getElementById('email').value
    var designation = document.getElementById('designation').value
    var status = document.getElementById('status').value
    var is_spoc = document.getElementById('is_spoc').value

    if(empname === "" && email === "" && designation === "" && status === "" && is_spoc === ""){
      return false;
    }
    
    API.get(`/api/employees?page=1&empname=${encodeURIComponent(empname)}&email=${encodeURIComponent(email)}&designation=${encodeURIComponent(designation)}&status=${encodeURIComponent(status)}&is_spoc=${encodeURIComponent(is_spoc)}`)
    .then(res => {
      this.setState({
        employees: res.data.data,
        count_user: res.data.count_user,
        isLoading: false,
        search_name: empname,
        search_email: email,
        search_desig: designation,
        search_status: status,
        remove_search: true
      });
    })
    .catch(err => {
      this.setState({
        isLoading: false
      });
      showErrorMessage(err,this.props);
    });
  }



  clearSearch = () => {
    document.getElementById('empname').value     = "";
    document.getElementById('email').value       = "";
    document.getElementById('designation').value = "";
    document.getElementById('status').value      = ""

    this.setState({
      search_name: "",
      search_email: "",
      search_desig: "",
      search_status: "",
      remove_search: false
    }, () => {
      this.getEmployeeList();
      this.setState({activePage: 1})
    })
  }

  downloadXLSX = (e) => {
    e.preventDefault();

    var empname = document.getElementById('empname').value;
    var email = document.getElementById('email').value;
    var designation = document.getElementById('designation').value;
    var status = document.getElementById('status').value; 

    API.get(`/api/employees/download?page=1&empname=${encodeURIComponent(empname)}&email=${encodeURIComponent(email)}&designation=${encodeURIComponent(designation)}&status=${encodeURIComponent(status)}`,{responseType: 'blob'})
    .then(res => { 
      let url    = window.URL.createObjectURL(res.data);
      let a      = document.createElement('a');
      a.href     = url;
      a.download = 'employees.xlsx';
      a.click();

    }).catch(err => {
      showErrorMessage(err,this.props);
    });
  }

  checkHandler = (event) => {
    event.preventDefault();
  };

  handleTabs = (event) =>{  
        
    if(event.currentTarget.className === "active" ){
        //DO NOTHING
    }else{

        var elems          = document.querySelectorAll('[id^="tab_"]');
        var elemsContainer = document.querySelectorAll('[id^="show_tab_"]');
        var currId         = event.currentTarget.id;

        for (var i = 0; i < elems.length; i++){
            elems[i].classList.remove('active');
        }

        for (var j = 0; j < elemsContainer.length; j++){
            elemsContainer[j].style.display = 'none';
        }

        event.currentTarget.classList.add('active');
        event.currentTarget.classList.add('active');
        document.querySelector('#show_'+currId).style.display = 'block';
        this.setState({tabsClicked:currId});

        if(currId === 'tab_2'){
          this.setState({myCustomerHTML:'',myCustomerBMHTML:'',bm_arr:[],selected_customers:[],posted_customers:[],bm_id:0,filter_search:false});
          API.get(`/api/employees/my_bms/${this.state.my_cust_emp_id}`)
          .then(res => {
            if(res.data.select.length > 0){
              this.setState({bm_arr:res.data.select});
            }else{
              swal({
                closeOnClickOutside: false,
                title: "Sorry",
                text: "No records found!",
                icon: "error"
              });
            }
          })
          .catch(err => {
            //showErrorMessage(err,this.props);
          });
        }else if(currId === 'tab_1'){
          this.changeTabDesignation(this.state.my_cust_emp_id);
        }

    }
  }

  filterCustomerHTML = (event,blank_search=false) => {
    event.preventDefault();

    let search_name = '';

    if(blank_search === true){
      document.getElementById('search_name').value = '';
      this.setState({filter_search:false,myCustomerHTML:'',myCustomerBMHTML:'',bm_arr:[],selected_customers:[],posted_customers:[],bm_id:0});
    }else{
      search_name = document.getElementById('search_name').value;
      this.setState({filter_search:true,myCustomerHTML:'',myCustomerBMHTML:'',bm_arr:[],selected_customers:[],posted_customers:[],bm_id:0});
    }
    
    API.get(`/api/employees/my_customers/${this.state.my_cust_emp_id}?f=${search_name}`)
    .then(res => {
      if(res.data.data.length > 0){
        this.setState({designation:res.data.designation,show_bm_tab:res.data.show_tab});
        this.getCustomerHtml(res.data.data,res.data.employees);
      }else{
        swal({
          closeOnClickOutside: false,
          title: "Sorry",
          text: "No records found!",
          icon: "error"
        });
      }
    })
    .catch(err => {
      showErrorMessage(err,this.props);
    });

  }

  filterCustomerBMHTML = (event,blank_search=false) => {
    event.preventDefault();

    console.log('blank_search',blank_search);

    let search_name_bm = '';

    if(blank_search === true){
      document.getElementById('search_name_bm').value = '';
      this.setState({filter_search:false,myCustomerHTML:'',myCustomerBMHTML:'',selected_customers:[],posted_customers:[]});
    }else{
      search_name_bm = document.getElementById('search_name_bm').value;
      this.setState({filter_search:true,myCustomerHTML:'',myCustomerBMHTML:'',selected_customers:[],posted_customers:[]});
    }
    
    API.get(`/api/employees/my_customers_bm/${this.state.bm_id}/${this.state.my_cust_emp_id}?f=${search_name_bm}`)
    .then(res => {
      if(res.data.data.length > 0){
        this.getCustomerBMHtml(res.data.data,res.data.employees);
      }else{
        swal({
          closeOnClickOutside: false,
          title: "Sorry",
          text: "No records found!",
          icon: "error"
        });
      }
    })
    .catch(err => {
      showErrorMessage(err,this.props);
    });

  }

  render() {
    
    const { employeeDetails } = this.state;
    const newInitialValues = Object.assign(initialValues, {
      first_name: employeeDetails.first_name ? htmlDecode(employeeDetails.first_name) : "",
      last_name: employeeDetails.last_name ? htmlDecode(employeeDetails.last_name) : "",
      emp_ref: employeeDetails.emp_ref ? htmlDecode(employeeDetails.emp_ref) : "",
      manager_id: employeeDetails.manager_id || +employeeDetails.manager_id === 0 ? employeeDetails.manager_id.toString() : "0",     
      language_code: employeeDetails.language_code
        ? employeeDetails.language_code.toString()
        : "en",  
      email: employeeDetails.email ? htmlDecode(employeeDetails.email) : "",
      status: employeeDetails.status || +employeeDetails.status === 0 ? employeeDetails.status.toString() : "",
      level: employeeDetails.level ? employeeDetails.level.toString() : "",
      desig_id: employeeDetails.desig_id ? employeeDetails.desig_id.toString() : "",
      is_spoc: employeeDetails.is_spoc ? employeeDetails.is_spoc.toString() : "",
      dept_id: employeeDetails.dept_id ? employeeDetails.dept_id.toString() : "",
      abilityToReopen: employeeDetails.ability_to_reopen || +employeeDetails.ability_to_reopen === 0  ? employeeDetails.ability_to_reopen.toString() : "",
      password:"",
      region: this.state.selectedRegion ? this.state.selectedRegion : "[]",
      country_id: employeeDetails.country_id ? employeeDetails.country_id.toString() : "",
      //qa_login: employeeDetails.qa_login || +employeeDetails.qa_login === 0 ? employeeDetails.qa_login.toString() : "",
      plant_code: employeeDetails.plant ? employeeDetails.plant.toString() : "",
     // xceed_login:employeeDetails.xceed_login || +employeeDetails.xceed_login === 0  ? employeeDetails.xceed_login.toString() : ""
    });
    
    let validateStopFlag = null;
    if (this.state.employeeflagId > 0) {
      validateStopFlag = Yup.object().shape({
        first_name: Yup.string().trim()
          .required("Please enter first name")
          .min(1, "First name can be minimum 1 characters long")  
          .matches(/^[A-Za-z0-9\s]*$/, "Invalid first name format! Only alphanumeric and spaces are allowed")      
          .max(30, "First name can be maximum 30 characters long"),
        last_name: Yup.string().trim()
          .required("Please enter last name")
          .min(1, "Last name can be minimum 1 characters long")  
          .matches(/^[A-Za-z0-9\s]*$/, "Invalid last name format! Only alphanumeric and spaces are allowed")      
          .max(30, "Last name can be maximum 30 characters long"),
        email: Yup.string().trim()
          .required("Please enter email")
          .email("Invalid email")
          .max(80, "Email can be maximum 80 characters long"),
        emp_ref: Yup.string().trim()
          .required("Please enter employee reference")
          .min(2, "Employee reference can be minimum 2 characters long")
          .matches(/^[A-Za-z0-9]*$/, "Invalid format! Only alpha-numeric characters are allowed")
          .max(14, "Employee reference can be maximum 14 characters long"),
        status: Yup.string().trim()
          .required("Please select status")
          .matches(/^[0|1]$/, "Invalid status selected"),
        language_code: Yup.string().trim().required("Please select language"),
        level: Yup.string().trim()
          .required("Please select level")
          .matches(/^[1|2|3]$/, "Invalid level selected"),
        desig_id: Yup.string().trim()
          .required("Please select designation"),
        is_spoc: Yup.string().trim()
          .required("Please select spoc"),  
        dept_id: Yup.string().trim()
          .required("Please select department"),
        abilityToReopen: Yup.string().trim()
          .required("Please select a value"),  
        password: Yup.string().trim().notRequired().test('password', 'Password can be minimum 4 characters and maximum 12 characters long', function(value) {
          if (value !== '') {
            const schema = Yup.string().min(4).max(12);
            return schema.isValidSync(value);
          }
          return true;
        }),
        country_id: Yup.string().trim().required("Please select country"),

      });
    }else{
      validateStopFlag = Yup.object().shape({
        first_name: Yup.string().trim()
          .required("Please enter first name")
          .min(1, "First name can be minimum 1 characters long")  
          .matches(/^[A-Za-z0-9\s]*$/, "Invalid first name format! Only alphanumeric and spaces are allowed")      
          .max(30, "First name can be maximum 30 characters long"),
        last_name: Yup.string().trim()
          .required("Please enter last name")
          .min(1, "Last name can be minimum 1 characters long")  
          .matches(/^[A-Za-z0-9\s]*$/, "Invalid last name format! Only alphanumeric and spaces are allowed")      
          .max(30, "Last name can be maximum 30 characters long"),
        email: Yup.string().trim()
          .required("Please enter email")
          .email("Invalid email")
          .max(80, "Email can be maximum 80 characters long"),
        emp_ref: Yup.string().trim()
          .required("Please enter employee reference")
          .min(2, "Employee reference can be minimum 2 characters long")
          .matches(/^[A-Za-z0-9]*$/, "Invalid format! Only alpha-numeric characters are allowed")
          .max(14, "Employee reference can be maximum 14 characters long"),
        status: Yup.string().trim()
          .required("Please select status")
          .matches(/^[0|1]$/, "Invalid status selected"),
        level: Yup.string().trim()
          .required("Please select level")
          .matches(/^[1|2|3]$/, "Invalid level selected"),
        desig_id: Yup.string().trim()
          .required("Please select designation"),
        is_spoc: Yup.string().trim()
          .required("Please select spoc"),
        dept_id: Yup.string().trim()
          .required("Please select department"),
        abilityToReopen: Yup.string().trim()
          .required("Please select a value"),  
        password: Yup.string().trim()          
          .min(4, "Password can be minimum 4 characters long")
          .max(12, "Password can be maximum 12 characters long"),
        conf_password: Yup.string()
          .required("Please confirm password")
          .test('match', 'Confirm password do not match', 
              function(confirmPassword) { 
                return confirmPassword === this.parent.password; 
              }),
        country_id: Yup.string().trim().required("Please select country")
      });
    }

    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy" >
                <img src={whitelogo}  alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      //console.log(this.state.access)
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">              
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <h1>
                    Manage Employees
                    <small />
                  </h1>

                </div>

                <div className="col-lg-12 col-sm-12 col-xs-12 topSearchSection">
                {this.state.access.add === true ? 
                  <div className="">
                    <button
                        type="button"
                        className="btn btn-info btn-sm"
                        onClick={e => this.modalShowHandler(e, "")}
                      >
                        <i className="fas fa-plus m-r-5" /> Add Employee
                      </button>
                  </div>
                : null }               
                  
                <form className="form">
    
                <div className="">
                <input
                            className="form-control"
                            name="empname"
                            id="empname"
                            placeholder="Filter by name"
                          />
                </div>

                <div className="">
                <input
                            className="form-control"
                            name="email"
                            id="email"
                            placeholder="Filter by email"
                          />
                </div>

                <div className="">
                <select name="designation" id="designation" className="form-control">
                            <option value="">Select designation</option>
                            {this.state.desigList.map(
                              (designation, i) => (
                                <option
                                  key={i}
                                  value={designation.desig_id}
                                >
                                  {designation.desig_name}
                                </option>
                              )
                            )}
                          </select>
                </div>

                <div className="">
                <select name="status" id="status" className="form-control">
                            <option value="">Select status</option>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                          </select>
                </div>

                <div className="">
                <select name="status" id="is_spoc" className="form-control">
                            <option value="">Is Spoc</option>
                            <option value="1">Yes</option>
                            <option value="2">No</option>
                          </select>
                </div>

                <div className="">
                <input
                            type="submit"
                            value="Search"
                            className="btn btn-warning btn-sm"
                            onClick={(e) => this.employeeSearch( e )}
                          />
                          {this.state.remove_search ? <a onClick={() => this.clearSearch()} className="btn btn-danger btn-sm"> Remove </a> : null}
                </div>

                <div className="clearfix"></div>
                </form>
                
                {/* <div className="clearfix"></div> */}



                  {/* <button
                    type="button"
                    className="btn btn-warning btn-sm btn-custom-green pull-right"
                    onClick={e => this.modalShowHandler(e, "")}
                  >
                    <i className="fas fa-plus m-r-5" /> Add Employee
                  </button> */}

                 {/* <div className="input-group">
                      <form className="form" id="addItemForm">
                           <input
                            className="form-control"
                            name="empname"
                            id="empname"
                            placeholder="Filter by name"
                          />
                          <input
                            className="form-control"
                            name="email"
                            id="email"
                            placeholder="Filter by email"
                          />
                          <select name="designation" id="designation">
                            <option value="">Select designation</option>
                            {this.state.desigList.map(
                              (designation, i) => (
                                <option
                                  key={i}
                                  value={designation.desig_id}
                                >
                                  {designation.desig_name}
                                </option>
                              )
                            )}
                          </select>
                          <select name="status" id="status">
                            <option value="">Select status</option>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                          </select>

                          <input
                            type="submit"
                            value="Search"
                            className="btn btn-warning btn-sm btn-custom-green pull-right"
                            onClick={(e) => this.employeeSearch( e )}
                          />
                          {this.state.remove_search ? <a onClick={() => this.clearSearch()}> Remove </a> : null}
                      </form>    
                  </div> */}

                </div>
                {/* <div className="clearfix"></div> */}
                {/* <div className="col-lg-12 col-sm-12 col-xs-12 mt-10">
                <button
                    type="button"
                    className="btn btn-info btn-sm"
                    onClick={e => this.modalShowHandler(e, "")}
                  >
                    <i className="fas fa-plus m-r-5" /> Add Employee
                  </button>
                </div> */}
              </div>
            </section>
            <section className="content">
              <div className="box">

               <div className="box-body">
                <div className="nav-tabs-custom">
<ul className="nav nav-tabs">
<li className="tabButtonSec pull-right">
{this.state.count_user > 0 ? 
<span onClick={(e) => this.downloadXLSX(e)} >
<LinkWithTooltip
tooltip={`Click here to download excel`}
href="#"
id="tooltip-my"
clicked={e => this.checkHandler(e)}
>
<i className="fas fa-download"></i>
</LinkWithTooltip>
</span>
: null }
</li>
</ul>
</div>
                  <BootstrapTable
                    data={this.state.employees}
                  >
                    <TableHeaderColumn isKey dataField="first_name" dataFormat={__htmlDecode(this)}>
                      First Name
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="last_name" dataFormat={__htmlDecode(this)}>
                      Last Name
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="email" dataFormat={__htmlDecode(this)}>
                      Email
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="desig_name">
                      Designation
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="dept_name">
                      Department
                    </TableHeaderColumn>     
                    <TableHeaderColumn dataField="display_login_date" dataFormat={loginDate(this)}>
                      Last Login Time
                    </TableHeaderColumn>  
                    <TableHeaderColumn
                      dataField="is_spoc"
                      dataFormat={isSpoc(this)}
                      width={"8%"}
                    >
                      Is Spoc
                    </TableHeaderColumn>  
                    <TableHeaderColumn
                      dataField="language"
                      dataFormat={__htmlDecode(this)}
                      width={"8%"}
                    >
                      Language
                    </TableHeaderColumn>              
                    <TableHeaderColumn dataField="status" dataFormat={custStatus(this)}>
                      Status
                    </TableHeaderColumn>
                    {this.state.access.view === true || this.state.access.edit === true || this.state.access.delete === true ?
                    <TableHeaderColumn
                      dataField="employee_id"
                      dataFormat={actionFormatter(this)}
                    >
                      Action
                    </TableHeaderColumn>
                    : null }
                  </BootstrapTable>

                  {this.state.count_user > 20 ? (
                    <Row>
                      <Col md={12}>
                        <div className="paginationOuter text-right">
                          <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemPerPage}
                            totalItemsCount={this.state.count_user}
                            itemClass='nav-item'
                            linkClass='nav-link'
                            activeClass='active'
                            onChange={this.handlePageChange}
                          />
                        </div>
                      </Col>
                    </Row>
                  ) : null}

                  {/* CHANGE TEAM */}
                  <Modal
                    show={this.state.showMyCustomerPopup}
                    onHide={() => this.closeMyCustomerPopup()} backdrop="static"
                  >
                    
                    <Modal.Header closeButton>
                      <Modal.Title>
                        Update Team
                      </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <div className="contBox">

                        <div className="nav-tabs-custom">

                          <ul className="nav nav-tabs">
                            <li className="active" onClick={(e) => this.handleTabs(e)} id="tab_1" >Update All</li>
                            {this.state.designation !== 1 && this.state.show_bm_tab === true && <li onClick={(e) => this.handleTabs(e)} id="tab_2" >Update selectively</li>}
                          </ul>

                          <div className="tab-content">
                              <div className="tab-pane active" id="show_tab_1">
                                <Row>
                                    <Col xs={12} sm={6} md={6}>
                                    <div className="">
                                        <input
                                            className="form-control"
                                            name="search_name"
                                            id="search_name"
                                            placeholder="Filter by User First Name or Last Name"
                                        />
                                    </div>    
                                    </Col>
                                    <Col xs={12} sm={3} md={3}>
                                      <input
                                          type="submit"
                                          value="Search"
                                          className="btn btn-warning btn-sm"
                                          onClick={(e) => this.filterCustomerHTML( e )}
                                      />
                                    </Col>
                                    {this.state.filter_search && <Col xs={12} sm={3} md={3}>
                                      <input
                                          type="submit"
                                          value="Reset"
                                          className="btn btn-danger btn-sm"
                                          onClick={(e) => this.filterCustomerHTML( e,true )}
                                      />
                                    </Col>}
                                </Row>
                              <Formik
                                onSubmit={this.handleSubmitCustomerPopup}
                              >
                              {({ values, errors, touched, isValid, isSubmitting, setFieldValue }) => {
                                  //console.log('manager_id', values.manager_id);
                                  return (
                                      <Form>
                                          {this.state.showModalLoader === true ? ( 
                                              <div className="loading_reddy_outer">
                                                  <div className="loading_reddy" >
                                                      <img src={whitelogo} alt="loader"/>
                                                  </div>
                                              </div>
                                          ) : null}

                                          {this.state.myCustomerHTML}
                                          {errors.message ? (
                                              <Row>
                                                  <Col xs={12} sm={12} md={12}>
                                                      <span className="errorMsg">
                                                      {errors.message}
                                                      </span>
                                                  </Col>
                                              </Row>
                                          ) : null}

                                          {this.state.myCustomerHTML != '' && <button
                                          className={`btn btn-success btn-sm btn-custom-green m-r-10`}
                                          type="submit"
                                          >
                                              Update
                                          </button>}

                                      </Form>
                                  );
                              }}
                              </Formik>
                              
                              </div>
                              <div className="tab-pane" id="show_tab_2">
                              {this.state.bm_arr.length > 0 && <><div className="contBox">
                                  <Formik
                                      onSubmit={this.handleSubmitBMPopup}
                                  >
                                  {({ values, errors, touched, isValid, isSubmitting, setFieldValue }) => {
                                      //console.log('manager_id', values.manager_id);
                                      return (
                                          <Form>
                                            <Row>
                                                <Col xs={12} sm={12} md={12}>
                                                <div className="form-group">
                                                  <label>Select BM</label>
                                                    <Select
                                                        className="basic-single"
                                                        classNamePrefix="select"
                                                        isClearable={true}
                                                        isSearchable={true}
                                                        name="bm_id"
                                                        options={this.state.bm_arr}
                                                        onChange={e => { this.changeBM(e,setFieldValue) }}                                            
                                                      />
                                                  {errors.bm_id && touched.bm_id ? (
                                                    <span className="errorMsg">
                                                      {errors.bm_id}
                                                    </span>
                                                  ) : null}
                                                </div>
                                                </Col>
                                            </Row>  

                                            {this.state.bm_id > 0 && <Row>
                                                <Col xs={12} sm={6} md={6}>
                                                <div className="">
                                                    <input
                                                        className="form-control"
                                                        name="search_name_bm"
                                                        id="search_name_bm"
                                                        placeholder="Filter by User First Name or Last Name"
                                                    />
                                                </div>    
                                                </Col>
                                                <Col xs={12} sm={3} md={3}>
                                                  <input
                                                      type="submit"
                                                      value="Search"
                                                      className="btn btn-warning btn-sm"
                                                      onClick={(e) => this.filterCustomerBMHTML( e )}
                                                  />
                                                </Col>
                                                {this.state.filter_search && <Col xs={12} sm={3} md={3}>
                                                  <input
                                                      type="submit"
                                                      value="Reset"
                                                      className="btn btn-danger btn-sm"
                                                      onClick={(e) => this.filterCustomerBMHTML( e,true )}
                                                  />
                                                </Col>}
                                            </Row>}

                                            {this.state.myCustomerBMHTML}

                                            {this.state.myCustomerBMHTML != '' && <button
                                              className={`btn btn-success btn-sm btn-custom-green m-r-10`}
                                              type="submit"
                                            >
                                                Update
                                            </button>}
                                          </Form>
                                      );
                                  }}
                                  </Formik>
                              </div>
                              </>}
                              </div>
                          </div>

                        </div>
                      </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button
                          onClick={e => this.closeMyCustomerPopup()}
                          className={`btn btn-danger btn-sm`}
                          type="button"
                        >
                          Close
                        </button>
                    </Modal.Footer>
                          
                  </Modal>  
                  {/* ======= Add/Edit Employee modal ======== */}
                  <Modal
                    show={this.state.showModal}
                    onHide={() => this.modalCloseHandler()} backdrop="static"
                  >
                    <Formik
                      initialValues={newInitialValues}
                      validationSchema={validateStopFlag}
                      onSubmit={this.handleSubmitEvent}
                    >
                      {({ values, errors, touched, isValid, isSubmitting, setFieldValue, setFieldTouched }) => {
                        //console.log('manager_id', values.manager_id);
                        return (
                          <Form>
                            {this.state.showModalLoader === true ? ( 
                                <div className="loading_reddy_outer">
                                    <div className="loading_reddy" >
                                        <img src={whitelogo} alt="loader"/>
                                    </div>
                                </div>
                              ) : ( "" )}
                            <Modal.Header closeButton>
                              <Modal.Title>
                                {this.state.employeeflagId > 0 ? "Edit" : "Add"}{" "}
                                Employee
                              </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="contBox">
                                <Row>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        First Name
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="first_name"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter first name"
                                        autoComplete="off"
                                      />
                                      {errors.first_name &&
                                      touched.first_name ? (
                                        <span className="errorMsg">
                                          {errors.first_name}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Last Name
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="last_name"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter last name"
                                        autoComplete="off"
                                      />
                                      {errors.last_name && touched.last_name ? (
                                        <span className="errorMsg">
                                          {errors.last_name}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>

                                <Row>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Employee Reference
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="emp_ref"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter employee reference"
                                        autoComplete="off"
                                      />
                                      {errors.emp_ref && touched.emp_ref ? (
                                        <span className="errorMsg">
                                          {errors.emp_ref}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>Manager</label>
                                        <Select
                                            className="basic-single"
                                            classNamePrefix="select"
                                            isClearable={true}
                                            isSearchable={true}
                                            name="manager_id"
                                            options={this.state.empDropdown}
                                            defaultValue={this.state.selectedDropdown}
                                            onChange={e => { this.changeManager(e,setFieldValue) }}                                            
                                          />
                                      {errors.manager_id && touched.manager_id ? (
                                        <span className="errorMsg">
                                          {errors.manager_id}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>

                                <Row>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Email<span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="email"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter email"
                                        autoComplete="off"
                                      />
                                      {errors.email && touched.email ? (
                                        <span className="errorMsg">
                                          {errors.email}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Designation
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="desig_id"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.desig_id}
                                        onChange={(e)=>{
                                          setFieldValue('desig_id',e.target.value);
                                          if(e.target.value == 7){
                                            this.setState({
                                              show_plant : true
                                            })
                                          }else{
                                            this.setState({
                                              show_plant : false
                                            })
                                          }
                                        }}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        {this.state.desigList.map(
                                          (designation, i) => (
                                            <option
                                              key={i}
                                              value={designation.desig_id}
                                            >
                                              {designation.desig_name}
                                            </option>
                                          )
                                        )}
                                      </Field>
                                      {errors.desig_id &&
                                      touched.desig_id ? (
                                        <span className="errorMsg">
                                          {errors.desig_id}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>

                                <Row>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        IS SPOC
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="is_spoc"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.is_spoc}
                                        onChange={(e)=>{
                                          setFieldValue('is_spoc',e.target.value);
                                          if(e.target.value == 1){
                                              this.setState({show_lang:true});
                                          }else{
                                              this.setState({show_lang:false});
                                          }
                                        }}
                                      >
                                        <option key="-1" value="0">
                                          Select
                                        </option>
                                        <option key="1" value="1">
                                          Yes
                                        </option>
                                        <option key="2" value="2">
                                          No
                                        </option>                                        
                                        
                                      </Field>
                                      {errors.is_spoc &&
                                      touched.is_spoc ? (
                                        <span className="errorMsg">
                                          {errors.is_spoc}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  {this.state.show_plant === true && <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Plant
                                        <span className="impField">*</span>
                                      </label>

                                      <Field
                                        name="plant_code"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.plant_code}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        {this.state.plantList.map(
                                          
                                          (plant, i) => (
                                            <option
                                              key={i}
                                              value={plant.value}
                                            >
                                              {plant.label}
                                            </option>
                                          )
                                        )}
                                      </Field>
                                      {errors.plant_code &&
                                      touched.plant_code ? (
                                        <span className="errorMsg">
                                          {errors.plant_code}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>}
                                </Row>

                                <Row>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Department
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="dept_id"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.dept_id}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        {this.state.deptList.map(
                                          (department, i) => (
                                            <option
                                              key={i}
                                              value={department.dept_id}
                                            >
                                              {department.dept_name}
                                            </option>
                                          )
                                        )}
                                      </Field>
                                      {errors.dept_id &&
                                      touched.dept_id ? (
                                        <span className="errorMsg">
                                          {errors.dept_id}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Status
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="status"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.status}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        {this.state.selectStatus.map(
                                          (status, i) => (
                                            <option key={i} value={status.id}>
                                              {status.name}
                                            </option>
                                          )
                                        )}
                                      </Field>
                                      {errors.status && touched.status ? (
                                        <span className="errorMsg">
                                          {errors.status}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>

                                {/* <Row>
                                  <Col xs={12} sm={6} md={6}>
                                  <div className="form-group">
                                      <label>
                                        Xceed Login
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="xceed_login"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.xceed_login}
                                      >
                                        <option key="-1" value="-1">
                                          Select
                                        </option>
                                        {this.state.selectXceedLogin.map(
                                          (xceed_login, i) => (
                                            <option key={i} value={xceed_login.id}>
                                              {xceed_login.name}
                                            </option>
                                          )
                                        )}
                                      </Field>
                                      {errors.xceed_login && touched.xceed_login ? (
                                        <span className="errorMsg">
                                          {errors.xceed_login}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        QA Login
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="qa_login"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.qa_login}
                                        onChange={(e)=>{
                                          setFieldValue('qa_login',e.target.value);
                                          if(e.target.value == 1){
                                              this.setState({show_plant:true});
                                          }else{
                                              this.setState({show_plant:false});
                                          }
                                        }}
                                      > 
                                        <option key="-1" value="-1">
                                          Select
                                        </option>
                                        {this.state.selectQaLogin.map(
                                          (qa_login, i) => (
                                            <option key={i} value={qa_login.id}>
                                              {qa_login.name}
                                            </option>
                                          )
                                        )}
                                      </Field>
                                      {errors.qa_login && touched.qa_login ? (
                                        <span className="errorMsg">
                                          {errors.qa_login}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row> */}

                                <Row>
                                <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Level
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="level"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.level}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        {this.state.selectLevel.map(
                                          (level, i) => (
                                            <option key={i} value={level.id}>
                                              {level.name}
                                            </option>
                                          )
                                        )}
                                      </Field>
                                      {errors.level && touched.level ? (
                                        <span className="errorMsg">
                                          {errors.level}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={6}>
                                  <div className="form-group">
                                      <label>
                                        Abiltiy to Reopen
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="abilityToReopen"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.abilityToReopen}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        {this.state.abilityToRepopen.map(
                                          (level, i) => (
                                            <option key={i} value={level.id}>
                                              {level.name}
                                            </option>
                                          )
                                        )}
                                      </Field>
                                      {errors.abilityToReopen && touched.abilityToReopen ? (
                                        <span className="errorMsg">
                                          {errors.abilityToReopen}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>                                                                   
                                </Row>
                                
                                <Row>
                                    {/* <Col xs={12} sm={6} md={6}>
                                      <div className="form-group">
                                        <label>Region</label>

                                        <Select
                                          isMulti
                                          name="region[]"
                                          options={this.state.regionList}
                                          className="basic-multi-select"
                                          classNamePrefix="select"
                                          onChange={evt =>
                                            setFieldValue(
                                              "region",
                                              [].slice
                                                .call(evt)
                                                .map(val => val.value)
                                            )
                                          }
                                          placeholder="Region"
                                          onBlur={() => setFieldTouched("region")}
                                          defaultValue={
                                            this.state.selectedRegionList
                                          }
                                        />
                                        {errors.region && touched.region ? (
                                          <span className="errorMsg">
                                            {errors.region}
                                          </span>
                                        ) : null}
                                      </div>
                                    </Col> */}

                                    {this.state.show_lang === true && <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Language
                                        <span className="impField">*</span>
                                      </label>

                                      <Field
                                        name="language_code"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.language_code}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        {this.state.languageList.map(
                                          (lang, i) => (
                                            <option
                                              key={i}
                                              value={lang.code}
                                            >
                                              {htmlDecode(lang.language)}
                                            </option>
                                          )
                                        )}
                                      </Field>
                                      {errors.language_code &&
                                      touched.language_code ? (
                                        <span className="errorMsg">
                                          {errors.language_code}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>}
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Country
                                        <span className="impField">*</span>
                                      </label>

                                      <Field
                                        name="country_id"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.country_id}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        {this.state.countryList.map(
                                          (country, i) => (
                                            <option
                                              key={i}
                                              value={country.country_id}
                                            >
                                              {htmlDecode(country.country_name)}
                                            </option>
                                          )
                                        )}
                                      </Field>
                                      {errors.country_id &&
                                      touched.country_id ? (
                                        <span className="errorMsg">
                                          {errors.country_id}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>

                                <Row>                                  
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Password
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="password"
                                        type="password"
                                        className={`form-control`}
                                        autoComplete="off"
                                      />
                                      {errors.password &&
                                      touched.password ? (
                                        <span className="errorMsg">
                                          {errors.password}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Confirm Password
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="conf_password"
                                        type="password"
                                        className={`form-control`}
                                        autoComplete="off"
                                      />
                                      {errors.conf_password && touched.conf_password ? (
                                        <span className="errorMsg">
                                          {errors.conf_password}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>                                   
                                </Row>

                                {errors.message ? (
                                    <Row>
                                      <Col xs={12} sm={12} md={12}>
                                        <span className="errorMsg">
                                          {errors.message}
                                        </span>
                                      </Col>
                                    </Row>
                                  ) : ( "" )
                                }
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <button
                                  className={`btn btn-success btn-sm ${
                                    isValid ? "btn-custom-green" : "btn-disable"
                                  } m-r-10`}
                                  type="submit"
                                  disabled={isValid ? false : true}
                                >
                                  {this.state.employeeflagId > 0
                                    ? isSubmitting
                                      ? "Updating..."
                                      : "Update"
                                    : isSubmitting
                                    ? "Submitting..."
                                    : "Submit"}
                                </button>
                                <button
                                  onClick={e => this.modalCloseHandler()}
                                  className={`btn btn-danger btn-sm`}
                                  type="button"
                                >
                                  Close
                                </button>
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>
                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}

export default Employee;
