import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal
} from "react-bootstrap";
import { Link } from "react-router-dom";
import API from "../../../shared/admin-axios";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import Select from "react-select";
import Layout from "../layout/Layout";
//import DashboardSearch from "./DashboardSearch";
import whitelogo from '../../../assets/images/drreddylogo_white.png';
import {showErrorMessage} from "../../../shared/handle_error";
import Pagination from "react-js-pagination";
import { htmlDecode } from '../../../shared/helper';
import { getSuperAdmin, getAdminGroup } from '../../../shared/helper';

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
const actionFormatter = refObj => cell => {
  return (
    <div className="actionStyle">
        
     
     {refObj.state.access.edit === true ?
      <LinkWithTooltip
        tooltip="Click to Edit"
        href="#"
        clicked={e => refObj.modalShowHandler(e, cell)}
        id="tooltip-1"
      >
        <i className="far fa-edit" />
      </LinkWithTooltip>
      : null }      
    </div>
  );
};
/*For Tooltip*/

const __htmlDecode = refObj => cell => {
  return htmlDecode(cell);
}


const visiblility = refObj => cell => {
  console.log(cell);
  let visiblility = '';
  if(cell == 1){
    visiblility = 'Both'
  }else if(cell == 2){
    visiblility = 'SAP New Order'
  }else{
    visiblility = 'Non SAP Form'
  }
  return visiblility;
}
const initialValues = {
  sku_code: '',
  new_order: ''
};

class Products extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      company: [],
      company_list:[],
      product_details : [],
      selectvisibility: [
        { new_order: "0", name: "Non SAP Form " },
        { new_order: "1", name: "Both" },
        { new_order: "2", name: "SAP New Order" }
      ],
      activePage      : 1,
      totalCount      : 0,
      itemPerPage     : 20,
      flagId: 0,
      showModal: false,
      showModalLoader : false,
      search_name: '',
      sku_code:'',
      new_order:'',
      remove_search: false,
      child_details : false,
      master_list : [],
      selectedParent: "",
      selectedOther : "",
      otherCompany: [],
      exchangeCompany : true,
      get_access_data:false
    }
  }

  componentDidMount() {
    //this.getProductList();
    const superAdmin  = getSuperAdmin(localStorage.admin_token);
    
    if(superAdmin === 1){
      this.setState({
        access: {
           view : true,
           add : true,
           edit : true,
           delete : true
         },
         get_access_data:true
     });
     this.getProductList();
    }else{
      const adminGroup  = getAdminGroup(localStorage.admin_token);
      API.get(`/api/adm_group/single_access/${adminGroup}/${'PRODUCT_MANAGEMENT'}`)
      .then(res => {
        this.setState({
          access: res.data.data,
          get_access_data:true
        }); 

        if(res.data.data.view === true){
          this.getProductList();
        }else{
          this.props.history.push('/admin/dashboard');
        }
        
      })
      .catch(err => {
        showErrorMessage(err,this.props);
      });
    } 
  }
  handleSubmitEvent = (values, actions) => {
    const post_data = {
      sku_code: values.sku_code,
      new_order: values.new_order,
    };

    if (this.state.flagId) {
      this.setState({ showModalLoader: true });
      const id = this.state.flagId;
      API.put(`/api/products/adm_product_update/${id}`, post_data)
        .then((res) => {
          this.modalCloseHandler();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record updated successfully.",
            icon: "success",
          }).then(() => {
            this.setState({ activePage: 1, showModalLoader: false });
            this.getProductList(this.state.activePage);
          });
        })
        .catch((err) => {
          this.setState({ showModalLoader: false });
          if (err.data.status === 3) {
            this.setState({
              showModal: false,
            });
            showErrorMessage(err, this.props);
          } else {
            actions.setErrors(err.data.errors);
            actions.setSubmitting(false);
          }
        });
    }
  };
 
  modalShowHandler = (event, id) => {
    if(id){
      event.preventDefault();
      API.get(`/api/products/adm_product_get/${id}`)
      .then(res => {
        this.setState({
          product_details: res.data.data,     
          isLoading: false,
          flagId : id,
          showModal: true,

        });

      }) .catch(err => {
        showErrorMessage(err,this.props);
      });
    }else{
      this.setState({
        product_details: [],     
        isLoading: true,
        flagId : 0,
        showModal: true
      });
    }
  }
  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getProductList(pageNumber > 0 ? pageNumber  : 1);
  };

  getProductList(page = 1) {

    let product_name = this.state.search_name;
    let sku_code = this.state.sku_code;
    let new_order = this.state.new_order;

    API.get(`/api/products/adm_product_list?page=${page}&product_name=${encodeURIComponent(product_name)}&sku_code=${encodeURIComponent(sku_code)}&new_order=${encodeURIComponent(new_order)}`)
      .then(res => {
        this.setState({
          product       : res.data.data,
          count_product : res.data.count_product,
          isLoading     : false,
          search_name   : product_name,
          sku_code      : sku_code,
          new_order     : new_order
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        showErrorMessage(err,this.props);
      });
  }

  productSearch = (e) => {
    e.preventDefault();

    var product_name = document.getElementById('product_name').value;
    var sku_code = document.getElementById('sku_code').value;
    var new_order = document.getElementById('new_order').value;

    if(product_name === "" && sku_code === "" && new_order === ""){
      return false;
    }

    API.get(`/api/products/adm_product_list?page=1&product_name=${encodeURIComponent(product_name)}&sku_code=${encodeURIComponent(sku_code)}&new_order=${encodeURIComponent(new_order)}`)
    .then(res => {
      this.setState({
        product: res.data.data,
        count_product: res.data.count_product,
        isLoading: false,
        search_name: product_name,
        sku_code      : sku_code,
        new_order     : new_order,
        remove_search: true,
        activePage: 1
      });
    })
    .catch(err => {
      this.setState({
        isLoading: false
      });
      showErrorMessage(err,this.props);
    });
  }
  modalCloseHandler = () => {
    this.setState({ activePage: 1, flagId: 0 });
    this.getProductList(this.state.activePage);
    this.setState({ showModal: false });
  };

  clearSearch = () => {
    document.getElementById('product_name').value = "";
    document.getElementById('sku_code').value     = "";
    document.getElementById('new_order').value    = "";

    this.setState({
      search_name: "",
      sku_code:"",
      new_order:"",
      remove_search: false
    }, () => {
      this.getProductList();
      this.setState({activePage: 1})
    })
  }

  downloadXLSX = (e) => {
    e.preventDefault();

    var product_name = document.getElementById('product_name').value;   
    var sku_code     = document.getElementById('sku_code').value;
    var new_order    = document.getElementById('new_order').value; 

    API.get(`/api/products/adm_product_list/download?page=1&product_name=${encodeURIComponent(product_name)}&sku_code=${encodeURIComponent(sku_code)}&new_order=${encodeURIComponent(new_order)}`,{responseType: 'blob'})
    .then(res => { 
      let url    = window.URL.createObjectURL(res.data);
      let a      = document.createElement('a');
      a.href     = url;
      a.download = 'product.xlsx';
      a.click();

    }).catch(err => {
      showErrorMessage(err,this.props);
    });
  }

  checkHandler = (event) => {
    event.preventDefault();
  };

  render() {
    const { product_details } = this.state;
    const newInitialValues = Object.assign(initialValues, {
      sku_code: product_details.sku_code
        ? htmlDecode(product_details.sku_code)
        : "",
        new_order: product_details.new_order || product_details.new_order === 0
               ? product_details.new_order.toString()
              : "",
    });
    const validateStopFlag = Yup.object().shape({
      sku_code: Yup.string()
        .required("Please enter sku_code"),
        new_order: Yup.number().required("Please select new_order"),
    });

    if (this.state.isLoading === true) {
      return (
        <>
            <div className="loderOuter">
            <div className="loading_reddy_outer">
                <div className="loading_reddy" >
                    <img src={whitelogo} alt="logo" />
                </div>
                </div>
            </div>
        </>        
      );
    } else {
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <h1>
                      Product Master
                    <small />
                  </h1>
                  
                </div>

                {/*  */}
                <div className="col-lg-12 col-sm-12 col-xs-12 topSearchSection">
                  <form className="form">
                          <div className="">
                          <input
                              className="form-control"
                              name="product_name"
                              id="product_name"
                              placeholder="Product name"
                            />
                          </div>

                          <div className="">
                          <input
                              className="form-control"
                              name="sku_code"
                              id="sku_code"
                              placeholder="SKU Code"
                            />
                          </div>

                          <div className="">
                            <select
                              name="new_order"
                              id="new_order"
                              className="form-control"
                            >
                              <option value="">Select Visiblility</option>   
                              <option value="1">Both</option>
                              <option value="0">Non SAP Form</option>
                              <option value="2">SAP New Order</option>
                            </select>
                          </div>

                          <div className="">
                          <input
                            type="submit"
                            value="Search"
                            className="btn btn-warning btn-sm"
                            onClick={(e) => this.productSearch( e )}
                          />
                          {this.state.remove_search ? <a onClick={() => this.clearSearch()} className="btn btn-danger btn-sm"> Remove </a> : null}
                          </div>
                    </form>
                  </div>
                  {/*  */}

              </div>
            </section>
            {/* <DashboardSearch groupList={this.state.groupList} /> */}
            <section className="content">
              <div className="box">

                <div className="box-body">

                <div className="nav-tabs-custom">
                    <ul className="nav nav-tabs">
                    <li className="tabButtonSec pull-right">
                    {this.state.count_product > 0 ? 
                    <span onClick={(e) => this.downloadXLSX(e)} >
                        <LinkWithTooltip
                            tooltip={`Click here to download excel`}
                            href="#"
                            id="tooltip-my"
                            clicked={e => this.checkHandler(e)}
                        >
                            <i className="fas fa-download"></i>
                        </LinkWithTooltip>
                    </span>
                  : null }
            </li>
            </ul>
            </div>

                  <BootstrapTable
                    data={this.state.product}
                  >
                    <TableHeaderColumn isKey dataField="product_id">
                      Drupal Identifier
                    </TableHeaderColumn>

                    <TableHeaderColumn  dataField="product_name"  dataFormat={__htmlDecode(this)}>
                      Product Name
                    </TableHeaderColumn>

                    <TableHeaderColumn dataField="sku_code"  dataFormat={__htmlDecode(this)}>
                      SKU Code
                    </TableHeaderColumn>

                    <TableHeaderColumn dataField="new_order"  dataFormat={visiblility(this)}>
                      Visibility
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="product_id"
                      dataFormat={actionFormatter(this)}
                    >
                      Action
                    </TableHeaderColumn>

                    

                    {/* <TableHeaderColumn
                      dataField="company_id"
                      dataFormat={actionFormatter(this)}
                      dataAlign=""
                    >
                      Action
                    </TableHeaderColumn> */}
                  </BootstrapTable>
                  {this.state.count_product > this.state.itemPerPage ? (
                    <Row>
                      <Col md={12}>
                        <div className="paginationOuter text-right">
                          <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemPerPage}
                            totalItemsCount={this.state.count_product}
                            itemClass='nav-item'
                            linkClass='nav-link'
                            activeClass='active'
                            onChange={this.handlePageChange}
                          />
                        </div>
                      </Col>
                    </Row>
                  ) : null}
                   {/* Edit*/}
                   <Modal
                    show={this.state.showModal}
                    onHide={() => this.modalCloseHandler()}
                    backdrop="static"
                  >
                    <Formik
                      initialValues={newInitialValues}
                      validationSchema={validateStopFlag}
                      onSubmit={this.handleSubmitEvent}
                    >
                      {({ values, errors, touched, isValid, isSubmitting }) => {
                        return (
                          <Form>
                            {this.state.showModalLoader === true ? (
                              <div className="loading_reddy_outer">
                                <div className="loading_reddy">
                                  <img src={whitelogo} alt="loader" />
                                </div>
                              </div>
                            ) : (
                              ""
                            )}
                            <Modal.Header closeButton>
                              <Modal.Title>
                                {this.state.flagId > 0 ? "Edit" : "Add"} Products
                              </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="contBox">
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>
                                      sku_code<span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="sku_code"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter sku_code"
                                        autoComplete="off"
                                        value={values.sku_code}
                                      />
                                      {errors.sku_code && touched.sku_code ? (
                                        <span className="errorMsg">
                                          {errors.sku_code}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>
                                      Visibility
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="new_order"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.new_order}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        {this.state.selectvisibility.map(
                                          (data, i) => (
                                            <option key={i} value={data.new_order}>
                                              {data.name}
                                            </option>
                                          )
                                        )}
                                      </Field>
                                      {errors.new_order && touched.new_order ? (
                                        <span className="errorMsg">
                                          {errors.new_order}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                {errors.message ? (
                                  <Row>
                                    <Col xs={12} sm={12} md={12}>
                                      <span className="errorMsg">
                                        {errors.message}
                                      </span>
                                    </Col>
                                  </Row>
                                ) : ("")
                                }
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                              <button
                                className={`btn btn-success btn-sm ${
                                  isValid ? "btn-custom-green" : "btn-disable"
                                } mr-2`}
                                type="submit"
                                disabled={
                                  isValid ? (isSubmitting ? true : false) : true
                                }
                              >
                                {this.state.flagId > 0
                                  ? isSubmitting
                                    ? "Updating..."
                                    : "Update"
                                  : isSubmitting
                                  ? "Submitting..."
                                  : "Submit"}
                              </button>
                              <button
                                onClick={(e) => this.modalCloseHandler()}
                                className={`btn btn-danger btn-sm`}
                                type="button"
                              >
                                Close
                              </button>
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>
                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}

export default Products;