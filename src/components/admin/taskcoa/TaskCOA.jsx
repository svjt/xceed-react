import React, { Component } from 'react';
import Pagination from "react-js-pagination";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  Tooltip,
  OverlayTrigger
} from "react-bootstrap";
//import { Label } from 'reactstrap';
import { Link } from "react-router-dom";

import Layout from "../layout/Layout";
import whitelogo from '../../../assets/images/drreddylogo_white.png';
import API from "../../../shared/admin-axios";
import { showErrorMessage } from "../../../shared/handle_error";
import { htmlDecode } from '../../../shared/helper';
import { getSuperAdmin, getAdminGroup } from '../../../shared/helper';
const s3bucket_task_coa_path = `${process.env.REACT_APP_API_URL}/api/feed/download_task_coa/`;

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

const actionFormatter = refObj => cell => {
  const current_coa = refObj.state.coaDetails.filter((data)=> cell == data.coa_id ) ;
  return (
    
    <div className="actionStyle">
    {refObj.state.access.view === true && current_coa[0].coa_file_path !='' && current_coa[0].processed == 1 ? 
      <LinkWithTooltip
        tooltip="Click to Download"
        href="#"
        clicked={(e) =>
          refObj.redirectUrlTask(
            e,
            `${s3bucket_task_coa_path}${cell}`
          )
        }
        id="tooltip-1"
      >
        <i className="fas fa-download" />
      </LinkWithTooltip>
      : null }    
    </div>
  );
};

const custStatus = () => cell => {
  return cell === 1 ? "Active" : "Inactive";
};

const custContent = () => cell => {
  return htmlDecode(cell);
};

// const initialValues = {
//   region_name: '',
//   status: ''
// };

class TaskCOA extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      get_access_data:false,
      activePage: 1,
      totalCount: 0,
      itemPerPage: 20,
      coaDetails: [],
      coaflagId: 0,
      selectStatus: [
        { id: "0", name: "Inactive" },
        { id: "1", name: "Active" }
      ],
      showModal: false,
      showModalLoader: false
    };
  }

  componentDidMount() {
    
    const superAdmin  = getSuperAdmin(localStorage.admin_token);
    
    if(superAdmin === 1){
      this.setState({
        access: {
           view : true,
           add : true,
           edit : true,
           delete : true
         },
         get_access_data:true
     });
     this.getTaskCOAList();
    }else{
      const adminGroup  = getAdminGroup(localStorage.admin_token);
      API.get(`/api/adm_group/single_access/${adminGroup}/${'REGION_MANAGEMENT'}`)
      .then(res => {
        this.setState({
          access: res.data.data,
          get_access_data:true
        }); 

        if(res.data.data.view === true){
          this.getTaskCOAList();
        }else{
          this.props.history.push('/admin/dashboard');
        }
        
      })
      .catch(err => {
        showErrorMessage(err,this.props);
      });
    }      
  }

  getTaskCOAList(page = 1) {
      API.get(`/api/feed/task_coa?page=${page}`)
      .then(res => {
        this.setState({
          coaDetails: res.data.data,
          count: res.data.count_task_coa,
          isLoading: false
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        showErrorMessage(err,this.props);
      });
      this.setState({
        isLoading: false
      });
  }

   handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getTaskCOAList(pageNumber > 0 ? pageNumber : 1);
  };

  redirectUrlTask = (event, path) => {
    event.preventDefault();
    window.open(path, "_blank");
  }


  checkHandler = (event) => {
    event.preventDefault();
  }
  

  render() {
    const { coaDetails } = this.state;  
    
    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy" >
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <h1>
                    Task COA
                    <small />
                  </h1>
                  
                </div>
              </div>
            </section>            
            <section className="content">
              <div className="box">
              
                <div className="box-body">               

                  <BootstrapTable data={this.state.coaDetails}>
                    <TableHeaderColumn isKey dataField="customer_name" dataFormat={custContent(this)}>
                      Customer Name
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="batch_no">
                      Batch No
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="material_id">
                      Material No
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="inspection_lot_no">
                      Inspection Lot No
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="date_added">
                      Date Added
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="source">
                      Source
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="plant">
                      Plant
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="coa_file">
                      File
                    </TableHeaderColumn>
                    {this.state.access.view === true ?
                    <TableHeaderColumn
                      dataField="coa_id"
                      dataFormat={actionFormatter(this)}                      
                    >
                      Download
                    </TableHeaderColumn>
                    : null}
                  </BootstrapTable>

                  {this.state.count > this.state.itemPerPage ? (
                    <Row>
                      <Col md={12}>
                        <div className="paginationOuter text-right">
                          <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemPerPage}
                            totalItemsCount={this.state.count}
                            itemClass='nav-item'
                            linkClass='nav-link'
                            activeClass='active'
                            onChange={this.handlePageChange}
                          />
                        </div>
                      </Col>
                    </Row>
                  ) : null}

                  
                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}
export default TaskCOA;