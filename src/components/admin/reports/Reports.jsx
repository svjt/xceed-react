import React, { Component } from 'react';
import Pagination from "react-js-pagination";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal
} from "react-bootstrap";
//import { Label } from 'reactstrap';
import { Link } from "react-router-dom";
import { Formik, Field, Form } from "formik";
import swal from "sweetalert";
import * as Yup from "yup";


import Layout from "../layout/Layout";
import whitelogo from '../../../assets/images/drreddylogo_white.png';
import API from "../../../shared/admin-axios";
import { showErrorMessage } from "../../../shared/handle_error";
import { htmlDecode } from '../../../shared/helper';

import dateFormat from "dateformat";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { getSuperAdmin, getAdminGroup } from '../../../shared/helper';

/* function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

const actionFormatter = refObj => cell => {
  return (
    <div className="actionStyle">
      <LinkWithTooltip
        tooltip="Click to Edit"
        href="#"
        clicked={e => refObj.modalShowHandler(e, cell)}
        id="tooltip-1"
      >
        <i className="far fa-edit" />
      </LinkWithTooltip>
      <LinkWithTooltip
        tooltip="Click to Delete"
        href="#"
        clicked={e => refObj.confirmDelete(e, cell)}
        id="tooltip-1"
      >
        <i className="far fa-trash-alt" />
      </LinkWithTooltip>
    </div>
  );
};

const custStatus = () => cell => {
  return cell === 1 ? "Active" : "Inactive";
};

const custContent = () => cell => {
  return htmlDecode(cell);
};

const initialValues = {
  region_name: '',
  status: ''
}; */

class Reports extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      get_access_data:false
      /*  activePage: 1,
       totalCount: 0,
       itemPerPage: 20,
       regionDetails: [],
       regionflagId: 0,
       selectStatus: [
         { id: "0", name: "Inactive" },
         { id: "1", name: "Active" }
       ],
       showModal: false,
       showModalLoader: false */
    };
  }

  componentDidMount() {
    const superAdmin  = getSuperAdmin(localStorage.admin_token);
    
    if(superAdmin === 1){
      this.setState({
        access: {
           view : true
         },
         get_access_data:true
     });
    }else{
      const adminGroup  = getAdminGroup(localStorage.admin_token);
      API.get(`/api/adm_group/single_access/${adminGroup}/${'REPORTS_MANAGEMENT'}`)
      .then(res => {
        this.setState({
          access: res.data.data,
          get_access_data:true
        }); 

        if(res.data.data.view === true){
          
        }else{
          this.props.history.push('/admin/dashboard');
        }
        
      })
      .catch(err => {
        showErrorMessage(err,this.props);
      });
    } 
    this.setState({
      isLoading: false
    });
  }

  changeDate = (event, setFieldValue, field_name) => {
    if (event === null) {
      setFieldValue(field_name, null);
    } else {
      setFieldValue(field_name, event);
    }
  }

  loginHandleSubmitEvent = (values, actions) => {
    let post_obj = {
      date_from: '',
      date_to: ''
    };
    let qurey_string = '';
    if (values.login_date_from != null) {
      post_obj.date_from = dateFormat(values.login_date_from, "yyyy-mm-dd");
      qurey_string += `?date_from=${post_obj.date_from}`;
    }

    if (values.login_date_to != null) {
      post_obj.date_to = dateFormat(values.login_date_to, "yyyy-mm-dd");
      if (qurey_string != '') {
        qurey_string += `&date_to=${post_obj.date_to}`;
      } else {
        qurey_string += `?date_to=${post_obj.date_to}`;
      }
    }

    if (values.login_date_from != null || values.login_date_to != null) {
      if (values.login_date_from != null && values.login_date_to != null) {
        let date_from = values.login_date_from.getTime();
        let date_to = values.login_date_to.getTime();
        if (date_from > date_to) {
          swal({
            title: "Alert!",
            text: "Invalid date range",
            icon: "warning"
          });
        } else {
          API.get(`/api/reports/admin_reports/admin_login${qurey_string}`, { responseType: 'blob' }).then(res => {
            // let url = window.URL.createObjectURL(res.data);
            // let a = document.createElement('a');
            // a.href = url;
            // a.download = `No's of times employee last login.xlsx`;
            // a.click();

            // SATYAJIT
            var fileDownload = require('js-file-download');
            fileDownload(res.data, 'nos_of_times_last_login.xlsx');
            // END

          }).catch(err => { 
            showErrorMessage(err,this.props);
          });
        }
      } else {
        API.get(`/api/reports/admin_reports/admin_login${qurey_string}`, { responseType: 'blob' }).then(res => {
          console.log('2')
          let url = window.URL.createObjectURL(res.data);
          let a = document.createElement('a');
          a.href = url;
          a.download = `No's of times employee last login.xlsx`;
          a.click();

        }).catch(err => { 
          showErrorMessage(err,this.props);
        });
      }
    } else {
      swal({
        title: "Alert!",
        text: "Please make necessary selection!",
        icon: "warning"
      });
    }
  }

  custPassHandleSubmitEvent = (values, actions) => {

    /* let post_obj = {
      date_from : dateFormat(values.cust_pass_date_from, "yyyy-mm-dd"),
      date_to   : dateFormat(values.cust_pass_date_to, "yyyy-mm-dd")
    };

    API.get( `/api/reports/admin_reports/admin_cust_pass?date_to=${post_obj.date_to}&date_from=${post_obj.date_from}`,{responseType: 'blob'}).then(res => {
      //console.log(res);
      let url    = window.URL.createObjectURL(res.data);
      let a      = document.createElement('a');
      a.href     = url;
      a.download = 'customer_list_with_password_set.xlsx';
      a.click();

    }).catch(err => {}); */
    let post_obj = {
      date_from: '',
      date_to: ''
    };
    let qurey_string = '';
    if (values.cust_pass_date_from != null) {
      post_obj.date_from = dateFormat(values.cust_pass_date_from, "yyyy-mm-dd");
      qurey_string += `?date_from=${post_obj.date_from}`;
    }

    if (values.cust_pass_date_to != null) {
      post_obj.date_to = dateFormat(values.cust_pass_date_to, "yyyy-mm-dd");
      if (qurey_string != '') {
        qurey_string += `&date_to=${post_obj.date_to}`;
      } else {
        qurey_string += `?date_to=${post_obj.date_to}`;
      }
    }

    if (values.cust_pass_date_from != null || values.cust_pass_date_to != null) {
      if (values.cust_pass_date_from != null && values.cust_pass_date_to != null) {
        let date_from = values.cust_pass_date_from.getTime();
        let date_to = values.cust_pass_date_to.getTime();
        if (date_from > date_to) {
          swal({
            title: "Alert!",
            text: "Invalid date range",
            icon: "warning"
          });
        } else {
          API.get(`/api/reports/admin_reports/admin_cust_pass${qurey_string}`, { responseType: 'blob' }).then(res => {
            let url = window.URL.createObjectURL(res.data);
            let a = document.createElement('a');
            a.href = url;
            a.download = 'Customer list with password set.xlsx';
            a.click();

          }).catch(err => { 
            showErrorMessage(err,this.props);
          });
        }
      }else{
        API.get( `/api/reports/admin_reports/admin_cust_pass${qurey_string}`,{responseType: 'blob'}).then(res => {
          let url    = window.URL.createObjectURL(res.data);
          let a      = document.createElement('a');
          a.href     = url;
          a.download = 'Customer list with password set.xlsx';
          a.click();
  
        }).catch(err => {
          showErrorMessage(err,this.props);
        });
      }

    } else {
      swal({
        title: "Alert!",
        text: "Please make necessary selection!",
        icon: "warning"
      });
    }
  }

  custLoginHandleSubmitEvent = (values, actions) => {
    /* let post_obj = {
      date_from : dateFormat(values.cust_login_date_from, "yyyy-mm-dd"),
      date_to   : dateFormat(values.cust_login_date_to, "yyyy-mm-dd")
    };

    API.get( `/api/reports/admin_reports/admin_cust_login?date_to=${post_obj.date_to}&date_from=${post_obj.date_from}`,{responseType: 'blob'}).then(res => {
      //console.log(res);
      let url    = window.URL.createObjectURL(res.data);
      let a      = document.createElement('a');
      a.href     = url;
      a.download = 'customer_list_with_password_set.xlsx';
      a.click();

    }).catch(err => {}); */
    let post_obj = {
      date_from: '',
      date_to: ''
    };
    let qurey_string = '';
    if (values.cust_login_date_from != null) {
      post_obj.date_from = dateFormat(values.cust_login_date_from, "yyyy-mm-dd");
      qurey_string += `?date_from=${post_obj.date_from}`;
    }

    if (values.cust_login_date_to != null) {
      post_obj.date_to = dateFormat(values.cust_login_date_to, "yyyy-mm-dd");
      if (qurey_string != '') {
        qurey_string += `&date_to=${post_obj.date_to}`;
      } else {
        qurey_string += `?date_to=${post_obj.date_to}`;
      }
    }

    /* if (values.cust_login_date_from != null || values.cust_login_date_to != null) {
      API.get(`/api/reports/admin_reports/admin_cust_login${qurey_string}`, { responseType: 'blob' }).then(res => {
        let url = window.URL.createObjectURL(res.data);
        let a = document.createElement('a');
        a.href = url;
        a.download = 'customer_last_login_time.xlsx';
        a.click();

      }).catch(err => { }); */
    if (values.cust_login_date_from != null || values.cust_login_date_to != null) {
      if (values.cust_login_date_from != null && values.cust_login_date_to != null) {
        let date_from = values.cust_login_date_from.getTime();
        let date_to = values.cust_login_date_to.getTime();
        if (date_from > date_to) {
          swal({
            title: "Alert!",
            text: "Invalid date range",
            icon: "warning"
          });
        } else {
          API.get(`/api/reports/admin_reports/admin_cust_login${qurey_string}`, { responseType: 'blob' }).then(res => {
            let url = window.URL.createObjectURL(res.data);
            let a = document.createElement('a');
            a.href = url;
            a.download = 'Customer last login time.xlsx';
            a.click();

          }).catch(err => {
            showErrorMessage(err,this.props);
           });
        }
      }else{
        API.get(`/api/reports/admin_reports/admin_cust_login${qurey_string}`, { responseType: 'blob' }).then(res => {
          let url = window.URL.createObjectURL(res.data);
          let a = document.createElement('a');
          a.href = url;
          a.download = 'Customer last login time.xlsx';
          a.click();
  
        }).catch(err => {
          showErrorMessage(err,this.props);
        });
      }
    } else {
      swal({
        title: "Alert!",
        text: "Please make necessary selection!",
        icon: "warning"
      });
    }
  }

  //==========not working============//
  followupHandleSubmitEvent = (values, actions) => {

    /* let post_obj = {
      date_from : dateFormat(values.followup_date_from, "yyyy-mm-dd"),
      date_to   : dateFormat(values.followup_date_to, "yyyy-mm-dd")
    };

    API.get( `/api/reports/admin_reports/admin_followup?date_to=${post_obj.date_to}&date_from=${post_obj.date_from}`,{responseType: 'blob'}).then(res => {
      //console.log(res);
      let url    = window.URL.createObjectURL(res.data);
      let a      = document.createElement('a');
      a.href     = url;
      a.download = 'follow_up_feature_usage.xlsx';
      a.click();

    }).catch(err => {}); */
    let post_obj = {
      date_from: '',
      date_to: ''
    };
    let qurey_string = '';
    if (values.followup_date_from != null) {
      post_obj.date_from = dateFormat(values.followup_date_from, "yyyy-mm-dd");
      qurey_string += `?date_from=${post_obj.date_from}`;
    }

    if (values.followup_date_to != null) {
      post_obj.date_to = dateFormat(values.followup_date_to, "yyyy-mm-dd");
      if (qurey_string != '') {
        qurey_string += `&date_to=${post_obj.date_to}`;
      } else {
        qurey_string += `?date_to=${post_obj.date_to}`;
      }
    }

    /* if (values.followup_date_from != null || values.followup_date_to != null) {
      API.get(`/api/reports/admin_reports/admin_followup${qurey_string}`, { responseType: 'blob' }).then(res => {
        let url = window.URL.createObjectURL(res.data);
        let a = document.createElement('a');
        a.href = url;
        a.download = 'nos_of_times_followup.xlsx';
        a.click();

      }).catch(err => { }); */
      if (values.followup_date_from != null || values.followup_date_to != null) {
        if (values.followup_date_from != null && values.followup_date_to != null) {
          let date_from = values.followup_date_from.getTime();
          let date_to = values.followup_date_to.getTime();
          if (date_from > date_to) {
            swal({
              title: "Alert!",
              text: "Invalid date range",
              icon: "warning"
            });
          } else {
            API.get(`/api/reports/admin_reports/admin_followup${qurey_string}`, { responseType: 'blob' }).then(res => {
              let url = window.URL.createObjectURL(res.data);
              let a = document.createElement('a');
              a.href = url;
              a.download = 'nos_of_times_followup.xlsx';
              a.click();
  
            }).catch(err => { 
              showErrorMessage(err,this.props);
            });
          }
        }else{
          API.get(`/api/reports/admin_reports/admin_followup${qurey_string}`, { responseType: 'blob' }).then(res => {
            let url = window.URL.createObjectURL(res.data);
            let a = document.createElement('a');
            a.href = url;
            a.download = 'nos_of_times_followup.xlsx';
            a.click();
    
          }).catch(err => {
            showErrorMessage(err,this.props);
          });
        }
    } else {
      swal({
        title: "Alert!",
        text: "Please make necessary selection!",
        icon: "warning"
      });
    }
  }

  custHandleSubmitEvent = (values, actions) => {
    let post_obj = {
      date_from: '',
      date_to: ''
    };
    let qurey_string = '';
    if (values.cust_date_from != null) {
      post_obj.date_from = dateFormat(values.cust_date_from, "yyyy-mm-dd");
      qurey_string += `?date_from=${post_obj.date_from}`;
    }

    if (values.cust_date_to != null) {
      post_obj.date_to = dateFormat(values.cust_date_to, "yyyy-mm-dd");
      if (qurey_string != '') {
        qurey_string += `&date_to=${post_obj.date_to}`;
      } else {
        qurey_string += `?date_to=${post_obj.date_to}`;
      }
    }


    if(values.cust_date_from != null || values.cust_date_to != null){
      if (values.cust_date_from != null && values.cust_date_to != null) {
        let date_from = values.cust_date_from.getTime();
        let date_to = values.cust_date_to.getTime();
        if (date_from > date_to) {
          swal({
            title: "Alert!",
            text: "Invalid date range",
            icon: "warning"
          });
        } else {
          API.get(`/api/reports/admin_reports/admin_customer${qurey_string}`, { responseType: 'blob' }).then(res => {
            let url = window.URL.createObjectURL(res.data);
            let a = document.createElement('a');
            a.href = url;
            a.download = 'Request logged using Customers.xlsx';
            a.click();

          }).catch(err => { 
            showErrorMessage(err,this.props);
          });
        }
      }else{
        API.get(`/api/reports/admin_reports/admin_customer${qurey_string}`, { responseType: 'blob' }).then(res => {
          let url = window.URL.createObjectURL(res.data);
          let a = document.createElement('a');
          a.href = url;
          a.download = 'Request logged using Customers.xlsx';
          a.click();
  
        }).catch(err => {
          showErrorMessage(err,this.props);
        });
      }
    }else{
      swal({
        title: "Alert!",
        text: "Please make necessary selection!",
        icon: "warning"
      });
    }
  } 

  ccpHandleSubmitEvent = (values, actions) => {
    let post_obj = {
      date_from: '',
      date_to: ''
    };
    let qurey_string = '';
    if (values.ccp_date_from != null) {
      post_obj.date_from = dateFormat(values.ccp_date_from, "yyyy-mm-dd");
      qurey_string += `?date_from=${post_obj.date_from}`;
    }

    if (values.ccp_date_to != null) {
      post_obj.date_to = dateFormat(values.ccp_date_to, "yyyy-mm-dd");
      if (qurey_string != '') {
        qurey_string += `&date_to=${post_obj.date_to}`;
      } else {
        qurey_string += `?date_to=${post_obj.date_to}`;
      }
    }


    if(values.ccp_date_from != null || values.ccp_date_to != null){
      if (values.ccp_date_from != null && values.ccp_date_to != null) {
        let date_from = values.ccp_date_from.getTime();
        let date_to = values.ccp_date_to.getTime();
        if (date_from > date_to) {
          swal({
            title: "Alert!",
            text: "Invalid date range",
            icon: "warning"
          });
        } else {
          API.get(`/api/reports/admin_reports/admin_ccp${qurey_string}`, { responseType: 'blob' }).then(res => {
            let url = window.URL.createObjectURL(res.data);
            let a = document.createElement('a');
            a.href = url;
            a.download = 'Request logged using Exceed Login.xlsx';
            a.click();

          }).catch(err => { 
            showErrorMessage(err,this.props);
          });
        }
      }else{
        API.get(`/api/reports/admin_reports/admin_ccp${qurey_string}`, { responseType: 'blob' }).then(res => {
          let url = window.URL.createObjectURL(res.data);
          let a = document.createElement('a');
          a.href = url;
          a.download = 'Request logged using Exceed Login.xlsx';
          a.click();
  
        }).catch(err => {
          showErrorMessage(err,this.props);
        });
      }
    }else{
      swal({
        title: "Alert!",
        text: "Please make necessary selection!",
        icon: "warning"
      });
    }
  }  

  shadowHandleSubmitEvent = (values, actions) => {
    let post_obj = {
      date_from: '',
      date_to: ''
    };
    let qurey_string = '';
    if (values.shadow_date_from != null) {
      post_obj.date_from = dateFormat(values.shadow_date_from, "yyyy-mm-dd");
      qurey_string += `?date_from=${post_obj.date_from}`;
    }

    if (values.shadow_date_to != null) {
      post_obj.date_to = dateFormat(values.shadow_date_to, "yyyy-mm-dd");
      if (qurey_string != '') {
        qurey_string += `&date_to=${post_obj.date_to}`;
      } else {
        qurey_string += `?date_to=${post_obj.date_to}`;
      }
    }


    if(values.shadow_date_from != null || values.shadow_date_to != null){
      if (values.shadow_date_from != null && values.shadow_date_to != null) {
        let date_from = values.shadow_date_from.getTime();
        let date_to = values.shadow_date_to.getTime();
        if (date_from > date_to) {
          swal({
            title: "Alert!",
            text: "Invalid date range",
            icon: "warning"
          });
        } else {
          API.get(`/api/reports/admin_reports/admin_shadow${qurey_string}`, { responseType: 'blob' }).then(res => {
            let url = window.URL.createObjectURL(res.data);
            let a = document.createElement('a');
            a.href = url;
            a.download = 'Request logged using Shadow Login.xlsx';
            a.click();

          }).catch(err => { 
            showErrorMessage(err,this.props);
          });
        }
      }else{
        API.get(`/api/reports/admin_reports/admin_shadow${qurey_string}`, { responseType: 'blob' }).then(res => {
          let url = window.URL.createObjectURL(res.data);
          let a = document.createElement('a');
          a.href = url;
          a.download = 'Request logged using Shadow Login.xlsx';
          a.click();
  
        }).catch(err => {
          showErrorMessage(err,this.props);
        });
      }
    }else{
      swal({
        title: "Alert!",
        text: "Please make necessary selection!",
        icon: "warning"
      });
    }
  }

  outlookHandleSubmitEvent = (values, actions) => {
    /* let post_obj = {
      date_from : dateFormat(values.outlook_date_from, "yyyy-mm-dd"),
      date_to   : dateFormat(values.outlook_date_to, "yyyy-mm-dd")
    };

    API.get( `/api/reports/admin_reports/admin_outlook?date_to=${post_obj.date_to}&date_from=${post_obj.date_from}`,{responseType: 'blob'}).then(res => {
      //console.log(res);
      let url    = window.URL.createObjectURL(res.data);
      let a      = document.createElement('a');
      a.href     = url;
      a.download = 'request_logged_using_outlook_widget.xlsx';
      a.click();

    }).catch(err => {}); */
    let post_obj = {
      date_from: '',
      date_to: ''
    };
    let qurey_string = '';
    if (values.outlook_date_from != null) {
      post_obj.date_from = dateFormat(values.outlook_date_from, "yyyy-mm-dd");
      qurey_string += `?date_from=${post_obj.date_from}`;
    }

    if (values.outlook_date_to != null) {
      post_obj.date_to = dateFormat(values.outlook_date_to, "yyyy-mm-dd");
      if (qurey_string != '') {
        qurey_string += `&date_to=${post_obj.date_to}`;
      } else {
        qurey_string += `?date_to=${post_obj.date_to}`;
      }
    }

    /* if (values.outlook_date_from != null || values.outlook_date_to != null) {
      API.get(`/api/reports/admin_reports/admin_outlook${qurey_string}`, { responseType: 'blob' }).then(res => {
        let url = window.URL.createObjectURL(res.data);
        let a = document.createElement('a');
        a.href = url;
        a.download = 'request_logged_using_outlook_widget.xlsx';
        a.click();

      }).catch(err => { }); */
      if (values.outlook_date_from != null || values.outlook_date_to != null) {
        if (values.outlook_date_from != null && values.outlook_date_to != null) {
          let date_from = values.outlook_date_from.getTime();
          let date_to = values.outlook_date_to.getTime();
          if (date_from > date_to) {
            swal({
              title: "Alert!",
              text: "Invalid date range",
              icon: "warning"
            });
          } else {
            API.get(`/api/reports/admin_reports/admin_outlook${qurey_string}`, { responseType: 'blob' }).then(res => {
              let url = window.URL.createObjectURL(res.data);
              let a = document.createElement('a');
              a.href = url;
              a.download = 'Request logged using Outlook widget.xlsx';
              a.click();
  
            }).catch(err => { 
              showErrorMessage(err,this.props);
            });
          }
        }else{
          API.get(`/api/reports/admin_reports/admin_outlook${qurey_string}`, { responseType: 'blob' }).then(res => {
            let url = window.URL.createObjectURL(res.data);
            let a = document.createElement('a');
            a.href = url;
            a.download = 'Request logged using Outlook widget.xlsx';
            a.click();
    
          }).catch(err => {
            showErrorMessage(err,this.props);
          });
        }
    } else {
      swal({
        title: "Alert!",
        text: "Please make necessary selection!",
        icon: "warning"
      });
    }
  }

  //==========not working============//
  featureHandleSubmitEvent = (values, actions) => {
    /* let post_obj = {
      date_from : dateFormat(values.feature_date_from, "yyyy-mm-dd"),
      date_to   : dateFormat(values.feature_date_to, "yyyy-mm-dd")};
    
    API.get( `/api/reports/admin_reports/admin_feature?date_to=${post_obj.date_to}&date_from=${post_obj.date_from}`,{responseType: 'blob'}).then(res => {
        //console.log(res);
        let url    = window.URL.createObjectURL(res.data);
        let a      = document.createElement('a');
        a.href     = url;
        a.download = 'l+1_feature_usage.xlsx';
        a.click();
  
    }).catch(err => {}); */
    let post_obj = {
      date_from: '',
      date_to: ''
    };
    let qurey_string = '';
    if (values.feature_date_from != null) {
      post_obj.date_from = dateFormat(values.feature_date_from, "yyyy-mm-dd");
      qurey_string += `?date_from=${post_obj.date_from}`;
    }

    if (values.feature_date_to != null) {
      post_obj.date_to = dateFormat(values.feature_date_to, "yyyy-mm-dd");
      if (qurey_string != '') {
        qurey_string += `&date_to=${post_obj.date_to}`;
      } else {
        qurey_string += `?date_to=${post_obj.date_to}`;
      }
    }

   /*  if (values.feature_date_from != null || values.feature_date_to != null) {
      API.get(`/api/reports/admin_reports/admin_l_one_feature${qurey_string}`, { responseType: 'blob' }).then(res => {
        let url = window.URL.createObjectURL(res.data);
        let a = document.createElement('a');
        a.href = url;
        a.download = 'L+1_from_my_teams_sections.xlsx';
        a.click();

      }).catch(err => { }); */
      if (values.feature_date_from != null || values.feature_date_to != null) {
        if (values.feature_date_from != null && values.feature_date_to != null) {
          let date_from = values.feature_date_from.getTime();
          let date_to = values.feature_date_to.getTime();
          if (date_from > date_to) {
            swal({
              title: "Alert!",
              text: "Invalid date range",
              icon: "warning"
            });
          } else {
            API.get(`/api/reports/admin_reports/admin_l_one_feature${qurey_string}`, { responseType: 'blob' }).then(res => {
              let url = window.URL.createObjectURL(res.data);
              let a = document.createElement('a');
              a.href = url;
              a.download = 'L+1_from_my_teams_sections.xlsx';
              a.click();
  
            }).catch(err => { 
              showErrorMessage(err,this.props);
            });
          }
        }else{
          API.get(`/api/reports/admin_reports/admin_l_one_feature${qurey_string}`, { responseType: 'blob' }).then(res => {
            let url = window.URL.createObjectURL(res.data);
            let a = document.createElement('a');
            a.href = url;
            a.download = 'L+1_from_my_teams_sections.xlsx';
            a.click();
    
          }).catch(err => {
            showErrorMessage(err,this.props);
          });
        }
    } else {
      swal({
        title: "Alert!",
        text: "Please make necessary selection!",
        icon: "warning"
      });
    }
  }


  compSignHandleSubmitEvent = (values, actions) => {
    let post_obj = {
      date_from: '',
      date_to: ''
    };
    let qurey_string = '';
    if (values.comp_sign_date_from != null) {
      post_obj.date_from = dateFormat(values.comp_sign_date_from, "yyyy-mm-dd");
      qurey_string += `?date_from=${post_obj.date_from}`;
    }

    if (values.comp_sign_date_to != null) {
      post_obj.date_to = dateFormat(values.comp_sign_date_to, "yyyy-mm-dd");
      if (qurey_string != '') {
        qurey_string += `&date_to=${post_obj.date_to}`;
      } else {
        qurey_string += `?date_to=${post_obj.date_to}`;
      }
    }

    /* if (values.comp_sign_date_from != null || values.comp_sign_date_to != null) {
      API.get(`/api/reports/admin_reports/admin_comp_sign${qurey_string}`, { responseType: 'blob' }).then(res => {
        let url = window.URL.createObjectURL(res.data);
        let a = document.createElement('a');
        a.href = url;
        a.download = 'month_wise_companies_signups.xlsx';
        a.click();

      }).catch(err => { }); */
      if (values.comp_sign_date_from != null || values.comp_sign_date_to != null) {
        if (values.comp_sign_date_from != null && values.comp_sign_date_to != null) {
          let date_from = values.comp_sign_date_from.getTime();
          let date_to = values.comp_sign_date_to.getTime();
          if (date_from > date_to) {
            swal({
              title: "Alert!",
              text: "Invalid date range",
              icon: "warning"
            });
          } else {
            API.get(`/api/reports/admin_reports/admin_comp_sign${qurey_string}`, { responseType: 'blob' }).then(res => {
              let url = window.URL.createObjectURL(res.data);
              let a = document.createElement('a');
              a.href = url;
              a.download = 'Month wise Companies Signups.xlsx';
              a.click();
  
            }).catch(err => { 
              showErrorMessage(err,this.props);
            });
          }
        }else{
          API.get(`/api/reports/admin_reports/admin_comp_sign${qurey_string}`, { responseType: 'blob' }).then(res => {
            let url = window.URL.createObjectURL(res.data);
            let a = document.createElement('a');
            a.href = url;
            a.download = 'Month wise Companies Signups.xlsx';
            a.click();
    
          }).catch(err => {
            showErrorMessage(err,this.props);
          });
        }
    } else {
      swal({
        title: "Alert!",
        text: "Please make necessary selection!",
        icon: "warning"
      });
    }
  }

  userSignHandleSubmitEvent = (values, actions) => {
    let post_obj = {
      date_from: '',
      date_to: ''
    };
    let qurey_string = '';
    if (values.user_sign_date_from != null) {
      post_obj.date_from = dateFormat(values.user_sign_date_from, "yyyy-mm-dd");
      qurey_string += `?date_from=${post_obj.date_from}`;
    }

    if (values.user_sign_date_to != null) {
      post_obj.date_to = dateFormat(values.user_sign_date_to, "yyyy-mm-dd");
      if (qurey_string != '') {
        qurey_string += `&date_to=${post_obj.date_to}`;
      } else {
        qurey_string += `?date_to=${post_obj.date_to}`;
      }
    }

    /* if (values.user_sign_date_from != null || values.user_sign_date_to != null) {
      API.get(`/api/reports/admin_reports/admin_user_sign${qurey_string}`, { responseType: 'blob' }).then(res => {
        let url = window.URL.createObjectURL(res.data);
        let a = document.createElement('a');
        a.href = url;
        a.download = 'month_wise_user_signups.xlsx';
        a.click();

      }).catch(err => { }); */
      if (values.user_sign_date_from != null || values.user_sign_date_to != null) {
        if (values.user_sign_date_from != null && values.user_sign_date_to != null) {
          let date_from = values.user_sign_date_from.getTime();
          let date_to = values.user_sign_date_to.getTime();
          if (date_from > date_to) {
            swal({
              title: "Alert!",
              text: "Invalid date range",
              icon: "warning"
            });
          } else {
            API.get(`/api/reports/admin_reports/admin_user_sign${qurey_string}`, { responseType: 'blob' }).then(res => {
              let url = window.URL.createObjectURL(res.data);
              let a = document.createElement('a');
              a.href = url;
              a.download = 'Month wise User Signups.xlsx';
              a.click();
  
            }).catch(err => { 
              showErrorMessage(err,this.props);
            });
          }
        }else{
          API.get(`/api/reports/admin_reports/admin_user_sign${qurey_string}`, { responseType: 'blob' }).then(res => {
            let url = window.URL.createObjectURL(res.data);
            let a = document.createElement('a');
            a.href = url;
            a.download = 'Month wise User Signups.xlsx';
            a.click();
    
          }).catch(err => {
            showErrorMessage(err,this.props);
          });
        }
    } else {
      swal({
        title: "Alert!",
        text: "Please make necessary selection!",
        icon: "warning"
      });
    }
  }

  render() {
    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy" >
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <h1>
                    Generate Reports
                    <small />
                  </h1>
                </div>
              </div>
            </section>


            <section class="content">
              <div class="box">
                <div class="box-body reportSec">
                  <div className="col-lg-12 col-sm-12 col-xs-12 ">

                    <Formik
                      initialValues={{ login_date_from: null, login_date_to: null }}
                      validationSchema={null}
                      onSubmit={this.loginHandleSubmitEvent}
                    >
                      {({
                        values,
                        errors,
                        touched,
                        isValid,
                        isSubmitting,
                        setFieldValue,
                        setFieldTouched,
                        handleChange
                      }) => {

                        return (
                          <Form>
                            <div className="">
                              <h4>No's of times employee last login:</h4>
                            </div>
                            <div className="row">
                              <div className="col-md-3">
                                <label>Date From</label>
                                <div className="">
                                  <DatePicker
                                    name={"login_date_from"}
                                    className="borderNone"
                                    selected={values.login_date_from}
                                    dateFormat="dd/MM/yyyy"
                                    onChange={e => { this.changeDate(e, setFieldValue, 'login_date_from') }}
                                  />
                                </div>
                              </div>

                              <div className="col-md-3">
                                <label>Date To</label>
                                <div className="">
                                  <DatePicker
                                    name={"login_date_to"}
                                    className="borderNone"
                                    selected={values.login_date_to}
                                    dateFormat="dd/MM/yyyy"
                                    onChange={e => { this.changeDate(e, setFieldValue, 'login_date_to') }}
                                  />
                                </div>
                              </div>

                              <div className="col-md-3">
                                <input
                                  type="submit"
                                  value="Generate Report"
                                  className="btn btn-info btn-sm mt-20"
                                />
                              </div>
                              <div className="clearfix"></div>
                            </div>
                          </Form>
                        );
                      }}
                    </Formik>

                  </div>
                  <hr />
                  <div className="col-lg-12 col-sm-12 col-xs-12 ">

                    <Formik
                      initialValues={{ cust_pass_date_from: null, cust_pass_date_to: null }}
                      validationSchema={null}
                      onSubmit={this.custPassHandleSubmitEvent}
                    >
                      {({
                        values,
                        errors,
                        touched,
                        isValid,
                        isSubmitting,
                        setFieldValue,
                        setFieldTouched,
                        handleChange
                      }) => {

                        return (
                          <Form>
                            <div className="">
                              <h4>Customer list with password set or not status along with password reset date:</h4>
                            </div>
                            <div className="row">
                              <div className="col-md-3">
                                <label>Date From</label>
                                <div className="">
                                  <DatePicker
                                    name={"cust_pass_date_from"}
                                    className="borderNone"
                                    selected={values.cust_pass_date_from}
                                    dateFormat="dd/MM/yyyy"
                                    onChange={e => { this.changeDate(e, setFieldValue, 'cust_pass_date_from') }}
                                  />
                                </div>
                              </div>

                              <div className="col-md-3">
                                <label>Date To</label>
                                <div className="">
                                  <DatePicker
                                    name={"cust_pass_date_to"}
                                    className="borderNone"
                                    selected={values.cust_pass_date_to}
                                    dateFormat="dd/MM/yyyy"
                                    onChange={e => { this.changeDate(e, setFieldValue, 'cust_pass_date_to') }}
                                  />
                                </div>
                              </div>

                              <div className="col-md-3">
                                <input
                                  type="submit"
                                  value="Generate Report"
                                  className="btn btn-info btn-sm mt-20"
                                />
                              </div>
                              <div className="clearfix"></div>
                            </div>
                          </Form>
                        );
                      }}
                    </Formik>

                  </div>
                  <hr />
                  <div className="col-lg-12 col-sm-12 col-xs-12 ">

                    <Formik
                      initialValues={{ cust_login_date_from: null, cust_login_date_to: null }}
                      validationSchema={null}
                      onSubmit={this.custLoginHandleSubmitEvent}
                    >
                      {({
                        values,
                        errors,
                        touched,
                        isValid,
                        isSubmitting,
                        setFieldValue,
                        setFieldTouched,
                        handleChange
                      }) => {

                        return (
                          <Form>
                            <div className="">
                              <h4>Customer last login time:</h4>
                            </div>
                            <div className="row">
                              <div className="col-md-3">
                                <label>Date From</label>
                                <div className="">
                                  <DatePicker
                                    name={"cust_login_date_from"}
                                    className="borderNone"
                                    selected={values.cust_login_date_from}
                                    dateFormat="dd/MM/yyyy"
                                    onChange={e => { this.changeDate(e, setFieldValue, 'cust_login_date_from') }}
                                  />
                                </div>
                              </div>

                              <div className="col-md-3">
                                <label>Date To</label>
                                <div className="">
                                  <DatePicker
                                    name={"cust_login_date_to"}
                                    className="borderNone"
                                    selected={values.cust_login_date_to}
                                    dateFormat="dd/MM/yyyy"
                                    onChange={e => { this.changeDate(e, setFieldValue, 'cust_login_date_to') }}
                                  />
                                </div>
                              </div>

                              <div className="col-md-3">
                                <input
                                  type="submit"
                                  value="Generate Report"
                                  className="btn btn-info btn-sm mt-20"
                                />
                              </div>
                              <div className="clearfix"></div>
                            </div>
                          </Form>
                        );
                      }}
                    </Formik>

                  </div>
                  <hr />
                  {/* <div className="col-lg-12 col-sm-12 col-xs-12 ">
                        
                        <Formik
                            initialValues={{followup_date_from:null,followup_date_to:null}}
                            validationSchema={null}
                            onSubmit={this.followupHandleSubmitEvent}
                          >
                            {({
                              values,
                              errors,
                              touched,
                              isValid,
                              isSubmitting,
                              setFieldValue,
                              setFieldTouched, 
                              handleChange 
                            }) => {  

                              return (                          
                                <Form>
                              <div className=""> 
                                <h4>Follow up feature usage:</h4>
                              </div>
                              <div className="row">
                                
                             
                              <div className="col-md-3"> 
                              <label>Date From</label>
                                <div className=""> 
                                <DatePicker
                                    name={"followup_date_from"}
                                    className="borderNone"
                                    selected={values.followup_date_from}
                                    dateFormat="dd/MM/yyyy"
                                    onChange={e => { this.changeDate(e,setFieldValue,'followup_date_from') }} 
                                  />
                              </div>
                              </div>
                                 
                              <div className="col-md-3"> 
                              <label>Date To</label>
                                <div className=""> 
                                <DatePicker
                                    name={"followup_date_to"}
                                    className="borderNone"
                                    selected={values.followup_date_to}
                                    dateFormat="dd/MM/yyyy"
                                    onChange={e => { this.changeDate(e,setFieldValue,'followup_date_to') }} 
                                  />
                              </div> 
                              </div>
                                 
                              <div className="col-md-3">
                              <input
                                type="submit"
                                value="Generate Report"
                                className="btn btn-info btn-sm mt-20"
                              />
                              </div>
                              <div className="clearfix"></div>
                              </div>
                              </Form>
                              );
                            }}
                        </Formik>
                  </div> */}
                  <hr />
                  <div className="col-lg-12 col-sm-12 col-xs-12 ">

                    <Formik
                      initialValues={{ outlook_date_from: null, outlook_date_to: null }}
                      validationSchema={null}
                      onSubmit={this.outlookHandleSubmitEvent}
                    >
                      {({
                        values,
                        errors,
                        touched,
                        isValid,
                        isSubmitting,
                        setFieldValue,
                        setFieldTouched,
                        handleChange
                      }) => {

                        return (
                          <Form>
                            <div className="">
                              <h4>Request logged using Outlook widget:</h4>
                            </div>
                            <div className="row">


                              <div className="col-md-3">
                                <label>Date From</label>
                                <div className="">
                                  <DatePicker
                                    name={"outlook_date_from"}
                                    className="borderNone"
                                    selected={values.outlook_date_from}
                                    dateFormat="dd/MM/yyyy"
                                    onChange={e => { this.changeDate(e, setFieldValue, 'outlook_date_from') }}
                                  />
                                </div>
                              </div>

                              <div className="col-md-3">
                                <label>Date To</label>
                                <div className="">
                                  <DatePicker
                                    name={"outlook_date_to"}
                                    className="borderNone"
                                    selected={values.outlook_date_to}
                                    dateFormat="dd/MM/yyyy"
                                    onChange={e => { this.changeDate(e, setFieldValue, 'outlook_date_to') }}
                                  />
                                </div>
                              </div>

                              <div className="col-md-3">
                                <input
                                  type="submit"
                                  value="Generate Report"
                                  className="btn btn-info btn-sm mt-20"
                                />
                              </div>
                              <div className="clearfix"></div>
                            </div>
                          </Form>
                        );
                      }}
                    </Formik>
                  </div>
                  <hr />

                  <div className="col-lg-12 col-sm-12 col-xs-12 ">

                    <Formik
                      initialValues={{ shadow_date_from: null, shadow_date_to: null }}
                      validationSchema={null}
                      onSubmit={this.shadowHandleSubmitEvent}
                    >
                      {({
                        values,
                        errors,
                        touched,
                        isValid,
                        isSubmitting,
                        setFieldValue,
                        setFieldTouched,
                        handleChange
                      }) => {

                        return (
                          <Form>
                            <div className="">
                              <h4>Request logged using Shadow Login:</h4>
                            </div>
                            <div className="row">


                              <div className="col-md-3">
                                <label>Date From</label>
                                <div className="">
                                  <DatePicker
                                    name={"shadow_date_from"}
                                    className="borderNone"
                                    selected={values.shadow_date_from}
                                    dateFormat="dd/MM/yyyy"
                                    onChange={e => { this.changeDate(e, setFieldValue, 'shadow_date_from') }}
                                  />
                                </div>
                              </div>

                              <div className="col-md-3">
                                <label>Date To</label>
                                <div className="">
                                  <DatePicker
                                    name={"shadow_date_to"}
                                    className="borderNone"
                                    selected={values.shadow_date_to}
                                    dateFormat="dd/MM/yyyy"
                                    onChange={e => { this.changeDate(e, setFieldValue, 'shadow_date_to') }}
                                  />
                                </div>
                              </div>

                              <div className="col-md-3">
                                <input
                                  type="submit"
                                  value="Generate Report"
                                  className="btn btn-info btn-sm mt-20"
                                />
                              </div>
                              <div className="clearfix"></div>
                            </div>
                          </Form>
                        );
                      }}
                    </Formik>
                    </div>
                    <hr />

                    <div className="col-lg-12 col-sm-12 col-xs-12 ">

                    <Formik
                      initialValues={{ ccp_date_from: null, ccp_date_to: null }}
                      validationSchema={null}
                      onSubmit={this.ccpHandleSubmitEvent}
                    >
                      {({
                        values,
                        errors,
                        touched,
                        isValid,
                        isSubmitting,
                        setFieldValue,
                        setFieldTouched,
                        handleChange
                      }) => {

                        return (
                          <Form>
                            <div className="">
                              <h4>Request logged by Xceed Employee:</h4>
                            </div>
                            <div className="row">


                              <div className="col-md-3">
                                <label>Date From</label>
                                <div className="">
                                  <DatePicker
                                    name={"ccp_date_from"}
                                    className="borderNone"
                                    selected={values.ccp_date_from}
                                    dateFormat="dd/MM/yyyy"
                                    onChange={e => { this.changeDate(e, setFieldValue, 'ccp_date_from') }}
                                  />
                                </div>
                              </div>

                              <div className="col-md-3">
                                <label>Date To</label>
                                <div className="">
                                  <DatePicker
                                    name={"ccp_date_to"}
                                    className="borderNone"
                                    selected={values.ccp_date_to}
                                    dateFormat="dd/MM/yyyy"
                                    onChange={e => { this.changeDate(e, setFieldValue, 'ccp_date_to') }}
                                  />
                                </div>
                              </div>

                              <div className="col-md-3">
                                <input
                                  type="submit"
                                  value="Generate Report"
                                  className="btn btn-info btn-sm mt-20"
                                />
                              </div>
                              <div className="clearfix"></div>
                            </div>
                          </Form>
                        );
                      }}
                    </Formik>
                    </div>
                    <hr />

                    <div className="col-lg-12 col-sm-12 col-xs-12 ">

                    <Formik
                      initialValues={{ cust_date_from: null, cust_date_to: null }}
                      validationSchema={null}
                      onSubmit={this.custHandleSubmitEvent}
                    >
                      {({
                        values,
                        errors,
                        touched,
                        isValid,
                        isSubmitting,
                        setFieldValue,
                        setFieldTouched,
                        handleChange
                      }) => {

                        return (
                          <Form>
                            <div className="">
                              <h4>Request logged by User:</h4>
                            </div>
                            <div className="row">


                              <div className="col-md-3">
                                <label>Date From</label>
                                <div className="">
                                  <DatePicker
                                    name={"cust_date_from"}
                                    className="borderNone"
                                    selected={values.cust_date_from}
                                    dateFormat="dd/MM/yyyy"
                                    onChange={e => { this.changeDate(e, setFieldValue, 'cust_date_from') }}
                                  />
                                </div>
                              </div>

                              <div className="col-md-3">
                                <label>Date To</label>
                                <div className="">
                                  <DatePicker
                                    name={"cust_date_to"}
                                    className="borderNone"
                                    selected={values.cust_date_to}
                                    dateFormat="dd/MM/yyyy"
                                    onChange={e => { this.changeDate(e, setFieldValue, 'cust_date_to') }}
                                  />
                                </div>
                              </div>

                              <div className="col-md-3">
                                <input
                                  type="submit"
                                  value="Generate Report"
                                  className="btn btn-info btn-sm mt-20"
                                />
                              </div>
                              <div className="clearfix"></div>
                            </div>
                          </Form>
                        );
                      }}
                    </Formik>
                    </div>
                    <hr />

                    

                  {/* <div className="col-lg-12 col-sm-12 col-xs-12 ">
                        
                      <Formik
                            initialValues={{feature_date_from:null,feature_date_to:null}}
                            validationSchema={null}
                            onSubmit={this.featureHandleSubmitEvent}
                          >
                            {({
                              values,
                              errors,
                              touched,
                              isValid,
                              isSubmitting,
                              setFieldValue,
                              setFieldTouched, 
                              handleChange 
                            }) => {  

                              return (                          
                                <Form>
                                <div className=""> 
                                  <h4>L+1 feature usage:</h4>
                                </div>
                                <div className="row">
                                  

                                <div className="col-md-3"> 
                                <label>Date From</label>
                                  <div className=""> 
                                  <DatePicker
                                      name={"feature_date_from"}
                                      className="borderNone"
                                      selected={values.feature_date_from}
                                      dateFormat="dd/MM/yyyy"
                                      onChange={e => { this.changeDate(e,setFieldValue,'feature_date_from') }} 
                                    />
                                </div> 
                                </div>
                                  
                                <div className="col-md-3"> 
                                <label>Date To</label>
                                  <div className=""> 
                                  <DatePicker
                                      name={"feature_date_to"}
                                      className="borderNone"
                                      selected={values.feature_date_to}
                                      dateFormat="dd/MM/yyyy"
                                      onChange={e => { this.changeDate(e,setFieldValue,'feature_date_to') }} 
                                    />
                                </div> 
                                </div>
                                  
                                <div className="col-md-3">
                                <input
                                  type="submit"
                                  value="Generate Report"
                                  className="btn btn-info btn-sm mt-20"
                                />
                                </div>
                                <div className="clearfix"></div>
                                </div>
                                </Form>
                              );
                            }}
                          </Formik>
                  </div> */}
                  <hr />
                  <div className="col-lg-12 col-sm-12 col-xs-12 ">

                    <Formik
                      initialValues={{ comp_sign_date_from: null, comp_sign_date_to: null }}
                      validationSchema={null}
                      onSubmit={this.compSignHandleSubmitEvent}
                    >
                      {({
                        values,
                        errors,
                        touched,
                        isValid,
                        isSubmitting,
                        setFieldValue,
                        setFieldTouched,
                        handleChange
                      }) => {

                        return (
                          <Form>
                            <div className="">
                              <h4>Month wise Companies Signups:</h4>
                            </div>
                            <div className="row">


                              <div className="col-md-3">
                                <label>Date From</label>
                                <div className="">
                                  <DatePicker
                                    name={"comp_sign_date_from"}
                                    className="borderNone"
                                    selected={values.comp_sign_date_from}
                                    dateFormat="dd/MM/yyyy"
                                    onChange={e => { this.changeDate(e, setFieldValue, 'comp_sign_date_from') }}
                                  />
                                </div>
                              </div>

                              <div className="col-md-3">
                                <label>Date To</label>
                                <div className="">
                                  <DatePicker
                                    name={"comp_sign_date_to"}
                                    className="borderNone"
                                    selected={values.comp_sign_date_to}
                                    dateFormat="dd/MM/yyyy"
                                    onChange={e => { this.changeDate(e, setFieldValue, 'comp_sign_date_to') }}
                                  />
                                </div>
                              </div>

                              <div className="col-md-3">
                                <input
                                  type="submit"
                                  value="Generate Report"
                                  className="btn btn-info btn-sm mt-20"
                                />
                              </div>
                              <div className="clearfix"></div>
                            </div>
                          </Form>
                        );
                      }}
                    </Formik>
                  </div>

                  <div className="col-lg-12 col-sm-12 col-xs-12 ">

                    <Formik
                      initialValues={{ user_sign_date_from: null, user_sign_date_to: null }}
                      validationSchema={null}
                      onSubmit={this.userSignHandleSubmitEvent}
                    >
                      {({
                        values,
                        errors,
                        touched,
                        isValid,
                        isSubmitting,
                        setFieldValue,
                        setFieldTouched,
                        handleChange
                      }) => {

                        return (
                          <Form>
                            <div className="">
                              <h4>Month wise User Signups:</h4>
                            </div>
                            <div className="row">


                              <div className="col-md-3">
                                <label>Date From</label>
                                <div className="">
                                  <DatePicker
                                    name={"user_sign_date_from"}
                                    className="borderNone"
                                    selected={values.user_sign_date_from}
                                    dateFormat="dd/MM/yyyy"
                                    onChange={e => { this.changeDate(e, setFieldValue, 'user_sign_date_from') }}
                                  />
                                </div>
                              </div>

                              <div className="col-md-3">
                                <label>Date To</label>
                                <div className="">
                                  <DatePicker
                                    name={"user_sign_date_to"}
                                    className="borderNone"
                                    selected={values.user_sign_date_to}
                                    dateFormat="dd/MM/yyyy"
                                    onChange={e => { this.changeDate(e, setFieldValue, 'user_sign_date_to') }}
                                  />
                                </div>
                              </div>

                              <div className="col-md-3">
                                <input
                                  type="submit"
                                  value="Generate Report"
                                  className="btn btn-info btn-sm mt-20"
                                />
                              </div>
                              <div className="clearfix"></div>
                            </div>
                          </Form>
                        );
                      }}
                    </Formik>
                  </div>

                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}
export default Reports;