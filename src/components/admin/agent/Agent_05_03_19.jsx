import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
//import Loader from "react-loader-spinner";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal
} from "react-bootstrap";
import { Link } from "react-router-dom";
import API from "../../../shared/axios";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import Layout from "../layout/Layout";
import Select from "react-select";
import whitelogo from '../../../assets/images/drreddylogo_white.png';
import { showErrorMessage } from "../../../shared/handle_error";

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const actionFormatter = refObj => cell => {
  return (
    <div className="actionStyle">
      <LinkWithTooltip
        tooltip="Click to Edit"
        href="#"
        clicked={e => refObj.modalShowHandler(e, cell)}
        id="tooltip-1"
      >
        <i className="far fa-edit" />
      </LinkWithTooltip>

      <LinkWithTooltip
        tooltip="Click to Delete"
        href="#"
        clicked={e => refObj.confirmDelete(e, cell)}
        id="tooltip-1"
      >
        <i className="far fa-trash-alt" />
      </LinkWithTooltip>
    </div>
  );
};

const custStatus = refObj => cell => {
  return cell === 1 ? "Active" : "Inactive";
};

const initialValues = {
  first_name: "",
  last_name: "",
  email: "",
  status: "",
  company: ""
};

class Agent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      agents: [],
      agentDetails: [],
      agentflagId: 0,
      showModal: false,
      selectStatus: [
        { id: "0", name: "Inactive" },
        { id: "1", name: "Active" }
      ],
      selectedCompanyList: [],
      selectedComp: {},
    };
  }

  componentDidMount() {
    this.getAgentList();
  }

  getAgentList() {
    API.get("/api/agents")
      .then(res => {
        this.setState({
          agents: res.data.data,
          isLoading: false
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        showErrorMessage(err, this.props);
      });
  }

  getCompanyList = () => {
    API.get('/api/customers/company')
      .then(res => {
        var myCompany = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          myCompany.push({
            value: element["company_id"],
            label: element["company_name"]
          });
        }
        this.setState({ companyList: myCompany });
      })
      .catch(err => {
        console.log(err);
      });
  }

  getAgentCompany(id) {
    var selAgtComp = [];
    var selComp = [];
    API.get(`/api/customers/company/${id}`)
      .then(res => {
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          selAgtComp.push({
            value: element["company_id"],
            label: element["company_name"]
          });
          selComp.push(element["company_id"]);
        }
        this.setState({
          selectedCompList: selAgtComp,
          selectedComp: selComp
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  getIndividualAgent(id, callBack) {
    API.get(`/api/agents/${id}`)
      .then(res => {
        this.setState({
          agentDetails: res.data.data
        });
        callBack();
      })
      .catch(err => {
        console.log(err);
      });
  }

  modalCloseHandler = () => {
    this.setState({ agentflagId: 0 });
    this.setState({ showModal: false });
  };

  modalShowHandler = (event, id) => {
    this.getCompanyList();
    if (id) {
      event.preventDefault();
      this.setState({ agentflagId: id });
      this.getAgentCompany(id);
      this.getIndividualAgent(id, () => this.setState({ showModal: true }));

    } else {
      ;
      this.setState({
        agentDetails: "",
        selectedCompList: "",
        selectedComp: "",
        showModal: true
      });
    }
  };

  handleSubmitEvent = (values, actions) => {
    if (this.state.agentflagId) {
      const post_data = {
        first_name: values.first_name,
        last_name: values.last_name,
        email: values.email,
        status: values.status,
        company: values.company
      };
      const id = this.state.agentflagId;
      API.put(`/api/agents/${id}`, post_data)
        .then(res => {
          this.modalCloseHandler();
          swal("Record updated successfully!", {
            icon: "success"
          }).then(() => {
            this.getAgentList();
          });
        })
        .catch(err => {
          if (err.data.status === 3) {
            this.setState({
              showModal: false
            });
            showErrorMessage(err, this.props);
          } else {
            actions.setErrors(err.data.errors);
            actions.setSubmitting(false);
          }
        });
    } else {
      const post_data = {
        first_name: values.first_name,
        last_name: values.last_name,
        email: values.email,
        status: values.status,
        company: values.company
      };
      API.post("/api/agents", post_data)
        .then(res => {
          this.modalCloseHandler();
          swal("Record added successfully!", {
            icon: "success"
          }).then(() => {
            this.getAgentList();
          });
        })
        .catch(err => {
          if (err.data.status === 3) {
            this.setState({
              showModal: false
            });
            showErrorMessage(err, this.props);
          } else {
            actions.setErrors(err.data.errors);
            actions.setSubmitting(false);
          }
        });
    }
  };

  confirmDelete = (event, id) => {
    event.preventDefault();
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this!",
      icon: "warning",
      buttons: {
        cancel: {
          text: "Cancel",
          value: false,
          visible: true,
          className: "class3 class4"
        },
        confirm: {
          text: "OK",
          value: true,
          visible: true,
          className: "class1 class2",
          closeModal: true
        }
      }
    }).then(willDelete => {
      if (willDelete) {
        this.deleteAgent(id);
      }
    });
  };

  deleteAgent = id => {
    API.delete(`/api/agents/${id}`).then(res => {
      swal("Record deleted successfully!", {
        icon: "success"
      }).then(() => {
        this.getAgentList();
      });
    }).catch(err => {
      if (err.data.status === 3) {
        this.setState({
          closeModal: true
        });
        showErrorMessage(err, this.props);
      }
    });
  };

  renderShowsTotal = (start, to, total) => {
    return (
      <span className="pageShow">
        Showing {start} to {to}, of {total} records
      </span>
    );
  };

  render() {

    const paginationOptions = {
      page: 1, // which page you want to show as default
      sizePerPageList: [
        {
          text: "10",
          value: 10
        },
        {
          text: "20",
          value: 20
        },
        {
          text: "All",
          value: this.state.agents.length > 0 ? this.state.agents.length : 1
        }
      ], // you can change the dropdown list for size per page
      sizePerPage: 10, // which size per page you want to locate as default
      pageStartIndex: 1, // where to start counting the pages
      paginationSize: 3, // the pagination bar size.
      prePage: "Prev", // Previous page button text
      nextPage: "Next", // Next page button text
      firstPage: "First", // First page button text
      lastPage: "Last", // Last page button text
      paginationShowsTotal: this.renderShowsTotal, // Accept bool or function
      paginationPosition: "bottom" // default is bottom, top and both is all available
      // hideSizePerPage: true //> You can hide the dropdown for sizePerPage
      // alwaysShowAllBtns: true // Always show next and previous button
      // withFirstAndLast: false //> Hide the going to First and Last page button
    };

    const { agentDetails } = this.state;

    const newInitialValues = Object.assign(initialValues, {
      first_name: agentDetails.first_name ? agentDetails.first_name : "",
      last_name: agentDetails.last_name ? agentDetails.last_name : "",
      email: agentDetails.email ? agentDetails.email : "",
      status: agentDetails.status || +agentDetails.status === 0 ? agentDetails.status.toString() : "",
      company: this.state.selectedComp ? this.state.selectedComp : ""
    });


    const validateAdmin = Yup.object().shape({
      first_name: Yup.string()
        .required("Please enter first name")
        .min(2, "First name must be at least 2 characters long"),
      last_name: Yup.string()
        .required("Please enter last name")
        .min(2, "Last name must be at least 2 characters long"),
      email: Yup.string()
        .required("Please enter email")
        .email("Invalid email"),
      status: Yup.string()
        .required("Please select status")
        .matches(/^[0|1]$/, "Invalid status selected"),
      company: Yup.array().ensure().min(1, "Please add at least one company.")
        .of(Yup.string().ensure().required("Company cannot be empty")),
    });

    if (this.state.isLoading === true) {
      return (
        <Layout>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy" >
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </Layout>
      );
    } else {
      return (
        <Layout>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">
                <div className="col-lg-9 col-sm-6 col-xs-12">
                  <h1>
                    Agent
                    <small />
                  </h1>
                </div>
                <div className="col-lg-3 col-sm-6 col-xs-12">
                  <button
                    type="button"
                    className="btn btn-warning btn-sm btn-custom-green pull-right"
                    onClick={e => this.modalShowHandler(e, "")}
                  >
                    <i className="fas fa-plus m-r-5" /> Add Agent
                  </button>
                </div>
              </div>
            </section>
            {/* <DashboardSearch groupList={this.state.groupList} /> */}
            <section className="content">
              <div className="box">
                <div className="box-body">
                  <BootstrapTable
                    data={this.state.agents}
                    pagination={true}
                    options={paginationOptions}
                    striped={true}
                    hover={true}
                    ignoreSinglePage
                  >
                    <TableHeaderColumn isKey dataField="first_name" dataSort={true}>
                      First Name
                    </TableHeaderColumn>

                    <TableHeaderColumn dataField="last_name" dataSort={true}>
                      Last Name
                    </TableHeaderColumn>

                    <TableHeaderColumn dataField="email" dataSort={true}>
                      Email
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="status"
                      dataFormat={custStatus(this)}
                      dataSort={true}
                    >Status</TableHeaderColumn>

                    <TableHeaderColumn
                      dataField="agent_id"
                      dataFormat={actionFormatter(this)}
                      dataAlign=""
                    >
                      Action
                    </TableHeaderColumn>
                  </BootstrapTable>

                  {/* ======= Add/Edit Admin ======== */}

                  <Modal
                    show={this.state.showModal}
                    onHide={this.modalCloseHandler} backdrop="static"
                  >
                    <Formik
                      initialValues={newInitialValues}
                      validationSchema={validateAdmin}
                      onSubmit={this.handleSubmitEvent}
                    >
                      {({ values, errors, touched, isValid, isSubmitting,
                        setFieldValue,
                        setFieldTouched }) => {
                        return (
                          <Form>
                            <Modal.Header closeButton>
                              <Modal.Title>
                                {this.state.agentflagId > 0 ? "Edit" : "Add"} Agent
                              </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="contBox">
                                <Row>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        First Name
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="first_name"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter first name"
                                        autoComplete="off"
                                      />
                                      {errors.first_name &&
                                        touched.first_name ? (
                                          <span className="errorMsg">
                                            {errors.first_name}
                                          </span>
                                        ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Last Name
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="last_name"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter last name"
                                        autoComplete="off"
                                      />
                                      {errors.last_name && touched.last_name ? (
                                        <span className="errorMsg">
                                          {errors.last_name}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Email<span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="email"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter email"
                                        autoComplete="off"
                                      />
                                      {errors.email && touched.email ? (
                                        <span className="errorMsg">
                                          {errors.email}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Status
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="status"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.status}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        {this.state.selectStatus.map(
                                          (status, i) => (
                                            <option key={i} value={status.id}>
                                              {status.name}
                                            </option>
                                          )
                                        )}
                                      </Field>
                                      {errors.status && touched.status ? (
                                        <span className="errorMsg">
                                          {errors.status}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>Assign Company</label>

                                      <Select
                                        isMulti
                                        name="company[]"
                                        options={this.state.companyList}
                                        className="basic-multi-select"
                                        classNamePrefix="select"
                                        onChange={evt =>
                                          setFieldValue(
                                            "company",
                                            [].slice
                                              .call(evt)
                                              .map(val => val.value)
                                          )
                                        }
                                        placeholder="Company"
                                        onBlur={() => setFieldTouched("company")}
                                        defaultValue={
                                          this.state.selectedCompList
                                        }
                                      />
                                      {errors.company && touched.company ? (
                                        <span className="errorMsg">
                                          {errors.company}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                {errors.message ? (
                                  <Row>
                                    <Col xs={12} sm={12} md={12}>
                                      <span className="errorMsg">
                                        {errors.message}
                                      </span>
                                    </Col>
                                  </Row>
                                ) : ("")
                                }
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                              <ButtonToolbar>
                                <Button
                                  className={`btn btn-success btn-sm ${
                                    isValid ? "btn-custom-green" : "btn-disable"
                                    } m-r-10`}
                                  type="submit"
                                  disabled={isValid ? false : true}
                                >
                                  {this.state.agentflagId > 0
                                    ? isSubmitting
                                      ? "Updating..."
                                      : "Update"
                                    : isSubmitting
                                      ? "Submitting..."
                                      : "Submit"}
                                </Button>
                                <Button
                                  onClick={e => this.modalCloseHandler()}
                                  className={`btn btn-danger btn-sm`}
                                  type="button"
                                >
                                  Close
                                </Button>
                              </ButtonToolbar>
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>
                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}

export default Agent;
