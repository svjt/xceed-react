import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
//import Loader from "react-loader-spinner";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal
} from "react-bootstrap";
import { Link } from "react-router-dom";
import API from "../../../shared/admin-axios";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import Layout from "../layout/Layout";
import Select from "react-select";
import whitelogo from '../../../assets/images/drreddylogo_white.png';
import {showErrorMessage} from "../../../shared/handle_error";
import Pagination from "react-js-pagination";
import { htmlDecode } from '../../../shared/helper';
import dateFormat from "dateformat";
import { getSuperAdmin, getAdminGroup } from '../../../shared/helper';

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const actionFormatter = refObj => cell => {
  return (
    <div className="actionStyle">
      {refObj.state.access.edit === true ?
      <LinkWithTooltip
        tooltip="Click to Edit"
        href="#"
        clicked={e => refObj.modalShowHandler(e, cell)}
        id="tooltip-1"
      >
        <i className="far fa-edit" />
      </LinkWithTooltip>
      : null }
      {refObj.state.access.delete === true ?
      <LinkWithTooltip
        tooltip="Click to Delete"
        href="#"
        clicked={e => refObj.confirmDelete(e, cell)}
        id="tooltip-1"
      >
        <i className="far fa-trash-alt" />
      </LinkWithTooltip>
      : null }
    </div>
  );
};

const custStatus = refObj => cell => {
  return cell === 1 ? "Active" : "Inactive";
};

const __htmlDecode = refObj => cell => {
  return htmlDecode(cell);
}

const vipStatus = refObj => cell => {
  return cell === 1 ? "Yes" : "No";
};

const loginDate = refObj => cell => {
  if(cell === null){
    return '-';
  }else{
    return cell;
  }
}

const setDND = refObj => cell => {
  if(cell === 1){
    return 'Yes';
  }else{
    return 'No';
  }
}

const initialValues = {
  first_name: "",
  last_name: "",
  email: "",
  status: "",
  fa_access:"",
  company: "",
  phone_no: "",
  country_id: "",
  language_code:"",
  agent_ref: ""
};

const setCreateDate = refObj => cell => {
  // if(cell != '' && cell != null){
  //   var mydate = new Date(cell);
  //   return dateFormat(mydate, "dd/mm/yyyy");
  // }else{
  //   return '';
  // }  

  if(cell === null){
    return '-';
  }else{
    return cell;
  }
};

class Agent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      agents: [],
      agentDetails: [],
      agentflagId: 0,
      showModal: false,
      selectStatus: [
        { id: "0", name: "Inactive" },
        { id: "1", name: "Active" }
      ],      
      faAccess: [
        { id: "0", name: "No" },
        { id: "1", name: "Yes" },
      ],
      countryList: [],
      languageList:[],
      selectedCompanyList: [],
      selectedComp: {},

      activePage: 1,
      totalCount: 0,
      itemPerPage: 20,
      showModalLoader : false,
      search_name: '',
      search_email: '',
      search_status: '',
      remove_search: false,
      get_access_data:false,
    };
  }

  componentDidMount() {
    const superAdmin  = getSuperAdmin(localStorage.admin_token);
    
    if(superAdmin === 1){
      this.setState({
        access: {
           view : true,
           add : true,
           edit : true,
           delete : true
         },
         get_access_data:true
     });
     this.getAgentList();
    }else{
      const adminGroup  = getAdminGroup(localStorage.admin_token);
      API.get(`/api/adm_group/single_access/${adminGroup}/${'AGENT_MANAGEMENT'}`)
      .then(res => {
        this.setState({
          access: res.data.data,
          get_access_data:true
        }); 

        if(res.data.data.view === true){
          this.getAgentList(); 
        }else{
          this.props.history.push('/admin/dashboard');
        }
        
      })
      .catch(err => {
        showErrorMessage(err,this.props);
      });
    }
   // this.getAgentList();  
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getAgentList(pageNumber > 0 ? pageNumber  : 1);
  };

  getAgentList(page = 1) {

    let agentname    = this.state.search_name
    let email       = this.state.search_email
    let status      = this.state.search_status

    API.get(`/api/agents?page=${page}&agentname=${encodeURIComponent(agentname)}&email=${encodeURIComponent(email)}&status=${encodeURIComponent(status)}`)
      .then(res => {
        this.setState({
          agents: res.data.data,
          count_user: res.data.count_user,
          isLoading: false,
          search_name: agentname,
          search_email: email,
          search_status: status
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        showErrorMessage(err,this.props);
      });
  }

  agentSearch = (e) => {
    e.preventDefault();

    var agentname = document.getElementById('agentname').value
    var email = document.getElementById('email').value
    var status = document.getElementById('status').value

    if(agentname === "" && email === "" && status === ""){
      return false;
    }

    API.get(`/api/agents?page=1&agentname=${encodeURIComponent(agentname)}&email=${encodeURIComponent(email)}&status=${encodeURIComponent(status)}`)
    .then(res => {
      this.setState({
        agents: res.data.data,
        count_user: res.data.count_user,
        isLoading: false,
        search_name: agentname,
        search_email: email,
        search_status: status,
        remove_search: true
      });
    })
    .catch(err => {
      this.setState({
        isLoading: false
      });
      showErrorMessage(err,this.props);
    });
  }

  clearSearch = () => {
    document.getElementById('agentname').value     = "";
    document.getElementById('email').value       = "";
    document.getElementById('status').value      = ""

    this.setState({
      search_name: "",
      search_email: "",
      search_status: "",
      remove_search: false
    }, () => {
      this.getAgentList();
      this.setState({activePage: 1})
    })
  }

  getCountryList() {
    API.get("/api/customers/country")
      .then(res => {
        this.setState({
          countryList: res.data.data
        });
      })
      .catch(err => {
        showErrorMessage(err,this.props);
      });
  }

  
  getLanguageList() {
    API.get("/api/feed/adm_language")
      .then((res) => {
        this.setState({
          languageList: res.data.data,
        });
      })
      .catch((err) => {
        showErrorMessage(err, this.props);
      });
  }

  getCompanyList = () => {
    API.get('/api/customers/company')
      .then(res => {
        var myCompany = [];
        for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            myCompany.push({
            value: element["company_id"],
            label: htmlDecode(element["company_name"])
            });
        }
        this.setState({companyList:myCompany});        
      })
      .catch(err => {
        showErrorMessage(err,this.props);
      });
  }

  getAgentCompany(id) {
    var selAgtComp = [];
    var selComp = [];
    API.get(`/api/customers/company/${id}`)
      .then(res => { 
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          selAgtComp.push({
            value: element["company_id"],
            label: element["company_name"] 
          });
          selComp.push( element["company_id"] );
        }
        this.setState({
          selectedCompList: selAgtComp, 
          selectedComp: selComp
        });        
      })
      .catch(err => {
        showErrorMessage(err,this.props);
      });
  }

  getIndividualAgent(id, callBack) {
    API.get(`/api/agents/${id}`)
      .then(res => {
        this.setState({
          agentDetails: res.data.data          
        });
        callBack();
      })
      .catch(err => {
        showErrorMessage(err,this.props);
      });
  }

  modalCloseHandler = () => {
    this.setState({ agentflagId: 0 });
    this.setState({ showModal: false });
  };

  modalShowHandler = (event, id) => {
    this.getCompanyList();
    this.getCountryList();
    this.getLanguageList();
    if (id) {    
      event.preventDefault();  
      this.setState({ agentflagId: id });
      this.getAgentCompany(id); 
      this.getIndividualAgent(id, () => this.setState({ showModal: true }));
      
    } else {;
      this.setState({
        agentDetails: "",
        selectedCompList: "",
        selectedComp: "",
        showModal: true
      });
    }
  };

  handleSubmitEvent = (values, actions) => {
    
    const post_data = {
      first_name: values.first_name,
      last_name: values.last_name,
      email: values.email,
      status: values.status,
      fa_access:values.fa_access,
      company: values.company,
      phone_no: values.phone_no,
      country_id: values.country_id,
      language_code: values.language_code,
      agent_ref: values.agent_ref,
      dnd: values.dnd,
      multi_lang_access: values.multi_lang_access
    };

    console.log('post_data',post_data);

    if (this.state.agentflagId) {      
      this.setState({ showModalLoader: true });
      const id = this.state.agentflagId;
      API.put(`/api/agents/${id}`, post_data)
        .then(res => {
          this.modalCloseHandler();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record updated successfully.",
            icon: "success"
          }).then(() => {
            this.setState({ showModalLoader: false });
            this.getAgentList(this.state.activePage);
          });
        })
        .catch(err => {
          this.setState({ showModalLoader: false });
          if(err.data.status === 3){
            this.setState({
              showModal: false
            });
            showErrorMessage(err,this.props);
          }else{
            actions.setErrors(err.data.errors);
            actions.setSubmitting(false);
          }
        });
    } else {
      this.setState({showModalLoader: true});
      API.post("/api/agents", post_data)
        .then(res => {
          this.modalCloseHandler();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record added successfully.",
            icon: "success"
          }).then(() => {
            this.setState({ activePage: 1, showModalLoader: false });
            this.getAgentList(this.state.activePage);
          });
        })
        .catch(err => {    
          this.setState({showModalLoader: false});      
          if(err.data.status === 3){
            this.setState({
              showModal: false
            });
            showErrorMessage(err,this.props);
          }else{
            actions.setErrors(err.data.errors);
            actions.setSubmitting(false);
          }
        });
    }
  };

  confirmDelete = (event, id) => {
    event.preventDefault();
    swal({
      closeOnClickOutside: false,
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this!",
      icon: "warning",
      buttons: {
        cancel: {
          text: "Cancel",
          value: false,
          visible: true,
          className: "class3 class4"
        },
        confirm: {
          text: "OK",
          value: true,
          visible: true,
          className: "class1 class2",
          closeModal: true
        }
      }
    }).then(willDelete => {
      if (willDelete) {
        this.deleteAgent(id);
      }
    });
  };

  deleteAgent = id => {    
    API.delete(`/api/agents/${id}`).then(res => {
      swal({
        closeOnClickOutside: false,
        title: "Success",
        text: "Record deleted successfully.",
        icon: "success"
      }).then(() => {
        this.setState({ activePage: 1 });
        this.getAgentList(this.state.activePage);
      });
    }).catch(err => {      
      if(err.data.status === 3){
        this.setState({
          closeModal: true
        });
        showErrorMessage(err,this.props);
      }
    });
  };  

  renderShowsTotal = (start, to, total) => {
    return (
      <span className="pageShow">
        Showing {start} to {to}, of {total} records
      </span>
    );
  };

  downloadXLSX = (e) => {
    e.preventDefault();

    var agentname = document.getElementById('agentname').value;
    var email = document.getElementById('email').value;
    var status = document.getElementById('status').value;
    
    API.get(`/api/agents/download?page=1&agentname=${encodeURIComponent(agentname)}&email=${encodeURIComponent(email)}&status=${encodeURIComponent(status)}`,{responseType: 'blob'})
    .then(res => { 
      let url    = window.URL.createObjectURL(res.data);
      let a      = document.createElement('a');
      a.href     = url;
      a.download = 'agents.xlsx';
      a.click();

    }).catch(err => {
      showErrorMessage(err,this.props);
    });
  }

  checkHandler = (event) => {
    event.preventDefault();
  };

  render() {

    const { agentDetails } = this.state;

    const newInitialValues = Object.assign(initialValues, {
      first_name: agentDetails.first_name ? htmlDecode(agentDetails.first_name) : "",
      last_name: agentDetails.last_name ? htmlDecode(agentDetails.last_name) : "",
      email: agentDetails.email ? htmlDecode(agentDetails.email) : "",
      status: agentDetails.status || +agentDetails.status === 0 ? agentDetails.status.toString() : "",
      phone_no: agentDetails.phone_no ? htmlDecode(agentDetails.phone_no) : "",
      company: this.state.selectedComp ? this.state.selectedComp : "",
      country_id: agentDetails.country_id ? agentDetails.country_id.toString() : "",
      fa_access:agentDetails.new_feature_fa || +agentDetails.new_feature_fa === 0
        ? agentDetails.new_feature_fa.toString()
        : "",  
      language_code: agentDetails.language_code
        ? agentDetails.language_code.toString()
        : "", 
      agent_ref: agentDetails.agent_ref ? agentDetails.agent_ref : "",
      dnd: agentDetails.dnd || +agentDetails.dnd === 0 ? agentDetails.dnd.toString() : "2",
      multi_lang_access: agentDetails.multi_lang_access || +agentDetails.multi_lang_access === 0 ? agentDetails.multi_lang_access.toString() : "2",

      
    });

    
    const validateAdmin = Yup.object().shape({
      first_name: Yup.string().trim()
        .required("Please enter first name")
        .min(1, "First name can be minimum 1 characters long")  
        .matches(/^[A-Za-z0-9\s]*$/, "Invalid first name format! Only alphanumeric and spaces are allowed")      
        .max(30, "First name can be maximum 30 characters long"),
      last_name: Yup.string().trim()
        .required("Please enter last name")
        .min(1, "Last name can be minimum 1 characters long")  
        .matches(/^[A-Za-z0-9\s]*$/, "Invalid last name format! Only alphanumeric and spaces are allowed")
        .max(30, "Last name can be maximum 30 characters long"),
      fa_access: Yup.string()
        .trim()
        .required("Please select option")
        .matches(/^[0|1]$/, "Invalid option selected"),  
      email: Yup.string().trim()
        .required("Please enter email")
        .email("Invalid email")
        .max(80, "Email can be maximum 80 characters long"),
      status: Yup.string().trim()
        .required("Please select status")
        .matches(/^[0|1]$/, "Invalid status selected"),
      phone_no: Yup.string().trim()
        .required("Please enter phone number")
        .max(30, "Phone number cannot be more than 30 characters long"),
      company: Yup.array().ensure().min(1, "Please add at least one company.")
      .of(Yup.string().ensure().required("Company cannot be empty")),
      country_id: Yup.string().trim()
        .required("Please select country"),
        
      language_code: Yup.string().trim().required("Please select language"),
      dnd: Yup.string().trim()
        .required("Please select status for do not disturb")
        .matches(/^[1|2]$/, "Invalid status for do not disturb"),
      multi_lang_access: Yup.string().trim()
        .required("Please select status for Multi Language Access")
        .matches(/^[1|2]$/, "Invalid status for Multi Language Access"),
        
      agent_ref: Yup.string().trim()
        .required("Please enter employee reference")
        .min(2, "Employee reference can be minimum 2 characters long")
        .matches(/^[A-Za-z0-9]*$/, "Invalid format! Only alpha-numeric characters are allowed")
        .max(14, "Employee reference can be maximum 14 characters long"),
      /* agent_ref: Yup.string().trim().notRequired().test('agent_ref', 'Agent Reference must be alpha-numeric', function(value) {
        if (value !== '') {
          const schema = Yup.string().matches(/^[A-Za-z0-9]*$/);
          return schema.isValidSync(value);
        }
        return true;
      }) */
    });

    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy" >
                <img src={whitelogo} alt="logo" /> 
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <h1>
                    Manage Agent
                    <small />
                  </h1>
                  
                </div>
                <div className="col-lg-12 col-sm-12 col-xs-12 topSearchSection">
                {this.state.access.add === true ? 
                <div className="">
                  <button
                      type="button"
                      className="btn btn-info btn-sm"
                      onClick={e => this.modalShowHandler(e, "")}
                    >
                      <i className="fas fa-plus m-r-5" /> Add Agent
                    </button>
                  </div>
                  : null}

                  {/*  */}
                  <form className="form">
                        <div className="">
                        <input
                            className="form-control"
                            name="agentname"
                            id="agentname"
                            placeholder="Filter by name"
                          />
                        </div>

                        <div className="">
                        <input
                          className="form-control"
                          name="email"
                          id="email"
                          placeholder="Filter by email"
                        />
                        </div>                        

                        <div className="">
                        <select name="status" id="status" className="form-control">
                          <option value="">Select status</option>
                          <option value="1">Active</option>
                          <option value="0">Inactive</option>
                        </select>
                        </div>

                        <div className="">
                        <input
                          type="submit"
                          value="Search"
                          className="btn btn-warning btn-sm"
                          onClick={(e) => this.agentSearch( e )}
                        />
                        {this.state.remove_search ? <a onClick={() => this.clearSearch()} className="btn btn-danger btn-sm"> Remove </a> : null}
                        </div>
                        <div className="clearfix"></div>
                  </form>
                  {/*  */}
                  <div className="clearfix"></div>

                </div>
                {/* <div className="clearfix"></div>
                <div className="col-lg-12 col-sm-12 col-xs-12 mt-10">
                <button
                    type="button"
                    className="btn btn-info btn-sm"
                    onClick={e => this.modalShowHandler(e, "")}
                  >
                    <i className="fas fa-plus m-r-5" /> Add Agent
                  </button>
                </div> */}
              </div>
            </section>
            {/* <DashboardSearch groupList={this.state.groupList} /> */}
            <section className="content">
              <div className="box">


                <div className="box-body">
                  <div className="nav-tabs-custom">
                <ul className="nav nav-tabs">
  <li className="tabButtonSec pull-right"> {this.state.count_user > 0 ? 
          
          <span onClick={(e) => this.downloadXLSX(e)}>
              <LinkWithTooltip
                  tooltip={`Click here to download excel`}
                  href="#"
                  id="tooltip-my"
                  clicked={e => this.checkHandler(e)}
              >
                  <i className="fas fa-download"></i>
              </LinkWithTooltip>
          </span>
    
        : null }</li>
</ul>
</div>             
        
                  <BootstrapTable
                    data={this.state.agents}
                  >
                    <TableHeaderColumn isKey dataField="first_name" dataFormat={__htmlDecode(this)}>
                      First Name
                    </TableHeaderColumn>

                    <TableHeaderColumn dataField="last_name" dataFormat={__htmlDecode(this)}>
                      Last Name
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="language"
                      dataFormat={__htmlDecode(this)}
                      width={"8%"}
                    >
                      Language
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="email" dataFormat={__htmlDecode(this)}>
                      Email
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="activated"
                      dataFormat={vipStatus(this)}                     
                    >
                      Password Set
                    </TableHeaderColumn>
                    
                    <TableHeaderColumn
                      dataField="status"
                      dataFormat={custStatus(this)}                    
                    >Status</TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="pass_update_date"
                      dataFormat={setCreateDate(this)}
                      editable={false}
                      expandable={false}
                    >
                      Pass Update Date
                    </TableHeaderColumn>

                    <TableHeaderColumn dataField="display_login_date" dataFormat={loginDate(this)}>
                      Last Login Time
                    </TableHeaderColumn>

                    <TableHeaderColumn dataField="dnd" dataFormat={setDND(this)}>
                      Do Not Disturb
                    </TableHeaderColumn>
                    {this.state.access.edit === true || this.state.access.delete === true ?
                    <TableHeaderColumn
                      dataField="agent_id"
                      dataFormat={actionFormatter(this)}
                      dataAlign=""
                    >
                      Action
                    </TableHeaderColumn>
                    : null}
                  </BootstrapTable>

                  {this.state.count_user > 20 ? (
                    <Row>
                      <Col md={12}>
                        <div className="paginationOuter text-right">
                          <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemPerPage}
                            totalItemsCount={this.state.count_user}
                            itemClass='nav-item'
                            linkClass='nav-link'
                            activeClass='active'
                            onChange={this.handlePageChange}
                          />
                        </div>
                      </Col>
                    </Row>
                  ) : null}

                  {/* ======= Add/Edit Admin ======== */}

                 <Modal
                    show={this.state.showModal}
                    onHide={() => this.modalCloseHandler()} 
                    backdrop="static"
                  >
                    <Formik
                      initialValues={newInitialValues}
                      validationSchema={validateAdmin}
                      onSubmit={this.handleSubmitEvent}
                    >
                    
                      {({ values, errors, touched, isValid, isSubmitting,
                        setFieldValue,
                        setFieldTouched }) => {
                        return (
                          <Form>
                            {this.state.showModalLoader === true ? ( 
                                <div className="loading_reddy_outer">
                                    <div className="loading_reddy" >
                                        <img src={whitelogo} alt="loader"/>
                                    </div>
                                </div>
                              ) : ( "" )}
                            <Modal.Header closeButton>
                              <Modal.Title>
                                {this.state.agentflagId > 0 ? "Edit" : "Add"} Agent
                              </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="contBox">
                                <Row>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        First Name
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="first_name"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter first name"
                                        autoComplete="off"
                                      />
                                      {errors.first_name &&
                                      touched.first_name ? (
                                        <span className="errorMsg">
                                          {errors.first_name}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Last Name
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="last_name"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter last name"
                                        autoComplete="off"
                                      />
                                      {errors.last_name && touched.last_name ? (
                                        <span className="errorMsg">
                                          {errors.last_name}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                <Row>                                  
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Email<span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="email"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter email"
                                        autoComplete="off"
                                      />
                                      {errors.email && touched.email ? (
                                        <span className="errorMsg">
                                          {errors.email}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Status
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="status"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.status}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        {this.state.selectStatus.map(
                                          (status, i) => (
                                            <option key={i} value={status.id}>
                                              {status.name}
                                            </option>
                                          )
                                        )}
                                      </Field>
                                      {errors.status && touched.status ? (
                                        <span className="errorMsg">
                                          {errors.status}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row> 
                                <Row>                                  
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Agent Reference
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="agent_ref"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter reference"
                                        autoComplete="off"
                                      />
                                      {errors.agent_ref && touched.agent_ref ? (
                                        <span className="errorMsg">
                                          {errors.agent_ref}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>    
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Phone Number
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="phone_no"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter phone number"
                                        autoComplete="off"
                                      />
                                      {errors.phone_no && touched.phone_no ? (
                                        <span className="errorMsg">
                                          {errors.phone_no}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>                              
                                </Row>
                                <Row>
                                <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>
                                        Country
                                        <span className="impField">*</span>
                                      </label>

                                      <Field
                                        name="country_id"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.country_id}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        {this.state.countryList.map(
                                          (country, i) => (
                                            <option
                                              key={i}
                                              value={country.country_id}
                                            >
                                              {htmlDecode(country.country_name)}
                                            </option>
                                          )
                                        )}
                                      </Field>
                                      {errors.country_id && touched.country_id ? (
                                        <span className="errorMsg">
                                          {errors.country_id}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>
                                        Do Not Disturb
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="dnd"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.dnd}
                                      >
                                        <option key="2" value="2">
                                          No
                                        </option>
                                        <option key="1" value="1">
                                          Yes
                                        </option>
                                        
                                      </Field>
                                      {errors.dnd &&
                                      touched.dnd ? (
                                        <span className="errorMsg">
                                          {errors.dnd}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>

                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>
                                      Multi Language Access
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="multi_lang_access"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.multi_lang_access}
                                      >
                                        <option key="2" value="2">
                                          No
                                        </option>
                                        <option key="1" value="1">
                                          Yes
                                        </option>
                                        
                                      </Field>
                                      {errors.multi_lang_access &&
                                      touched.multi_lang_access ? (
                                        <span className="errorMsg">
                                          {errors.multi_lang_access}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                    <Col xs={12} sm={12} md={12}>
                                      <div className="form-group">
                                        <label>Assign Company</label>

                                        <Select
                                          isMulti
                                          name="company[]"
                                          options={this.state.companyList}
                                          className="basic-multi-select"
                                          classNamePrefix="select"
                                          onChange={evt =>
                                            setFieldValue(
                                              "company",
                                              [].slice
                                                .call(evt)
                                                .map(val => val.value)
                                            )
                                          }
                                          placeholder="Company"
                                          onBlur={() => setFieldTouched("company")}
                                          defaultValue={
                                            this.state.selectedCompList
                                          }
                                        />
                                        {errors.company && touched.company ? (
                                          <span className="errorMsg">
                                            {errors.company}
                                          </span>
                                        ) : null}
                                      </div>
                                    </Col>
                                  </Row> 

                                  <Row>        
                                <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Language
                                        <span className="impField">*</span>
                                      </label>

                                      <Field
                                        name="language_code"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.language_code}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        {this.state.languageList.map(
                                          (lang, i) => (
                                            <option
                                              key={i}
                                              value={lang.code}
                                            >
                                              {htmlDecode(lang.language)}
                                            </option>
                                          )
                                        )}
                                      </Field>
                                      {errors.language_code &&
                                      touched.language_code ? (
                                        <span className="errorMsg">
                                          {errors.language_code}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Formulation Assistant Access
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="fa_access"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.fa_access}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        {this.state.faAccess.map((status, i) => (
                                          <option key={i} value={status.id}>
                                            {status.name}
                                          </option>
                                        ))}
                                      </Field>
                                      {errors.fa_access &&
                                      touched.fa_access ? (
                                        <span className="errorMsg">
                                          {errors.fa_access}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>

                                  {errors.message ? (
                                      <Row>
                                        <Col xs={12} sm={12} md={12}>
                                          <span className="errorMsg">
                                            {errors.message}
                                          </span>
                                        </Col>
                                      </Row>
                                    ) : ( "" )
                                  }                                                             
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                             
                                <button
                                  className={`btn btn-success btn-sm ${
                                    isValid ? "btn-custom-green" : "btn-disable"
                                  } m-r-10`}
                                  type="submit"
                                  disabled={isValid ? false : true}
                                >
                                  {this.state.agentflagId > 0
                                    ? isSubmitting
                                      ? "Updating..."
                                      : "Update"
                                    : isSubmitting
                                    ? "Submitting..."
                                    : "Submit"}
                                </button>
                                <button
                                  onClick={e => this.modalCloseHandler()}
                                  className={`btn btn-danger btn-sm`}
                                  type="button"
                                >
                                  Close
                                </button>
                             
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal> 
                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}

export default Agent;
