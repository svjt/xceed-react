import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom'; 
import { getAdminName,getSuperAdmin } from '../../../shared/helper';


// connect to store
import { connect } from 'react-redux';
import { adminLogout } from "../../../store/actions/auth";


import logoImage from '../../../assets/images/drreddylogo_white.png';
import logoImageMini from '../../../assets/images/drreddylogosmall_white.png';
import userImage from '../../../assets/images/user2-160x160.jpg';

class Header extends Component {

    constructor(){
        super();
        this.state = {
            openProfile: false,
            openNotification: false,
            toggleMenu : false
        }
    }

     // SATYAJIT
     displayProfile = () => {
        this.setState({
            openProfile : !this.state.openProfile,
            openNotification : false
        });
    }
    // SATYAJIT
    displayNotification = () => {
        this.setState({
            openProfile : false,
            openNotification : !this.state.openNotification
        });
    }
    // SATYAJIT
    setContainerRef = (node) => {
        this.containerRef = node;
    }
    // SATYAJIT
    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
        // for web page
        document.body.classList.add('sidebar-open');
    }
    //SATYAJIT
    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }
    // SATYAJIT
    handleClickOutside = (event) => {
        if (this.containerRef && !this.containerRef.contains(event.target)) {
            this.setState({
                openProfile : false,
                openNotification : false
            });
        }
    }
    //SATYAJIT
    handleToggleMenu = () => {

        if(document.body.classList.contains('sidebar-open')) {
            document.body.classList.remove('sidebar-open');
            document.body.classList.add('sidebar-collapse');

        } else if(document.body.classList.contains('sidebar-collapse')) {
            document.body.classList.add('sidebar-open');
            document.body.classList.remove('sidebar-collapse');
        }
    }
    //SATYAJIT
    logout = () =>  {
        this.props.dispatch(adminLogout());
        this.props.history.push('/admin');
    }

      
    render() {

        if( this.props.isLoggedIn === false) return null;

        // display name
        const displayAdminName = getAdminName(localStorage.admin_token);

        const superAdmin       = getSuperAdmin(localStorage.admin_token);

        return (  
            <header className="main-header">
                    <Link to="/" className="logo">
                        {/*<!-- mini logo for sidebar mini 50x50 pixels -->*/}
                        <span className="logo-mini"><img src={logoImageMini} alt="Dr.Reddy's" /></span>
                        {/*<!-- logo for regular state and mobile devices -->*/}
                        <span className="logo-lg"><img src={logoImage} alt="Dr.Reddy's" /></span>
                    </Link>
                    <nav className="navbar navbar-static-top"> 
                   
                    
                    <span   className="sidebar-toggle sidebar-toggle-dektop"
                            onClick={this.handleToggleMenu}
                            data-toggle="offcanvas" role="button">

                        <i className="fas fa-bars"></i>
                    </span>

                    <span   className="sidebar-toggle sidebar-toggle-mobile"
                            onClick={this.handleToggleMenu}
                            data-toggle="offcanvas" role="button">

                        <i className="fas fa-bars"></i>
                    </span>

                        <div className="navbar-custom-menu">
                        <ul className="nav navbar-nav">
                            <li ref={this.setContainerRef} className={this.state.openProfile === true ? "dropdown user user-menu open" : "dropdown user user-menu"} onClick={this.displayProfile}>

                            <span className="dropdown-toggle" data-toggle="dropdown">
                                <img src={userImage} className="user-image" alt="User Img" />

                                <span className="hidden-xs user-name">{displayAdminName}</span>
                                <i className="fa fa-angle-down"></i>
                                <div className="clearFix"></div>
                            </span>

                                <ul className="dropdown-menu">
                                    <li className="user-header"> 
                                    <img src={userImage} className="img-circle" alt="User Img"/>
                                    <p> {displayAdminName} - Admin <small>Dr. Reddy's Lab</small> </p>
                                    </li>                                    
                                    <li className="user-footer">
                                    <div className="pull-left">
                                        <Link to="/admin/profile" className="btn btn-default btn-flat">Profile</Link>
                                    </div>
                                    <div className="pull-right">
                                    <button className="btn btn-default btn-flat" onClick={this.logout}>Sign out</button>
                                    </div>
                                    </li>
                                </ul>

                            </li>
                        </ul>
                        </div>
                    </nav>
                </header>
        );          
    }

}

const mapStateToProps = state => {
	return {
	  ...state
	};
};

export default withRouter(connect(mapStateToProps)(Header));
