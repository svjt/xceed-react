import React, { Component } from 'react';
import Pagination from "react-js-pagination";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal
} from "react-bootstrap";
//import { Label } from 'reactstrap';
import { Link } from "react-router-dom";
import { Formik, Field, Form } from "formik";
import swal from "sweetalert";
import * as Yup from "yup";


import Layout from "../layout/Layout";
import whitelogo from '../../../assets/images/drreddylogo_white.png';
import API from "../../../shared/admin-axios";
import { showErrorMessage } from "../../../shared/handle_error";
import { htmlDecode } from '../../../shared/helper';
import { getSuperAdmin, getAdminGroup } from '../../../shared/helper';

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

const actionFormatter = refObj => cell => {
  //console.log(refObj.state.access)
  return (

    <div className="actionStyle">
      {refObj.state.access.edit === true ?
        <LinkWithTooltip
          tooltip="Click to Edit"
          href="#"
          clicked={e => refObj.modalShowHandler(e, cell)}
          id="tooltip-1"
        >
          <i className="far fa-edit" />
        </LinkWithTooltip>
        : null}
      {refObj.state.access.delete === true ?
        <LinkWithTooltip
          tooltip="Click to Delete"
          href="#"
          clicked={e => refObj.confirmDelete(e, cell)}
          id="tooltip-1"
        >
          <i className="far fa-trash-alt" />
        </LinkWithTooltip>
        : null}
    </div>
  );
};

const custStatus = () => cell => {
  return cell === 1 ? "Active" : "Inactive";
};

const custContent = () => cell => {
  return htmlDecode(cell);
};

const initialValues = {
  region_name: '',
  status: '',
  type: '',
};

class Region extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      get_access_data: false,
      activePage: 1,
      totalCount: 0,
      itemPerPage: 10,
      regionDetails: [],
      regionflagId: 0,
      selectStatus: [
        { id: "0", name: "Inactive" },
        { id: "1", name: "Active" }
      ],
      selectType: [
        { id: "1" , name: "Closing in 24 Hours" },
        { id: "2" , name: "Breached" },
        { id:"3"  , name: "Responding to the customer"}
      ],
      showModal: false,
      showModalLoader: false
    };
  }

  componentDidMount() {

    const superAdmin = getSuperAdmin(localStorage.admin_token);

    if (superAdmin === 1) {
      this.setState({
        access: {
          view: true,
          add: true,
          edit: true,
          delete: true
        },
        get_access_data: true
      });
      this.getTextSuggestionList();
    } else {
      const adminGroup = getAdminGroup(localStorage.admin_token);
      API.get(`/api/adm_group/single_access/${adminGroup}/${'TAG_MANAGEMENT'}`)
        .then(res => {
          this.setState({
            access: res.data.data,
            get_access_data: true
          });

          if (res.data.data.view === true) {
            this.getTextSuggestionList();
          } else {
            this.props.history.push('/admin/dashboard');
          }

        })
        .catch(err => {
          showErrorMessage(err, this.props);
        });
    }
  }

  getTextSuggestionList(page = 1) {
    API.get(`/api/feed/txt_suggestion_list?page=${page}`)
      .then(res => {
        this.setState({
          region: res.data.data,
          count: res.data.count_txt_suggestion_list,
          isLoading: false
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        showErrorMessage(err, this.props);
      });
    this.setState({
      isLoading: false
    });
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getTextSuggestionList(pageNumber > 0 ? pageNumber : 1);
  };

  modalShowHandler = (event, id) => {
    if (id) {
      event.preventDefault();
      API.get(`/api/feed/get_txt_suggestion/${id}`)
        .then(res => {
          this.setState({
            regionDetails: res.data.data,
            regionflagId: id,
            isLoading: false,
            showModal: true
          });
        })
        .catch(err => {
          showErrorMessage(err, this.props);
        });
    } else {
      this.setState({
        regionDetails: [],
        regionflagId: 0,
        showModal: true
      });
    }
  };

  modalCloseHandler = () => {
    this.setState({ regionflagId: 0 });
    this.setState({ showModal: false });
  };

  handleSubmitEvent = (values, actions) => {

    const post_data = {
      name: values.name,
      status: values.status,
      type: values.type
    };

    if (this.state.regionflagId) {
      this.setState({ showModalLoader: true });
      const id = this.state.regionflagId;
      API.put(`/api/feed/update_txt_suggestion/${id}`, post_data)
        .then(res => {
          this.modalCloseHandler();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record updated successfully.",
            icon: "success"
          }).then(() => {
            this.setState({ showModalLoader: false });
            this.getTextSuggestionList(this.state.activePage);
          });
        })
        .catch(err => {
          this.setState({ showModalLoader: false });
          if (err.data.status === 3) {
            this.setState({
              showModal: false
            });
            showErrorMessage(err, this.props);
          } else {
            actions.setErrors(err.data.errors);
            actions.setSubmitting(false);
          }
        });
    } else {
      this.setState({ showModalLoader: true });
      API.post("/api/feed/add_txt_suggestion", post_data)
        .then(res => {
          this.modalCloseHandler();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record added successfully.",
            icon: "success"
          }).then(() => {
            this.setState({ activePage: 1, showModalLoader: false });
            this.getTextSuggestionList(this.state.activePage);
          });
        })
        .catch(err => {
          this.setState({ showModalLoader: false });
          if (err.data.status === 3) {
            this.setState({
              showModal: false
            });
            showErrorMessage(err, this.props);
          } else {
            actions.setErrors(err.data.errors);
            actions.setSubmitting(false);
          }
        });
    }
  };

  confirmDelete = (event, id) => {
    event.preventDefault();
    swal({
      closeOnClickOutside: false,
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this!",
      icon: "warning",
      buttons: true,
      dangerMode: true
    }).then(willDelete => {
      if (willDelete) {
        this.deleteRegion(id);
      }
    });
  };

  deleteRegion = id => {
    if (id) {
      API.delete(`/api/feed/delete_txt_suggestion/${id}`).then(res => {
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: "Record deleted successfully.",
          icon: "success"
        }).then(() => {
          this.setState({ activePage: 1 });
          this.getTextSuggestionList(this.state.activePage);
        });
      }).catch(err => {
        if (err.data.status === 3) {
          this.setState({ closeModal: true });
          showErrorMessage(err, this.props);
        }
      });
    }
  };

  downloadXLSX = (e) => {
    e.preventDefault();

    API.get(`/api/feed/txt_suggestion_list_download?page=1`, { responseType: 'blob' })
      .then(res => {
        let url = window.URL.createObjectURL(res.data);
        let a = document.createElement('a');
        a.href = url;
        a.download = 'tag_management.xlsx';
        a.click();

      }).catch(err => {
        showErrorMessage(err, this.props);
      });
  }

  checkHandler = (event) => {
    event.preventDefault();
  };


  render() {
    const { regionDetails } = this.state;
    const newInitialValues = Object.assign(initialValues, {
      name: regionDetails.name ? htmlDecode(regionDetails.name) : "",
      status: regionDetails.status || +regionDetails.status === 0 ? regionDetails.status.toString() : "",
      type: regionDetails.type ? regionDetails.type.toString() : ""
    });

    const validateStopFlag = Yup.object().shape({
      name: Yup.string()
        .min(2, 'Name must be at least 2 characters')
        .max(200, 'Name must be at most 200 characters')
        .required("Please enter name"),
      status: Yup.string().trim()
        .required("Please select status")
        .matches(/^[0|1]$/, "Invalid status selected"),
      type: Yup.string().trim()
        .required("Please select type")
        .matches(/^[1|2|3]$/, "Invalid type selected")
    });

    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy" >
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <h1>
                    Tags Management
                  <small />
                  </h1>

                </div>
                <div className="col-lg-12 col-sm-12 col-xs-12 topSearchSection">
                  {this.state.access.add === true ?
                    <div className="">
                      <button
                        type="button"
                        className="btn btn-info btn-sm"
                        onClick={e => this.modalShowHandler(e, "")}
                      >
                        <i className="fas fa-plus m-r-5" /> Add New Tag
                    </button>
                    </div>
                    : null}
                  <div className="clearfix"></div>
                </div>
              </div>
            </section>
            <section className="content">
              <div className="box">

                <div className="box-body">

                  <div className="nav-tabs-custom">
                    <ul className="nav nav-tabs">
                      <li className="tabButtonSec pull-right">
                        {this.state.count > 0 ?
                          <span onClick={(e) => this.downloadXLSX(e)} >
                            <LinkWithTooltip
                              tooltip={`Click here to download excel`}
                              href="#"
                              id="tooltip-my"
                              clicked={e => this.checkHandler(e)}
                            >
                              <i className="fas fa-download"></i>
                            </LinkWithTooltip>
                          </span>
                          : null}
                      </li>
                    </ul>
                  </div>

                  <BootstrapTable data={this.state.region}>
                    <TableHeaderColumn isKey dataField="name" dataFormat={custContent(this)}>
                      Name
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="status" dataFormat={custStatus(this)}>
                      Status
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="type">
                      Type
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="display_add_date"
                      editable={false}
                      expandable={false}
                    >
                      Date Added
                    </TableHeaderColumn>
                    {this.state.access.edit === true || this.state.access.delete === true ?
                      <TableHeaderColumn
                        dataField="id"
                        dataFormat={actionFormatter(this)}
                      >
                        Action
                    </TableHeaderColumn>
                      : null}
                  </BootstrapTable>

                  {this.state.count > this.state.itemPerPage ? (
                    <Row>
                      <Col md={12}>
                        <div className="paginationOuter text-right">
                          <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemPerPage}
                            totalItemsCount={this.state.count}
                            itemClass='nav-item'
                            linkClass='nav-link'
                            activeClass='active'
                            onChange={this.handlePageChange}
                          />
                        </div>
                      </Col>
                    </Row>
                  ) : null}

                  {/* ======= Add/Edit ======== */}
                  <Modal
                    show={this.state.showModal}
                    onHide={() => this.modalCloseHandler()} backdrop="static"
                  >
                    <Formik
                      initialValues={newInitialValues}
                      validationSchema={validateStopFlag}
                      onSubmit={this.handleSubmitEvent}
                    >
                      {({ values, errors, touched, isValid, isSubmitting }) => {
                        return (
                          <Form>
                            {this.state.showModalLoader === true ? (
                              <div className="loading_reddy_outer">
                                <div className="loading_reddy" >
                                  <img src={whitelogo} alt="loader" />
                                </div>
                              </div>
                            ) : ("")}
                            <Modal.Header closeButton>
                              <Modal.Title>
                                {this.state.regionflagId > 0 ? "Edit" : "Add"}{" "}
                                Tag
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="contBox">
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>
                                        Name<span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="name"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter name"
                                        autoComplete="off"
                                        value={values.name}
                                      />
                                      {errors.name && touched.name ? (
                                        <span className="errorMsg">
                                          {errors.name}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                               
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>
                                        Type
                                          <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="type"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.type}
                                      >
                                        <option key="-1" value="">
                                          Select
                                          </option>
                                        {this.state.selectType.map(
                                          (type, i) => (
                                            <option key={i} value={type.id}>
                                              {type.name}
                                            </option>
                                          )
                                        )}
                                      </Field>
                                      {errors.type && touched.type ? (
                                        <span className="errorMsg">
                                          {errors.type}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>
                                        Status
                                          <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="status"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.status}
                                      >
                                        <option key="-1" value="">
                                          Select
                                          </option>
                                        {this.state.selectStatus.map(
                                          (status, i) => (
                                            <option key={i} value={status.id}>
                                              {status.name}
                                            </option>
                                          )
                                        )}
                                      </Field>
                                      {errors.status && touched.status ? (
                                        <span className="errorMsg">
                                          {errors.status}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                {errors.message ? (
                                  <Row>
                                    <Col xs={12} sm={12} md={12}>
                                      <span className="errorMsg">
                                        {errors.message}
                                      </span>
                                    </Col>
                                  </Row>
                                ) : ("")
                                }
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                              <button
                                className={`btn btn-success btn-sm 
                                ${isValid ? "btn-custom-green" : "btn-disable"}                                  
                                  mr-2`}
                                type="submit"

                              >
                                {this.state.regionflagId > 0
                                  ? isSubmitting
                                    ? "Updating..."
                                    : "Update"
                                  : isSubmitting
                                    ? "Submitting..."
                                    : "Submit"}
                              </button>
                              <button
                                onClick={e => this.modalCloseHandler()}
                                className={`btn btn-danger btn-sm`}
                                type="button"
                              >
                                Close
                                  </button>
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>
                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}
export default Region;