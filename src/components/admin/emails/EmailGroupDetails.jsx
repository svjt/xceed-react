import React, { Component } from "react";
import Pagination from "react-js-pagination";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal,
} from "react-bootstrap";
//import { Label } from 'reactstrap';
import { Link } from "react-router-dom";
import { Formik, Field, Form } from "formik";
import swal from "sweetalert";
import * as Yup from "yup";

import Layout from "../layout/Layout";
import whitelogo from "../../../assets/images/drreddylogo_white.png";
import API from "../../../shared/admin-axios";
import { showErrorMessage } from "../../../shared/handle_error";
import { htmlDecode } from "../../../shared/helper";
import { getSuperAdmin, getAdminGroup } from "../../../shared/helper";
import { Editor } from "@tinymce/tinymce-react";

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

const actionFormatter = (refObj) => (cell) => {
  //console.log(refObj.state.access)
  return (
    <div className="actionStyle">
      {refObj.state.access.edit === true ? (
        <LinkWithTooltip
          tooltip="Click to Edit"
          href="#"
          clicked={(e) => refObj.modalShowHandler(e, cell)}
          id="tooltip-1"
        >
          <i className="far fa-edit" />
        </LinkWithTooltip>
      ) : null}
    </div>
  );
};

const custContent = () => (cell) => {
  return htmlDecode(cell);
};

const initialValues = {
  subject: "",
  content: "",
};

class EmailGroupDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      get_access_data: false,
      activePage: 1,
      totalCount: 0,
      itemPerPage: 20,
      emailDetails: [],
      emailflagId: 0,
      showModal: false,
      showModalLoader: false,
    };
  }

  componentDidMount() {
    const superAdmin = getSuperAdmin(localStorage.admin_token);

    if (superAdmin === 1) {
      this.setState({
        access: {
          view: true,
          add: true,
          edit: true,
          delete: true,
        },
        get_access_data: true,
      });
      this.getEmailList();
    } else {
      const adminGroup = getAdminGroup(localStorage.admin_token);
      API.get(
        `/api/adm_group/single_access/${adminGroup}/${"EMAILS_MANAGEMENT"}`
      )
        .then((res) => {
          this.setState({
            access: res.data.data,
            get_access_data: true,
          });

          if (res.data.data.view === true) {
            this.getEmailList();
          } else {
            this.props.history.push("/admin/dashboard");
          }
        })
        .catch((err) => {
          showErrorMessage(err, this.props);
        });
    }
  }

  getEmailList(page = 1) {
    let email_type_id = this.props.match.params.id;
    API.get(`/api/feed/email_group/${email_type_id}/?page=${page}`)
      .then((res) => {
        this.setState({
          emails: res.data.data,
          count: res.data.count_email_group,
          isLoading: false,
        });
      })
      .catch((err) => {
        this.setState({
          isLoading: false,
        });
        showErrorMessage(err, this.props);
      });
    this.setState({
      isLoading: false,
    });
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getEmailList(pageNumber > 0 ? pageNumber : 1);
  };

  modalShowHandler = (event, id) => {
    if (id) {
      event.preventDefault();
      API.get(`/api/feed/get_email/${id}`)
        .then((res) => {
          this.setState({
            emailDetails: res.data.data,
            emailflagId: id,
            isLoading: false,
            showModal: true,
          });
        })
        .catch((err) => {
          showErrorMessage(err, this.props);
        });
    }
  };

  modalCloseHandler = () => {
    this.setState({ emailflagId: 0 });
    this.setState({ showModal: false });
  };

  handleSubmitEvent = (values, actions) => {
    const post_data = {
      subject: values.subject,
      content: values.content,
    };

    if (this.state.emailflagId) {
      this.setState({ showModalLoader: true });
      const id = this.state.emailflagId;
      API.put(`/api/feed/update_email/${id}`, post_data)
        .then((res) => {
          this.modalCloseHandler();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record updated successfully.",
            icon: "success",
          }).then(() => {
            this.setState({ showModalLoader: false });
            this.getEmailList(this.state.activePage);
          });
        })
        .catch((err) => {
          this.setState({ showModalLoader: false });
          if (err.data.status === 3) {
            this.setState({
              showModal: false,
            });
            showErrorMessage(err, this.props);
          } else {
            actions.setErrors(err.data.errors);
            actions.setSubmitting(false);
          }
        });
    }
  };

  render() {
    const { emailDetails } = this.state;
    const newInitialValues = Object.assign(initialValues, {
      subject: emailDetails.subject ? htmlDecode(emailDetails.subject) : "",
      content: emailDetails.content ? htmlDecode(emailDetails.content) : "",
      email_code: emailDetails.email_code
        ? htmlDecode(emailDetails.email_code)
        : "",
      language: emailDetails.language ? htmlDecode(emailDetails.language) : "",
      params: emailDetails.params ? htmlDecode(emailDetails.params) : "",
    });
    const validateStopFlag = Yup.object().shape({
      subject: Yup.string().trim().required("Please enter subject"),
      content: Yup.string().trim().required("Please enter content"),
    });

    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy">
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <h1>
                    Manage Emails
                    <small />
                  </h1>
                </div>
                <div className="col-lg-12 col-sm-12 col-xs-12 topSearchSection">
                  <div className="clearfix"></div>
                </div>
              </div>
            </section>
            <section className="content">
              <div className="box">
                <div className="box-body">
                  <BootstrapTable data={this.state.emails}>
                    <TableHeaderColumn
                      isKey
                      dataField="email_code"
                      width="30%"
                      dataFormat={custContent(this)}
                    >
                      Code
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="language"
                      width="15%"
                      dataFormat={custContent(this)}
                    >
                      Language
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="subject"
                      dataFormat={custContent(this)}
                    >
                      Subject
                    </TableHeaderColumn>
                    {this.state.access.edit === true ? (
                      <TableHeaderColumn
                        dataField="email_id"
                        width="10%"
                        dataFormat={actionFormatter(this)}
                      >
                        Action
                      </TableHeaderColumn>
                    ) : null}
                  </BootstrapTable>

                  {this.state.count > this.state.itemPerPage ? (
                    <Row>
                      <Col md={12}>
                        <div className="paginationOuter text-right">
                          <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemPerPage}
                            totalItemsCount={this.state.count}
                            itemClass="nav-item"
                            linkClass="nav-link"
                            activeClass="active"
                            onChange={this.handlePageChange}
                          />
                        </div>
                      </Col>
                    </Row>
                  ) : null}

                  {/* ======= Add/Edit ======== */}
                  <Modal
                    show={this.state.showModal}
                    onHide={() => this.modalCloseHandler()}
                    backdrop="static"
                  >
                    <Formik
                      initialValues={newInitialValues}
                      validationSchema={validateStopFlag}
                      onSubmit={this.handleSubmitEvent}
                    >
                      {({
                        values,
                        errors,
                        touched,
                        isValid,
                        isSubmitting,
                        setFieldValue,
                      }) => {
                        return (
                          <Form>
                            {this.state.showModalLoader === true ? (
                              <div className="loading_reddy_outer">
                                <div className="loading_reddy">
                                  <img src={whitelogo} alt="loader" />
                                </div>
                              </div>
                            ) : (
                              ""
                            )}
                            <Modal.Header closeButton>
                              <Modal.Title>
                                {this.state.emailflagId > 0 ? "Edit" : "Add"}{" "}
                                Email
                              </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="contBox">
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>Code</label>
                                      <div>{values.email_code}</div>
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>Params</label>
                                      <div>{values.params}</div>
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>Language</label>
                                      <div>{values.language}</div>
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>
                                        Subject
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="subject"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter subject"
                                        autoComplete="off"
                                        value={values.subject}
                                      />
                                      {errors.subject && touched.subject ? (
                                        <span className="errorMsg">
                                          {errors.subject}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>
                                        Content
                                        <span className="impField">*</span>
                                      </label>
                                      <Editor
                                        name="content"
                                        className={`selectArowGray form-control`}
                                        value={
                                          values.content !== null &&
                                          values.content !== ""
                                            ? values.content
                                            : ""
                                        }
                                        content={
                                          values.content !== null &&
                                          values.content !== ""
                                            ? values.content
                                            : ""
                                        }
                                        init={{
                                          menubar: false,
                                          branding: false,
                                          placeholder: "Enter content",
                                          plugins:
                                            "link table hr visualblocks code placeholder lists autoresize textcolor",
                                          toolbar:
                                            "bold italic strikethrough superscript subscript | forecolor backcolor | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | visualblocks code | fontselect",
                                          font_formats:
                                            "Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats",
                                          color_map: [
                                            "000000",
                                            "Black",
                                            "808080",
                                            "Gray",
                                            "FFFFFF",
                                            "White",
                                            "FF0000",
                                            "Red",
                                            "FFFF00",
                                            "Yellow",
                                            "008000",
                                            "Green",
                                            "0000FF",
                                            "Blue",
                                          ],
                                        }}
                                        onEditorChange={(value) =>
                                          setFieldValue("content", value)
                                        }
                                      />
                                      {errors.content && touched.content ? (
                                        <span className="errorMsg">
                                          {errors.content}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>

                                {errors.message ? (
                                  <Row>
                                    <Col xs={12} sm={12} md={12}>
                                      <span className="errorMsg">
                                        {errors.message}
                                      </span>
                                    </Col>
                                  </Row>
                                ) : (
                                  ""
                                )}
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                              <button
                                className={`btn btn-success btn-sm ${
                                  isValid ? "btn-custom-green" : "btn-disable"
                                } mr-2`}
                                type="submit"
                                disabled={isValid ? false : false}
                              >
                                {this.state.emailflagId > 0
                                  ? isSubmitting
                                    ? "Updating..."
                                    : "Update"
                                  : isSubmitting
                                  ? "Submitting..."
                                  : "Submit"}
                              </button>
                              <button
                                onClick={(e) => this.modalCloseHandler()}
                                className={`btn btn-danger btn-sm`}
                                type="button"
                              >
                                Close
                              </button>
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>
                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}
export default EmailGroupDetails;
