import React, { Component } from "react";
import Pagination from "react-js-pagination";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import { Formik, Field, Form } from "formik";
import swal from "sweetalert";

import Layout from "../layout/Layout";
import whitelogo from "../../../assets/images/drreddylogo_white.png";
import API from "../../../shared/admin-axios";
import { showErrorMessage } from "../../../shared/handle_error";
import { htmlDecode } from "../../../shared/helper";
import { getSuperAdmin, getAdminGroup } from "../../../shared/helper";
import ReactHtmlParser from "react-html-parser";

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

const actionFormatter = (refObj) => (cell) => {
  //console.log(refObj.state.access)
  return (
    <div className="actionStyle">
      {refObj.state.access.view === true ? (
        <LinkWithTooltip
          tooltip="Click to view"
          href="#"
          clicked={(e) => refObj.modalShowHandler(e, cell)}
          id="tooltip-1"
        >
          <i className="fas fa-eye" />
        </LinkWithTooltip>
      ) : null}
    </div>
  );
};

const custContent = () => (cell) => {
  return htmlDecode(cell);
};

const initialValues = {
  subject: "",
  content: "",
};

class Emails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      get_access_data: false,
      activePage: 1,
      totalCount: 0,
      itemPerPage: 20,
      emailLogDetails: [],
      emailflagId: 0,
      showModal: false,
      showModalLoader: false,
      search_to_email: "",
      search_subject: "",
    };
  }

  componentDidMount() {
    const superAdmin = getSuperAdmin(localStorage.admin_token);

    if (superAdmin === 1) {
      this.setState({
        access: {
          view: true,
          add: true,
          edit: true,
          delete: true,
        },
        get_access_data: true,
      });
      this.getEmailLogList();
    } else {
      const adminGroup = getAdminGroup(localStorage.admin_token);
      API.get(
        `/api/adm_group/single_access/${adminGroup}/${"EMAIL_LOG_MANAGEMENT"}`
      )
        .then((res) => {
          this.setState({
            access: res.data.data,
            get_access_data: true,
          });

          if (res.data.data.view === true) {
            this.getEmailLogList();
          } else {
            this.props.history.push("/admin/dashboard");
          }
        })
        .catch((err) => {
          showErrorMessage(err, this.props);
        });
    }
  }

  getEmailLogList(page = 1) {
    let to_email = this.state.search_to_email;
    let subject = this.state.search_subject;
    API.get(`/api/feed/email_log?page=${page}&to_email=${encodeURIComponent(to_email)}&subject=${encodeURIComponent(subject)}`)
      .then((res) => {
        this.setState({
          emails: res.data.data,
          count: res.data.count_email_log,
          isLoading: false,
          search_to_email: to_email,
          search_subject: subject,
        });
      })
      .catch((err) => {
        this.setState({
          isLoading: false,
        });
        showErrorMessage(err, this.props);
      });
    this.setState({
      isLoading: false,
    });
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getEmailLogList(pageNumber > 0 ? pageNumber : 1);
  };

  modalShowHandler = (event, id) => {
    if (id) {
      event.preventDefault();
      API.get(`/api/feed/get_email_log/${id}`)
        .then((res) => {
          this.setState({
            emailLogDetails: res.data.data,
            emailflagId: id,
            isLoading: false,
            showModal: true,
          });
        })
        .catch((err) => {
          showErrorMessage(err, this.props);
        });
    }
  };

  modalCloseHandler = () => {
    this.setState({ emailflagId: 0 });
    this.setState({ showModal: false });
  };

  customSearch = (e) => {
    e.preventDefault();
    var to_email = document.getElementById('to_email').value;
    var subject = document.getElementById('subject').value;

    if (to_email === "" && subject === "") {
      return false;
    }

    API.get(`/api/feed/email_log?page=1&to_email=${encodeURIComponent(to_email)}&subject=${encodeURIComponent(subject)}`)
      .then(res => {
        this.setState({
          emails: res.data.data,
          count: res.data.count_email_log,
          isLoading: false,
          search_to_email: to_email,
          search_subject: subject,
          remove_search: true,
          activePage: 1,
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        showErrorMessage(err, this.props);
      });
  }

  clearSearch = () => {
    document.getElementById('to_email').value = "";
    document.getElementById('subject').value = "";

    this.setState({
      search_to_email: "",
      search_subject: "",
      remove_search: false
    }, () => {
      this.getEmailLogList();
      this.setState({ activePage: 1 })
    })
  }

  render() {
    const { emailLogDetails } = this.state;

    let formType = "";
    let toType = "";
    if (emailLogDetails.from_type === "s") {
      formType = "System";
    } else {
      formType = "Other";
    }
    if (emailLogDetails.to_type === "e") {
      toType = "Employee";
    } else if (emailLogDetails.to_type === "c") {
      toType = "Customer";
    } else {
      toType = "Agent";
    }

    const newInitialValues = Object.assign(initialValues, {
      email_code: emailLogDetails.email_code
        ? htmlDecode(emailLogDetails.email_code)
        : "",
      from_email: emailLogDetails.from_email
        ? htmlDecode(emailLogDetails.from_email)
        : "",
      email_code: emailLogDetails.email_code
        ? htmlDecode(emailLogDetails.email_code)
        : "",
      to_email: emailLogDetails.to_email
        ? htmlDecode(emailLogDetails.to_email)
        : "",
      from_type: formType,
      to_type: toType,
      mail_subject: emailLogDetails.mail_subject
        ? htmlDecode(emailLogDetails.mail_subject)
        : "",
      mail_content: emailLogDetails.mail_content
        ? htmlDecode(emailLogDetails.mail_content)
        : "",
    });

    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy">
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <h1>
                    Emails Log
                    <small />
                  </h1>
                </div>
                <div className="col-lg-12 col-sm-12 col-xs-12 topSearchSection">
                  <form className="form">
                    <div className="">
                      <input
                        className="form-control"
                        name="to_email"
                        id="to_email"
                        placeholder="Filter by to email"
                      />
                    </div>

                    <div className="">
                      <input
                        className="form-control"
                        name="subject"
                        id="subject"
                        placeholder="Filter by subject"
                      />
                    </div>

                    <div className="">
                      <input
                        type="submit"
                        value="Search"
                        className="btn btn-warning btn-sm"
                        onClick={(e) => this.customSearch(e)}
                      />
                      {this.state.remove_search ? <a onClick={() => this.clearSearch()} className="btn btn-danger btn-sm"> Remove </a> : null}
                    </div>
                    <div className="clearfix"></div>
                  </form>
                  <div className="clearfix"></div>
                </div>
              </div>
            </section>
            <section className="content">
              <div className="box">
                <div className="box-body">
                  <BootstrapTable data={this.state.emails}>
                    <TableHeaderColumn
                      isKey
                      dataField="from_email"
                      dataFormat={custContent(this)}
                    >
                      From Email
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="to_email"
                      dataFormat={custContent(this)}
                    >
                      To Email
                    </TableHeaderColumn>

                    <TableHeaderColumn
                      dataField="mail_subject"
                      width="36%"
                      dataFormat={custContent(this)}
                    >
                      Subject
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="display_add_date">
                      Sent Date
                    </TableHeaderColumn>
                    {this.state.access.view === true ? (
                      <TableHeaderColumn
                        dataField="email_log_id"
                        width="10%"
                        dataFormat={actionFormatter(this)}
                      >
                        Action
                      </TableHeaderColumn>
                    ) : null}
                  </BootstrapTable>

                  {this.state.count > this.state.itemPerPage ? (
                    <Row>
                      <Col md={12}>
                        <div className="paginationOuter text-right">
                          <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemPerPage}
                            totalItemsCount={this.state.count}
                            itemClass="nav-item"
                            linkClass="nav-link"
                            activeClass="active"
                            onChange={this.handlePageChange}
                          />
                        </div>
                      </Col>
                    </Row>
                  ) : null}

                  {/* ======= Add/Edit ======== */}
                  <Modal
                    show={this.state.showModal}
                    onHide={() => this.modalCloseHandler()}
                    backdrop="static"
                  >
                    <Formik initialValues={newInitialValues}>
                      {({ values }) => {
                        return (
                          <Form>
                            {this.state.showModalLoader === true ? (
                              <div className="loading_reddy_outer">
                                <div className="loading_reddy">
                                  <img src={whitelogo} alt="loader" />
                                </div>
                              </div>
                            ) : (
                                ""
                              )}
                            <Modal.Header closeButton>
                              <Modal.Title>View Email</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="contBox">
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>Code</label>
                                      <div>{values.email_code}</div>
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>From Email</label>
                                      <div>{values.from_email}</div>
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>To Email</label>
                                      <div>{values.to_email}</div>
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>From Type</label>
                                      <div>{values.from_type}</div>
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>To Type</label>
                                      <div>{values.to_type}</div>
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>Mail Subject</label>
                                      <div>{values.mail_subject}</div>
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>Content</label>
                                      <div>
                                        {ReactHtmlParser(values.mail_content)}
                                      </div>
                                    </div>
                                  </Col>
                                </Row>
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                              <button
                                onClick={(e) => this.modalCloseHandler()}
                                className={`btn btn-danger btn-sm`}
                                type="button"
                              >
                                Close
                              </button>
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>
                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}
export default Emails;
