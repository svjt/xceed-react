import React, { Component } from "react";
import Pagination from "react-js-pagination";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal,
} from "react-bootstrap";
//import { Label } from 'reactstrap';
import { Link } from "react-router-dom";
import { Formik, Field, Form } from "formik";
import swal from "sweetalert";
import * as Yup from "yup";

import Layout from "../layout/Layout";
import whitelogo from "../../../assets/images/drreddylogo_white.png";
import API from "../../../shared/admin-axios";
import { showErrorMessage } from "../../../shared/handle_error";
import { htmlDecode, inArray } from "../../../shared/helper";

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

const actionFormatter = (refObj) => (cell) => {
  return (
    <div className="actionStyle">
      <LinkWithTooltip
        tooltip="Click to view details"
        href={`/admin/email_group/${cell}`}
        id="tooltip-1"
      >
        <i className="fas fa-eye" />
      </LinkWithTooltip>
    </div>
  );
};
const custContent = () => (cell) => {
  return htmlDecode(cell);
};

const initialValues = {
  group_name: "",
};

class EmailGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
    };
  }

  componentDidMount() {
    this.getGroupList();
  }

  getGroupList(page = 1) {
    API.get(`/api/feed/email_group`)
      .then((res) => {
        this.setState({
          group: res.data.data,
          isLoading: false,
        });
      })
      .catch((err) => {
        this.setState({
          isLoading: false,
        });
        showErrorMessage(err, this.props);
      });
    this.setState({
      isLoading: false,
    });
  }

  render() {
    if (this.state.isLoading === true) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy">
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <h1>
                    Email Group
                    <small />
                  </h1>
                </div>
              </div>
            </section>
            <section className="content">
              <div className="box">
                <div className="box-body">
                  <BootstrapTable data={this.state.group}>
                    <TableHeaderColumn
                      isKey
                      dataField="email_type_name"
                      dataFormat={custContent(this)}
                    >
                      Name
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="email_type_id"
                      dataFormat={actionFormatter(this)}
                    >
                      Action
                    </TableHeaderColumn>
                  </BootstrapTable>
                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}
export default EmailGroup;
