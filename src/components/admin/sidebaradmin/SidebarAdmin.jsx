import React, { Component } from "react";
//import './sidebarAdmin.css';
import { Link } from "react-router-dom";
import { getAdminGroup } from "../../../shared/helper";
import API from "../../../shared/admin-axios";
import { getAdminName, getSuperAdmin } from "../../../shared/helper";
import { showErrorMessage } from "../../../shared/handle_error";

class SidebarAdmin extends Component {
  constructor() {
    super();
    this.state = {
      shown: "",
      super_admin: 0,
      api_end: false,
    };
  }

  /* toggleMenu( event ){
    event.preventDefault();
    this.setState({shown: !this.state.shown});
  } */

  componentDidMount = () => {
    var path = this.props.path_name; //console.log(path);
    if (
      path === "/admin/company" ||
      path === "/admin/customers" ||
      path === "/admin/master_company"
    ) {
      this.setState({ shown: "1" });
    }
    if (path === "/admin/sla" || path === "/admin/notificationsla") {
      this.setState({ shown: "2" });
    }
    if (path === "/admin/payment_terms" || path === "/admin/delivery_terms") {
      this.setState({ shown: "3" });
    }
    if (
      path === "/admin/events" ||
      path === "/admin/increased_sla" ||
      path === "/admin/paused_sla"
    ) {
      this.setState({ shown: "4" });
    }
    if (path === "/admin/error_log" || path === "/admin/crm_error") {
      this.setState({ shown: "5" });
    }
    if (
      path === "/admin/task_ratings" ||
      path === "/admin/task_rating_questions" ||
      path === "/admin/task_rating_options"
    ) {
      this.setState({ shown: "6" });
    }

    if (
      path === "/admin/fa_ratings" ||
      path === "/admin/fa_rating_questions" ||
      path === "/admin/fa_rating_options" ||
      path === "/admin/product_enquiries" ||
      path === "/admin/product_search"
    ) {
      this.setState({ shown: "7" });
    }

    if (this.props.isLoggedIn === true) {
      const superAdmin = getSuperAdmin(localStorage.admin_token);
      if (superAdmin === 1) {
        this.setState({ super_admin: 1, api_end: true });
      } else {
        let groupId = getAdminGroup(localStorage.admin_token);
        API.get(`/api/adm_group/sections/${groupId}`)
          .then((res) => {
            console.log(res.data.data);
            this.setState({
              access_data: res.data.data.access_permission,
              api_end: true,
            });
          })
          .catch((err) => {
            showErrorMessage(err, this.props);
          });
      }
    }
  };

  handleClick = (event) => {
    event.preventDefault();
    const id = event.target.getAttribute("data-id");
    this.setState({ shown: id });
  };

  getSuperAdminMenu = () => {
    const rotate = this.state.shown;
    return (
      <section className="sidebar">
        <ul className="sidebar-menu">
          {/* <li> <Link to="/admin" onClick={this.logout} > <i className="fas fa-sign-out-alt"></i> <span>Logout</span></Link>
          </li> */}
          {this.props.path_name === "/admin/dashboard" ? (
            <li className="active">
              {" "}
              <Link to="/admin/dashboard">
                {" "}
                <i className="fas fa-tachometer-alt"></i> <span>Dashboard</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/dashboard">
                  {" "}
                  <i className="fas fa-tachometer-alt"></i> <span>Dashboard</span>
                </Link>{" "}
              </li>
            )}

          {this.props.path_name === "/admin/tasks" ? (
            <li className="active">
              {" "}
              <Link to="/admin/tasks">
                {" "}
                <i className="fas fa-clipboard-list"></i>{" "}
                <span>Manage Tasks</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/tasks">
                  {" "}
                  <i className="fas fa-clipboard-list"></i>{" "}
                  <span>Manage Tasks</span>
                </Link>{" "}
              </li>
            )}

          {this.props.path_name === "/admin/employees" ? (
            <li className="active">
              {" "}
              <Link to="/admin/employees">
                {" "}
                <i className="fas fa-users"></i> <span>Manage Employees</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/employees">
                  {" "}
                  <i className="fas fa-users"></i> <span>Manage Employees</span>
                </Link>{" "}
              </li>
            )}

          <li className={rotate == "1" ? "treeview active" : "treeview"}>
            <Link to="#" data-id="1" onClick={this.handleClick}>
              <i
                className="fas fa-user sub-menu"
                data-id="1"
                onClick={this.handleClick}
              ></i>{" "}
              <span data-id="1" onClick={this.handleClick}>
                Customers{" "}
              </span>
              <span className="pull-right-container">
                <i
                  data-id="1"
                  onClick={this.handleClick}
                  className={
                    rotate == "1"
                      ? "fa pull-right fa-minus"
                      : "fa pull-right fa-plus"
                  }
                ></i>
              </span>
            </Link>

            <ul className="treeview-menu">
              {this.props.path_name === "/admin/company" ? (
                <li className="active">
                  {" "}
                  <Link to="/admin/company">
                    {" "}
                    <i className="fa fa-life-ring"></i>{" "}
                    <span>Manage Customers</span>
                  </Link>{" "}
                </li>
              ) : (
                  <li>
                    {" "}
                    <Link to="/admin/company">
                      {" "}
                      <i className="fa fa-life-ring"></i>{" "}
                      <span>Manage Customers</span>
                    </Link>{" "}
                  </li>
                )}
              {this.props.path_name === "/admin/customers" ? (
                <li className="active">
                  {" "}
                  <Link to="/admin/customers">
                    {" "}
                    <i className="fas fa-user-tie"></i>{" "}
                    <span>Manage Users</span>
                  </Link>{" "}
                </li>
              ) : (
                  <li>
                    {" "}
                    <Link to="/admin/customers">
                      {" "}
                      <i className="fas fa-user-tie"></i>{" "}
                      <span>Manage Users</span>
                    </Link>{" "}
                  </li>
                )}
              {this.props.path_name === "/admin/master_company" ? (
                <li className="active">
                  {" "}
                  <Link to="/admin/master_company">
                    {" "}
                    <i className="fa fa-deaf"></i>{" "}
                    <span>Manage Parent Customers</span>
                  </Link>{" "}
                </li>
              ) : (
                  <li>
                    {" "}
                    <Link to="/admin/master_company">
                      {" "}
                      <i className="fa fa-deaf"></i>{" "}
                      <span>Manage Parent Customers</span>
                    </Link>{" "}
                  </li>
                )}
            </ul>
          </li>

          {this.props.path_name === "/admin/agent" ? (
            <li className="active">
              {" "}
              <Link to="/admin/agent">
                {" "}
                <i className="fas fa-hand-pointer"></i>{" "}
                <span>Manage Agents</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/agent">
                  {" "}
                  <i className="fas fa-hand-pointer"></i>{" "}
                  <span>Manage Agents</span>
                </Link>{" "}
              </li>
            )}

          <li className={rotate == "2" ? "treeview active" : "treeview"}>
            <Link to="#" data-id="2" onClick={this.handleClick}>
              <i
                className="fas fa-chart-line sub-menu"
                data-id="2"
                onClick={this.handleClick}
              ></i>{" "}
              <span data-id="2" onClick={this.handleClick}>
                Manage SLA{" "}
              </span>
              <span className="pull-right-container">
                <i
                  data-id="2"
                  onClick={this.handleClick}
                  className={
                    rotate == "2"
                      ? "fa pull-right fa-minus"
                      : "fa pull-right fa-plus"
                  }
                ></i>
              </span>
            </Link>

            <ul className="treeview-menu">
              {this.props.path_name === "/admin/sla" ? (
                <li className="active">
                  {" "}
                  <Link to="/admin/sla">
                    {" "}
                    <i className="far fa-chart-bar"></i>{" "}
                    <span>&nbsp; SLA Type</span>
                  </Link>{" "}
                </li>
              ) : (
                  <li>
                    {" "}
                    <Link to="/admin/sla">
                      {" "}
                      <i className="far fa-chart-bar"></i>{" "}
                      <span>&nbsp; SLA Type</span>
                    </Link>{" "}
                  </li>
                )}

              {this.props.path_name === "/admin/notificationsla" ? (
                <li className="active">
                  {" "}
                  <Link to="/admin/notificationsla">
                    {" "}
                    <i className="fas fa-bell"></i>{" "}
                    <span>SLA Notification Type</span>
                  </Link>{" "}
                </li>
              ) : (
                  <li>
                    {" "}
                    <Link to="/admin/notificationsla">
                      {" "}
                      <i className="fas fa-bell"></i>{" "}
                      <span>SLA Notification Type</span>
                    </Link>{" "}
                  </li>
                )}
            </ul>
          </li>

          <li className={rotate == "3" ? "treeview active" : "treeview"}>
            <Link to="#" data-id="3" onClick={this.handleClick}>
              <i
                className="fas fa-clipboard-list sub-menu"
                data-id="3"
                onClick={this.handleClick}
              ></i>{" "}
              <span data-id="3" onClick={this.handleClick}>
                Manage <br />
                Performance Invoice{" "}
              </span>
              <span className="pull-right-container">
                <i
                  data-id="3"
                  onClick={this.handleClick}
                  className={
                    rotate == "3"
                      ? "fa pull-right fa-minus"
                      : "fa pull-right fa-plus"
                  }
                ></i>
              </span>
            </Link>

            <ul className="treeview-menu">
              {this.props.path_name === "/admin/payment_terms" ? (
                <li className="active">
                  {" "}
                  <Link to="/admin/payment_terms">
                    {" "}
                    <i className="fas fa-money-bill"></i>{" "}
                    <span>Payment Terms</span>
                  </Link>{" "}
                </li>
              ) : (
                  <li>
                    {" "}
                    <Link to="/admin/payment_terms">
                      {" "}
                      <i className="fas fa-money-bill"></i>{" "}
                      <span>Payment Terms</span>
                    </Link>{" "}
                  </li>
                )}

              {this.props.path_name === "/admin/delivery_terms" ? (
                <li className="active">
                  {" "}
                  <Link to="/admin/delivery_terms">
                    {" "}
                    <i className="fas fa-truck"></i> <span>Delivery Terms</span>
                  </Link>{" "}
                </li>
              ) : (
                  <li>
                    {" "}
                    <Link to="/admin/delivery_terms">
                      {" "}
                      <i className="fas fa-truck"></i> <span>Delivery Terms</span>
                    </Link>{" "}
                  </li>
                )}
            </ul>
          </li>

          {this.props.path_name === "/admin/products" ? (
            <li className="active">
              {" "}
              <Link to="/admin/products">
                {" "}
                <i className="fab fa-product-hunt"></i>{" "}
                <span>&nbsp; Product Master</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/products">
                  {" "}
                  <i className="fab fa-product-hunt"></i>{" "}
                  <span>&nbsp; Product Master</span>
                </Link>{" "}
              </li>
            )}

          {this.props.path_name === "/admin/region" ? (
            <li className="active">
              {" "}
              <Link to="/admin/region">
                {" "}
                <i className="fab fa-blackberry"></i>{" "}
                <span>&nbsp; Manage Regions</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/region">
                  {" "}
                  <i className="fab fa-blackberry"></i>{" "}
                  <span>&nbsp; Manage Regions</span>
                </Link>{" "}
              </li>
            )}
            {this.props.path_name === "/admin/pharmacopeial" ? (
            <li className="active">
              {" "}
              <Link to="/admin/pharmacopeial">
                {" "}
                <i className="fab fa-blackberry"></i>{" "}
                <span>&nbsp; Manage pharmacopeial</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/pharmacopeial">
                  {" "}
                  <i className="fab fa-blackberry"></i>{" "}
                  <span>&nbsp; Manage pharmacopeial</span>
                </Link>{" "}
              </li>
            )}
            {/* {this.props.path_name === "/admin/country" ? (
            <li className="active">
              {" "}
              <Link to="/admin/country">
                {" "}
                <i className="fab fa-blackberry"></i>{" "}
                <span>&nbsp; Manage Countries</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/country">
                  {" "}
                  <i className="fab fa-blackberry"></i>{" "}
                  <span>&nbsp; Manage Countries</span>
                </Link>{" "}
              </li>
            )} */}
            {this.props.path_name === "/admin/master_country" ? (
            <li className="active">
              {" "}
              <Link to="/admin/master_country">
                {" "}
                <i className="fab fa-blackberry"></i>{" "}
                <span>&nbsp; Manage Master Countries</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/master_country">
                  {" "}
                  <i className="fab fa-blackberry"></i>{" "}
                  <span>&nbsp; Manage Master Countries</span>
                </Link>{" "}
              </li>
            )}


          {/* {this.props.path_name === "/admin/region" ? <li className="active"> <Link to="/admin/region"> <i className="fab fa-blackberry"></i>  <span>&nbsp; Manage Regions</span></Link> </li> : <li> <Link to="/admin/region"> <i className="fab fa-blackberry"></i> <span>&nbsp; Manage Regions</span></Link> </li>
          } */}

          {this.props.path_name === "/admin/reports" ? (
            <li className="active">
              {" "}
              <Link to="/admin/reports">
                {" "}
                <i className="fas fa-tachometer-alt"></i> <span>Reports</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/reports">
                  {" "}
                  <i className="fas fa-tachometer-alt"></i> <span>Reports</span>
                </Link>{" "}
              </li>
            )}
          
          {this.props.path_name === "/admin/task_coa" ? (
            <li className="active">
              {" "}
              <Link to="/admin/task_coa">
                {" "}
                <i className="fas fa-list"></i> <span>Task COA</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/task_coa">
                  {" "}
                  <i className="fas fa-list"></i> <span>Task COA</span>
                </Link>{" "}
              </li>
          )}

          {/* {this.props.path_name === "/admin/task_ratings" ? (
            <li className="active">
              {" "}
              <Link to="/admin/task_ratings">
                {" "}
                <i className="fab fa-blackberry"></i>{" "}
                <span>&nbsp; Task Ratings</span>
              </Link>{" "}
            </li>
          ) : (
            <li>
              {" "}
              <Link to="/admin/task_ratings">
                {" "}
                <i className="fab fa-blackberry"></i>{" "}
                <span>&nbsp; Task Ratings</span>
              </Link>{" "}
            </li>
          )}

          {this.props.path_name === "/admin/task_rating_options" ? (
            <li className="active">
              {" "}
              <Link to="/admin/task_rating_options">
                {" "}
                <i className="fab fa-blackberry"></i>{" "}
                <span>&nbsp; Task Rating Options</span>
              </Link>{" "}
            </li>
          ) : (
            <li>
              {" "}
              <Link to="/admin/task_rating_options">
                {" "}
                <i className="fab fa-blackberry"></i>{" "}
                <span>&nbsp; Task Rating Options</span>
              </Link>{" "}
            </li>
          )} */}

          <li className={rotate == "6" ? "treeview active" : "treeview"}>
            <Link to="#" data-id="6" onClick={this.handleClick}>
              <i
                className="fas fa-clipboard-list sub-menu"
                data-id="6"
                onClick={this.handleClick}
              ></i>{" "}
              <span data-id="6" onClick={this.handleClick}>
                Task Rating{" "}
              </span>
              <span className="pull-right-container">
                <i
                  data-id="6"
                  onClick={this.handleClick}
                  className={
                    rotate == "6"
                      ? "fa pull-right fa-minus"
                      : "fa pull-right fa-plus"
                  }
                ></i>
              </span>
            </Link>

            <ul className="treeview-menu">
              {this.props.path_name === "/admin/task_ratings" ? (
                <li className="active">
                  {" "}
                  <Link to="/admin/task_ratings">
                    {" "}
                    <i className="fas fa-money-bill"></i>{" "}
                    <span>Task Ratings</span>
                  </Link>{" "}
                </li>
              ) : (
                  <li>
                    {" "}
                    <Link to="/admin/task_ratings">
                      {" "}
                      <i className="fas fa-money-bill"></i>{" "}
                      <span>Task Ratings</span>
                    </Link>{" "}
                  </li>
                )}

              {this.props.path_name === "/admin/task_rating_questions" ? (
                <li className="active">
                  {" "}
                  <Link to="/admin/task_rating_questions">
                    {" "}
                    <i className="fa fa-deaf"></i>{" "}
                    <span>Task Rating Questions</span>
                  </Link>{" "}
                </li>
              ) : (
                  <li>
                    {" "}
                    <Link to="/admin/task_rating_questions">
                      {" "}
                      <i className="fa fa-deaf"></i>{" "}
                      <span>Task Rating Questions</span>
                    </Link>{" "}
                  </li>
                )}
              {this.props.path_name === "/admin/task_rating_options" ? (
                <li className="active">
                  {" "}
                  <Link to="/admin/task_rating_options">
                    {" "}
                    <i className="fas fa-truck"></i>{" "}
                    <span>Task Rating Options</span>
                  </Link>{" "}
                </li>
              ) : (
                  <li>
                    {" "}
                    <Link to="/admin/task_rating_options">
                      {" "}
                      <i className="fas fa-truck"></i>{" "}
                      <span>Task Rating Options</span>
                    </Link>{" "}
                  </li>
                )}
            </ul>
          </li>

          <li className={rotate == "7" ? "treeview active" : "treeview"}>
            <Link to="#" data-id="7" onClick={this.handleClick}>
              <i
                className="fas fa-clipboard-list sub-menu"
                data-id="7"
                onClick={this.handleClick}
              ></i>{" "}
              <span data-id="7" onClick={this.handleClick}>
                FA {" "}
              </span>
              <span className="pull-right-container">
                <i
                  data-id="7"
                  onClick={this.handleClick}
                  className={
                    rotate == "7"
                      ? "fa pull-right fa-minus"
                      : "fa pull-right fa-plus"
                  }
                ></i>
              </span>
            </Link>

            <ul className="treeview-menu">
              {this.props.path_name === "/admin/fa_ratings" ? (
                <li className="active">
                  {" "}
                  <Link to="/admin/fa_ratings">
                    {" "}
                    <i className="fas fa-money-bill"></i>{" "}
                    <span>FA Ratings</span>
                  </Link>{" "}
                </li>
              ) : (
                  <li>
                    {" "}
                    <Link to="/admin/fa_ratings">
                      {" "}
                      <i className="fas fa-money-bill"></i>{" "}
                      <span>FA Ratings</span>
                    </Link>{" "}
                  </li>
                )}

              {this.props.path_name === "/admin/fa_rating_questions" ? (
                <li className="active">
                  {" "}
                  <Link to="/admin/fa_rating_questions">
                    {" "}
                    <i className="fa fa-deaf"></i>{" "}
                    <span>FA Rating Questions</span>
                  </Link>{" "}
                </li>
              ) : (
                  <li>
                    {" "}
                    <Link to="/admin/fa_rating_questions">
                      {" "}
                      <i className="fa fa-deaf"></i>{" "}
                      <span>FA Rating Questions</span>
                    </Link>{" "}
                  </li>
                )}
              {this.props.path_name === "/admin/fa_rating_options" ? (
                <li className="active">
                  {" "}
                  <Link to="/admin/fa_rating_options">
                    {" "}
                    <i className="fas fa-truck"></i>{" "}
                    <span>FA Rating Options</span>
                  </Link>{" "}
                </li>
              ) : (
                  <li>
                    {" "}
                    <Link to="/admin/fa_rating_options">
                      {" "}
                      <i className="fas fa-truck"></i>{" "}
                      <span>FA Rating Options</span>
                    </Link>{" "}
                  </li>
                )}
              {this.props.path_name === "/admin/product_enquiries" ? (
                <li className="active">
                  {" "}
                  <Link to="/admin/product_enquiries">
                    {" "}
                    <i className="fas fa-truck"></i>{" "}
                    <span>Product Enquiries</span>
                  </Link>{" "}
                </li>
              ) : (
                  <li>
                    {" "}
                    <Link to="/admin/product_enquiries">
                      {" "}
                      <i className="fas fa-truck"></i>{" "}
                      <span>Product Enquiries</span>
                    </Link>{" "}
                  </li>
                )}
              {this.props.path_name === "/admin/product_search" ? (
                <li className="active">
                  {" "}
                  <Link to="/admin/product_search">
                    {" "}
                    <i className="fas fa-truck"></i>{" "}
                    <span>Product Search</span>
                  </Link>{" "}
                </li>
              ) : (
                  <li>
                    {" "}
                    <Link to="/admin/product_search">
                      {" "}
                      <i className="fas fa-truck"></i>{" "}
                      <span>Product Search</span>
                    </Link>{" "}
                  </li>
                )}
            </ul>
          </li>

          {this.props.path_name === "/admin/admin_list" ? (
            <li className="active">
              {" "}
              <Link to="/admin/admin_list">
                {" "}
                <i className="fas fa-list-ul"></i> <span>Admin Users</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/admin_list">
                  {" "}
                  <i className="fas fa-list-ul"></i> <span>Admin Users</span>
                </Link>{" "}
              </li>
            )}

          {this.props.path_name === "/admin/group" ? (
            <li className="active">
              {" "}
              <Link to="/admin/group">
                {" "}
                <i className="fas fa-list-ul"></i> <span>Group</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/group">
                  {" "}
                  <i className="fas fa-list-ul"></i> <span>Group</span>
                </Link>{" "}
              </li>
            )}

          {this.props.path_name === "/admin/email" ? (
            <li className="active">
              {" "}
              <Link to="/admin/email">
                {" "}
                <i className="fab fa-blackberry"></i>{" "}
                <span>&nbsp; Manage Emails</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/email">
                  {" "}
                  <i className="fab fa-blackberry"></i>{" "}
                  <span>&nbsp; Manage Emails</span>
                </Link>{" "}
              </li>
            )}

          {this.props.path_name === "/admin/email_group" ? (
            <li className="active">
              {" "}
              <Link to="/admin/email_group">
                {" "}
                <i className="fab fa-blackberry"></i>{" "}
                <span>&nbsp; Emails Group</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/email_group">
                  {" "}
                  <i className="fab fa-blackberry"></i>{" "}
                  <span>&nbsp; Emails Group</span>
                </Link>{" "}
              </li>
            )}

          {this.props.path_name === "/admin/email_log" ? (
            <li className="active">
              {" "}
              <Link to="/admin/email_log">
                {" "}
                <i className="fab fa-blackberry"></i>{" "}
                <span>&nbsp; Email Log</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/email_log">
                  {" "}
                  <i className="fab fa-blackberry"></i>{" "}
                  <span>&nbsp; Email Log</span>
                </Link>{" "}
              </li>
            )}

          {this.props.path_name === "/admin/txt_suggestion" ? (
            <li className="active">
              {" "}
              <Link to="/admin/txt_suggestion">
                {" "}
                <i className="fab fa-blackberry"></i>{" "}
                <span>&nbsp; Tags Management</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/txt_suggestion">
                  {" "}
                  <i className="fab fa-blackberry"></i>{" "}
                  <span>&nbsp; Tags Management</span>
                </Link>{" "}
              </li>
            )}

          {this.props.path_name === "/admin/customer_escalation" ? (
            <li className="active">
              {" "}
              <Link to="/admin/customer_escalation">
                {" "}
                <i className="fab fa-blackberry"></i>{" "}
                <span>&nbsp; Customer Escalation</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/customer_escalation">
                  {" "}
                  <i className="fab fa-blackberry"></i>{" "}
                  <span>&nbsp; Customer Escalation</span>
                </Link>{" "}
              </li>
            )}


          {this.props.path_name === "/admin/commercial_checklist" ? (
            <li className="active">
              {" "}
              <Link to="/admin/commercial_checklist">
                {" "}
                <i className="fas fa-pause-circle"></i>{" "}
                <span>Commercial Checklist</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/commercial_checklist">
                  {" "}
                  <i className="fas fa-pause-circle"></i>{" "}
                  <span>Commercial Checklist</span>
                </Link>{" "}
              </li>
            )}

          <li className={rotate == "4" ? "treeview active" : "treeview"}>
            <Link to="#" data-id="4" onClick={this.handleClick}>
              <i
                className="fas fa-calendar-alt sub-menu"
                data-id="4"
                onClick={this.handleClick}
              ></i>{" "}
              <span data-id="4" onClick={this.handleClick}>
                Event{" "}
              </span>
              <span className="pull-right-container">
                <i
                  data-id="4"
                  onClick={this.handleClick}
                  className={
                    rotate == "4"
                      ? "fa pull-right fa-minus"
                      : "fa pull-right fa-plus"
                  }
                ></i>
              </span>
            </Link>

            <ul className="treeview-menu">
              {this.props.path_name === "/admin/events" ? (
                <li className="active">
                  {" "}
                  <Link to="/admin/events">
                    {" "}
                    <i className="fas fa-pause-circle"></i>{" "}
                    <span>Event Logs</span>
                  </Link>{" "}
                </li>
              ) : (
                  <li>
                    {" "}
                    <Link to="/admin/events">
                      {" "}
                      <i className="fas fa-pause-circle"></i>{" "}
                      <span>Event Logs</span>
                    </Link>{" "}
                  </li>
                )}

              {this.props.path_name === "/admin/increased_sla" ? (
                <li className="active">
                  {" "}
                  <Link to="/admin/increased_sla">
                    {" "}
                    <i className="fas fa-chart-line"></i>{" "}
                    <span>Increased SLA Logs</span>
                  </Link>{" "}
                </li>
              ) : (
                  <li>
                    {" "}
                    <Link to="/admin/increased_sla">
                      {" "}
                      <i className="fas fa-chart-line"></i>{" "}
                      <span>Increased SLA Logs</span>
                    </Link>{" "}
                  </li>
                )}

              {this.props.path_name === "/admin/paused_sla" ? (
                <li className="active">
                  {" "}
                  <Link to="/admin/paused_sla">
                    {" "}
                    <i className="fas fa-pause-circle"></i>{" "}
                    <span>Paused SLA Logs</span>
                  </Link>{" "}
                </li>
              ) : (
                  <li>
                    {" "}
                    <Link to="/admin/paused_sla">
                      {" "}
                      <i className="fas fa-pause-circle"></i>{" "}
                      <span>Paused SLA Logs</span>
                    </Link>{" "}
                  </li>
                )}
            </ul>
          </li>

          {/* {this.props.path_name === "/admin/error_log" ? <li className="active"> <Link to="/admin/error_log"> <i className="fas fa-pause-circle"></i> <span>Error Log</span></Link> </li> : <li> <Link to="/admin/error_log"> <i className="fas fa-pause-circle"></i> <span>Error Log</span></Link> </li>} */}

          <li className={rotate == "5" ? "treeview active" : "treeview"}>
            <Link to="#" data-id="5" onClick={this.handleClick}>
              <i
                className="fas fa-calendar-alt sub-menu"
                data-id="5"
                onClick={this.handleClick}
              ></i>{" "}
              <span data-id="5" onClick={this.handleClick}>
                Errors{" "}
              </span>
              <span className="pull-right-container">
                <i
                  data-id="5"
                  onClick={this.handleClick}
                  className={
                    rotate == "5"
                      ? "fa pull-right fa-minus"
                      : "fa pull-right fa-plus"
                  }
                ></i>
              </span>
            </Link>

            <ul className="treeview-menu">
              {this.props.path_name === "/admin/error_log" ? (
                <li className="active">
                  {" "}
                  <Link to="/admin/error_log">
                    {" "}
                    <i className="fas fa-pause-circle"></i>{" "}
                    <span>Site Error</span>
                  </Link>{" "}
                </li>
              ) : (
                  <li>
                    {" "}
                    <Link to="/admin/error_log">
                      {" "}
                      <i className="fas fa-pause-circle"></i>{" "}
                      <span>Site Error</span>
                    </Link>{" "}
                  </li>
                )}

              {this.props.path_name === "/admin/crm_error" ? (
                <li className="active">
                  {" "}
                  <Link to="/admin/crm_error">
                    {" "}
                    <i className="fas fa-chart-line"></i> <span>Crm Error</span>
                  </Link>{" "}
                </li>
              ) : (
                  <li>
                    {" "}
                    <Link to="/admin/crm_error">
                      {" "}
                      <i className="fas fa-chart-line"></i> <span>Crm Error</span>
                    </Link>{" "}
                  </li>
                )}
            </ul>
          </li>

          {/* <li className={rotate=='1' ? 'treeview active' : 'treeview'}> 
            <Link to="#" data-id="1" onClick={this.handleClick}> 
              <i className="fas fa-clipboard-list sub-menu" data-id="1" onClick={this.handleClick}></i> <span data-id="1" onClick={this.handleClick}>Task</span>
              <span className="pull-right-container"><i data-id="1" onClick={this.handleClick} className={rotate=='1' ? 'fa pull-right fa-minus' : 'fa pull-right fa-plus'}></i></span>
            </Link>
                                          
            <ul className="treeview-menu">
              <li><Link to={'/admin/assigned'}><i className="fas fa-edit"></i> Assigned</Link></li>
              <li><Link to={'/admin/in_progress'}><i className="fas fa-hourglass-end"></i> In progress</Link></li>
              <li><Link to={'/admin/closed'}><i className="fas fa-times"></i> Closed</Link></li>
            </ul>                       
              
          </li> */}

          {/* <li> <Link to="/admin/update-password"> <i className="fas fa-key"></i> <span>Update Password</span></Link>
          </li> */}
        </ul>
      </section>
    );
  };

  getPermittedMenu = () => {

    const rotate = this.state.shown;
    let li_html = [];

    let customer_block = [];
    let is_customer_block = 0;

    let sla_block = [];
    let is_sla_block = 0;

    let invoice_block = [];
    let is_invoice_block = 0;

    let event_block = [];
    let is_event_block = 0;

    let err_log_block = [];
    let is_err_log_block = 0;

    let task_rating_block = [];
    let is_task_rating_block = 0;

    for (let index = 0; index < this.state.access_data.length; index++) {
      const element = this.state.access_data[index];

      let indexOF = element.checkBoxes.findIndex((person) => {
        return person.key == "V";
      });

      if (
        element.section_code === "TASK_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        li_html.push(
          this.props.path_name === "/admin/tasks" ? (
            <li className="active">
              {" "}
              <Link to="/admin/tasks">
                {" "}
                <i className="fas fa-clipboard-list"></i>{" "}
                <span>Manage Tasks</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/tasks">
                  {" "}
                  <i className="fas fa-clipboard-list"></i>{" "}
                  <span>Manage Tasks</span>
                </Link>{" "}
              </li>
            )
        );
      }

      if (
        element.section_code === "EMPLOYEES_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        li_html.push(
          this.props.path_name === "/admin/employees" ? (
            <li className="active">
              {" "}
              <Link to="/admin/employees">
                {" "}
                <i className="fas fa-users"></i> <span>Manage Employees</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/employees">
                  {" "}
                  <i className="fas fa-users"></i> <span>Manage Employees</span>
                </Link>{" "}
              </li>
            )
        );
      }

      if (
        element.section_code === "CUSTOMER_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        customer_block.push(
          this.props.path_name === "/admin/company" ? (
            <li className="active">
              {" "}
              <Link to="/admin/company">
                {" "}
                <i className="fa fa-life-ring"></i>{" "}
                <span>Manage Customers</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/company">
                  {" "}
                  <i className="fa fa-life-ring"></i>{" "}
                  <span>Manage Customers</span>
                </Link>{" "}
              </li>
            )
        );
        is_customer_block++;
      }

      if (
        element.section_code === "USERS_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        customer_block.push(
          this.props.path_name === "/admin/customers" ? (
            <li className="active">
              {" "}
              <Link to="/admin/customers">
                {" "}
                <i className="fas fa-user-tie"></i> <span>Manage Users</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/customers">
                  {" "}
                  <i className="fas fa-user-tie"></i> <span>Manage Users</span>
                </Link>{" "}
              </li>
            )
        );
        is_customer_block++;
      }

      if (
        element.section_code === "PARENT_CUSTOMER_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        customer_block.push(
          this.props.path_name === "/admin/master_company" ? (
            <li className="active">
              {" "}
              <Link to="/admin/master_company">
                {" "}
                <i className="fa fa-deaf"></i>{" "}
                <span>Manage Parent Customers</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/master_company">
                  {" "}
                  <i className="fa fa-deaf"></i>{" "}
                  <span>Manage Parent Customers</span>
                </Link>{" "}
              </li>
            )
        );
        is_customer_block++;
      }

      if (
        element.section_code === "AGENT_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        li_html.push(
          this.props.path_name === "/admin/agent" ? (
            <li className="active">
              {" "}
              <Link to="/admin/agent">
                {" "}
                <i className="fas fa-hand-pointer"></i>{" "}
                <span>Manage Agents</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/agent">
                  {" "}
                  <i className="fas fa-hand-pointer"></i>{" "}
                  <span>Manage Agents</span>
                </Link>{" "}
              </li>
            )
        );
      }

      if (
        element.section_code === "SLA_TYPE_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        sla_block.push(
          this.props.path_name === "/admin/sla" ? (
            <li className="active">
              {" "}
              <Link to="/admin/sla">
                {" "}
                <i className="far fa-chart-bar"></i>{" "}
                <span>&nbsp; SLA Type</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/sla">
                  {" "}
                  <i className="far fa-chart-bar"></i>{" "}
                  <span>&nbsp; SLA Type</span>
                </Link>{" "}
              </li>
            )
        );
        is_sla_block++;
      }

      if (
        element.section_code === "SLA_NOTIFICATION_TYPE_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        sla_block.push(
          this.props.path_name === "/admin/notificationsla" ? (
            <li className="active">
              {" "}
              <Link to="/admin/notificationsla">
                {" "}
                <i className="fas fa-bell"></i>{" "}
                <span>SLA Notification Type</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/notificationsla">
                  {" "}
                  <i className="fas fa-bell"></i>{" "}
                  <span>SLA Notification Type</span>
                </Link>{" "}
              </li>
            )
        );
        is_sla_block++;
      }

      if (
        element.section_code === "PAYMENT_TERMS_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        invoice_block.push(
          this.props.path_name === "/admin/payment_terms" ? (
            <li className="active">
              {" "}
              <Link to="/admin/payment_terms">
                {" "}
                <i className="fas fa-money-bill"></i> <span>Payment Terms</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/payment_terms">
                  {" "}
                  <i className="fas fa-money-bill"></i> <span>Payment Terms</span>
                </Link>{" "}
              </li>
            )
        );
        is_invoice_block++;
      }

      if (
        element.section_code === "DELIVERY_TERMS_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        invoice_block.push(
          this.props.path_name === "/admin/delivery_terms" ? (
            <li className="active">
              {" "}
              <Link to="/admin/delivery_terms">
                {" "}
                <i className="fas fa-truck"></i> <span>Delivery Terms</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/delivery_terms">
                  {" "}
                  <i className="fas fa-truck"></i> <span>Delivery Terms</span>
                </Link>{" "}
              </li>
            )
        );
        is_invoice_block++;
      }

      if (
        element.section_code === "PRODUCT_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        li_html.push(
          this.props.path_name === "/admin/products" ? (
            <li className="active">
              {" "}
              <Link to="/admin/products">
                {" "}
                <i className="fab fa-product-hunt"></i>{" "}
                <span>&nbsp; Product Master</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/products">
                  {" "}
                  <i className="fab fa-product-hunt"></i>{" "}
                  <span>&nbsp; Product Master</span>
                </Link>{" "}
              </li>
            )
        );
      }

      if (
        element.section_code === "REGION_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        li_html.push(
          this.props.path_name === "/admin/region" ? (
            <li className="active">
              {" "}
              <Link to="/admin/region">
                {" "}
                <i className="fab fa-blackberry"></i>{" "}
                <span>&nbsp; Manage Regions</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/region">
                  {" "}
                  <i className="fab fa-blackberry"></i>{" "}
                  <span>&nbsp; Manage Regions</span>
                </Link>{" "}
              </li>
            )
        );
      }

      if (
        element.section_code === "PHARMACOPEIAL_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        li_html.push(
          this.props.path_name === "/admin/pharmacopeial" ? (
            <li className="active">
              {" "}
              <Link to="/admin/pharmacopeial">
                {" "}
                <i className="fab fa-blackberry"></i>{" "}
                <span>&nbsp; Manage pharmacopeial</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/pharmacopeial">
                  {" "}
                  <i className="fab fa-blackberry"></i>{" "}
                  <span>&nbsp; Manage pharmacopeial</span>
                </Link>{" "}
              </li>
            )
        );
      }

      // if (
      //   element.section_code === "COUNTRY_MANAGEMENT" &&
      //   element.checkBoxes[indexOF].value === true
      // ) {
      //   li_html.push(
      //     this.props.path_name === "/admin/country" ? (
      //       <li className="active">
      //         {" "}
      //         <Link to="/admin/country">
      //           {" "}
      //           <i className="fab fa-blackberry"></i>{" "}
      //           <span>&nbsp; Manage Countries</span>
      //         </Link>{" "}
      //       </li>
      //     ) : (
      //         <li>
      //           {" "}
      //           <Link to="/admin/country">
      //             {" "}
      //             <i className="fab fa-blackberry"></i>{" "}
      //             <span>&nbsp; Manage Countries</span>
      //           </Link>{" "}
      //         </li>
      //       )
      //   );
      // }
      if (
        element.section_code === "MASTER_COUNTRY_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        li_html.push(
          this.props.path_name === "/admin/master_country" ? (
            <li className="active">
              {" "}
              <Link to="/admin/master_country">
                {" "}
                <i className="fab fa-blackberry"></i>{" "}
                <span>&nbsp; Manage Master Countries</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/master_country">
                  {" "}
                  <i className="fab fa-blackberry"></i>{" "}
                  <span>&nbsp; Manage Master Countries</span>
                </Link>{" "}
              </li>
            )
        );
      }


      if (
        element.section_code === "REPORTS_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        li_html.push(
          this.props.path_name === "/admin/reports" ? (
            <li className="active">
              {" "}
              <Link to="/admin/reports">
                {" "}
                <i className="fas fa-tachometer-alt"></i> <span>Reports</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/reports">
                  {" "}
                  <i className="fas fa-tachometer-alt"></i> <span>Reports</span>
                </Link>{" "}
              </li>
            )
        );
      }

      if (
        element.section_code === "EVENT_LOGS_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        event_block.push(
          this.props.path_name === "/admin/events" ? (
            <li className="active">
              {" "}
              <Link to="/admin/events">
                {" "}
                <i className="fas fa-pause-circle"></i> <span>Event Logs</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/events">
                  {" "}
                  <i className="fas fa-pause-circle"></i> <span>Event Logs</span>
                </Link>{" "}
              </li>
            )
        );
        is_event_block++;
      }
      if (
        element.section_code === "INCREASED_SLA_LOGS_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        event_block.push(
          this.props.path_name === "/admin/increased_sla" ? (
            <li className="active">
              {" "}
              <Link to="/admin/increased_sla">
                {" "}
                <i className="fas fa-chart-line"></i>{" "}
                <span>Increased SLA Logs</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/increased_sla">
                  {" "}
                  <i className="fas fa-chart-line"></i>{" "}
                  <span>Increased SLA Logs</span>
                </Link>{" "}
              </li>
            )
        );
        is_event_block++;
      }
      if (
        element.section_code === "PAUSED_SLA_LOGS_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        event_block.push(
          this.props.path_name === "/admin/paused_sla" ? (
            <li className="active">
              {" "}
              <Link to="/admin/paused_sla">
                {" "}
                <i className="fas fa-pause-circle"></i>{" "}
                <span>Paused SLA Logs</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/paused_sla">
                  {" "}
                  <i className="fas fa-pause-circle"></i>{" "}
                  <span>Paused SLA Logs</span>
                </Link>{" "}
              </li>
            )
        );
        is_event_block++;
      }

      if (
        element.section_code === "ERRLOG_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        err_log_block.push(
          this.props.path_name === "/admin/error_log" ? (
            <li className="active">
              {" "}
              <Link to="/admin/error_log">
                {" "}
                <i className="fas fa-pause-circle"></i> <span>Site Error</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/error_log">
                  {" "}
                  <i className="fas fa-pause-circle"></i> <span>Site Error</span>
                </Link>{" "}
              </li>
            )
        );
        is_err_log_block++;
      }

      if (
        element.section_code === "CRM_ERRLOG_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        err_log_block.push(
          this.props.path_name === "/admin/crm_error" ? (
            <li className="active">
              {" "}
              <Link to="/admin/crm_error">
                {" "}
                <i className="fas fa-chart-line"></i> <span>Crm Error</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/crm_error">
                  {" "}
                  <i className="fas fa-chart-line"></i> <span>Crm Error</span>
                </Link>{" "}
              </li>
            )
        );
        is_err_log_block++;
      }

      if (
        element.section_code === "EMAILS_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        li_html.push(
          this.props.path_name === "/admin/email" ? (
            <li className="active">
              {" "}
              <Link to="/admin/email">
                {" "}
                <i className="fab fa-blackberry"></i>{" "}
                <span>&nbsp; Manage Emails</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/email">
                  {" "}
                  <i className="fab fa-blackberry"></i>{" "}
                  <span>&nbsp; Manage Emails</span>
                </Link>{" "}
              </li>
            )
        );
      }

      if (
        element.section_code === "EMAIL_GROUP_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        li_html.push(
          this.props.path_name === "/admin/email_group" ? (
            <li className="active">
              {" "}
              <Link to="/admin/email_group">
                {" "}
                <i className="fab fa-blackberry"></i>{" "}
                <span>&nbsp; Emails Group</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/email_group">
                  {" "}
                  <i className="fab fa-blackberry"></i>{" "}
                  <span>&nbsp; Emails Group</span>
                </Link>{" "}
              </li>
            )
        );
      }

      if (
        element.section_code === "EMAIL_LOG_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        li_html.push(
          this.props.path_name === "/admin/email_log" ? (
            <li className="active">
              {" "}
              <Link to="/admin/email_log">
                {" "}
                <i className="fab fa-blackberry"></i>{" "}
                <span>&nbsp; Email Log</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/email_log">
                  {" "}
                  <i className="fab fa-blackberry"></i>{" "}
                  <span>&nbsp; Email Log</span>
                </Link>{" "}
              </li>
            )
        );
      }

      // li_html.push(
      //   this.props.path_name === "/admin/txt_suggestion" ? (
      //     <li className="active">
      //       {" "}
      //       <Link to="/admin/txt_suggestion">
      //         {" "}
      //         <i className="fab fa-blackberry"></i>{" "}
      //         <span>&nbsp; Tags Management</span>
      //       </Link>{" "}
      //     </li>
      //   ) : (
      //       <li>
      //         {" "}
      //         <Link to="/admin/txt_suggestion">
      //           {" "}
      //           <i className="fab fa-blackberry"></i>{" "}
      //           <span>&nbsp; Tags Management</span>
      //         </Link>{" "}
      //       </li>
      //     )
      // );

      if (
        element.section_code === "COMMERCIAL_CHECKLIST_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        li_html.push(
          this.props.path_name === "/admin/commercial_checklist" ? (
            <li className="active">
              {" "}
              <Link to="/admin/commercial_checklist">
                {" "}
                <i className="fab fa-blackberry"></i>{" "}
                <span>&nbsp; Commercial Checklist</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/commercial_checklist">
                  {" "}
                  <i className="fab fa-blackberry"></i>{" "}
                  <span>&nbsp; Commercial Checklist</span>
                </Link>{" "}
              </li>
            )
        );
      }
      if (
        element.section_code === "TASK_RATING_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        task_rating_block.push(
          this.props.path_name === "/admin/task_ratings" ? (
            <li className="active">
              {" "}
              <Link to="/admin/task_ratings">
                {" "}
                <i className="fas fa-money-bill"></i> <span>Task Ratings</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/task_ratings">
                  {" "}
                  <i className="fas fa-money-bill"></i> <span>Task Ratings</span>
                </Link>{" "}
              </li>
            )
        );
        is_task_rating_block++;
      }
      if (
        element.section_code === "TASK_RATING_QUESTIONS_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        task_rating_block.push(
          this.props.path_name === "/admin/task_rating_questions" ? (
            <li className="active">
              {" "}
              <Link to="/admin/task_rating_questions">
                {" "}
                <i className="fa fa-deaf"></i>{" "}
                <span>Task Rating Questions</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/task_rating_questions">
                  {" "}
                  <i className="fa fa-deaf"></i>{" "}
                  <span>Task Rating Questions</span>
                </Link>{" "}
              </li>
            )
        );
        is_task_rating_block++;
      }
      if (
        element.section_code === "TASK_RATING_OPTIONS_MANAGEMENT" &&
        element.checkBoxes[indexOF].value === true
      ) {
        task_rating_block.push(
          this.props.path_name === "/admin/task_rating_options" ? (
            <li className="active">
              {" "}
              <Link to="/admin/task_rating_options">
                {" "}
                <i className="fas fa-truck"></i>{" "}
                <span>Task Rating Options</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/task_rating_options">
                  {" "}
                  <i className="fas fa-truck"></i>{" "}
                  <span>Task Rating Options</span>
                </Link>{" "}
              </li>
            )
        );
        is_task_rating_block++;
      }
    }

    if (is_customer_block > 0) {
      li_html.push(
        <li className={rotate == "1" ? "treeview active" : "treeview"}>
          <Link to="#" data-id="1" onClick={this.handleClick}>
            <i
              className="fas fa-user sub-menu"
              data-id="1"
              onClick={this.handleClick}
            ></i>{" "}
            <span data-id="1" onClick={this.handleClick}>
              Customers{" "}
            </span>
            <span className="pull-right-container">
              <i
                data-id="1"
                onClick={this.handleClick}
                className={
                  rotate == "1"
                    ? "fa pull-right fa-minus"
                    : "fa pull-right fa-plus"
                }
              ></i>
            </span>
          </Link>

          <ul className="treeview-menu">{customer_block}</ul>
        </li>
      );
    }

    if (is_sla_block > 0) {
      li_html.push(
        <li className={rotate == "2" ? "treeview active" : "treeview"}>
          <Link to="#" data-id="2" onClick={this.handleClick}>
            <i
              className="fas fa-chart-line sub-menu"
              data-id="2"
              onClick={this.handleClick}
            ></i>{" "}
            <span data-id="2" onClick={this.handleClick}>
              Manage SLA{" "}
            </span>
            <span className="pull-right-container">
              <i
                data-id="2"
                onClick={this.handleClick}
                className={
                  rotate == "2"
                    ? "fa pull-right fa-minus"
                    : "fa pull-right fa-plus"
                }
              ></i>
            </span>
          </Link>

          <ul className="treeview-menu">{sla_block}</ul>
        </li>
      );
    }

    if (is_invoice_block > 0) {
      li_html.push(
        <li className={rotate == "3" ? "treeview active" : "treeview"}>
          <Link to="#" data-id="3" onClick={this.handleClick}>
            <i
              className="fas fa-clipboard-list sub-menu"
              data-id="3"
              onClick={this.handleClick}
            ></i>{" "}
            <span data-id="3" onClick={this.handleClick}>
              Manage <br />
              Performance Invoice{" "}
            </span>
            <span className="pull-right-container">
              <i
                data-id="3"
                onClick={this.handleClick}
                className={
                  rotate == "3"
                    ? "fa pull-right fa-minus"
                    : "fa pull-right fa-plus"
                }
              ></i>
            </span>
          </Link>

          <ul className="treeview-menu">{invoice_block}</ul>
        </li>
      );
    }

    if (is_task_rating_block > 0) {
      li_html.push(
        <li className={rotate == "6" ? "treeview active" : "treeview"}>
          <Link to="#" data-id="6" onClick={this.handleClick}>
            <i
              className="fas fa-user sub-menu"
              data-id="6"
              onClick={this.handleClick}
            ></i>{" "}
            <span data-id="6" onClick={this.handleClick}>
              Customers{" "}
            </span>
            <span className="pull-right-container">
              <i
                data-id="6"
                onClick={this.handleClick}
                className={
                  rotate == "6"
                    ? "fa pull-right fa-minus"
                    : "fa pull-right fa-plus"
                }
              ></i>
            </span>
          </Link>

          <ul className="treeview-menu">{task_rating_block}</ul>
        </li>
      );
    }

    if (is_event_block > 0) {
      li_html.push(
        <li className={rotate == "4" ? "treeview active" : "treeview"}>
          <Link to="#" data-id="4" onClick={this.handleClick}>
            <i
              className="fas fa-calendar-alt sub-menu"
              data-id="4"
              onClick={this.handleClick}
            ></i>{" "}
            <span data-id="4" onClick={this.handleClick}>
              Event{" "}
            </span>
            <span className="pull-right-container">
              <i
                data-id="4"
                onClick={this.handleClick}
                className={
                  rotate == "4"
                    ? "fa pull-right fa-minus"
                    : "fa pull-right fa-plus"
                }
              ></i>
            </span>
          </Link>

          <ul className="treeview-menu">{event_block}</ul>
        </li>
      );
    }

    if (is_err_log_block > 0) {
      li_html.push(
        <li className={rotate == "5" ? "treeview active" : "treeview"}>
          <Link to="#" data-id="5" onClick={this.handleClick}>
            <i
              className="fas fa-calendar-alt sub-menu"
              data-id="5"
              onClick={this.handleClick}
            ></i>{" "}
            <span data-id="5" onClick={this.handleClick}>
              Errors{" "}
            </span>
            <span className="pull-right-container">
              <i
                data-id="5"
                onClick={this.handleClick}
                className={
                  rotate == "5"
                    ? "fa pull-right fa-minus"
                    : "fa pull-right fa-plus"
                }
              ></i>
            </span>
          </Link>

          <ul className="treeview-menu">{err_log_block}</ul>
        </li>
      );
    }

    return (
      <section className="sidebar">
        <ul className="sidebar-menu">
          {this.props.path_name === "/admin/dashboard" ? (
            <li className="active">
              {" "}
              <Link to="/admin/dashboard">
                {" "}
                <i className="fas fa-tachometer-alt"></i> <span>Dashboard</span>
              </Link>{" "}
            </li>
          ) : (
              <li>
                {" "}
                <Link to="/admin/dashboard">
                  {" "}
                  <i className="fas fa-tachometer-alt"></i> <span>Dashboard</span>
                </Link>{" "}
              </li>
            )}
          {li_html}
        </ul>
      </section>
    );
  };

  render() {
    if (this.props.isLoggedIn === false) return null;
    return (
      <aside className="main-sidebar">
        {this.state.api_end === true &&
          (this.state.super_admin === 1
            ? this.getSuperAdminMenu()
            : this.getPermittedMenu())}
      </aside>
    );
  }
}

export default SidebarAdmin;
