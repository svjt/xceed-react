import React, { Component } from 'react';
import Pagination from "react-js-pagination";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal
} from "react-bootstrap";
//import { Label } from 'reactstrap';
import { Link } from "react-router-dom";
import { Formik, Field, Form } from "formik";
import Select from "react-select";
import swal from "sweetalert";
import * as Yup from "yup";


import Layout from "../layout/Layout";
import whitelogo from '../../../assets/images/drreddylogo_white.png';
import API from "../../../shared/admin-axios";
import { showErrorMessage } from "../../../shared/handle_error";
import { htmlDecode } from '../../../shared/helper';
import { getSuperAdmin, getAdminGroup } from '../../../shared/helper';

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

const actionFormatter = refObj => cell => {
  //console.log(refObj.state.access)
  return (
     
    <div className="actionStyle">
    {refObj.state.access.edit === true ?
      <LinkWithTooltip
        tooltip="Click to Edit"
        href="#"
        clicked={e => refObj.modalShowHandler(e, cell)}
        id="tooltip-1"
      >
        <i className="far fa-edit" />
      </LinkWithTooltip>
      : null }
      
    </div>
  );
};

const custStatus = () => cell => {
  if(cell === 1){
    return (<div className="text-success">Active</div>)
  }else{
    return (<div className="text-danger">Inactive</div>)
  } 
  
};

const custContent = () => cell => {
  return htmlDecode(cell);
};

const initialValues = {
  country_name: ''
};

class country extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      get_access_data:false,
      activePage: 1,
      country_name : '',
      totalCount: 0,
      itemPerPage: 10,
      countryDetails: [],
      countryflagId: 0,
      showModal: false,
      search_country_name: '',
      search_employee : '',
      remove_search: false,
      showModalLoader: false,
      employee_bm : [],
      employee_cscc : [],
      employee_csct : [],
      employee_ra : [],
      all_employee : []
    };
  }

  componentDidMount() {
    
    const superAdmin  = getSuperAdmin(localStorage.admin_token);
    
    if(superAdmin === 1){
      this.setState({
        access: {
           view : true,
           add : false,
           edit : true,
           delete : false
         },
         get_access_data:true
     });
     this.getcountryList();
    }else{
      const adminGroup  = getAdminGroup(localStorage.admin_token);
      API.get(`/api/adm_group/single_access/${adminGroup}/${'MASTER_COUNTRY_MANAGEMENT'}`)
      .then(res => {
        this.setState({
          access: res.data.data,
          get_access_data:true
        }); 

        if(res.data.data.view === true){
          this.getcountryList();
        }else{
          this.props.history.push('/admin/dashboard');
        }
        
      })
      .catch(err => {
        showErrorMessage(err,this.props);
      });
    } 
    API.get(`/api/employees/list_emp_wrt_desg`)
      .then((res) => {
        let employee_details = res.data.data ;
        let bm_arr = [];
        let cscc_arr = [];
        let csct_arr = [];
        let ra_arr = [] ;
        let all_emp = []
  
        employee_details.map((e)=>{

               all_emp.push({value : e.employee_id , label : `${e.employee_name} (${e.desig_name})` })
              if(e.desig_id == 1){
                bm_arr.push({value : e.employee_id , label : e.employee_name })
              }
              if(e.desig_id == 5){
                cscc_arr.push({value : e.employee_id , label : e.employee_name })
              }
              if(e.desig_id == 2){
                csct_arr.push({value : e.employee_id , label : e.employee_name })
              }
              if(e.desig_id == 3){
                ra_arr.push({value : e.employee_id , label : e.employee_name })
              }
        })
          // console.log("bm arr ",csct_arr);
        
        this.setState({ employee_bm : bm_arr , employee_cscc : cscc_arr ,employee_csct : csct_arr , employee_ra : ra_arr , all_employee : all_emp });
      })
      .catch((err) => {
      });     
  }
  clearSearch = () => {
    document.getElementById('country_name').value = "";
    document.getElementById('employee_id').value = "";

    this.setState({
      search_country_name: "",
      search_employee  : "",
      remove_search: false
    }, () => {
      this.getcountryList();
      this.setState({activePage: 1})
    })
  }
  getcountryList(page = 1) {
    let country_name    = this.state.search_country_name
    let employee_id     =  this.state.search_employee
      API.get(`/api/feed/master_country_list?page=${page}&country_name=${encodeURIComponent(country_name)}&employee_id = ${encodeURIComponent(employee_id)}`)
      .then(res => {
        this.setState({
          country: res.data.data,
          count: res.data.count_country,
          isLoading: false,
          search_country_name: country_name,
          search_employee : employee_id
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        showErrorMessage(err,this.props);
      });
      this.setState({
        isLoading: false
      });
  }
  regSearch = (e,page=1) => {
    e.preventDefault();

    var country_name = document.getElementById('country_name').value;
    var employee_id = document.getElementById('employee_id').value
   

    if(country_name === "" && employee_id === "" ){
      return false;
    }

    API.get(`/api/feed/master_country_list?page=${page}&country_name=${encodeURIComponent(country_name)}&employee_id=${encodeURIComponent(employee_id)}`)
    .then(res => {
      this.setState({
        country: res.data.data,
         count: res.data.count_country,
         isLoading: false,
        search_country_name: country_name,
        search_employee : employee_id,
        remove_search: true,
        activePage : 1
      });
    })
    .catch(err => {
      this.setState({
        isLoading: false
      });
      showErrorMessage(err,this.props);
    });
  }
   handlePageChange = (pageNumber) => {
     //console.log("this is page number : " , pageNumber);
    this.setState({ activePage: pageNumber });
    this.getcountryList(pageNumber > 0 ? pageNumber : 1);
  };  

  modalShowHandler = (event, id) => { 
    if (id) {
      event.preventDefault();
      API.get(`/api/feed/get_master_country/${id}`)
        .then(res => {
          this.setState({
            countryDetails: res.data.data,
            countryflagId: id,
            isLoading: false,
            showModal: true
          });
        })
        .catch(err => {
          showErrorMessage(err, this.props);
        });
    } else {
      this.setState({
        countryDetails: [],
        countryflagId: 0,
        showModal: true
      });
    }
  };

  modalCloseHandler = () => {
    this.setState({ countryflagId: 0 });
    this.setState({ showModal: false });
  };

  handleSubmitEvent = (values, actions) => {

    const toTitleCase = (phrase) => {
      return phrase
        .toLowerCase()
        .split(' ')
        .map(word => word.charAt(0).toUpperCase() + word.slice(1))
        .join(' ');
    };

    //console.log("++++++++++++++++++++++++++++++++++++++++++++++++ ",values.country_name);

    const post_data = {
        emp_bm: values.emp_bm,
        emp_cscc: values.emp_cscc,
        emp_csct: values.emp_csct,
        emp_ra: values.emp_ra
    };

    if (this.state.countryflagId) {
      this.setState({ showModalLoader: true });
      const id = this.state.countryflagId;
      API.put(`/api/feed/update_master_country/${id}`, post_data)
        .then(res => {
          this.modalCloseHandler();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record updated successfully.",
            icon: "success"
          }).then(() => {
            this.setState({ showModalLoader: false });
            this.getcountryList(this.state.activePage);
          });
        })
        .catch(err => {
          this.setState({ showModalLoader: false });
          if (err.data.status === 3) {
            this.setState({
              showModal: false
            });
            showErrorMessage(err, this.props);
          } else {
            actions.setErrors(err.data.errors);
            actions.setSubmitting(false);
          }
        });
    } 

    
  };



  downloadXLSX = (e) => {
    e.preventDefault();

    API.get(`/api/feed/master_country_list/download?page=1`,{responseType: 'blob'})
    .then(res => { 
      let url    = window.URL.createObjectURL(res.data);
      let a      = document.createElement('a');
      a.href     = url;
      a.download = 'Master-country.xlsx';
      a.click();

    }).catch(err => {
      showErrorMessage(err,this.props);
    });
  }

  checkHandler = (event) => {
    //console.log("this is event:::",event.value);
    event.preventDefault();
  };
  

  render() {
    const { countryDetails } = this.state;
    const newInitialValues = Object.assign(initialValues, {
      country_name: countryDetails.country_name ? htmlDecode(countryDetails.country_name) : "", 
      emp_bm :  countryDetails.emp_bm ? countryDetails.emp_bm : "",
      emp_cscc :  countryDetails.emp_cscc ? countryDetails.emp_cscc : "",
      emp_csct :  countryDetails.emp_csct ? countryDetails.emp_csct : "",
      emp_ra :  countryDetails.emp_ra ? countryDetails.emp_ra : "",

      
    });
    // const validateStopFlag = Yup.object().shape({
    //   country_name: Yup.string()
    //     .min(2, 'country name must be at least 2 characters')
    //     .max(200, 'country name must be at most 200 characters')  
    //     .required("Please enter country name")
  
    // });    
    
    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy" >
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">
              <div className="col-lg-12 col-sm-12 col-xs-12">
                <h1>
                  Manage Master country
                  <small />
                </h1>
                
              </div>
              <div className="col-lg-12 col-sm-12 col-xs-12 topSearchSection">
                  <form className="form">
                        <div className="">
                        <input
                            className="form-control"
                            name="country_name"
                            id="country_name"
                            placeholder="Filter by country name"
                          />
                        </div>
                        <div className="">
                            <select name="employee_id" id="employee_id" className="form-control">
                              <option value="">Select Employee</option>
                              {this.state.all_employee.map(
                                (emp, i) => (
                                  <option key={i} value={emp.value}>
                                    {emp.label}
                                  </option>
                                )
                              )}
                            </select>
                          </div>

                        <div className="">
                        <input
                          type="submit"
                          value="Search"
                          className="btn btn-warning btn-sm"
                          onClick={(e) => this.regSearch( e )}
                        />
                        {this.state.remove_search ? <a onClick={() => this.clearSearch()} className="btn btn-danger btn-sm"> Remove </a> : null}
                        </div>
                        <div className="clearfix"></div>
                  </form>
                  <div className="clearfix"></div>
                </div>
              </div>
            </section>            
            <section className="content">
              <div className="box">
              
                <div className="box-body">

                <div className="nav-tabs-custom">
                <ul className="nav nav-tabs">
                <li className="tabButtonSec pull-right">
                {this.state.count > 0 ? 
                  <span onClick={(e) => this.downloadXLSX(e)} >
                      <LinkWithTooltip
                          tooltip={`Click here to download excel`}
                          href="#"
                          id="tooltip-my"
                          clicked={e => this.checkHandler(e)}
                      >
                          <i className="fas fa-download"></i>
                      </LinkWithTooltip>
                  </span>
                : null }
                </li>
                </ul>
                </div>

                  <BootstrapTable data={this.state.country}>
                    <TableHeaderColumn isKey dataField="country_name" dataFormat={custContent(this)}>
                      Country Name
                    </TableHeaderColumn>
                    <TableHeaderColumn  dataField="bm_name" dataFormat={custContent(this)}>
                      BM Name
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="cscc_name" dataFormat={custContent(this)}>
                      CSCC NAME
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="csct_name"
                      dataFormat={custContent(this)}
                    >
                       CSCT NAME
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="ra_name"
                      dataFormat={custContent(this)}
                    >
                       RA NAME
                    </TableHeaderColumn>
                    {this.state.access.edit === true  ?
                    <TableHeaderColumn
                      dataField="country_id"
                      dataFormat={actionFormatter(this)}
                    >
                      Action
                    </TableHeaderColumn>
                    : null}
                  </BootstrapTable>

                  {this.state.count > this.state.itemPerPage ? (
                    <Row>
                      <Col md={12}>
                        <div className="paginationOuter text-right">
                          <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemPerPage}
                            totalItemsCount={this.state.count}
                            itemClass='nav-item'
                            linkClass='nav-link'
                            activeClass='active'
                            onChange={this.handlePageChange}
                          />
                        </div>
                      </Col>
                    </Row>
                  ) : null}

                  {/* ======= Edit ======== */}
                    <Modal
                      show={this.state.showModal}
                      onHide={() => this.modalCloseHandler()} backdrop="static"
                    >
                      <Formik
                        initialValues={newInitialValues}
                        validationSchema={false}
                        onSubmit={this.handleSubmitEvent}
                      >
                        {({ values, errors, touched, isValid, isSubmitting }) => {
                          return (
                            <Form>
                              {this.state.showModalLoader === true ? (
                                <div className="loading_reddy_outer">
                                  <div className="loading_reddy" >
                                    <img src={whitelogo} alt="loader" />
                                  </div>
                                </div>
                              ) : ("")}
                              <Modal.Header closeButton>
                                <Modal.Title>
                                  Edit Country
                                </Modal.Title>
                              </Modal.Header>
                              <Modal.Body>
                                <div className="contBox">                              
                                <Row>
                                <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>
                                        Country Name
                                      </label>
                                      <Field
                                        name="country_name"
                                         id = "country_name"
                                        type="text"
                                        readOnly
                                        className={`form-control`}
                                        placeholder="Enter name"
                                        autoComplete="off"
                                        value={values.country_name}
                                      />
                                      {errors.country_name && touched.country_name ? (
                                        <span className="errorMsg">
                                          {errors.country_name}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>   
                                                          
                                <Row>
                                <Col xs={12} sm={12} md={6}>
                                  <div className="form-group">
                                    <label>
                                      BM Name
                                    </label>
                                    <Field
                                      className={`selectArowGray form-control`}
                                      component="select"
                                      autoComplete="off"
                                      name="emp_bm"
                                      value={values.emp_bm}
                                    >
                                      <option key="-1" value="">
                                        Select
                                      </option>
                                      {this.state.employee_bm.map(
                                        (bm, i) => (
                                          <option key={i} value={bm.value}>
                                            {bm.label}
                                          </option>
                                        )
                                      )}
                                    </Field>
                                    {errors.emp_bm && touched.emp_bm ? (
                                      <span className="errorMsg">
                                        {errors.emp_bm}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                                <Col xs={12} sm={12} md={6}>
                                  <div className="form-group">
                                    <label>
                                      CSCC Name
                                    </label>
                                    <Field
                                      className={`selectArowGray form-control`}
                                      component="select"
                                      autoComplete="off"
                                      name="emp_cscc"
                                      value={values.emp_cscc}
                                    >
                                      <option key="-1" value="">
                                        Select
                                      </option>
                                      {this.state.employee_cscc.map(
                                        (cscc, i) => (
                                          <option key={i} value={cscc.value}>
                                            {cscc.label}
                                          </option>
                                        )
                                      )}
                                    </Field>
                                    {errors.emp_cscc && touched.emp_cscc ? (
                                      <span className="errorMsg">
                                        {errors.emp_cscc}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              </Row>
                              <Row>
                                <Col xs={12} sm={12} md={6}>
                                  <div className="form-group">
                                    <label>
                                      CSCT Name
                                    </label>
                                    <Field
                                      className={`selectArowGray form-control`}
                                      component="select"
                                      autoComplete="off"
                                      name="emp_csct"
                                      value={values.emp_csct}
                                    >
                                      <option key="-1" value="">
                                        Select
                                      </option>
                                      {this.state.employee_csct.map(
                                        (csct, i) => (
                                          <option key={i} value={csct.value}>
                                            {csct.label}
                                          </option>
                                        )
                                      )}
                                    </Field>
                                    {errors.emp_csct && touched.emp_csct ? (
                                      <span className="errorMsg">
                                        {errors.emp_csct}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                                 <Col xs={12} sm={12} md={6}>
                                  <div className="form-group">
                                    <label>
                                      RA Name
                                    </label>
                                    <Field
                                      className={`selectArowGray form-control`}
                                      component="select"
                                      autoComplete="off"
                                      name="emp_ra"
                                      value={values.emp_ra}
                                    >
                                      <option key="-1" value="">
                                        Select
                                      </option>
                                      {this.state.employee_ra.map(
                                        (ra, i) => (
                                          <option key={i} value={ra.value}>
                                            {ra.label}
                                          </option>
                                        )
                                      )}
                                    </Field>
                                    {errors.emp_ra && touched.emp_ra ? (
                                      <span className="errorMsg">
                                        {errors.emp_ra}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              </Row>
                                  {errors.message ? (
                                    <Row>
                                      <Col xs={12} sm={12} md={12}>
                                        <span className="errorMsg">
                                          {errors.message}
                                        </span>
                                      </Col>
                                    </Row>
                                  ) : ("")
                                  }
                                </div>
                              </Modal.Body>
                              <Modal.Footer>
                                  <button
                                    className={`btn btn-success btn-sm ${
                                      isValid ? "btn-custom-green" : "btn-disable"
                                      } mr-2`}
                                    type="submit"
                                    disabled={isValid ? false : false}
                                  >
                                    {this.state.countryflagId > 0
                                      ? isSubmitting
                                        ? "Updating..."
                                        : "Update"
                                      : isSubmitting
                                        ? "Submitting..."
                                        : "Submit"}
                                  </button>
                                  <button
                                    onClick={e => this.modalCloseHandler()}
                                    className={`btn btn-danger btn-sm`}
                                    type="button"
                                  >
                                    Close
                                  </button>
                              </Modal.Footer>
                            </Form>
                          );
                        }}
                      </Formik>
                    </Modal>
                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}
export default country;