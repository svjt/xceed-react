import React, { Component } from 'react';
import Pagination from "react-js-pagination";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,  
  Tooltip,
  OverlayTrigger
} from "react-bootstrap";
import { Link } from "react-router-dom";


import Layout from "../layout/Layout";
import whitelogo from '../../../assets/images/drreddylogo_white.png';
import API from "../../../shared/admin-axios";
import { showErrorMessage } from "../../../shared/handle_error";
import { htmlDecode } from '../../../shared/helper';
import { getSuperAdmin, getAdminGroup } from '../../../shared/helper';

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

const custStatus = () => cell => {
  return cell === 1 ? "Open" : "Closed";
};

const custContentCreate = () => (cell, row) => {
  let cre_name = `${htmlDecode(row.created_by_f_name)} ${htmlDecode(row.created_by_l_name)} (${row.created_by_emp_desig_name})`;
  return cre_name;
};

const custContentClosed = () => (cell, row) => {
  let clo_name = '-';
  if (row.closed_by_id > 0) {
    clo_name = `${htmlDecode(row.closed_by_f_name)} ${htmlDecode(row.closed_by_l_name)} (${row.closed_by_emp_desig_name})`;
  }
  return clo_name;
};

class PausedSla extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      activePage: 1,
      totalCount: 0,
      itemPerPage: 20,

      search_name: '',
      remove_search: false,
      get_access_data:false
    };
  }

  componentDidMount() {
    //this.getPausedSlaList();
    const superAdmin  = getSuperAdmin(localStorage.admin_token);
    
    if(superAdmin === 1){
      this.setState({
        access: {
           view : true,
           add : true,
           edit : true,
           delete : true
         },
         get_access_data:true
     });
     this.getPausedSlaList();
    }else{
      const adminGroup  = getAdminGroup(localStorage.admin_token);
      API.get(`/api/adm_group/single_access/${adminGroup}/${'PAUSED_SLA_LOGS_MANAGEMENT'}`)
      .then(res => {
        this.setState({
          access: res.data.data,
          get_access_data:true
        }); 

        if(res.data.data.view === true){
          this.getPausedSlaList();
        }else{
          this.props.history.push('/admin/dashboard');
        }
        
      })
      .catch(err => {
        showErrorMessage(err,this.props);
      });
    } 
  }

  getPausedSlaList(page = 1) {
    let task_reference = this.state.search_name;
    let cond = '';
    if (task_reference.length > 0) {
      cond = `&task_reference=${encodeURIComponent(task_reference)}`;
    }

    API.get(`/api/feed/paused_sla?page=${page}${cond}`)
      .then(res => {
        this.setState({
          paused_sla: res.data.data,
          count: res.data.count_paused_sla,
          isLoading: false
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        showErrorMessage(err, this.props);
      });
    this.setState({
      isLoading: false
    });
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getPausedSlaList(pageNumber > 0 ? pageNumber : 1);
  };

  slaSearch = (e) => {
    e.preventDefault();

    var task_reference = document.getElementById('task_reference').value

    if (task_reference === '') {
      return false;
    }

    API.get(`/api/feed/paused_sla?page=1&task_reference=${encodeURIComponent(task_reference)}`)
      .then(res => {
        this.setState({
          paused_sla: res.data.data,
          count: res.data.count_paused_sla,
          isLoading: false,
          search_name: task_reference,
          remove_search: true,
          activePage: 1
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        showErrorMessage(err, this.props);
      });
  }

  clearSearch = () => {
    document.getElementById('task_reference').value = '';

    this.setState({
      search_name: '',
      remove_search: false
    }, () => {
      this.getPausedSlaList();
      this.setState({ activePage: 1 })
    })
  }

  downloadXLSX = (e) => {
    e.preventDefault();

    var task_reference = document.getElementById('task_reference').value;
    
    API.get(`/api/feed/paused_sla/download?page=1&task_reference=${encodeURIComponent(task_reference)}`,{responseType: 'blob'})
    .then(res => { 
      let url    = window.URL.createObjectURL(res.data);
      let a      = document.createElement('a');
      a.href     = url;
      a.download = 'paused_sla_logs.xlsx';
      a.click();

    }).catch(err => {
      showErrorMessage(err,this.props);
    });
  }

  checkHandler = (event) => {
    event.preventDefault();
  };

  render() {
    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy" >
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">
                <div className="col-lg-9 col-sm-6 col-xs-12">
                  <h1>
                    Paused SLA Logs
                    <small />
                  </h1>
                  
                </div>
                <div className="col-lg-12 col-sm-12 col-xs-12 topSearchSection">
                  <form className="form">
                    <div className="">
                      <input
                        className="form-control"
                        name="task_reference"
                        id="task_reference"
                        placeholder="Task reference"
                      />
                    </div>

                    <div className="">
                      <input
                        type="submit"
                        value="Search"
                        className="btn btn-warning btn-sm"
                        onClick={(e) => this.slaSearch(e)}
                      />
                      {this.state.remove_search ? <a onClick={() => this.clearSearch()} className="btn btn-danger btn-sm"> Remove </a> : null}
                    </div>
                  </form>
                </div>
              </div>
            </section>
            <section className="content">
              <div className="box">
              
                <div className="box-body">

                <div className="nav-tabs-custom">
<ul className="nav nav-tabs">
<li className="tabButtonSec pull-right">
{this.state.count > 0 ? 
                    <span onClick={(e) => this.downloadXLSX(e)} >
                        <LinkWithTooltip
                            tooltip={`Click here to download excel`}
                            href="#"
                            id="tooltip-my"
                            clicked={e => this.checkHandler(e)}
                        >
                            <i className="fas fa-download"></i>
                        </LinkWithTooltip>
                    </span>
                  : null }
</li>
</ul>
</div>


                  <BootstrapTable data={this.state.paused_sla}>
                    <TableHeaderColumn isKey dataField="created_by_f_name" dataFormat={custContentCreate(this)}>
                      Created By Name
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="task_ref">
                      Task
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="closed_by_f_name" dataFormat={custContentClosed(this)}>
                      Closed By Name
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="display_start_date">
                      Start Date
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="display_end_date">
                      End Date
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="display_existing_front_sla">
                      Existing Front SLA
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="display_existing_back_sla">
                      Existing Back SLA
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="status" dataFormat={custStatus(this)}>
                      Status
                    </TableHeaderColumn>
                  </BootstrapTable>

                  {this.state.count > this.state.itemPerPage ? (
                    <Row>
                      <Col md={12}>
                        <div className="paginationOuter text-right">
                          <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemPerPage}
                            totalItemsCount={this.state.count}
                            itemClass='nav-item'
                            linkClass='nav-link'
                            activeClass='active'
                            onChange={this.handlePageChange}
                          />
                        </div>
                      </Col>
                    </Row>
                  ) : null}
                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}
export default PausedSla;