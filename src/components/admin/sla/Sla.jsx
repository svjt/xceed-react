import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
//import Loader from "react-loader-spinner";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import API from "../../../shared/admin-axios";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import Layout from "../layout/Layout";
import whitelogo from "../../../assets/images/drreddylogo_white.png";
import { showErrorMessage } from "../../../shared/handle_error";
import { getSuperAdmin, getAdminGroup } from "../../../shared/helper";

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const actionFormatter = (refObj) => (cell) => {
  return (
    <div className="actionStyle">
      {refObj.state.access.edit === true ? (
        <LinkWithTooltip
          tooltip="Click to Edit"
          href="#"
          clicked={(e) => refObj.modalShowHandler(e, cell)}
          id="tooltip-1"
        >
          <i className="far fa-edit" />
        </LinkWithTooltip>
      ) : null}
    </div>
  );
};

const slaStatus = (refObj) => (cell) => {
  return cell === 1 ? "Active" : "Inactive";
};

const initialValues = {
  req_name: "",
  backend_sla: "",
  frontend_sla: "",
  total_sla: "",
};

class Sla extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      slaArr: [],
      slaDetails: [],
      slaflagId: 0,
      showModal: false,
      selectStatus: [
        { id: "0", name: "Inactive" },
        { id: "1", name: "Active" },
      ],
      get_access_data: false,
    };
  }

  componentDidMount() {
    //this.getSlaList();
    const superAdmin = getSuperAdmin(localStorage.admin_token);

    if (superAdmin === 1) {
      this.setState({
        access: {
          view: true,
          add: true,
          edit: true,
          delete: true,
        },
        get_access_data: true,
      });
      this.getSlaList();
    } else {
      const adminGroup = getAdminGroup(localStorage.admin_token);
      API.get(
        `/api/adm_group/single_access/${adminGroup}/${"SLA_TYPE_MANAGEMENT"}`
      )
        .then((res) => {
          this.setState({
            access: res.data.data,
            get_access_data: true,
          });

          if (res.data.data.view === true) {
            this.getSlaList();
          } else {
            this.props.history.push("/admin/dashboard");
          }
        })
        .catch((err) => {
          showErrorMessage(err, this.props);
        });
    }
  }

  getSlaList() {
    API.get("/api/feed/request_type_adm")
      .then((res) => {
        this.setState({
          slaArr: res.data.data,
          isLoading: false,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  getIndividualSla(id, callBack) {
    API.get(`/api/feed/request_type/${id}`)
      .then((res) => {
        this.setState({
          slaDetails: res.data.data,
        });
        callBack();
      })
      .catch((err) => {
        console.log(err);
      });
  }

  modalCloseHandler = () => {
    this.setState({ slaflagId: 0 });
    this.setState({ showModal: false });
  };

  modalShowHandler = (event, id) => {
    event.preventDefault();
    this.setState({ slaflagId: id });
    this.getIndividualSla(id, () => this.setState({ showModal: true }));
  };

  handleSubmitEvent = (values, actions) => {
    const post_data = {
      frontend_sla: values.frontend_sla,
      backend_sla: values.backend_sla,
    };
    const id = this.state.slaflagId;
    API.put(`/api/feed/request_type/${id}`, post_data)
      .then((res) => {
        this.modalCloseHandler();
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: "Record updated successfully.",
          icon: "success",
        }).then(() => {
          this.getSlaList();
        });
      })
      .catch((err) => {
        this.setState({ showModalLoader: false });
        if (err.data.status === 3) {
          this.setState({
            showModal: false,
          });
          showErrorMessage(err, this.props);
        } else {
          actions.setErrors(err.data.errors);
          actions.setSubmitting(false);
        }
      });
  };

  renderShowsTotal = (start, to, total) => {
    return (
      <span className="pageShow">
        Showing {start} to {to}, of {total} records
      </span>
    );
  };

  render() {
    const paginationOptions = {
      page: 1, // which page you want to show as default
      sizePerPageList: [
        {
          text: "40",
          value: 40,
        },
        {
          text: "100",
          value: 100,
        },
        {
          text: "All",
          value: this.state.slaArr.length > 0 ? this.state.slaArr.length : 1,
        },
      ], // you can change the dropdown list for size per page
      sizePerPage: 20, // which size per page you want to locate as default
      pageStartIndex: 1, // where to start counting the pages
      paginationSize: 3, // the pagination bar size.
      prePage: "Prev", // Previous page button text
      nextPage: "Next", // Next page button text
      firstPage: "First", // First page button text
      lastPage: "Last", // Last page button text
      paginationShowsTotal: this.renderShowsTotal, // Accept bool or function
      paginationPosition: "bottom", // default is bottom, top and both is all available
      // hideSizePerPage: true //> You can hide the dropdown for sizePerPage
      // alwaysShowAllBtns: true // Always show next and previous button
      // withFirstAndLast: false //> Hide the going to First and Last page button
    };

    const { slaDetails } = this.state;

    const newInitialValues = Object.assign(initialValues, {
      req_name: slaDetails.req_name,
      frontend_sla: slaDetails.frontend_sla
        ? slaDetails.frontend_sla.toString()
        : "",
      backend_sla: slaDetails.backend_sla
        ? slaDetails.backend_sla.toString()
        : "",
    });

    const validateAdmin = Yup.object().shape({
      frontend_sla: Yup.string()
        .required("Please enter SLA")
        .matches(/^[1-9][0-9]*$/, "Invalid Frontend SLA"),
      backend_sla: Yup.string()
        .required("Please enter SLA")
        .matches(/^[1-9][0-9]*$/, "Invalid Backend SLA"),
    });

    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy">
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="row">
                <div className="col-lg-9 col-sm-6 col-xs-12">
                  <h1>
                    SLA Type
                    <small />
                  </h1>
                </div>
              </div>
            </section>
            <section className="content">
              <div className="box">
                <div className="box-body">
                  <BootstrapTable
                    data={this.state.slaArr}
                    pagination={true}
                    options={paginationOptions}
                    striped={true}
                    hover={true}
                  >
                    <TableHeaderColumn
                      isKey
                      dataField="req_name"
                      dataSort={true}
                    >
                      Name
                    </TableHeaderColumn>

                    <TableHeaderColumn dataField="frontend_sla" dataSort={true}>
                      Front End SLA
                    </TableHeaderColumn>

                    <TableHeaderColumn dataField="backend_sla" dataSort={true}>
                      Back End SLA
                    </TableHeaderColumn>

                    <TableHeaderColumn dataField="total_sla" dataSort={true}>
                      Total SLA
                    </TableHeaderColumn>

                    <TableHeaderColumn
                      dataField="status"
                      dataFormat={slaStatus(this)}
                    >
                      Status
                    </TableHeaderColumn>

                    {this.state.access.edit === true ? (
                      <TableHeaderColumn
                        dataField="type_id"
                        dataFormat={actionFormatter(this)}
                        dataAlign=""
                      >
                        Action
                      </TableHeaderColumn>
                    ) : null}
                  </BootstrapTable>

                  {/* ======= Add/Edit Admin ======== */}

                  <Modal
                    show={this.state.showModal}
                    onHide={() => this.modalCloseHandler()}
                    backdrop="static"
                  >
                    <Formik
                      initialValues={newInitialValues}
                      validationSchema={validateAdmin}
                      onSubmit={this.handleSubmitEvent}
                    >
                      {({ values, errors, touched, isValid, isSubmitting }) => {
                        return (
                          <Form>
                            <Modal.Header closeButton>
                              <Modal.Title>
                                {this.state.slaflagId > 0 ? "Edit" : "Add"} SLA
                              </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="contBox">
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>{slaDetails.req_name}</label>
                                    </div>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Front End SLA
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="frontend_sla"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter total sla"
                                        autoComplete="off"
                                      />
                                      {errors.frontend_sla &&
                                      touched.frontend_sla ? (
                                        <span className="errorMsg">
                                          {errors.frontend_sla}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>

                                <Row>
                                  <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                      <label>
                                        Back End SLA
                                        <span className="impField">*</span>
                                      </label>
                                      <Field
                                        name="backend_sla"
                                        type="text"
                                        className={`form-control`}
                                        placeholder="Enter total sla"
                                        autoComplete="off"
                                      />
                                      {errors.backend_sla &&
                                      touched.backend_sla ? (
                                        <span className="errorMsg">
                                          {errors.backend_sla}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                                {errors.message ? (
                                  <Row>
                                    <Col xs={12} sm={12} md={12}>
                                      <span className="errorMsg">
                                        {errors.message}
                                      </span>
                                    </Col>
                                  </Row>
                                ) : (
                                  ""
                                )}
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                              <button
                                className={`btn btn-success btn-sm ${
                                  isValid ? "btn-custom-green" : "btn-disable"
                                } m-r-10`}
                                type="submit"
                                disabled={isValid ? false : true}
                              >
                                {this.state.slaflagId > 0
                                  ? isSubmitting
                                    ? "Updating..."
                                    : "Update"
                                  : isSubmitting
                                  ? "Submitting..."
                                  : "Submit"}
                              </button>
                              <button
                                onClick={(e) => this.modalCloseHandler()}
                                className={`btn btn-danger btn-sm`}
                                type="button"
                              >
                                Close
                              </button>
                            </Modal.Footer>
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>
                </div>
              </div>
            </section>
          </div>
        </Layout>
      );
    }
  }
}

export default Sla;
