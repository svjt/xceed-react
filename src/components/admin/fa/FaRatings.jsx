import React, { Component } from "react";
import Pagination from "react-js-pagination";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal,
  Table,
} from "react-bootstrap";
//import { Label } from 'reactstrap';
import { Link } from "react-router-dom";
import { Formik, Field, Form } from "formik";
import swal from "sweetalert";
import * as Yup from "yup";

import Layout from "../layout/Layout";
import whitelogo from "../../../assets/images/drreddylogo_white.png";
import API from "../../../shared/admin-axios";
import { showErrorMessage } from "../../../shared/handle_error";
import { htmlDecode } from "../../../shared/helper";
import { getSuperAdmin, getAdminGroup, inArray } from "../../../shared/helper";
import StatusColumn from "./StatusColumn";
import { Chart } from "react-google-charts";
import Rating from "react-rating";
import ProductRatingTable from './ProductRatingTable';
import RatingTable from './RatingTable';

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
const actionFormatter = (refObj) => (cell, row) => {
  //console.log("===========", row);
  if (row.rating > 0) {
    return (
      <div className="actionStyle">
        <LinkWithTooltip
          tooltip="Click to View"
          href="#"
          clicked={(e) =>
            refObj.modalShowHandler(
              e,
              cell,
              row.rating_options,
              row.rating_comment,
              row.rating
            )
          }
          id="tooltip-1"
        >
          <i className="fas fa-eye" />
        </LinkWithTooltip>
      </div>
    );
  }
};

const setRating = (refOBj) => (cell, row) => {
  if (row.rating > 0 && row.rating_skipped == 0) {
    return `${row.rating}`;
  } else if (row.rating == 0 && row.rating_skipped == 0) {
    return "Not rated";
  } else if (row.rating_skipped == 1 && row.rating == 0) {
    return "Skipped";
  }
};

const setCompany = (refOBj) => (cell, row) => {

  if (row.rated_by_type == 2) {
    return `${row.agent_company}`;
  } else {
    if (row.company_name != null) {
      return `${row.company_name}`;
    } else {
      return "-";
    }
  }

};
const setCustomer = (refOBj) => (cell, row) => {
  if (row.rated_by_type == 2) {
    return `${row.first_name_rated_by_agent} ${row.last_name_rated_by_agent}`;
  } else {
    return `${row.first_name_rated_by} ${row.last_name_rated_by}`;
  }
};

const setSubmittedBy = (refOBj) => (cell, row) => {
  if (row.rated_by > 0) {
    if (row.rated_by_type == 2) {
      return `${row.first_name_rated_by_agent} ${row.last_name_rated_by_agent} (Agent)`;
    } else {
      return `${row.first_name_rated_by} ${row.last_name_rated_by}`;
    }
  } else {
    return `-`;
  }
};

class TaskRatings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      get_access_data: false,
      task_ratings_product: [],
      task_ratings: [],
      request_type: [],
      companyList: [],
      productCustomerList: [],
      activePage: 1,
      totalCount: 0,
      itemPerPage: 20,
      search_rating_score: "",
      search_type: "",
      search_compid: "",
      search_cid: "",
      showModal: false,
      showCustomerBlock: false,
      showCompanyBlock: false,
    };
  }

  modalShowHandler = (
    event,
    id,
    rating_options,
    rating_comment,
    rating_stars
  ) => {
    event.preventDefault();
    //console.log("rating_options", rating_options.split());
    if (rating_options && rating_options.length > 0) {
      var rating_options = rating_options.split(",");
      API.get(`api/fa/task_ratings_options`)
        .then((res) => {
          var taskOptionArr = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            if (inArray(element.o_id, rating_options)) {
              taskOptionArr.push([element.option_name_en]);
            }
          }
          var r_comment = rating_comment;

          this.setState({
            taskOptionArr: taskOptionArr,
            comment: r_comment,
            rating_stars: rating_stars,
            showModal: true,
          });
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      var taskOptionArr = [];
      var r_comment = rating_comment;
      this.setState({
        taskOptionArr: taskOptionArr,
        comment: r_comment,
        rating_stars: rating_stars,
        showModal: true,
      });
    }
  };

  modalCloseHandler = () => {
    this.setState({ showModal: false });
  };

  componentDidMount() {
    const superAdmin = getSuperAdmin(localStorage.admin_token);

    if (superAdmin === 1) {
      this.setState({
        access: {
          view: true,
          add: true,
          edit: true,
          delete: true,
        },
        get_access_data: true,
      });
      this.getTaskRatingsList();
      this.getCompanyList();
    } else {
      const adminGroup = getAdminGroup(localStorage.admin_token);
      API.get(`/api/adm_group/single_access/${adminGroup}/${"TASK_MANAGEMENT"}`)
        .then((res) => {
          this.setState({
            access: res.data.data,
            get_access_data: true,
          });

          if (res.data.data.view === true) {
            this.getTaskRatingsList();
            this.getCompanyList();
          } else {
            this.props.history.push("/admin/dashboard");
          }
        })
        .catch((err) => {
          showErrorMessage(err, this.props);
        });
    }
  }

  getTaskRatingsList(page = 1) {
    let rating_score = this.state.search_rating_score;
    let type = this.state.search_type;
    let compid = this.state.search_compid;
    let cid = this.state.search_cid;

    //==================MONOSOM=================//
    API.get(
      `/api/fa/get_task_ratings_review?page=1&type=${encodeURIComponent(type)}&compid=${encodeURIComponent(compid)}&cid=${encodeURIComponent(
        cid
      )}`
    )
      .then((res) => {
        this.setState({
          total_riview:
            res.data.tasks_positive +
            res.data.task_neutral +
            res.data.task_negative,
          tasks_positive: res.data.tasks_positive,
          task_neutral: res.data.task_neutral,
          task_negative: res.data.task_negative,
          agent_total_riview:
            res.data.agent_tasks_positive +
            res.data.agent_task_neutral +
            res.data.agent_task_negative,
          agent_tasks_positive: res.data.agent_tasks_positive,
          agent_task_neutral: res.data.agent_task_neutral,
          agent_task_negative: res.data.agent_task_negative,
        });
      })
      .catch((err) => {
        console.log(err);
      });

    API.get(
      `/api/fa/task_ratings_product?page=${page}&type=${encodeURIComponent(type)}&rating_score=${encodeURIComponent(
        rating_score
      )}&compid=${encodeURIComponent(compid)}&cid=${encodeURIComponent(
        cid
      )}`
    )
      .then((res) => {
        this.setState({
          task_ratings_product: res.data.data,
          count_product: res.data.count_task_ratings_product
        });

        API.get(`/api/fa/task_ratings?page=${page}&type=${encodeURIComponent(type)}&rating_score=${encodeURIComponent(
          rating_score
        )}&compid=${encodeURIComponent(compid)}&cid=${encodeURIComponent(
          cid
        )}`).then(res => {
          this.setState({
            task_ratings: res.data.data,
            count: res.data.count_task_ratings,
            search_rating_score: rating_score,
            search_type: type,
            search_compid: compid,
            search_cid: cid,
            isLoading: false,
          });
        }).catch(err => { });

      }).catch((err) => {
        this.setState({
          isLoading: false,
        });
        showErrorMessage(err, this.props);
      });
  }

  getCompanyList() {
    API.get("/api/customers/company")
      .then((res) => {
        this.setState({
          companyList: res.data.data,
        });
      })
      .catch((err) => {
        showErrorMessage(err, this.props);
      });
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getTaskRatingsList(pageNumber > 0 ? pageNumber : 1);
  };

  taskRatingsListSearch = (e) => {
    e.preventDefault();


    var rating_score = document.getElementById("rating_score").value;
    var type = document.getElementById("type").value;
    if (type == 2) {
      var type = type;
      var compid = "";
      var cid = "";
    } else if (type == 1) {
      var type = type;
      var compid = document.getElementById("compid").value;
      if (compid > 0) {
        var cid = document.getElementById("cid").value;
      } else {
        var cid = "";
      }
    } else {
      var type = "";
      var compid = "";
      var cid = "";
    }


    if (
      rating_score === "" &&
      type === "" &&
      compid === "" &&
      cid === ""
    ) {
      return false;
    }

    this.setState({
      total_riview: "",
      tasks_positive: "",
      task_neutral: "",
      task_negative: "",
      agent_total_riview: "",
      agent_tasks_positive: "",
      agent_task_neutral: "",
      agent_task_negative: "",
      task_ratings_product: "",
      task_ratings: "",
      search_rating_score: "",
      search_type: "",
      search_compid: "",
      search_cid: "",
      activePage: 1,
    });

    //==================MONOSOM=================//
    API.get(
      `/api/fa/get_task_ratings_review?page=1&type=${encodeURIComponent(type)}&compid=${encodeURIComponent(compid)}&cid=${encodeURIComponent(
        cid
      )}`
    )
      .then((res) => {
        this.setState({
          total_riview:
            res.data.tasks_positive +
            res.data.task_neutral +
            res.data.task_negative,
          tasks_positive: res.data.tasks_positive,
          task_neutral: res.data.task_neutral,
          task_negative: res.data.task_negative,
          agent_total_riview:
            res.data.agent_tasks_positive +
            res.data.agent_task_neutral +
            res.data.agent_task_negative,
          agent_tasks_positive: res.data.agent_tasks_positive,
          agent_task_neutral: res.data.agent_task_neutral,
          agent_task_negative: res.data.agent_task_negative,
        });
      })
      .catch((err) => {
        console.log(err);
      });

    API.get(
      `/api/fa/task_ratings_product?page=1&type=${encodeURIComponent(type)}&rating_score=${encodeURIComponent(
        rating_score
      )}&compid=${encodeURIComponent(compid)}&cid=${encodeURIComponent(
        cid
      )}`
    )
      .then((res) => {
        this.setState({
          task_ratings_product: res.data.data,
          count_product: res.data.count_task_ratings_product,
        });
        API.get(`/api/fa/task_ratings?page=1&type=${encodeURIComponent(type)}&rating_score=${encodeURIComponent(
          rating_score
        )}&compid=${encodeURIComponent(compid)}&cid=${encodeURIComponent(
          cid
        )}`).then(res => {
          this.setState({
            task_ratings: res.data.data,
            count: res.data.count_task_ratings,
            search_rating_score: rating_score,
            search_type: type,
            search_compid: compid,
            search_cid: cid,
            activePage: 1,
            isLoading: false,
          });
        }).catch(err => { });
      })
      .catch((err) => {
        this.setState({
          isLoading: false,
        });
        showErrorMessage(err, this.props);
      });
  };

  clearSearch = () => {
    this.setState({
      total_riview: "",
      tasks_positive: "",
      task_neutral: "",
      task_negative: "",
      agent_total_riview: "",
      agent_tasks_positive: "",
      agent_task_neutral: "",
      agent_task_negative: "",
      task_ratings_product: "",
      task_ratings: "",
      search_rating_score: "",
      search_type: "",
      search_compid: "",
      search_cid: "",
      activePage: 1,
    });

    document.getElementById("rating_score").value = "";
    document.getElementById("type").value = "";

    if (this.state.showCompanyBlock === true) {
      document.getElementById("compid").value = "";
    }
    if (this.state.showCustomerBlock === true) {
      document.getElementById("cid").value = "";
    }
    this.setState(
      {
        search_rating_score: "",
        search_type: "",
        search_compid: "",
        search_cid: "",
        showCompanyBlock: false,
        showCustomerBlock: false,
      },
      () => {
        this.getTaskRatingsList();
        this.setState({ activePage: 1 });
      }
    );
  };

  filterSearchCust = (event) => {
    if (event.target.value > 0) {
      this.companyCustomers(event.target.value);
    } else {
      this.setState({
        showCustomerBlock: false,
        productCustomerList: [],
      });
    }
  };

  filterSearchComp = (event) => {
    if (event.target.value == 1) {
      this.setState({
        showCompanyBlock: true,
      });
    } else {
      this.setState({
        showCompanyBlock: false,
        showCustomerBlock: false,
      });
    }
  };

  companyCustomers(id) {
    API.get(`/api/agents/company_customers/${id}`)
      .then((res) => {
        this.setState({
          productCustomerList: res.data.data,
          showCustomerBlock: true,
        });
      })
      .catch((err) => {
        showErrorMessage(err, this.props);
      });
  }


  handleTabs = (event) => {

    if (event.currentTarget.className === "active") {
      //DO NOTHING
    } else {

      var elems = document.querySelectorAll('[id^="tab_"]');
      var elemsContainer = document.querySelectorAll('[id^="show_tab_"]');
      var currId = event.currentTarget.id;

      for (var i = 0; i < elems.length; i++) {
        elems[i].classList.remove('active');
      }

      for (var j = 0; j < elemsContainer.length; j++) {
        elemsContainer[j].style.display = 'none';
      }

      event.currentTarget.classList.add('active');
      event.currentTarget.classList.add('active');
      document.querySelector('#show_' + currId).style.display = 'block';
      this.setState({ tabsClicked: currId });
    }
  }

  render() {
    if (this.state.isLoading === true || this.state.get_access_data === false) {
      return (
        <>
          <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy">
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      //console.log('===', this.state.count_product)
      const { valueCompid } = this.state;
      return (
        <Layout {...this.props}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="topSearchSection">
                <div className="searchfield">
                  <form className="form">
                    <div>
                      <select
                        name="total"
                        id="rating_score"
                        className="form-control"
                      >
                        <option value="">Score</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                        <option value="4">Four</option>
                        <option value="5">Five</option>
                      </select>
                    </div>
                    <div>
                      <select
                        name="type"
                        id="type"
                        className="form-control"
                        onChange={this.filterSearchComp}
                      >
                        <option value="">Type</option>
                        <option value="1">Customer</option>
                        <option value="2">Agent</option>
                      </select>
                    </div>
                    <div>
                      {this.state.showCompanyBlock && (
                        <select
                          name="custcompany"
                          id="compid"
                          className="form-control w-150"
                          onChange={this.filterSearchCust}
                          value={valueCompid}
                        >
                          <option value="">Select Customer</option>
                          {this.state.companyList.map((company, i) => (
                            <option key={i} value={company.company_id}>
                              {htmlDecode(company.company_name)}
                            </option>
                          ))}
                        </select>
                      )}
                    </div>
                    <div>
                      {this.state.showCustomerBlock && (
                        <select
                          name="product"
                          id="cid"
                          className="form-control w-150"
                        >
                          <option value="">Select User</option>
                          {this.state.productCustomerList.map((customer, i) => (
                            <option key={i} value={customer.customer_id}>
                              {htmlDecode(customer.first_name)}{" "}
                              {htmlDecode(customer.last_name)}
                            </option>
                          ))}
                        </select>
                      )}
                    </div>
                    <div className="reset">
                      <input
                        type="submit"
                        value="Search"
                        className="btn btn-warning btn-sm"
                        onClick={(e) => this.taskRatingsListSearch(e)}
                      />

                      <a
                        onClick={() => this.clearSearch()}
                        className="btn btn-danger btn-sm"
                      >
                        {" "}
                        Reset{" "}
                      </a>

                    </div>
                    <div className="clearfix"></div>
                  </form>
                </div>
              </div>

              <div className="row">
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <div className="pd-15">
                    <div className="chartbg">
                      <div className="d-flex justify-content-center align-item-center midhead">
                        <strong>Search CSAT score </strong> {""} ={" "}
                        {this.state.tasks_positive > 0 &&
                          this.state.total_riview > 0 ? (
                            <div>
                              {(
                                (this.state.tasks_positive /
                                  this.state.total_riview) *
                                100
                              ).toFixed(2)}
                              %
                            </div>
                          ) : (
                            <div>0%</div>
                          )}
                      </div>
                      <Row>
                        <Col sm={12} md={2}></Col>
                        <Col sm={12} md={8}>
                          <Col sm={12} md={4}>
                            <div className="smilebox">
                              <img
                                src={require("../../../assets/images/smile-1.svg")}
                                alt=""
                              />
                              <h3>{this.state.tasks_positive}</h3>
                              {this.state.tasks_positive > 0 &&
                                this.state.total_riview > 0 ? (
                                  <span>
                                    {(
                                      (this.state.tasks_positive /
                                        this.state.total_riview) *
                                      100
                                    ).toFixed(2)}
                                    %
                                  </span>
                                ) : (
                                  <span>0%</span>
                                )}
                              <p>Satisfied Customer</p>
                            </div>
                          </Col>

                          <Col sm={12} md={4}>
                            <div className="smilebox">
                              <img
                                src={require("../../../assets/images/smile-2.svg")}
                                alt=""
                              />
                              <h3>{this.state.task_neutral}</h3>
                              {this.state.task_neutral > 0 &&
                                this.state.total_riview > 0 ? (
                                  <span>
                                    {(
                                      (this.state.task_neutral /
                                        this.state.total_riview) *
                                      100
                                    ).toFixed(2)}
                                    %
                                  </span>
                                ) : (
                                  <span>0%</span>
                                )}
                              <p>Neutral Customer</p>
                            </div>
                          </Col>

                          <Col sm={12} md={4}>
                            <div className="smilebox">
                              <img
                                src={require("../../../assets/images/smile-3.svg")}
                                alt=""
                              />
                              <h3>{this.state.task_negative}</h3>
                              {this.state.task_negative > 0 &&
                                this.state.total_riview > 0 ? (
                                  <span>
                                    {(
                                      (this.state.task_negative /
                                        this.state.total_riview) *
                                      100
                                    ).toFixed(2)}
                                    %
                                  </span>
                                ) : (
                                  <span>0%</span>
                                )}
                              <p>Unsatisfied Customer</p>
                            </div>
                          </Col>
                        </Col>
                        <Col sm={12} md={2}></Col>
                      </Row>

                      <div className="ratingtabl">
                        <div className="bggrey">
                          <span className="greenclr m-r-10">5 Satisfied</span>
                          <span className="orangeclr m-r-10">4 Neutral</span>
                          <span className="redclr">3 Unsatisfied</span>
                        </div>
                      </div>

                      <div className="ratingtabl">
                        <Table striped bordered hover>
                          <thead>
                            <tr>
                              <th>Unsatisfied</th>
                              <th>Neutral</th>
                              <th>Satisfied</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td className="redclr">
                                <strong>
                                  {" "}
                                  <span className="mr-10"> 1 </span>
                                  <span className="mr-10"> 2 </span>
                                  <span className="mr-10"> 3 </span>
                                </strong>
                              </td>

                              <td className="orangeclr">
                                <strong> 4 </strong>
                              </td>

                              <td className="greenclr">
                                <strong> 5</strong>
                              </td>
                            </tr>
                          </tbody>
                        </Table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <div className="pd-15">
                    <div className="chartbg">
                      <div className="d-flex justify-content-center align-item-center midhead">
                        <strong> Application CSAT score </strong> {""} ={" "}
                        {this.state.agent_tasks_positive > 0 &&
                          this.state.agent_total_riview > 0 ? (
                            <div>
                              {(
                                (this.state.agent_tasks_positive /
                                  this.state.agent_total_riview) *
                                100
                              ).toFixed(2)}
                              %
                            </div>
                          ) : (
                            <div>0%</div>
                          )}
                      </div>
                      <Row>
                        <Col sm={12} md={2}></Col>
                        <Col sm={12} md={8}>
                          <Col sm={12} md={4}>
                            <div className="smilebox">
                              <img
                                src={require("../../../assets/images/smile-1.svg")}
                                alt=""
                              />
                              <h3>{this.state.agent_tasks_positive}</h3>
                              {this.state.agent_tasks_positive > 0 &&
                                this.state.agent_total_riview > 0 ? (
                                  <span>
                                    {(
                                      (this.state.agent_tasks_positive /
                                        this.state.agent_total_riview) *
                                      100
                                    ).toFixed(2)}
                                    %
                                  </span>
                                ) : (
                                  <span>0%</span>
                                )}
                              <p>Satisfied Customer</p>
                            </div>
                          </Col>

                          <Col sm={12} md={4}>
                            <div className="smilebox">
                              <img
                                src={require("../../../assets/images/smile-2.svg")}
                                alt=""
                              />
                              <h3>{this.state.agent_task_neutral}</h3>
                              {this.state.agent_task_neutral > 0 &&
                                this.state.agent_total_riview > 0 ? (
                                  <span>
                                    {(
                                      (this.state.agent_task_neutral /
                                        this.state.agent_total_riview) *
                                      100
                                    ).toFixed(2)}
                                    %
                                  </span>
                                ) : (
                                  <span>0%</span>
                                )}
                              <p>Neutral Customer</p>
                            </div>
                          </Col>

                          <Col sm={12} md={4}>
                            <div className="smilebox">
                              <img
                                src={require("../../../assets/images/smile-3.svg")}
                                alt=""
                              />
                              <h3>{this.state.agent_task_negative}</h3>
                              {this.state.agent_task_negative > 0 &&
                                this.state.agent_total_riview > 0 ? (
                                  <span>
                                    {(
                                      (this.state.agent_task_negative /
                                        this.state.agent_total_riview) *
                                      100
                                    ).toFixed(2)}
                                    %
                                  </span>
                                ) : (
                                  <span>0%</span>
                                )}
                              <p>Unsatisfied Customer</p>
                            </div>
                          </Col>
                        </Col>
                        <Col sm={12} md={2}></Col>
                      </Row>

                      <div className="ratingtabl">
                        <div className="bggrey">
                          <span className="greenclr m-r-10">5 Satisfied</span>
                          <span className="orangeclr m-r-10">4 Neutral</span>
                          <span className="redclr">3 Unsatisfied</span>
                        </div>
                      </div>

                      <div className="ratingtabl">
                        <Table striped bordered hover>
                          <thead>
                            <tr>
                              <th>Unsatisfied</th>
                              <th>Neutral</th>
                              <th>Satisfied</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td className="redclr">
                                <strong>
                                  {" "}
                                  <span className="mr-10"> 1 </span>
                                  <span className="mr-10"> 2 </span>
                                  <span className="mr-10"> 3 </span>
                                </strong>
                              </td>

                              <td className="orangeclr">
                                <strong> 4 </strong>
                              </td>

                              <td className="greenclr">
                                <strong> 5</strong>
                              </td>
                            </tr>
                          </tbody>
                        </Table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <div className="pd-15">
              <section className="content">
                <div className="box">
                  <div className="box-body">
                    <div className="row">
                      <div className="col-xs-12">
                        <div className="nav-tabs-custom">
                          <ul className="nav nav-tabs">
                            <li className="active" onClick={(e) => this.handleTabs(e)} id="tab_1" >SEARCH</li>
                            <li onClick={(e) => this.handleTabs(e)} id="tab_2">APPLICATION</li>
                          </ul>

                          <div className="tab-content">

                            <div className="tab-pane active" id="show_tab_1">

                              {this.state.task_ratings_product && this.state.task_ratings_product.length > 0 && <ProductRatingTable tableData={this.state.task_ratings_product} count_product={this.state.count_product} search_rating_score={this.state.search_rating_score} search_type={this.state.search_type} search_compid={this.state.search_compid} search_cid={this.state.search_cid} />}

                              {this.state.task_ratings_product && this.state.task_ratings_product.length === 0 && <div className="noData">No Data Found</div>}

                            </div>

                            <div className="tab-pane" id="show_tab_2">

                              {this.state.task_ratings && this.state.task_ratings.length > 0 && <RatingTable tableData={this.state.task_ratings} count={this.state.count} search_rating_score={this.state.search_rating_score} search_type={this.state.search_type} search_compid={this.state.search_compid} search_cid={this.state.search_cid} />}

                              {this.state.task_ratings && this.state.task_ratings.length === 0 && <div className="noData">No Data Found</div>}
                            </div>

                          </div>
                        </div>
                      </div>
                    </div>
                    {/* <BootstrapTable data={this.state.task_ratings}>
                      <TableHeaderColumn
                        isKey
                        dataField="rated_by"
                        dataFormat={setSubmittedBy(this)}
                      >
                        Submitted By
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        dataField="company_name"
                        dataFormat={setCompany(this)}
                      >
                        Company Name
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        dataField="cust_name"
                        dataFormat={setCustomer(this)}
                      >
                        User Name
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        dataField="rating"
                        dataFormat={setRating(this)}
                      >
                        Rating
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        dataField="task_id"
                        dataFormat={actionFormatter(this)}
                      >
                        View
                      </TableHeaderColumn>
                    </BootstrapTable> */}


                    {/* <Row>
                        <Col md={12}>
                          <div className="paginationOuter text-right">
                            <Pagination
                              activePage={this.state.activePage}
                              itemsCountPerPage={this.state.itemPerPage}
                              totalItemsCount={this.state.count}
                              itemClass="nav-item"
                              linkClass="nav-link"
                              activeClass="active"
                              onChange={this.handlePageChange}
                            />
                          </div>
                        </Col>
                      </Row> */}

                    {/* <Modal
                      show={this.state.showModal}
                      onHide={() => this.modalCloseHandler()}
                      backdrop="static"
                      className="feedbckProv"
                    >
                      <Formik>
                        {({ }) => {
                          return (
                            <Form>
                              <Modal.Header>
                                <Modal.Title>Feedback provided</Modal.Title>
                              </Modal.Header>
                              <Modal.Body>
                                <div className="contBox">
                                  {this.state.rating_stars &&
                                    this.state.rating_stars != null ? (
                                      <Row>
                                        <Col xs={12} sm={12} md={12}>
                                          <div className="form-group">
                                            <h2> Rating</h2>
                                            <div className="ratinfo starimg">
                                              <span>
                                                {" "}
                                                <Rating
                                                  initialRating={
                                                    this.state.rating_stars
                                                  }
                                                  readonly
                                                  emptySymbol={
                                                    <img
                                                      src={require("../../../assets/images/uncheck-star.svg")}
                                                      alt=""
                                                      className="icon"
                                                    />
                                                  }
                                                  placeholderSymbol={
                                                    <img
                                                      src={require("../../../assets/images/uncheck-stars.svg")}
                                                      alt=""
                                                      className="icon"
                                                    />
                                                  }
                                                  fullSymbol={
                                                    <img
                                                      src={require("../../../assets/images/uncheck-stars.svg")}
                                                      alt=""
                                                      className="icon"
                                                    />
                                                  }
                                                />
                                              </span>
                                            </div>
                                          </div>
                                        </Col>
                                      </Row>
                                    ) : null}

                                  <div className="opsn">
                                    {this.state.taskOptionArr &&
                                      this.state.taskOptionArr != "" ? (
                                        <div>
                                          <p>Options selected</p>
                                          {this.state.taskOptionArr.map(
                                            (option, i) => {
                                              return (
                                                <Row>
                                                  <Col xs={12} sm={12} md={12}>
                                                    <div className="form-group">
                                                      <div className="checkboxfeedbck">
                                                        <input
                                                          type="checkbox"
                                                          checked="checked"
                                                        />
                                                        <span> {option}</span>
                                                      </div>
                                                    </div>
                                                  </Col>
                                                </Row>
                                              );
                                            }
                                          )}
                                        </div>
                                      ) : null}
                                    {this.state.comment &&
                                      this.state.comment != null ? (
                                        <Row>
                                          <Col xs={12} sm={12} md={12}>
                                            <div className="form-group">
                                              <label>Comment</label>
                                              <span> {this.state.comment}</span>
                                            </div>
                                          </Col>
                                        </Row>
                                      ) : null}
                                  </div>
                                </div>
                              </Modal.Body>
                              <Modal.Footer>
                                <button
                                  onClick={(e) => this.modalCloseHandler()}
                                  className={`btn btn-danger btn-sm`}
                                  type="button"
                                >
                                  Close
                                </button>
                              </Modal.Footer>
                            </Form>
                          );
                        }}
                      </Formik>
                    </Modal> */}
                  </div>
                </div>
              </section>
            </div>
          </div>
        </Layout>
      );
    }
  }
}
export default TaskRatings;
