import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { getMyDrupalId,localDate } from '../../shared/helper';
import base64 from 'base-64';
import {
  Row,
  Col,
  Tooltip,
  OverlayTrigger
} from "react-bootstrap";
import "./Dashboard.css";

import dateFormat from "dateformat";

import StatusColumn from "./StatusColumn";
import SubTaskTable from "./SubTaskTable";

//import TaskDetails from './TaskDetails';

import API from "../../shared/axios";
import swal from "sweetalert";

import { Link } from "react-router-dom";
import Pagination from "react-js-pagination";
import {showErrorMessage} from "../../shared/handle_error";

const portal_url = `${process.env.REACT_APP_PORTAL_URL}`; // SATYAJIT

const priority_arr = [
  { priority_id: 1, priority_value: "Low" },
  { priority_id: 2, priority_value: "Medium" },
  { priority_id: 3, priority_value: "High" }
];


const getStatusColumn = refObj => (cell, row) => {
  return <StatusColumn rowData={row} />;
};

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const setDescription = refOBj => (cell, row) => {

  if(row.parent_id > 0){
    return (
        <LinkWithTooltip
            tooltip={`${row.title}`}
            href="#"
            id="tooltip-1"
            clicked={e => refOBj.checkHandler(e)}
          >
          {row.title}
        </LinkWithTooltip>
    );
  }else{
    return (      
      <LinkWithTooltip
        tooltip={`${row.req_name}`}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {row.req_name}
      </LinkWithTooltip>
      );
  }
};


const setCreateDate = refObj => cell => {
  var date = localDate(cell);
  return dateFormat(date, "dd/mm/yyyy");
};

const setDaysPending = refObj => (cell, row) => {
  var date = localDate(cell);
  return dateFormat(date, "dd/mm/yyyy");
};

const clickToShowTasks = refObj => (cell, row) => {
  return (<LinkWithTooltip tooltip={`${cell}`} href="#" id="tooltip-1" clicked={e => refObj.checkHandler(e)}>{cell}</LinkWithTooltip>);
};

const priorityEditor = refObj => (onUpdate, props) => {
  ///console.log(props.row.priority);
  //refObj.setState({ updt_priority:props.row.priority,updt_priority_value:props.row.priority_value });
  return (
    <>
      <select
        value={
          refObj.state.updt_priority > 0
            ? refObj.state.updt_priority
            : props.row.priority
        }
        onChange={ev => {
          refObj.setState({ updt_priority: ev.currentTarget.value });
        }}
      >
        {priority_arr.map(priority => (
          <option key={priority.priority_id} value={priority.priority_id}>
            {priority.priority_value}
          </option>
        ))}
      </select>
      <button
        className="btn btn-info btn-xs textarea-save-btn"
        onClick={() => {
          //AXIOS CALL TO UPDATE THE PRIORITY

          props.row.priority = refObj.state.updt_priority;
          onUpdate(refObj.state.updt_priority);
        }}
      >
        save
      </button>
    </>
  );
};

const setCustomerName = refObj => (cell, row) => {
  if (row.customer_drupal_id > 0) {
    //hard coded customer id - SATYAJIT
    //row.customer_id = 2;
    return (
      /*<Link
        to={{
          pathname:
            portal_url + "customer-dashboard/" +
            row.customer_drupal_id
        }}
        target="_blank"
        style={{ cursor: "pointer" }}
      >*/
        <LinkWithTooltip
            tooltip={`${row.first_name + " " + row.last_name}`}
            href="#"
            id="tooltip-1"
            clicked={e => refObj.checkHandler(e)}
          >
          {row.first_name + " " + row.last_name}
        </LinkWithTooltip>        
      /*</Link>*/
    );
  } else {
    return "";
  }
};

const setPriorityName = refObj => (cell, row) => {
  var ret = "Set Priority";
  for (let index = 0; index < priority_arr.length; index++) {
    const element = priority_arr[index];
    if (element["priority_id"] === cell) {
      ret = element["priority_value"];
    }
  }

  return ret;
};

const setAssignedTo = refOBj => (cell, row) => {
  return (    
    <LinkWithTooltip
        tooltip={`${row.emp_first_name + " " + row.emp_last_name + " (" + row.desig_name + ")"}`}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
        {row.emp_first_name + " " + row.emp_last_name + " (" + row.desig_name + ")"}
    </LinkWithTooltip>
  );
};

class MyTeamOpenTasksTable extends Component {
  state = {
    showCreateSubTask: false,
    showAssign: false,
    showRespondBack: false,
    showTaskDetails: false,
    showRespondCustomer: false,
    showAuthorizeBack: false,
    changeData: true,
    task_id: 0,
    tableData: [],
        
    activePage: 1,
    totalCount: 0,
    itemPerPage: 20,
  };

  checkHandler = (event) => {
    event.preventDefault();
  };
  
  redirectUrl = (event, id) => {
    event.preventDefault();
    //http://reddy.indusnet.cloud/customer-dashboard?source=Mi02NTE=
    var emp_drupal_id = getMyDrupalId(localStorage.token);
    var base_encode   = base64.encode(`${id}-${emp_drupal_id}`); 
    window.open( portal_url + "customer-dashboard?source="+base_encode, '_blank');
  };

  taskDetails = row => {
    this.setState({ showTaskDetails: true, currRow: row });
  };

  showSubTaskPopup = currRow => {
    //console.log("Create Sub Task");
    this.setState({ showCreateSubTask: true, currRow: currRow });
  };

  showCloseTaskPopup = currRow => {
    //console.log("Close Task Initiated");
    swal({
      closeOnClickOutside: false,
      title: "Close task",
      text: "Are you sure you want to close this task?",
      icon: "warning",
      buttons: true,
      dangerMode: true
    }).then(willDelete => {
      if (willDelete) {
        API.delete(`/api/tasks/close/${currRow.task_id}`).then(res => {
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Task has been closed.",
            icon: "success"
          }).then(() => {
            //console.log("Task Closed");
            this.props.allTaskDetails();
          });
        });

        // swal("Task has been closed!", {
        //     icon: "success"
        // }).then(() => {
        //     console.log('Task Closed');
        //     console.log(this.state.tableData.length);
        //     currRow = null;
        //     console.log(currRow);
        //     console.log(this.props.tableData);
        //     for (let index = 0; index < this.props.tableData.length; index++) {
        //         const element = this.props.tableData[index];
        //         if(currRow.task_id === element.task_id){
        //             delete this.props.tableData[index];
        //         }
        //     }
        //     console.log(this.props.tableData);
        //     this.props.fetchMyTasks();
        // });
      }
    });
  };

  showAuthorizeTaskPopup = currRow => {
    //console.log("Authorize");
    this.setState({ showAuthorizeBack: true, currRow: currRow });
  };

  showAssignPopup = currRow => {
    //console.log("Assign Task");
    this.setState({ showAssign: true, currRow: currRow });
  };

  showRespondPopup = currRow => {
    //console.log("Respond");
    this.setState({ showRespondBack: true, currRow: currRow });
  };

  showRespondCustomerPopup = currRow => {
    //console.log("Respond Customer");
    this.setState({ showRespondCustomer: true, currRow: currRow });
  };

  componentDidMount() {
    //console.log(this.props.tableData);
    this.setState({
      tableData: this.props.tableData,
      countOpenTasks: this.props.countOpenTasks,
      options: {
        clearSearch: true,
        expandBy: "column",
        page: !this.state.start_page ? 1 : this.state.start_page, // which page you want to show as default
        sizePerPageList: [
          {
            text: "10",
            value: 10
          },
          {
            text: "20",
            value: 20
          },
          {
            text: "All",
            value: !this.state.tableData ? 1 : this.state.tableData
          }
        ], // you can change the dropdown list for size per page
        sizePerPage: 10, // which size per page you want to locate as default
        pageStartIndex: 1, // where to start counting the pages
        paginationSize: 3, // the pagination bar size.
        prePage: "‹", // Previous page button text
        nextPage: "›", // Next page button text
        firstPage: "«", // First page button text
        lastPage: "»", // Last page button text
        //paginationShowsTotal: this.renderShowsTotal,  // Accept bool or function
        paginationPosition: "bottom" // default is bottom, top and both is all available
        // hideSizePerPage: true > You can hide the dropdown for sizePerPage
        // alwaysShowAllBtns: true // Always show next and previous button
        // withFirstAndLast: false > Hide the going to First and Last page button
      },
      cellEditProp: {
        mode: "click",
        beforeSaveCell: this.onBeforeSetPriority, // a hook for before saving cell
        afterSaveCell: this.onAfterSetPriority // a hook for after saving cell
      }
    });
  }

  handlePageChange = (pageNumber) => {
      this.setState({ activePage: pageNumber });
      this.getMyTasks(pageNumber > 0 ? pageNumber  : 1);
  };

  getMyTasks(page = 1) {
    let url;
    if(this.props.queryString !== '' ){
      url = `/api/team/open?page=${page}&${this.props.queryString}`; 
    }else{
      url = `/api/team/open?page=${page}`;
    }
    API.get(url)
    .then(res => {
    this.setState({ 
        tableData: res.data.data, 
        countOpenTasks: res.data.count_open_tasks});
    })
    .catch(err => { 
        showErrorMessage(err,this.props);
    });
  }

  onBeforeSetPriority = (row, cellName, cellValue) => {};

  onAfterSetPriority = (row, cellName, cellValue) => {
    this.setState({ updt_priority: 0 });
  };

  getSubTasks = row => {
    return (
      <SubTaskTable
        tableData={row.sub_tasks}
        dashType={this.props.dashType}
        reloadTaskSubTask={() => this.props.allTaskDetails()}
      />
    );
  };

  checkSubTasks = row => {
    //console.log("subtask")

    if (typeof row.sub_tasks !== "undefined" && row.sub_tasks.length > 0) {
      return true;
    } else {
      return false;
    }
  };

  refreshTableStatus = currRow => {
    currRow.status = 2;
  };

  handleClose = closeObj => {
    this.setState(closeObj);
  };

  tdClassName = (fieldValue, row) => {
    var dynamicClass = "width-150 ";
    if (row.vip_customer === 1) {
      dynamicClass += "bookmarked-column ";
    }
    // if(row.sub_tasks.length > 0 && typeof row.sub_tasks !== 'undefined' && typeof row.bookmarked !== 'undefined' ){
    //     return 'sub-task-column bookmarked-column';
    // }else if( typeof row.bookmarked !== 'undefined' ){
    //     return 'bookmarked-column';
    // }else if(row.sub_tasks.length > 0 && typeof row.sub_tasks !== 'undefined'){
    //     return 'sub-task-column';
    // }else{
    //     return '';
    // }
    return dynamicClass;
  };

  trClassName = (row, rowIndex) => {
    var ret = " ";
    var selDueDate = row.due_date;
    var dueDate = localDate(selDueDate);
    var today = new Date();
    var timeDiff = dueDate.getTime() - today.getTime();

    if (timeDiff > 0) {
    } else {
      if (row.vip_customer === 1) {
        ret += "tr-red";
      }
    }

    return ret;
  };

  expandColumnComponent({ isExpandableRow, isExpanded }) {
    let content = "";

    if (isExpandableRow) {
      content = isExpanded ? "-" : "+";
    } else {
      content = " ";
    }
    return <div> {content} </div>;
  }

  render() {
    /* const paginationOptions = {
      page: 1, // which page you want to show as default
      sizePerPageList: [
        {
          text: "10",
          value: 10
        },
        {
          text: "20",
          value: 20
        },
        {
          text: "All",
          value: this.props.tableData.length > 0 ? this.props.tableData.length : 1
        }
      ], // you can change the dropdown list for size per page
      sizePerPage: 10, // which size per page you want to locate as default
      pageStartIndex: 1, // where to start counting the pages
      paginationSize: 3, // the pagination bar size.
      prePage: '‹', // Previous page button text
      nextPage: '›', // Next page button text
      firstPage: '«', // First page button text
      lastPage: '»', // Last page button text
      //paginationShowsTotal: this.renderShowsTotal, // Accept bool or function
      paginationPosition: "bottom" // default is bottom, top and both is all available
      // hideSizePerPage: true //> You can hide the dropdown for sizePerPage
      // alwaysShowAllBtns: true // Always show next and previous button
      // withFirstAndLast: false //> Hide the going to First and Last page button
    }; */
    return (
      <>
        {this.state.changeData && (
          <BootstrapTable
            data={this.state.tableData}
            /* options={paginationOptions}
            pagination */
            expandableRow={this.checkSubTasks}
            expandComponent={this.getSubTasks}
            expandColumnOptions={{
              expandColumnVisible: true,
              expandColumnComponent: this.expandColumnComponent,
              columnWidth: 25
            }}
            trClassName={this.trClassName}
            cellEdit={this.state.cellEditProp}
          >
            <TableHeaderColumn
              isKey
              dataField="task_ref"
              //dataSort={true}
              columnClassName={this.tdClassName}
              editable={false}
              expandable={false}
              dataFormat={clickToShowTasks(this)}
            >
              Tasks
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="request_type"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={setDescription(this)}
            >
              Description
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="date_added"
              //dataSort={true}
              dataFormat={setCreateDate(this)}
              editable={false}
              expandable={false}
            >
              Created
            </TableHeaderColumn>

            {/* <TableHeaderColumn dataField='rdd' dataSort={ true } dataFormat={ setCreateDate(this) } editable={ false } expandable={ false }>RDD</TableHeaderColumn> */}

            <TableHeaderColumn
              dataField="due_date"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={setDaysPending(this)}
            >
              Due Date
            </TableHeaderColumn>

            {/* <TableHeaderColumn dataField='assigned_to' dataSort={ true } editable={ false } expandable={ false } >Assigned To</TableHeaderColumn> */}

            <TableHeaderColumn
              dataField="dept_name"
              //dataSort={true}
              editable={false}
              expandable={false}
            >
              Department
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="dept_name"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={setAssignedTo(this)}
            >
              Assigned To{" "}
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="priority"
              //dataSort={true}
              expandable={false}
              customEditor={{ getElement: priorityEditor(this) }}
              dataFormat={setPriorityName(this)}
            >
              Priority
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="cust_name"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={setCustomerName(this)}
            >
              Customer Name
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="status"
              dataFormat={getStatusColumn(this)}
              expandable={false}
              editable={false}
            >
              Status
            </TableHeaderColumn>

          </BootstrapTable>
        )}
        {this.state.countOpenTasks > 20 ? (
          <Row>
              <Col md={12}>
              <div className="paginationOuter text-right">
                  <Pagination
                  activePage={this.state.activePage}
                  itemsCountPerPage={this.state.itemPerPage}
                  totalItemsCount={this.state.countOpenTasks}
                  itemClass='nav-item'
                  linkClass='nav-link'
                  activeClass='active'
                  onChange={this.handlePageChange}
                  />
              </div>
              </Col>
          </Row>
          ) : null}
      </>
    );
  }
}

export default MyTeamOpenTasksTable;
