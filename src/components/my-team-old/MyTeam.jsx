

import React, { Component } from 'react';

import MyTeamOpenTasksTable from './MyTeamOpenTasksTable';
import MyTeamCloseTasksTable from './MyTeamCloseTasksTable';
import MyTeamAllocatedTasksTable from './MyTeamAllocatedTasksTable';


//import './Dashboard.css';

import API from "../../shared/axios";

import whitelogo from '../../assets/images/drreddylogo_white.png';
import MyTeamSearch from './MyTeamSearch';
import { Link } from 'react-router-dom';

class BMOverview extends Component {

    state = {
        openTasks :[],
        closedTasks : [],  
        isLoading : true,
        showCreateTasks:false,
        emp_id:0,
        view_employee_list:[]
    }

    componentDidMount(){
        this.allTaskDetails();        
    }

    allTaskDetails = (page = 1) => {
       
        this.setState({
            openTasks       : [],
            closedTasks     : [],
            queryString     : "",
            countOpenTasks  : 0, 
            countClosedTasks: 0,
            isLoading       : true
        })
        
        API.get( `/api/team/open?page=${page}` ).then(res => {
            if(res.data.count_open_tasks === undefined){
                this.setState({countOpenTasks: '0'});
            }else{
                this.setState({countOpenTasks: res.data.count_open_tasks});
            }
            this.setState({ openTasks: res.data.data});
            API.get( `/api/team/closed?page=${page}` ).then(res => {
                if(res.data.count_closed_tasks === undefined){
                    this.setState({countClosedTasks: '0'})
                }else{
                    this.setState({countClosedTasks: res.data.count_closed_tasks})
                }
                this.setState({ closedTasks: res.data.data});
                API.get( `/api/team/allocated?page=${page}` ).then(res => {
                    if(res.data.count_all_allocated_tasks === undefined){
                        this.setState({countAllocatedTasks: '0'})
                    }else{
                        this.setState({countAllocatedTasks: res.data.count_all_allocated_tasks})
                    }
                    this.setState({ allocatedTasks: res.data.data});
                    API.get(`/api/team/my_team_members`).then(res => {
                        this.setState({ view_employee_list : res.data.data, isLoading:false });
                    }).catch(err => {});
                }).catch(err => {});
            }).catch(err => {});
        }).catch(err => {});
    }

    updateState = (arr) => {
        if(arr !== undefined){

            this.setState({
                openTasks          : [],
                closedTasks        : [],
                allocatedTasks     : [],
                queryString        : "",
                countOpenTasks     : 0,
                countAllocatedTasks: 0,
                countClosedTasks   : 0
            });

            this.setState({
                openTasks          : arr[0].openTasks,
                countOpenTasks     : arr[1].countOpenTasks,
                closedTasks        : arr[2].closedTasks,
                countClosedTasks   : arr[3].countClosedTasks,
                allocatedTasks     : arr[4].allocatedTasks,
                countAllocatedTasks: arr[5].countAllocatedTasks,
                queryString        : arr[6].queryString
            });

        }
    }

    handleTabs = (event) =>{
        
        if(event.currentTarget.className === "active" ){
            //DO NOTHING
        }else{

            var elems          = document.querySelectorAll('[id^="tab_"]');
            var elemsContainer = document.querySelectorAll('[id^="show_tab_"]');
            var currId         = event.currentTarget.id;

            for (var i = 0; i < elems.length; i++){
                elems[i].classList.remove('active');
            }

            for (var j = 0; j < elemsContainer.length; j++){
                elemsContainer[j].style.display = 'none';
            }

            event.currentTarget.classList.add('active');
            event.currentTarget.classList.add('active');
            document.querySelector('#show_'+currId).style.display = 'block';
        }
        // this.tabElem.addEventListener("click",function(event){
        //     alert(event.target);
        // }, false);
    }

    showCreateTaskPopup = () => {
      this.setState({showCreateTasks:true});
    }

    handleClose = (closeObj) => {
      this.setState(closeObj);
    }

    render(){      
        
        if(this.state.isLoading){
            return(
                <>
                    <div className="loderOuter">
                    <div className="loading_reddy_outer">
                        <div className="loading_reddy" >
                            <img src={whitelogo} alt="logo" />
                        </div>
                        </div>
                    </div>
                </>
            );
        }else{
            return(
                <>
                    <div className="content-wrapper">
                        <section className="content-header heading-side-by-side">
                            <h1>My Team
                            {this.state.view_employee_list.length > 0 ? (
                                 <small>
                                <Link to='/user/view_team'>
                                   View Team Members
                                </Link>
                                </small>
                            ) : null} 
                            </h1>                           
                        </section>
                        <section className="content">
                            <div className="clearfix serchapanel">
                                <MyTeamSearch changeState={this.updateState} />
                            </div>
                            <div className="row">
                                <div className="col-xs-12">
                                    <div className="nav-tabs-custom">
                                        <ul className="nav nav-tabs">
                                            <li className="active" onClick = {(e) => this.handleTabs(e)} id="tab_1" >OPEN TASKS ({this.state.countOpenTasks}) </li>
                                            <li onClick = {(e) => this.handleTabs(e)}  id="tab_2">ALLOCATED TASKS ({this.state.countAllocatedTasks})</li>
                                            <li onClick = {(e) => this.handleTabs(e)}  id="tab_3">CLOSED TASKS ({this.state.countClosedTasks})</li>
                                        </ul>

                                        <div className="tab-content">

                                            <div className="tab-pane active" id="show_tab_1">
                                                
                                                {this.state.openTasks && this.state.openTasks.length > 0 && <MyTeamOpenTasksTable dashType="BM" tableData={this.state.openTasks} countOpenTasks={this.state.countOpenTasks} queryString={this.state.queryString} allTaskDetails={(e) => this.allTaskDetails()} />}
                                                {this.state.openTasks && this.state.openTasks.length === 0 && <div className="noData">No Data Found</div>}
                                            </div>

                                            <div className="tab-pane" id="show_tab_2">
                                                {this.state.allocatedTasks && this.state.allocatedTasks.length > 0 && <MyTeamAllocatedTasksTable dashType="BM" tableData={this.state.allocatedTasks} countAllocatedTasks={this.state.countAllocatedTasks} queryString={this.state.queryString}   allTaskDetails={(e) => this.allTaskDetails()} />}
                                                {this.state.allocatedTasks && this.state.allocatedTasks.length === 0 && <div className="noData">No Data Found</div>}
                                            </div>

                                            <div className="tab-pane" id="show_tab_3">
                                                {this.state.closedTasks && this.state.closedTasks.length > 0 && <MyTeamCloseTasksTable dashType="BM" tableData={this.state.closedTasks} countClosedTasks={this.state.countClosedTasks}queryString={this.state.queryString}   allTaskDetails={(e) => this.allTaskDetails()} />}
                                                {this.state.closedTasks && this.state.closedTasks.length === 0 && <div className="noData">No Data Found</div>}
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </>
            );
        }
    }
}

export default BMOverview;
