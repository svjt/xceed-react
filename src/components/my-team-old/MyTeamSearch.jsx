import React, { Component } from 'react';

import API from "../../shared/axios";

import whitelogo from '../../assets/images/drreddylogo_white.png';

import Select from "react-select";
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import dateFormat from "dateformat";
import { htmlDecode } from '../../shared/helper';

class BMDashboardSearch extends Component {

    state = {
      search_loader:false,
      my_customer_list: null,
      showCustomerBlock:false,
      sla_arr: [
        {value:1,label:'Due Today'},
        {value:3,label:'Due Tomorrow'},
        {value:4,label:' Due This Week'},
        {value:2,label:'Overdue'}
      ],
      params:[]
    }

    componentDidMount(){
      API.get(`/api/team/my_team_members`)
        .then(res => {
            var myEmployee = [];
            for (let index = 0; index < res.data.data.length; index++) {
                const element   = res.data.data[index];
                myEmployee.push({
                  value: element["employee_id"],
                  label: element["first_name"] +" "+ element["last_name"]
                });
            }

            this.setState({my_employee_list:myEmployee});

            API.get(`/api/feed/request_type`)
            .then(res => {
                var requestTypes = [];
                for (let index = 0; index < res.data.data.length; index++) {
                    const element   = res.data.data[index];
                    requestTypes.push({
                        value: element["type_id"],
                        label: element["req_name"]
                    });
                }

                this.setState({request_type:requestTypes});

                API.get(`/api/feed/designations`)
                .then(res => {
                    var desigTypes = [];
                    for (let index = 0; index < res.data.data.length; index++) {
                        const element   = res.data.data[index];
                        desigTypes.push({
                            value: element["desig_id"],
                            label: element["desig_name"]
                        });
                    }
                    this.setState({designations:desigTypes,is_loading:false});

                    API.get('/api/team/l1_companies')
                    .then(res => {
                    
                      var myCompany = [];
                      for (let index = 0; index < res.data.data.length; index++) {
                          const element = res.data.data[index];
                          myCompany.push({
                            value: element["company_id"],
                            label: htmlDecode(element["company_name"])
                          });
                      }

                      this.setState({company:myCompany});

                    })
                    .catch(err => {
                      console.log(err);
                    });


                })
                .catch(err => {
                    console.log(err);
                });

            })
            .catch(err => {
                console.log(err);
            });
        })
        .catch(err => {
            console.log(err);
        }); 
    }

    filterSearch = (event,nextEV) => {
      this.setState({
        search_loader:true
      });
      var currIndex = null;
      if(event == null){
        for (let index = 0; index < this.state.params.length; index++) {
          const element = this.state.params[index];
          
          if(element.name === nextEV.name){
            currIndex = index;
          }
          
        }
        if(currIndex !== null){
          this.state.params.splice(currIndex,1);
        }
      }else{        
        for (let index = 0; index < this.state.params.length; index++) {
          const element = this.state.params[index];
          if(element.name === nextEV.name){
            currIndex = index;
          }
        }

        if(currIndex == null){
          this.state.params.push({name:nextEV.name,val:event.value});
        }else{
          const myCurrIndex = this.state.params[currIndex];
          myCurrIndex.val = event.value;
        }
      }

      this.callApi();
    }

    filterSearchMulti = (event,nextEV) => {
      this.setState({
        search_loader:true
      });
      var currIndex = null;
      if(event.length == 0){
        for (let index = 0; index < this.state.params.length; index++) {
          const element = this.state.params[index];
          if(element.name === nextEV.name){
            currIndex = index;
          }
          
        }
        if(currIndex !== null){
          this.state.params.splice(currIndex,1);
        }
        this.companyCustomers(nextEV.name,'');
      }else{        
        for (let index = 0; index < this.state.params.length; index++) {
          const element = this.state.params[index];
          if(element.name === nextEV.name){
            currIndex = index;
          }
        }

        var multi_val = this.getMultiValueString(event);

        if(currIndex == null){
          this.state.params.push({name:nextEV.name,val:multi_val});
        }else{
          const myCurrIndex = this.state.params[currIndex];
          myCurrIndex.val   = multi_val;
        }
        this.companyCustomers(nextEV.name,multi_val);
      }

      this.callApi();
    }

    getMultiValueString = (selectedValues) => {
      var string = "";
      for (let index = 0; index < selectedValues.length; index++) {
        const element = selectedValues[index];
        string += `${element.value},` 
      }
      string = string.replace(/,+$/,'');
      return string;
    }    

    companyCustomers = (name,val) => {
      if(name === 'compid'){
        if(val === ''){
          var currIndex;
          for (let index = 0; index < this.state.params.length; index++) {
            const element = this.state.params[index];
            if(element.name === 'cid'){
              currIndex = index;
              break;
            }
          }
          this.state.params.splice(currIndex,1);
          this.setState({showCustomerBlock:false,my_customer_list:[]});
        }else{
          API.get(`/api/team/my_team_customers?compid=${val}`)
            .then(res => {
                var myCustomer = [];
                for (let index = 0; index < res.data.data.length; index++) {
                    const element   = res.data.data[index];
                    var keyCustomer;
                    if(element.vip_customer === 1){
                        keyCustomer= '*';
                    }else{
                        keyCustomer= '';
                    }
                    myCustomer.push({
                      value: element["customer_id"],
                      label: element["first_name"] +" "+ element["last_name"] +" "+keyCustomer
                    });
                }

                this.setState({showCustomerBlock:true,my_customer_list:myCustomer});

            })
            .catch(err => {
                console.log(err);
            }); 
        }
      }
    }

    filterDateFrom = (value) => {
      this.setState({date_from:value,search_loader:true});
      var currIndex = null;
      for (let index = 0; index < this.state.params.length; index++) {
        const element = this.state.params[index];
        if(element.name === 'date_from'){
          currIndex = index;
        }
      }

      if(currIndex == null){
        this.state.params.push({name:'date_from',val:dateFormat(value, "yyyy-mm-dd")});
      }else{
        const myCurrIndex = this.state.params[currIndex];
        myCurrIndex.val   = dateFormat(value, "yyyy-mm-dd");
      }

      this.callApi();
    }

    filterDateTo = (value) => {
      this.setState({date_to:value,search_loader:true});
      var currIndex = null;
      for (let index = 0; index < this.state.params.length; index++) {
        const element = this.state.params[index];
        if(element.name === 'date_to'){
          currIndex = index;
        }
      }

      if(currIndex == null){
        this.state.params.push({name:'date_to',val:dateFormat(value, "yyyy-mm-dd")});
      }else{
        const myCurrIndex = this.state.params[currIndex];
        myCurrIndex.val   = dateFormat(value, "yyyy-mm-dd");
      }

      this.callApi();
    }

    removeDateTo = () => {
      this.setState({date_to:null,search_loader:true});
      var currIndex = null;
      for (let index = 0; index < this.state.params.length; index++) {
        const element = this.state.params[index];
        
        if(element.name === 'date_to'){
          currIndex = index;
        }
        
      }
      if(currIndex !== null){
        this.state.params.splice(currIndex,1);
      }  
      
      this.callApi();
    }

    removeDateFrom = () => {
      this.setState({date_from:null,search_loader:true});
      var currIndex = null;
      for (let index = 0; index < this.state.params.length; index++) {
        const element = this.state.params[index];
        
        if(element.name === 'date_from'){
          currIndex = index;
        }
        
      }
      if(currIndex !== null){
        this.state.params.splice(currIndex,1);
      }  

      this.callApi();
    }

    callApi = () => {
        var retArr = [];
        var page   = 1;
        if(this.state.params.length > 0){
            var query_string = '';
            for (let index = 0; index < this.state.params.length; index++) {
              const element = this.state.params[index];
              query_string += `${element.name}=${element.val}&`;
            }
            query_string = query_string.substring(0,(query_string.length-1));      
            API.get( `/api/team/open?page=${page}&${query_string}` ).then(res => {
                retArr.push({ openTasks: res.data.data });
                if(res.data.count_open_tasks === undefined){
                    retArr.push({ countOpenTasks: 0 });
                }else{
                    retArr.push({ countOpenTasks: res.data.count_open_tasks });
                }

                API.get( `/api/team/closed?page=${page}&${query_string}` ).then(res => {
                    retArr.push({ closedTasks: res.data.data});
                    if(res.data.count_closed_tasks === undefined){
                        retArr.push({ countClosedTasks: 0});
                    }else{
                        retArr.push({ countClosedTasks: res.data.count_closed_tasks});
                    }

                    API.get( `/api/team/allocated?page=${page}&${query_string}` ).then(res => {
                        retArr.push({ allocatedTasks: res.data.data});
                        if(res.data.count_all_allocated_tasks === undefined){
                            retArr.push({countAllocatedTasks: '0'})
                        }else{
                            retArr.push({countAllocatedTasks: res.data.count_all_allocated_tasks})
                        }
                        retArr.push({ queryString: query_string});
                        this.setState({
                          search_loader:false
                        });
                        this.props.changeState(retArr);
                    }).catch(err => {});
                }).catch(err => {});
            }).catch(err => {});
        }else{
            API.get( `/api/team/open?page=${page}` ).then(res => {
                retArr.push({ openTasks: res.data.data });
                if(res.data.count_open_tasks === undefined){
                    retArr.push({ countOpenTasks: 0 });
                }else{
                    retArr.push({ countOpenTasks: res.data.count_open_tasks });
                }
                API.get( `/api/team/closed?page=${page}` ).then(res => {
                  retArr.push({ closedTasks: res.data.data});
                  if(res.data.count_closed_tasks === undefined){
                      retArr.push({ countClosedTasks: 0});
                  }else{
                      retArr.push({ countClosedTasks: res.data.count_closed_tasks});
                  }

                  API.get( `/api/team/allocated?page=${page}` ).then(res => {
                      retArr.push({ allocatedTasks: res.data.data});
                      if(res.data.count_all_allocated_tasks === undefined){
                          retArr.push({countAllocatedTasks: '0'})
                      }else{
                          retArr.push({countAllocatedTasks: res.data.count_all_allocated_tasks})
                      }
                      retArr.push({ queryString:""});
                      this.setState({
                        search_loader:false
                      });
                      this.props.changeState(retArr);
                  }).catch(err => {});
                }).catch(err => {});
            }).catch(err => {});
        }
    }

    render(){
        return(
            <>
            {this.state.search_loader === true && 
              <>
                  <div className="loderOuter">
                  <div className="loading_reddy_outer">
                      <div className="loading_reddy" >
                          <img src={whitelogo}  alt="logo" />
                      </div>
                      </div>
                  </div>
              </>
            }
            <ul>
              
              {this.state.request_type && <li>
                <div className="form-group  has-feedback">
                  <label>Request Type</label>
                  <Select
                      isMulti
                      className="basic-single"
                      classNamePrefix="select"
                      defaultValue={0}
                      isClearable={true}
                      isSearchable={true}
                      name="rid"
                      options={this.state.request_type}
                      onChange={this.filterSearchMulti}
                  />
                </div>
              </li>}

              {this.state.company && <li>
                <div className="form-group  has-feedback">
                  <label>Company</label>
                  <Select
                      isMulti
                      className="basic-single"
                      classNamePrefix="select"
                      defaultValue={0}
                      isClearable={true}
                      isSearchable={true}
                      name="compid"
                      options={this.state.company}
                      onChange={this.filterSearchMulti}
                  />
                </div>
              </li>}

              {this.state.showCustomerBlock && <li>
                <div className="form-group  has-feedback">
                  <label>Customer</label>
                  <Select
                      isMulti
                      className="basic-single"
                      classNamePrefix="select"
                      defaultValue={0}
                      isClearable={true}
                      isSearchable={true}
                      name="cid"
                      options={this.state.my_customer_list}
                      onChange={this.filterSearchMulti}
                  />
                </div>
              </li>}

              {this.state.my_employee_list && <li>
                <div className="form-group  has-feedback">
                  <label>Employees</label>
                  <Select
                      isMulti
                      className="basic-single"
                      classNamePrefix="select"
                      defaultValue={0}
                      isClearable={true}
                      isSearchable={true}
                      name="eid"
                      options={this.state.my_employee_list}
                      onChange={this.filterSearchMulti}
                  />
                </div>
              </li>}

              {this.state.designations && <li>
                <div className="form-group  has-feedback">
                  <label>Functions</label>
                  <Select
                      isMulti
                      className="basic-single"
                      classNamePrefix="select"
                      defaultValue={0}
                      isClearable={true}
                      isSearchable={true}
                      name="desig"
                      options={this.state.designations}
                      onChange={this.filterSearchMulti}
                  />
                </div>
              </li>}

              <li>
                <div className="form-group react-date-picker">
                  <label>Date From</label>
                  <div className="form-control">
                    <DatePicker
                        name={'date_from'}
                        className="borderNone"
                        dateFormat='dd/MM/yyyy'
                        autoComplete="off"
                        selected={this.state.date_from}
                        onChange={this.filterDateFrom} 
                    />
                    
                  </div>
                  {this.state.date_from && <button onClick={this.removeDateFrom} className="date-close">x</button>}
                </div>
              </li>

              <li>
                <div className="form-group react-date-picker">
                  <label>Date To</label>
                  <div className="form-control">
                    <DatePicker
                        name={'date_to'}
                        className="borderNone"
                        dateFormat='dd/MM/yyyy'
                        autoComplete="off"
                        selected={this.state.date_to}
                        onChange={this.filterDateTo} 
                    />
                  </div>
                  {this.state.date_to && <button onClick={this.removeDateTo} className="date-close">x</button>}
                </div>
              </li>

              {this.state.sla_arr && <li>
                <div className="form-group  has-feedback">
                  <label>Due On</label>
                  <Select
                      className="basic-single"
                      classNamePrefix="select"
                      defaultValue={0}
                      isClearable={true}
                      isSearchable={true}
                      name="sla"
                      options={this.state.sla_arr}
                      onChange={this.filterSearch}
                  />
                </div>
              </li>}

          </ul>
            </>
        );
    }
}

export default BMDashboardSearch;