import React, { Component } from 'react';
import { localDate } from '../../shared/helper';

//import {Tooltip,OverlayTrigger} from 'react-bootstrap';

class StatusColumn extends Component {


    isDue = () =>{

        var selDueDate = this.props.rowData.due_date;
        var dueDate    = localDate(selDueDate);
        var today      = new Date();
        var timeDiff   = dueDate.getTime() - today.getTime();

        if(timeDiff > 0 ){
            return false;
        }else{
            return true;
        }
    }

    render() {

        //STATUS    
        // var tick,in_progress,exclaim = false;

        // if(this.props.rowData.status === 3 || this.props.rowData.status === 4){
        //     tick = true;
        //     if(this.checkDueDate()){
        //         if(this.props.rowData.vip_customer === 1){
        
        //         }else{
        //             exclaim = true;
        //         }
        //     }
        //     if(this.props.rowData.status === 3){
        //         var tooltip = (
        //             <Tooltip id="tooltip">
        //                 Responded.
        //             </Tooltip>
        //         );
        //     }else if(this.props.rowData.status === 4){
        //         var tooltip = (
        //             <Tooltip id="tooltip">
        //                 Responded To Customer.
        //             </Tooltip>
        //         );
        //     }

            

        // }else{
        //     if(this.props.rowData.status === 2){
        //         in_progress = true;
        //         if(this.checkDueDate()){
        //             if(this.props.rowData.vip_customer === 1){
            
        //             }else{
        //                 exclaim = true;
        //             }
        //         }
        //     }else if(this.props.rowData.status === 1){
        //         if(this.checkDueDate()){
        //             if(this.props.rowData.vip_customer === 1){
            
        //             }else{
        //                 exclaim = true;
        //             }
        //         }
        //     }
        // }

        return (
            <>
                <div className="actionStyle">
                    {this.isDue() && <i>Due</i> }
                    {this.props.rowData.status === 1 && !this.isDue() && <i>Assigned</i> }
                    {this.props.rowData.status === 2 && !this.isDue() && <i>In Progress</i> }
                    {this.props.rowData.status === 3 && !this.isDue() && <i>Responded</i> }
                </div>
            </>
        );
    }
}

export default StatusColumn;
