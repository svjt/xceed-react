import React, { Component } from 'react';
import {BootstrapTable,TableHeaderColumn} from 'react-bootstrap-table';
import { getMyDrupalId,localDate } from '../../shared/helper';
import base64 from 'base-64';

import dateFormat from 'dateformat';

import StatusColumn from './StatusColumn';
import { Link } from 'react-router-dom';

import API from "../../shared/axios";
import swal from "sweetalert";
//import { getMyId } from '../../shared/helper';

import {
  Tooltip,
  OverlayTrigger
} from "react-bootstrap";
import './Dashboard.css';

const portal_url = `${process.env.REACT_APP_PORTAL_URL}`; // SATYAJIT

const priority_arr = [
    {priority_id:1,priority_value:'Low'},
    {priority_id:2,priority_value:'Medium'},
    {priority_id:3,priority_value:'High'}
];

const getStatusColumn = refObj => (cell,row) => {
    if(row.discussion !== 1){
        return <StatusColumn rowData={row} />
    }else{
        return "";
    }
}

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const setDescription = refOBj => (cell,row) =>{
    if(row.parent_id > 0){
        return (
            <LinkWithTooltip
                tooltip={`${row.title}`}
                href="#"
                id="tooltip-1"
                clicked={e => refOBj.checkHandler(e)}
              >
              {row.title}
            </LinkWithTooltip>
        );
    }else if(row.discussion === 1){
        return (
            <LinkWithTooltip
                tooltip={`${row.comment}`}
                href="#"
                id="tooltip-1"
                clicked={e => refOBj.checkHandler(e)}
              >
              {row.comment}
            </LinkWithTooltip>
        );
    }else{
        return (      
          <LinkWithTooltip
            tooltip={`${row.req_name}`}
            href="#"
            id="tooltip-1"
            clicked={e => refOBj.checkHandler(e)}
          >
            {row.req_name}
          </LinkWithTooltip>
          );
    }
}

const setCreateDate = refObj => cell =>{
    var date = localDate(cell);
    return dateFormat(date, "dd/mm/yyyy");
}

const setDaysPending = refObj => (cell,row) =>{
    if(row.discussion === 1){
        return '';
    }else{
        var date = localDate(cell);
        return dateFormat(date, "dd/mm/yyyy");
    }
}

const clickToShowTasks = refObj => (cell,row) =>{
    return <LinkWithTooltip tooltip={`${cell}`} href="#" id="tooltip-1" clicked={e => refObj.checkHandler(e)}>{cell}</LinkWithTooltip>
}

const setEmpCustName = refObj => (cell, row) => {
  if (row.customer_drupal_id > 0) {
    //hard coded customer id - SATYAJIT
    //row.customer_id = 2;
    return (
      /*<Link
        to={{
          pathname:
            portal_url + "customer-dashboard/" +
            row.customer_drupal_id
        }}
        target="_blank"
        style={{ cursor: "pointer" }}
      >*/
        <LinkWithTooltip
            tooltip={`${row.first_name + " " + row.last_name}`}
            href="#"
            id="tooltip-1"
            clicked={e => refObj.checkHandler(e)}
          >
          {row.first_name + " " + row.last_name}
        </LinkWithTooltip>        
      /*</Link>*/
    );
  } else {
    return "";
  }
};

/*const setEmpCustName = refObj => (cell,row) =>{
    if(row.customer_drupal_id > 0){
        //hard coded customer id - SATYAJIT
        //row.customer_id = 2;

        return <Link to={{ pathname: '/user/customer-dashboard/'+row.customer_drupal_id }} target="_blank" style={{cursor:'pointer'}}>{row.first_name+' '+row.last_name}</Link>
    }else{
        var my_id = getMyId();
        if(row.assigned_to === my_id){
            return '';
        }else{
            return row.emp_first_name+' '+row.emp_last_name+' ('+row.dept_name+')';
        }
    }
}*/

const setPriorityName = refObj => (cell,row) =>{
    if(row.discussion === 1){
        return '';
    }else{
        var ret = 'Set Priority';
        for (let index = 0; index < priority_arr.length; index++) {
            const element = priority_arr[index];
            if(element['priority_id'] === cell ){
                ret = element['priority_value'];
            }
        }

        return ret
    }

}

const setAssignedTo = refOBj => (cell,row) => {
    //return row.emp_first_name+' '+row.emp_last_name+' ('+row.desig_name+')';
    if(row.discussion === 1){
        return '';
    }else{
        return (      
            <LinkWithTooltip
              tooltip={`${row.emp_first_name+' '+row.emp_last_name+' ('+row.desig_name+')'}`}
              href="#"
              id="tooltip-1"
              clicked={e => refOBj.checkHandler(e)}
            >
              {row.emp_first_name+' '+row.emp_last_name+' ('+row.desig_name+')'}
            </LinkWithTooltip>
            );
    }
}

class SubTaskTable extends Component {

    state = {
        showCreateSubTask   : false,
        showAssign          : false,
        showReAssign        : false,
        showCQTReAssign     : false,
        showRespondBack     : false
    };

    checkHandler = (event) => {
        event.preventDefault();
    };

    redirectUrl = (event, id) => {
        event.preventDefault();
        //http://reddy.indusnet.cloud/customer-dashboard?source=Mi02NTE=
        var emp_drupal_id = getMyDrupalId(localStorage.token);
        var base_encode   = base64.encode(`${id}-${emp_drupal_id}`); 
        window.open( portal_url + "customer-dashboard?source="+base_encode, '_blank');
    };

    showCloseTaskPopup = (currRow) => {
        swal({
            closeOnClickOutside: false,
            title: "Close task",
            text: "Are you sure you want to close this task?",
            icon: "warning",
            buttons: true,
            dangerMode: true
        }).then(willDelete => {
            if (willDelete) {
                API.delete(`/api/tasks/close/${currRow.task_id}`).then(res => {
                    swal({
                        closeOnClickOutside: false,
                        title: "Success",
                        text: "Task has been closed.",
                        icon: "success"
                    }).then(() => {
                        this.props.reloadTaskSubTask();
                    });
                });
            }
        });
    }


    showSubTaskAssignPopup = (id) => {
        this.setState({showAssignSubTask:true,task_id:id});
    }

    poke = (id) => {
        console.log('poke subtask');
    }

    showRespondPopup = (currRow) => {
        console.log('Respond');
        this.setState({showRespondBack:true,currRow});
    }

    showAssignPopup = (currRow) => {
        console.log('Assign Task');
        this.setState({showAssign:true,currRow:currRow});
    }

    showReAssignPopup = (currRow) => {
        console.log('Re Assign Task');
        this.setState({showReAssign:true,currRow:currRow});
    }

    showReAssignCQTPopup = (currRow) => {
        console.log('Re Assign CQT Task', currRow);
        this.setState({showCQTReAssign:true,currRow:currRow});
    }

    showSubTaskRespondPopup = (id) => {
        this.setState({showRespondSubTask:true,task_id:id});
    }

    refreshTable = () => {
        this.props.refreshTable();
    }

    handleClose = (closeObj) => {
        this.setState(closeObj);
    }

    tdClassName = (fieldValue, row) =>{
        var dynamicClass = 'width-150 ';
        if(row.vip_customer === 1){
            dynamicClass += 'bookmarked-column ';
        }
        return dynamicClass;
    }

    render(){

        const selectRowProp = {
            bgColor       : '#fff8f6'
        };

        return (
            <>
                <BootstrapTable 
                    data={this.props.tableData} 
                    selectRow={ selectRowProp } 
                    tableHeaderClass={"col-hidden"} 
                    expandColumnOptions={ 
                        { 
                            expandColumnVisible: true,
                            expandColumnComponent: this.expandColumnComponent,
                            columnWidth: 25
                        } 
                    }
                    trClassName="tr-expandable" 
                >
                    
                    <TableHeaderColumn isKey dataField='task_ref' dataSort={ true } columnClassName={ this.tdClassName } editable={ false }  dataFormat={ clickToShowTasks(this) } >Tasks</TableHeaderColumn>

                    <TableHeaderColumn dataField='request_type' dataSort={ true } editable={ false }  dataFormat={ setDescription(this) } >Description</TableHeaderColumn>

                    <TableHeaderColumn dataField='date_added' dataSort={ true } dataFormat={ setCreateDate(this) } editable={ false } >Created</TableHeaderColumn>

                    {/* <TableHeaderColumn dataField='due_date' dataSort={ true } editable={ false }  dataFormat={ setCreateDate(this) } >Due Date</TableHeaderColumn> */}
                    
                    <TableHeaderColumn dataField='due_date' dataSort={ true } editable={ false }  dataFormat={ setDaysPending(this) } >Due Date</TableHeaderColumn>

                    {/* <TableHeaderColumn dataField='assigned_to' dataSort={ true } editable={ false }  >Assigned To</TableHeaderColumn> */}

                    <TableHeaderColumn dataField='dept_name' dataSort={ true } editable={ false } expandable={ false } >Department</TableHeaderColumn>

                    <TableHeaderColumn dataField='dept_name' dataSort={ true } editable={ false } expandable={ false } dataFormat={ setAssignedTo(this) } >Assigned To </TableHeaderColumn>

                    <TableHeaderColumn dataField='priority' dataSort={ true }  dataFormat={ setPriorityName(this) } >Priority</TableHeaderColumn>

                    <TableHeaderColumn dataField='cust_name' dataSort={ true } editable={ false }  dataFormat={ setEmpCustName(this) } >Customer Name</TableHeaderColumn>

                    {/* <TableHeaderColumn dataField='dept_name' dataSort={ true } editable={ false } expandable={ false } >Department</TableHeaderColumn> */}

                    <TableHeaderColumn dataField='status' dataFormat={ getStatusColumn(this) }  editable={ false } >Status</TableHeaderColumn>
                </BootstrapTable>
            </>
        );
    }

}

export default SubTaskTable;