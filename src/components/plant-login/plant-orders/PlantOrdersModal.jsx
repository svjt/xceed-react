import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Formik, Field, Form } from 'formik';
import { Row, Col, Modal, Button, ButtonToolbar } from 'react-bootstrap';
import Dropzone from 'react-dropzone';
import swal from 'sweetalert';
import * as Yup from 'yup';

import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

import axios from '../../../shared/axios_logistics';
import { trimString } from '../../../shared/helper';
import ModalLoader from '../../loader/ModalLoader';

var path = require('path');

let initialValues = {
    invoice_number: '',
    flight_booked: '',
    flight_booked_location: '',
    flight_booked_time: '',
    file_name: '',
};

const validationSchema = (refObj) =>
    Yup.object().shape({
        invoice_number: Yup.string()
            .matches(/^([0-9])*$/, 'Invoice Number should be numeric value!')
            .min(2, 'Invoice Number can be minimum 2 digits long')
            .max(15, 'Invoice Number can be maximum 15 digits long')
            .required('Please enter Invoice Number'),
        flight_booked: Yup.string()
            .matches(/^([0-9- ])*$/, 'Only numbers, spaces and "-" are allowed!')
            .min(2, 'Flight Booked Number can be minimum 2 digits long')
            .max(15, 'Flight Booked Number can be maximum 15 digits long')
            .required('Please enter Flight Booked Number'),
        flight_booked_location: Yup.string().required('Please enter Flight Booked Location'),
        flight_booked_time: Yup.string().required('Please Select Date'),
        file_name: Yup.string()
            .test('file_name', 'AWB doc is required. Only .pdf file is allowed', () => refObj.state.files.length > 0 || refObj.state.prevFilesHtml)
            .test('file_name', refObj.state.fileErrText, () => refObj.state.isValidFile)
            .test('filecount', 'Maximum 1 file is allowed', () => refObj.state.files.length <= 1),
    });

const removeDropZoneFiles = (fileName, objRef, setErrors, setFieldValue) => {
    var newArr = [];
    for (let index = 0; index < objRef.state.files.length; index++) {
        const element = objRef.state.files[index];

        if (fileName === element.name) {
        } else {
            newArr.push(element);
        }
    }

    var fileListHtml = newArr.map((file) => (
        <>
            <span onClick={() => removeDropZoneFiles(file.name, objRef, setErrors, setFieldValue)}>
                <i className='far fa-times-circle'></i>
            </span>{' '}
            {file.name}
        </>
    ));
    setErrors({ file_name: '' });
    setFieldValue('file_name', newArr);
    objRef.setState({
        isValidFile: true,
        files: newArr,
        filesHtml: fileListHtml,
    });
};

class PlantOrdersModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            files: [],
            filesHtml: '',
            prevFilesHtml: '',
            fileErrText: '',
            isValidFile: false,
            plantOrdersDetails: {},
        };
    }

    componentDidMount() {
        if (this.props.plantOrdersDetails && Object.keys(this.props.plantOrdersDetails).length > 0) {
            this.setState({ plantOrdersDetails: this.props.plantOrdersDetails });
            if(this.props.plantOrdersDetails.file_name){
                let prevFilesHtml =
                <>
                    <span
                        onClick={() => { this.setState({prevFilesHtml: ''})}}
                    >
                        <i className='far fa-times-circle'></i>
                    </span>{' '}
                    {trimString(25, this.props.plantOrdersDetails.file_name)}
                </>
                this.setState({
                    prevFilesHtml: prevFilesHtml,
                });
            }
        }
    }

    handleSubmitEvent = (values, actions) => {
        const { plantOrdersDetails } = this.state;

        this.setState({ isLoading: true });

        let formData = new FormData();
        formData.append('invoice_number', values.invoice_number);
        formData.append('flight_booked', values.flight_booked);
        formData.append('flight_booked_location', values.flight_booked_location);
        formData.append('flight_booked_time', values.flight_booked_time);

        if (this.state.files && this.state.files.length > 0) {
            for (let index = 0; index < this.state.files.length; index++) {
                const element = this.state.files[index];
                formData.append('file', element);
            }
        } else {
            formData.append('file', []);
        }

        if (plantOrdersDetails && Object.keys(plantOrdersDetails).length > 0) {
            // Update Old Invoice
            axios
                .put(`/api/tasks/logistics_awb_form/${values.invoice_number}`, formData)
                .then((res) => {
                    actions.resetForm();
                    this.setState({ isLoading: false });
                    this.props.modalCloseHandler();
                    swal({
                        closeOnClickOutside: false,
                        title: 'Success',
                        text: 'Data updated Successfully',
                        icon: 'success',
                    });
                })
                .catch((err) => {
                    this.setState({ isLoading: false });
                    actions.setSubmitting(false);
                    if (err.data && err.data.errors) {
                        actions.setErrors(err.data.errors);
                    } else if (err.data && err.data.message) {
                        actions.setErrors({ message: err.data.message });
                    }
                });
        } else {
            // Add New Invoice
            axios
                .post('/api/tasks/logistics_awb_form', formData)
                .then((res) => {
                    actions.resetForm();
                    this.setState({ isLoading: false });
                    this.props.modalCloseHandler();
                    swal({
                        closeOnClickOutside: false,
                        title: 'Success',
                        text: 'Data Added Successfully',
                        icon: 'success',
                    });
                })
                .catch((err) => {
                    this.setState({ isLoading: false });
                    actions.setSubmitting(false);
                    if (err.data && err.data.errors) {
                        actions.setErrors(err.data.errors);
                    } else if (err.data && err.data.message) {
                        actions.setErrors({ message: err.data.message });
                    }
                });
        }
    };

    fileChangedHandler = (event) => {
        this.setState({ upload_file: event.target.files[0] });
    };

    setDropZoneFiles = (acceptedFiles, setErrors, setFieldValue, setFieldTouched) => {
        var rejectedFiles = [];
        var uploadFile = [];

        setErrors({ file_name: false });
        setFieldValue(this.state.files);

        // var prevFiles = this.state.files;
        var newFiles = acceptedFiles;

        // uncomment below code if multi file option is required
        // if (prevFiles.length > 0) {
        //     for (let index = 0; index < acceptedFiles.length; index++) {
        //         var remove = 0;

        //         for (let index2 = 0; index2 < prevFiles.length; index2++) {
        //             if (acceptedFiles[index].name === prevFiles[index2].name) {
        //                 remove = 1;
        //                 break;
        //             }
        //         }
        //         if (remove === 0) {
        //             prevFiles.push(acceptedFiles[index]);
        //         }
        //     }
        //     newFiles = prevFiles;
        // } else {
        //     newFiles = acceptedFiles;
        // }

        const totalfile = newFiles.length;
        for (let index = 0; index < totalfile; index++) {
            var error = 0;
            var filename = newFiles[index].name.toLowerCase();
            var extension_list = ['pdf'];
            var ext_with_dot = path.extname(filename);
            var file_extension = ext_with_dot.split('.').join('');

            var obj = {};

            var fileErrText = `Only files with the following extensions are allowed: pdf.`;

            if (extension_list.indexOf(file_extension) === -1) {
                error = error + 1;

                // let str_1 = `One or more files could not be uploaded.The specified file [file_name] could not be uploaded.`;
                // let str_2 = `The specified file [file_name] could not be uploaded.`;
                let str_1 = '';
                let str_2 = '';
                if (totalfile > 1) {
                    if (index === 0) {
                        obj['errorText'] = str_1.replace('[file_name]', filename) + fileErrText;
                    } else {
                        obj['errorText'] = str_2.replace('[file_name]', filename) + fileErrText;
                    }
                } else {
                    obj['errorText'] = str_2.replace('[file_name]', filename) + fileErrText;
                }
                rejectedFiles.push(obj);
            }

            if (newFiles[index].size > 50000000) {
                let err_txt = `The file [file_name] could not be saved because it exceeds 50 MB, the maximum allowed size for uploads.`;
                obj['errorText'] = err_txt.replace('[file_name]', filename);
                error = error + 1;
                rejectedFiles.push(obj);
            }

            if (error === 0) {
                uploadFile.push(newFiles[index]);
            } else {
                // setErrors({file_name: obj.errorText}); // This wont work as re render will clear it
                this.setState({ isValidFile: false, fileErrText: obj.errorText });
            }
        }

        if (rejectedFiles.length === 0) {
            setErrors({ file_name: false });
            this.setState({ isValidFile: true, fileErrText: '' });
        }

        setFieldValue('file_name', newFiles);
        setFieldTouched('file_name');

        this.setState({
            rejectedFile: rejectedFiles,
        });

        var fileListHtml = newFiles.map((file) => (
            <>
                <span
                    onClick={() => {
                        removeDropZoneFiles(file.name, this, setErrors, setFieldValue);
                    }}
                >
                    <i className='far fa-times-circle'></i>
                </span>{' '}
                {trimString(25, file.name)}
            </>
        ));

        this.setState({
            files: uploadFile,
            filesHtml: fileListHtml,
            prevFilesHtml: ''
        });
    };

    render() {
        const { plantOrdersDetails, isLoading } = this.state;

        const newInitialValues = Object.assign(initialValues, {
            invoice_number:
                Object.keys(plantOrdersDetails).length > 0 && plantOrdersDetails.invoice_number
                    ? plantOrdersDetails.invoice_number
                    : '',
            flight_booked:
                Object.keys(plantOrdersDetails).length > 0 && plantOrdersDetails.flight_booked
                    ? plantOrdersDetails.flight_booked
                    : '',
            flight_booked_location:
                Object.keys(plantOrdersDetails).length > 0 && plantOrdersDetails.flight_booked_location
                    ? plantOrdersDetails.flight_booked_location
                    : '',
            flight_booked_time:
                Object.keys(plantOrdersDetails).length > 0 && plantOrdersDetails.flight_booked_time
                    ? new Date(plantOrdersDetails.flight_booked_time)
                    : '',
        });

        return (
            <Modal show={this.props.showModal} onHide={this.props.modalCloseHandler} backdrop='static'>
                <ModalLoader isLoading={isLoading} />
                <Modal.Header closeButton>
                    <Modal.Title>{ Object.keys(plantOrdersDetails).length > 0 ? 'Edit AWB Document' : 'Add AWB Document' }</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className='contBox'>
                        <Row>
                            <Col xs={12} sm={12} md={12}>
                                <div className='profile'>
                                    <Formik
                                        initialValues={newInitialValues}
                                        validationSchema={validationSchema(this)}
                                        onSubmit={this.handleSubmitEvent}
                                    >
                                        {({
                                            values,
                                            errors,
                                            touched,
                                            setFieldTouched,
                                            setFieldValue,
                                            setErrors,
                                            isSubmitting,
                                        }) => {
                                            return (
                                                <Form>
                                                    <Row className='eaditRow'>
                                                        <Col xs={12} sm={6} md={6}>
                                                            <div className='form-group'>
                                                                <label>
                                                                    Invoice Number <span class='required-field'>*</span>{' '}
                                                                </label>
                                                                <Field
                                                                    name='invoice_number'
                                                                    type='text'
                                                                    className='form-control'
                                                                    autoComplete='off'
                                                                    placeholder={`Enter Invoice Number`}
                                                                ></Field>

                                                                {errors.invoice_number && touched.invoice_number ? (
                                                                    <span className='errorMsg'>
                                                                        {errors.invoice_number}
                                                                    </span>
                                                                ) : null}
                                                            </div>
                                                        </Col>
                                                        <Col xs={12} sm={6} md={6}>
                                                            <div className='form-group'>
                                                                <label>
                                                                    Flight Booked Number{' '}
                                                                    <span class='required-field'>*</span>
                                                                </label>
                                                                <Field
                                                                    name='flight_booked'
                                                                    type='text'
                                                                    className='form-control'
                                                                    autoComplete='off'
                                                                    placeholder={`Enter Flight Booked Number`}
                                                                ></Field>

                                                                {errors.flight_booked && touched.flight_booked ? (
                                                                    <span className='errorMsg'>
                                                                        {errors.flight_booked}
                                                                    </span>
                                                                ) : null}
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col xs={12} sm={6} md={6}>
                                                            <div className='form-group'>
                                                                <label>
                                                                    Flight Booked Location{' '}
                                                                    <span class='required-field'>*</span>
                                                                </label>
                                                                <Field
                                                                    name='flight_booked_location'
                                                                    type='text'
                                                                    className='form-control'
                                                                    autoComplete='off'
                                                                    placeholder={`Enter Flight Booked Location`}
                                                                ></Field>

                                                                {errors.flight_booked_location &&
                                                                touched.flight_booked_location ? (
                                                                    <span className='errorMsg'>
                                                                        {errors.flight_booked_location}
                                                                    </span>
                                                                ) : null}
                                                            </div>
                                                        </Col>
                                                        <Col xs={12} sm={6} md={6}>
                                                            <div className='form-group'>
                                                                <label>
                                                                    Flight Booked Date{' '}
                                                                    <span class='required-field'>*</span>
                                                                </label>
                                                                <DatePicker
                                                                    name='flight_booked_time'
                                                                    id='flight_booked_time'
                                                                    showMonthDropdown
                                                                    showYearDropdown
                                                                    dropdownMode='select'
                                                                    className='form-control'
                                                                    selected={values.flight_booked_time}
                                                                    dateFormat='dd-MM-yyyy'
                                                                    autoComplete='off'
                                                                    placeholderText='Select date'
                                                                    onChange={(e) => {
                                                                        if (e === null) {
                                                                            setFieldValue('flight_booked_time', '');
                                                                        } else {
                                                                            setFieldValue('flight_booked_time', e);
                                                                        }
                                                                    }}
                                                                />
                                                                {errors.flight_booked_time &&
                                                                touched.flight_booked_time ? (
                                                                    <span className='errorMsg'>
                                                                        {errors.flight_booked_time}
                                                                    </span>
                                                                ) : null}
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col xs={12} sm={6} md={6}>
                                                            <div className='form-group'>
                                                                <label>
                                                                    Upload Logistics doc{' '}
                                                                    <span class='required-field'>*</span>
                                                                </label>
                                                                <Dropzone
                                                                    maxFiles={1}
                                                                    multiple={false}
                                                                    onDrop={(acceptedFiles) =>
                                                                        this.setDropZoneFiles(
                                                                            acceptedFiles,
                                                                            setErrors,
                                                                            setFieldValue,
                                                                            setFieldTouched
                                                                        )
                                                                    }
                                                                >
                                                                    {({ getRootProps, getInputProps }) => (
                                                                        <section>
                                                                            <div
                                                                                {...getRootProps()}
                                                                                className='custom-file-upload-header'
                                                                            >
                                                                                <input {...getInputProps()} />
                                                                                <p>Upload file</p>
                                                                            </div>
                                                                            <div className='custom-file-upload-area'>
                                                                                {this.state.filesHtml}
                                                                                {this.state.prevFilesHtml}
                                                                            </div>
                                                                        </section>
                                                                    )}
                                                                </Dropzone>

                                                                {errors.file_name && touched.file_name ? (
                                                                    <span className='errorMsg'>{errors.file_name}</span>
                                                                ) : null}
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col>
                                                            {errors.message ? (
                                                                <span className='errorMsg'>{errors.message}</span>
                                                            ) : null}
                                                        </Col>
                                                    </Row>
                                                    <ButtonToolbar>
                                                        <Button
                                                            onClick={this.props.modalCloseHandler}
                                                            className={`btn-line`}
                                                            type='button'
                                                        >
                                                            Close
                                                        </Button>
                                                        <Button className='btn-fill' type='submit'>
                                                            {isSubmitting ? 'Submitting...' : 'Submit'}
                                                        </Button>
                                                    </ButtonToolbar>
                                                </Form>
                                            );
                                        }}
                                    </Formik>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </Modal.Body>
            </Modal>
        );
    }
}

export default withRouter(PlantOrdersModal);
