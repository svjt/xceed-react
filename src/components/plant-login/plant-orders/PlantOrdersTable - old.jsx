import React, { Component } from 'react';
import { Row, Col, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Link } from 'react-router-dom';
import Pagination from 'react-js-pagination';
import dateFormat from 'dateformat';

import axios from '../../../shared/axios_plant';
import { htmlDecode } from '../../../shared/helper';
import PackingModal from './PackingModal';
import InvoiceModal from './InvoiceModal';
import { isMobile } from "react-device-detect";


const s3bucket_task_diss_path_coa = `${process.env.REACT_APP_API_URL}/api/tasks/download_task_bucket_coa/`;
const s3bucket_task_ship_download = `${process.env.REACT_APP_API_URL}/api/tasks/download_ship_doc/`;

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
    return (
        <OverlayTrigger
            overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
            placement='left'
            delayShow={300}
            delayHide={150}
            trigger={['hover']}
        >
            <Link to={href || ''} onClick={clicked}>
                {children}
            </Link>
        </OverlayTrigger>
    );
}

const actionFormatter = (refObj) => (cell) => {
    const file = refObj.state.plantOrders.filter((data) => cell == data.invoice_number);
    return (
        <div className='actionStyle'>
            {file[0].file_path != '' ? (
                <LinkWithTooltip
                    tooltip='Click to Download Invoice File'
                    href='#'
                    clicked={(e) => refObj.redirectUrlTask(e, `${s3bucket_task_ship_download}${file[0].file_path}`)}
                    id='tooltip-1'
                >
                    <i className='fas fa-download' />
                </LinkWithTooltip>
            ) : null}
            {file[0].file_path != '' ? (
                <LinkWithTooltip
                    tooltip='Click to Download Packing File'
                    href='#'
                    clicked={(e) => refObj.redirectUrlTask(e, `${s3bucket_task_ship_download}${file[0].file_path}`)}
                    id='tooltip-1'
                >
                    <i className='fas fa-download' />
                </LinkWithTooltip>
            ) : null}
            <LinkWithTooltip
                tooltip='Click to Edit Invoice'
                href='#'
                clicked={(e) => refObj.modalShowHandlerInvoice(e, cell)}
                id='tooltip-1'
            >
                <i className='far fa-edit' />
            </LinkWithTooltip>
            {"  "}
            <LinkWithTooltip
                tooltip='Click to Edit Packing List'
                href='#'
                clicked={(e) => refObj.modalShowHandlerPacking(e, cell)}
                id='tooltip-1'
            >
                <i className='far fa-edit' />
            </LinkWithTooltip>
        </div>
    );
};

class PlantOrdersTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            showInvoiceModal: false,
            showPackingModal: false,
            plantOrders: [],
            plantOrdersCount: 1,
            activePage: 1,
            itemPerPage: 20,
            invoiceDetails: {},
            packingDetails: {},
        };
    }

    componentDidMount() {
        // Do the API Calls
        this.getPlantTaskList();
    }

    getPlantTaskList(page = 1) {
        this.setState({ isLoading: true });
        axios.get(`/api/employees/plant/invoice_task_list?page=${page}`)
            .then(async (res) => {
                this.setState({
                    plantOrders: res.data.data,
                    plantOrdersCount: Number(res.data.count),
                    isLoading: false,
                });
            })
            .catch((err) => {
                this.setState({
                    isLoading: false,
                });
            });
    };

    handlePageChange = (pageNumber) => {
        this.setState({ activePage: pageNumber });
        this.getPlantTaskList(pageNumber > 0 ? pageNumber : 1);
    };

    getSubmittedBy = (request) => {
        if (request.submitted_by > 0) {
            return `${request.agent_fname} ${request.agent_lname}`;
        } else if (request.ccp_posted_by > 0) {
            let posted_from = "";
            return `${request.emp_posted_by_fname} ${request.emp_posted_by_lname} ${posted_from}`;
        } else {
            return `${request.first_name} ${request.last_name}`;
        }
    };

    openMobileView = (onBehalf) => {
        this.setState({
            showMobileAlert: true,
            displayMobileText: `This request is raised on behalf of ${onBehalf} by Dr.Reddy’s`,
        });
    };

    isListingHover = (targetName) => {
        return this.state[targetName] ? this.state[targetName].hoverTip : false;
    };

    toggleListingTip = (targetName) => {
        if (!this.state[targetName]) {
            this.setState({
                [targetName]: {
                    hoverTip: true,
                },
            });
        } else {
            this.setState({
                ...this.state,
                [targetName]: {
                    hoverTip: !this.state[targetName].hoverTip,
                },
            });
        }
    };
    modalShowHandlerInvoiceAdd = (event, sales_order) => {
        event.preventDefault();
        if (sales_order) {
            let invoiceValues = {
                sales_order_number: sales_order,                
            };
            this.setState({ invoiceDetails: invoiceValues,showInvoiceModal: true });
        }else{
            this.setState({ invoiceDetails: [],showInvoiceModal: true });
        }
    };

    modalShowHandlerInvoice = (event, id) => {
        event.preventDefault();
        if (id) {
            this.setState({ isLoading: true });
            axios
                .get(`/api/employees/plant_orders_invoice/${id}`)
                .then((res) => {
                    this.setState({ isLoading: false });
                    if (res.data.data && res.data.data.length > 0) {
                        this.setState({ invoiceDetails: res.data.data[0], showInvoiceModal: true });
                    }
                })
                .catch((err) => {
                    this.setState({ isLoading: false, invoiceDetails: {} });
                    console.log({ err });
                });
        } else {
            this.setState({ invoiceDetails: {} });
        }
    };

    modalCloseHandlerInvoice = () => {
        this.setState({ showInvoiceModal: false });
    };

    modalShowHandlerPackingAdd = (event, id, sales_order) => {
        event.preventDefault();
        if (id) {
            let packingValues = {
                sales_order_number: sales_order,
                invoice_number : id             
            };
            this.setState({ packingDetails: packingValues, showPackingModal: true });
        }else{
            let packingValues = {
                sales_order_number: sales_order,                       
            };
            this.setState({ packingDetails: packingValues, showPackingModal: true });
        }
    };

    modalShowHandlerPacking = (event, id) => {
        event.preventDefault();
        if (id) {
            this.setState({ isLoading: true });
            axios
                .get(`/api/employees/plant_orders_packing/${id}`)
                .then((res) => {
                    this.setState({ isLoading: false });
                    if (res.data.data && res.data.data.length > 0) {
                        this.setState({ packingDetails: res.data.data[0], showPackingModal: true });
                    }
                })
                .catch((err) => {
                    this.setState({ isLoading: false, packingDetails: {} });
                    console.log({ err });
                });
        } else {
            this.setState({ packingDetails: {} });
        }
    };

    modalCloseHandlerPacking = () => {
        this.setState({ showPackingModal: false });
    };

    _htmlDecode = (refObj) => (cell) => {
        return htmlDecode(cell);
    };

    setDate = (refObj) => (cell) => {
        if (cell) {
            return dateFormat(cell, 'dd-mm-yyyy HH:MM:ss');
        }
        return '-';
    };

    render() {
        const { plantOrders, showPackingModal, showInvoiceModal, invoiceDetails, packingDetails } = this.state;

        return (
            <>
                <section className='content awbDocumentTable'>
                    <div className='serchapanel'>
                        <div className='box-body'>
                            {/* <BootstrapTable data={plantOrders}>
                                <TableHeaderColumn
                                    isKey
                                    tdStyle={{ whiteSpace: 'normal' }}
                                    dataField='task_ref'
                                    dataFormat={this._htmlDecode(this)}
                                >
                                    Task Ref
                                </TableHeaderColumn>

                                <TableHeaderColumn
                                    tdStyle={{ whiteSpace: 'normal' }}
                                    dataField='invoice_number'
                                    dataFormat={this._htmlDecode(this)}
                                >
                                    Invoice Number
                                </TableHeaderColumn>

                                <TableHeaderColumn
                                    dataField='invoice_location'
                                    tdStyle={{ whiteSpace: 'normal' }}
                                    dataFormat={this._htmlDecode(this)}
                                >
                                    Invoice Location
                                </TableHeaderColumn>

                                <TableHeaderColumn dataField='invoice_date' dataFormat={this.setDate(this)}>
                                    Invoice Date
                                </TableHeaderColumn>

                                <TableHeaderColumn dataField='invoice_file_name'>Invoice File Name</TableHeaderColumn>

                                <TableHeaderColumn
                                    dataField='packing_location'
                                    tdStyle={{ whiteSpace: 'normal' }}
                                    dataFormat={this._htmlDecode(this)}
                                >
                                    Packing Location
                                </TableHeaderColumn>

                                <TableHeaderColumn dataField='packing_date' dataFormat={this.setDate(this)}>
                                    Packing Date
                                </TableHeaderColumn>

                                <TableHeaderColumn dataField='packing_file_name'>Packing File Name</TableHeaderColumn>

                                <TableHeaderColumn dataField='invoice_number' dataFormat={actionFormatter(this)}>
                                    Action
                                </TableHeaderColumn>
                            </BootstrapTable> */}
                            <div className="listing-table table-responsive">
                                <table className="table table-hover table-bordered" style={{ width: '1500px', maxWidth: 'inherit' }}>
                                    <thead>
                                        <tr>
                                            <th width="120">Task Ref</th>
                                            <th width="150">Purchase Order (Customer PO number)</th>
                                            <th width="150">Sales Order #</th>
                                            <th width="150">Material Name</th>
                                            <th width="150">Invoice #</th>
                                            {/* <th width="150">Invoice PDF</th> */}
                                            <th width="150">Packing List PDF</th>
                                            <th width="150">AWB #</th>
                                            <th width="150">COA</th>
                                            <th width="150">Payment Status</th>
                                            <th width="150">Submitted By</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.plantOrders.map((order, key) => (
                                            <tr key={key}>
                                                <td>
                                                    <Link                                                        
                                                        to={{
                                                            pathname: `/plant_task_details/${order.task_id}`,
                                                        }}
                                                        style={{ cursor: "pointer" }}
                                                    >
                                                        {order.task_ref}
                                                    </Link>
                                                </td>
                                                <td>
                                                    <p>{order.po_number != null && order.po_number != '' ? order.po_number : '-'}</p>
                                                </td>
                                                <td>
                                                    <p>{order.sales_order_no != null && order.sales_order_no != '' ? order.sales_order_no : '-'}</p>
                                                    {order.sales_order_no != null && order.sales_order_no != '' ? `(${order.sales_order_date})` : ""}
                                                </td>
                                                <td>{order.product_name}</td>
                                                <td>
                                                    {order.invoice_number != null && order.invoice_number != '' ?
                                                        (
                                                            <>
                                                                {order.invoice_number} {"  "}
                                                                <LinkWithTooltip
                                                                    tooltip='Click to Download Invoice File'
                                                                    href='#'
                                                                    className="download_link"
                                                                    clicked={(e) => window.open(`${s3bucket_task_ship_download}${order.all_file_hash}/invoice`, '_blank')}
                                                                    id='tooltip-1'
                                                                >
                                                                    <i className='fas fa-download' />
                                                                </LinkWithTooltip>{"  "}
                                                                <LinkWithTooltip
                                                                    tooltip='Click to Edit Invoice'
                                                                    href='#'
                                                                    clicked={(e) => this.modalShowHandlerInvoice(e, order.invoice_number)}
                                                                    id='tooltip-2'
                                                                >
                                                                    <i className='far fa-edit' />
                                                                </LinkWithTooltip>
                                                            </>
                                                        ) : (
                                                            <LinkWithTooltip
                                                                tooltip='Click to Add Invoice'
                                                                href='#'
                                                                clicked={(e) => this.modalShowHandlerInvoiceAdd(e, order.sales_order_no)}
                                                                id='tooltip-3'
                                                            >
                                                                <i className='fas fa-plus' />
                                                            </LinkWithTooltip>
                                                        )
                                                    }
                                                </td>
                                                {/* <td>
                                                    {order.invoice_number != null && order.invoice_number != '' && order.invoice_file_name != '' && order.invoice_file_name != null ?  order.invoice_file_name : null}
                                                </td> */}
                                                <td>
                                                    {order.invoice_number != null && order.invoice_number != '' && order.packing_file_name != '' && order.packing_file_name != null ?
                                                        (
                                                            <>
                                                                {order.packing_file_name} {"  "}
                                                                <LinkWithTooltip
                                                                    tooltip='Click to Download Packing File'
                                                                    href='#'
                                                                    className="download_link"
                                                                    clicked={(e) => window.open(`${s3bucket_task_ship_download}${order.all_file_hash}/package`, '_blank')}
                                                                    id='tooltip-4'
                                                                >
                                                                    <i className='fas fa-download' />
                                                                </LinkWithTooltip>
                                                                {"  "}
                                                                <LinkWithTooltip
                                                                    tooltip='Click to Edit Packing List'
                                                                    href='#'
                                                                    clicked={(e) => this.modalShowHandlerPacking(e, order.invoice_number)}
                                                                    id='tooltip-5'
                                                                >
                                                                    <i className='far fa-edit' />
                                                                </LinkWithTooltip>
                                                            </>
                                                        ) : (
                                                            <LinkWithTooltip
                                                                tooltip='Click to Add Packing List'
                                                                href='#'
                                                                clicked={(e) => this.modalShowHandlerPackingAdd(e, order.invoice_number, order.sales_order_no)}
                                                                id='tooltip-6'
                                                            >
                                                                <i className='fas fa-plus' />
                                                            </LinkWithTooltip>
                                                        )}
                                                </td>
                                                <td>{order.awb != null && order.awb != '' ? `${order.awb}${`\n`}( ${dateFormat(order.awb_date, "d mmm yyyy")} )` : "-"}</td>
                                                <td>
                                                    {order.coa_file.length > 0 && (
                                                        order.coa_file
                                                            .map((file, p) => {
                                                                return (
                                                                    <p key={p}>
                                                                        {file.actual_file_name}{"  "}
                                                                        <LinkWithTooltip
                                                                            tooltip='Click to Download'
                                                                            href='#'
                                                                            clicked={(e) => window.open(`${s3bucket_task_diss_path_coa}${file.coa_id}`, "_blank")}
                                                                            id='tooltip-1'
                                                                        >
                                                                            <i className='fas fa-download' />
                                                                        </LinkWithTooltip>
                                                                    </p>
                                                                );
                                                            })
                                                    )}
                                                </td>
                                                <td>{order.payment_status}</td>

                                                <td>
                                                    {isMobile ? (
                                                        <span
                                                            id={`tip-${order.task_id}`}
                                                            onClick={() =>
                                                                (order.submitted_by > 0 ||
                                                                    order.ccp_posted_by > 0) &&
                                                                this.openMobileView(
                                                                    `${order.first_name} ${order.last_name}`
                                                                )
                                                            }
                                                        >
                                                            {this.getSubmittedBy(order)}
                                                        </span>
                                                    ) : (
                                                        <>
                                                            <span id={`tip-${order.task_id}`}>
                                                                {this.getSubmittedBy(order)}
                                                            </span>

                                                            {(order.submitted_by > 0 ||
                                                                order.ccp_posted_by > 0) && (
                                                                    <Tooltip
                                                                        placement="right"
                                                                        isOpen={this.isListingHover(
                                                                            `tip-${order.task_id}`
                                                                        )}
                                                                        autohide={false}
                                                                        target={`tip-${order.task_id}`}
                                                                        toggle={() =>
                                                                            this.toggleListingTip(
                                                                                `tip-${order.task_id}`
                                                                            )
                                                                        }
                                                                        className="listingTip"
                                                                    >
                                                                        This request is raised on behalf of {`${order.first_name} ${order.last_name}`} by {this.getSubmittedBy(order)}
                                                                    </Tooltip>
                                                                )}
                                                        </>
                                                    )}
                                                </td>

                                            </tr>
                                        ))}
                                        {this.state.plantOrders.length === 0 && (
                                            <tr>
                                                <td colSpan="11" align="center">
                                                    No Data to display
                                                </td>
                                            </tr>
                                        )}
                                    </tbody>
                                </table>
                            </div>

                            {this.state.plantOrdersCount > this.state.itemPerPage ? (
                                <Row>
                                    <Col md={12}>
                                        <div className='paginationOuter text-right'>
                                            <Pagination
                                                activePage={this.state.activePage}
                                                itemsCountPerPage={this.state.itemPerPage}
                                                totalItemsCount={this.state.plantOrdersCount}
                                                itemClass='nav-item'
                                                linkClass='nav-link'
                                                activeClass='active'
                                                onChange={this.handlePageChange}
                                            />
                                        </div>
                                    </Col>
                                </Row>
                            ) : null}
                        </div>
                    </div>
                </section>
                {showInvoiceModal && (
                    <InvoiceModal
                        showModal={showInvoiceModal}
                        modalShowHandler={this.modalShowHandlerInvoice}
                        modalCloseHandler={this.modalCloseHandlerInvoice}
                        invoiceDetails={invoiceDetails}
                    />
                )}
                {showPackingModal && (
                    <PackingModal
                        showModal={showPackingModal}
                        modalShowHandler={this.modalShowHandlerPacking}
                        modalCloseHandler={this.modalCloseHandlerPacking}
                        packingDetails={packingDetails}
                    />
                )}
            </>
        );
    }
}

export default PlantOrdersTable;
