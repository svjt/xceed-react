import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';

import './Sidebar.css';

import DashboradImage from '../../../assets/images/dashboard-icon.svg';
import DashboradImageActive from '../../../assets/images/dashboard-icon-active.svg';

class Sidebar extends Component {

	constructor() {
		super();
	}

	render() {
		if (this.props.isLoggedIn === false) return null;

		return (
			<>
				<aside className="main-sidebar">
					<section className="sidebar">
						<ul className="sidebar-menu">
							<li className="header">MAIN NAVIGATION</li>
							{
								window.location.pathname === "/plant_orders" ? (
									<li className="treeview active">
										<Link to={'/plant_orders'}>
											<img src={DashboradImageActive} alt="Dashboard" className="main-icon" />
											<span className="static-text">Orders</span>
										</Link>
									</li>
								) : (
									<li className="treeview">
										<Link to={'/plant_orders'}>
											<img src={DashboradImage} alt="Dashboard" className="main-icon" />
											<span className="static-text">Orders</span>
										</Link>
									</li>
								)
							}
						</ul>
					</section>
				</aside>
			</>
		);
	}
}

export default withRouter(Sidebar);