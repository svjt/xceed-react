import React, { Component } from 'react';
import { Row, Col } from "react-bootstrap";
import { withRouter, Link } from 'react-router-dom';
import swal from "sweetalert";
import dateFormat from 'dateformat';
import ReactHtmlParser from "react-html-parser";
import { Accordion, AccordionItem, AccordionItemHeading, AccordionItemButton, AccordionItemPanel } from 'react-accessible-accordion';
import { htmlDecode, localDate, localDateOnly } from "../../../shared/helper";
import whitelogo from '../../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../../assets/images/Xceed_Logo-animated.gif";
import downloadIcon from "../../../assets/images/download-icon.svg";
import Header from '../header/Header';
import axios from '../../../shared/axios_plant';

const ship_download = `${process.env.REACT_APP_API_URL}/api/tasks/download_ship_doc/`;
const s3bucket_task_diss_path_coa = `${process.env.REACT_APP_API_URL}/api/tasks/download_task_bucket_coa/`;
const req_category_arr = [
    { request_category_id: 1, request_category_value: "New Order" },
    { request_category_id: 2, request_category_value: "Other" },
    { request_category_id: 3, request_category_value: "Complaint" },
    { request_category_id: 4, request_category_value: "Forecast" },
    { request_category_id: 5, request_category_value: "Payment" },
    { request_category_id: 6, request_category_value: "Notification" },
    { request_category_id: 7, request_category_value: "Request for Proforma Invoice" },
];
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
    return (

        <Link to={href} onClick={clicked}>
            {children}
        </Link>

    );
};

class PlantOrderDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            invalid_access: false,
            show_lang: "EN",
            show_lang_diss: "EN",
            taskDetails: [],
            taskDetailsFiles: [],
            taskDetailsFilesCoa: [],
            taskDetailsFilesSTOCoa: [],
            taskDetailsFilesInvoice: [],
            taskDetailsFilesPacking: [],
            for_review_task: "",
            shippingInfo: [],
            shippingSTOInfo: [],
            task_id: this.props.match.params.id,
        };
    }

    componentDidMount() {
        this.setState({
            isLoading: true,
        });
        this.getPlantTaskDetails();
    };

    getPlantTaskDetails() {
        this.setState({ invalid_access: false });
        axios.get(`/api/employees/plant/tasks_details/${this.state.task_id}`)
            .then((res) => {
                this.setState({
                    taskDetails: res.data.data.taskDetails,
                    taskDetailsFiles: res.data.data.taskDetailsFiles,
                    taskDetailsFilesCoa: res.data.data.taskDetailsFilesCoa,
                    taskDetailsFilesSTOCoa: res.data.data.taskDetailsFilesSTOCoa,
                    taskDetailsFilesInvoice: res.data.data.taskDetailsFilesInvoice,
                    taskDetailsFilesPacking: res.data.data.taskDetailsFilesPacking,
                    shippingInfo: res.data.data.shippingInfo,
                    shippingSTOInfo: res.data.data.shippingSTOInfo,
                });
            })
            .catch((err) => {
                var errText = "Invalid Access To This Page.";
                this.setState({ invalid_access: true });
                swal({
                    closeOnClickOutside: false,
                    title: "Error in page",
                    text: errText,
                    icon: "error",
                }).then(() => {
                    this.props.history.push('/plant_orders');
                });
            });

    };

    redirectUrlTask = (event, path) => {
        event.preventDefault();
        window.open(path, "_self");
    };

    setDateOnly(date_value) {
        var mydate = localDateOnly(date_value);
        return dateFormat(mydate, "dd/mm/yyyy");
    }

    checkHandler = (event) => {
        event.preventDefault();
    };


    setReqCategoryName = (req_cate_id) => {
        var ret = "Not Set";
        for (let index = 0; index < req_category_arr.length; index++) {
            const element = req_category_arr[index];
            if (element["request_category_id"] === req_cate_id) {
                ret = element["request_category_value"];
            }
        }

        return ret;
    };

    getConverTime = (time) => {
        let timeString = '';
        let H = time.substr(0, 2);
        let h = H % 12 || 12;
        let ampm = (H < 12 || H === 24) ? "AM" : "PM";
        timeString = h.toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false }) + ":" + time.substr(2, 3) + " " + ampm;
        return timeString;
    };

    getBlueDartDynamicContent = (sel_index, infoArr) => {

        let dynamic_html = infoArr.map((value, index) => {
            if (sel_index == index) {
                let obj = infoArr[sel_index][Object.keys(infoArr[sel_index])];

                let html_bottom = [];
                for (let props in obj.shipping_steps) {
                    html_bottom.push(
                        <tr>
                            <td>{obj.shipping_steps[props].scan_date}</td>
                            <td>{this.getConverTime(obj.shipping_steps[props].scan_time)}</td>
                            <td>{obj.shipping_steps[props].scanned_location}</td>
                            <td>
                                {(obj.shipping_steps[props].cstat_desc) ? obj.shipping_steps[props].cstat_desc : obj.shipping_steps[props].scan}
                                {(obj.shipping_steps[props].scan_code == 'POD' && obj.shipping_steps[props].file_path != '') && (<a href={`${ship_download + obj.shipping_steps[props].file_path}/bluedart`} target="_blank">
                                    <img src={downloadIcon} width="28" height="28" id="bluedartdownload" type="button" />
                                </a>)}
                            </td>
                        </tr>
                    );
                }

                return (
                    <Row>
                        <Col sm="12">
                            <div className="views-row">
                                <div className="">
                                    <strong>
                                        Invoice Date
                                    </strong>
                                    <div className="field-content">
                                        {obj.invoice_display_date}
                                    </div>
                                </div>
                                <div className="">
                                    <strong>
                                        Invoice File
                                    </strong>
                                    <div className="field-content">
                                        {obj.invoice_file_name}
                                        {(obj.invoice_file_name != '') && (<a href={`${ship_download + obj.invoice_file_hash}/invoice`} target="_blank">
                                            <img src={downloadIcon} width="28" height="28" id="invoicedownload" type="button" />
                                        </a>)}
                                    </div>
                                </div>
                                <div className="">
                                    <strong>
                                        Invoice Location
                                    </strong>
                                    <div className="field-content">
                                        {obj.invoice_location}
                                    </div>
                                </div>
                                <div className="">
                                    <strong>
                                        Packing Date
                                    </strong>
                                    <div className="field-content">
                                        {obj.packing_display_date}
                                    </div>
                                </div>
                                <div className="">
                                    <strong>
                                        Packing File
                                    </strong>
                                    <div className="field-content">
                                        {obj.packing_file_name}
                                        {(obj.packing_file_name != '') && (<a href={`${ship_download + obj.packing_file_hash}/package`} target="_blank">
                                            <img src={downloadIcon} width="28" height="28" id="invoicedownload" type="button" />
                                        </a>)}
                                    </div>
                                </div>
                                <div className="">
                                    <strong>
                                        Packing Location
                                    </strong>
                                    <div className="field-content">
                                        {obj.packing_location}
                                    </div>
                                </div>

                                <div className="">
                                    <strong>
                                        Ref. No.
                                    </strong>
                                    <div className="field-content">
                                        {obj.ref_no}
                                    </div>
                                </div>
                                <div className="">
                                    <strong>
                                        Origin Location
                                    </strong>
                                    <div className="field-content">
                                        {obj.origin}
                                    </div>
                                </div>
                                <div className="">
                                    <strong>
                                        Destination Location
                                    </strong>
                                    <div className="field-content">
                                        {obj.destination}
                                    </div>
                                </div>
                                <div className="">
                                    <strong>
                                        Pick Up Date
                                    </strong>
                                    <div className="field-content">
                                        {obj.pick_up_date}
                                    </div>
                                </div>
                                <div className="">
                                    <strong>
                                        Pick Up Time
                                    </strong>
                                    <div className="field-content">
                                        {(obj.pick_up_time != null && obj.pick_up_time != '') ? this.getConverTime(obj.pick_up_time) : null}
                                    </div>
                                </div>
                                <div className="">
                                    <strong>
                                        Expected Delivery Date
                                    </strong>
                                    <div className="field-content">
                                        {obj.expected_delivery_date}
                                    </div>
                                </div>
                                <div className="">
                                    <strong>
                                        Original Delivery Date
                                    </strong>
                                    <div className="field-content">
                                        {obj.dynamic_expected_delivery_date}
                                    </div>
                                </div>
                            </div>
                            <div className="table-responsive-lg">
                                <table className="orderDetailsTable table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Time</th>
                                            <th>Location</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {html_bottom}

                                    </tbody>
                                </table>
                            </div>
                        </Col>
                    </Row>
                );
            }
        });
        return dynamic_html;
    };

    getDynamicContent = (sel_index = 0, infoArr) => {  //alert('get Details')

        let dynamic_html = infoArr.map((value, index) => {
            if (sel_index == index) {
                let obj = infoArr[sel_index][Object.keys(infoArr[sel_index])];

                let html_bottom = [];
                let shipping_step = ['Approval Document', 'Approval Haz Clearance', 'FDA Approval', 'Customs Clearance'];
                for (let property in obj) {

                    if (property == 'Shipping Progress') {
                        for (let props in obj[property]) {
                            if (obj[property][props].date != '' && obj[property][props].date != undefined && property != 'partner') {
                                html_bottom.push(
                                    <tr>
                                        <td>{obj[property][props].date}</td>
                                        <td>{obj[property][props].time}</td>
                                        <td>{obj[property][props].location}</td>
                                        <td>{shipping_step[props]} {obj[property][props].comment}</td>
                                    </tr>
                                );
                            }
                        }
                    } else if (property != 'partner') {
                        if (obj[property] && obj[property].date != '') {
                            html_bottom.push(
                                <tr>
                                    <td>{obj[property].date}</td>
                                    <td>{obj[property].time}</td>
                                    <td>{obj[property].location}</td>
                                    <td>
                                        {property + ".  "}
                                        {(property == 'Invoice Doc' && obj[property].comment != '') && (<a href={`${ship_download + obj[property].file_hash}/invoice`} target="_blank">
                                            <img src={downloadIcon} width="28" height="28" id="invoicedownload" type="button" />
                                        </a>)}
                                        {(property == 'Packing List' && obj[property].comment != '') && (<a href={`${ship_download + obj[property].file_hash}/package`} target="_blank">
                                            <img src={downloadIcon} width="28" height="28" id="invoicedownload" type="button" />
                                        </a>)}
                                        {(property == 'AWB Doc' && obj[property].comment != '') && (<a href={`${ship_download + obj[property].file_hash}/${obj[property].file_type}`} target="_blank">
                                            <img src={downloadIcon} width="28" height="28" id="invoicedownload" type="button" />
                                        </a>)}
                                        {(property == 'HAWB Doc' && obj[property].comment != '') && (<a href={`${ship_download + obj[property].file_hash}/${obj[property].file_type}`} target="_blank">
                                            <img src={downloadIcon} width="28" height="28" id="invoicedownload" type="button" />
                                        </a>)}
                                    </td>
                                </tr>
                            );
                        }
                    }
                }

                return (
                    <Row>
                        <Col sm="12">
                            <div className="table-responsive-lg">
                                <table className="orderDetailsTable table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Time</th>
                                            <th>Location</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {html_bottom}
                                    </tbody>
                                </table>
                            </div>
                        </Col>
                    </Row>);

            }
        });
        return dynamic_html;
        //this.setState({dynamic_html:dynamic_html});
    }


    render() {

        if (this.state.taskDetails.task_id > 0 && this.state.invalid_access == false) {
            return (
                <>
                    <div className='content-wrapper qaWrapper'>
                        <Header title='Order Details' />

                        <section className="content">
                            <div className="serchapanel">
                                <div className="box-body">
                                    <section className="content-header task-accept-decline-main-top">
                                        <h1>
                                            {this.state.taskDetails.req_name} | {this.state.taskDetails.task_ref}
                                        </h1>
                                    </section>
                                    <section className="content switch-class-new">
                                        {/* <div className="nav-tabs-custom nav-tabs-custom2">
                                            <ul className="nav nav-tabs">
                                                <li
                                                    className={this.state.taskDetailsClass}
                                                    onClick={(e) => this.handleTabs(e)}
                                                    id="tab_1"
                                                >
                                                    TASK DETAILS
                                                </li>
                                                {this.state.commentExists && (
                                                        <li onClick={(e) => this.handleTabs(e)} id="tab_2">
                                                            INTERNAL COMMENTS
                                                        </li>
                                                    )}
                                                    {this.state.discussionExists && (
                                                        <li
                                                            onClick={(e) => this.handleTabs(e)}
                                                            id="tab_3"
                                                            className={this.state.discussionClass}
                                                        >
                                                            CUSTOMER DISCUSSIONS
                                                        </li>
                                                    )}
                                                    <li onClick={(e) => this.handleTabs(e)} id="tab_4">
                                                        ACTIVITY LOG
                                                    </li>
                                            </ul> 
                                        </div>*/}

                                        <div className="tab-content">
                                            <div
                                                className={`tab-pane active`}
                                                id="show_tab_1"
                                            >
                                                <div className="boxPapanel content-padding">
                                                    <div className="taskdetails">
                                                        <div className="clearfix tdBtmlist">
                                                            <Row className="task_details_complete">

                                                                {(this.state.taskDetails.request_type == 43 || this.state.taskDetails.request_type == 23) && this.state.taskDetails.curr_owner_fname !== "" && (
                                                                    <Col xs={12} sm={6} md={4}>
                                                                        <div className="form-group">
                                                                            <label>
                                                                                Current Owner:
                                                                            </label>{" "}
                                                                            {this.state.taskDetails.curr_owner_fname}{" "}
                                                                            {this.state.taskDetails.curr_owner_lname}{" "}
                                                                            ({this.state.taskDetails.curr_owner_desig})
                                                                        </div>
                                                                    </Col>
                                                                )}
                                                                {(this.state.taskDetails.request_type == 43 || this.state.taskDetails.request_type == 23) && this.state.taskDetails.company_name !==
                                                                    "" && (
                                                                        <Col xs={12} sm={6} md={4}>
                                                                            <div className="form-group">
                                                                                <label>Customer Name:</label>{" "}
                                                                                {
                                                                                    this.state.taskDetails.company_name
                                                                                }{" "}
                                                                            </div>
                                                                        </Col>
                                                                    )}
                                                                {(this.state.taskDetails.request_type == 43 || this.state.taskDetails.request_type == 23) && (this.state.taskDetails.first_name !==
                                                                    "" ||
                                                                    this.state.taskDetails.last_name !==
                                                                    "") && (
                                                                        <Col xs={12} sm={6} md={4}>
                                                                            <div className="form-group">
                                                                                <label>User Name:</label>{" "}
                                                                                {htmlDecode(
                                                                                    this.state.taskDetails
                                                                                        .first_name
                                                                                )}{" "}
                                                                                {htmlDecode(
                                                                                    this.state.taskDetails.last_name
                                                                                )}
                                                                            </div>
                                                                        </Col>
                                                                    )}
                                                                {(this.state.taskDetails.request_type == 43 || this.state.taskDetails.request_type == 23) && this.state.taskDetails.parent_id == 0 && (
                                                                    <Col xs={12} sm={6} md={4}>
                                                                        <div className="form-group">
                                                                            <label>Market:</label>{" "}
                                                                            {htmlDecode(
                                                                                this.state.taskDetails.country_name
                                                                            )}
                                                                        </div>
                                                                    </Col>
                                                                )}
                                                                {this.state.taskDetails.request_type == 23 && this.state.taskDetails.parent_id == 0 && (
                                                                    <Col xs={12} sm={6} md={4}>
                                                                        <div className="form-group">
                                                                            <label>Request Category:</label>{" "}
                                                                            {this.setReqCategoryName(
                                                                                this.state.taskDetails
                                                                                    .request_category
                                                                            )}
                                                                        </div>
                                                                    </Col>
                                                                )}
                                                                {(this.state.taskDetails.request_type == 43 || this.state.taskDetails.request_type == 23) && this.state.taskDetails.parent_id == 0 && (
                                                                    <Col xs={12} sm={6} md={4}>
                                                                        <div className="form-group">
                                                                            <label>Product:</label>{" "}
                                                                            {htmlDecode(
                                                                                this.state.taskDetails.product_name
                                                                            )}
                                                                        </div>
                                                                    </Col>
                                                                )}
                                                                {(this.state.taskDetails.request_type == 43 || this.state.taskDetails.request_type == 23) && this.state.taskDetails.parent_id == 0 && (
                                                                    <Col xs={12} sm={6} md={4}>
                                                                        <div className="form-group">
                                                                            <label>Quantity:</label>{" "}
                                                                            {this.state.taskDetails.quantity}
                                                                        </div>
                                                                    </Col>
                                                                )}
                                                                {this.state.taskDetails.request_type == 23 && this.state.taskDetails.parent_id == 0 && (
                                                                    <Col xs={12} sm={6} md={4}>
                                                                        <div className="form-group">
                                                                            <label>Requested Date of Delivery:</label>{" "}
                                                                            {this.state.taskDetails.rdd !== null &&
                                                                                this.setDateOnly(this.state.taskDetails.rdd)}
                                                                        </div>
                                                                    </Col>
                                                                )}

                                                                {this.state.taskDetails.request_type === 43 && this.state.taskDetails.sales_order_no != null && (
                                                                    <Col xs={12} sm={6} md={4}>
                                                                        <div className="form-group forecast-image">
                                                                            <label>Sales Order No:</label>{" "}
                                                                            {ReactHtmlParser(htmlDecode(this.state.taskDetails.sales_order_no))}
                                                                        </div>
                                                                    </Col>
                                                                )}

                                                                {this.state.taskDetails.request_type === 43 && this.state.taskDetails.sales_order_date != null && (
                                                                    <Col xs={12} sm={6} md={4}>
                                                                        <div className="form-group forecast-image">
                                                                            <label>
                                                                                Sales Order Date
                                                                            </label>{" "}
                                                                            {ReactHtmlParser(htmlDecode(this.state.taskDetails.sales_order_date))}
                                                                        </div>
                                                                    </Col>
                                                                )}

                                                                {(this.state.taskDetails.request_type == 43 || this.state.taskDetails.request_type == 23) && this.state.taskDetails.po_no != null && (
                                                                    <Col xs={12} sm={6} md={4}>
                                                                        <div className="form-group forecast-image">
                                                                            <label>
                                                                                Purchase Order No.
                                                                            </label>{" "}
                                                                            {ReactHtmlParser(htmlDecode(this.state.taskDetails.po_no))}
                                                                        </div>
                                                                    </Col>
                                                                )}
                                                                {this.state.taskDetails.request_type === 43 && this.state.taskDetails.SAPDetails.length > 0 && (
                                                                    <Col xs={12} sm={6} md={4}>
                                                                        <div className="form-group forecast-image">
                                                                            <label>
                                                                                ShipTo Address
                                                                            </label>{" "}
                                                                            <div>{ReactHtmlParser(htmlDecode(this.state.taskDetails.SAPDetails.shipto_name))}
                                                                            </div>
                                                                            {ReactHtmlParser(htmlDecode(this.state.taskDetails.SAPDetails.street)) + ', '}
                                                                            {ReactHtmlParser(htmlDecode(this.state.taskDetails.SAPDetails.city)) + ', '}
                                                                            {ReactHtmlParser(htmlDecode(this.state.taskDetails.SAPDetails.post_code)) + ', '}
                                                                            {ReactHtmlParser(htmlDecode(this.state.taskDetails.SAPDetails.country)) + ', '}
                                                                        </div>
                                                                    </Col>
                                                                )}
                                                                {this.state.taskDetails.request_type === 43 && this.state.taskDetails.po_no != null && this.state.taskDetails.SAPDetails.po_file_date != null && (
                                                                    <Col xs={12} sm={6} md={4}>
                                                                        <div className="form-group forecast-image">
                                                                            <label>
                                                                                PO Uploaded Date
                                                                            </label>{" "}
                                                                            {ReactHtmlParser(htmlDecode(this.state.taskDetails.SAPDetails.po_file_date))}
                                                                        </div>
                                                                    </Col>
                                                                )}
                                                                {this.state.taskDetails.request_type === 43 && this.state.taskDetails.po_no != null && this.state.taskDetails.SAPDetails.po_file_name != null && (
                                                                    <Col xs={12} sm={6} md={4}>
                                                                        <div className="form-group forecast-image">
                                                                            <label>
                                                                                PO File
                                                                            </label>{" "}
                                                                            {ReactHtmlParser(htmlDecode(this.state.taskDetails.SAPDetails.po_file_name))}

                                                                        </div>
                                                                    </Col>
                                                                )}
                                                                {this.state.taskDetails.parent_id ===
                                                                    0 && (
                                                                        <Col xs={12} sm={12} md={12}>
                                                                            <div className="form-group forecast-image">
                                                                                <label>Requirement:</label>{" "}
                                                                                {this.state.taskDetails.language == 'en' ? ReactHtmlParser(htmlDecode(this.state.taskDetails.task_content)) : ReactHtmlParser(htmlDecode(this.state.taskDetails.content))}
                                                                            </div>
                                                                        </Col>
                                                                    )}

                                                                {this.state.taskDetails.ccp_posted_by > 0 && (
                                                                    <Col xs={12} sm={6} md={4}>
                                                                        <div className="form-group">
                                                                            <label>Submitted By:</label>{" "}
                                                                            {this.state.taskDetails.emp_posted_by_first_name}{" "}{this.state.taskDetails.emp_posted_by_last_name
                                                                            }{" "}
                                                                            ({this.state.taskDetails.emp_posted_by_desig_name})
                                                                        </div>
                                                                    </Col>
                                                                )}

                                                                {this.state.taskDetails.submitted_by > 0 && (
                                                                    <Col xs={12} sm={6} md={4}>
                                                                        <div className="form-group">
                                                                            <label>Submitted By:</label>{" "}
                                                                            {this.state.taskDetails.agnt_first_name}{" "}{this.state.taskDetails.agnt_last_name}{" "}
                                                                            (Agent)
                                                                        </div>
                                                                    </Col>
                                                                )}

                                                                {this.state.taskDetailsFiles.length >
                                                                    0 && (
                                                                        <Col xs={12}>
                                                                            <div className="mb-20">
                                                                                <div className="form-group">
                                                                                    <label>
                                                                                        Attachment
                                                                                        {this.state.taskDetailsFiles.length > 1 ? "s" : ""} : {" "}
                                                                                    </label>
                                                                                    <div className="cqtDetailsDeta-btm">
                                                                                        <ul className="conDocList">
                                                                                            <>
                                                                                                {this.state.taskDetailsFiles.map((file, k) => (
                                                                                                    <li key={k}>
                                                                                                        {process.env.NODE_ENV === "development" ? (
                                                                                                            <LinkWithTooltip
                                                                                                                href="#"
                                                                                                                id="tooltip-1"
                                                                                                            >
                                                                                                                {file.actual_file_name}
                                                                                                            </LinkWithTooltip>
                                                                                                        ) : (
                                                                                                            <LinkWithTooltip
                                                                                                                href="#"
                                                                                                                id="tooltip-1"
                                                                                                            >
                                                                                                                {file.actual_file_name}
                                                                                                            </LinkWithTooltip>
                                                                                                        )}
                                                                                                    </li>
                                                                                                ))}
                                                                                            </>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </Col>
                                                                    )}

                                                                {this.state.taskDetailsFilesCoa.length > 0 && (
                                                                    <Col xs={12}>
                                                                        <div className="mb-20">
                                                                            <div className="form-group">
                                                                                <label>
                                                                                    COA Attachment
                                                                                    {this.state.taskDetailsFilesCoa.length > 1
                                                                                        ? "s"
                                                                                        : ""}
                                                                                    :{" "}
                                                                                </label>
                                                                                <div className="cqtDetailsDeta-btm">
                                                                                    <ul className="conDocList">
                                                                                        {this.state.taskDetailsFilesCoa.map((file, k) => (
                                                                                            <li key={k}>
                                                                                                {process.env.NODE_ENV === "development" ? (
                                                                                                    <LinkWithTooltip
                                                                                                        href="#"
                                                                                                        id="tooltip-1"
                                                                                                        clicked={(e) =>
                                                                                                            this.redirectUrlTask(
                                                                                                                e,
                                                                                                                `${s3bucket_task_diss_path_coa}${file.coa_id}`
                                                                                                            )
                                                                                                        }
                                                                                                    >
                                                                                                        {file.actual_file_name}
                                                                                                    </LinkWithTooltip>
                                                                                                ) : (
                                                                                                    <LinkWithTooltip
                                                                                                        href="#"
                                                                                                        id="tooltip-1"
                                                                                                        clicked={(e) =>
                                                                                                            this.redirectUrlTask(
                                                                                                                e,
                                                                                                                `${s3bucket_task_diss_path_coa}${file.coa_id}`
                                                                                                            )
                                                                                                        }
                                                                                                    >
                                                                                                        {file.actual_file_name}
                                                                                                    </LinkWithTooltip>
                                                                                                )}
                                                                                            </li>
                                                                                        ))}
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </Col>
                                                                )}

                                                            </Row>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </section>

                                    {this.state.shippingInfo.length > 0 ?
                                        <>
                                            <section className="content track-section">
                                                <div className="boxPapanel content-padding">
                                                    {/* Accordion */}
                                                    <h2>Track Shipment Information</h2>
                                                    <Accordion allowZeroExpanded>
                                                        {this.state.shippingInfo.map((value, index) => {
                                                            return (
                                                                <AccordionItem>
                                                                    <AccordionItemHeading>
                                                                        <AccordionItemButton>
                                                                            Invoice #{Object.keys(this.state.shippingInfo[index])}
                                                                        </AccordionItemButton>
                                                                    </AccordionItemHeading>
                                                                    <AccordionItemPanel>
                                                                        <Row>
                                                                            <Col sm="12">
                                                                                {this.state.taskDetailsFilesCoa.length > 0 &&

                                                                                    <div className="row coaDocumentsHead">
                                                                                        <div className="col-md-2"><label className="custom-label"><strong> COA Document </strong></label></div>
                                                                                        <div className="col-md-10 documentsFile">
                                                                                            {this.state.taskDetailsFilesCoa.map((valuecoa, indexcoa) => {
                                                                                                if (valuecoa.invoice_number == Object.keys(this.state.shippingInfo[index])) {
                                                                                                    return (<span className="">
                                                                                                        <a href={s3bucket_task_diss_path_coa + valuecoa.coa_id} target={'_blank'}>
                                                                                                            {valuecoa.actual_file_name}
                                                                                                            <img src={downloadIcon} width="28" height="28" /></a>
                                                                                                    </span>)
                                                                                                }
                                                                                            })
                                                                                            }
                                                                                        </div>



                                                                                    </div>


                                                                                }
                                                                                {this.state.shippingInfo[index][Object.keys(this.state.shippingInfo[index])].partner == 'bluedart' ? this.getBlueDartDynamicContent(index, this.state.shippingInfo) : this.getDynamicContent(index, this.state.shippingInfo)}
                                                                            </Col>
                                                                        </Row>
                                                                    </AccordionItemPanel>
                                                                </AccordionItem>
                                                            )
                                                        })}
                                                    </Accordion>
                                                </div>
                                            </section>
                                        </> : ""
                                    }

                                    {this.state.shippingSTOInfo.length > 0 ?
                                        <>
                                            <section id="shipping_section" className="content track-section" style={{ minHeight: '200px' }}>
                                                <div className="boxPapanel content-padding">
                                                    {/* Accordion */}
                                                    <h2>STO Invoice Information</h2>
                                                    <Accordion allowZeroExpanded>
                                                        {this.state.shippingSTOInfo.map((value, index) => {
                                                            return (
                                                                <AccordionItem>
                                                                    <AccordionItemHeading>
                                                                        <AccordionItemButton>
                                                                            Invoice #{Object.keys(this.state.shippingSTOInfo[index])}
                                                                        </AccordionItemButton>
                                                                    </AccordionItemHeading>
                                                                    <AccordionItemPanel>
                                                                        <Row>
                                                                            <Col sm="12">
                                                                                {this.state.taskDetailsFilesSTOCoa.length > 0 &&

                                                                                    <div className="row coaDocumentsHead">
                                                                                        <div className="col-md-2"><label className="custom-label"><strong> COA Document </strong></label></div>
                                                                                        <div className="col-md-10 documentsFile">
                                                                                            {this.state.taskDetailsFilesSTOCoa.map((valuecoa, indexcoa) => {
                                                                                                if (valuecoa.invoice_number == Object.keys(this.state.shippingSTOInfo[index])) {
                                                                                                    return (<span className="">
                                                                                                        <a href={s3bucket_task_diss_path_coa + valuecoa.coa_id} target={'_blank'}> {valuecoa.actual_file_name}  <img src={downloadIcon} width="28" height="28" /></a>
                                                                                                    </span>)
                                                                                                }
                                                                                            })
                                                                                            }
                                                                                        </div>
                                                                                    </div>
                                                                                }
                                                                                {this.state.shippingSTOInfo[index][Object.keys(this.state.shippingSTOInfo[index])].partner == 'bluedart' ? this.getBlueDartDynamicContent(index, this.state.shippingSTOInfo) : this.getDynamicContent(index, this.state.shippingSTOInfo)}
                                                                            </Col>
                                                                        </Row>
                                                                    </AccordionItemPanel>
                                                                </AccordionItem>
                                                            )
                                                        })}
                                                    </Accordion>
                                                </div>
                                            </section>
                                        </> : ""
                                    }
                                </div>
                            </div>
                        </section>
                    </div>
                </>
            );

        } else {
            if (this.state.invalid_access) {
                return <></>;
            } else {
                return (
                    <>
                        <div className="loderOuter">
                            <div className="loader">
                                <img src={loaderlogo} alt="logo" />
                                <div className="loading">Loading...</div>
                            </div>
                        </div>
                    </>
                );
            }
        }
    }
}

export default withRouter(PlantOrderDetails);
