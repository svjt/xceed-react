import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';
import axios from '../../../shared/axios';

import LoadingSpinner from '../../loader/loadingSpinner';
import drreddylogo from '../../../assets/images/drreddylogo_white.png';
import exceedlogo from '../../../assets/images/Xceed_Logo_Purple_RGB.png';

const initialValues = {
    username: '',
    password: '',
};

var message = '';

class Login extends Component {
    constructor(props) {
        super(props);
        message = this.props.location.state ? this.props.location.state.message : '';
    }

    state = {
        errMsg: null,
        deniedPermissionCookies: false,
        errStatusCode: '',
        website_lang: '',
    };

    componentDidMount() {
        if (localStorage.plant_token !== null && typeof localStorage.plant_token != 'undefined') {
            this.props.history.push('/plant_orders');
        }

        if (message !== null) {
            this.props.history.push({
                pathname: '',
                state: '',
            });
            this.setState({ message: message });
        }
        document.title = `Plant Login | Dr.Reddys API`;
    }

    handleSubmit = (values, actions) => {
        this.setState({ errMsg: '', errStatusCode: '' });

        axios
            .post('/api/tasks/plant_login', { username: values.username, password: values.password })
            .then((res) => {
                localStorage.setItem('plant_token', res.data.token);
                this.props.history.push('/plant_orders');
            })
            .catch((err) => {
                actions.setSubmitting(false);
				if(err.data && err.data.errors){
					actions.setErrors(err.data.errors);
				}
            });
    };

    hideSuccessMsg = () => {
        message = '';
    };

    removeError = (setErrors) => {
        this.setState({ errMsg: '' });
    };

    render() {
        const loginvalidation = Yup.object().shape({
            username: Yup.string().required(`Please enter email address`).email(`Please enter a valid email address`),
            password: Yup.string().required(`Please enter password`),
        });

        const { loading } = this.state;

        return (
            <>
                <div className='login-box'>
                    {loading ? (
                        <LoadingSpinner />
                    ) : (
                        <Formik
                            initialValues={initialValues}
                            validationSchema={loginvalidation}
                            onSubmit={this.handleSubmit}
                        >
                            {({ values, errors, touched, handleChange, isValid, setFieldValue }) => {
                                return (
                                    <Form>
                                        <div className='login-logo'>
                                            <Link to='/' className='logo'>
                                                <span className='logo-mini'>
                                                    <img src={drreddylogo} alt="Dr.Reddy's" />
                                                </span>
                                            </Link>
                                            <br />
                                            <span className='logo-mini'>
                                                <img src={exceedlogo} alt="Dr.Reddy's" height='36px' />
                                            </span>
                                        </div>

                                        <div className='login-box-body'>
                                            {this.state.message && (
                                                <h5
                                                    style={{
                                                        color: '#1bb200',
                                                        padding: '5px 10px',
                                                        textAlign: 'center',
                                                    }}
                                                >
                                                    {this.state.message}
                                                </h5>
                                            )}
                                            <h2>{errors.process}</h2>
                                            <p className='login-box-msg'>Plant Login</p>
                                            <label>Email:</label>
                                            <div className='form-group has-feedback m-b-25'>
                                                <Field
                                                    name='username'
                                                    type='text'
                                                    className='form-control'
                                                    autoComplete='off'
                                                    placeholder='Please enter Email'
                                                    onChange={handleChange}
                                                    autoFocus={this.state.enableFocus}
                                                />

                                                {errors.username && touched.username && (
                                                    <label>{errors.username}</label>
                                                )}
                                            </div>
                                            <label>Password:</label>
                                            <div className='form-group has-feedback'>
                                                <Field
                                                    name='password'
                                                    type='password'
                                                    className='form-control'
                                                    autoComplete='off'
                                                    placeholder='Please enter Password'
                                                    onChange={handleChange}
                                                    autoFocus={this.state.enableFocus}
                                                />
                                                {errors.password && touched.password && (
                                                    <label>{errors.password}</label>
                                                )}
                                            </div>
                                            <div className='row'>
                                                <div className='col-xs-8'>
                                                    <Link to={'/plant_forgot_password'}>Forgot password?</Link>
                                                </div>
                                                <div className='col-xs-4'>
                                                    <button className='btn-fill' type='submit'>
                                                        Login
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </Form>
                                );
                            }}
                        </Formik>
                    )}
                </div>
            </>
        );
    }
}

export default Login;
