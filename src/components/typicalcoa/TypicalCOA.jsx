import React, { Component } from "react";
import Pagination from "react-js-pagination";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal
} from "react-bootstrap";
import { Link } from "react-router-dom";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import { htmlDecode } from "../../shared/helper";
import API from "../../shared/axios";
import Select from "react-select";
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
import { showErrorMessageFront } from "../../shared/handle_error_front";
import { Editor } from "@tinymce/tinymce-react";
import swal from "sweetalert";
import { Next } from "react-bootstrap/lib/Pagination";
import Autosuggest from "react-autosuggest";
const s3bucket_task_coa_path = `${process.env.REACT_APP_API_URL}/api/feed/download_typical_coa/`;

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
const custContent = () => cell => {
  //console.log(cell);
  return (<div data-toggle="tooltip" data-placement="top" title={htmlDecode(cell)}>{htmlDecode(cell)}</div>);
};
const custArr = () => cell => {
  const listItems = cell.map((d) => <li key={d}>{d}</li>);
  return (<div data-toggle="tooltip" data-placement="top" title={cell}>{listItems}</div>);
}
const custStatus = () => cell => {
  if (cell === 1) {
    return (<div className="text-success">Active</div>)
  } else {
    return (<div className="text-danger">Inactive</div>)
  }

};
const initialValues = {
  comment: ''
};
const actionFormatter = refObj => cell => {
  //  console.log("this is ref obj ++++++++++++++++",refObj.state.typicalCoa);return false;
  const current_coa = refObj.state.typicalCoa.filter((data) => cell == data.id);
  return (

    <div className="actionStyle">

      {current_coa[0].file_name != '' && current_coa[0].status == 1 ?

        <div className="actionStyle">

          <LinkWithTooltip
            tooltip="Click to Mail"
            href="#"
            clicked={e => refObj.showMailPopup(e, cell)}
            id="tooltip-1"
          >
            <i className="fa fa-envelope" />
          </LinkWithTooltip>
          {" "}
          <LinkWithTooltip
            tooltip="Click to Download"
            href="#"
            clicked={(e) =>
              refObj.redirectUrlTask(
                e,
                `${s3bucket_task_coa_path}${cell}`
              )
            }
            id="tooltip-1"
          >
            <i className="fas fa-download" />

          </LinkWithTooltip>

        </div>

        : current_coa[0].status == 0 ?
          <div className="actionStyle">
            <LinkWithTooltip
              tooltip="Click to Mail"
              href="#"
              clicked={e => refObj.showMailPopup(e, cell)}
              id="tooltip-1"
            >
              {" "}<i className="fa fa-envelope" />
            </LinkWithTooltip>
          </div> : null}
    </div>
  );
};
const validateStopFlag = {};

class TypicalCOA extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showLoader: false,
      product_code: '',
      country: [],
      pharmacopeial: '',
      pharmacopeial_id: '',
      country_code: '',
      defaultValue_pharmacopeial: '',
      defaultValue_country: '',
      defaultValue_specification: '',
      defaultValue_product: '',
      defaultValue_status: '',
      product_code_auto_suggest: '',
      specification: '',
      typicalCoa: '',
      search_country_code: '',
      search_product_code: '',
      search_specification: '',
      search_specification: '',
      search_status: '',
      selectStatus: [
        { value: "0", label: "Inactive" },
        { value: "1", label: "Active" }
      ],
      count: '',
      showLoader: false,
      remove_search: false,
      isListView: false,
      itemPerPage: 10,
      activePage: 1,
      value: "", // for auto-suggest
      suggestions: [], // for auto-suggest
      ref_loader: false, // for auto-suggest
      showSendMailPopup: false,
      comment: '',
      typicalCoaflagId: 0
    };
  }
  componentDidMount() {

    API.get(`/api/feed/get_sap_countries`)
      .then((res) => {
        // console.log("country arr :",res);
        this.setState({ country: res.data.data });
      })
      .catch((err) => {
        //console.log(err);
      });
    API.get(`/api/feed/get_sap_pharmacopeial`)
      .then((res) => {
        this.setState({ pharmacopeial: res.data.data });
        // console.log("hello ++++++",this.state.pharmacopeial);
      })
      .catch((err) => {
        //console.log(err);
      });
    this.getCOAList();

  }
  filterSearchPharmacopeial = (e) => {

    if (e != null) {
      this.setState({
        showLoader: false,
        defaultValue_pharmacopeial: e

      });
    } else {
      this.setState({
        showLoader: false,
        defaultValue_pharmacopeial: ''

      });
    }
  }
  filterSearchstatus = (e) => {
    // console.log("this is e ::",e);

    if (e != null) {
      this.setState({
        showLoader: false,
        defaultValue_status: e.value

      });
    } else {
      this.setState({
        showLoader: false,
        defaultValue_status: ''

      });
    }
  }
  filterSearchcountry = (e) => {
    // console.log("this is e",e);
    if (e != null) {
      this.setState({
        showLoader: false,
        defaultValue_country: e

      });
    } else {
      this.setState({
        showLoader: false,
        defaultValue_country: ''

      });
    }

  }
  searchSpecification = (e) => {
    this.setState({
      showLoader: false,
      defaultValue_specification: e

    });
  }
  searchProduct = (e) => {
    this.setState({
      showLoader: false,
      defaultValue_product: e

    });
  }
  clearSearch = () => {
    document.getElementById('country').value = ''
    document.getElementById('pharmacopeial').value = ''
    document.getElementById('status').value = ''
    document.getElementById('specification').value = ''
    document.getElementById('product_code').value = ''

    this.setState({
      search_pharmacopeial: '',
      search_country_code: '',
      search_product_code: '',
      search_status: '',
      product_code: '',
      search_specification: '',
      remove_search: false,
      isListView: false,
      count: ''
    }, () => {
      this.setState({ activePage: 1 })
    })
  }
  getCOAList(page = 1) {
    var country = this.state.search_country_code
    var pharmacopeial = this.state.search_pharmacopeial
    var specification = this.state.search_specification
    var product_code = this.state.search_product_code
    var status = this.state.search_status
    API.get(`/api/feed/typical_coa_list_emp?page=${page}&country_code=${encodeURIComponent(country)}&specification=${encodeURIComponent(specification)}&pharmacopeial_id=${encodeURIComponent(pharmacopeial)}&product_code=${encodeURIComponent(product_code)}&status=${encodeURIComponent(status)}`)
      .then(res => {
        this.setState({
          typicalCoa: res.data.data,
          count: res.data.count_typicalCoa,
          showLoader: false,
          search_pharmacopeial: pharmacopeial,
          search_country_code: country,
          search_product_code: product_code,
          search_specification: specification,
          search_status: status,
          remove_search: false
        });
      })
      .catch(err => {
        this.setState({
          showLoader: false
        });
        showErrorMessageFront(err, this.props);
      });
    this.setState({
      showLoader: false
    });
  }
  coaSearch = (e, page = 1) => {
    e.preventDefault();
    //   console.log("++++++",document.getElementById('product_code'));
    var country_code = document.getElementById('country').value
    var pharmacopeial_code = document.getElementById('pharmacopeial').value
    var status = document.getElementById('status').value;
    var specification = document.getElementById('specification').value
    var product_code = document.getElementById('product_code').value

    if (country_code === "" && pharmacopeial_code === "" && specification === "" && product_code === "" && status === "") {
      return false;
    }


    API.get(`/api/feed/typical_coa_list_emp?page=${page}&country_code=${encodeURIComponent(country_code)}&specification=${encodeURIComponent(specification)}&pharmacopeial_id=${encodeURIComponent(pharmacopeial_code)}&product_code=${encodeURIComponent(product_code)}&status=${encodeURIComponent(status)}`)
      .then(res => {
        this.setState({
          typicalCoa: res.data.data,
          count: res.data.count_typicalCoa,
          showLoader: false,
          search_pharmacopeial: pharmacopeial_code,
          search_country_code: country_code,
          search_product_code: product_code,
          search_specification: specification,
          search_status: status,
          remove_search: true,
          isListView: true,
          activePage: 1
        });
      })
      .catch(err => {
        this.setState({
          showLoader: false
        });
        showErrorMessageFront(err, this.props);
      });
  }
  showMailPopup = (event, id) => {
    this.setState({
      showSendMailPopup: true,
      typicalCoaflagId: id,
    });
  };
  handleSubmit = (values, actions) => {
    //console.log("this is comment : ",values.comment);
    const post_data = {
      subject: values.subject,
      comment: values.comment
    };
    this.setState({ showSendMailPopup: true });
    const id = this.state.typicalCoaflagId;
    //console.log("this is id : ",id);
    API.post(`/api/feed/email_for_inactive_COA/${id}`, post_data)
      .then(res => {
        this.handleSendMailClose();
        //console.log("response", res);
        swal({
          closeOnClickOutside: false,
          title: res.data.status == 1 ? "Success" : "Error",
          text: res.data.message,
          icon: "success"
        }).then(() => {
          //window.location.reload();
          this.setState({ showSendMailPopup: false });
        });
      })
      .catch(err => {
        this.setState({ showSendMailPopup: false });
        actions.setErrors(err.data.errors);
        actions.setSubmitting(false);
      });

  };
  handleSendMailClose = () => {
    this.setState({
      showSendMailPopup: false,
      typicalCoaflagId: 0
    });
  };
  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getCOAList(pageNumber > 0 ? pageNumber : 1);
  };



  // getAutoSuggestionProduct = (material) => {
  //     if (material && material.length > 2) {
  //         API.post(`/api/feed/get_sap_Product_code`, { material })
  //             .then((res) => {

  //                 let ret_html = res.data.data.map((val, index) => {
  //                     return (<li style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => this.setState({ product_code: val.value, product_code_auto_suggest:'' })} >{val.label}<hr style={{ marginTop: '6px', marginBottom: '0px' }} /></li>)
  //                 })
  //                 this.setState({ product_code_auto_suggest: ret_html })
  //             })
  //             .catch((err) => {
  //                 console.log(err);
  //             });
  //     }
  // }
  redirectUrlTask = (event, path) => {
    event.preventDefault();
    window.open(path, "_blank");
  }

  setMaterialID = (event) => {
    this.setState({ product_code: event.target.value.toString() });


    if (event.target.value && event.target.value.length > 2) {
      API.post(`/api/feed/get_sap_Product_code`, { material_id: event.target.value })
        .then((res) => {

          let ret_html = res.data.data.map((val, index) => {
            return (<li style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => this.setMaterialIDAutoSuggest(val.value)} >{val.label}<hr style={{ marginTop: '6px', marginBottom: '0px' }} /></li>)
          })
          this.setState({ product_code_auto_suggest: ret_html });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  setMaterialIDAutoSuggest = (event) => {
    this.setState({ product_code_auto_suggest: '', product_code: event.toString() });
  };

  render() {
    let validateEmail = Yup.object().shape({
      comment: Yup.string().trim().required("Please enter description"),
      subject: Yup.string().trim().required("Please enter subject")
    })
    if (this.state.showLoader) {
      return (
        <div className="loderOuter">
          <div className="loader">
            <img src={loaderlogo} alt="logo" />
            <div className="loading">Loading...</div>
          </div>
        </div>
      );
    } else {
      return (
        <>


          <div className="content-wrapper">
            <section className="content-header">
              <h1>SEARCH TYPICAL COA</h1>
            </section>
            <section className="content">
              <div className="row">
                <form className='form'>
                  <div className="clearfix serchapanel">
                    <ul>
                      <li>
                        <div
                          className="form-group"
                          onMouseLeave={(e) => {
                            if (this.state.product_code_auto_suggest != '') {
                              document.getElementById(`product_code`).blur();
                              this.setState({ product_code_auto_suggest: '' });
                            }
                          }}
                        >
                          <label>Product</label>
                          <input
                            type="text"
                            name="product_code"
                            id="product_code"
                            className="form-control"
                            style={{ textTransform: "uppercase" }}
                            //onChange={(e)=>this.searchProduct(e)}
                            value={
                              this.state.product_code !== null &&
                                this.state.product_code !== ""
                                ? this.state.product_code
                                : ""
                            }
                            onChange={(e) => {
                              this.setMaterialID(e);
                            }}

                            onFocus={(e) => {
                              if (this.state.product_code != '' && this.state.product_code.length > 2) {
                                API.post(`/api/feed/get_sap_Product_code`, { material_id: this.state.product_code })
                                  .then((res) => {

                                    let ret_html = res.data.data.map((val, index) => {
                                      return (<li style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => this.setMaterialIDAutoSuggest(val.value)} >{val.label}<hr style={{ marginTop: '6px', marginBottom: '0px' }} /></li>)
                                    })
                                    this.setState({ product_code_auto_suggest: ret_html });
                                  })
                                  .catch((err) => {
                                    console.log(err);
                                  });
                              }
                            }}

                          />
                          {this.state.product_code_auto_suggest != '' && <ul id={`material_id_auto_suggect`} style={{ zIndex: '30', position: 'absolute', backgroundColor: '#d0d0d0', height: "100px", overflowY: 'auto', listStyleType: 'none', width: '100%' }} >
                            <li>{this.state.product_code_auto_suggest}</li>
                          </ul>}
                        </div>
                      </li>

                      {this.state.country && (
                        <li>
                          <div className="form-group  has-feedback">
                            <label>Market</label>
                            <select name="country" id="country" className="form-control">
                              <option value="">Select country</option>
                              {this.state.country.map(
                                (country, i) => (
                                  <option key={i} value={country.value}>
                                    {country.label}
                                  </option>
                                )
                              )}
                            </select>
                          </div>
                        </li>
                      )}
                      {this.state.pharmacopeial && (
                        <li>
                          <div className="form-group  has-feedback">
                            <label>Pharmacopeial</label>
                            <select name="pharmacopeial" id="pharmacopeial" className="form-control">
                              <option value="">Select pharmacopeial</option>
                              {this.state.pharmacopeial.map(
                                (pharmacopeial, i) => (
                                  <option key={i} value={pharmacopeial.value}>
                                    {pharmacopeial.label}
                                  </option>
                                )
                              )}
                            </select>
                          </div>
                        </li>
                      )}
                      <li>
                        <div className="form-group">
                          <label>Specification</label>
                          <input
                            type="text"
                            name="specification"
                            id="specification"
                            className="form-control"
                            style={{ textTransform: "uppercase" }}
                          //onChange={(e)=>this.searchSpecification(e)}

                          />
                        </div>
                      </li>
                      <li>
                        <div className="form-group  has-feedback">
                          <label>Status</label>
                          <select name="status" id="status" className="form-control">
                            <option value="">Select status</option>
                            {this.state.selectStatus.map(
                              (status, i) => (
                                <option key={i} value={status.value}>
                                  {status.label}
                                </option>
                              )
                            )}
                          </select>
                        </div>
                      </li>
                      <li>
                        <div class="searchBtnGroup">
                          <button type="button" className="btn btn-search" value="Search" onClick={(e) => this.coaSearch(e)}>Search</button>
                          {this.state.remove_search ? <button type="button" className="btn btn-remove" value="Search" onClick={() => this.clearSearch()}>Remove</button> : ''}
                        </div>
                      </li>
                    </ul>
                  </div>
                </form>
                {this.state.isListView ? <div className="clearfix"></div> : <div className="clearfix"><b>Note :</b> Please use Search panel to view the list of COA data . </div>}
                
              </div>


              {this.state.isListView ?
                <div className="serchapanel">
                  <div className="box-body">
                    <BootstrapTable data={this.state.typicalCoa}>

                      <TableHeaderColumn isKey dataField="product" dataFormat={custContent(this)}>
                        Product
                      </TableHeaderColumn>
                      <TableHeaderColumn dataField="country" dataFormat={custArr(this)}>
                        Market
                      </TableHeaderColumn>
                      <TableHeaderColumn dataField="pharmacopeial" dataFormat={custContent(this)} width='20%'>
                        Pharmacopeial
                      </TableHeaderColumn>
                      <TableHeaderColumn dataField="specification_number" dataFormat={custContent(this)}>
                        Specification number
                      </TableHeaderColumn>
                      <TableHeaderColumn dataField="status" dataFormat={custStatus(this)}>
                        Status
                      </TableHeaderColumn>
                      <TableHeaderColumn
                        dataField="id"
                        dataFormat={actionFormatter(this)}
                      >
                        Action
                      </TableHeaderColumn>

                    </BootstrapTable>
                    {this.state.showSendMailPopup === true && (
                      <Modal
                        show={this.state.showSendMailPopup}
                        onHide={this.handleSendMailClose}
                        backdrop="static"
                        className="grantmodal"
                      >
                        <Formik
                          initialValues={initialValues}
                          validationSchema={validateEmail}
                          onSubmit={this.handleSubmit}
                        >
                          {({
                            values,
                            errors,
                            touched,
                            isValid,
                            isSubmitting,
                            handleChange,
                            setFieldValue,
                            setFieldTouched,
                            setErrors,
                          }) => {
                            return (
                              <Form>
                                <Modal.Header closeButton className="respondcus">
                                  <Modal.Title>
                                    Send Email
                                  </Modal.Title>

                                </Modal.Header>
                                <Modal.Body>
                                  <div className="contBox">
                                    <Row>
                                      <Col xs={12} sm={12} md={12}>
                                        <div className="form-group">
                                          <label>
                                            Subject<span className="impField">*</span>
                                          </label>
                                          <Field
                                            name="subject"
                                            type="text"
                                            className={`form-control`}
                                            placeholder="Enter subject"
                                            autoComplete="off"
                                            value={values.subject}
                                          />
                                          {errors.subject && touched.subject ? (
                                            <span className="errorMsg">
                                              {errors.subject}
                                            </span>
                                          ) : null}
                                        </div>
                                      </Col>
                                    </Row>
                                    <Row>
                                      <div className="form-group">

                                        <Col xs={12} sm={12} md={12}>
                                          <label>
                                            Content<span className="impField">*</span>
                                          </label>
                                          <Editor
                                            name="comment"
                                            content={values.comment}
                                            init={{
                                              menubar: false,
                                              branding: false,
                                              placeholder: 'Enter Comments *',
                                              plugins:
                                                "link table hr visualblocks code placeholder lists",
                                              toolbar:
                                                "bold italic strikethrough superscript subscript | alignleft aligncenter alignright alignjustify | numlist bullist | removeformat underline | link unlink | blockquote table  hr | visualblocks code ",
                                              content_css: ["/css/editor.css"],
                                            }}
                                            onEditorChange={(value) => {
                                              setFieldValue("comment", value);
                                              this.setState({
                                                comment: value
                                              });
                                            }}

                                          />
                                        </Col>
                                      </div>
                                    </Row>
                                  </div>
                                </Modal.Body>
                                <Modal.Footer>
                                  <button
                                    className={`btn-fill ${isValid ? "btn-custom-green" : "btn-disable"
                                      } m-r-10`}
                                    type="submit"
                                    disabled={isValid ? false : false}
                                  >
                                    Send Mail
                                  </button>
                                  <button
                                    onClick={e => this.handleSendMailClose()}
                                    className={`btn-line`}
                                    type="button"
                                  >
                                    Cancel
                                  </button>
                                </Modal.Footer>
                              </Form>
                            );
                          }}
                        </Formik>
                      </Modal>
                    )}
                  </div>

                </div> : null}


              {this.state.count > this.state.itemPerPage ? (
                <Row>
                  <Col md={12}>
                    <div className="paginationOuter text-right">
                      <Pagination
                        activePage={this.state.activePage}
                        itemsCountPerPage={this.state.itemPerPage}
                        totalItemsCount={this.state.count}
                        itemClass='nav-item'
                        linkClass='nav-link'
                        activeClass='active'
                        onChange={this.handlePageChange}
                      />
                    </div>
                  </Col>
                </Row>

              ) : null}


            </section>
          </div>
        </>
      )
    }
  }
}

export default TypicalCOA;