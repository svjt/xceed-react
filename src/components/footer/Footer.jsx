import React, { Component } from 'react';
import dateFormat from "dateformat";

// const io = require('socket.io-client');

// const socket = io(`http://testmymobileapp.com:6301`, {
//     transportOptions: {
//       polling: {
//         extraHeaders: {
//           'Authorization': `Bearer ${localStorage.getItem('token')}`,
//           'Content-Type':'application/json'
//         },
//       },
//     },
//   });

//   socket.on('connect', () => {
//     console.log('connected!');
//   });

class Footer extends Component {
    
    render() {
        
        if( this.props.isLoggedIn === false) return null;
        return ( 
            <footer className="main-footer">
                Copyright {dateFormat(new Date(), "yyyy")}. Dr.Reddy’s Laboratories Ltd. All rights reserved. <span className="footerVersion">(v2.9.0)</span>
                
            </footer>
        );
    }
}
 
export default Footer;