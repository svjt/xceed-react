import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "../../shared/axios";
import InfiniteScroll from "react-infinite-scroller";
import Select from "react-select";
import Autocomplete from "react-autocomplete";
import productList from "../../assets/File/Dr.-Reddy’s-API-Product-List.pdf";
import portfolioProductList from "../../assets/File/API+Portfolio-Product-List.pdf";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
// import Loader from 'react-loader-spinner';

var product_url = process.env.REACT_APP_PORTAL_URL;

class ProductCatalog extends Component {
  state = {
    therapyCategory: [],
    dosageForm: [],
    developmentStage: [],
    apiTechnology: [],
    products: [],
    errMsg: null,
    showLoader: true,
    page: 0,
    hasMore: false,
    title: "",
    selectedCategory: [],
    selectedDosage: [],
    selectedApi: [],
    selectedStage: [],
    query: "",
    reset: false,
    value: "",
    autocompleteOptions: [],
    api_product_file: "",
    generic_product_file: "",
    no_products: false,
    //selectedOption: null,
  };
  componentDidMount() {
    this.getTherapyCategory();
    this.getDosageForm();
    this.getDevelopmentStage();
    this.getApiTechnology();
    this.getProductList();
    this.getProductTitles();
    document.title = "Products | Dr.Reddys API";
  }
  fetchMoreData = () => {
    // a fake async api call like which sends
    // 20 more records in 1.5 secs
    // let query = this.state.query;
    let title = "";
    let development_status = [];
    let api_technology = [];
    let dose_form = [];
    let therapy_category = [];

    if (
      this.state.selectedCategory !== [] &&
      this.state.selectedCategory !== null
    ) {
      this.state.selectedCategory.map((category, key) =>
        // query = query + '&therapy_category[]=' + category.value
        therapy_category.push({ id: category.value })
      );
    }
    if (
      this.state.selectedDosage !== [] &&
      this.state.selectedDosage !== null
    ) {
      this.state.selectedDosage.map((dosage, key) =>
        // query = query + '&formulation_nfc[]=' + dosage.value
        dose_form.push({ id: dosage.value })
      );
    }
    if (this.state.selectedApi !== [] && this.state.selectedApi !== null) {
      this.state.selectedApi.map((api, key) =>
        // query = query + '&api_technology[]=' + api.value
        api_technology.push({ id: api.value })
      );
    }
    if (this.state.selectedStage !== [] && this.state.selectedStage !== null) {
      this.state.selectedStage.map((stage, key) =>
        // query = query + '&development_status[]=' + stage.value
        development_status.push({ id: stage.value })
      );
    }
    if (this.state.title !== "") {
      title = this.state.title;
    }
    //this.setState({no_products:true});
    //console.log('query', query);
    // axios.get(product_url + '/ext/api/get-products?_format=json' + query)
    let filter_data = {
      title: title,
      development_status: development_status,
      api_technology: api_technology,
      dose_form: dose_form,
      therapy_category: therapy_category,
    };
    setTimeout(() => {
      axios
        .post("api/products/list_all", filter_data)
        .then((res) => {
          this.setState({
            products: this.state.products.concat(res.data.data),
          });
          this.setState({ page: this.state.page + 1 });
          this.setState({ no_products: false });
          this.setState({ showLoader: false });
          // if (res.data.length < 20) {
          //     this.setState({ hasMore: false })
          // }
        })
        .catch((err) => {});
    }, 1500);
  };

  getTherapyCategory() {
    // axios.get(product_url + 'ext/api/get-terms?_format=json&vid=therapy_area')
    axios
      .get("api/products/category")
      .then((res) => {
        this.setState({ therapyCategory: res.data.data });
      })
      .catch((err) => {});
  }
  getDosageForm() {
    // axios.get(product_url + 'ext/api/get-terms?_format=json&vid=formulation_type')
    axios
      .get("api/products/dosage")
      .then((res) => {
        this.setState({ dosageForm: res.data.data });
      })
      .catch((err) => {});
  }
  getDevelopmentStage() {
    // axios.get(product_url + 'ext/api/get-terms?_format=json&vid=development_stage')
    axios
      .get("api/products/stage")
      .then((res) => {
        this.setState({ developmentStage: res.data.data });
      })
      .catch((err) => {});
  }
  getApiTechnology() {
    // axios.get(product_url + 'ext/api/get-terms?_format=json&vid=api_technology')
    axios
      .get("api/products/technology")
      .then((res) => {
        this.setState({ apiTechnology: res.data.data });
      })
      .catch((err) => {});
  }
  getProductTitles() {
    // axios.get(product_url + 'ext/api/get-product-title?_format=json&vid=api_technology')
    axios
      .get("api/products/all")
      .then((res) => {
        this.setState({ autocompleteOptions: res.data.data });
      })
      .catch((err) => {});
  }
  getProductList() {
    this.setState({ page: 0 });
    this.setState({ hasMore: false });
    this.setState({ query: "" });
    this.setState({ products: [] });
    this.setState({ selectedCategory: [] });
    this.setState({ selectedDosage: [] });
    this.setState({ selectedApi: [] });
    this.setState({ selectedStage: [] });
    this.setState({ title: "" });
    this.setState({ reset: false });
    this.setState({ showLoader: true });
    this.setState({ api_product_file: "" });
    this.setState({ generic_product_file: "" });
    //this.setState({no_products:true});
    axios
      .post("api/products/list_all")
      .then((res) => {
        if (res.data.data.length > 0) {
          this.setState({ products: res.data.data });
          this.setState({ page: this.state.page + 1 });
          this.setState({ no_products: false });
          this.setState({ showLoader: false });
        } else {
          this.setState({ showLoader: false });
          this.setState({ no_products: true });
        }
        this.setState({ api_product_file: res.data.api_product_file });
        this.setState({ generic_product_file: res.data.generic_product_file });
        // if (res.data.length < 20) {
        //     this.setState({ hasMore: false })
        // }
      })
      .catch((err) => {
        this.setState({ showLoader: false });
        this.setState({ no_products: true });
        if (err.data.api_product_file !== "") {
          this.setState({ api_product_file: err.data.api_product_file });
        }
        if (err.data.generic_product_file !== "") {
          this.setState({
            generic_product_file: err.data.generic_product_file,
          });
        }
      });
  }
  handleTitleChange = (evt) => {
    console.log("Title", evt.target.value);
    this.setState({ title: evt.target.value });
  };
  updateOptionList = (evt) => {
    console.log("Title", evt.target.value);
    //this.setState({ title: evt.target.value })
  };
  handleCategoryChange = (selectedOption) => {
    this.setState({ selectedCategory: selectedOption });
  };
  handleDosageChange = (selectedOption) => {
    this.setState({ selectedDosage: selectedOption });
  };
  handleDevelopmentChange = (selectedOption) => {
    this.setState({ selectedStage: selectedOption });
  };
  handleApiChange = (selectedOption) => {
    this.setState({ selectedApi: selectedOption });
  };

  handleSubmit() {
    this.setState({ products: [] });
    this.setState({ hasMore: false });
    this.setState({ showLoader: true });
    this.setState({ page: 0 });
    this.setState({ query: "" });
    this.setState({ reset: true });
    let query = "";
    let title = "";
    let development_status = [];
    let api_technology = [];
    let dose_form = [];
    let therapy_category = [];

    if (
      this.state.selectedCategory !== [] &&
      this.state.selectedCategory !== null
    ) {
      this.state.selectedCategory.map((category, key) =>
        // query = query + '&therapy_category[]=' + category.value
        therapy_category.push({ id: category.value })
      );
    }
    if (
      this.state.selectedDosage !== [] &&
      this.state.selectedDosage !== null
    ) {
      this.state.selectedDosage.map((dosage, key) =>
        // query = query + '&formulation_nfc[]=' + dosage.value
        dose_form.push({ id: dosage.value })
      );
    }
    if (this.state.selectedApi !== [] && this.state.selectedApi !== null) {
      this.state.selectedApi.map((api, key) =>
        // query = query + '&api_technology[]=' + api.value
        api_technology.push({ id: api.value })
      );
    }
    if (this.state.selectedStage !== [] && this.state.selectedStage !== null) {
      this.state.selectedStage.map((stage, key) =>
        // query = query + '&development_status[]=' + stage.value
        development_status.push({ id: stage.value })
      );
    }
    if (this.state.title !== "") {
      title = this.state.title;
    }
    // this.setState({ query: query })
    // console.log("QUERY", query);
    //this.setState({no_products:true});
    //console.log('query', query);
    // axios.get(product_url + '/ext/api/get-products?_format=json' + query)
    let filter_data = {
      title: title,
      development_status: development_status,
      api_technology: api_technology,
      dose_form: dose_form,
      therapy_category: therapy_category,
    };
    axios
      .post("api/products/list_all", filter_data)
      .then((res) => {
        if (res.data.data.length > 0) {
          this.setState({ products: res.data.data });
          this.setState({ page: this.state.page + 1 });
          this.setState({ no_products: false });
          this.setState({ showLoader: false });
          if (res.data.length < 20) {
            this.setState({ hasMore: false });
          }
        } else {
          this.setState({ showLoader: false });
          this.setState({ no_products: true });
        }
      })
      .catch((err) => {
        this.setState({ showLoader: false });
        this.setState({ no_products: true });
      });
  }
  render() {
    var style = {
      //marginTop: '20px',
      display: "flex",
      justifyContent: "flex-start",
      flexWrap: "wrap",
      width: "100%",
    };
    return (
      <>
        <div className="content-wrapper">
          <section className="content">
            <div className="productSec">
              <div className="dashboard-content-sec">
                <div>
                  <div
                    id="block-reddy-admin-content"
                    className="block block-system block-system-main-block"
                  >
                    <div className="row">
                      <div className="col-md-12">
                        <div className="float-right text-right">
                          <a
                            href={this.state.api_product_file}
                            download
                            className="download-product"
                            target="_blank"
                          >
                            API Product List
                          </a>
                          <a
                            href={this.state.generic_product_file}
                            className="download-product"
                            download
                            target="_blank"
                          >
                            Generics Product List
                          </a>
                        </div>
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            flexWrap: "nowrap",
                            justifyContent: "space-between",
                          }}
                        ></div>
                        <div className="product-search-section">
                          <h2>Search your Products</h2>
                          {/* <input placeholder="Search by Product Name..." className="customInput" onChange={this.handleTitleChange} value={this.state.title}/> */}
                          <div className="search-panel">
                            <Autocomplete
                              inputProps={{
                                placeholder: "Search by product name...",
                              }}
                              items={this.state.autocompleteOptions}
                              shouldItemRender={(item, title) =>
                                item.label
                                  .toLowerCase()
                                  .indexOf(title.toLowerCase()) > -1
                              }
                              getItemValue={(item) => item.label}
                              renderItem={(item, highlighted) => (
                                <div
                                  key={item.id}
                                  style={{
                                    backgroundColor: highlighted
                                      ? "#eee"
                                      : "transparent",
                                  }}
                                >
                                  {item.label}
                                </div>
                              )}
                              value={this.state.title}
                              onChange={(e) =>
                                this.setState({ title: e.target.value })
                              }
                              onSelect={(title) => this.setState({ title })}
                              //onKeyUp={this.updateOptionList}
                            />
                          </div>
                          <div className="clearfix"></div>
                          <div className="row">
                            <div className="col-md-10 searchCategories">
                              <div className="row">
                                <div className="col-md-3">
                                  {/* <select className="form-control customeSelect" >
                                                                    <option></option>
                                                                </select> */}
                                  <Select
                                    isMulti
                                    value={this.state.selectedCategory}
                                    placeholder="Therapeutic Category"
                                    onChange={this.handleCategoryChange}
                                    options={this.state.therapyCategory}
                                  />
                                </div>
                                <div className="col-md-3">
                                  <Select
                                    isMulti
                                    value={this.state.selectedDosage}
                                    placeholder="Dosage Form"
                                    onChange={this.handleDosageChange}
                                    options={this.state.dosageForm}
                                  />
                                </div>
                                <div className="col-md-3">
                                  <Select
                                    isMulti
                                    value={this.state.selectedStage}
                                    placeholder="Development Stage"
                                    onChange={this.handleDevelopmentChange}
                                    options={this.state.developmentStage}
                                  />
                                </div>
                                <div className="col-md-3">
                                  <Select
                                    isMulti
                                    value={this.state.selectedApi}
                                    placeholder="API Technology"
                                    onChange={this.handleApiChange}
                                    options={this.state.apiTechnology}
                                  />
                                </div>
                                <div className="clearfix"></div>
                              </div>
                            </div>
                            <div className="col-md-2 pl-0">
                              <input
                                type="submit"
                                value="Search"
                                className="product-search-btn"
                                onClick={() => this.handleSubmit()}
                              />
                              {this.state.reset ? (
                                <input
                                  type="submit"
                                  value="Reset"
                                  className="product-reset-btn"
                                  onClick={() => this.getProductList()}
                                />
                              ) : (
                                ""
                              )}
                            </div>
                            <div className="clearfix"></div>
                          </div>
                        </div>
                      </div>
                    </div>

                    {
                      <div style={style}>
                        {
                          // console.log("Products",this.state),
                          this.state.showLoader ? (
                            <div className="loderOuter">
                              <div className="loader">
                                <img src={loaderlogo} alt="logo" />
                                <div className="loading">Loading...</div>
                              </div>
                            </div>
                          ) : this.state.no_products ? (
                            <>
                              <div className="col-md-12 mb-20">
                                <span>
                                  Don't see your product here?{" "}
                                  <Link
                                    to={{ pathname: `/contact-us` }}
                                    style={{
                                      cursor: "pointer",
                                      color: "#5225B5",
                                      textDecoration: "underline",
                                    }}
                                  >
                                    Contact Us
                                  </Link>{" "}
                                  to explore product development opportunities.
                                </span>
                              </div>
                              <div className="col-md-12">
                                <center>
                                  <b>
                                    No Product found. Please refine your search.
                                  </b>
                                </center>
                              </div>
                            </>
                          ) : (
                            this.state.products &&
                            this.state.products.map((product, productKey) => (
                              <div
                                key={productKey}
                                className="col-lg-3 col-md-6 col-sm-12 boxWrap"
                              >
                                <div className="products-box">
                                  <div className="products-content">
                                    <h3>{product.title}</h3>
                                    <p>
                                      Therapy Area{" "}
                                      <span>{product.therapy_area}</span>
                                    </p>
                                    <div className="know-more-btn">
                                      <Link
                                        to={
                                          "/user/product-details/" + product.id
                                        }
                                      >
                                        Know More
                                      </Link>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            ))
                          )
                        }
                      </div>
                    }
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </>
    );
  }
}

export default ProductCatalog;
