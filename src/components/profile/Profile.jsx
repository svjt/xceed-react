import React, { Component } from "react";
import { Row, Col } from "react-bootstrap";
import { Formik, Field, Form } from "formik";
import { Link } from "react-router-dom";
import editImage from "../../assets/images/image-edit.jpg";

import { getMyId,getMyPic } from "../../shared/helper";
import API from "../../shared/axios";
import * as Yup from "yup";
import swal from "sweetalert";

const initialValues = {
  first_name: "",
  last_name: "",
  email: "",
  phone_no: ""
};

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      employeeDetails: []
    };
  }

  componentDidMount() {
    // var my_id = getMyId(localStorage.token);
    // API.get(`/api/employees/${my_id}`)
    //   .then(res => {
    //     this.setState({
    //       employeeDetails: res.data.data
    //     });
    //   })
    //   .catch(err => {
    //     console.log(err);
    //   });
  }

  // handleSubmitEvent = (values, actions) => {
  //   var my_id = getMyId(localStorage.token);
  //   const post_data = {
  //     first_name: values.first_name,
  //     last_name: values.last_name,
  //     email: values.email,
  //     phone_no: values.phone_no
  //   };

  //   API.put(`/api/employees/${my_id}`, post_data)
  //     .then(res => {
  //       this.modalCloseHandler();
  //       swal({
  //         closeOnClickOutside: false,
  //         title: "Success",
  //         text: "Record updated successfully.",
  //         icon: "success"
  //       }).then(() => {
  //         this.getEmployeeList();
  //       });
  //     })
  //     .catch(err => {
  //       actions.setErrors(err.data.errors);
  //       actions.setSubmitting(false);
  //     });
  // };

  render() {
    // const { employeeDetails } = this.state;

    // const newInitialValues = Object.assign(initialValues, {
    //   first_name: employeeDetails.first_name ? employeeDetails.first_name : "",
    //   last_name: employeeDetails.last_name ? employeeDetails.last_name : "",
    //   email: employeeDetails.email ? employeeDetails.email : "",
    //   phone_no: employeeDetails.phone_no ? employeeDetails.phone_no : ""
    // });

    // const validateProfile = Yup.object().shape({
    //   first_name: Yup.string()
    //     .required("Please enter first name")
    //     .min(2, "First name must be at least 2 characters long"),
    //   last_name: Yup.string()
    //     .required("Please enter last name")
    //     .min(2, "Last name must be at least 2 characters long"),
    //   email: Yup.string()
    //     .required("Please enter email")
    //     .email("Invalid email"),
    //   phone_no: Yup.string()
    //     .required("Please enter phone number")
    //     .min(10, "Phone number must be at least 10 characters long"),
    //   password: Yup.string()
    //     .required("Please enter your new password")
    //     .min(8, "Password must be at least 8 characters long"),
    //   confirm_password: Yup.string()
    //     .required("Please enter your confirm password")
    //     .test("match", "Confirm password do not match", function(
    //       confirm_password
    //     ) {
    //       return confirm_password === this.parent.password;
    //     })
    // });
    // console.log(this.state.employeeDetails);
    return (
      <>
        <div className="content-wrapper">
          {/*<!-- Content Header (Page header) -->*/}
          <section className="content-header">
            <div className="row">
              <div className="col-lg-9 col-sm-6 col-xs-12">
                <h1>
                  Profile
                  <small />
                </h1>
              </div>
              <div className="col-lg-3 col-sm-6 col-xs-12" />
            </div>
          </section>

          {/*<!-- Main content -->*/}
          <section className="content">
            <div className="boxPapanel content-padding">
              <Row>
                <Col xs={2} sm={2} md={2}>
                  <div className="profile">
                    <div className="imageArea">
                      <img alt="noimage" src={getMyPic()} />
                    </div>
                    <Link
                      to={{
                        pathname: "/"
                      }}
                      className="edit-image"
                    >
                      <i className="far fa-edit" aria-hidden="true" />
                      Edit Image
                    </Link>
                  </div>
                </Col>
                {/* <Formik
                  initialValues={newInitialValues}
                  validationSchema={validateProfile}
                  onSubmit={this.handleSubmitEvent}
                >
                  {({ values, errors, touched, isValid, isSubmitting }) => {
                    return (
                      <Form>
                        <Col xs={10} sm={10} md={10}>
                          <Row>
                            <Col xs={12} sm={6} md={6}>
                              <div className="form-group">
                                <label>First Name</label>
                                <Field
                                  name="first_name"
                                  type="text"
                                  className={`form-control`}
                                  placeholder="Enter first name"
                                  autoComplete="off"
                                  value={values.designation}
                                />
                                {errors.first_name && touched.first_name ? (
                                  <span className="errorMsg">
                                    {errors.first_name}
                                  </span>
                                ) : null}
                              </div>
                            </Col>
                            <Col xs={12} sm={6} md={6}>
                              <div className="form-group">
                                <label>Last Name</label>
                                <Field
                                  name="last_name"
                                  type="text"
                                  className={`form-control`}
                                  placeholder="Enter last name"
                                  autoComplete="off"
                                  value={values.designation}
                                />
                                {errors.last_name && touched.last_name ? (
                                  <span className="errorMsg">
                                    {errors.last_name}
                                  </span>
                                ) : null}
                              </div>
                            </Col>
                            <Col xs={12} sm={6} md={6}>
                              <div className="form-group">
                                <label>E-mail</label>
                                <Field
                                  name="email"
                                  type="text"
                                  className={`form-control`}
                                  placeholder="Enter email"
                                  autoComplete="off"
                                  value={values.email}
                                />
                                {errors.email && touched.email ? (
                                  <span className="errorMsg">
                                    {errors.email}
                                  </span>
                                ) : null}
                              </div>
                            </Col>
                            <Col xs={12} sm={6} md={6}>
                              <div className="form-group">
                                <label>Phone</label>
                                <Field
                                  name="phone_no"
                                  type="text"
                                  className={`form-control`}
                                  placeholder="Enter phone number"
                                  autoComplete="off"
                                  value={values.phone_no}
                                />
                                {errors.phone_no && touched.phone_no ? (
                                  <span className="errorMsg">
                                    {errors.phone_no}
                                  </span>
                                ) : null}
                              </div>
                            </Col>
                            <Col xs={12} sm={6} md={6}>
                              <div className="form-group">
                                <label>Password</label>
                                <Field
                                  name="password"
                                  type="password"
                                  className={`form-control`}
                                  placeholder="Enter password"
                                  autoComplete="off"
                                />
                                {errors.password && touched.password ? (
                                  <span className="errorMsg">
                                    {errors.password}
                                  </span>
                                ) : null}
                              </div>
                            </Col>
                            <Col xs={12} sm={6} md={6}>
                              <div className="form-group">
                                <label>Confirm Password</label>
                                <Field
                                  name="confirm_password"
                                  type="password"
                                  className={`form-control`}
                                  placeholder="Enter confirm password"
                                  autoComplete="off"
                                />
                                {errors.confirm_password &&
                                touched.confirm_password ? (
                                  <span className="errorMsg">
                                    {errors.confirm_password}
                                  </span>
                                ) : null}
                              </div>
                            </Col>
                          </Row>
                          <div className="button-footer btn-toolbar">
                            <button
                              type="button"
                              className="btn btn-danger btn-sm"
                            >
                              Cancel
                            </button>
                            <button
                              className={`btn btn-success btn-sm ${
                                isValid ? "btn-custom-green" : "btn-disable"
                              } m-r-10`}
                              type="submit"
                              disabled={isValid ? false : true}
                            >
                              {isSubmitting ? "Updating..." : "Save"}
                            </button>
                          </div>
                        </Col>
                      </Form>
                    );
                  }}
                </Formik> */}
              </Row>
            </div>
          </section>
        </div>
      </>
    );
  }
}

export default Profile;
