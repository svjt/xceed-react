import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import whitelogo from '../../../assets/images/drreddylogo_white.png';

import Header from '../header/Header';

class LogisticsOrders extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            showModal: false,
        };
    }
        
    modalShowHandler = () => {
        this.setState({ showModal: true });
    };

    modalCloseHandler = () => {
        this.setState({ showModal: false });
    };

    render() {
        if (this.state.isLoading) {
            return (
                <div className='loderOuter'>
                    <div className='loading_reddy_outer'>
                        <div className='loading_reddy'>
                            <img src={whitelogo} alt='logo' />
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <>
                    <div className='content-wrapper qaWrapper'>
                        <Header title="Logistics Orders"/>
                    </div>
                </>
            );
        }
    }
}

export default withRouter(LogisticsOrders);
