import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';

import './Sidebar.css';

import DashboradImage from '../../../assets/images/dashboard-icon.svg';
import DashboradImageActive from '../../../assets/images/dashboard-icon-active.svg';

class Sidebar extends Component {

	constructor() {
		super();
	}

	render() {
		if (this.props.isLoggedIn === false) return null;

		return (	
			<>
				<aside className="main-sidebar">
					<section className="sidebar">
						<ul className="sidebar-menu">
							<li className="header">MAIN NAVIGATION</li>
							{
								(window.location.pathname === "/logistics_orders" || window.location.pathname === "/logistics_order_details/") ? (
									<li className="treeview active">
										<Link to={'/logistics_orders'}>
											<img src={DashboradImageActive} alt="Dashboard" className="main-icon" />
											<span className="static-text">Orders</span>
										</Link>
									</li>
								) : (
									<li className="treeview">
										<Link to={'/logistics_orders'}>
											<img src={DashboradImage} alt="Dashboard" className="main-icon" />
											<span className="static-text">Orders</span>
										</Link>
									</li>
								)
							}
							{/* {
								window.location.pathname === "/logistics_awb_documents" ? (
									<li className="treeview active">
										<Link to={'/logistics_awb_documents'}>
											<img src={DashboradImageActive} alt="Orders" className="main-icon" />
											<span className="static-text">Orders</span>
										</Link>
									</li>
								) : (
									<li className="treeview">
										<Link to={'/logistics_awb_documents'}>
											<img src={DashboradImage} alt="Orders" className="main-icon" />
											<span className="static-text">Orders</span>
										</Link>
									</li>
								)
							} */}
						</ul>
					</section>
				</aside>
			</>
		);
	}
}

export default withRouter(Sidebar);