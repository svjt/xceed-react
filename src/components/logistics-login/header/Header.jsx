import React, { Component } from 'react'
import { withRouter } from 'react-router-dom';

import ChangePasswordModal from '../change-password/ChangePasswordModal';

class Header extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            showModal: false,
        };
    }
    
    modalShowHandler = () => {
        this.setState({ showModal: true });
    };

    modalCloseHandler = () => {
        this.setState({ showModal: false });
    };
    
    render() {
        return (
            <>
            <section className='content-header'>
                <div className='row'>
                    <div className='col-lg-12 col-sm-6 col-xs-12'>
                        <h1>{this.props.title}</h1>
                        <div
                            className='customer-password-button btn-fill bdrRadius'
                            onClick={this.modalShowHandler}
                        >
                            Change Password
                        </div>
                        <div
                            className='customer-edit-button btn-fill bdrRadius'
                            onClick={() => {
                                localStorage.clear();
                                this.props.history.push('/logistics_login');
                            }}
                        >
                            Log Out
                        </div>
                    </div>
                </div>
            </section>
            <ChangePasswordModal
                showModal={this.state.showModal}
                modalCloseHandler={this.modalCloseHandler}
                modalCloseHandler={this.modalCloseHandler}
            />
            </>
        )
    }
}

export default withRouter(Header);