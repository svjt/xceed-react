import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';

import axios from '../../../shared/axios';

import LoadingSpinner from '../../loader/loadingSpinner';
import drreddylogo from '../../../assets/images/drreddylogo_white.png';
import exceedlogo from '../../../assets/images/Xceed_Logo_Purple_RGB.png';

const initialValues = {
    password: '',
    confirm_password: '',
};

let message = '';
let err_message = '';

const resetPasswordvalidation = Yup.object().shape({
    password: Yup.string()
        .trim()
        .required('Please enter password')
        .min(8, 'Password must be at least 8 characters long'),
    confirm_password: Yup.string()
        .trim()
        .required('Please enter confirm password')
        .test('passwords-match', 'Password and Confirm password must match', function (value) {
            return this.parent.password === value;
        }),
});

class resetPassword extends Component {
    constructor(props) {
        super(props);

        message = this.props.location.state ? this.props.location.state.message : '';
        err_message = this.props.location.state ? this.props.location.state.err_message : '';
    }

    state = {
        message: '',
        err_message: '',
        token: '',
    };

    componentDidMount() {
        if (localStorage.logistics_token !== null && typeof localStorage.logistics_token != 'undefined') {
            this.props.history.push('/logistics_form');
        }
        this.setState({
            token: this.props.match.params.token,
        });
        if (message !== null) {
            this.props.history.push({
                pathname: '',
                state: '',
            });
            this.setState({ message: message });
        }
        if (err_message !== null) {
            this.props.history.push({
                pathname: '',
                state: '',
            });
            this.setState({ err_message: err_message });
        }

        document.title = `Logistics Reset Password | Dr.Reddys API`;

        axios
            .post('/api/employees/logistics/check_token', {
                token: this.props.match.params.token,
            })
            .then((res) => {})
            .catch((err) => {
                if (err.data && err.data.status === 3) {
                    this.props.history.push({
                        pathname: '/logistics_forgot_password',
                        state: {
                            err_message:
                                'The link has expired or it has been used earlier or is no longer valid. Please raise a request again.',
                        },
                    });
                }
            });
    }

    handleSubmit = (values, actions) => {
        axios
            .post('/api/employees/logistics/resetpassword', { token: this.state.token, password: values.password.trim() })
            .then((res) => {
                if (res.status === 200) {
                    this.props.history.push({
                        pathname: '/logistics_login',
                        state: {
                            message: 'Password reset successfully.',
                        },
                    });
                    actions.resetForm();
                } else {
                    this.setState({
                        err_message: 'There is some problem please try again later',
                    });
                }
            })
            .catch((err) => {
                actions.setSubmitting(false);
                if (err.data && err.data.errors) {
                    this.setState({
                        err_message: err.data.errors.password,
                    });
                }
            });
    };

    removeError = () => {
        this.setState({ err_msg: '' });
    };

    render() {
        const { loading } = this.state;

        return (
            <>
                <div className='login-box'>
                    {loading ? (
                        <LoadingSpinner />
                    ) : (
                        <Formik
                            initialValues={initialValues}
                            validationSchema={resetPasswordvalidation}
                            onSubmit={this.handleSubmit}
                        >
                            {({ values, errors, touched, handleChange, isValid, setFieldValue }) => {
                                return (
                                    <Form>
                                        <div className='login-logo'>
                                            <Link to='/' className='logo'>
                                                <span className='logo-mini'>
                                                    <img src={drreddylogo} alt="Dr.Reddy's" />
                                                </span>
                                            </Link>
                                            <br />
                                            <span className='logo-mini'>
                                                <img src={exceedlogo} alt="Dr.Reddy's" height='36px' />
                                            </span>
                                        </div>
                                        <div className='login-box-body'>
                                            {this.state.message && (
                                                <h5
                                                    style={{
                                                        color: '#1bb200',
                                                        padding: '5px 10px',
                                                        textAlign: 'center',
                                                    }}
                                                >
                                                    {this.state.message}
                                                </h5>
                                            )}
                                            {this.state.err_message && (
                                                <h5
                                                    style={{
                                                        color: '#c83a40',
                                                        padding: '5px 10px',
                                                        textAlign: 'center',
                                                    }}
                                                >
                                                    {this.state.err_message}
                                                </h5>
                                            )}
                                            <p className='login-box-msg'>Logistics Reset Password</p>

                                            <label>Password:</label>
                                            <div className='form-group has-feedback m-b-25'>
                                                <Field
                                                    name='password'
                                                    type='password'
                                                    className='form-control'
                                                    autoComplete='off'
                                                    placeholder='Please enter password'
                                                />

                                                {errors.password && touched.password && (
                                                    <label>{errors.password}</label>
                                                )}
                                            </div>

                                            <label>Confirm Password:</label>
                                            <div className='form-group has-feedback m-b-25'>
                                                <Field
                                                    name='confirm_password'
                                                    type='password'
                                                    className='form-control'
                                                    autoComplete='off'
                                                    placeholder='Please enter confirm password'
                                                />

                                                {errors.confirm_password && touched.confirm_password && (
                                                    <label>{errors.confirm_password}</label>
                                                )}
                                            </div>

                                            <div className='row'>
                                                <div className='col-xs-8'>
                                                    <Link to={'/logistics_login'}>I already have an account</Link>
                                                </div>
                                                <div className='col-xs-4'>
                                                    <button className='btn-fill' type='submit'>
                                                        Submit
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </Form>
                                );
                            }}
                        </Formik>
                    )}
                </div>
            </>
        );
    }
}

export default resetPassword;
