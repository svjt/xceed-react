import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import * as Yup from 'yup';
import { Formik, Field, Form } from 'formik';
import { Row, Col, Modal } from 'react-bootstrap';
import swal from "sweetalert";

import axios from "../../../shared/axios_logistics";

const validatePassword = Yup.object().shape({
    password: Yup.string()
        .trim()
        .required('Please enter your new password')
        .min(8, 'Password must be at least 8 characters long'),
    confirm_password: Yup.string()
        .trim()
        .required('Please enter your confirm password')
        .test('match', 'Confirm password do not match', function (confirm_password) {
            return confirm_password === this.parent.password;
        }),
});

class ChangePasswordModal extends Component {

    handleSubmitEvent = (values, actions) => {
        const post_data = {
            password: values.password,
        };

        axios
            .put(`/api/employees/logistics/changepassword`, post_data)
            .then((res) => {
                this.props.modalCloseHandler();
                swal({
                    closeOnClickOutside: false,
                    title: 'Success',
                    text: 'Password updated successfully.',
                    icon: 'success',
                }).then(() => {
                    window.location.reload();
                });
            })
            .catch((err) => {
                actions.setSubmitting(false);
                if(err.data && err.data.errors){
                    actions.setErrors(err.data.errors);
                }
            });
    };

    render() {
        return (
            <>
                <Modal
                    show={this.props.showModal}
                    onHide={this.props.modalCloseHandler}
                    backdrop='static'
                    dialogClassName='profile-popup'
                >
                    <Modal.Header closeButton>
                        <Modal.Title>Change Password</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className='contBox'>
                            <Row>
                                <Col xs={12} sm={12} md={12}>
                                    <div className='profile'>
                                        <Formik validationSchema={validatePassword} onSubmit={this.handleSubmitEvent}>
                                            {({ values, errors, touched, isValid, isSubmitting }) => {
                                                return (
                                                    <Form>
                                                        <Row>
                                                            <Col xs={12} sm={12} md={12}>
                                                                <div className='form-group'>
                                                                    <label
                                                                        className='pull-left'
                                                                        style={{ float: 'left' }}
                                                                    >
                                                                        Password
                                                                    </label>
                                                                    <Field
                                                                        name='password'
                                                                        type='password'
                                                                        className={`form-control`}
                                                                        placeholder='Enter password'
                                                                        autoComplete='off'
                                                                    />
                                                                    {errors.password && touched.password ? (
                                                                        <span className='errorMsg'>
                                                                            {errors.password}
                                                                        </span>
                                                                    ) : null}
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col xs={12} sm={12} md={12}>
                                                                <div className='form-group'>
                                                                    <label className='pull-left'>
                                                                        Confirm Password
                                                                    </label>
                                                                    <Field
                                                                        name='confirm_password'
                                                                        type='password'
                                                                        className={`form-control`}
                                                                        placeholder='Enter confirm password'
                                                                        autoComplete='off'
                                                                    />
                                                                    {errors.confirm_password &&
                                                                    touched.confirm_password ? (
                                                                        <span className='errorMsg'>
                                                                            {errors.confirm_password}
                                                                        </span>
                                                                    ) : null}
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                        <div className='button-footer btn-toolbar'>
                                                            <button
                                                                type='button'
                                                                className='btn btn-success btn-sm'
                                                                onClick={this.props.modalCloseHandler}
                                                            >
                                                                Cancel
                                                            </button>
                                                            <button
                                                                className={`btn btn-danger btn-sm ${
                                                                    isValid ? 'btn-custom-green' : 'btn-disable'
                                                                } m-r-10`}
                                                                type='submit'
                                                                disabled={isValid ? false : true}
                                                            >
                                                                {isSubmitting ? 'Updating...' : 'Submit'}
                                                            </button>
                                                        </div>
                                                    </Form>
                                                );
                                            }}
                                        </Formik>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </Modal.Body>
                </Modal>
            </>
        );
    }
}

export default withRouter(ChangePasswordModal);
