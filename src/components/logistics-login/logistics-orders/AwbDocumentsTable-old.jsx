import React, { Component } from 'react';
import { Row, Col, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Link } from 'react-router-dom';
import Pagination from 'react-js-pagination';
import dateFormat from 'dateformat';

import axios from '../../../shared/axios_logistics';
import { htmlDecode } from '../../../shared/helper';
import AwbDocumentModal from './AwbDocumentModal';
import { isMobile } from "react-device-detect";

const s3bucket_ship_download_path = `${process.env.REACT_APP_API_URL}/api/tasks/download_ship_doc/`;
const s3bucket_task_diss_path_coa = `${process.env.REACT_APP_API_URL}/api/tasks/download_task_bucket_coa/`;

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
    return (
        <OverlayTrigger
            overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
            placement='left'
            delayShow={300}
            delayHide={150}
            trigger={['hover']}
        >
            <Link to={href || ''} onClick={clicked}>
                {children}
            </Link>
        </OverlayTrigger>
    );
}

const actionFormatter = (refObj) => (cell) => {
    const file = refObj.state.awbDocuments.filter((data) => cell == data.invoice_number);

    return (
        <>
            <span></span>
            <div className='actionStyle'>
                {file[0].awb != '' && file[0].awb != null ? file[0].awb : null}
                {file[0].awb != '' && file[0].awb != null ? (
                    <>
                        {file[0].awb_file_hash != '' ? (
                            <LinkWithTooltip
                                tooltip='Click to Download'
                                href='#'
                                clicked={(e) => window.open(`${s3bucket_ship_download_path}${file[0].awb_file_hash}/flight`, "_blank")}
                                id='tooltip-1'
                            >
                                <i className='fas fa-download' />
                            </LinkWithTooltip>
                        ) : null}
                        <LinkWithTooltip
                            tooltip='Click to Edit'
                            href='#'
                            clicked={(e) => refObj.modalShowHandler(e, cell)}
                            id='tooltip-1'
                        >
                            <i className='far fa-edit' />
                        </LinkWithTooltip>
                    </>
                ) : (
                    <LinkWithTooltip
                        tooltip='Click to Add'
                        href='#'
                        clicked={(e) => refObj.modalShowHandler(e, cell)}
                        id='tooltip-1'
                    >
                        <i className='fas fa-plus' />
                    </LinkWithTooltip>
                )}
            </div>
        </>
    );
};

class AwbDocumentTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            isLoading: false,
            awbDocuments: [],
            awbDocumentsCount: 0,
            activePage: 1,
            itemPerPage: 10,
            awbDocumentDetails: {},
        };
    }

    componentDidMount() {
        // Do the API Calls
        this.getLogisticTaskList();
    }

    getLogisticTaskList(page = 1) {
        this.setState({ isLoading: true });
        axios.get(`/api/employees/logistics/invoice_task_list?page=${page}`)
            .then(async (res) => {
                this.setState({
                    awbDocuments: res.data.data,
                    awbDocumentsCount: Number(res.data.count),
                    isLoading: false,
                });
            })
            .catch((err) => {
                this.setState({
                    isLoading: false,
                });
            });
    };

    handlePageChange = (pageNumber) => {
        this.setState({ activePage: pageNumber });
        this.getLogisticTaskList(pageNumber > 0 ? pageNumber : 1);
    };

    getSubmittedBy = (request) => {
        if (request.submitted_by > 0) {
            return `${request.agent_fname} ${request.agent_lname}`;
        } else if (request.ccp_posted_by > 0) {
            let posted_from = "";
            return `${request.emp_posted_by_fname} ${request.emp_posted_by_lname} ${posted_from}`;
        } else {
            return `${request.first_name} ${request.last_name}`;
        }
    };

    openMobileView = (onBehalf) => {
        this.setState({
            showMobileAlert: true,
            displayMobileText: `This request is raised on behalf of ${onBehalf} by Dr.Reddy’s`,
        });
    };

    isListingHover = (targetName) => {
        return this.state[targetName] ? this.state[targetName].hoverTip : false;
    };

    modalShowHandlerAdd = (event, id) => {
        event.preventDefault();
        if (id) {
            let invoiceValues = {
                invoice_number: id,
            };
            this.setState({ awbDocumentDetails: invoiceValues, showModal: true });
        } else {
            this.setState({ awbDocumentDetails: {}, showModal: true });
        }
    }

    modalShowHandler = (event, id) => {
        event.preventDefault();
        if (id) {
            this.setState({ isLoading: true });
            axios
                .get(`/api/employees/logistics_awb_form/${id}`)
                .then((res) => {
                    this.setState({ isLoading: false });
                    if (res.data.data && res.data.data.length > 0) {
                        this.setState({ awbDocumentDetails: res.data.data[0], showModal: true })
                    }
                })
                .catch((err) => {
                    this.setState({ isLoading: false, awbDocumentDetails: {} });
                    console.log({ err });
                });
        } else {
            this.setState({ awbDocumentDetails: {} });
        }
    };

    modalCloseHandler = () => {
        this.setState({ showModal: false });
    };

    _htmlDecode = (refObj) => (cell) => {
        return htmlDecode(cell);
    };

    setDate = (refObj) => (cell) => {
        if (cell) {
            return dateFormat(cell, 'dd-mm-yyyy HH:MM:ss');
        }
        return '-';
    };

    render() {
        const { awbDocuments, showModal, awbDocumentDetails } = this.state;

        return (
            <>
                <section className='content awbDocumentTable'>
                    <div className='serchapanel'>
                        <div className='box-body'>
                            {/* <BootstrapTable data={awbDocuments}>
                                <TableHeaderColumn
                                    isKey
                                    dataFormat={this._htmlDecode(this)}
                                    dataField='task_ref'
                                >
                                    Task Ref No
                                </TableHeaderColumn>
                                <TableHeaderColumn
                                    tdStyle={{ whiteSpace: 'normal' }}
                                    dataField='invoice_number'
                                    dataFormat={this._htmlDecode(this)}
                                >
                                    Invoice #
                                </TableHeaderColumn>
                                <TableHeaderColumn
                                    tdStyle={{ whiteSpace: 'normal' }}
                                    dataField='invoice_file_name'
                                    dataFormat={this._htmlDecode(this)}
                                >
                                    Invoice PDF
                                </TableHeaderColumn>
                                <TableHeaderColumn dataField='payment_status'>
                                    Logistic Status
                                </TableHeaderColumn>
                                <TableHeaderColumn
                                    dataField='invoice_number'
                                    tdStyle={{ whiteSpace: 'normal' }}
                                    dataFormat={actionFormatter(this)}
                                >
                                    AWB#
                                </TableHeaderColumn>
                                
                                <TableHeaderColumn dataField='invoice_number' dataFormat={actionFormatter(this)}>
                                    Action
                                </TableHeaderColumn>
                            </BootstrapTable> */}
                            <div className="listing-table table-responsive">
                                <table className="table table-hover table-bordered" style={{ width: '1700px', maxWidth: 'inherit' }}>
                                    <thead>
                                        <tr>
                                            <th width="120">Task Ref.</th>
                                            <th width="150">Purchase Order (Customer PO number)</th>
                                            <th width="150">Sales Order #</th>
                                            <th width="150">Material Name</th>
                                            <th width="150">Invoice #</th>
                                            {/* <th width="150">Invoice PDF</th> */}
                                            <th width="150">Packing List PDF</th>
                                            <th width="150">AWB #</th>
                                            <th width="150">COA</th>
                                            <th width="150">Payment Status</th>
                                            <th width="150">Submitted By</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.awbDocuments.map((order, key) => (
                                            <tr key={key}>
                                                <td>
                                                    <Link
                                                        to={{
                                                            pathname: `/logistics_order_details/${order.task_id}`,
                                                        }}
                                                        //to="#"
                                                        style={{ cursor: "pointer" }}
                                                    >
                                                        {order.task_ref}
                                                    </Link>
                                                </td>
                                                <td>
                                                    <p>{order.po_number != null && order.po_number != '' ? order.po_number : '-'}</p>
                                                </td>
                                                <td>
                                                    <p>{order.sales_order_no != null && order.sales_order_no != '' ? order.sales_order_no : '-'}</p>
                                                    {order.sales_order_no != null && order.sales_order_no != '' ? `(${order.sales_order_date})` : ""}
                                                </td>
                                                <td>{order.product_name}</td>
                                                <td>
                                                    {order.invoice_number}{"  "}
                                                    {order.invoice_number != null && order.invoice_number != '' && order.invoice_file_name != null && order.invoice_file_name != '' &&
                                                        (

                                                            <LinkWithTooltip
                                                                tooltip='Click to Download Invoice File'
                                                                href='#'
                                                                className="download_link"
                                                                clicked={(e) => window.open(`${s3bucket_ship_download_path}${order.all_file_hash}/invoice`, '_blank')}
                                                                id='tooltip-1'
                                                            >
                                                                <i className='fas fa-download' />
                                                            </LinkWithTooltip>
                                                        )}
                                                </td>
                                                {/* <td>{order.invoice_number != null && order.invoice_number != '' ? order.invoice_file_name : ''}</td> */}
                                                <td>{order.invoice_number != null && order.invoice_number != '' && order.packing_file_name != null && order.packing_file_name != '' ?
                                                    (
                                                        <>
                                                            {order.packing_file_name} {"  "}
                                                            <LinkWithTooltip
                                                                tooltip='Click to Download Packing File'
                                                                href='#'
                                                                className="download_link"
                                                                clicked={(e) => window.open(`${s3bucket_ship_download_path}${order.all_file_hash}/package`, '_blank')}
                                                                id='tooltip-4'
                                                            >
                                                                <i className='fas fa-download' />
                                                            </LinkWithTooltip>
                                                        </>
                                                    ) : null
                                                }

                                                </td>
                                                <td>{order.awb != null && order.awb != '' ?
                                                    (
                                                        <>
                                                            {order.awb}{`\n`}( {dateFormat(order.awb_date, "d mmm yyyy")} ){"  "}
                                                            {order.awb_file_hash != '' ? (
                                                                <>
                                                                    <LinkWithTooltip
                                                                        tooltip='Click to Download'
                                                                        href='#'
                                                                        clicked={(e) => window.open(`${s3bucket_ship_download_path}${order.awb_file_hash}/flight`, "_blank")}
                                                                        id='tooltip-1'
                                                                    >
                                                                        <i className='fas fa-download' />
                                                                    </LinkWithTooltip>{"  "}
                                                                </>
                                                            ) : null}
                                                            <LinkWithTooltip
                                                                tooltip='Click to Edit'
                                                                href='#'
                                                                clicked={(e) => this.modalShowHandler(e, order.invoice_number)}
                                                                id='tooltip-1'
                                                            >
                                                                <i className='far fa-edit' />
                                                            </LinkWithTooltip>
                                                        </>
                                                    ) : (
                                                        <LinkWithTooltip
                                                            tooltip='Click to Add'
                                                            href='#'
                                                            clicked={(e) => this.modalShowHandlerAdd(e, order.invoice_number)}
                                                            id='tooltip-1'
                                                        >
                                                            <i className='fas fa-plus' />
                                                        </LinkWithTooltip>
                                                    )}
                                                </td>
                                                <td>
                                                    {order.coa_file.length > 0 && (
                                                        order.coa_file
                                                            .map((file, p) => {
                                                                return (
                                                                    <p key={p}>
                                                                        {file.actual_file_name}{"  "}
                                                                        <LinkWithTooltip
                                                                            tooltip='Click to Download'
                                                                            href='#'
                                                                            clicked={(e) => window.open(`${s3bucket_task_diss_path_coa}${file.coa_id}`, "_blank")}
                                                                            id='tooltip-1'
                                                                        >
                                                                            <i className='fas fa-download' />
                                                                        </LinkWithTooltip>
                                                                    </p>
                                                                );
                                                            })
                                                    )}
                                                </td>
                                                <td>{order.payment_status}</td>

                                                <td>
                                                    {isMobile ? (
                                                        <span
                                                            id={`tip-${order.task_id}`}
                                                            onClick={() =>
                                                                (order.submitted_by > 0 ||
                                                                    order.ccp_posted_by > 0) &&
                                                                this.openMobileView(
                                                                    `${order.first_name} ${order.last_name}`
                                                                )
                                                            }
                                                        >
                                                            {this.getSubmittedBy(order)}
                                                        </span>
                                                    ) : (
                                                        <>
                                                            <span id={`tip-${order.task_id}`}>
                                                                {this.getSubmittedBy(order)}
                                                            </span>

                                                            {(order.submitted_by > 0 ||
                                                                order.ccp_posted_by > 0) && (
                                                                    <Tooltip
                                                                        placement="right"
                                                                        isOpen={this.isListingHover(
                                                                            `tip-${order.task_id}`
                                                                        )}
                                                                        autohide={false}
                                                                        target={`tip-${order.task_id}`}
                                                                        toggle={() =>
                                                                            this.toggleListingTip(
                                                                                `tip-${order.task_id}`
                                                                            )
                                                                        }
                                                                        className="listingTip"
                                                                    >
                                                                        This request is raised on behalf of {`${order.first_name} ${order.last_name}`} by {this.getSubmittedBy(order)}
                                                                    </Tooltip>
                                                                )}
                                                        </>
                                                    )}
                                                </td>

                                            </tr>
                                        ))}
                                        {this.state.awbDocuments.length === 0 && (
                                            <tr>
                                                <td colSpan="11" align="center">
                                                    No Data to display
                                                </td>
                                            </tr>
                                        )}
                                    </tbody>
                                </table>
                            </div>


                            {this.state.awbDocumentsCount > this.state.itemPerPage ? (
                                <Row>
                                    <Col md={12}>
                                        <div className='paginationOuter text-right'>
                                            <Pagination
                                                activePage={this.state.activePage}
                                                itemsCountPerPage={this.state.itemPerPage}
                                                totalItemsCount={this.state.awbDocumentsCount}
                                                itemClass='nav-item'
                                                linkClass='nav-link'
                                                activeClass='active'
                                                onChange={this.handlePageChange}
                                            />
                                        </div>
                                    </Col>
                                </Row>
                            ) : null}
                        </div>
                    </div>
                </section>
                {showModal && (
                    <AwbDocumentModal
                        showModal={showModal}
                        modalShowHandler={this.modalShowHandler}
                        modalCloseHandler={this.modalCloseHandler}
                        awbDocumentDetails={awbDocumentDetails}
                    />
                )}
            </>
        );
    }
}

export default AwbDocumentTable;
