import React, { Component } from "react";

import { Tooltip, OverlayTrigger } from "react-bootstrap";
import { Link } from "react-router-dom";

import CustomerApproval from "../dashboard/CustomerApproval";
import MyTaskBox from "../dashboard/MyTaskBox";
import MyCustomerBox from "../dashboard/MyCustomerBox";
import SlaBox from "../dashboard/SlaBox";
import SlaBoxTask from "../dashboard/SlaBoxTask";
import CreateCustomerPopup from "../dashboard/CreateCustomerPopup";
import MyTasksTable from "../dashboard/MyTasksTable";
import CreateMyTasksPopup from "../dashboard/CreateMyTasksPopup";
import ParamsCreateMyTasksPopup from "../dashboard/ParamsCreateMyTasksPopup";
import ReviewRequestPopup from "../dashboard/ReviewRequestPopup";
import CSCDashboardSearch from "./CSCDashboardSearch";
import AllocatedTasksTable from "../dashboard/AllocatedTasksTable";
import ApprovalTasksTable from "../dashboard/ApprovalTasksTable";

import ClosedTasksTable from "../dashboard/ClosedTasksTable";

import searchImage from "../../assets/images/search-icon.svg";
import calenderTaskImage from "../../assets/images/calender-task-icon.svg";
import { getMyId,isSPOC,fullLangName } from "../../shared/helper";

import AnalyticsModule from "../dashboard/AnalyticsModule";
import swal from "sweetalert";
import "./Dashboard.css";

import API from "../../shared/axios";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
import { getUserDisplayName, inArray } from "../../shared/helper";

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

class CSCOverview extends Component {

  constructor(props){
    super(props);
    this.state = {
      myTasks: [],
      allocatedTasks: [],
      closedTasks: [],
      isLoading: true,
      showCreateTasks: false,
      showCreateCustomer: false,
      outlookShowCreateTasks: false,
      outlookShowCustomerAdd: false,
      showCustomerApproval:false,
      countApprovalTasks: 0,
      emp_id: 0,
      analytics_text:`Show Analytics`,
      show_analytics_module:false,
      deselectTeam:false
    };
  }

  checkHandler = (event) => {
    event.preventDefault();
  };

  componentDidMount() {
    this.allTaskDetails();
    if (this.props.match.params.id && this.props.match.params.id !== "") {
      API.get(`/api/outlook/details/${this.props.match.params.id}`)
        .then((res) => {
          if (res.data.status === 1) {

            let outlookPopup  = false;
            let customerPopup = false;

            if(res.data.open_customer_popup == true){
              customerPopup = true;
            }else{
              outlookPopup  = true;
            }

            
            this.setState({
              outlookShowCreateTasks: outlookPopup,
              outlookShowCustomerAdd: customerPopup,
              outlook_content: res.data.content,
              cust_company_details: res.data.cust_company_details,
              outlook_attachments: res.data.attachments,
              ot_id: res.data.ot_id,
            });
          } else {
            this.setState({ outlookShowCreateTasks: false,outlookShowCustomerAdd: false });
          }
        })
        .catch((err) => {});
    } else {
      var ot = localStorage.getItem("ot");
      var pattern = /^[a-zA-Z0-9]*$/;

      if (ot !== null && ot.match(pattern)) {
        API.get(`/api/outlook/details/${ot}`)
          .then((res) => {
            localStorage.removeItem("ot");
            if (res.data.status === 1) {
              let outlookPopup  = false;
              let customerPopup = false;

              if(res.data.open_customer_popup == true){
                customerPopup = true;
              }else{
                outlookPopup  = true;
              }

              this.setState({
                outlookShowCreateTasks: outlookPopup,
                outlookShowCustomerAdd: customerPopup,
                outlook_content: res.data.content,
                cust_company_details: res.data.cust_company_details,
                outlook_attachments: res.data.attachments,
                ot_id: res.data.ot_id,
              });
            } else {
              this.setState({ outlookShowCreateTasks: false,outlookShowCustomerAdd: false });
            }
          })
          .catch((err) => {});
      } else {
        localStorage.removeItem("ot");
      }
    }

    if (this.props.match.params.review_team_id && this.props.match.params.review_team_id !== "") {
      API.get(`/api/employees/check_customer_team/${this.props.match.params.review_team_id}`)
        .then((res) => {
          if(res.data.status === 1){
            this.setState({customer_approval_id:res.data.customer_id,team_approval_id:res.data.team_id,showCustomerApproval:true});
          }else{
            swal({
              closeOnClickOutside: false,
              title: (res.data.message) ? res.data.message : "Invalid Access",
              text: ``,
              icon: "success",
            }).then(() => {
              this.props.history.push('/user/dashboard')
            });
          }
        })
        .catch((err) => { 

        });
    } else {
      var customer_approval = localStorage.getItem("customer_approval");
      var pattern = /^[a-zA-Z0-9]*$/;

      if (customer_approval !== null && customer_approval.match(pattern)) {
        API.get(`/api/employees/check_customer_team/${customer_approval}`)
          .then((res) => {
            localStorage.removeItem("customer_approval");
            if(res.data.status === 1){
              this.setState({customer_approval_id:res.data.customer_id,team_approval_id:res.data.team_id,showCustomerApproval:true});
            }else{
              swal({
                closeOnClickOutside: false,
                title: (res.data.message) ? res.data.message : "Invalid Access",
                text: ``,
                icon: "success",
              }).then(() => {
                this.props.history.push('/user/dashboard')
              });
            }
          })
          .catch((err) => { 

          });
      } else {
        localStorage.removeItem("customer_approval");
      }
    }

  }

  allTaskDetails = (page = 1) => {
    this.setState({
      myTasks: [],
      allocatedTasks: [],
      closedTasks: [],
      queryString: "",
      countMyTasks: 0,
      explicit_my_tasks:0,
      explicit_allocated:0,
      explicit_closed:0,
      countAllocatedTasks: 0,
      countClosedTasks: 0,
      countPink:0,
      countApprovalTasks: 0,
      countApprovalTasksClosed:0,
      activeApprovalTasks:0,
      approvalCustomer: [],
      countApprovalCustomer: 0,
      isLoading: true,
      tabsClicked: "tab_1",
      update_state: true,
    });

    API.get(`/api/tasks?page=${page}`)
      .then((res) => {
        this.setState({
          myTasks: res.data.data,
          countMyTasks: res.data.count_my_tasks,
          explicit_my_tasks:res.data.explicit_task_id
        });
        API.get(`/api/tasks/allocated?page=${page}`)
          .then((res) => {
            this.setState({
              allocatedTasks: res.data.data,
              countAllocatedTasks: res.data.count_allocated_tasks,
              explicit_allocated:res.data.explicit_task_id
            });
            API.get(`/api/tasks/closed`)
              .then((res) => {
                this.setState({
                  closedTasks: res.data.data,
                  countClosedTasks: res.data.count_closed_tasks,
                  countPink: res.data.count_pink,
                  explicit_closed:res.data.explicit_task_id
                });

                if(isSPOC() == 1){
                  API.get(`/api/tasks/approval?page=${page}`)
                  .then((res) => {
                    this.setState({
                      approvalTasks: res.data.data,
                      countApprovalTasks: res.data.count_approval_tasks,
                      activeApprovalTasks:res.data.active_approval_tasks
                    });
                  })
                  .catch((err) => {});
                  API.get(`/api/tasks/approval_closed?page=${page}`)
                  .then((res) => {
                    this.setState({
                      approvalTasksClosed: res.data.data,
                      countApprovalTasksClosed: res.data.count_approval_tasks,
                      isLoading: false
                    });
                  })
                  .catch((err) => {});
                }else{
                  this.setState({isLoading: false});
                }

              })
              .catch((err) => {});
          })
          .catch((err) => {});
      })
      .catch((err) => {});

      API.get(`/api/employees/get_approval_customer?page=${page}`)
      .then((res) => {
        this.setState({
          approvalCustomer: res.data.data,
          countApprovalCustomer: res.data.count,
        });
      });
  };

  allTaskDetailsOld = () => {
    Promise.all([
      this.fetchMyTasks(),
      this.fetchAllocatedTasks(),
      this.fetchClosedTasks(),
    ])
      .then((data) => {
        if (data[0].status === 200) {
          this.setState({
            myTasks: data[0].data.data,
            isLoading: false,
          });
        }

        if (data[1].status === 200) {
          this.setState({
            allocatedTasks: data[1].data.data,
          });
        }

        if (data[2].status === 200) {
          this.setState({
            closedTasks: data[2].data.data,
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  fetchMyTasks = async () => await API.get(`/api/tasks`);

  fetchAllocatedTasks = async () => await API.get(`/api/tasks/allocated`);

  fetchClosedTasks = async () => await API.get(`/api/tasks/closed`);

  handleTabs = (event) => {
    if (event.currentTarget.className === "active") {
      //DO NOTHING
    } else {
      var elems = document.querySelectorAll('[id^="tab_"]');
      var elemsContainer = document.querySelectorAll('[id^="show_tab_"]');
      var currId = event.currentTarget.id;

      for (var i = 0; i < elems.length; i++) {
        elems[i].classList.remove("active");
      }

      for (var j = 0; j < elemsContainer.length; j++) {
        elemsContainer[j].style.display = "none";
      }

      event.currentTarget.classList.add("active");
      event.currentTarget.classList.add("active");
      document.querySelector("#show_" + currId).style.display = "block";
      this.setState({ tabsClicked: currId });
    }
    // this.tabElem.addEventListener("click",function(event){
    //     alert(event.target);
    // }, false);
  };

  showCreateTaskPopup = () => {
    this.setState({ showCreateTasks: true });
  };

  showCreateCustomerPopup = () => {
    this.setState({ showCreateCustomer: true });
  };

  handleClose = (closeObj) => {
    this.setState(closeObj);
  };

  openOutlookPopup = (ot_id) => {
    var pattern = /^[a-zA-Z0-9]*$/;

    if (ot_id !== null && ot_id.match(pattern)) {
      API.get(`/api/outlook/details/${ot_id}`)
      .then((res) => {
        if (res.data.status === 1) {

          let outlookPopup  = false;
          let customerPopup = false;

          if(res.data.open_customer_popup == true){
            customerPopup = true;
          }else{
            outlookPopup  = true;
          }

          this.setState({
            outlookShowCreateTasks: outlookPopup,
            outlookShowCustomerAdd: customerPopup,
            outlook_content: res.data.content,
            cust_company_details: res.data.cust_company_details,
            outlook_attachments: res.data.attachments,
            ot_id: res.data.ot_id,
          });
        } else {
          this.setState({ outlookShowCreateTasks: false,outlookShowCustomerAdd: false });
        }
      })
      .catch((err) => { 
        
      });
    } else {
    }
  }

  updateState = (arr) => {
    if (arr !== undefined) {
      this.setState({
        myTasks: [],
        allocatedTasks: [],
        closedTasks: [],
        queryString: "",
        countMyTasks: 0,
        countAllocatedTasks: 0,
        countClosedTasks: 0,
        countPink:0,
        update_state: false,
      });

      this.setState({
        myTasks: arr[0].myTasks,
        countMyTasks: arr[1].countMyTasks,
        allocatedTasks: arr[2].allocatedTasks,
        countAllocatedTasks: arr[3].countAllocatedTasks,
        closedTasks: arr[4].closedTasks,
        countClosedTasks: arr[5].countClosedTasks,
        queryString: arr[6].queryString,
        update_state: true,
        explicit_my_tasks: arr[7].explicit_my_tasks,
        explicit_allocated: arr[8].explicit_allocated,
        explicit_closed: arr[9].explicit_closed,
        countPink: arr[10].count_pink,
      });
    }
  };
  
  openOutlookPopupExplicit = (ot_id) => {
    var pattern = /^[a-zA-Z0-9]*$/;

    if (ot_id !== null && ot_id.match(pattern)) {
      API.get(`/api/outlook/details/${ot_id}`)
      .then((res) => {
        if (res.data.status === 1) {

          let outlookPopup  = true;
          let customerPopup = false;

          this.setState({
            outlookShowCreateTasks: outlookPopup,
            outlookShowCustomerAdd: customerPopup,
            outlook_content: res.data.content,
            cust_company_details: res.data.cust_company_details,
            outlook_attachments: res.data.attachments,
            ot_id: res.data.ot_id,
            deselectTeam:true
          });
        } else {
          this.setState({ outlookShowCreateTasks: false,outlookShowCustomerAdd: false });
        }
      })
      .catch((err) => { 
        
      });
    } else {
    }
  }

  downloadXLSX = (e) => {
    e.preventDefault();

    if (this.state.tabsClicked === "tab_1") {
      API.get(`/api/tasks/my_task_excel?${this.state.queryString}`, {
        responseType: "blob",
      })
        .then((res) => {
          //console.log(res);
          let url = window.URL.createObjectURL(res.data);
          let a = document.createElement("a");
          a.href = url;
          a.download = "my_tasks.xlsx";
          a.click();
        })
        .catch((err) => {});
    } else if (this.state.tabsClicked === "tab_2") {
      API.get(`/api/tasks/allocated_excel?${this.state.queryString}`, {
        responseType: "blob",
      })
        .then((res) => {
          //console.log(res);
          let url = window.URL.createObjectURL(res.data);
          let a = document.createElement("a");
          a.href = url;
          a.download = "allocated_tasks.xlsx";
          a.click();
        })
        .catch((err) => {});
    } else if (this.state.tabsClicked === "tab_3") {
      API.get(`/api/tasks/closed_excel?${this.state.queryString}`, {
        responseType: "blob",
      })
        .then((res) => {
          //console.log(res);
          let url = window.URL.createObjectURL(res.data);
          let a = document.createElement("a");
          a.href = url;
          a.download = "close_tasks.xlsx";
          a.click();
        })
        .catch((err) => {});
    }
  };

  showAnalytics = () => {
    let prev_state = this.state.show_analytics_module;
    let text = '';
    if(prev_state === true){
      text = 'Show Analytics'
    }else{
      text = 'Hide Analytics'
    }

    this.setState({analytics_text:text,
    show_analytics_module:!prev_state,
    isLoading:false});

  }

  reloadCountApprovalCustomer = (count) => {
    this.setState({countApprovalCustomer:count});
  }

  render() {
    var my_id = getMyId();
    //console.log('count', this.state.countMyTasks);
    const { designation } = getUserDisplayName(localStorage.token);
    if (this.state.isLoading) {
      return (
        <>
          {/* <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy">
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div> */}
          <div className="loderOuter">
            <div className="loader">
              <img src={loaderlogo} alt="logo" />
              <div className="loading">Loading ...</div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <>
          <div className="content-wrapper">
            <section className="content-header">
              <h1>
                Dashboard
                <small>{`${designation}`} Overview {isSPOC() == 1?<span style={{fontSize:'12px'}} >(Regional Spoc For {fullLangName()} Language)</span>:''}</small>
              </h1>
              <button className="btn btn-fill analyticsBtn" style={{ marginRight: '115px' }} onClick={() => this.showCreateCustomerPopup()}>Create User</button>
              <button className="btn btn-fill analyticsBtn" onClick={()=>{this.setState({isLoading:true});this.showAnalytics();}} >{this.state.analytics_text}</button>
            </section>
            <section className="content dashboard-top">
              <div className="row">
                {this.state.update_state === true && (
                  <MyCustomerBox
                    {...this.props}
                    queryString={this.state.queryString}
                  />
                )}
                {this.state.update_state === true && (
                  <MyTaskBox queryString={this.state.queryString} />
                )}
                {this.state.update_state === true && (
                  <SlaBoxTask queryString={this.state.queryString} />
                )}
                {this.state.update_state === true && (
                  <SlaBox queryString={this.state.queryString} />
                )}
              </div>

              {this.state.show_analytics_module === true && this.state.update_state === true && (
                <AnalyticsModule
                  {...this.props}
                  queryString={this.state.queryString}
                  emp_id={my_id}
                />
              )}

              {/* {(this.state.tabsClicked === 'tab_1' && this.state.countMyTasks > 0) && <a href="javascript:void(0);" onClick={(e) => this.downloadXLSX(e)} ><i className="fas fa-download"></i>Download Excel</a>}

                            {(this.state.tabsClicked === 'tab_2' && this.state.countAllocatedTasks > 0) && <a href="javascript:void(0);" onClick={(e) => this.downloadXLSX(e)} ><i className="fas fa-download"></i>Download Excel</a>}

                            {(this.state.tabsClicked === 'tab_3' && this.state.countClosedTasks > 0) && <a href="javascript:void(0);" onClick={(e) => this.downloadXLSX(e)} ><i className="fas fa-download"></i>Download Excel</a>} */}

              <div className="clearfix serchapanel">
                <CSCDashboardSearch changeState={this.updateState} />
              </div>

              <div className="row">
                <div className="col-xs-12">
                  <div className="nav-tabs-custom">
                    <ul className="nav nav-tabs">
                      <li
                        className={`${(this.state.activeApprovalTasks === 0)?'active':''}`}
                        onClick={(e) => this.handleTabs(e)}
                        id="tab_1"
                      >
                        MY TASKS ({this.state.countMyTasks})
                      </li>
                      <li onClick={(e) => this.handleTabs(e)} id="tab_2">
                        ALLOCATED TASKS ({this.state.countAllocatedTasks})
                      </li>
                      <li onClick={(e) => this.handleTabs(e)} id="tab_3">
                        CLOSED TASKS ({this.state.countClosedTasks}) <span className="pink-total-notification" >{this.state.countPink}</span>
                      </li>

                      {isSPOC() == 1 && <li 
                      className={`${(this.state.activeApprovalTasks > 0)?'active':''}`}
                      onClick={(e) => this.handleTabs(e)} id="tab_4">
                        PENDING REVIEWS ({this.state.countApprovalTasks})
                      </li>}

                      {isSPOC() == 1 && <li onClick={(e) => this.handleTabs(e)} id="tab_5">
                        CLOSED REVIEWS ({this.state.countApprovalTasksClosed})
                      </li>}

                      {this.state.countApprovalCustomer > 0 && <li id="tab_6" onClick={(e) => this.handleTabs(e)}>
                        CUSTOMER APPROVAL ({this.state.countApprovalCustomer})
                      </li>}

                      <li
                        className="plus-item"
                        onClick={() => this.showCreateTaskPopup()}
                      >
                        <i className="fas fa-plus"></i>
                      </li>

                      <li className="tabButtonSec pull-right">
                        {this.state.tabsClicked === "tab_1" &&
                          this.state.countMyTasks > 0 && (
                            <a
                              href="javascript:void(0);"
                              onClick={(e) => this.downloadXLSX(e)}
                            >
                              <LinkWithTooltip
                                tooltip={`Click here to download excel`}
                                href="#"
                                id="tooltip-my"
                                clicked={(e) => this.checkHandler(e)}
                              >
                                <i className="fas fa-download"></i>
                              </LinkWithTooltip>
                            </a>
                          )}

                        {this.state.tabsClicked === "tab_2" &&
                          this.state.countAllocatedTasks > 0 && (
                            <a
                              href="javascript:void(0);"
                              onClick={(e) => this.downloadXLSX(e)}
                            >
                              <LinkWithTooltip
                                tooltip={`Click here to download excel`}
                                href="#"
                                id="tooltip-allocated"
                                clicked={(e) => this.checkHandler(e)}
                              >
                                <i className="fas fa-download"></i>
                              </LinkWithTooltip>
                            </a>
                          )}

                        {this.state.tabsClicked === "tab_3" &&
                          this.state.countClosedTasks > 0 && (
                            <a
                              href="javascript:void(0);"
                              onClick={(e) => this.downloadXLSX(e)}
                            >
                              <LinkWithTooltip
                                tooltip={`Click here to download excel`}
                                href="#"
                                id="tooltip-close"
                                clicked={(e) => this.checkHandler(e)}
                              >
                                <i className="fas fa-download"></i>
                              </LinkWithTooltip>
                            </a>
                          )}
                      </li>

                      {/* <li className="pull-right pull-right-off">
                                                <span className="text-muted pull-right">
                                                    <img src={calenderTaskImage} alt="calendar" />
                                                </span>
                                            <div className="search-form pull-left">
                                                    <div className="form-group has-feedback">
                                                        <label htmlFor="search" className="sr-only">Search</label>
                                                        <input type="text" className="form-control" name="search" ></input>
                                                        <img src={searchImage} alt="search" className="form-control-feedback" />
                                                    </div>
                                                </div>
                                            </li> */}
                    </ul>
                    <div className="tab-content">
                      <div className={`tab-pane ${(this.state.activeApprovalTasks === 0)?'active':''}`} id="show_tab_1">
                        {this.state.myTasks &&
                          this.state.myTasks.length > 0 && (
                            <MyTasksTable
                              dashType="CSC"
                              tableData={this.state.myTasks}
                              countMyTasks={this.state.countMyTasks}
                              queryString={this.state.queryString}
                              allTaskDetails={(e) => this.allTaskDetails()}
                              currentTab={this.state.tabsClicked}
                            />
                          )}

                          {this.state.myTasks &&
                          this.state.myTasks.length === 0 && (
                            <div className="noData">
                              {this.state.explicit_my_tasks > 0 ? <>This task is not assigned to you. <a href="#" onClick={(e)=>{e.preventDefault();window.open(`/user/user_task_details/${this.state.explicit_my_tasks}`, "_blank");}} ><b>Click here</b></a> if you wish to see the task details</>:`No Data Found.`}
                            </div>
                          )}

                        <CreateMyTasksPopup
                          showCreateTasks={this.state.showCreateTasks}
                          handleClose={this.handleClose}
                          allTaskDetails={(e) => this.allTaskDetails()}
                        />

                      </div>

                      <div className="tab-pane" id="show_tab_2">
                        {this.state.allocatedTasks &&
                          this.state.allocatedTasks.length > 0 && (
                            <AllocatedTasksTable
                              dashType="CSC"
                              tableData={this.state.allocatedTasks}
                              countAllocatedTasks={
                                this.state.countAllocatedTasks
                              }
                              queryString={this.state.queryString}
                              allTaskDetails={(e) => this.allTaskDetails()}
                            />
                          )}

                          {this.state.allocatedTasks &&
                          this.state.allocatedTasks.length === 0 && (
                            <div className="noData">
                              {this.state.explicit_allocated > 0 ? <>This task is not assigned to you. <a href="#" onClick={(e)=>{e.preventDefault();window.open(`/user/user_task_details/${this.state.explicit_allocated}`, "_blank");}} ><b>Click here</b></a> if you wish to see the task details</>:`No Data Found.`}
                            </div>
                          )}
                      </div>

                      <div className="tab-pane" id="show_tab_3">
                        {this.state.closedTasks &&
                          this.state.closedTasks.length > 0 && (
                            <ClosedTasksTable
                              dashType="CSC"
                              tableData={this.state.closedTasks}
                              countClosedTasks={this.state.countClosedTasks}
                              queryString={this.state.queryString}
                              allTaskDetails={(e) => this.allTaskDetails()}
                            />
                          )}

                          {this.state.closedTasks &&
                          this.state.closedTasks.length === 0 && (
                            <div className="noData">
                              {this.state.explicit_closed > 0 ? <>This task is not assigned to you. <a href="#" onClick={(e)=>{e.preventDefault();window.open(`/user/user_task_details/${this.state.explicit_closed}`, "_blank");}} ><b>Click here</b></a> if you wish to see the task details</>:`No Data Found.`}
                            </div>
                          )}
                      </div>

                      {isSPOC() == 1 && <div className={`tab-pane ${(this.state.activeApprovalTasks > 0)?'active':''}`} id="show_tab_4">
                        {this.state.approvalTasks &&
                          this.state.approvalTasks.length > 0 && (
                            <ApprovalTasksTable
                              dashType="CSC"
                              tableData={this.state.approvalTasks}
                              countApprovalTasks={this.state.countApprovalTasks}
                              queryString={this.state.queryString}
                              allTaskDetails={(e) => this.allTaskDetails()}
                            />
                          )}

                        {this.state.approvalTasks &&
                          this.state.approvalTasks.length === 0 && (
                          <div className="noData">No Data Found</div>
                          )}
                      </div>}

                      {isSPOC() == 1 && <div className={`tab-pane`} id="show_tab_5">
                        {this.state.approvalTasksClosed &&
                          this.state.approvalTasksClosed.length > 0 && (
                            <ApprovalTasksTable
                              dashType="CSC"
                              tableData={this.state.approvalTasksClosed}
                              countApprovalTasks={this.state.countApprovalTasksClosed}
                              queryString={this.state.queryString}
                              allTaskDetails={(e) => this.allTaskDetails()}
                            />
                          )}

                        {this.state.approvalTasksClosed &&
                          this.state.approvalTasksClosed.length === 0 && (
                          <div className="noData">No Data Found</div>
                          )}
                      </div>}

                      {this.state.countApprovalCustomer > 0 && (
                        <div className={`tab-pane`} id="show_tab_6">
                          <CustomerApproval
                            tableData={this.state.approvalCustomer}
                            countApprovalCustomer={this.state.countApprovalCustomer}
                            reloadCountApprovalCustomer={this.reloadCountApprovalCustomer}
                            currentTab={this.state.tabsClicked}
                          />
                        </div>
                      )}

                    </div>

                    {this.state.showCreateCustomer === true &&
                      <CreateCustomerPopup
                        showCreateCustomer={this.state.showCreateCustomer}
                        handleClose={this.handleClose} 
                      />
                    }

                    {console.log("otlk",this.state.outlookShowCreateTasks)}
                    {this.state.outlookShowCreateTasks === true && (
                      <ParamsCreateMyTasksPopup
                        ot_id={this.state.ot_id}
                        outlook_content={this.state.outlook_content}
                        cust_company_details={this.state.cust_company_details}
                        outlook_attachments={this.state.outlook_attachments}
                        showCreateTasks={true}
                        handleClose={this.handleClose}
                        allTaskDetails={(e) => this.allTaskDetails()}
                        deselectTeam={this.state.deselectTeam}
                      />
                    )}

                    {this.state.showCustomerApproval === true && (
                      <ReviewRequestPopup
                        customer_id={this.state.customer_approval_id}
                        assign_id={this.state.team_approval_id}
                        showModal={true}
                        handleClose={this.handleClose}
                        allTaskDetails={(e) => this.allTaskDetails()}
                      />
                    )}

                    {this.state.outlookShowCustomerAdd === true && (
                      <CreateCustomerPopup
                        ot_id={this.state.ot_id}
                        showCreateCustomer={this.state.outlookShowCustomerAdd}
                        cust_company_details={this.state.cust_company_details}
                        handleClose={this.handleClose} 
                        openOutlook={this.openOutlookPopup}
                        openOutlookExplicit={this.openOutlookPopupExplicit}
                      />
                    )}

                  </div>
                </div>
              </div>
            </section>
          </div>
        </>
      );
    }
  }
}

export default CSCOverview;
