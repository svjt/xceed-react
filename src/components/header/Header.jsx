import React, { Component, Fragment } from 'react';
import { Row, Col } from "react-bootstrap";
import "./Header.css";

// import router
import { Link, withRouter } from 'react-router-dom';

// connect to store
import { connect } from 'react-redux';
import { authLogout,getNotifications,getNotificationCount,closeNotification } from "../../store/actions/auth";
// get user display name
import { getUserDisplayName,getMyPic,choosePic } from '../../shared/helper';
import { showErrorMessageFront } from "../../shared/handle_error_front";

import logoImage from '../../assets/images/drreddylogo.png';
import userImage from '../../assets/images/user-icon.svg';
import whitelogo from "../../assets/images/drreddylogo_white.png";

import { Button, ButtonToolbar, Modal } from "react-bootstrap";

import {isMobile} from 'react-device-detect';
import API from "../../shared/axios";
import swal from "sweetalert";
import ReactHtmlParser from 'react-html-parser';

class TopHeader extends Component {

    constructor(props){
        super(props);
        this.state = {
            openProfile: false,
            toggleMenu : false,
            showModal : false,
            //showNotification:false,
            update_profile_loader:false,
            error_uplaod:'',
            reload:false
        }

    }

    modalShowHandler = () => {       
        this.setState({ showModal: true, error_uplaod:'', choose_pic:choosePic() });
        // this.props.history.push({
        //     pathname: `/user/profile`
        // });
    };
    modalCloseHandler = () => {
        this.setState({ showModal: false });
    };

    // SATYAJIT
    displayProfile = () => {
        this.setState({
            openProfile : !this.state.openProfile
        });
    }
    // SATYAJIT
    displayNotification = () => {
        this.setState({
            openProfile : false
        });
    }
    // SATYAJIT
    setContainerRef = (node) => {
        this.containerRef = node;
    }

    // SATYAJIT 27/02/2020
    setNotificationRef = (node) => {
        this.notifyRef = node;
    }

    // SATYAJIT
    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
        //console.log("Header Load");
        //FOR WEB PAGE
        if (isMobile) {
            document.body.classList.add('sidebar-collapse');
        }else{
            document.body.classList.add('sidebar-open');
        }
        // Suvojit --Please check this.. make this call when u r clicking on notification bell icon
        //if( this.props.isLoggedIn === true){   // SATYAJIT
    }

    componentDidUpdate() {
        //console.log('clicked');
        //this.props.getNotificationCount();
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    // SATYAJIT
    handleClickOutside = (event) => {
        if (this.containerRef && !this.containerRef.contains(event.target)) {
            this.setState({
                openProfile : false
            });
        }

        if(this.notifyRef && !this.notifyRef.contains(event.target)){
            this.props.closeNotification({countNotification:this.props.auth.countNotification,countUnreadNotification:this.props.auth.countUnreadNotification});
        }
    }

    handleToggleMenu = () => {
        if(document.body.classList.contains('sidebar-open')) {
            document.body.classList.remove('sidebar-open');
            document.body.classList.add('sidebar-collapse');

        } else if(document.body.classList.contains('sidebar-collapse')) {
            document.body.classList.add('sidebar-open');
            document.body.classList.remove('sidebar-collapse');
        }
    }

    // SATYAJIT
    logout = () => {
        this.props.logMeOut();        
        this.props.history.push('/');
    }

    
    changeDateFormat = (input_date) => {
        if (input_date) {
        var dateFormat = require('dateformat');
        //return dateFormat(input_date.replace("-", "/"), "d mmm yyyy")

        var date_time = input_date.split(' ');
        var date = date_time[0].split('-');
        var date_format = new Date(date[0], (date[1] - 1), date[2]);
        return dateFormat(date_format, "d mmm yyyy");
        }
    }

    readUserNotification = (e,notificationId, taskId, taskType) => {
        this.props.getNotificationCount();
        //console.log('hello world')
        e.preventDefault(); 
        
        let requestData = {
          notification_id: notificationId,
          task_id: taskId
        }

        API.put(`/api/employees/notifications`, requestData)
        .then(response => {
            if(taskType === 6){
                // this.props.history.push({
                //     pathname: `/user/task_details/${taskId}/${response.data.data}`
                // });

                
                window.location.href = `/user/task_details/${taskId}/${response.data.data}/#discussion`;

            }else{
                // this.props.history.push({
                //     pathname: `/user/task_details/${taskId}/${response.data.data}/#discussion`
                // });
                // this.props.history.push({
                //     pathname: `/user/task_details/${taskId}/${response.data.data}`
                // });

                window.location.href = `/user/task_details/${taskId}/${response.data.data}`;

            }

           
        })
        .catch(err => {
            this.props.history.push(
                `/user/dashboard`
            );
        });
      }

    // clickOpenNotification = () => { //console.log(this.state.showNotification);
    //     this.setState({openNotification : !this.state.openNotification,openProfile : false});

    //     if( this.props.isLoggedIn === true){              
    //             API.get(`/api/employees/notifications`)
    //             .then(res => {
    //                 this.setState({
    //                     countNotification       : res.data.total_count,
    //                     countUnreadNotification : res.data.count,
    //                     dataNotification        : res.data.data,
    //                     openProfile : false
    //                 });
    //             })
    //             .catch(err => {
    //                 console.log(err);
    //             });
    //     }
        
    // }    

    setUpdatedPic = (res,choose_pic) => {
        return new Promise((resolve,reject)=>{
            document.getElementById('img_pic').src = res.data.profile_url;
            localStorage.setItem('token', res.data.token);
            this.setState({choose_pic:choose_pic,error_uplaod:''});
            resolve(true);
        });
    }

    render() {
  
        if( this.props.isLoggedIn === false) return null;

        // display logged in user name
        const { name, designation } = getUserDisplayName();

        //console.log("logo image", logoImage );
       
        return  ( 
                    <>
                    <header className="main-header">            
                        {/*<!-- Logo -->*/}
                        <Link to="/" className="logo">
                            {/*<!-- mini logo for sidebar mini 50x50 pixels -->*/}
                            <span className="logo-mini"><img src={logoImage} alt="Dr.Reddy's" /></span>
                            {/*<!-- logo for regular state and mobile devices -->*/}
                            <span className="logo-lg"><img src={logoImage} alt="Dr.Reddy's" /></span>
                        </Link>
                        {/*<!-- Header Navbar: style can be found in header.less -->*/}
                        <nav className="navbar navbar-static-top" role="navigation">
                                
                            <span   className="sidebar-toggle"
                                    onClick={this.handleToggleMenu}
                                    data-toggle="offcanvas" role="button">

                                <i className="fas fa-bars"></i>
                            </span>

                                <div className="navbar-custom-menu">
                                <ul className="nav navbar-nav">
                                    {/* <li ref={this.setContainerRef} className={this.state.openNotification === true ? "dropdown notifications-menu open" : "dropdown notifications-menu"} onClick={this.displayNotification}>
                                        <span className="dropdown-toggle" data-toggle="dropdown">
                                            <i className="far fa-bell"></i>                                        
                                            <span className="label label-danger">5</span>
                                            <div className="clearFix"></div>
                                        </span>
                                        <ul className="dropdown-menu">
                                        <li className="header">You have 10 notifications</li>
                                        <li>
                                            <ul className="menu">
                                                <li>
                                                <Link to="/">
                                                <i className="fa fa-users text-aqua"></i> 5 new members joined today
                                                </Link>
                                                </li>
                                                <li>
                                                <Link to="/">
                                                <i className="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
                                                </Link>
                                                </li>
                                                <li>
                                                <Link to="/">
                                                <i className="fa fa-users text-red"></i> 5 new members joined
                                                </Link>
                                                </li>
                                                <li>
                                                <Link to="/">
                                                <i className="fa fa-shopping-cart text-green"></i> 25 sales made
                                                </Link>
                                                </li>
                                                <li>
                                                <Link to="/">
                                                <i className="fa fa-user text-red"></i> You changed your username
                                                </Link>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="footer"><Link to="/">View all</Link></li>
                                    </ul>
                                    </li> */}
                                    {/*<!-- User Account: style can be found in dropdown.less -->*/}
                                    
                                    <li className="nav-item dropdown show notificationNav" ref={this.setNotificationRef} >

                                    {this.props.auth.countNotification > 0 ?
                                        <span 
                                            className="nav-link" 
                                            data-toggle="dropdown"                                            
                                            aria-expanded="true" 
                                            onClick={()=>{ this.props.clickOpenNotification(this.props.auth.openNotification)}}
                                        > 
                                            <i className="far fa-bell"></i> <span className="badge badge-danger navbar-badge">{this.props.auth.countUnreadNotification}</span> 
                                        </span> : 'No Notifications'
                                     }
                                    

                                    {(this.props.auth.openNotification && this.props.auth.notificationData && this.props.auth.notificationData.length > 0 )?  
                                   
                                    <div  className= "dropdown-menu dropdown-menu-lg dropdown-menu-right notificationDisplay show">
                                        {this.props.auth.notificationData.map((n, index_noti) => (
                                            
                                                
                                                <span
                                                    
                                                    onClick={(e) => {  
                                                        this.readUserNotification(e,n.n_id, n.task_id, n.n_type)
                                                    }}
                                                    action='replace'
                                                    className="dropdown-item"
                                                    key={index_noti}
                                                >
                                                    <div className={`media`}>
                                                        <div className={`media-body ${(n.read_status === 1)?``:`notificationRead`}`}>
                                                            <p className="text-sm" >
                                                                {ReactHtmlParser(n.notification_text)}
                                                            </p>
                                                            <p className="text-sm" >
                                                                {ReactHtmlParser(n.notification_subject)}
                                                            </p>
                                                            <p className="text-sm text-muted">{this.changeDateFormat(n.add_date)}</p>
                                                        </div>
                                                    </div>
                                                </span>
                                                // <div className="dropdown-divider"></div>
                                            
                                        ))
                                       
                                        } </div> : ''
                                    }
                                    
                                    </li>
                                    
                                    <li ref={this.setContainerRef} className={this.state.openProfile === true ? "dropdown user user-menu open" : "dropdown user user-menu"} onClick={this.displayProfile}>
                                    
                                        <span className="dropdown-toggle" data-toggle="dropdown">
                                            <img src={getMyPic()} className="user-image" alt="User Img" />

                                            <span className="hidden-xs user-name">{name}</span>
                                            <i className="fa fa-angle-down"></i>
                                            <div className="clearFix"></div>
                                        </span>
                                        <ul className="dropdown-menu">
                                            {/*<!-- User image -->*/}
                                            <li className="user-header">
                                                <img onClick={e => this.modalShowHandler()} src={getMyPic()} className="user-image" alt="User Img" />
                                                <p>
                                                    {name} - {designation}
                                                </p>
                                            </li>
                                            {/*<!-- Menu Body -->*/}
                                            {/* <li className="user-body">
                                                <div className="col-xs-4 text-center">
                                                    <Link to="/">Followers</Link> 
                                                </div>
                                                <div className="col-xs-4 text-center">
                                                    <Link to="/">Sales</Link>
                                                </div>
                                                <div className="col-xs-4 text-center">
                                                    <Link to="/">Friends</Link>
                                                </div>
                                            </li> */}
                                            {/*<!-- Menu Footer-->*/}
                                            <li className="user-footer">
                                                <div className="pull-left">
                                                    <button onClick={this.logout} className="btn-line">Sign out</button>
                                                </div>
                                                <div className="pull-right">                                            
                                                    {/* <Link to="/user/profile" className="btn btn-default btn-flat">Profile</Link> */}
                                                    <p onClick={e => this.modalShowHandler()} className="btn-fill">Profile</p>
                                                </div>
                                            </li>

                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </header>  
                    {this.state.update_profile_loader && <div className="loderOuter">
                        <div className="loading_reddy_outer">
                            <div className="loading_reddy">
                            <img src={whitelogo} alt="loaderImage" />
                            </div>
                        </div>
                        </div>}
                    <Modal show={this.state.showModal} onHide={() => this.modalCloseHandler()} backdrop="static" dialogClassName="profile-popup">
                        <Modal.Header closeButton>
                            <Modal.Title>Update Profile</Modal.Title>
                        </Modal.Header>

                        <Modal.Body>
                        <div className="contBox">
                        <Row>
                            <Col xs={12} sm={12} md={12}>
                            <div className="profile">
                                <div className="imageArea">
                                    <img alt="noimage" src={getMyPic()} id="img_pic" />
                                </div>
                                <div>
                                    {this.state.choose_pic === 0 && <span style={{lineHeight:'18px', margin:'5px', cursor:'pointer'}} onClick={(e) =>{
                                        //document.getElementById('img_pic').src = URL.createObjectURL(e.target.files[0]);

                                        this.setState({update_profile_loader: true});
                                        e.preventDefault();

                                        API.delete(`/api/employees/remove_profile_pic`)
                                        .then(res => {
                                            this.setUpdatedPic(res,1).then((resp)=>{
                                                if(resp){
                                                    this.setState({update_profile_loader: false});
                                                }
                                            })
                                        })
                                        .catch(err => {
                                            document.getElementById('img_pic').src = getMyPic();
                                            this.setState({
                                                update_profile_loader: false
                                            });
                                            showErrorMessageFront(err, this.props);
                                        });
                                    }} >Remove <i className="fa fa-times"></i></span>}
                                </div>
                                <div>
                                    {this.state.choose_pic === 1 && <>
                                    
                                    <label className="custom-file-upload-header" for="sel_pic" style={{marginTop:'11px'}} >
                                        
                                        <p>Select File</p>
                                    </label>

                                    <input style={{display:'none'}} id="sel_pic" type="file" onChange={(e)=>{
                                        
                                        if(['image/gif', 'image/jpeg', 'image/png', 'image/jpg'].includes(e.target.files[0].type)){
                                            document.getElementById('img_pic').src = URL.createObjectURL(e.target.files[0]);
                                        }

                                        this.setState({update_profile_loader: true});
                                        e.preventDefault();

                                        var formData = new FormData();
                                        formData.append('file', e.target.files[0]);

                                        API.post(`/api/employees/update_profile_pic`,formData).then(res => {
                                            
                                            this.setUpdatedPic(res,0).then((resp)=>{
                                                if(resp){
                                                    this.setState({update_profile_loader: false});
                                                }
                                            });
                                        }).catch(error => {
                                            document.getElementById('img_pic').src = getMyPic();
                                            let err = JSON.parse(error.request.response);
                                            this.setState({
                                                update_profile_loader: false,
                                                error_uplaod:err.errors.file_name
                                            });
                                        });  

                                        }} 
                                    />
                                    <p class="small" style={{marginTop:10}}>Only files with the following extensions are allowed: gif, jpeg, jpg and png<br />Maximum upload size 5 MB</p>
                                    {this.state.error_uplaod != '' && <p className="errorMsg" >{this.state.error_uplaod}</p>}
                                    </>}
                                </div>
                            </div>
                            </Col>
                        </Row>
                        </div>
                        </Modal.Body>

                        {/* <Modal.Footer>
                                <button className={`btn-fill m-r-10`} type="button" onClick={e => this.modalCloseHandler()}>Close</button>
                        </Modal.Footer> */}
                    </Modal>
                    </>     
                );
             }  
}
 
const mapStateToProps = state => {
    // console.log('state',state);
	return {
	  ...state
	};
};

const mapDispatchToProps = dispatch => {
	//console.log("dispatch called")
	return {
        clickOpenNotification: (data, onSuccess, setErrors) => dispatch(getNotifications(data, onSuccess, setErrors)),
        getNotificationCount: (data, onSuccess, setErrors) => dispatch(getNotificationCount(data, onSuccess, setErrors)),
        closeNotification:(data, onSuccess, setErrors) => dispatch(closeNotification(data, onSuccess, setErrors)),
        logMeOut: () => dispatch(authLogout())
        
	};
};

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(TopHeader));

//export default TopHeader;