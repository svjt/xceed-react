import React, { Component, Fragment } from 'react';
import { Row, Col } from "react-bootstrap";
import "./Header.css";

// import router
import { Link, withRouter } from 'react-router-dom';

// connect to store
import { connect } from 'react-redux';
import { authLogout } from "../../store/actions/auth";
// get user display name
import { getUserDisplayName,getMyPic,choosePic } from '../../shared/helper';
import { showErrorMessage } from "../../shared/handle_error";

import logoImage from '../../assets/images/drreddylogo.png';
import userImage from '../../assets/images/user-icon.svg';
import whitelogo from "../../assets/images/drreddylogo_white.png";

import { Button, ButtonToolbar, Modal } from "react-bootstrap";

import {isMobile} from 'react-device-detect';
import API from "../../shared/axios";
import swal from "sweetalert";

class TopHeader extends Component {

    constructor(){
        super();
        this.state = {
            openProfile: false,
            openNotification: false,
            toggleMenu : false,
            showModal : false,
            showNotification:false,
            apiNotificationStatus:false,
            countNotification:0,
            dataNotification:[],
            update_profile_loader:false,
            error_uplaod:''

        }
    }

    modalShowHandler = () => {       
        this.setState({ showModal: true, error_uplaod:''});
        // this.props.history.push({
        //     pathname: `/user/profile`
        // });
    };
    modalCloseHandler = () => {
        this.setState({ showModal: false });
    };

    // SATYAJIT
    displayProfile = () => {
        this.setState({
            openProfile : !this.state.openProfile,
            openNotification : false,
            showNotification:false
        });
    }
    // SATYAJIT
    displayNotification = () => {
        this.setState({
            openProfile : false,
            openNotification : !this.state.openNotification
        });
    }
    // SATYAJIT
    setContainerRef = (node) => {
        this.containerRef = node;
    }

    // SATYAJIT 27/02/2020
    setNotificationRef = (node) => {
        this.notifyRef = node;
    }

    // SATYAJIT
    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
        
        //FOR WEB PAGE
        if (isMobile) {
            document.body.classList.add('sidebar-collapse');
        }else{
            document.body.classList.add('sidebar-open');
        }

        // Suvojit --Please check this.. make this call when u r clicking on notification bell icon
        //if( this.props.isLoggedIn === true){   // SATYAJIT
        
        if( this.props.isLoggedIn === true && this.state.showNotification === false){
            // alert();
            API.get(`/api/employees/notifications`)
            .then(res => {
                this.setState({
                    apiNotificationStatus : true,
                    countNotification     : res.data.count,
                    dataNotification      : res.data.data
                });
            })
            .catch(err => {
                console.log(err);
            });
            this.setState({choose_pic:choosePic()});
        }
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    // SATYAJIT
    handleClickOutside = (event) => {
        if (this.containerRef && !this.containerRef.contains(event.target)) {
            this.setState({
                openProfile : false,
                openNotification : false
            });
        }

        if(this.notifyRef && !this.notifyRef.contains(event.target)){
            this.setState({showNotification:false})
        }
    }

    handleToggleMenu = () => {
        if(document.body.classList.contains('sidebar-open')) {
            document.body.classList.remove('sidebar-open');
            document.body.classList.add('sidebar-collapse');

        } else if(document.body.classList.contains('sidebar-collapse')) {
            document.body.classList.add('sidebar-open');
            document.body.classList.remove('sidebar-collapse');
        }
    }

    // SATYAJIT
    logout = () => {
        this.props.dispatch(authLogout());
        this.props.history.push('/');
    }

    
    changeDateFormat = (input_date) => {
        if (input_date) {
        var dateFormat = require('dateformat');
        //return dateFormat(input_date.replace("-", "/"), "d mmm yyyy")

        var date_time = input_date.split(' ');
        var date = date_time[0].split('-');
        var date_format = new Date(date[0], (date[1] - 1), date[2]);
        return dateFormat(date_format, "d mmm yyyy");
        }
    }

    readUserNotification = (notificationId, taskId) => {
        console.log('hello world')
        //e.preventDefault(); 

        let requestData = {
          notification_id: notificationId,
          task_id: taskId
        }

        API.put(`/api/employees/notifications`, requestData)
        .then(response => {
            this.props.history.push(
                `/user/task_details/${taskId}/${response.data.data}`
            );
        })
        .catch(err => {
            this.props.history.push(
                `/user/dashboard`
            );
        });
      }

    clickOpenNotification = () => {
        this.setState({showNotification: !this.state.showNotification});
    }    

    render() {
  
        if( this.props.isLoggedIn === false) return null;

        // display logged in user name
        const { name, designation } = getUserDisplayName();

        //console.log("logo image", logoImage );
       
        return  ( 
                    <>
                    <header className="main-header">            
                        {/*<!-- Logo -->*/}
                        <Link to="/" className="logo">
                            {/*<!-- mini logo for sidebar mini 50x50 pixels -->*/}
                            <span className="logo-mini"><img src={logoImage} alt="Dr.Reddy's" /></span>
                            {/*<!-- logo for regular state and mobile devices -->*/}
                            <span className="logo-lg"><img src={logoImage} alt="Dr.Reddy's" /></span>
                        </Link>
                        {/*<!-- Header Navbar: style can be found in header.less -->*/}
                        <nav className="navbar navbar-static-top" role="navigation">
                                
                            <span   className="sidebar-toggle"
                                    onClick={this.handleToggleMenu}
                                    data-toggle="offcanvas" role="button">

                                <i className="fas fa-bars"></i>
                            </span>

                                <div className="navbar-custom-menu">
                                <ul className="nav navbar-nav">
                                    {/* <li ref={this.setContainerRef} className={this.state.openNotification === true ? "dropdown notifications-menu open" : "dropdown notifications-menu"} onClick={this.displayNotification}>
                                        <span className="dropdown-toggle" data-toggle="dropdown">
                                            <i className="far fa-bell"></i>                                        
                                            <span className="label label-danger">5</span>
                                            <div className="clearFix"></div>
                                        </span>
                                        <ul className="dropdown-menu">
                                        <li className="header">You have 10 notifications</li>
                                        <li>
                                            <ul className="menu">
                                                <li>
                                                <Link to="/">
                                                <i className="fa fa-users text-aqua"></i> 5 new members joined today
                                                </Link>
                                                </li>
                                                <li>
                                                <Link to="/">
                                                <i className="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
                                                </Link>
                                                </li>
                                                <li>
                                                <Link to="/">
                                                <i className="fa fa-users text-red"></i> 5 new members joined
                                                </Link>
                                                </li>
                                                <li>
                                                <Link to="/">
                                                <i className="fa fa-shopping-cart text-green"></i> 25 sales made
                                                </Link>
                                                </li>
                                                <li>
                                                <Link to="/">
                                                <i className="fa fa-user text-red"></i> You changed your username
                                                </Link>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="footer"><Link to="/">View all</Link></li>
                                    </ul>
                                    </li> */}
                                    {/*<!-- User Account: style can be found in dropdown.less -->*/}
                                    
                                    <li className="nav-item dropdown show notificationNav">

                                    {this.state.apiNotificationStatus && 
                                        <span ref={this.setNotificationRef}
                                            className="nav-link" 
                                            data-toggle="dropdown"                                            
                                            aria-expanded="true" 
                                            onClick={()=>{ this.clickOpenNotification()}}
                                        > 
                                            <i className="far fa-bell"></i> <span className="badge badge-danger navbar-badge">{this.state.countNotification}</span> 
                                        </span>
                                     }
                                    
                                    <div 
                                        className={this.state.showNotification === true ? 
                                                "dropdown-menu dropdown-menu-lg dropdown-menu-right notificationDisplay show" :
                                                ""}
                                    >    


                                    {this.state.showNotification && this.state.dataNotification && this.state.dataNotification.length > 0 &&  

                                        this.state.dataNotification && this.state.dataNotification.length > 0 && this.state.dataNotification.map((n, index) => (
                                            <div key={index}>                                                
                                                <span key={index}
                                                    //action='replace'
                                                    className={`dropdown-item`}                                                    
                                                >
                                                    <div className={`media`}>
                                                        <div className={`media-body ${(n.read_status === 1)?`notificationRead`:``}`}>
                                                            <p
                                                                className="text-sm"
                                                                onClick={() => this.readUserNotification( n.n_id, n.task_id)}
                                                            >

                                                                {n.n_type === 1 &&
                                                                `Status for ${n.task_ref} has been updated.`
                                                                }

                                                                {n.n_type === 2 &&
                                                                `Request ${n.task_ref} has been closed.`
                                                                }

                                                                {n.n_type === 3 &&
                                                                `Request ${n.task_ref} needs input from the customer.`
                                                                }

                                                                {n.n_type === 4 && n.posted_by === 0 &&
                                                                `Dr. Reddys team has posted a new response on ${n.task_ref}`
                                                                }

                                                                {n.n_type === 4 && n.posted_by === 1 &&
                                                                `Customer has posted a new comment on ${n.task_ref}`
                                                                }

                                                                {n.n_type === 5 &&
                                                                `Expected Closure date for ${n.task_ref} has been updated.`
                                                                }

                                                            </p>
                                                            <p className="text-sm text-muted">{this.changeDateFormat(n.add_date)}</p>
                                                        </div>
                                                    </div>
                                                </span>
                                                <div className="dropdown-divider"></div>
                                            </div>
                                        ))
                                    }
                                    </div>
                                    </li>
                                    
                                    <li ref={this.setContainerRef} className={this.state.openProfile === true ? "dropdown user user-menu open" : "dropdown user user-menu"} onClick={this.displayProfile}>
                                    
                                        <span className="dropdown-toggle" data-toggle="dropdown">
                                            <img src={getMyPic()} className="user-image" alt="User Img" />

                                            <span className="hidden-xs user-name">{name}</span>
                                            <i className="fa fa-angle-down"></i>
                                            <div className="clearFix"></div>
                                        </span>
                                        <ul className="dropdown-menu">
                                            {/*<!-- User image -->*/}
                                            <li className="user-header">
                                                <img src={getMyPic()} className="user-image" alt="User Img" />
                                                <p>
                                                    {name} - {designation}
                                                </p>
                                            </li>
                                            {/*<!-- Menu Body -->*/}
                                            {/* <li className="user-body">
                                                <div className="col-xs-4 text-center">
                                                    <Link to="/">Followers</Link> 
                                                </div>
                                                <div className="col-xs-4 text-center">
                                                    <Link to="/">Sales</Link>
                                                </div>
                                                <div className="col-xs-4 text-center">
                                                    <Link to="/">Friends</Link>
                                                </div>
                                            </li> */}
                                            {/*<!-- Menu Footer-->*/}
                                            <li className="user-footer">
                                                <div className="pull-left">
                                                    <button onClick={this.logout} className="btn-line">Sign out</button>
                                                </div>
                                                <div className="pull-right">                                            
                                                    {/* <Link to="/user/profile" className="btn btn-default btn-flat">Profile</Link> */}
                                                    <p onClick={e => this.modalShowHandler()} className="btn-fill">Profile</p>
                                                </div>
                                            </li>

                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </header>  
                    {this.state.update_profile_loader && <div className="loderOuter">
                        <div className="loading_reddy_outer">
                            <div className="loading_reddy">
                            <img src={whitelogo} alt="loaderImage" />
                            </div>
                        </div>
                        </div>}
                    <Modal show={this.state.showModal} onHide={() => this.modalCloseHandler()} backdrop="static">
                        <Modal.Header>
                            <Modal.Title>Update Profile</Modal.Title>
                        </Modal.Header>

                        <Modal.Body>
                        <div className="contBox">
                        <Row>
                            <Col xs={12} sm={12} md={12}>
                            <div className="profile">
                                <div className="imageArea">
                                    <img alt="noimage" src={getMyPic()} id="img_pic" />
                                </div>
                                <div>
                                    {this.state.choose_pic === 0 && <span style={{lineHeight:'18px', margin:'5px', cursor:'pointer'}} onClick={(e) =>{
                                        this.setState({
                                            update_profile_loader: true
                                        });
                                        e.preventDefault();
                                        API.delete(`/api/employees/remove_profile_pic`)
                                        .then(res => {
                                            localStorage.setItem('token', res.data.token);
                                            document.getElementById('img_pic').src = res.data.profile_url;
                                            this.setState({choose_pic:choosePic(),error_uplaod:''},()=>{
                                                this.setState({update_profile_loader: false});
                                            });
                                        })
                                        .catch(err => {
                                            this.setState({
                                                update_profile_loader: false
                                            });
                                            showErrorMessage(err, this.props);
                                        });
                                    }} >Remove <i className="fa fa-times"></i></span>}
                                </div>
                                <div>
                                    {this.state.choose_pic === 1 && <><input type="file" onChange={(e)=>{
                                        this.setState({
                                            update_profile_loader: true
                                        });
                                        e.preventDefault();
                                        //this.setState({ showModal: false });
                                        //console.log('e.target.value',e.target.files[0])

                                        var formData = new FormData();
                                        formData.append('file', e.target.files[0]);

                                        API.post(`/api/employees/update_profile_pic`,formData).then(res => {
                                            localStorage.setItem('token', res.data.token);
                                            // swal({
                                            //     closeOnClickOutside: false,
                                            //     title:"Success",
                                            //     text:"Profile Has Been Updated!",
                                            //     icon: "success"
                                            //   }).then(() => {
                                            //     window.location.reload();
                                            //   });

                                            document.getElementById('img_pic').src = res.data.profile_url;
                                            this.setState({choose_pic:choosePic(),error_uplaod:''},()=>{
                                                this.setState({update_profile_loader:false});
                                            });
                                            
                                        }).catch(error => {
                                            let err = JSON.parse(error.request.response);
                                            this.setState({
                                                update_profile_loader: false,
                                                error_uplaod:err.errors.file_name
                                            });
                                        });  

                                        }} /><p class="small" style={{marginTop:10}}>Only files with the following extensions are allowed: gif, jpeg, jpg and png<br />Maximum upload size 5 MB</p>
                                        {this.state.error_uplaod != '' && <p className="errorMsg" >{this.state.error_uplaod}</p>}
                                        </>}
                                </div>
                            </div>
                            </Col>
                        </Row>
                        </div>
                        </Modal.Body>

                        <Modal.Footer>
                                <button className={`btn-fill m-r-10`} type="button" onClick={e => this.modalCloseHandler()}>Close</button>
                        </Modal.Footer>
                    </Modal>
                    </>     
                );
             }  
}
 
const mapStateToProps = state => {
	return {
	  ...state
	};
};

export default withRouter(connect(mapStateToProps)(TopHeader));