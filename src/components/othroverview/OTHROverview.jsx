import React, { Component } from "react";

import MyTasksTable from "../dashboard/MyTasksTable";
import CreateMyTasksPopup from "../dashboard/CreateMyTasksPopup";
import ParamsCreateMyTasksPopup from "../dashboard/ParamsCreateMyTasksPopup";
import OTHRDashboardSearch from "./OTHRDashboardSearch";
import AllocatedTasksTable from "../dashboard/AllocatedTasksTable";
import ApprovalTasksTable from "../dashboard/ApprovalTasksTable";

import ClosedTasksTable from "../dashboard/ClosedTasksTable";
import MyTaskBoxOther from "../dashboard/MyTaskBoxOther";

import "./Dashboard.css";
import API from "../../shared/axios";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
import { getMyId,isSPOC,fullLangName } from "../../shared/helper";

import AnalyticsModule from "../dashboard/AnalyticsModule";

class OTHROverview extends Component {
  state = {
    myTasks: [],
    allocatedTasks: [],
    closedTasks: [],
    isLoading: true,
    showCreateTasks: false,
    outlookShowCreateTasks: false,
    countApprovalTasks: 0,
    emp_id: 0,
    analytics_text:`Show Analytics`,
    show_analytics_module:false
  };

  componentDidMount() {
    this.allTaskDetails();
    // if(this.props.match.params.id && this.props.match.params.id !== ''){
    //     API.get( `/api/outlook/details/${this.props.match.params.id}` ).then(res => {
    //         if(res.data.status === 1){
    //             this.setState({outlookShowCreateTasks:true,outlook_content:res.data.content,outlook_attachments:res.data.attachments});
    //         }else{
    //             this.setState({outlookShowCreateTasks:false});
    //         }
    //     }).catch(err => {});
    // }
  }

  allTaskDetails = (page = 1) => {
    this.setState({
      myTasks: [],
      allocatedTasks: [],
      closedTasks: [],
      queryString: "",
      countMyTasks: 0,
      explicit_my_tasks:0,
      explicit_allocated:0,
      explicit_closed:0,
      countAllocatedTasks: 0,
      countClosedTasks: 0,
      countPink:0,
      countApprovalTasks: 0,
      countApprovalTasksClosed:0,
      activeApprovalTasks:0,
      isLoading: true,
      update_state: true,
    });

    API.get(`/api/tasks?page=${page}`)
      .then((res) => {
        this.setState({
          myTasks: res.data.data,
          countMyTasks: res.data.count_my_tasks,
          explicit_my_tasks:res.data.explicit_task_id
        });
        API.get(`/api/tasks/allocated?page=${page}`)
          .then((res) => {
            this.setState({
              allocatedTasks: res.data.data,
              countAllocatedTasks: res.data.count_allocated_tasks,
              explicit_allocated:res.data.explicit_task_id
            });
            API.get(`/api/tasks/closed?page=${page}`)
              .then((res) => {
                this.setState({
                  closedTasks: res.data.data,
                  countClosedTasks: res.data.count_closed_tasks,
                  countPink: res.data.count_pink,
                  explicit_closed:res.data.explicit_task_id
                });

                if(isSPOC() == 1){
                  API.get(`/api/tasks/approval?page=${page}`)
                  .then((res) => {
                    this.setState({
                      approvalTasks: res.data.data,
                      countApprovalTasks: res.data.count_approval_tasks,
                      activeApprovalTasks:res.data.active_approval_tasks
                    });
                  })
                  .catch((err) => {});
                  API.get(`/api/tasks/approval_closed?page=${page}`)
                  .then((res) => {
                    this.setState({
                      approvalTasksClosed: res.data.data,
                      countApprovalTasksClosed: res.data.count_approval_tasks,
                      isLoading: false
                    });
                  })
                  .catch((err) => {});
                }else{
                  this.setState({isLoading: false});
                }

              })
              .catch((err) => {});
          })
          .catch((err) => {});
      })
      .catch((err) => {});
  };

  allTaskDetailsOld = () => {
    Promise.all([
      this.fetchMyTasks(),
      this.fetchAllocatedTasks(),
      this.fetchClosedTasks(),
    ])
      .then((data) => {
        if (data[0].status === 200) {
          this.setState({
            myTasks: data[0].data.data,
            isLoading: false,
          });
        }

        if (data[1].status === 200) {
          this.setState({
            allocatedTasks: data[1].data.data,
          });
        }

        if (data[2].status === 200) {
          this.setState({
            closedTasks: data[2].data.data,
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  fetchMyTasks = async () => await API.get(`/api/tasks`);

  fetchAllocatedTasks = async () => await API.get(`/api/tasks/allocated`);

  fetchClosedTasks = async () => await API.get(`/api/tasks/closed`);

  handleTabs = (event) => {
    if (event.currentTarget.className === "active") {
      //DO NOTHING
    } else {
      var elems = document.querySelectorAll('[id^="tab_"]');
      var elemsContainer = document.querySelectorAll('[id^="show_tab_"]');
      var currId = event.currentTarget.id;

      for (var i = 0; i < elems.length; i++) {
        elems[i].classList.remove("active");
      }

      for (var j = 0; j < elemsContainer.length; j++) {
        elemsContainer[j].style.display = "none";
      }

      event.currentTarget.classList.add("active");
      event.currentTarget.classList.add("active");
      document.querySelector("#show_" + currId).style.display = "block";
    }
    // this.tabElem.addEventListener("click",function(event){
    //     alert(event.target);
    // }, false);
  };

  // showCreateTaskPopup = () => {
  //   //console.log('create new task');
  //   this.setState({showCreateTasks:true,emp_id:1});
  // }

  handleClose = (closeObj) => {
    this.setState(closeObj);
  };

  updateState = (arr) => {
    if (arr !== undefined) {
      this.setState({
        myTasks: [],
        allocatedTasks: [],
        closedTasks: [],
        queryString: "",
        countMyTasks: 0,
        countAllocatedTasks: 0,
        countClosedTasks: 0,
        countPink:0,
        update_state: false,
      });

      this.setState({
        myTasks: arr[0].myTasks,
        countMyTasks: arr[1].countMyTasks,
        allocatedTasks: arr[2].allocatedTasks,
        countAllocatedTasks: arr[3].countAllocatedTasks,
        closedTasks: arr[4].closedTasks,
        countClosedTasks: arr[5].countClosedTasks,
        queryString: arr[6].queryString,
        update_state: true,
        explicit_my_tasks: arr[7].explicit_my_tasks,
        explicit_allocated: arr[8].explicit_allocated,
        explicit_closed: arr[9].explicit_closed,
        countPink: arr[10].count_pink,
      });
    }
  };

  showAnalytics = () => {
    let prev_state = this.state.show_analytics_module;
    let text = '';
    if(prev_state === true){
      text = 'Show Analytics'
    }else{
      text = 'Hide Analytics'
    }

    this.setState({analytics_text:text,
    show_analytics_module:!prev_state,
    isLoading:false});

  }

  render() {
    var my_id = getMyId();
    if (this.state.isLoading) {
      return (
        <>
          {/* <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy">
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div> */}
          <div className="loderOuter">
            <div className="loader">
              <img src={loaderlogo} alt="logo" />
              <div className="loading">Loading ...</div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <>
          <div className="content-wrapper">
            <section className="content-header">
              <h1>
                Dashboard
                <small>Other Overview {isSPOC() == 1?<span style={{fontSize:'12px'}} >(Regional Spoc For {fullLangName()} Language)</span>:''}</small>
              </h1>
              <button className="btn btn-fill analyticsBtn" onClick={()=>{this.setState({isLoading:true});this.showAnalytics();}} >{this.state.analytics_text}</button>
            </section>
            <section className="content">
              <div className="warehousRow">
                <div className="row">
                  {this.state.update_state === true && (
                    <MyTaskBoxOther queryString={this.state.queryString} />
                  )}
                  {this.state.show_analytics_module === true && this.state.update_state === true && (
                    <AnalyticsModule
                      {...this.props}
                      queryString={this.state.queryString}
                      emp_id={my_id}
                    />
                  )}
                  <OTHRDashboardSearch changeState={this.updateState} />
                  <div className="col-sm-12 col-xs-12">
                    <div className="nav-tabs-custom">
                      <ul className="nav nav-tabs">
                        <li
                          className={`${(this.state.activeApprovalTasks === 0)?'active':''}`}
                          onClick={(e) => this.handleTabs(e)}
                          id="tab_1"
                        >
                          MY TASKS ({this.state.countMyTasks})
                          {this.state.task_notifications > 0 && (
                            <span className="label label-mark">
                              {this.state.task_notifications}
                            </span>
                          )}
                        </li>
                        <li onClick={(e) => this.handleTabs(e)} id="tab_2">
                          ALLOCATED TASKS ({this.state.countAllocatedTasks})
                        </li>
                        <li onClick={(e) => this.handleTabs(e)} id="tab_3">
                          CLOSED TASKS ({this.state.countClosedTasks}) <span className="pink-total-notification" >{this.state.countPink}</span>
                        </li>

                        {isSPOC() == 1 && <li 
                        className={`${(this.state.activeApprovalTasks > 0)?'active':''}`}
                        onClick={(e) => this.handleTabs(e)} id="tab_4">
                          PENDING REVIEWS ({this.state.countApprovalTasks})
                        </li>}

                        {isSPOC() == 1 && <li onClick={(e) => this.handleTabs(e)} id="tab_5">
                          CLOSED REVIEWS ({this.state.countApprovalTasksClosed})
                        </li>}

                        {/* <li className="plus-item" onClick={()=>this.showCreateTaskPopup()}><i className="fas fa-plus"></i></li> */}

                        {/* <li className="pull-right pull-right-off">
                                                    <span className="text-muted pull-right"><i className="fas fa-calendar-alt"></i></span>
                                                
                                                    <div className="search-form pull-left">
                                                        <div className="form-group has-feedback">
                                                            <label htmlFor="search" className="sr-only">Search</label>
                                                            <input type="text" className="form-control" name="search" ></input>
                                                            <span className="glyphicon glyphicon-search form-control-feedback"></span>
                                                        </div>
                                                    </div>
                                                </li> */}
                      </ul>
                      <div className="tab-content">
                        <div className={`tab-pane ${(this.state.activeApprovalTasks === 0)?'active':''}`} id="show_tab_1">
                          {this.state.myTasks &&
                            this.state.myTasks.length > 0 && (
                              <MyTasksTable
                                dashType="OTHR"
                                tableData={this.state.myTasks}
                                allTaskDetails={(e) => this.allTaskDetails()}
                              />
                            )}

                          {this.state.myTasks &&
                          this.state.myTasks.length === 0 && (
                            <div className="noData">
                              {this.state.explicit_my_tasks > 0 ? <>This task is not assigned to you. <a href="#" onClick={(e)=>{e.preventDefault();window.open(`/user/user_task_details/${this.state.explicit_my_tasks}`, "_blank");}} >Click here</a> if you wish to see the task details</>:`No Data Found.`}
                            </div>
                          )}

                          {/* <CreateMyTasksPopup showCreateTasks={this.props.showCreateTasks} handleClose={this.handleClose} fetchMyTasks={this.fetchMyTasks}/>

                                                    {this.state.outlookShowCreateTasks === true && <ParamsCreateMyTasksPopup outlook_content={this.state.outlook_content} outlook_attachments={this.state.outlook_attachments} showCreateTasks={true} handleClose={this.handleClose} allTaskDetails={(e) => this.allTaskDetails()}/>} */}
                        </div>

                        <div className="tab-pane" id="show_tab_2">
                          {this.state.allocatedTasks &&
                            this.state.allocatedTasks.length > 0 && (
                              <AllocatedTasksTable
                                dashType="OTHR"
                                tableData={this.state.allocatedTasks}
                                allTaskDetails={(e) => this.allTaskDetails()}
                              />
                            )}

                          {this.state.allocatedTasks &&
                          this.state.allocatedTasks.length === 0 && (
                            <div className="noData">
                              {this.state.explicit_allocated > 0 ? <>This task is not assigned to you. <a href="#" onClick={(e)=>{e.preventDefault();window.open(`/user/user_task_details/${this.state.explicit_allocated}`, "_blank");}} >Click here</a> if you wish to see the task details</>:`No Data Found.`}
                            </div>
                          )}
                        </div>

                        <div className="tab-pane" id="show_tab_3">
                          {this.state.closedTasks &&
                            this.state.closedTasks.length > 0 && (
                              <ClosedTasksTable
                                dashType="OTHR"
                                tableData={this.state.closedTasks}
                                allTaskDetails={(e) => this.allTaskDetails()}
                              />
                            )}

                          {this.state.closedTasks &&
                          this.state.closedTasks.length === 0 && (
                            <div className="noData">
                              {this.state.explicit_closed > 0 ? <>This task is not assigned to you. <a href="#" onClick={(e)=>{e.preventDefault();window.open(`/user/user_task_details/${this.state.explicit_closed}`, "_blank");}} >Click here</a> if you wish to see the task details</>:`No Data Found.`}
                            </div>
                          )}
                        </div>

                        {isSPOC() == 1 && <div className={`tab-pane ${(this.state.activeApprovalTasks > 0)?'active':''}`} id="show_tab_4">
                        {this.state.approvalTasks &&
                          this.state.approvalTasks.length > 0 && (
                            <ApprovalTasksTable
                              dashType="OTHR"
                              tableData={this.state.approvalTasks}
                              countApprovalTasks={this.state.countApprovalTasks}
                              queryString={this.state.queryString}
                              allTaskDetails={(e) => this.allTaskDetails()}
                            />
                          )}

                        {this.state.approvalTasks &&
                          this.state.approvalTasks.length === 0 && (
                          <div className="noData">No Data Found</div>
                          )}
                      </div>}

                      {isSPOC() == 1 && <div className={`tab-pane`} id="show_tab_5">
                        {this.state.approvalTasksClosed &&
                          this.state.approvalTasksClosed.length > 0 && (
                            <ApprovalTasksTable
                              dashType="OTHR"
                              tableData={this.state.approvalTasksClosed}
                              countApprovalTasks={this.state.countApprovalTasksClosed}
                              queryString={this.state.queryString}
                              allTaskDetails={(e) => this.allTaskDetails()}
                            />
                          )}

                        {this.state.approvalTasksClosed &&
                          this.state.approvalTasksClosed.length === 0 && (
                          <div className="noData">No Data Found</div>
                          )}
                      </div>}

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </>
      );
    }
  }
}

export default OTHROverview;
