import React, { Component } from 'react';
import Layout from "../layout/Layout";
import {
    Row,
    Col,
    ButtonToolbar,
    Button,
    Tooltip,
    OverlayTrigger,
    Modal,
} from "react-bootstrap";
import { Formik, Field, Form } from "formik";

class Ratings extends Component {
    render() {
        return (
            <>
                <div className="content-wrapper">
                    <section className="content-header">
                        <div className="ratebox">
                            <div className="cntrimg">
                            <img src={require('../../assets/images/rat-1.svg')} alt="" />
                            </div>
                            <div className="p-d-25">
                            <Formik
                                initialValues={""}>
                            <Form>
                                <div className="ratinfo">
                                <h4>Please rate your overall experience with us!</h4>
                                <div className="d-flex justify-content-center m-b-60">
                                            <div className="p-r-25">
                                                <label className="customCheckBox">
                                                <Field
                                                    type="checkbox"
                                                    name='Star Rating'                                            
                                                    value='1'
                                                    key='1' 
                                                />
                                                    <span className="checkmark " />
                                                </label>
                                            </div>

                                            <div className="p-r-25">
                                                <label className="customCheckBox">
                                                <Field
                                                    type="checkbox"
                                                    name='Star Rating'                                            
                                                    value='0'
                                                    key='1'  
                                                />
                                                    <span className="checkmark" />
                                                </label>
                                            </div>

                                            <div className="p-r-25">
                                                <label className="customCheckBox">
                                                <Field
                                                    type="checkbox"
                                                    name='Star Rating'                                            
                                                    value='0'
                                                    key='1'  
                                                />
                                                    <span className="checkmark" />
                                                </label>
                                            </div>

                                            <div className="p-r-25">
                                                <label className="customCheckBox">
                                                <Field
                                                    type="checkbox"
                                                    name='Star Rating'                                            
                                                    value='0'
                                                    key='1'  
                                                />
                                                    <span className="checkmark" />
                                                </label>
                                            </div>

                                            <div>
                                                <label className="customCheckBox">
                                                <Field
                                                    type="checkbox"
                                                    name='Star Rating'                                            
                                                    value='0'
                                                    key='1'  
                                                />
                                                    <span className="checkmark" />
                                                </label>
                                            </div>
                                        </div>

                                <p>Tell us what went well</p>
                                <Row>
                                    <Col sm={12}>
                                      <div className="form-group">
                                        {/* <label>
                                          First Name
                                          <span className="impField">*</span>
                                        </label> */}
                                        <Field
                                          name="Add Comment"
                                          type="text"
                                          className={`form-control inputratng`}
                                          placeholder="Add a comment"
                                          autoComplete="off"
                                        />
                                      </div>
                                    </Col>
                                  </Row>   

                                  <div className="d-flex justify-content-center txtbtn">
                                      <button type="submit" className="submit">Submit Review</button>
                                      </div>   
                                </div>
                            </Form>
                            </Formik>
                            </div>
                        </div>
                    </section>
                </div>

            </>
        );
    }
}

export default Ratings;