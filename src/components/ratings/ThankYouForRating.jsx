import React, { Component } from 'react';

class ThankYouForRating extends Component {
    render() {
        return (
            <>
                 <div className="content-wrapper">
                    <section className="content-header">
                    <div className="ratebox m-t-0">
                    <div className="thankuhead">
                        <div className="d-flex justify-content-center txtcntr">
                            <div>
                        <img src={require('../../assets/images/thank-You.svg')} alt="" />
                        <p className="thankutxt">Thank you!</p>
                        </div>
                        </div>
                    </div>
                    <div className="p-d-25">
                    <div className="infopara">
                        <p>We received your ratings and feedback. This will help us to improve on the overall process. </p>
                        <p className="txtcntr m-b-5">Mind checking your pending reviews?</p>
                        <div className="d-flex justify-content-center infopara"><a href="#">Share your feedback</a></div>
                    </div>
                    </div>
                    </div>
                   </section>
                 </div>   
            </>
        );
    }
}

export default ThankYouForRating;