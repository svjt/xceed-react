import React, { Component } from 'react';
import {
    Row,
    Col,
    ButtonToolbar,
    Button,
    Tooltip,
    OverlayTrigger,
    Modal,
} from "react-bootstrap";
// import ScrollArea from 'react-scrollbar';

class OverallExperience extends Component {
    render() {
        return (
            <>
                <div className="content-wrapper">
                    <section className="content-header">
                        <div className="ratebox MXW600">
                            <div className="ovrpad">
                                <div className="d-flex justify-content-between">
                                    <div className="past"><p>Past reviews</p></div>
                                    <div className="feedbck"> <p>Feedback Due <span className="bedge">4</span></p></div>
                                </div>

                                {/* <ScrollArea
                                speed={0.8}
                                className="area"
                                contentClassName="content"
                                horizontal={false}
                            > */}
                                <div className="revbox">
                                    <div className="p-d-15">
                                <Row>
                                    <Col sm={8}>
                                        <div className="enqnmbr">
                                            <p className="m-b-25">Enquiry No. <strong> 01234567 </strong></p>
                                            <p>Description:<strong> Need Abacavir Base to be delivered by 18th July,2020 in quantity of… </strong></p>
                                        </div>
                                    </Col>
                                    <Col sm={4}>
                                    <div className="pndng">
                                        <p>Pending feedback 
                                        <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Empty</Tooltip>}>
                                            <a href="#" className="infoIcon"><img src={require('../../assets/images/tooltip-i.svg')} alt=""
                                            className="m-l-5"/></a>
                                            </OverlayTrigger>
                                        </p>
                                        <button className="ratebtn">Rate Us</button>
                                        </div>
                                    </Col>
                                  </Row>  
                                  </div>
                                  </div>

                                  <div className="revbox">
                                    <div className="p-d-15">
                                <Row>
                                    <Col sm={8}>
                                        <div className="enqnmbr">
                                            <p className="m-b-25">Enquiry No. 4598762</p>
                                           <p> Description: <strong> Dispatch of API product on 4th June, 2020. Invoice settlement… </strong> </p>
                                        </div>
                                    </Col>
                                    <Col sm={4}>
                                    <div className="pndng">
                                        <p>Feedback received <img src={require('../../assets/images/green-tick.svg')} alt="" className="m-l-5" /> </p>
                                        <p className="ratsmall">Your rating</p>
                                        <img src={require('../../assets/images/rating-img.svg')} alt="" />
                                        </div>
                                    </Col>
                                  </Row>  
                                  </div>
                                  </div>
                                  {/* </ScrollArea> */}
                            </div>
                        </div>
                    </section>
                </div>
            </>
        );
    }
}

export default OverallExperience;