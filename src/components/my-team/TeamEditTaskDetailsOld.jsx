import React, {lazy, Component } from 'react';
import { Row,Col,Button,Panel,PanelGroup,Alert,Tooltip,OverlayTrigger, Modal, ButtonToolbar } from "react-bootstrap";
import dateFormat from 'dateformat';

import API from "../../shared/axios";

import { Redirect, Link } from 'react-router-dom';
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import Loader from 'react-loader-spinner';
import DatePicker from 'react-datepicker';
import swal from "sweetalert";
import Select from "react-select";

import { htmlDecode } from '../../shared/helper';
import {showErrorMessageFront} from "../../shared/handle_error_front";

import mapValues from 'lodash/mapValues';
import { localDate,localDateOnly } from '../../shared/helper';

import whitelogo from '../../assets/images/drreddylogo_white.png';
import exclamationImage from '../../assets/images/exclamation-icon-black.svg';

const initialValues = {
    country_id: null,
    product_id: 0,
    content: '',
    pharmacopoeia: '',
    polymorphic_form: '',
    nature_of_issue: '',
    batch_number: '',
    quantity: '',
    units: '',
    rdd: '',
    stability_data_type: '',
    audit_visit_site_name: '',
    request_audit_visit_date: '',

    service_request_type: {
        field_sample: 'Samples',
        field_working: 'Working Standards',
        field_impurites: 'Impurities'
    },

    sampleBatchNo: 0,
    sampleQuantity: 0,
    sampleUnit: '',

    workingQuantity: 0,
    workingUnit: '',

    impureRequired: '',
    impureQuantity: 0,
    impureUnit: '',

    shipping_address: '',

    samplesCheck: false,
    workingCheck: false,
    impureCheck: false
};

/*For Tooltip*/
function LinkWithTooltipText({ id, children, href, tooltip, clicked }) {
    return (
      <OverlayTrigger
        overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
        placement="top"
        delayShow={300}
        delayHide={150}
        trigger={["hover"]}
      >
        <Link to={href} onClick={clicked}>
          {children}
        </Link>
      </OverlayTrigger>
    );
  }
  /*For Tooltip*/

class TeamEditTaskDetails extends Component {

    constructor(props) {
        super(props);

        //const {changeStatus} = this.props.location.state;

        this.state = {
            taskDetails: [],
            country: [],
            product: [],
            ccCust:[],
            ccSelCust:[],
            task_id: this.props.match.params.id,
            assign_id: this.props.match.params.assign_id,
            posted_for: this.props.match.params.posted_for,

            service_request_type: {
                check_sample: false,
                check_working: false,
                check_impurities: false
            },


            sampleBatchNo: 0,
            sampleQuantity: 0,
            sampleUnit: '',

            workingQuantity: 0,
            workingUnit: '',

            impureRequired: '',
            impureQuantity: 0,
            impureUnit: '',

            shipping_address: '',
            editLoader:true,
            updateTaskLoader:false
        };
    }

    toggleCheckbox = (e, setFieldValue) => {

        if (e.target.value === 'Samples') {
            this.setState({
                service_request_type: {
                    check_sample: !this.state.service_request_type.check_sample,
                    check_working: this.state.service_request_type.check_working,
                    check_impurities: this.state.service_request_type.check_impurities
                }
            })
        }

        if (e.target.value === 'Working Standards') {
            this.setState({
                service_request_type: {
                    check_sample: this.state.service_request_type.check_sample,
                    check_working: !this.state.service_request_type.check_working,
                    check_impurities: this.state.service_request_type.check_impurities
                }
            })
        }

        if (e.target.value === 'Impurities') {
            this.setState({
                service_request_type: {
                    check_sample: this.state.service_request_type.check_sample,
                    check_working: this.state.service_request_type.check_working,
                    check_impurities: !this.state.service_request_type.check_impurities
                }
            })
        }

        //console.log('====',this.state.service_request_type)

    }

    fetchTaskList = (id, assign_id) => (API.get(`/api/my_team_tasks/${id}/${assign_id}`));

    fetchCountryList = () => (API.get(`/api/feed/country`));

    fetchProductList = () => (API.get(`/api/feed/products`));

    componentDidMount = () => {

        if (this.state.task_id > 0) {

            API.get( `/api/feed/country` ).then(res => {

                var country = [];
                for (let index = 0; index < res.data.data.length; index++) {
                    const element = res.data.data[index];
                    country.push({
                    value: element["country_id"],
                    label: htmlDecode(element["country_name"])
                    });
                }

                this.setState({country: country});

                API.get( `/api/feed/products` ).then(res => {
                    
                    this.setState({product: res.data.data});

                    API.get( `/api/my_team_tasks/${this.state.task_id}/${this.state.assign_id}/${this.state.posted_for}` ).then(res => {

                        var ccTeam   = [];
                        var ccSelTeam = [];
                        
                        for (let index = 0; index < res.data.data.ccCutomerList.length; index++) {
                            const element = res.data.data.ccCutomerList[index];
                            ccTeam.push({
                            value: element["customer_id"],
                            label: element["first_name"] +" "+ element["last_name"]
                            });
                        }

                        for (let index = 0; index < res.data.data.selCCCustList.length; index++) {
                            const element = res.data.data.selCCCustList[index];
                            ccSelTeam.push({
                            value: element["customer_id"],
                            label: element["first_name"] +" "+ element["last_name"]
                            });
                        }

                        this.setState({
                            ccCust:ccTeam,
                            ccSelCust:ccSelTeam,
                            taskDetails: res.data.data.taskDetails
                        });

                        var quantity = [];

                        if (this.state.taskDetails.request_type === 7 || this.state.taskDetails.request_type === 23) {
                            quantity = this.state.taskDetails.quantity.split(" ");
                        }

                        //console.log("quantity", quantity)

                        initialValues.ccCustId   = this.state.ccSelCust
                        initialValues.country_id = this.state.taskDetails.country_id
                        initialValues.product_id = this.state.taskDetails.product_id
                        initialValues.content = htmlDecode(this.state.taskDetails.content)
                        initialValues.pharmacopoeia = htmlDecode(this.state.taskDetails.pharmacopoeia)
                        initialValues.polymorphic_form = htmlDecode(this.state.taskDetails.polymorphic_form)
                        initialValues.nature_of_issue = htmlDecode(this.state.taskDetails.nature_of_issue)
                        initialValues.batch_number = this.state.taskDetails.batch_number

                        initialValues.quantity = (quantity.length > 0) ? quantity[0] : 0
                        initialValues.units = (quantity.length > 0) ? quantity[1] : ''
                        initialValues.rdd = (this.state.taskDetails.rdd !== null) ? localDateOnly(this.state.taskDetails.rdd) : ''

                        initialValues.stability_data_type = this.state.taskDetails.stability_data_type
                        initialValues.audit_visit_site_name = htmlDecode(this.state.taskDetails.audit_visit_site_name)
                        initialValues.request_audit_visit_date = (this.state.taskDetails.request_audit_visit_date !== null) ? localDateOnly(this.state.taskDetails.request_audit_visit_date) : '';

                        // for samples only
                        if (this.state.taskDetails.request_type === 1) {

                            let requestType = this.state.taskDetails.service_request_type.split(",");

                            let sampleSplit = [];
                            let workingSplit = [];
                            let impureSplit = [];

                            if (this.state.taskDetails.quantity) {
                                sampleSplit = this.state.taskDetails.quantity.split(" ");
                            }
                            if (this.state.taskDetails.working_quantity) {
                                workingSplit = this.state.taskDetails.working_quantity.split(" ");
                            }
                            if (this.state.taskDetails.impurities_quantity) {
                                impureSplit = this.state.taskDetails.impurities_quantity.split(" ");
                            }

                            this.setState({
                                service_request_type: {
                                    check_sample: (requestType.indexOf('Samples') === -1) ? false : true,
                                    check_working: (requestType.indexOf('Working Standards') === -1) ? false : true,
                                    check_impurities: (requestType.indexOf('Impurities') === -1) ? false : true
                                },

                                sampleBatchNo: (requestType.indexOf('Samples') === -1) ? 0 : this.state.taskDetails.number_of_batches,
                                sampleQuantity: (sampleSplit.length > 0) ? sampleSplit[0] : '',
                                sampleUnit: (sampleSplit.length > 0) ? sampleSplit[1] : '',


                                workingQuantity: (workingSplit.length > 0) ? workingSplit[0] : '',
                                workingUnit: (workingSplit.length > 0) ? workingSplit[1] : '',


                                impureRequired: (impureSplit.length > 0) ? htmlDecode(this.state.taskDetails.specify_impurity_required) : '',
                                impureQuantity: (impureSplit.length > 0) ? impureSplit[0] : '',
                                impureUnit: (impureSplit.length > 0) ? impureSplit[1] : '',
                                shipping_address: htmlDecode(this.state.taskDetails.shipping_address)
                            });

                            //update initial values
                            initialValues.sampleBatchNo = this.state.sampleBatchNo
                            initialValues.sampleQuantity = this.state.sampleQuantity
                            initialValues.sampleUnit = this.state.sampleUnit
                            initialValues.workingQuantity = this.state.workingQuantity
                            initialValues.workingUnit = this.state.workingUnit
                            initialValues.impureRequired = this.state.impureRequired
                            initialValues.impureQuantity = this.state.impureQuantity
                            initialValues.impureUnit = this.state.impureUnit
                            initialValues.shipping_address = this.state.shipping_address
                            //end here
                        }
                        this.setState({editLoader:false});
                    }).catch(err => {});
                }).catch(err => {});
            }).catch(err => {});


            // Promise.all([this.fetchTaskList(this.state.task_id, this.state.assign_id), this.fetchCountryList(), this.fetchProductList()])
            // .then((data) => {

            //     if (data[0].status === 200) {

            //         this.setState({
            //             taskDetails: data[0].data.data.taskDetails
            //         });

            //         var quantity = [];

            //         if (this.state.taskDetails.request_type === 7 || this.state.taskDetails.request_type === 23) {
            //             quantity = this.state.taskDetails.quantity.split(" ");
            //         }

            //         //console.log("quantity", quantity)

            //         initialValues.country_id = this.state.taskDetails.country_id
            //         initialValues.product_id = this.state.taskDetails.product_id
            //         initialValues.content = htmlDecode(this.state.taskDetails.content)
            //         initialValues.pharmacopoeia = htmlDecode(this.state.taskDetails.pharmacopoeia)
            //         initialValues.polymorphic_form = htmlDecode(this.state.taskDetails.polymorphic_form)
            //         initialValues.nature_of_issue = htmlDecode(this.state.taskDetails.nature_of_issue)
            //         initialValues.batch_number = this.state.taskDetails.batch_number

            //         initialValues.quantity = (quantity.length > 0) ? quantity[0] : 0
            //         initialValues.units = (quantity.length > 0) ? quantity[1] : ''
            //         initialValues.rdd = (this.state.taskDetails.rdd !== null) ? localDateOnly(this.state.taskDetails.rdd) : ''

            //         initialValues.stability_data_type = this.state.taskDetails.stability_data_type
            //         initialValues.audit_visit_site_name = htmlDecode(this.state.taskDetails.audit_visit_site_name)
            //         initialValues.request_audit_visit_date = localDateOnly(this.state.taskDetails.request_audit_visit_date)

            //         // for samples only
            //         if (this.state.taskDetails.request_type === 1) {

            //             let requestType = this.state.taskDetails.service_request_type.split(",");

            //             let sampleSplit = [];
            //             let workingSplit = [];
            //             let impureSplit = [];

            //             if (this.state.taskDetails.quantity) {
            //                 sampleSplit = this.state.taskDetails.quantity.split(" ");
            //             }
            //             if (this.state.taskDetails.working_quantity) {
            //                 workingSplit = this.state.taskDetails.working_quantity.split(" ");
            //             }
            //             if (this.state.taskDetails.impurities_quantity) {
            //                 impureSplit = this.state.taskDetails.impurities_quantity.split(" ");
            //             }

            //             this.setState({
            //                 service_request_type: {
            //                     check_sample: (requestType.indexOf('Samples') === -1) ? false : true,
            //                     check_working: (requestType.indexOf('Working Standards') === -1) ? false : true,
            //                     check_impurities: (requestType.indexOf('Impurities') === -1) ? false : true
            //                 },

            //                 sampleBatchNo: (requestType.indexOf('Samples') === -1) ? 0 : this.state.taskDetails.number_of_batches,
            //                 sampleQuantity: (sampleSplit.length > 0) ? sampleSplit[0] : '',
            //                 sampleUnit: (sampleSplit.length > 0) ? sampleSplit[1] : '',


            //                 workingQuantity: (workingSplit.length > 0) ? workingSplit[0] : '',
            //                 workingUnit: (workingSplit.length > 0) ? workingSplit[1] : '',


            //                 impureRequired: (impureSplit.length > 0) ? htmlDecode(this.state.taskDetails.specify_impurity_required) : '',
            //                 impureQuantity: (impureSplit.length > 0) ? impureSplit[0] : '',
            //                 impureUnit: (impureSplit.length > 0) ? impureSplit[1] : '',
            //                 shipping_address: htmlDecode(this.state.taskDetails.shipping_address)
            //             });

            //             //update initial values
            //             initialValues.sampleBatchNo = this.state.sampleBatchNo
            //             initialValues.sampleQuantity = this.state.sampleQuantity
            //             initialValues.sampleUnit = this.state.sampleUnit
            //             initialValues.workingQuantity = this.state.workingQuantity
            //             initialValues.workingUnit = this.state.workingUnit
            //             initialValues.impureRequired = this.state.impureRequired
            //             initialValues.impureQuantity = this.state.impureQuantity
            //             initialValues.impureUnit = this.state.impureUnit
            //             initialValues.shipping_address = this.state.shipping_address
            //             //end here

            //             //console.log('>>>', sampleSplit.length);
            //         }
            //     }

            //     if (data[1].status === 200) {
            //         this.setState({
            //             country: data[1].data.data
            //         });
            //     }

            //     if (data[2].status === 200) {
            //         this.setState({
            //             product: data[2].data.data
            //         });
            //     }
            // })
            // .catch((err) => {
            //     console.log(err)
            // });
        }
    }

    closeBrowserWindow = () => {
        window.close()
    }

    checkPharmacopoeia(request_type, parent_id) {

        if (request_type === null) {
            return false;
        } else {
            if ((request_type === 22 || request_type === 21 || request_type === 19 || request_type === 18 || request_type === 17 || request_type === 2 || request_type === 3 || request_type === 4 || request_type === 10 || request_type === 12 || request_type === 1) && parent_id === 0) {
                return true;
            } else {
                return false;
            }
        }

    }

    checkPolymorphicForm(request_type, parent_id) {
        if (request_type === null) {
            return false;
        } else {
            if ((request_type === 2 || request_type === 3) && parent_id === 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    checkStabilityDataType(request_type, parent_id) {
        if (request_type === null) {
            return false;
        } else {
            if (request_type === 13 && parent_id === 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    checkAuditSiteName(request_type, parent_id) {
        if (request_type === null) {
            return false;
        } else {
            if (request_type === 9 && parent_id === 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    checkAuditVistDate(request_type, parent_id) {
        if (request_type === null) {
            return false;
        } else {
            if (request_type === 9 && parent_id === 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    checkQuantity(request_type, parent_id) {
        if (request_type === null) {
            return false;
        } else {
            if ((request_type === 7 || request_type === 23) && parent_id === 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    checkBatchNo(request_type, parent_id) {
        if (request_type === null) {
            return false;
        } else {
            if (request_type === 7 && parent_id === 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    checkNatureOfIssue(request_type, parent_id) {
        if (request_type === null) {
            return false;
        } else {
            if (request_type === 7 && parent_id === 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    checkRDD(request_type, parent_id) {
        if (request_type === null) {
            return false;
        } else {
            if (request_type === 23 && parent_id === 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    setDateFormat(date_value) {
        var mydate = localDate(date_value);
        return dateFormat(mydate, "dd/mm/yyyy");
    }

    checkSampleRequest(request_type, parent_id) {
        if (request_type === null) {
            return false;
        } else {
            if (request_type === 1 && parent_id === 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    checkProductRequest = (request_type, parent_id) => {
        if (request_type === null) {
            return false;
        } else {
            if (request_type !== 24 && parent_id === 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    checkMarketRequest = (request_type, parent_id) => {
        if (request_type === null) {
            return false;
        } else {
            if (request_type !== 24 && parent_id === 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    submitEditTask = (values, actions) => {
        this.setState({updateTaskLoader:true});
        let
            req_type = this.state.taskDetails.request_type,
            pid = this.state.taskDetails.parent_id,
            endpoint = '',
            postObject = {};

        postObject.ccCust = values.ccCustId;    

        if (this.checkProductRequest(req_type, pid) === true) {
            postObject.product_id = values.product_id;
        }

        if (this.checkMarketRequest(req_type, pid) === true) {  
            if(values.country_id !== ""){
                postObject.country_id = JSON.stringify(values.country_id);
            }else{
                postObject.country_id = 0;
            }        
        }
        
        if (this.checkPharmacopoeia(req_type, pid) === true) {
            postObject.pharmacopoeia = values.pharmacopoeia;
        }

        if (this.checkPolymorphicForm(req_type, pid) === true) {
            postObject.polymorphic_form = values.polymorphic_form;
        }

        if (this.checkNatureOfIssue(req_type, pid) === true) {
            postObject.nature_of_issue = values.nature_of_issue;
        }

        if (this.checkBatchNo(req_type, pid) === true) {
            postObject.batch_number = values.batch_number;
        }

        if (this.checkQuantity(req_type, pid) === true) {
            postObject.quantity = values.quantity + ' ' + values.units;
        }

        if (this.checkRDD(req_type, pid) === true) {
            postObject.rdd = dateFormat(values.rdd,'yyyy-mm-dd');
        }

        if (this.checkAuditVistDate(req_type, pid) === true) {
            postObject.request_audit_visit_date = dateFormat(values.request_audit_visit_date,'yyyy-mm-dd');
        }

        if (this.checkStabilityDataType(req_type, pid) === true) {
            postObject.stability_data_type = values.stability_data_type;
        }

        if (this.checkAuditSiteName(req_type, pid) === true) {
            postObject.audit_visit_site_name = values.audit_visit_site_name;
        }

        if (pid === 0) {
            postObject.content = values.content.trim();
        }
        //SATYAJIT
        if (this.checkSampleRequest(req_type, pid) === true) {

            if (this.state.service_request_type.check_sample) {
                postObject.samples = [
                    { no_of_batch: values.sampleBatchNo },
                    { quantity: values.sampleQuantity },
                    { unit: values.sampleUnit }
                ]
            }

            if (this.state.service_request_type.check_working) {
                postObject.working = [
                    { quantity: values.workingQuantity },
                    { unit: values.workingUnit }
                ]
            }

            if (this.state.service_request_type.check_impurities) {
                postObject.impurities = [
                    { requireText: values.impureRequired },
                    { quantity: values.impureQuantity },
                    { unit: values.impureUnit }
                ]
            }

            postObject.shipping_address = (values.shipping_address) ? values.shipping_address : ""
        }

        if (req_type === 24) {
            endpoint = `/api/my_team_tasks/update_forecast/${this.state.task_id}`
        } else {
            endpoint = `/api/my_team_tasks/${this.state.task_id}`;
        }

        //console.log(this.state.service_request_type.check_sample);   
        
        postObject.posted_for = this.state.posted_for;

        API.put(endpoint, postObject)
            .then(response => {
                this.setState({updateTaskLoader:false});
                swal({
                    closeOnClickOutside: false,
                    title: "Success",
                    text: "Task has been updated successfully.",
                    icon: "success"
                }).then(() => {
                    this.props.history.push(`/user/team_task_details/${this.state.task_id}/${this.state.assign_id}/${this.state.posted_for}`)
                })
            })
            .catch(err => {
                this.setState({updateTaskLoader:false});
                if(err.data.status === 3){
                    var token_rm = 2;
                    showErrorMessageFront(err,token_rm,this.props);
                    //this.handleClose();
                }else{
                    actions.setErrors(err.data.errors);
                    actions.setSubmitting(false);
                }
            });

        //console.log( postObject )
    }


    changeCCCust = (event,setFieldValue) => {
        if(event === null){
            setFieldValue("ccCustId", "");
        }else{
            setFieldValue("ccCustId", event);
        }      
    }

    setCountry = (event, setFieldValue) => {
        setFieldValue("country_id",event);
    }

    render() {

        //console.log('log', this.state.taskDetails.request_type);
        //console.log('sss', this.state.taskDetails.number_of_batches);

        
        
        // SATYAJIT
        if (this.props.match.params.id === 0 || this.props.match.params.id === "") {
            return <Redirect to='/user/dashboard' />
        }

        const { taskDetails, product, country, service_request_type } = this.state;

        //console.log('+++++',service_request_type);

        let validateEditTask = null;
        if(taskDetails.task_id > 0){            
            if(taskDetails.request_type === 1) {
                
                validateEditTask = Yup.object().shape({ 
                    product_id: Yup.string().trim()
                        .required("Please select product name"),
                    shipping_address: Yup.string().trim()
                        .required("Please enter shipping address"),
                    /* service_request_type: Yup.lazy(obj => Yup.object(
                        mapValues(obj, (v, k) => { 

                            console.log('====k===', k, this.state.service_request_type.check_sample, v );
                            
                          if (k.startsWith('check_impurities')) {
                            return Yup.string().required("Hello")
                          }
                        })
                      )) */
                    /* service_request_type: lazy(obj =>                             
                        Yup.object(
                                mapValues(obj, (value, key) => {
                                    console.log(value,key,'>>>>>');
                                    if (value === key || value === key || value === key) {
                                      return Yup.string().required();
                                    }
                                }
                            )
                        )
                    ) */ 
                    /* samplesCheck: Yup.bool()
                        .test(
                          'samplesCheck',
                          'You have to agree with our Terms and Conditions!',
                          value => value === true
                        )
                        .required(
                          'You have to agree with our Terms and Conditions!'
                        ), */
                        //samplesCheck: Yup.array().required('Permission not checked')
                }); 

                //console.log( validateEditTask );
                
            }else if(taskDetails.request_type === 2 || taskDetails.request_type === 3 || taskDetails.request_type === 4  || taskDetails.request_type === 5  || taskDetails.request_type === 6  || taskDetails.request_type === 8  || taskDetails.request_type === 10 || taskDetails.request_type === 11  || taskDetails.request_type === 12 || taskDetails.request_type === 14 || taskDetails.request_type === 15 || taskDetails.request_type === 16 || taskDetails.request_type === 21 || taskDetails.request_type === 22) {
                
                validateEditTask = Yup.object().shape({ 
                    product_id: Yup.string().trim()
                        .required("Please select product name")
        
                });
            }else if(taskDetails.request_type === 7) {
                validateEditTask = Yup.object().shape({ 
                    product_id: Yup.string().trim()
                        .required("Please select product name"),
                    batch_number: Yup.string().trim()
                        .required("Please enter batch number")
                        .min(10, "Batch Number cannot be less than 10 characters")
                        .max(10, "Batch Number cannot be more than 10 characters")
                        .matches(/[a-zA-Z]{4}\d{6}/, "Batch Number field is not in the right format")
                    // quantity: Yup.string().trim().notRequired().test('quantity', 'Invalid quantity', function(value) {
                    //         if (value !== '') {
                    //           const schema = Yup.string().matches(/^[0-9]*$/);
                    //           return schema.isValidSync(value);
                    //         }
                    //         return true;
                    // })
                }); 
            }else if(taskDetails.request_type === 9) {
                validateEditTask = Yup.object().shape({ 
                    product_id: Yup.string().trim()
                        .required("Please select product name"),
                    audit_visit_site_name: Yup.string().trim()
                        .required("Please enter site name"),
                    request_audit_visit_date: Yup.date().nullable()
                        .required("Audit/Visit date required")      
                }); 
            }else if(taskDetails.request_type === 13) {
                validateEditTask = Yup.object().shape({ 
                    product_id: Yup.string().trim()
                        .required("Please select product name"),
                    stability_data_type: Yup.string().trim()
                        .required("Please select data type")     
                }); 
            }else if(taskDetails.request_type === 17 || taskDetails.request_type === 18 || taskDetails.request_type === 19 || taskDetails.request_type === 20) {
                validateEditTask = Yup.object().shape({ 
                    product_id: Yup.string().trim()
                        .required("Please select product name"),
                    /* country_id: Yup.string()
                        .required("Please select country") */        
                });
            }else if(taskDetails.request_type === 23) {
                validateEditTask = Yup.object().shape({ 
                    product_id: Yup.string().trim()
                        .required("Please select product name"),
                    /* country_id: Yup.string()
                        .required("Please select country"), */ 
                    quantity: Yup.string().trim()
                        .required("Please enter quantity").matches(/^[0-9]*$/, "Invalid quantity"),
                    units: Yup.string().trim()
                        .required("Please select unit"),      
                }); 
            }            
        }

        if (taskDetails.task_id > 0 && this.state.editLoader === false ) {
            
            return (
                <>
                    {this.state.updateTaskLoader && <div className="loderOuter">
                        <div className="loading_reddy_outer">
                            <div className="loading_reddy" >
                                <img src={whitelogo} alt="loaderImage"/>
                            </div>
                        </div>
                    </div>}
                    <div className="content-wrapper">

                        <section className="content-header">
                            <h1>{taskDetails.req_name}</h1>
                            <div className="customer-edit-button btn-fill" onClick={()=>this.props.history.push(`/user/task_details/${this.state.task_id}/${this.state.assign_id}`)} >Back</div>
                        </section>
                        
                        

                        <section className="content">
                            <div className="boxPapanel content-padding form-min-hight">

                                <div className="taskdetails">

                                    {/*<div className="taskdetailsHeader">
                                        <h3>{taskDetails.req_name}</h3>
                                    </div>*/}


                                    <div className="clearfix tdBtmlist">

                                        <Formik
                                            initialValues={initialValues}
                                            validationSchema={validateEditTask}
                                            onSubmit={this.submitEditTask}
                                        >
                                            {({ values, errors, touched, isValid, isSubmitting, setFieldValue }) => {
                                                //console.log("Errors === ", errors);
                                                return (
                                                    <Form>
                                                        <Row>
                                                            {this.checkProductRequest(taskDetails.request_type, taskDetails.parent_id) === true &&
                                                                <Col xs={12} sm={6} md={3}>
                                                                    <div className="form-group  has-feedback">
                                                                        <label>Product <span class="required-field">*</span></label>

                                                                        <Field
                                                                            name="product_id"
                                                                            component="select"
                                                                            className="form-control"
                                                                            autoComplete="off"
                                                                            value={values.product_id}
                                                                        >
                                                                            <option key='-1' value="" >Select Product</option>
                                                                            {product.map((products, i) => (
                                                                                <option key={i} value={products.product_id}>
                                                                                    {htmlDecode(products.product_name)}
                                                                                </option>
                                                                            ))}
                                                                        </Field>
                                                                        {errors.product_id && touched.product_id ? (
                                                                            <span className="errorMsg">
                                                                                {errors.product_id}
                                                                            </span>
                                                                        ) : null}

                                                                    </div>
                                                                </Col>
                                                            }

                                                            {this.checkMarketRequest(taskDetails.request_type, taskDetails.parent_id) === true &&
                                                                <Col xs={12} sm={6} md={3}>
                                                                    <div className="form-group  has-feedback">
                                                                        <label>Market {this.state.request_type_id === 17 || this.state.request_type_id === 18 || this.state.request_type_id === 19 || this.state.request_type_id === 20 || this.state.request_type_id === 23 ? <span class="required-field">*</span>:''}</label>

                                                                        <Select
                                                                            isMulti
                                                                            className="form-control"
                                                                            classNamePrefix="select"
                                                                            defaultValue=""
                                                                            isClearable={true}
                                                                            isSearchable={true}
                                                                            name="country_id"
                                                                            options={country}
                                                                            value={(values.country_id !== null && values.country_id !== "") ? values.country_id : ''}
                                                                            onChange={e => { this.setCountry(e,setFieldValue) }}
                                                                        />

                                                                        {/* <Field
                                                                            name="country_id"
                                                                            component="select"
                                                                            className="form-control"
                                                                            autoComplete="off"
                                                                            value={(values.country_id !== null && values.country_id !== "") ? values.country_id : ''}
                                                                        >
                                                                            <option key='-1' value="" >Select Country</option>
                                                                            {country.map((countries, k) => (
                                                                                <option key={k} value={countries.country_id}>
                                                                                    {htmlDecode(countries.country_name)}
                                                                                </option>
                                                                            ))}
                                                                        </Field> */}
                                                                        {errors.country_id && touched.country_id ? (
                                                                            <span className="errorMsg">
                                                                                {errors.country_id}
                                                                            </span>
                                                                        ) : null}

                                                                    </div>
                                                                </Col>
                                                            }


                                                            {
                                                                this.checkPharmacopoeia(taskDetails.request_type, taskDetails.parent_id) === true &&

                                                                <Col xs={12} sm={6} md={3}>
                                                                    <div className="form-group">
                                                                        <label>Pharmacopoeia</label>
                                                                        <Field
                                                                            name="pharmacopoeia"
                                                                            type="text"
                                                                            className="form-control"
                                                                            autoComplete="off"
                                                                            value={(values.pharmacopoeia !== null && values.pharmacopoeia !== "") ? values.pharmacopoeia : ''}
                                                                        >
                                                                        </Field>
                                                                        {errors.pharmacopoeia && touched.pharmacopoeia ? (
                                                                            <span className="errorMsg">
                                                                                {errors.pharmacopoeia}
                                                                            </span>
                                                                        ) : null}
                                                                    </div>
                                                                </Col>
                                                            }

                                                            {
                                                                this.checkNatureOfIssue(taskDetails.request_type, taskDetails.parent_id) === true &&
                                                                <Col xs={12} sm={6} md={3}>
                                                                    <div className="form-group">
                                                                        <label>Nature Of Issue</label>

                                                                        <Field
                                                                            name="nature_of_issue"
                                                                            type="text"
                                                                            className="form-control"
                                                                            autoComplete="off"
                                                                            value={(values.nature_of_issue !== null && values.nature_of_issue !== "") ? values.nature_of_issue : ''}
                                                                        >
                                                                        </Field>
                                                                        {errors.nature_of_issue && touched.nature_of_issue ? (
                                                                            <span className="errorMsg">
                                                                                {errors.nature_of_issue}
                                                                            </span>
                                                                        ) : null}

                                                                    </div>
                                                                </Col>
                                                            }

                                                            {
                                                                this.checkBatchNo(taskDetails.request_type, taskDetails.parent_id) === true &&
                                                                <Col xs={12} sm={6} md={3}>
                                                                    <div className="form-group">
                                                                        <label>Batch No <span class="required-field">*</span></label>

                                                                        <Field
                                                                            name="batch_number"
                                                                            type="text"
                                                                            className="form-control"
                                                                            autoComplete="off"
                                                                            value={(values.batch_number !== null && values.batch_number !== "") ? values.batch_number : ''}
                                                                        >
                                                                        </Field>
                                                                        {errors.batch_number && touched.batch_number ? (
                                                                            <span className="errorMsg errorBatch">
                                                                                {errors.batch_number}
                                                                            </span>
                                                                        ) : null}

                                                                    </div>
                                                                </Col>
                                                            }

                                                            {
                                                                this.checkQuantity(taskDetails.request_type, taskDetails.parent_id) === true &&
                                                                <>
                                                                    <Col xs={12} sm={6} md={3}>
                                                                        <div className="form-group">
                                                                            <label>Quantity {this.state.request_type_id === 23 ? <span class="required-field">*</span>:''}</label>

                                                                            <Field
                                                                                name="quantity"
                                                                                type="text"
                                                                                className="form-control"
                                                                                autoComplete="off"
                                                                                value={(values.quantity !== null && values.quantity !== "") ? values.quantity : ''}
                                                                            >
                                                                            </Field>
                                                                            {errors.quantity && touched.quantity ? (
                                                                                <span className="errorMsg">
                                                                                    {errors.quantity}
                                                                                </span>
                                                                            ) : null}

                                                                        </div>
                                                                    </Col>
                                                                    <Col xs={12} sm={6} md={3}>
                                                                        <div className="form-group">
                                                                            <label>Units {this.state.request_type_id === 23 ? <span class="required-field">*</span>:''}</label>

                                                                            <Field
                                                                                name="units"
                                                                                component="select"
                                                                                className={`selectArowGray form-control`}
                                                                                autoComplete="off"
                                                                                value={values.units}
                                                                            >
                                                                                <option key='-1' value="" >Select</option>
                                                                                <option key='1' value="Mgs" >Mgs</option>

                                                                                {(taskDetails.request_type === 1 || taskDetails.request_type === 7) &&
                                                                                    <option key='2' value="Gms" >Gms</option>
                                                                                }

                                                                                {taskDetails.request_type === 23 &&
                                                                                    <option key='2' value="Grams" >Grams</option>
                                                                                }
                                                                                <option key='3' value="Kgs" >Kgs</option>

                                                                            </Field>
                                                                            {errors.units && touched.units ? (
                                                                                <span className="errorMsg">
                                                                                    {errors.units}
                                                                                </span>
                                                                            ) : null}

                                                                        </div>
                                                                    </Col>
                                                                </>
                                                            }


                                                            {
                                                                this.checkRDD(taskDetails.request_type, taskDetails.parent_id) === true &&
                                                                <Col xs={12} sm={6} md={3}>
                                                                    <div className="form-group">
                                                                        <label>Request Date Of Delivery</label>
                                                                        <div className="form-control">
                                                                            {/* <DatePicker
                                                            name="rdd"
                                                            onChange={value =>
                                                                setFieldValue("rdd",value)
                                                            }
                                                            value={values.rdd}
                                                        /> */}

                                                                            <DatePicker
                                                                                name="rdd"
                                                                                className="borderNone"
                                                                                selected={values.rdd ? values.rdd : null}
                                                                                onChange={value => setFieldValue('rdd', value)}
                                                                                dateFormat='dd/MM/yyyy'
                                                                                autoComplete="off"
                                                                            />

                                                                            {errors.rdd && touched.rdd ? (
                                                                                <span className="errorMsg">
                                                                                    {errors.rdd}
                                                                                </span>
                                                                            ) : null}
                                                                        </div>
                                                                    </div>
                                                                </Col>
                                                            }


                                                            {
                                                                this.checkPolymorphicForm(taskDetails.request_type, taskDetails.parent_id) === true &&
                                                                <Col xs={12} sm={6} md={3}>
                                                                    <div className="form-group">
                                                                        <label>Polymorphic Form</label>

                                                                        <Field
                                                                            name="polymorphic_form"
                                                                            type="text"
                                                                            className="form-control"
                                                                            autoComplete="off"
                                                                            value={(values.polymorphic_form !== null && values.polymorphic_form !== "") ? values.polymorphic_form : ''}
                                                                        >
                                                                        </Field>
                                                                        {errors.polymorphic_form && touched.polymorphic_form ? (
                                                                            <span className="errorMsg">
                                                                                {errors.polymorphic_form}
                                                                            </span>
                                                                        ) : null}

                                                                    </div>
                                                                </Col>
                                                            }
                                                        </Row>

                                                        {/*edit task details Start*/}
                                                        {taskDetails.request_type === 1 && <div className="edit_task_details">
                                                            <Row>

                                                                <Col xs={12} sm={12} md={12}>
                                                                    <div className="form-group1">
                                                                        <Field
                                                                            type="checkbox"
                                                                            name="samplesCheck"
                                                                            checked={service_request_type.check_sample}
                                                                            value={values.service_request_type.field_sample}
                                                                            onChange={e => {
                                                                                this.toggleCheckbox(e, setFieldValue);
                                                                            }}
                                                                        />

                                                                        <label>
                                                                            Samples
                                                    </label>
                                                    {errors.samplesCheck && touched.samplesCheck ? (
                                                                            <span className="errorMsg">
                                                                                {errors.samplesCheck}
                                                                            </span>
                                                                        ) : null}
                                                                    </div>

                                                                    {service_request_type.check_sample &&
                                                                        <Row>
                                                                            <Col xs={12} sm={6} md={3}>
                                                                                <fieldset>
                                                                                    <legend> <span className="fieldset-legend">Number of Batches</span> </legend>

                                                                                    <div className="fieldset-wrapper">
                                                                                        <label className="radio-inline">
                                                                                            <Field
                                                                                                type="radio"
                                                                                                name="sampleBatchNo"
                                                                                                checked={values.sampleBatchNo === "1" ? true : false}
                                                                                                value="1"
                                                                                            /> 1
                                                                </label>

                                                                                        <label className="radio-inline">
                                                                                            <Field
                                                                                                type="radio"
                                                                                                name="sampleBatchNo"
                                                                                                checked={values.sampleBatchNo === "2" ? true : false}
                                                                                                value="2"
                                                                                            />
                                                                                            2
                                                                </label>

                                                                                        <label className="radio-inline">
                                                                                            <Field
                                                                                                type="radio"
                                                                                                name="sampleBatchNo"
                                                                                                checked={values.sampleBatchNo === "3" ? true : false}
                                                                                                value="3"
                                                                                            />
                                                                                            3
                                                                </label>

                                                                                    </div>
                                                                                </fieldset>
                                                                            </Col>
                                                                            <Col xs={12} sm={6} md={3}>
                                                                                <div className="form-group">
                                                                                    <Field
                                                                                        name="sampleQuantity"
                                                                                        type="text"
                                                                                        className="form-control"
                                                                                        autoComplete="off"
                                                                                        placeholder="Quantity"
                                                                                        value={values.sampleQuantity}
                                                                                    >
                                                                                    </Field>
                                                                                </div>
                                                                            </Col>
                                                                            <Col xs={12} sm={6} md={3}>
                                                                                <div className="form-group">

                                                                                    <Field
                                                                                        name="sampleUnit"
                                                                                        component="select"
                                                                                        className={`selectArowGray form-control`}
                                                                                        autoComplete="off"
                                                                                        value={values.sampleUnit}
                                                                                    >
                                                                                        <option key='-1' value="" >Select</option>
                                                                                        <option key='1' value="Mgs" >Mgs</option>
                                                                                        <option key='2' value="Gms" >Grams</option>
                                                                                        <option key='3' value="Kgs" >Kgs</option>

                                                                                    </Field>
                                                                                    {errors.sampleUnit && touched.sampleUnit ? (
                                                                                        <span className="errorMsg">
                                                                                            {errors.sampleUnit}
                                                                                        </span>
                                                                                    ) : null}

                                                                                </div>
                                                                            </Col>
                                                                        </Row>
                                                                    }
                                                                </Col>


                                                                <Col xs={12} sm={12} md={12}>
                                                                    <div className="form-group1">
                                                                        <Field
                                                                            type="checkbox"
                                                                            name="workingCheck"
                                                                            checked={service_request_type.check_working}
                                                                            value={values.service_request_type.field_working}
                                                                            onChange={e => {
                                                                                this.toggleCheckbox(e, setFieldValue);
                                                                            }}
                                                                        />

                                                                        <label>
                                                                            Working Standards
                                                    </label>
                                                                    </div>

                                                                    {service_request_type.check_working &&
                                                                        <Row>
                                                                            <Col xs={12} sm={6} md={3}>
                                                                                <div className="form-group">
                                                                                    <Field
                                                                                        name="workingQuantity"
                                                                                        type="text"
                                                                                        className="form-control"
                                                                                        autoComplete="off"
                                                                                        placeholder="Quantity"
                                                                                        value={values.workingQuantity}
                                                                                    >
                                                                                    </Field>

                                                                                </div>
                                                                            </Col>
                                                                            <Col xs={12} sm={6} md={3}>
                                                                                <div className="form-group">
                                                                                    <Field
                                                                                        name="workingUnit"
                                                                                        component="select"
                                                                                        className={`selectArowGray form-control`}
                                                                                        autoComplete="off"
                                                                                        value={values.workingUnit}
                                                                                    >
                                                                                        <option key='-1' value="" >Select</option>
                                                                                        <option key='1' value="Mgs" >Mgs</option>
                                                                                        <option key='2' value="Gms" >Grams</option>
                                                                                        <option key='3' value="Kgs" >Kgs</option>

                                                                                    </Field>
                                                                                    {errors.workingUnit && touched.workingUnit ? (
                                                                                        <span className="errorMsg">
                                                                                            {errors.workingUnit}
                                                                                        </span>
                                                                                    ) : null}

                                                                                </div>
                                                                            </Col>
                                                                            <Col xs={12} sm={6} md={3}>

                                                                            </Col>
                                                                        </Row>
                                                                    }
                                                                </Col>



                                                                <Col xs={12} sm={12} md={12}>
                                                                    <div className="form-group1">

                                                                        <Field
                                                                            type="checkbox"
                                                                            name="impureCheck"
                                                                            checked={service_request_type.check_impurities}
                                                                            value={values.service_request_type.field_impurites}
                                                                            onChange={e => {
                                                                                this.toggleCheckbox(e, setFieldValue);
                                                                            }}
                                                                        />
                                                                        {errors.typeCheck && touched.typeCheck ? (
                                                                            <span className="errorMsg">
                                                                                {errors.typeCheck}
                                                                            </span>
                                                                        ) : null}

                                                                        <label>
                                                                            Impurities
                                                    </label>
                                                    {errors.service_request_type && touched.service_request_type ? (
                                                                            <span className="errorMsg">
                                                                                {errors.service_request_type}
                                                                            </span>
                                                                        ) : null}
                                                                    </div>

                                                                    {service_request_type.check_impurities &&
                                                                        <Row>
                                                                            <Col xs={12} sm={6} md={3}>
                                                                                <div className="form-group">

                                                                                    <Field
                                                                                        name="impureRequired"
                                                                                        type="text"
                                                                                        className="form-control"
                                                                                        autoComplete="off"
                                                                                        placeholder="Specify impurity required"
                                                                                        value={values.impureRequired}
                                                                                    >
                                                                                    </Field>

                                                                                </div>
                                                                            </Col>
                                                                            <Col xs={12} sm={6} md={3}>
                                                                                <div className="form-group">
                                                                                    <Field
                                                                                        name="impureQuantity"
                                                                                        type="text"
                                                                                        className="form-control"
                                                                                        autoComplete="off"
                                                                                        placeholder="Quantity"
                                                                                        value={values.impureQuantity}
                                                                                    >
                                                                                    </Field>
                                                                                </div>
                                                                            </Col>
                                                                            <Col xs={12} sm={6} md={3}>
                                                                                <div className="form-group">

                                                                                    <Field
                                                                                        name="impureUnit"
                                                                                        component="select"
                                                                                        className={`selectArowGray form-control`}
                                                                                        autoComplete="off"
                                                                                        value={values.impureUnit}
                                                                                    >
                                                                                        <option key='-1' value="" >Select</option>
                                                                                        <option key='1' value="Mgs" >Mgs</option>
                                                                                        <option key='2' value="Gms" >Grams</option>
                                                                                        <option key='3' value="Kgs" >Kgs</option>

                                                                                    </Field>
                                                                                    {errors.impureUnit && touched.impureUnit ? (
                                                                                        <span className="errorMsg">
                                                                                            {errors.impureUnit}
                                                                                        </span>
                                                                                    ) : null}

                                                                                </div>
                                                                            </Col>
                                                                        </Row>
                                                                    }
                                                                </Col>


                                                            </Row>
                                                        </div>}
                                                        {/*edit task details Start*/}

                                                        <Row>

                                                            {
                                                                this.checkStabilityDataType(taskDetails.request_type, taskDetails.parent_id) === true &&
                                                                <Col xs={12} sm={6} md={3}>
                                                                    <div className="form-group">
                                                                        <label>Stability Data Type <span class="required-field">*</span></label>

                                                                        <Field
                                                                            name="stability_data_type"
                                                                            component="select"
                                                                            className="form-control"
                                                                            autoComplete="off"
                                                                            value={values.stability_data_type}
                                                                        >
                                                                            <option key='-1' value="" >Select Type</option>
                                                                            <option key='1' value='ACC'>ACC</option>
                                                                            <option key='2' value='Long Term'>Long Term</option>
                                                                            <option key='3' value='Others'>Others</option>
                                                                        </Field>
                                                                        {errors.stability_data_type && touched.stability_data_type ? (
                                                                            <span className="errorMsg">
                                                                                {errors.stability_data_type}
                                                                            </span>
                                                                        ) : null}

                                                                    </div>
                                                                </Col>
                                                            }

                                                            {
                                                                this.checkAuditSiteName(taskDetails.request_type, taskDetails.parent_id) === true &&
                                                                <Col xs={12} sm={6} md={3}>
                                                                    <div className="form-group">
                                                                        <label>Site Name <span class="required-field">*</span></label>

                                                                        <Field
                                                                            name="audit_visit_site_name"
                                                                            type="text"
                                                                            className="form-control"
                                                                            autoComplete="off"
                                                                            value={(values.audit_visit_site_name !== null && values.audit_visit_site_name !== "") ? values.audit_visit_site_name : ''}
                                                                        >
                                                                        </Field>

                                                                        {errors.audit_visit_site_name && touched.audit_visit_site_name ? (
                                                                            <span className="errorMsg">
                                                                                {errors.audit_visit_site_name}
                                                                            </span>
                                                                        ) : null}

                                                                    </div>
                                                                </Col>
                                                            }


                                                            {
                                                                this.checkAuditVistDate(taskDetails.request_type, taskDetails.parent_id) === true &&
                                                                <Col xs={12} sm={6} md={3}>
                                                                    <div className="form-group">
                                                                        <label>Audit/Visit Date <span class="required-field">*</span></label>
                                                                        <div className="form-control">
                                                                            {/* <DatePicker
                                                            name="request_audit_visit_date"
                                                            onChange={value =>
                                                                setFieldValue("request_audit_visit_date",value)
                                                            }
                                                            value={values.request_audit_visit_date}
                                                        /> */}

                                                                            <DatePicker
                                                                                name="request_audit_visit_date"
                                                                                className="borderNone"
                                                                                selected={values.request_audit_visit_date ? values.request_audit_visit_date : null}
                                                                                onChange={value => setFieldValue('request_audit_visit_date', value)}
                                                                                dateFormat='dd/MM/yyyy'
                                                                                autoComplete="off"
                                                                            />

                                                                            
                                                                        </div>
                                                                        {errors.request_audit_visit_date && touched.request_audit_visit_date ? (
                                                                                <span className="errorMsg">
                                                                                    {errors.request_audit_visit_date}
                                                                                </span>
                                                                            ) : null}
                                                                    </div>
                                                                </Col>
                                                            }

                                                            {taskDetails.parent_id === 0 &&
                                                                <Col xs={12}>
                                                                    <div className="form-group">
                                                                        <label>Requirement</label>
                                                                        <Field
                                                                            name="content"
                                                                            component="textarea"
                                                                            className="form-control"
                                                                            autoComplete="off"
                                                                            value={(values.content !== null && values.content !== "") ? values.content : ''}
                                                                        >
                                                                        </Field>
                                                                        {errors.content && touched.content ? (
                                                                            <span className="errorMsg">
                                                                                {errors.content}
                                                                            </span>
                                                                        ) : null}
                                                                    </div>
                                                                </Col>}

                                                            {taskDetails.request_type === 1 && <Col xs={12}>
                                                                <div className="form-group">
                                                                    <label>Shipping Address <span class="required-field">*</span></label>
                                                                    <Field
                                                                        name="shipping_address"
                                                                        component="textarea"
                                                                        className="form-control"
                                                                        autoComplete="off"
                                                                        value={(values.shipping_address !== null && values.shipping_address !== "") ? values.shipping_address : ''}
                                                                    >
                                                                    </Field>
                                                                    {errors.shipping_address && touched.shipping_address ? (
                                                                        <span className="errorMsg">
                                                                            {errors.shipping_address}
                                                                        </span>
                                                                    ) : null}
                                                                </div>
                                                            </Col>}


                                                            <Col xs={12} className="spec-moa-customer">
                                                            <label>External Email notification{` `}
                                                                    <LinkWithTooltipText
                                                                    tooltip={`Select list of users within Customer’s organization who will receive email notification`}
                                                                    href="#"
                                                                    id={`tooltip-menu`}
                                                                    clicked={e => this.checkHandler(e)}
                                                                    >
                                                                    <img src={exclamationImage} alt="exclamation" />
                                                                    </LinkWithTooltipText>
                                                                </label>
                                                                <div className="customers-group">

                                                                <div className="customers-drodpwn">
                                                                    
                                                                    <Select
                                                                        isMulti
                                                                        className="basic-single"
                                                                        classNamePrefix="select"
                                                                        defaultValue={this.state.ccSelCust}
                                                                        isClearable={true}
                                                                        isSearchable={true}
                                                                        maxMenuHeight={110}
                                                                        name="ccCustId"
                                                                        options={this.state.ccCust}
                                                                        onChange={e => { this.changeCCCust(e,setFieldValue) }}
                                                                    />
                                                                    {errors.shipping_address && touched.shipping_address ? (
                                                                        <span className="errorMsg">
                                                                            {errors.shipping_address}
                                                                        </span>
                                                                    ) : null}
                                                                </div>
                                                            

                                                                    
                                                                    <Button
                                                                        onClick={this.closeBrowserWindow}
                                                                        className={`btn-line ml-10`}
                                                                        type="button"
                                                                    >
                                                                        Cancel
                                                                    </Button>
                                                                    <Button
                                                                        className="btn-fill ml-10"
                                                                        type="submit"
                                                                    >
                                                                        Update
                                                                    </Button>

         
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                    </Form>
                                                );
                                            }}
                                        </Formik>

                                    </div>
                                </div>

                            </div>
                        </section>
                    </div>
                </>
            );
        } else {

            return (
                <>
                    <div className="loderOuter">
                    <div className="loading_reddy_outer">
                        <div className="loading_reddy" >
                            <img src={whitelogo} alt="logo" />
                        </div>
                        </div>
                    </div>
                </>
            );
        }

    }
}

export default TeamEditTaskDetails;
