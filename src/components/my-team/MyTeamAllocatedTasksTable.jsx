import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  getMyDrupalId,
  localDate,
  localDateTime,
  htmlDecode,
} from "../../shared/helper";
import { showErrorMessageFront } from "../../shared/handle_error_front";
import base64 from "base-64";
import { Row, Col, Tooltip, OverlayTrigger } from "react-bootstrap";

import ReactHtmlParser from "react-html-parser";
import "./Dashboard.css";

import dateFormat from "dateformat";

import StatusColumn from "./StatusColumn";
import AllocatedSubTaskTable from "./AllocatedSubTaskTable";
import exclamationImage from "../../assets/images/exclamation-icon.svg";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";

//import TaskDetails from './TaskDetails';

import API from "../../shared/axios";
import swal from "sweetalert";

import { Link } from "react-router-dom";
import Pagination from "react-js-pagination";

import VerticalMenuAllocatedTask from "./VerticalMenuAllocatedTask";

import RespondCustomerAssignedPopup from "./RespondCustomerAssignedPopup";
import RespondBackAssignedPopup from "./RespondBackAssignedPopup";
import ReAssignAssignedPopup from "./ReAssignAssignedPopup";
import Poke from "./Poke";
import ClosedCommentMyTask from './ClosedCommentMyTask';
import ReclassifyMyTasksPopup from "../dashboard/ReclassifyMyTasksPopup";

import PokeSPOC from "./PokeSPOC";
import DeleteTaskPopup from "./DeleteTaskPopup";
import IPDOPopup from "../dashboard/IPDOPopup";

const portal_url = `${process.env.REACT_APP_PORTAL_URL}`; // SATYAJIT

const priority_arr = [
  { priority_id: 1, priority_value: "Low" },
  { priority_id: 2, priority_value: "Medium" },
  { priority_id: 3, priority_value: "High" },
];

const ipdoTypes = [1,2,3,4,5,8,9,10,12,13,14,15,16,21,38,42,35,22];

const getActions = (refObj) => (cell, row) => {
  return (
    <VerticalMenuAllocatedTask
      id={cell}
      currRow={row}
      showReAssignPopup={refObj.showReAssignPopup}
      showRespondCustomerPopup={refObj.showRespondCustomerPopup}
      showCloseTaskPopup={refObj.showCloseTaskPopup}
      showRespondAssignPopup={refObj.showRespondAssignPopup}
      poke={refObj.showPoke}
      poke_spoc={refObj.showPokeSPOC}
      showDeleteTaskPopup={refObj.showDeleteTaskPopup}
      review_task={refObj.review_task}
      teamArr={refObj.props.teamData}
      reclassify={refObj.reclassify}
    />
  );
};

const getStatusColumn = (refObj) => (cell, row) => {
  return <StatusColumn rowData={row} />;
};

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const setDescription = (refOBj) => (cell, row) => {
  if (row.parent_id > 0) {
    return (
      <LinkWithTooltip
        tooltip={`${ReactHtmlParser(row.title)}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {ReactHtmlParser(row.title)}
      </LinkWithTooltip>
    );
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${ReactHtmlParser(row.req_name)}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {ReactHtmlParser(row.req_name)}
      </LinkWithTooltip>
    );
  }
};

const setProductName = (refOBj) => (cell, row) => {
  if (cell === null) {
    return "";
  } else {
    if(row.order_verified_status == 0 && row.request_type == 23){
      return (

        <LinkWithTooltip
          tooltip={`${row.unverified_products}`}
          href="#"
          id="tooltip-1"
          clicked={(e) => refOBj.checkHandler(e)}
        >
          {row.unverified_products}
        </LinkWithTooltip>
      );
    }else{
      return (
        <LinkWithTooltip
          tooltip={`${cell}`}
          href="#"
          id="tooltip-1"
          clicked={(e) => refOBj.checkHandler(e)}
        >
          {cell}
        </LinkWithTooltip>
      );
    }
  }
};

const setCreateDate = (refObj) => (cell) => {
  var date = localDate(cell);
  return dateFormat(date, "dd/mm/yyyy");
};

const setDaysPending = (refObj) => (cell, row) => {
  var date = localDate(cell);
  return dateFormat(date, "dd/mm/yyyy");
};

const clickToShowTasks = (refObj) => (cell, row) => {
  //return (<LinkWithTooltip tooltip={`${cell}`} href="#" id="tooltip-1" clicked={e => refObj.checkHandler(e)}>{cell}</LinkWithTooltip>);

  if (row.discussion === 1) {
    return (
      <LinkWithTooltip
        tooltip={`${cell}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refObj.redirectUrlTask(e, row.task_id)}
      >
        {cell}
      </LinkWithTooltip>
    );
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${cell}`}
        href="#"
        id="tooltip-1"
        clicked={(e) =>
          refObj.redirectUrlTask(
            e,
            row.task_id,
            row.assignment_id,
            row.assigned_by
          )
        }
      >
        {cell}
      </LinkWithTooltip>
    );
  }
};

const setCompanyName = (refObj) => (cell, row) => {
  return htmlDecode(cell);
};

const setCustomerName = (refObj) => (cell, row) => {
  if (row.customer_id > 0) {
    //hard coded customer id - SATYAJIT
    //row.customer_id = 2;
    return (
      /*<Link
        to={{
          pathname:
            portal_url + "customer-dashboard/" +
            row.customer_drupal_id
        }}
        target="_blank"
        style={{ cursor: "pointer" }}
      >*/
      <LinkWithTooltip
        tooltip={`${row.first_name + " " + row.last_name}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refObj.redirectUrl(e, row.customer_id)}
      >
        {row.first_name + " " + row.last_name}
      </LinkWithTooltip>
      /*</Link>*/
    );
  } else {
    return "";
  }
};

const setPriorityName = (refObj) => (cell, row) => {
  var ret = "Set Priority";
  for (let index = 0; index < priority_arr.length; index++) {
    const element = priority_arr[index];
    if (element["priority_id"] === cell) {
      ret = element["priority_value"];
    }
  }

  return ret;
};

const setAssignedTo = (refOBj) => (cell, row) => {
  return (
    <LinkWithTooltip
      tooltip={`${
        row.at_emp_first_name +
        " " +
        row.at_emp_last_name +
        " (" +
        row.at_emp_desig_name +
        ")"
      }`}
      href="#"
      id="tooltip-1"
      clicked={(e) => refOBj.checkHandler(e)}
    >
      {row.at_emp_first_name +
        " " +
        row.at_emp_last_name +
        " (" +
        row.at_emp_desig_name +
        ")"}
    </LinkWithTooltip>
  );
};

const setAssignedBy = (refOBj) => (cell, row) => {
  if (row.assigned_by == "-1") {
    return (
      <LinkWithTooltip
        tooltip={`System`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        System
      </LinkWithTooltip>
    );
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${
          row.ab_emp_first_name +
          " " +
          row.ab_emp_last_name +
          " (" +
          row.ab_emp_desig_name +
          ")"
        }`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {row.ab_emp_first_name +
          " " +
          row.ab_emp_last_name +
          " (" +
          row.ab_emp_desig_name +
          ")"}
      </LinkWithTooltip>
    );
  }
};

const hoverDate = (refOBj) => (cell, row) => {
  return (
    <LinkWithTooltip
      tooltip={cell}
      href="#"
      id="tooltip-1"
      clicked={(e) => refOBj.checkHandler(e)}
    >
      {cell}
    </LinkWithTooltip>
  );
};

const getBreach = (refOBj) => (cell, row) => {
  var breach = false;
  if (row.breach_reason !== null && row.breach_reason !== "") {
    breach = true;
  }

  return (
    <>
      {/* <div className="actionStyle"> */}
      <div className="btn-group">
        {breach && (
          <LinkWithTooltip
            tooltip={`${htmlDecode(row.breach_reason)}`}
            href="#"
            id={`tooltip-menu-${row.task_id}`}
            clicked={(e) => this.checkHandler(e)}
          >
            <img src={exclamationImage} alt="exclamation" />
          </LinkWithTooltip>
        )}
      </div>
      {/* </div> */}
    </>
  );
};

class MyTeamAllocatedTasksTable extends Component {
  state = {
    showCreateSubTask: false,
    showIPDO: false,
    fromPopup:0,
    showReclassifyTasks:false,
    showClosedComment: false,
    showAssign: false,
    showRespondBack: false,
    showReAssign: false,
    showTaskDetails: false,
    showRespondCustomer: false,
    showAuthorizeBack: false,
    changeData: true,
    task_id: 0,
    tableData: [],
    showModalLoader: false,

    activePage: 1,
    totalCount: 0,
    itemPerPage: 20,
    showDeleteTaskPopup: false,
    reloadOnClose:false
  };

  checkHandler = (event) => {
    event.preventDefault();
  };

  redirectUrl = (event, id) => {
    event.preventDefault();
    //http://reddy.indusnet.cloud/customer-dashboard?source=Mi02NTE=
    API.post(`/api/employees/shlogin`, { customer_id: parseInt(id) })
      .then((res) => {
        if (res.data.shadowToken) {
          var url =
            process.env.REACT_APP_CUSTOMER_PORTAL +
            `setToken/` +
            res.data.shadowToken;
          window.open(url, "_blank");
        }
      })
      .catch((error) => {
        showErrorMessageFront(error, 0, this.props);
      });
  };

  redirectUrlTask = (event, task_id, assignment_id = "", posted_for = "") => {
    event.preventDefault();
    if (assignment_id === "" && posted_for === "") {
      window.open(`/user/team_discussion_details/${task_id}`, "_blank");
    } else {
      window.open(
        `/user/team_task_details/${task_id}/${assignment_id}/${posted_for}`,
        "_blank"
      );
    }
  };

  taskDetails = (row) => {
    this.setState({ showTaskDetails: true, currRow: row });
  };

  showSubTaskPopup = (currRow) => {
    //console.log("Create Sub Task");
    if(currRow.po_no === null && currRow.request_type == 23){
      swal({
        closeOnClickOutside: false,
        title: "Warning !!",
        text: "Please set Purchase Order Number first.",
        icon: "warning",
      }).then(() => {
        
      });
    }else if (currRow.request_related_to === null && ipdoTypes.includes(currRow.request_type)) {
      this.setState({ showIPDO: true, currRow: currRow, fromPopup:1 });
    }else{
      this.setState({ showCreateSubTask: true, currRow: currRow });
    }
  };

  reclassify = (currRow) => {
    //console.log("Assign Task");
    if(currRow.order_verified_status == 0 && currRow.request_type == 23){
      swal({
        closeOnClickOutside: false,
        title: "Warning !!",
        text: "Please process the order first.",
        icon: "warning",
      }).then(() => {
        
      });
    }else{
      this.setState({ showReclassifyTasks: true, currRow: currRow });
    }
  };

  showDeleteTaskPopup = (currRow) => {
    this.setState({ showDeleteTaskPopup: true, currRow: currRow });
  };

  showDeleteTaskPopupOld = (currRow) => {
    swal({
      closeOnClickOutside: false,
      title: "Cancel Request",
      text: "Are you sure you want to cancel this request?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.setState({ showModalLoader: true });
        API.delete(`/api/my_team_tasks/check_delete/${currRow.task_id}`)
          .then((res) => {
            if (res.data.has_sub_task === 1) {
              this.setState({ showModalLoader: false });
              swal({
                closeOnClickOutside: false,
                title: "Alert",
                text:
                  "This task has some open sub-tasks. \r\n Do you want to delete them as well?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              }).then((willDelete) => {
                if (willDelete) {
                  this.setState({ showModalLoader: true });
                  API.delete(`/api/my_team_tasks/delete/${currRow.task_id}`)
                    .then((res) => {
                      this.setState({ showModalLoader: false });
                      swal({
                        closeOnClickOutside: false,
                        title: "Success",
                        text: "Task has been canceled!",
                        icon: "success",
                      }).then(() => {
                        this.props.allTaskDetails();
                      });
                    })
                    .catch((err) => {
                      var token_rm = 2;
                      showErrorMessageFront(err, token_rm, this.props);
                    });
                }
              });
            } else {
              swal({
                closeOnClickOutside: false,
                title: "Success",
                text: "Task has been canceled!",
                icon: "success",
              }).then(() => {
                this.props.allTaskDetails();
              });
            }
          })
          .catch((err) => {
            var token_rm = 2;
            showErrorMessageFront(err, token_rm, this.props);
          });
      }
    });
  };

  showCloseTaskPopup = (currRow) => {

    if(currRow.new_order_type === null && currRow.request_type == 23){
      swal({
        closeOnClickOutside: false,
        title: "Warning !!",
        text: "Please set Order Type first.",
        icon: "warning",
      }).then(() => {
        
      });
    }else if (currRow.request_related_to === null && ipdoTypes.includes(currRow.request_type)) {
      this.setState({ showIPDO: true, currRow: currRow, fromPopup:2 });
    }else{
      var task_date_added = localDate(currRow.task_date_added);

      var dueDate = localDateTime(currRow.original_due_date);
      var today = localDateTime(currRow.today_date);
      var timeDiff = dueDate.getTime() - today.getTime();

      let current_date  = new Date(currRow.today_date);
      let add_date_24 = new Date(new Date(currRow.task_date_added).getTime() + 60 * 60 * 24 * 1000);
      
      if(current_date < add_date_24 || timeDiff < 0){
        //alert('change Here');
        this.setState({ showClosedComment: true, currRow: currRow,posted_for:currRow.assigned_by });
      }else{
        swal({
          closeOnClickOutside: false,
          title: "Close task",
          text: "Are you sure you want to close this task?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        }).then((willDelete) => {
          if (willDelete) {
            this.setState({ showModalLoader: true });
            API.delete(
              `/api/my_team_tasks/check_close/${currRow.task_id}/${currRow.assigned_by}`
            )
              .then((res) => {
                if (res.data.has_sub_task === 1) {
                  this.setState({ showModalLoader: false });
                  swal({
                    closeOnClickOutside: false,
                    title: "Alert",
                    text:
                      "This task has some open sub-tasks. \r\n Do you want to close them as well?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                  }).then((willDelete) => {
                    if (willDelete) {
                      this.setState({ showModalLoader: true });
                      API.delete(
                        `/api/my_team_tasks/close/${currRow.task_id}/${currRow.assigned_by}`
                      )
                        .then((res) => {
                          this.setState({ showModalLoader: false });
                          swal({
                            closeOnClickOutside: false,
                            title: "Success",
                            text: "Task has been closed!",
                            icon: "success",
                          }).then(() => {
                            this.props.allTaskDetails();
                          });
                        })
                        .catch((err) => {
                          var token_rm = 2;
                          showErrorMessageFront(err, token_rm, this.props);
                        });
                    }
                  });
                } else {
                  swal({
                    closeOnClickOutside: false,
                    title: "Success",
                    text: "Task has been closed!",
                    icon: "success",
                  }).then(() => {
                    this.props.allTaskDetails();
                  });
                }
              })
              .catch((err) => {
                var token_rm = 2;
                showErrorMessageFront(err, token_rm, this.props);
              });
          }
        });
      }
    }
  };

  showRespondCustomerPopup = (currRow) => {
    //console.log("Respond Customer");

    if(currRow.new_order_type === null && currRow.request_type == 23){
      swal({
        closeOnClickOutside: false,
        title: "Warning !!",
        text: "Please set Order Type first.",
        icon: "warning",
      }).then(() => {
        
      });
    }else if (currRow.request_related_to === null && ipdoTypes.includes(currRow.request_type)) {
      this.setState({ showIPDO: true, currRow: currRow, fromPopup:3 });
    }else{

      API.get(`/api/my_team_tasks/get_respond_customer/${currRow.task_id}`).then(
        (res) => {
          //console.log(res.data.data);
          let custResp;
          if (res.data.data.rs_id > 0) {
            custResp = {
              original_comment: htmlDecode(res.data.data.original_comment),
              delivery_date: localDate(res.data.data.expected_closure_date),
              status: res.data.data.status,
              status_id: res.data.data.status_id,
              action_req: res.data.data.required_action,
              action_id: res.data.data.action_id,
              po_number: res.data.data.po_number,
              pause_sla: res.data.data.pause_sla,
              translated_comment:htmlDecode(res.data.data.translated_comment),
              original_language: res.data.data.original_language
            };
          } else {
            custResp = {
              original_comment: "",
              delivery_date: "",
              status: "",
              status_id: 0,
              action_req: "",
              action_id: 0,
              po_number: "",
              pause_sla: 0,
              translated_comment:"",
              original_language: ""
            };
          }
          this.setState({
            showRespondCustomer: true,
            currRow: currRow,
            custResp: custResp,
          });
        }
      );
    }
  };

  showPoke = (currRow) => {
    this.setState({ showPoke: true, currRow: currRow });
  };

  review_task = (currRow) => {
    //console.log('currRow',currRow);
    swal({
      closeOnClickOutside: false,
      title: "Request review",
      text: "Are you sure you want this request to be reviewed ?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.setState({ showModalLoader: true });
        API.post(`/api/my_team_tasks/spoc_approval_request/${currRow.task_id}/${currRow.assignment_id}/${currRow.assigned_by}`)
          .then((res) => {
            swal({
              closeOnClickOutside: false,
              title: "Success",
              text: "Notification sent to Regional Spoc to review Request",
              icon: "success",
            }).then(() => {
              this.props.allTaskDetails();
            });
          })
          .catch((err) => {
            var token_rm = 2;
            showErrorMessageFront(err, token_rm, this.props);
          });
      }
    });
  }

  showReAssignPopup = (currRow) => {
    this.setState({ showReAssign: true, currRow: currRow });
  };

  showRespondAssignPopup = (currRow) => {
    this.setState({ showRespondBack: true, currRow });
  };

  showPokeSPOC = (currRow) => {
    this.setState({ showPokeSPOC: true, currRow: currRow });
  };

  componentDidMount() {
    //console.log(this.props.tableData);
    this.setState({
      tableData: this.props.tableData,
      countAllocatedTasks: this.props.countAllocatedTasks,
      options: {
        clearSearch: true,
        expandBy: "column",
        page: !this.state.start_page ? 1 : this.state.start_page, // which page you want to show as default
        sizePerPageList: [
          {
            text: "10",
            value: 10,
          },
          {
            text: "20",
            value: 20,
          },
          {
            text: "All",
            value: !this.state.tableData ? 1 : this.state.tableData,
          },
        ], // you can change the dropdown list for size per page
        sizePerPage: 10, // which size per page you want to locate as default
        pageStartIndex: 1, // where to start counting the pages
        paginationSize: 3, // the pagination bar size.
        prePage: "‹", // Previous page button text
        nextPage: "›", // Next page button text
        firstPage: "«", // First page button text
        lastPage: "»", // Last page button text
        //paginationShowsTotal: this.renderShowsTotal,  // Accept bool or function
        paginationPosition: "bottom", // default is bottom, top and both is all available
        // hideSizePerPage: true > You can hide the dropdown for sizePerPage
        // alwaysShowAllBtns: true // Always show next and previous button
        // withFirstAndLast: false > Hide the going to First and Last page button
      },
      cellEditProp: {
        mode: "click",
        beforeSaveCell: this.onBeforeSetPriority, // a hook for before saving cell
        afterSaveCell: this.onAfterSetPriority, // a hook for after saving cell
      },
    });
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getMyTasks(pageNumber > 0 ? pageNumber : 1);
  };

  getMyTasks(page = 1) {
    let url;

    if (this.props.graphURL && this.props.graphURL != "") {
      url = `${this.props.graphURL}&page=${page}`;
    } else {
      if (this.props.queryString && this.props.queryString !== "") {
        url = `/api/team/allocated?page=${page}&${this.props.queryString}`;
      } else {
        url = `/api/team/allocated?page=${page}`;
      }
    }
    API.get(url)
      .then((res) => {
        if (this.props.graphURL && this.props.graphURL != "") {
          this.setState({
            tableData: res.data.data.allocated,
            countOpenTasks: res.data.count.count_allocated,
          });
        } else {
          this.setState({
            tableData: res.data.data,
            countAllocatedTasks: res.data.count_all_allocated_tasks,
          });
        }
      })
      .catch((err) => {
        showErrorMessageFront(err, this.props);
      });
  }

  onBeforeSetPriority = (row, cellName, cellValue) => {};

  onAfterSetPriority = (row, cellName, cellValue) => {
    this.setState({ updt_priority: 0 });
  };

  getSubTasks = (row) => {
    return (
      <AllocatedSubTaskTable
        assignId={row.assignment_id}
        postedFor={row.assigned_by}
        tableData={row.sub_tasks}
        teamData={this.props.teamData}
        reloadTaskSubTask={() => this.props.allTaskDetails()}
      />
    );
  };

  checkSubTasks = (row) => {
    //console.log("subtask")

    if (typeof row.sub_tasks !== "undefined" && row.sub_tasks.length > 0) {
      return true;
    } else {
      return false;
    }
  };

  refreshTableStatus = (currRow) => {
    currRow.status = 2;
  };

  handleClose = (closeObj) => {
    console.log(closeObj);
    this.setState(closeObj);
  };

  showNextPopup = (nextPopupObj) => {
    if(nextPopupObj.fromPopup == 1){
      this.setState({ showCreateSubTask: true, currRow: nextPopupObj.currRow,reloadOnClose:true });
    }else if(nextPopupObj.fromPopup == 2){
      var task_date_added = localDate(nextPopupObj.currRow.task_date_added);
  
      var dueDate = localDateTime(nextPopupObj.currRow.original_due_date);
      var today = localDateTime(nextPopupObj.currRow.today_date);
      var timeDiff = dueDate.getTime() - today.getTime();
  
      let current_date  = new Date(nextPopupObj.currRow.today_date);
      let add_date_24 = new Date(new Date(nextPopupObj.currRow.task_date_added).getTime() + 60 * 60 * 24 * 1000);
        
      if(current_date < add_date_24 || timeDiff < 0){
          this.setState({ showClosedComment: true, currRow: nextPopupObj.currRow,posted_for:nextPopupObj.currRow.assigned_by,reloadOnClose:true });
      }else{
          swal({
            closeOnClickOutside: false,
            title: "Close task",
            text: "Are you sure you want to close this task?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          }).then((willDelete) => {
            if (willDelete) {
              this.setState({ showModalLoader: true });
              API.delete(
                `/api/my_team_tasks/check_close/${nextPopupObj.currRow.task_id}/${nextPopupObj.currRow.assigned_by}`
              )
                .then((res) => {
                  if (res.data.has_sub_task === 1) {
                    this.setState({ showModalLoader: false });
                    swal({
                      closeOnClickOutside: false,
                      title: "Alert",
                      text:
                        "This task has some open sub-tasks. \r\n Do you want to close them as well?",
                      icon: "warning",
                      buttons: true,
                      dangerMode: true,
                    }).then((willDelete) => {
                      if (willDelete) {
                        this.setState({ showModalLoader: true });
                        API.delete(
                          `/api/my_team_tasks/close/${nextPopupObj.currRow.task_id}/${nextPopupObj.currRow.assigned_by}`
                        )
                          .then((res) => {
                            this.setState({ showModalLoader: false });
                            swal({
                              closeOnClickOutside: false,
                              title: "Success",
                              text: "Task has been closed!",
                              icon: "success",
                            }).then(() => {
                              this.props.allTaskDetails();
                            });
                          })
                          .catch((err) => {
                            var token_rm = 2;
                            showErrorMessageFront(err, token_rm, this.props);
                          });
                      }
                    });
                  } else {
                    swal({
                      closeOnClickOutside: false,
                      title: "Success",
                      text: "Task has been closed!",
                      icon: "success",
                    }).then(() => {
                      this.props.allTaskDetails();
                    });
                  }
                })
                .catch((err) => {
                  var token_rm = 2;
                  showErrorMessageFront(err, token_rm, this.props);
                });
            }else{
              this.props.allTaskDetails();
            }
          });
      }
    }else if(nextPopupObj.fromPopup == 3){
      API.get(`/api/my_team_tasks/get_respond_customer/${nextPopupObj.currRow.task_id}`).then(
          (res) => {
            //console.log(res.data.data);
            let custResp;
            if (res.data.data.rs_id > 0) {
              custResp = {
                original_comment: htmlDecode(res.data.data.original_comment),
                delivery_date: localDate(res.data.data.expected_closure_date),
                status: res.data.data.status,
                status_id: res.data.data.status_id,
                action_req: res.data.data.required_action,
                action_id: res.data.data.action_id,
                po_number: res.data.data.po_number,
                pause_sla: res.data.data.pause_sla,
                translated_comment:htmlDecode(res.data.data.translated_comment),
                original_language: res.data.data.original_language
              };
            } else {
              custResp = {
                original_comment: "",
                delivery_date: "",
                status: "",
                status_id: 0,
                action_req: "",
                action_id: 0,
                po_number: "",
                pause_sla: 0,
                translated_comment:"",
                original_language: ""
              };
            }
            this.setState({
              showRespondCustomer: true,
              currRow: nextPopupObj.currRow,
              custResp: custResp,
              reloadOnClose:true
            });
          }
      );
    }
  };

  tdClassName = (fieldValue, row) => {
    var dynamicClass = "width-150 increased-column ";
    if (row.vip_customer === 1) {
      dynamicClass += "bookmarked-column ";
    }
    // if(row.sub_tasks.length > 0 && typeof row.sub_tasks !== 'undefined' && typeof row.bookmarked !== 'undefined' ){
    //     return 'sub-task-column bookmarked-column';
    // }else if( typeof row.bookmarked !== 'undefined' ){
    //     return 'bookmarked-column';
    // }else if(row.sub_tasks.length > 0 && typeof row.sub_tasks !== 'undefined'){
    //     return 'sub-task-column';
    // }else{
    //     return '';
    // }
    return dynamicClass;
  };

  trClassName = (row, rowIndex) => {
    var ret = " ";
    var selDueDate = row.due_date;
    var dueDate = localDateTime(selDueDate);
    var today = localDateTime(row.today_date);
    var timeDiff = dueDate.getTime() - today.getTime();
    var addDate = localDateTime(row.date_added);

    if (timeDiff > 0) {
      var parseEighty =
        ((dueDate.getTime() - addDate.getTime()) * 80) / 100 +
        addDate.getTime();
      //console.log(new Date(today.getTime()),new Date(parseEighty));
      if (today.getTime() > parseEighty) {
        ret += " tr-yellow ";
      }
    } else {
      ret += "tr-red";
      /* if (row.vip_customer === 1) {
        ret += "tr-red";
      } */
    }

    if (row.status === 1) {
      ret += " tr-pink ";
    }

    
    // if(row.order_verified_status == 0 && row.request_type == 23){
    //   ret += " tr-blue ";
    // }

    // if(row.rdd_pending && row.request_type == 23){
    //   ret += " tr-yellow ";
    // }

    return ret;
  };

  expandColumnComponent({ isExpandableRow, isExpanded }) {
    let content = "";

    if (isExpandableRow) {
      content = isExpanded ? "-" : "+";
    } else {
      content = " ";
    }
    return <div> {content} </div>;
  }

  render() {
    //console.log('lllll', this.state.countAllocatedTasks);
    /* const paginationOptions = {
      page: 1, // which page you want to show as default
      sizePerPageList: [
        {
          text: "10",
          value: 10
        },
        {
          text: "20",
          value: 20
        },
        {
          text: "All",
          value: this.props.tableData.length > 0 ? this.props.tableData.length : 1
        }
      ], // you can change the dropdown list for size per page
      sizePerPage: 10, // which size per page you want to locate as default
      pageStartIndex: 1, // where to start counting the pages
      paginationSize: 3, // the pagination bar size.
      prePage: '‹', // Previous page button text
      nextPage: '›', // Next page button text
      firstPage: '«', // First page button text
      lastPage: '»', // Last page button text
      //paginationShowsTotal: this.renderShowsTotal, // Accept bool or function
      paginationPosition: "bottom" // default is bottom, top and both is all available
      // hideSizePerPage: true //> You can hide the dropdown for sizePerPage
      // alwaysShowAllBtns: true // Always show next and previous button
      // withFirstAndLast: false //> Hide the going to First and Last page button
    }; */
    return (
      <>
        {this.state.showModalLoader === true ? (
          <div className="loderOuter">
            <div className="loader">
              <img src={loaderlogo} alt="logo" />
              <div className="loading">Loading...</div>
            </div>
          </div>
        ) : (
          ""
        )}
        {this.state.changeData && (
          <BootstrapTable
            data={this.state.tableData}
            /* options={paginationOptions}
            pagination */
            expandableRow={this.checkSubTasks}
            expandComponent={this.getSubTasks}
            expandColumnOptions={{
              expandColumnVisible: true,
              expandColumnComponent: this.expandColumnComponent,
              columnWidth: 25,
            }}
            trClassName={this.trClassName}
            cellEdit={this.state.cellEditProp}
          >
            <TableHeaderColumn
              dataField="task_ref"
              //dataSort={true}
              columnClassName={this.tdClassName}
              editable={false}
              expandable={false}
              dataFormat={clickToShowTasks(this)}
            >
              <LinkWithTooltip
                tooltip={`Tasks `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Tasks{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="request_type"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={setDescription(this)}
            >
              <LinkWithTooltip
                tooltip={`Description `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Description{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="product_name"
              editable={false}
              expandable={false}
              dataFormat={setProductName(this)}
            >
              <LinkWithTooltip
                tooltip={`Product Name `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Product Name{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="display_date_added"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={hoverDate(this)}
            >
              <LinkWithTooltip
                tooltip={`Created `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Created{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            {/* <TableHeaderColumn dataField='rdd' dataSort={ true } dataFormat={ setCreateDate(this) } editable={ false } expandable={ false }>RDD</TableHeaderColumn> */}

            <TableHeaderColumn
              dataField="display_assign_date"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={hoverDate(this)}
            >
              <LinkWithTooltip
                tooltip={`Assigned Date `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Assigned Date{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="display_new_due_date"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={hoverDate(this)}
            >
              <LinkWithTooltip
                tooltip={`Due Date `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Due Date{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            {/* <TableHeaderColumn dataField='assigned_to' dataSort={ true } editable={ false } expandable={ false } >Assigned To</TableHeaderColumn> */}

            <TableHeaderColumn
              dataField="dept_name"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={setAssignedBy(this)}
            >
              <LinkWithTooltip
                tooltip={`Assigned By `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Assigned By{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="dept_name"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={setAssignedTo(this)}
            >
              <LinkWithTooltip
                tooltip={`Assigned To `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Assigned To{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="company_name"
              //dataSort={true}
              expandable={false}
              dataFormat={setCompanyName(this)}
            >
              <LinkWithTooltip
                tooltip={`Customer `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Customer{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="cust_name"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={setCustomerName(this)}
            >
              <LinkWithTooltip
                tooltip={`User `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                User{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="status"
              dataFormat={getStatusColumn(this)}
              expandable={false}
              editable={false}
            >
              <LinkWithTooltip
                tooltip={`Status `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Status{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              isKey
              dataField="task_id"
              dataFormat={getActions(this)}
              expandable={false}
              editable={false}
            />
          </BootstrapTable>
        )}

        <ReAssignAssignedPopup
          showReAssign={this.state.showReAssign}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.allTaskDetails()}
        />

        <RespondCustomerAssignedPopup
          showRespondCustomer={this.state.showRespondCustomer}
          currRow={this.state.currRow}
          custResp={this.state.custResp}
          handleClose={this.handleClose}
          reloadTask={() => this.props.allTaskDetails()}
          reloadOnClose={this.state.reloadOnClose}
        />

        <RespondBackAssignedPopup
          showRespondBack={this.state.showRespondBack}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.allTaskDetails()}
        />

        <Poke
          showPoke={this.state.showPoke}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.allTaskDetails()}
          from={`allocated`}
        />

        <ReclassifyMyTasksPopup
          showReclassifyTasks={this.state.showReclassifyTasks}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          allTaskDetails={(e) => this.props.allTaskDetails()}
        />

        <ClosedCommentMyTask
            showClosedComment={this.state.showClosedComment}
            currRow={this.state.currRow}
            posted_for={this.state.posted_for}
            handleClose={this.handleClose}
            reloadTask={() => this.props.allTaskDetails()}
            reloadOnClose={this.state.reloadOnClose}
        />

        <DeleteTaskPopup
          showDeleteTaskPopup={this.state.showDeleteTaskPopup}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.allTaskDetails()}
          from={`allocated`}
        />

        <IPDOPopup
          showIPDO={this.state.showIPDO}
          fromPopup={this.state.fromPopup}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          showNextPopup={this.showNextPopup}
        />

              <PokeSPOC
                showPokeSPOC={this.state.showPokeSPOC}
                currRow={this.state.currRow}
                handleClose={this.handleClose}
                reloadTask={() => this.props.allTaskDetails()}
              />

        {this.state.countAllocatedTasks > 20 ? (
          <Row>
            <Col md={12}>
              <div className="paginationOuter text-right">
                <Pagination
                  activePage={this.state.activePage}
                  itemsCountPerPage={this.state.itemPerPage}
                  totalItemsCount={this.state.countAllocatedTasks}
                  itemClass="nav-item"
                  linkClass="nav-link"
                  activeClass="active"
                  onChange={this.handlePageChange}
                />
              </div>
            </Col>
          </Row>
        ) : null}
      </>
    );
  }
}

export default MyTeamAllocatedTasksTable;
