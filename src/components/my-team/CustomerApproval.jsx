import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  localDate,
  localDateTime,
  htmlDecode,
} from "../../shared/helper";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import Select from "react-select";
import { Row, Col, Tooltip, OverlayTrigger, Modal } from "react-bootstrap";
import "./Dashboard.css";

import API from "../../shared/axios";
import swal from "sweetalert";

import { Link } from "react-router-dom";
import Pagination from "react-js-pagination";
import ReviewRequestPopup from "../dashboard/ReviewRequestPopup";

import { showErrorMessageFront } from "../../shared/handle_error_front";
import { format } from "path";

import dateFormat from "dateformat";

import ReactHtmlParser from "react-html-parser";

import ellipsisImage from '../../assets/images/ellipsis-icon.svg';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";

const portal_url = `${process.env.REACT_APP_PORTAL_URL}`; // SATYAJIT

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const newInitialValues = {};

const setEmployeeName = (refObj) => (cell, row) => {
  return (
    <LinkWithTooltip
      tooltip={`${row.emp_first_name + " " + row.emp_last_name}`}
      href="#"
      id="tooltip-1"
    >
      {row.emp_first_name + " " + row.emp_last_name}
    </LinkWithTooltip>
  );

};

const setCustomerName = (refObj) => (cell, row) => {
  return (
    <LinkWithTooltip
      tooltip={`${row.first_name + " " + row.last_name}`}
      href="#"
      id="tooltip-1"
    >
      {row.first_name + " " + row.last_name}
    </LinkWithTooltip>
  );

};

const setCompanyName = (refObj) => (cell, row) => {
  return htmlDecode(cell);
};

const hoverDate = (refOBj) => (cell, row) => {
  return (
    <LinkWithTooltip
      tooltip={cell}
      href="#"
      id="tooltip-1"
      clicked={(e) => refOBj.checkHandler(e)}
    >
      {cell}
    </LinkWithTooltip>
  );
};

const setCountry = (refOBj) => (cell, row) => {
  return (
    <LinkWithTooltip
      tooltip={`${ReactHtmlParser(row.country_name)}`}
      href="#"
      id="tooltip-1"
      clicked={(e) => refOBj.checkHandler(e)}
    >
      {ReactHtmlParser(row.country_name)}
    </LinkWithTooltip>
  );
};

const setEmail = (refOBj) => (cell, row) => {
  return (
    <LinkWithTooltip
      tooltip={`${row.email}`}
      href="#"
      id="tooltip-1"
      clicked={(e) => refOBj.checkHandler(e)}
    >
      {row.email}
    </LinkWithTooltip>
  );

};

const getActions = (refOBj) => (cell, row) => {
  return (
    <>
      <div className="actionStyle">
        <div className="btn-group">
          <button type="button" className="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" onClick={(e) => refOBj.handleVerticalMenu(e)} >
            <img src={ellipsisImage} alt="ellipsisImage" />
          </button>
          <ul className="dropdown-menu pull-right" role="menu">
            {/* <li><span onClick={(e) => refOBj.viewCustomer(row.customer_id)} style={{ cursor: 'pointer' }} ><i className="fas fa-eye" />View Customer</span></li> */}
            <li><span onClick={(e) => refOBj.assignCustomer(row.assign_id)} style={{ cursor: 'pointer' }} ><i className="fas fa-check" />Assign</span></li>
            <li><span onClick={(e) => refOBj.reviewCustomer(row.customer_id, row.assign_id)} style={{ cursor: 'pointer' }} ><i className="fas fa-pencil-alt" />Review Request</span></li>
          </ul>
        </div>
      </div>
    </>
  );
};

let time_interval = null;
const interval_time = 20000; // Milliseconds i.e 30 sec ( 30 x 1000 )

class CustomerApproval extends Component {
  state = {
    tableData: [],
    countApprovalCustomer: 0,
    userData: [],
    employeeData: [],
    assignTo: '',
    activePage: 1,
    totalCount: 0,
    itemPerPage: 20,
    showModalLoader: false,
    showModal: false,
    showassignModal: false,
    customer_approval_id: 0,
    team_approval_id: 0,
    showReviewRequestPopup: false
  };

  handleClose = (closeObj) => {
    this.setState({ showModal: false, showassignModal: false, team_approval_id: 0, showReviewRequestPopup: false });
    this.setState(closeObj);
  };

  reloadApprovalTable = () => {
    API.get(`/api/team/get_approval_customer?page=1`)
      .then((res) => {
        this.setState({
          tableData: res.data.data,
          countApprovalCustomer: res.data.count,
        });
        this.props.reloadCountApprovalCustomer(res.data.count)
      });
  };

  checkHandler = (event) => {
    event.preventDefault();
  };

  handleVerticalMenu = (event) => {
    var elems = document.querySelectorAll('.btn-group');
    for (var i = 0; i < elems.length; i++) {
      elems[i].classList.remove('open');
    }

    if (event.target.parentNode.classList.contains("btn-group")) {
      event.target.parentNode.classList.add("open");
    } else if (event.target.parentNode.classList.contains("dropdown-toggle")) {
      event.target.parentNode.parentNode.classList.add("open");
    }

  }


  componentDidMount() {

    this.setState({
      userData: [],
      employeeData: [],
      assignTo: '',
      employee_id: '',
      tableData: this.props.tableData,
      countApprovalCustomer: this.props.countApprovalCustomer,
    });

    time_interval = setInterval(()=>{
      this.getApprovalCustomers(1);
    }, interval_time);
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getApprovalCustomers(pageNumber > 0 ? pageNumber : 1);
  };

  getApprovalCustomers(page = 1) {
    let url = `/api/team/get_approval_customer?page=${page}`;

    API.get(url)
      .then((res) => {
        this.setState({
          tableData: res.data.data,
          countApprovalCustomer: res.data.count,
        });
      })
      .catch((err) => {
        showErrorMessageFront(err, 2, this.props);
      });
  };

  reviewCustomer = (customer_id, team_id) => {
    this.setState({ customer_approval_id: customer_id, team_approval_id: team_id, showReviewRequestPopup: true });
  };

  // viewCustomer = (customer_id) => {

  //   this.setState({ showModalLoader: true });
  //   API.get(`/api/customers/view_customer_approval/${customer_id}`)
  //     .then((res) => {
  //       if (res.data.data) {
  //         //console.log(res.data.data)
  //         this.setState({ userData: res.data.data, showModal: true });
  //       }
  //       this.setState({ showModalLoader: false });
  //     })
  //     .catch((err) => {
  //       showErrorMessageFront(err, 2, this.props);
  //     });

  // };

  // approveCustomer = (assign_id) => {
  //   alert(assign_id)
  //   swal({
  //     closeOnClickOutside: false,
  //     title: "Customer Approval Request",
  //     text: "Are you sure you want to approved this user ?",
  //     icon: "warning",
  //     buttons: true,
  //     dangerMode: true,
  //   }).then((willDelete) => {
  //     if (willDelete) {
  //       this.setState({ showModalLoader: true });
  //       API.post(`/api/employees/approve_customer_approval/`, { assign_id: assign_id })
  //         .then((res) => {
  //           swal({
  //             closeOnClickOutside: false,
  //             title: "Success",
  //             text: "User successfully approved.",
  //             icon: "success",
  //           }).then(() => {
  //             this.setState({ showModalLoader: false });
  //             //this.getApprovalCustomers(1);
  //             window.location.reload();
  //           });
  //         })
  //         .catch((err) => {
  //           showErrorMessageFront(err, 2, this.props);
  //         });
  //     }

  //   });
  // };

  assignCustomer = (assign_id) => {
    this.setState({ showModalLoader: true });
    API.post(`/api/employees/get_approval_assign/`, { assign_id: assign_id,for_teams:1 })
      .then((res) => {
        if (res.data.status) {
          var employee = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            employee.push({
              value: element["employee_id"],
              label: htmlDecode(element["employee_name"]),
            });
          }
          this.setState({ employeeData: employee, assignTo: res.data.assign_to, showassignModal: true });
          newInitialValues.assign_id = assign_id;
        } else {
          swal({
            closeOnClickOutside: false,
            title: "Error",
            text: "Unable to assign to others!!!",
            icon: "danger",
          }).then(() => {
            window.location.reload();
          });
        }
        this.setState({ showModalLoader: false });
      })
      .catch((err) => {
        showErrorMessageFront(err, 2, this.props);
      });
  };

  setEmployee = (event, setFieldValue) => {
    if (event && Object.keys(event).length > 0) {
      setFieldValue("employee_id", event);
      this.setState({ employee_id: event });      
    } else {
      setFieldValue("employee_id", "");
      this.setState({ employee_id: "" });
    }
  };

  handleSubmitCustomerAssign = (values, actions) => {
    console.log("values", values);

    var err_cnt = 0;

    if (values.assign_to == 'employee' && (values.employee_id === undefined || values.employee_id === "")) {
      err_cnt++;
      actions.setErrors({ employee_id: "Please select employee." });
      actions.setSubmitting(false);
      this.setState({ createCustomerLoader: false });
    }

    if (err_cnt == 0) {
      let post_data = {
        assign_id: values.assign_id,
        assign_to: values.assign_to,
      }
      if (values.assign_to == 'employee') {
        post_data.employee_id = values.employee_id.value;
      }
      // console.log(post_data);
      this.setState({ createCustomerLoader: true });
      API.post(`/api/employees/set_approval_assign/`, post_data)
        .then((res) => {
          this.setState({ createCustomerLoader: false, showassignModal: false });
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: `User successfully assigned to ${values.assign_to}.`,
            icon: "success",
          }).then(() => {
            window.location.reload();
          });
        })
        .catch((err) => {
          let erms = {};
          erms.data = err.data.errors;
          showErrorMessageFront(erms, 2, this.props);
        });
    }
  };


  render() {

    const validateAddFlag = Yup.object().shape({
      employee_id: Yup.string().trim().required("Please select employee"),
    });

    return (
      <>
        {this.state.showModalLoader === true ? (
          <div className="loderOuter">
            <div className="loader">
              <img src={loaderlogo} alt="logo" />
              <div className="loading">Loading...</div>
            </div>
          </div>
        ) : (
          ""
        )}

        <Modal
          show={this.state.showassignModal}
          onHide={() => this.handleClose()}
          backdrop="static"
          className="assignModal"
        >
          <Formik
            initialValues={newInitialValues}
            onSubmit={this.handleSubmitCustomerAssign}
          >
            {({
              values,
              errors,
              touched,
              isValid,
              isSubmitting,
              setFieldValue,
              setFieldTouched,
              setErrors,
            }) => {
              return (
                <Form>
                  {this.state.createCustomerLoader && (
                    <div className="loderOuter">
                      <div className="loderOuter">
                        <div className="loader">
                          <img src={loaderlogo} alt="logo" />
                          <div className="loading">Loading...</div>
                        </div>
                      </div>
                    </div>
                  )}
                  <Modal.Header closeButton>
                    <Modal.Title>
                      Assign to Employee
                    </Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <div className="contBox">
                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <label>
                              Assign to Employee{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              name="employee_id"
                              isClearable
                              isSearchable
                              options={this.state.employeeData}
                              isDisabled={this.state.assignTo == 'others' ? false : true}
                              onChange={(e) => {
                                this.setEmployee(e, setFieldValue);
                              }}
                            />
                            {(errors.employee_id || touched.employee_id) ? (
                              <span className="errorMsg">{errors.employee_id}</span>
                            ) : null}
                          </div>
                        </Col>
                        <Col xs={12} sm={12} md={12}>
                          <button
                            className={`btn btn-fill m-r-10`}
                            type="submit"
                            disabled={this.state.assignTo == 'others' ? false : true}
                            onClick={(e) => {
                              setFieldValue("assign_to", "employee");
                            }}
                          >
                            Assign to Selected Employee
                          </button>
                          <button
                            className={`btn btn-fill pink-button`}
                            type="submit"
                            onClick={(e) => {
                              setFieldValue("assign_to", "admin");
                            }}
                            disabled={this.state.employee_id != '' ? true : false}
                          >
                            Assign back to Admin
                          </button>
                        </Col>
                      </Row>
                    </div>
                  </Modal.Body>
                  <Modal.Footer>
                    {/* <button
                      onClick={this.handleClose}
                      className={`btn btn-line`}
                      type="button"
                    >
                      Close
                    </button> */}
                  </Modal.Footer>
                </Form>
              );
            }}
          </Formik>
        </Modal>


        <Modal
          show={this.state.showModal}
          onHide={() => this.handleClose()}
          backdrop="static"
          className="customerDetailsPopup"
        >

          <Modal.Header closeButton>
            <Modal.Title>
              Customer Details
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="contBox">
              <Row>
                <Col xs={12} sm={12} md={12}>
                  <div className="form-group">
                    <table className="table table-bordered">
                      <tr>
                        <td>User Name</td>
                        <td>{this.state.userData.first_name} {this.state.userData.last_name}</td>
                      </tr>
                      <tr>
                        <td>Customer Name</td>
                        <td>{this.state.userData.company_name}</td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td>{this.state.userData.email}</td>
                      </tr>
                      <tr>
                        <td>Phone no</td>
                        <td>{this.state.userData.phone_no}</td>
                      </tr>
                      <tr>
                        <td>Country</td>
                        <td>{this.state.userData.country_name}</td>
                      </tr>
                      <tr>
                        <td>Language</td>
                        <td>{this.state.userData.language}</td>
                      </tr>

                      <tr>
                        <td>Key Account</td>
                        <td>{(this.state.userData.vip_customer == 1) ? 'Yes' : 'No'}</td>
                      </tr>
                      <tr>
                        <td>Status</td>
                        <td>{(this.state.userData.status == 1) ? 'Active' : 'Inactive'}</td>
                      </tr>
                      {this.state.userData.customer_type != '' && this.state.userData.customer_type != null && (
                        <tr>
                          <td>User Type</td>
                          <td>{this.state.userData.customer_type}</td>
                        </tr>
                      )}
                      <tr>
                        <td>Assign Team</td>
                        <td>
                          {this.state.userData.teams && this.state.userData.teams.map((answer, i) => {
                            return (<p>{answer.first_name} {answer.last_name} ({answer.desig_name}) </p>)
                          })}
                        </td>
                      </tr>
                      <tr>
                        <td>Is Admin</td>
                        <td>{(this.state.userData.user_is_admin == 1 && this.state.userData.role_type == 0) ? 'Yes' : 'No'}</td>
                      </tr>
                      <tr>
                        <td>Disable Notifications</td>
                        <td>{(this.state.userData.dnd == 1) ? 'Yes' : 'No'}</td>
                      </tr>
                      <tr>
                        <td>Multi Language Access</td>
                        <td>{(this.state.userData.multi_lang_access == 1) ? 'Yes' : 'No'}</td>
                      </tr>
                      {this.state.userData.role_type != 0 && (
                        <>
                          <tr>
                            <td>Role</td>
                            <td>{this.state.userData.role && this.state.userData.role.map((rl, j) => {
                              return (<p>{rl.role_name} </p>)
                            })}</td>
                          </tr>
                          <tr>
                            <td>Role Type</td>
                            <td>{(this.state.userData.role_type == 1) ? 'Admin' : 'Regular'}</td>
                          </tr>
                        </>
                      )}
                    </table>
                  </div>
                </Col>
              </Row>
            </div>
          </Modal.Body>
          <Modal.Footer>
            {/* <button
              onClick={this.handleClose}
              className={`btn btn-line`}
              type="button"
            >
              Close
            </button> */}
          </Modal.Footer>

        </Modal>
        {
          this.state.showReviewRequestPopup === true &&
          <ReviewRequestPopup
            customer_id={this.state.customer_approval_id}
            assign_id={this.state.team_approval_id}
            showModal={this.state.showReviewRequestPopup}
            handleClose={this.handleClose}
            reloadApprovalTable={this.reloadApprovalTable}
          />
        }

        {this.state.countApprovalCustomer > 0 && (
          <BootstrapTable
            data={this.state.tableData}
            trClassName={this.trClassName}
          >
            <TableHeaderColumn
              dataField="cust_name"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={setCustomerName(this)}
            >
              <LinkWithTooltip
                tooltip={`User `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                User{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="company_name"
              editable={false}
              expandable={false}
              dataFormat={setCompanyName(this)}
            >
              <LinkWithTooltip
                tooltip={`Customer `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Customer{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="product_name"
              editable={false}
              expandable={false}
              dataFormat={setEmail(this)}
            >
              <LinkWithTooltip
                tooltip={`Product Name `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                E-mail{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="country_name"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={setCountry(this)}
            >
              <LinkWithTooltip
                tooltip={`Description `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Country{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="emp_name"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={setEmployeeName(this)}
            >
              <LinkWithTooltip
                tooltip={`Employee `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Employee{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="date_added"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={hoverDate(this)}
            >
              <LinkWithTooltip
                tooltip={`Registration Date`}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Registration Date{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              isKey
              dataField="task_id"
              dataFormat={getActions(this)}
              expandable={false}
              editable={false}
            />
          </BootstrapTable>
        )}



        {this.state.countApprovalCustomer > 20 ? (
          <Row>
            <Col md={12}>
              <div className="paginationOuter text-right">
                <Pagination
                  activePage={this.state.activePage}
                  itemsCountPerPage={this.state.itemPerPage}
                  totalItemsCount={this.state.countApprovalCustomer}
                  itemClass="nav-item"
                  linkClass="nav-link"
                  activeClass="active"
                  hideNavigation="false"
                  onChange={this.handlePageChange}
                />
              </div>
            </Col>
          </Row>
        ) : null}

      </>
    );
  }
}

export default CustomerApproval;
