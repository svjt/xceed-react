import React, { Component } from "react";
import { Row, Col, ButtonToolbar, Button, Modal, Alert } from "react-bootstrap";

//import { FilePond } from 'react-filepond';
//import 'filepond/dist/filepond.min.css';
import Dropzone from "react-dropzone";

import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import dateFormat from "dateformat";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";
import API from "../../shared/axios";
import { showErrorMessageFront } from "../../shared/handle_error_front";

import swal from "sweetalert";

import { localDate, trimString } from "../../shared/helper";

//import uploadAxios from 'axios';

import TinyMCE from "react-tinymce";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";

const initialValues = {
  comment: "",
  reason: null,
};

const removeDropZoneFiles = (fileName, objRef, setErrors) => {
  var newArr = [];
  for (let index = 0; index < objRef.state.files.length; index++) {
    const element = objRef.state.files[index];

    if (fileName === element.name) {
    } else {
      newArr.push(element);
    }
  }

  var fileListHtml = newArr.map((file) => (
    <Alert key={file.name}>
      <span onClick={() => removeDropZoneFiles(file.name, objRef, setErrors)}>
        <i className="far fa-times-circle"></i>
      </span>{" "}
      {file.name}
    </Alert>
  ));
  setErrors({ file_name: "" });
  objRef.setState({
    files: newArr,
    filesHtml: fileListHtml,
  });
};

class DeleteTaskPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPoke: false,
      files: [],
      sla_breach: false,
      showModalLoader: false,
      acceptNewEmp: false,
      acceptNewEmpMsg: "",
      filesHtml: "",
      showDeleteTaskPopup: false,
      reason: null,
      reasonArr: [
        {
          label: "Duplicate Request",
          value: 1,
        },
        {
          label: "Customer Cancelled",
          value: 2,
        },
        {
          label: "Other",
          value: 3,
        },
      ],
    };
  }

  handleChange = (e, field) => {
    this.setState({
      [field]: e.target.value,
    });
  };

  handleCommentChange = (e, setFieldValue) => {
    setFieldValue(e.target.name, e.target.value);
  };

  componentWillReceiveProps = (nextProps) => {
    if (
      nextProps.showDeleteTaskPopup === true &&
      nextProps.currRow.task_id > 0
    ) {
      this.setState({
        showDeleteTaskPopup: nextProps.showDeleteTaskPopup,
        currRow: nextProps.currRow,
      });
    }
  };

  handleClose = () => {
    var close = {
      showModalLoader: false,
      showDeleteTaskPopup: false,
      reason: null,
    };
    this.setState(close);
    this.props.handleClose(close);
  };

  changeReason = (e, setFieldValue, setFieldTouched) => {
    setFieldTouched("comment");
    if (e === null) {
      setFieldValue("reason", null);
      this.setState({ reason: "" });
    } else {
      setFieldValue("reason", e.value);
      this.setState({ reason: e });
    }
  };

  handleDeleteTask = (values, actions) => {
    this.setState({ showModalLoader: true });
    var post_data = {
      reason: values.reason,
      cancel_comment: values.comment,
    };

    if (this.props.from === "allocated") {
      post_data.posted_for = this.state.currRow.assigned_by;
    } else if (this.props.from === "open") {
      post_data.posted_for = this.state.currRow.assigned_to;
    }

    API.put(
      `/api/my_team_tasks/check_delete/${this.state.currRow.task_id}`,
      post_data
    )
      .then((res) => {
        this.handleClose();
        if (res.data.has_sub_task === 1) {
          this.setState({ showModalLoader: false, showDeleteTaskPopup: false });
          swal({
            closeOnClickOutside: false,
            title: "Alert",
            text:
              "This task has some open sub-tasks. \r\n Do you want to cancel them as well?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          }).then((willDelete) => {
            if (willDelete) {
              this.setState({ showModalLoader: true });
              API.put(
                `/api/my_team_tasks/delete/${this.state.currRow.task_id}`,
                post_data
              )
                .then((res) => {
                  this.setState({ showModalLoader: false });
                  swal({
                    closeOnClickOutside: false,
                    title: "Success",
                    text: "Task has been cancelled!",
                    icon: "success",
                  }).then(() => {
                    this.props.reloadTask();
                  });
                })
                .catch((err) => {
                  var token_rm = 2;
                  showErrorMessageFront(err, token_rm, this.props);
                });
            }
          });
        } else {
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Task has been cancelled!",
            icon: "success",
          }).then(() => {
            this.props.reloadTask();
          });
        }
      })
      .catch((err) => {
        var token_rm = 2;
        showErrorMessageFront(err, token_rm, this.props);
      });
  };

  render() {
    const newInitialValues = Object.assign(initialValues, {
      comment: "",
    });

    const validationSchema = Yup.object().shape({
      reason: Yup.string().trim()
        .required("Please select a reason")
        .typeError("Please select a reason"),
      comment: Yup.string().trim().test(
        "testReason",
        "Please enter your comment",
        function (value) {
          return this.parent.reason === "3" && value === undefined
            ? false
            : true;
        }
      ),
    });

    return (
      <>
        <Modal
          show={this.state.showDeleteTaskPopup}
          onHide={() => this.handleClose()}
          backdrop="static"
        >
          <Formik
            initialValues={newInitialValues}
            validationSchema={validationSchema}
            onSubmit={this.handleDeleteTask}
          >
            {({
              values,
              errors,
              touched,
              isValid,
              isSubmitting,
              handleChange,
              setFieldValue,
              setFieldTouched,
              setErrors,
            }) => {
              return (
                <Form encType="multipart/form-data">
                  {this.state.showModalLoader === true ? (
                    <div className="loderOuter">
                      <div className="loader">
                        <img src={loaderlogo} alt="logo" />
                        <div className="loading">Loading...</div>
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                  <Modal.Header closeButton>
                    <Modal.Title>
                      Cancel Request | {this.state.currRow.task_ref}
                    </Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <div className="contBox">
                      <p>Are you sure you want to cancel this request?</p>
                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <label>
                              Select Reason{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              defaultValue={this.state.reason}
                              value={this.state.reason}
                              isClearable={true}
                              isSearchable={true}
                              name="reason"
                              options={this.state.reasonArr}
                              onChange={(e) => {
                                this.changeReason(
                                  e,
                                  setFieldValue,
                                  setFieldTouched
                                );
                              }}
                            />
                            {errors.reason && touched.reason ? (
                              <span className="errorMsg">{errors.reason}</span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <label>
                              Comment{" "}
                              {this.state.reason &&
                              this.state.reason.value === 3 ? (
                                <span className="required-field">*</span>
                              ) : (
                                ""
                              )}
                            </label>
                            <Field
                              name="comment"
                              component="textarea"
                              className="form-control"
                              autoComplete="off"
                              placeholder="Comment here"
                              onChange={(e) =>
                                this.handleCommentChange(e, setFieldValue)
                              }
                              style={{ height: "130px" }}
                            />

                            {errors.comment && touched.comment ? (
                              <span className="errorMsg">{errors.comment}</span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </Modal.Body>
                  <Modal.Footer>
                    <button
                      onClick={this.handleClose}
                      className={`btn-line`}
                      type="button"
                    >
                      No
                    </button>
                    <button
                      className={`btn-fill ${
                        isValid ? "btn-custom-green" : "btn-disable"
                      } m-r-10`}
                      type="submit"
                      disabled={isValid ? false : true}
                    >
                      Yes
                    </button>
                  </Modal.Footer>
                </Form>
              );
            }}
          </Formik>
        </Modal>
      </>
    );
  }
}

export default DeleteTaskPopup;
