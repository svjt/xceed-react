import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  getMyDrupalId,
  localDate,
  localDateTime,
  htmlDecode,
} from "../../shared/helper";
import base64 from "base-64";
import { Row, Col, Tooltip, OverlayTrigger } from "react-bootstrap";
import "./Dashboard.css";

import BMVerticalMenu from "../bmoverview/BMVerticalMenu";
import CSCVerticalMenu from "../cscoverview/CSCVerticalMenu";
import RAVerticalMenu from "../raoverview/RAVerticalMenu";
import OTHRVerticalMenu from "../othroverview/OTHRVerticalMenu";

import StatusColumn from "./StatusColumn";
import SubTaskTable from "./SubTaskTable";

import TranslateCommentPopup from "./TranslateCommentPopup";


//import TaskDetails from './TaskDetails';

import API from "../../shared/axios";
import swal from "sweetalert";

import { Link } from "react-router-dom";
import Pagination from "react-js-pagination";

import { showErrorMessageFront } from "../../shared/handle_error_front";
import { format } from "path";

import dateFormat from "dateformat";

import ReactHtmlParser from "react-html-parser";

//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";

const portal_url = `${process.env.REACT_APP_PORTAL_URL}`; // SATYAJIT

const priority_arr = [
  { priority_id: 1, priority_value: "Low" },
  { priority_id: 2, priority_value: "Medium" },
  { priority_id: 3, priority_value: "High" },
];

const getActions = (refObj) => (cell, row) => {
  if (refObj.props.dashType === "BM") {
    return (
      <BMVerticalMenu
        id={cell}
        currRow={row}
        showApprovalPopup={refObj.showApprovalPopup}
        showTranslateCommentPopup={refObj.showTranslateCommentPopup}
        showApproveTaskPopup={refObj.showApproveTaskPopup}
        showEditTaskPopup={refObj.showEditTaskPopup}
        approvalTab={1}
      />
    );
  } else if (refObj.props.dashType === "CSC") {
    return (
      <CSCVerticalMenu
        id={cell}
        currRow={row}

      />
    );
  } else if (refObj.props.dashType === "RA") {
    return (
      <RAVerticalMenu
        id={cell}
        currRow={row}

      />
    );
  } else if (refObj.props.dashType === "OTHR") {
    return (
      <OTHRVerticalMenu
        id={cell}
        currRow={row}

      />
    );
  }
};

const getStatusColumn = (refObj) => (cell, row) => {
  return <StatusColumn rowData={row} />;
};

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const setDescription = (refOBj) => (cell, row) => {
  if (row.parent_id > 0) {
    let stripHtml = row.title.replace(/<[^>]+>/g, "");

    return (
      <LinkWithTooltip
        tooltip={`${ReactHtmlParser(stripHtml)}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {ReactHtmlParser(stripHtml)}
      </LinkWithTooltip>
    );
  } else {
    let stripHtml = row.req_name.replace(/<[^>]+>/g, "");

    return (
      <LinkWithTooltip
        tooltip={`${ReactHtmlParser(stripHtml)}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {ReactHtmlParser(stripHtml)}
      </LinkWithTooltip>
    );
  }
};

const setLang = (refOBj) => (cell, row) => {
    return (
      <LinkWithTooltip
        tooltip={`${cell}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {cell}
      </LinkWithTooltip>
    );
};

const setRequestedType = (refOBj) => (cell, row) => {

  if(row.review_comment_id > 0){
    return (
      <LinkWithTooltip
        tooltip={`Comment`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        Comment
      </LinkWithTooltip>
    );
  }else{
    return (
      <LinkWithTooltip
        tooltip={`Task`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        Task
      </LinkWithTooltip>
    );
  }

  
};



const setStatus = (refOBj) => (cell, row) => {

  if(cell.comment_status === 1){
    return (
      <LinkWithTooltip
        tooltip={`Approved`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        Approved
      </LinkWithTooltip>
    );
  }else{
    return (
      <LinkWithTooltip
        tooltip={`Yet To Be Approved`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        Yet To Be Approved
      </LinkWithTooltip>
    );
  }
};





const setPostedComment = (refOBj) => (cell, row) => {

    let stripHtml = htmlDecode(row.posted_comment).replace(/<[^>]+>/g, "");

    return (
      <LinkWithTooltip
        tooltip={`${ReactHtmlParser(stripHtml)}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {ReactHtmlParser(stripHtml)}
      </LinkWithTooltip>
    );
};

const setTranslatedComment = (refOBj) => (cell, row) => {
  if(row.translated_comment != ''){
    let stripHtml = htmlDecode(row.translated_comment).replace(/<[^>]+>/g, "");
    return (
      <LinkWithTooltip
        tooltip={`${ReactHtmlParser(stripHtml)}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {ReactHtmlParser(stripHtml)}
      </LinkWithTooltip>
    );
  }
};

const setCreateDate = (refObj) => (cell) => {
  var date = localDate(cell);
  return dateFormat(date, "dd/mm/yyyy");
};

const setDaysPending = (refObj) => (cell, row) => {
  var date = localDate(cell);
  return dateFormat(date, "dd/mm/yyyy");
};

const clickToShowTasks = (refObj) => (cell, row) => {
  
  return (
    <LinkWithTooltip
      tooltip={`${cell}`}
      href="#"
      id="tooltip-1"
      clicked={(e) =>{
          API.put(`/api/tasks/review_highlight/${row.review_id}`,{})
          .then((response) => {
            refObj.redirectUrlTask(e, row.task_id, row.assignment_id, row.posted_for)
          }).catch((err) => {
            console.log(err);
          });
          
        }
      }
    >
      {cell}
    </LinkWithTooltip>
  );
};

const priorityEditor = (refObj) => (onUpdate, props) => {
  ///console.log(props.row.priority);
  //refObj.setState({ updt_priority:props.row.priority,updt_priority_value:props.row.priority_value });
  return (
    <>
      <select
        value={
          refObj.state.updt_priority > 0
            ? refObj.state.updt_priority
            : props.row.priority
        }
        onChange={(ev) => {
          refObj.setState({ updt_priority: ev.currentTarget.value });
        }}
      >
        {priority_arr.map((priority) => (
          <option key={priority.priority_id} value={priority.priority_id}>
            {priority.priority_value}
          </option>
        ))}
      </select>
      <button
        className="btn btn-info btn-xs textarea-save-btn"
        onClick={() => {
          //AXIOS CALL TO UPDATE THE PRIORITY

          props.row.priority = refObj.state.updt_priority;
          onUpdate(refObj.state.updt_priority);
        }}
      >
        save
      </button>
    </>
  );
};

const setProductName = (refOBj) => (cell, row) => {
  if (cell === null) {
    return "";
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${cell}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {cell}
      </LinkWithTooltip>
    );
  }
};

const setCustomerName = (refObj) => (cell, row) => {
  if (row.customer_id > 0) {
    //hard coded customer id - SATYAJIT
    //row.customer_id = 2;
    return (
      /*<Link
        to={{
          pathname:
            portal_url + "customer-dashboard/" +
            row.customer_drupal_id
        }}
        target="_blank"
        style={{ cursor: "pointer" }}
      >*/
      <LinkWithTooltip
        tooltip={`${row.first_name + " " + row.last_name}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refObj.redirectUrl(e, row.customer_id)}
      >
        {row.first_name + " " + row.last_name}
      </LinkWithTooltip>
      /*</Link>*/
    );
  } else {
    return "";
  }
};

const setPriorityName = (refObj) => (cell, row) => {
  var ret = "Set Priority";
  for (let index = 0; index < priority_arr.length; index++) {
    const element = priority_arr[index];
    if (element["priority_id"] === cell) {
      ret = element["priority_value"];
    }
  }

  return ret;
};

const setAddedBy = (refOBj) => (cell, row) => {
  return (
    <LinkWithTooltip
      tooltip={`${
        row.ab_emp_first_name +
        " " +
        row.ab_emp_last_name +
        " (" +
        row.ab_emp_desig_name +
        ")"
      }`}
      href="#"
      id="tooltip-1"
      clicked={(e) => refOBj.checkHandler(e)}
    >
      {row.ab_emp_first_name +
        " " +
        row.ab_emp_last_name +
        " (" +
        row.ab_emp_desig_name +
        ")"}
    </LinkWithTooltip>
  );
};

const setAssignedBy = (refOBj) => (cell, row) => {
  if (row.assigned_by == "-1") {
    return (
      <LinkWithTooltip
        tooltip={`System`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        System
      </LinkWithTooltip>
    );
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${
          row.ab_emp_first_name +
          " " +
          row.ab_emp_last_name +
          " (" +
          row.ab_emp_desig_name +
          ")"
        }`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {row.ab_emp_first_name +
          " " +
          row.ab_emp_last_name +
          " (" +
          row.ab_emp_desig_name +
          ")"}
      </LinkWithTooltip>
    );
  }
};

const hoverDate = (refOBj) => (cell, row) => {
  return (
    <LinkWithTooltip
      tooltip={cell}
      href="#"
      id="tooltip-1"
      clicked={(e) => refOBj.checkHandler(e)}
    >
      {cell}
    </LinkWithTooltip>
  );
};

const setCompanyName = (refObj) => (cell, row) => {
  return htmlDecode(cell);
};

class ApprovalTasksTable extends Component {
  state = {
    showCreateSubTask: false,
    showCloneCreateSubTask: false,

    showIncreaseSla: false,
    showIncreaseSlaSubTask: false,
    showCreateTasks: false,
    showAssign: false,
    showRespondBack: false,
    showTranslateEditComment: false,
    showProformaForm: false,
    showTaskDetails: false,
    showRespondCustomer: false,
    showAuthorizeBack: false,
    changeData: true,
    task_id: 0,
    tableData: [],

    activePage: 1,
    totalCount: 0,
    itemPerPage: 20,
    showModalLoader: false,
    showDeleteTaskPopup: false,
  };

  checkHandler = (event) => {
    event.preventDefault();
  };

  redirectUrl = (event, id) => {
    event.preventDefault();
    //http://reddy.indusnet.cloud/customer-dashboard?source=Mi02NTE=
    API.post(`/api/employees/shlogin`, { customer_id: parseInt(id) })
      .then((res) => {
        if (res.data.shadowToken) {
          var url =
            process.env.REACT_APP_CUSTOMER_PORTAL +
            `setToken/` +
            res.data.shadowToken;
          window.open(url, "_blank");
        }
      })
      .catch((error) => {
        showErrorMessageFront(error, 0, this.props);
      });
  };

  redirectUrlTask = (event, task_id, assignment_id = "",posted_for = "") => {
    event.preventDefault();
    window.open(`/user/team_task_details/${task_id}/${assignment_id}/${posted_for}`, "_blank");
  };

  showEditTaskPopup = (currRow) => {
    window.open(`/user/team_task_details/${currRow.task_id}/${currRow.assignment_id}/${currRow.posted_for}`, "_blank");
  }

  taskDetails = (row) => {
    this.setState({ showTaskDetails: true, currRow: row });
  };

  showSubTaskPopup = (currRow) => {
    //console.log("Create Sub Task");
    if(currRow.po_no === null && currRow.request_type == 23){
      swal({
        closeOnClickOutside: false,
        title: "Warning !!",
        text: "Please set Purchase Order Number first.",
        icon: "warning",
      }).then(() => {
        
      });
    }else{
      this.setState({ showCreateSubTask: true, currRow: currRow });
    }
  };

  showCloneSubTaskPopup = (currRow) => {
    //console.log("Create Sub Task");
    this.setState({ showCloneCreateSubTask: true, currRow: currRow });
  };

  showIncreaseSlaPopup = (currRow) => {
    if (currRow.parent_id > 0) {
      this.setState({ showIncreaseSlaSubTask: true, currRow: currRow });
    } else {
      this.setState({ showIncreaseSla: true, currRow: currRow });
    }
  };

  showDeleteTaskPopup = (currRow) => {
    this.setState({ showDeleteTaskPopup: true, currRow: currRow });
  };

  showApprovalPopup = (currRow) => {
    swal({
      closeOnClickOutside: false,
      title: "Approve request",
      text: "Please note, post approving, the response will be shared to the customer.",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.setState({ showModalLoader: true });
        API.post(`/api/my_team_tasks/approve_comments/${currRow.task_id}/${currRow.discussion_id}/${currRow.review_comment_id}/${currRow.posted_for}`)
          .then((res) => {
            swal({
              closeOnClickOutside: false,
              title: "Success",
              text: "Comment has been approved!",
              icon: "success",
            }).then(() => {
              this.props.allTaskDetails();
            });
          })
          .catch((err) => {
            var token_rm = 2;
            showErrorMessageFront(err, token_rm, this.props);
          });
      }
    });
  };

  showApproveTaskPopup = (currRow) => {
    swal({
      closeOnClickOutside: false,
      title: "Request Confirmation",
      text: "Are you sure you have reviewed the task ?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.setState({ showModalLoader: true });
        API.post(`/api/my_team_tasks/approve_translated_task/${currRow.review_id}/${currRow.posted_for}`)
          .then((res) => {
            swal({
              closeOnClickOutside: false,
              title: "Success",
              text: "Task content has been reviewed!",
              icon: "success",
            }).then(() => {
              this.props.allTaskDetails();
            });
          })
          .catch((err) => {
            var token_rm = 2;
            showErrorMessageFront(err, token_rm, this.props);
          });
      }
    });
  };

  showAuthorizeTaskPopup = (currRow) => {
    //console.log("Authorize");
    this.setState({ showAuthorizeBack: true, currRow: currRow });
  };

  showAssignPopup = (currRow) => {
    //console.log("Assign Task");
    this.setState({ showAssign: true, currRow: currRow });
  };

  showCloneMainTaskPopup = (currRow) => {
    //console.log("Assign Task");
    this.setState({ showCreateTasks: true, currRow: currRow });
  };

  showRespondPopup = (currRow) => {
    //console.log("Respond");
    this.setState({ showRespondBack: true, currRow: currRow });
  };


  showTranslateCommentPopup = (currRow) => {
    //console.log("Respond");
    this.setState({ showTranslateEditComment: true, currRow: currRow });
  };

  showProforma = (currRow) => {
    this.setState({ showProformaForm: true, currRow: currRow });
  };

  showPoke = (currRow) => {
    this.setState({ poke: true, currRow: currRow });
  };

  showRespondCustomerPopup = (currRow) => {
    //console.log("Respond Customer");

    API.get(`/api/tasks/get_respond_customer/${currRow.task_id}`).then(
      (res) => {
        let custResp;
        if (res.data.data.rs_id > 0) {
          custResp = {
            comment: htmlDecode(res.data.data.comment),
            delivery_date: localDate(res.data.data.expected_closure_date),
            status: res.data.data.status,
            action_req: res.data.data.required_action,
            po_number: res.data.data.po_number,
            pause_sla: res.data.data.pause_sla,
          };
        } else {
          custResp = {
            comment: "",
            delivery_date: "",
            status: "",
            action_req: "",
            po_number: "",
            pause_sla: 0,
          };
        }
        this.setState({
          showRespondCustomer: true,
          currRow: currRow,
          custResp: custResp,
        });
      }
    );
  };

  componentDidMount() {
    //console.log('>>>>>',this.props.countMyTasks);
    this.setState({
      tableData: this.props.tableData,
      countMyTasks: this.props.countMyTasks,
      options: {
        clearSearch: true,
        expandBy: "column",
        page: !this.state.start_page ? 1 : this.state.start_page, // which page you want to show as default
        sizePerPageList: [
          {
            text: "10",
            value: 10,
          },
          {
            text: "20",
            value: 20,
          },
          {
            text: "All",
            value: !this.state.tableData ? 1 : this.state.tableData,
          },
        ], // you can change the dropdown list for size per page
        sizePerPage: 10, // which size per page you want to locate as default
        pageStartIndex: 1, // where to start counting the pages
        paginationSize: 3, // the pagination bar size.
        prePage: "‹", // Previous page button text
        nextPage: "›", // Next page button text
        firstPage: "«", // First page button text
        lastPage: "»", // Last page button text
        //paginationShowsTotal: this.renderShowsTotal,  // Accept bool or function
        paginationPosition: "bottom", // default is bottom, top and both is all available
        // hideSizePerPage: true > You can hide the dropdown for sizePerPage
        // alwaysShowAllBtns: true // Always show next and previous button
        // withFirstAndLast: false > Hide the going to First and Last page button
      },
      cellEditProp: {
        mode: "click",
        beforeSaveCell: this.onBeforeSetPriority, // a hook for before saving cell
        afterSaveCell: this.onAfterSetPriority, // a hook for after saving cell
      },
    });
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getMyTasks(pageNumber > 0 ? pageNumber : 1);
  };

  getMyTasks(page = 1) {
    let url;
    if (this.props.queryString !== "") {
      url = `/api/tasks?page=${page}&${this.props.queryString}`;
    } else {
      url = `/api/tasks?page=${page}`;
    }
    API.get(url)
      .then((res) => {
        this.setState({
          tableData: res.data.data,
          countMyTasks: res.data.count_my_tasks,
        });
      })
      .catch((err) => {
        showErrorMessageFront(err, this.props);
      });
  }

  onBeforeSetPriority = (row, cellName, cellValue) => {};

  onAfterSetPriority = (row, cellName, cellValue) => {
    this.setState({ updt_priority: 0 });
  };

  getSubTasks = (row) => {
    return (
      <SubTaskTable
        tableData={row.sub_tasks}
        assignId={row.assignment_id}
        dashType={this.props.dashType}
        reloadTaskSubTask={() => this.props.allTaskDetails()}
      />
    );
  };

  checkSubTasks = (row) => {
    //console.log("subtask")

    if (typeof row.sub_tasks !== "undefined" && row.sub_tasks.length > 0) {
      return true;
    } else {
      return false;
    }
  };

  refreshTableStatus = (currRow) => {
    currRow.status = 2;
  };

  handleClose = (closeObj) => {
    this.setState(closeObj);
  };

  tdClassName = (fieldValue, row) => {
    var dynamicClass = "width-150 increased-column ";
    if (row.vip_customer === 1) {
      dynamicClass += "bookmarked-column ";
    }
    // if(row.sub_tasks.length > 0 && typeof row.sub_tasks !== 'undefined' && typeof row.bookmarked !== 'undefined' ){
    //     return 'sub-task-column bookmarked-column';
    // }else if( typeof row.bookmarked !== 'undefined' ){
    //     return 'bookmarked-column';
    // }else if(row.sub_tasks.length > 0 && typeof row.sub_tasks !== 'undefined'){
    //     return 'sub-task-column';
    // }else{
    //     return '';
    // }
    return dynamicClass;
  };

  trClassName = (row, rowIndex) => {
    var ret = " ";
    // var selDueDate = row.new_due_date;
    // var dueDate = localDateTime(selDueDate);
    // var today = localDateTime(row.today_date);
    // var timeDiff = dueDate.getTime() - today.getTime();
    // var addDate = localDateTime(row.date_added);

    // if (timeDiff > 0) {
    //   var parseEighty =
    //     ((dueDate.getTime() - addDate.getTime()) * 80) / 100 +
    //     addDate.getTime();
    //   if (today.getTime() > parseEighty) {
    //     ret += " tr-yellow ";
    //   }
    // } else {
    //   ret += "tr-red";
    //   /* if (row.vip_customer === 1) {
    //     ret += "tr-red";
    //   } */
    // }

    if (row.highlight === 1) {
      ret += " tr-pink ";
    }

    return ret;
  };

  expandColumnComponent({ isExpandableRow, isExpanded }) {
    let content = "";

    if (isExpandableRow) {
      content = isExpanded ? "-" : "+";
    } else {
      content = " ";
    }
    return <div> {content} </div>;
  }

  render() {
    /* const paginationOptions = {
      page: 1, // which page you want to show as default
      sizePerPageList: [
        {
          text: "10",
          value: 10
        },
        {
          text: "20",
          value: 20
        },
        {
          text: "All",
          value: this.props.tableData.length > 0 ? this.props.tableData.length : 1
        }
      ], // you can change the dropdown list for size per page
      sizePerPage: 10, // which size per page you want to locate as default
      pageStartIndex: 1, // where to start counting the pages
      paginationSize: 3, // the pagination bar size.
      prePage: '‹', // Previous page button text
      nextPage: '›', // Next page button text
      firstPage: '«', // First page button text
      lastPage: '»', // Last page button text
      //paginationShowsTotal: this.renderShowsTotal, // Accept bool or function
      paginationPosition: "bottom" // default is bottom, top and both is all available
      // hideSizePerPage: true //> You can hide the dropdown for sizePerPage
      // alwaysShowAllBtns: true // Always show next and previous button
      // withFirstAndLast: false //> Hide the going to First and Last page button
    }; */
    return (
      <>
        {this.state.showModalLoader === true ? (
          <div className="loderOuter">
            <div className="loader">
              <img src={loaderlogo} alt="logo" />
              <div className="loading">Loading...</div>
            </div>
          </div>
        ) : (
          ""
        )}

        {this.state.changeData && (
          <BootstrapTable
            data={this.state.tableData}
            /* options={paginationOptions}
            pagination */
            expandableRow={this.checkSubTasks}
            expandComponent={this.getSubTasks}
            expandColumnOptions={{
              expandColumnVisible: true,
              expandColumnComponent: this.expandColumnComponent,
              columnWidth: 25,
            }}
            trClassName={this.trClassName}
            cellEdit={this.state.cellEditProp}
          >
            <TableHeaderColumn
              dataField="task_ref"
              //dataSort={true}
              columnClassName={this.tdClassName}
              editable={false}
              expandable={false}
              dataFormat={clickToShowTasks(this)}
            >
              <LinkWithTooltip
                tooltip={`Tasks `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Tasks{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>
            
            <TableHeaderColumn
              dataField="request_type"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={setDescription(this)}
            >
              <LinkWithTooltip
                tooltip={`Description `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Description{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="product_name"
              editable={false}
              expandable={false}
              dataFormat={setProductName(this)}
            >
              <LinkWithTooltip
                tooltip={`Product Name `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Product Name{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="ab_emp_first_name"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={setAddedBy(this)}
            >
              <LinkWithTooltip
                tooltip={`Requested By `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Requested By{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="comment_date_added"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={hoverDate(this)}
            >
              <LinkWithTooltip
                tooltip={`Requested Date `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Requested Date{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="task_date_added"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={hoverDate(this)}
            >
              <LinkWithTooltip
                tooltip={`Created Date `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Created Date{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="company_name"
              editable={false}
              expandable={false}
              dataFormat={setCompanyName(this)}
            >
              <LinkWithTooltip
                tooltip={`Customer `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Customer{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="cust_name"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={setCustomerName(this)}
            >
              <LinkWithTooltip
                tooltip={`User `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                User{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="language_text"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={setRequestedType(this)}
            >
              <LinkWithTooltip
                tooltip={`Review Type `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Review Type{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>
            

            <TableHeaderColumn
              dataField="language_text"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={setLang(this)}
            >
              <LinkWithTooltip
                tooltip={`Requested Language `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Requested Language{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            {/* <TableHeaderColumn
              dataField="comment_status"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={setStatus(this)}
            >
              <LinkWithTooltip
                tooltip={`Requested STATUS `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Status{" "}
              </LinkWithTooltip>
            </TableHeaderColumn> */}

            <TableHeaderColumn
              isKey
              dataField="task_id"
              dataFormat={getActions(this)}
              expandable={false}
              editable={false}
            />
          </BootstrapTable>
        )}

        {/*DONE*/}
        <TranslateCommentPopup
          showTranslateEditComment={this.state.showTranslateEditComment}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.allTaskDetails()}
          posted_for={0}
        />

        {this.state.countMyTasks > 20 ? (
          <Row>
            <Col md={12}>
              <div className="paginationOuter text-right">
                <Pagination
                  activePage={this.state.activePage}
                  itemsCountPerPage={this.state.itemPerPage}
                  totalItemsCount={this.state.countMyTasks}
                  itemClass="nav-item"
                  linkClass="nav-link"
                  activeClass="active"
                  hideNavigation="false"
                  onChange={this.handlePageChange}
                />
              </div>
            </Col>
          </Row>
        ) : null}

        {/*DONE*/}
        {/* <TaskDetails showTaskDetails={this.state.showTaskDetails} currRow={this.state.currRow} handleClose={this.handleClose} refreshTableStatus={this.refreshTableStatus} /> */}
      </>
    );
  }
}

export default ApprovalTasksTable;
