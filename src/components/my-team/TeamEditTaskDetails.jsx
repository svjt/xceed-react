import React, { lazy, Component } from "react";
import {
  Row,
  Col,
  Button,
  Panel,
  PanelGroup,
  Alert,
  Tooltip,
  OverlayTrigger,
  Modal,
  ButtonToolbar,
} from "react-bootstrap";
import dateFormat from "dateformat";

import API from "../../shared/axios";

import { Redirect, Link } from "react-router-dom";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import Loader from "react-loader-spinner";
import DatePicker from "react-datepicker";
import swal from "sweetalert";
import Select from "react-select";

import { Editor } from "@tinymce/tinymce-react";
import ReactHtmlParser from "react-html-parser";

import { htmlDecode } from "../../shared/helper";
import { showErrorMessageFront } from "../../shared/handle_error_front";

import mapValues from "lodash/mapValues";
import { localDate, localDateOnly } from "../../shared/helper";

//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
import exclamationImage from "../../assets/images/exclamation-icon-black.svg";

const initialValues = {
  country_id: null,
  product_id: 0,
  po_delivery_date:'',
  content: "",
  pharmacopoeia: "",
  polymorphic_form: "",
  nature_of_issue: "",
  batch_number: "",
  quantity: "",
  units: "",
  rdd: "",
  stability_data_type: "",
  audit_visit_site_name: "",
  request_audit_visit_date: "",

  service_request_type: {
    field_sample: "Samples",
    field_working: "Working Standards",
    field_impurites: "Impurities",
  },

  sampleBatchNo: 0,
  sampleQuantity: 0,
  sampleUnit: "",

  workingQuantity: 0,
  workingUnit: "",

  impureRequired: "",
  impureQuantity: 0,
  impureUnit: "",

  shipping_address: "",

  gmp_clearance_id: "",
  tga_email_id: "",
  applicant_name: "",
  doc_required: "",

  samplesCheck: false,
  workingCheck: false,
  impureCheck: false,
  share_with_agent: 0
};

/*For Tooltip*/
function LinkWithTooltipText({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

var minDate = "";
var maxDate = "";

const modifyArr = (arr) => {
  for (let index = 0; index < arr.length; index++) {
    if(arr[index].rdd != null){
      arr[index].rdd = dateFormat(arr[index].rdd, "yyyy-mm-dd");
    }
    arr[index].material_id_auto_suggest = '';

  }
  return arr;
}

class TeamEditTaskDetails extends Component {
  constructor(props) {
    super(props);

    //const {changeStatus} = this.props.location.state;

    this.state = {
      taskDetails: [],
      country: [],
      product: [],
      ccCust: [],
      ccSelCust: [],
      task_id: this.props.match.params.id,
      assign_id: this.props.match.params.assign_id,
      posted_for: this.props.match.params.posted_for,

      service_request_type: {
        check_sample: false,
        check_working: false,
        check_impurities: false,
      },

      sampleBatchNo: 0,
      sampleQuantity: 0,
      sampleUnit: "",

      workingQuantity: 0,
      workingUnit: "",

      impureRequired: "",
      impureQuantity: 0,
      impureUnit: "",

      shipping_address: "",
      editLoader: true,
      updateTaskLoader: false,
      selProducts: null,
      SWIhtml: "",
      not_approved_tasks:[],
      suggestions:[],
      product_code_auto_suggest:""
    };
  }

  toggleCheckbox = (e, setFieldValue) => {
    if (e.target.value === "Samples") {
      this.setState({
        service_request_type: {
          check_sample: !this.state.service_request_type.check_sample,
          check_working: this.state.service_request_type.check_working,
          check_impurities: this.state.service_request_type.check_impurities,
        },
      });
    }

    if (e.target.value === "Working Standards") {
      this.setState({
        service_request_type: {
          check_sample: this.state.service_request_type.check_sample,
          check_working: !this.state.service_request_type.check_working,
          check_impurities: this.state.service_request_type.check_impurities,
        },
      });
    }

    if (e.target.value === "Impurities") {
      this.setState({
        service_request_type: {
          check_sample: this.state.service_request_type.check_sample,
          check_working: this.state.service_request_type.check_working,
          check_impurities: !this.state.service_request_type.check_impurities,
        },
      });
    }

    //console.log('====',this.state.service_request_type)
  };

  fetchTaskList = (id, assign_id) =>
    API.get(`/api/my_team_tasks/${id}/${assign_id}`);

  fetchCountryList = () => API.get(`/api/feed/country`);

  fetchProductList = () => API.get(`/api/feed/products`);

  componentDidMount = () => {
    if (this.state.task_id > 0) {
      API.get(`/api/feed/country`)
        .then((res) => {
          var country = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            country.push({
              value: element["country_id"],
              label: htmlDecode(element["country_name"]),
            });
          }

          this.setState({ country: country });

          API.get(`/api/feed/products`)
            .then((res) => {
              this.setState({ product: res.data.data });

              API.get(
                `/api/my_team_tasks/${this.state.task_id}/${this.state.assign_id}/${this.state.posted_for}`
              )
                .then((res) => {
                  var ccTeam = [];
                  var ccSelTeam = [];

                  for (
                    let index = 0;
                    index < res.data.data.ccCutomerList.length;
                    index++
                  ) {
                    const element = res.data.data.ccCutomerList[index];
                    ccTeam.push({
                      value: element["customer_id"],
                      label:
                        element["first_name"] +
                        " " +
                        element["last_name"] +
                        " (" +
                        element["company_name"] +
                        ")",
                    });
                  }

                  for (
                    let index = 0;
                    index < res.data.data.selCCCustList.length;
                    index++
                  ) {
                    const element = res.data.data.selCCCustList[index];
                    ccSelTeam.push({
                      value: element["customer_id"],
                      label:
                        element["first_name"] +
                        " " +
                        element["last_name"] +
                        " (" +
                        element["company_name"] +
                        ")",
                    });
                  }
                  // console.log("task details", res.data.data.taskDetails);

                  
                  if(res.data.data.taskDetails.request_type == 23 && res.data.data.taskDetails.order_verified_status == 0){
                    let prev_state = [];
                    API.get(`/api/tasks/temp_task_orders/${this.state.task_id}`).then((res) => {

                      for (let index = 0; index < res.data.data.length; index++) {

                        let quant = res.data.data[index].quantity.split(" ");
                        prev_state.push({
                          product_id: res.data.data[index].product_id,
                          temp_id:res.data.data[index].temp_id,
                          material_id:"",
                          material_id_auto_suggest:"",
                          quantity: quant[0],
                          units: quant[1],
                          rdd:res.data.data[index].edit_rdd,
                          common_err: "",
                        }); 
                      }

                      this.setState({ not_approved_tasks: prev_state });
                    })
                    .catch(err => {
          
                      console.log(err);
                    });
                  }

                  this.setState({
                    ccCust: ccTeam,
                    ccSelCust: ccSelTeam,
                    taskDetails: res.data.data.taskDetails,
                  });

                  var quantity = [];

                  if (
                    this.state.taskDetails.request_type === 7 ||
                    this.state.taskDetails.request_type === 34 ||
                    this.state.taskDetails.request_type === 23 ||
                    this.state.taskDetails.request_type === 41 ||
                    this.state.taskDetails.request_type === 31 ||
                    this.state.taskDetails.request_type === 44
                  ) {
                    if(this.state.taskDetails.language != 'en'){
                      quantity = this.state.taskDetails.quantity_edit.split(" ");
                    }else{
                      quantity = this.state.taskDetails.quantity.split(" ");
                    }
                  }
                  initialValues.share_with_agent = this.state.taskDetails.share_with_agent;
                  initialValues.ccCustId = this.state.ccSelCust;
                  initialValues.country_id = this.state.taskDetails.country_id;
                  initialValues.product_id = this.state.taskDetails.product_id;
                  initialValues.content = htmlDecode(
                    this.state.taskDetails.content
                  );
                  initialValues.pharmacopoeia = htmlDecode(
                    this.state.taskDetails.pharmacopoeia
                  );

                  initialValues.polymorphic_form = htmlDecode(
                    this.state.taskDetails.polymorphic_form
                  );
                  initialValues.nature_of_issue = htmlDecode(
                    this.state.taskDetails.nature_of_issue
                  );
                  initialValues.batch_number = this.state.taskDetails.batch_number;
                  initialValues.dmf_number = htmlDecode(
                    this.state.taskDetails.dmf_number
                  );

                  var apos_document_type = ''

                  if(this.state.taskDetails.language != 'en'){
                    apos_document_type = this.state.taskDetails.apos_document_type_edit;
                  }else{
                    apos_document_type = this.state.taskDetails.apos_document_type;
                  }

                  initialValues.apos_document_type = this.setAposDocumentType(apos_document_type);

                  initialValues.notification_number = htmlDecode(
                    this.state.taskDetails.notification_number
                  );
                  initialValues.rdfrc =
                    this.state.taskDetails.edit_rdfrc !== null
                      ? new Date(this.state.taskDetails.edit_rdfrc)
                      : "";

                  initialValues.quantity =
                    quantity.length > 0 ? quantity[0] : 0;
                  initialValues.units = quantity.length > 0 ? quantity[1] : "";
                  initialValues.rdd = this.state.taskDetails.rdd !== null
                  ? new Date(this.state.taskDetails.rdd)
                  : "";
                  initialValues.product_code = this.state.taskDetails.product_code !== ''? this.state.taskDetails.product_code: "";
                  initialValues.po_number = this.state.taskDetails.po_no !== null ? this.state.taskDetails.po_no : "";

                  if(this.state.taskDetails.language != 'en'){
                    initialValues.stability_data_type = this.state.taskDetails.stability_data_type_edit;
                  }else{
                    initialValues.stability_data_type = this.state.taskDetails.stability_data_type;
                  }

                  initialValues.audit_visit_site_name = htmlDecode(
                    this.state.taskDetails.audit_visit_site_name
                  );
                  initialValues.request_audit_visit_date =
                    this.state.taskDetails.request_audit_visit_date !== null
                      ? localDateOnly(
                          this.state.taskDetails.request_audit_visit_date
                        )
                      : "";

                  // for samples only
                  if (this.state.taskDetails.request_type === 1) {
                    let requestType = this.state.taskDetails.service_request_type.split(
                      ","
                    );

                    let sampleSplit = [];
                    let workingSplit = [];
                    let impureSplit = [];

                    if (this.state.taskDetails.quantity) {
                      if(this.state.taskDetails.language != 'en'){
                        sampleSplit = this.state.taskDetails.quantity_edit.split(" ");
                      }else{
                        sampleSplit = this.state.taskDetails.quantity.split(" ");
                      }
                    }
                    if (this.state.taskDetails.working_quantity) {
                      if(this.state.taskDetails.language != 'en'){
                        workingSplit = this.state.taskDetails.working_quantity_edit.split(" ");
                      }else{
                        workingSplit = this.state.taskDetails.working_quantity.split(" ");
                      }
                    }
                    if (this.state.taskDetails.impurities_quantity) {
                      if(this.state.taskDetails.language != 'en'){
                        impureSplit = this.state.taskDetails.impurities_quantity_edit.split(" ");
                      }else{
                        impureSplit = this.state.taskDetails.impurities_quantity.split(" ");
                      }
                    }

                    this.setState({
                      service_request_type: {
                        check_sample:
                          requestType.indexOf("Samples") === -1 ? false : true,
                        check_working:
                          requestType.indexOf("Working Standards") === -1
                            ? false
                            : true,
                        check_impurities:
                          requestType.indexOf("Impurities") === -1
                            ? false
                            : true,
                      },

                      sampleBatchNo:
                        requestType.indexOf("Samples") === -1
                          ? 0
                          : this.state.taskDetails.number_of_batches,
                      sampleQuantity:
                        sampleSplit.length > 0 ? sampleSplit[0] : "",
                      sampleUnit: sampleSplit.length > 0 ? sampleSplit[1] : "",

                      workingQuantity:
                        workingSplit.length > 0 ? workingSplit[0] : "",
                      workingUnit:
                        workingSplit.length > 0 ? workingSplit[1] : "",

                      impureRequired:
                        impureSplit.length > 0
                          ? htmlDecode(
                              this.state.taskDetails.specify_impurity_required
                            )
                          : "",
                      impureQuantity:
                        impureSplit.length > 0 ? impureSplit[0] : "",
                      impureUnit: impureSplit.length > 0 ? impureSplit[1] : "",
                      shipping_address: htmlDecode(
                        this.state.taskDetails.shipping_address
                      ),
                    });

                    //update initial values
                    initialValues.sampleBatchNo = this.state.sampleBatchNo;
                    initialValues.sampleQuantity = this.state.sampleQuantity;
                    initialValues.sampleUnit = this.state.sampleUnit;
                    initialValues.workingQuantity = this.state.workingQuantity;
                    initialValues.workingUnit = this.state.workingUnit;
                    initialValues.impureRequired = this.state.impureRequired;
                    initialValues.impureQuantity = this.state.impureQuantity;
                    initialValues.impureUnit = this.state.impureUnit;
                    initialValues.shipping_address = this.state.shipping_address;
                    //end here
                  }
                  if (this.state.taskDetails.request_type === 40) {
                    //update initial values
                    initialValues.gmp_clearance_id = this.state.taskDetails.gmp_clearance_id;
                    initialValues.tga_email_id = this.state.taskDetails.tga_email_id;
                    initialValues.applicant_name = this.state.taskDetails.applicant_name;
                    initialValues.doc_required = this.state.taskDetails.doc_required;
                  }

                  this.setState({
                    editLoader: false,
                  });
                })
                .catch((err) => {});
            })
            .catch((err) => {});
        })
        .catch((err) => {});

      // Promise.all([this.fetchTaskList(this.state.task_id, this.state.assign_id), this.fetchCountryList(), this.fetchProductList()])
      // .then((data) => {

      //     if (data[0].status === 200) {

      //         this.setState({
      //             taskDetails: data[0].data.data.taskDetails
      //         });

      //         var quantity = [];

      //         if (this.state.taskDetails.request_type === 7 || this.state.taskDetails.request_type === 23) {
      //             quantity = this.state.taskDetails.quantity.split(" ");
      //         }

      //         //console.log("quantity", quantity)

      //         initialValues.country_id = this.state.taskDetails.country_id
      //         initialValues.product_id = this.state.taskDetails.product_id
      //         initialValues.content = htmlDecode(this.state.taskDetails.content)
      //         initialValues.pharmacopoeia = htmlDecode(this.state.taskDetails.pharmacopoeia)
      //         initialValues.polymorphic_form = htmlDecode(this.state.taskDetails.polymorphic_form)
      //         initialValues.nature_of_issue = htmlDecode(this.state.taskDetails.nature_of_issue)
      //         initialValues.batch_number = this.state.taskDetails.batch_number

      //         initialValues.quantity = (quantity.length > 0) ? quantity[0] : 0
      //         initialValues.units = (quantity.length > 0) ? quantity[1] : ''
      //         initialValues.rdd = (this.state.taskDetails.rdd !== null) ? localDateOnly(this.state.taskDetails.rdd) : ''

      //         initialValues.stability_data_type = this.state.taskDetails.stability_data_type
      //         initialValues.audit_visit_site_name = htmlDecode(this.state.taskDetails.audit_visit_site_name)
      //         initialValues.request_audit_visit_date = localDateOnly(this.state.taskDetails.request_audit_visit_date)

      //         // for samples only
      //         if (this.state.taskDetails.request_type === 1) {

      //             let requestType = this.state.taskDetails.service_request_type.split(",");

      //             let sampleSplit = [];
      //             let workingSplit = [];
      //             let impureSplit = [];

      //             if (this.state.taskDetails.quantity) {
      //                 sampleSplit = this.state.taskDetails.quantity.split(" ");
      //             }
      //             if (this.state.taskDetails.working_quantity) {
      //                 workingSplit = this.state.taskDetails.working_quantity.split(" ");
      //             }
      //             if (this.state.taskDetails.impurities_quantity) {
      //                 impureSplit = this.state.taskDetails.impurities_quantity.split(" ");
      //             }

      //             this.setState({
      //                 service_request_type: {
      //                     check_sample: (requestType.indexOf('Samples') === -1) ? false : true,
      //                     check_working: (requestType.indexOf('Working Standards') === -1) ? false : true,
      //                     check_impurities: (requestType.indexOf('Impurities') === -1) ? false : true
      //                 },

      //                 sampleBatchNo: (requestType.indexOf('Samples') === -1) ? 0 : this.state.taskDetails.number_of_batches,
      //                 sampleQuantity: (sampleSplit.length > 0) ? sampleSplit[0] : '',
      //                 sampleUnit: (sampleSplit.length > 0) ? sampleSplit[1] : '',

      //                 workingQuantity: (workingSplit.length > 0) ? workingSplit[0] : '',
      //                 workingUnit: (workingSplit.length > 0) ? workingSplit[1] : '',

      //                 impureRequired: (impureSplit.length > 0) ? htmlDecode(this.state.taskDetails.specify_impurity_required) : '',
      //                 impureQuantity: (impureSplit.length > 0) ? impureSplit[0] : '',
      //                 impureUnit: (impureSplit.length > 0) ? impureSplit[1] : '',
      //                 shipping_address: htmlDecode(this.state.taskDetails.shipping_address)
      //             });

      //             //update initial values
      //             initialValues.sampleBatchNo = this.state.sampleBatchNo
      //             initialValues.sampleQuantity = this.state.sampleQuantity
      //             initialValues.sampleUnit = this.state.sampleUnit
      //             initialValues.workingQuantity = this.state.workingQuantity
      //             initialValues.workingUnit = this.state.workingUnit
      //             initialValues.impureRequired = this.state.impureRequired
      //             initialValues.impureQuantity = this.state.impureQuantity
      //             initialValues.impureUnit = this.state.impureUnit
      //             initialValues.shipping_address = this.state.shipping_address
      //             //end here

      //             //console.log('>>>', sampleSplit.length);
      //         }
      //     }

      //     if (data[1].status === 200) {
      //         this.setState({
      //             country: data[1].data.data
      //         });
      //     }

      //     if (data[2].status === 200) {
      //         this.setState({
      //             product: data[2].data.data
      //         });
      //     }
      // })
      // .catch((err) => {
      //     console.log(err)
      // });
    }
  };

  closeBrowserWindow = () => {
    window.close();
  };

  checkPharmacopoeia(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (
        (request_type === 35 ||
          request_type === 36 ||
          request_type === 37 ||
          request_type === 22 ||
          request_type === 21 ||
          request_type === 19 ||
          request_type === 18 ||
          request_type === 17 ||
          request_type === 2 ||
          request_type === 3 ||
          request_type === 4 ||
          request_type === 10 ||
          request_type === 12 ||
          request_type === 1 ||
          request_type === 30 ||
          request_type === 32 ||
          request_type === 41 ||
          request_type === 33 ||
          request_type === 42 ||
          request_type === 44) &&
        parent_id === 0
      ) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkTGA = (request_type, parent_id) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 40 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkDMFNumber = (request_type, parent_id) => {
    if (request_type === null) {
      return false;
    } else {
      if ((request_type === 27 || request_type === 39) && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkNotificationNumber = (request_type, parent_id) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 29 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkAposDocumentType = (request_type, parent_id) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 38 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkRDORC = (request_type, parent_id) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 28 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkPolymorphicForm(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if ((request_type === 2 || request_type === 3) && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkStabilityDataType(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 13 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkAuditSiteName(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 9 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkAuditVistDate(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 9 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkQuantity(request_type, parent_id,order_verified_status) {
    if (request_type === null) {
      return false;
    } else {
      if (
        (request_type === 7 ||
          request_type === 34 ||
          (request_type === 23 && order_verified_status == 1) ||
          request_type === 31 ||
          request_type === 41 ||
          request_type === 44) &&
        parent_id === 0
      ) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkBatchNo(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if ((request_type === 7 || request_type === 34) && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkNatureOfIssue(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if ((request_type === 7 || request_type === 34) && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkRDD(request_type, parent_id,order_verified_status) {
    if (request_type === null) {
      return false;
    } else {
      if (
        ((request_type === 23 && order_verified_status === 1) ||
          request_type === 39 ||
          request_type === 40 ||
          request_type === 41) &&
        parent_id === 0
      ) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkProductCode(request_type, parent_id,order_verified_status) {
    if (request_type === null) {
      return false;
    } else {
      if ((request_type === 23 && order_verified_status === 1) && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  setMaterialID = (event, setFieldValue) => {
    setFieldValue("product_code", event.target.value.toString());


    if(event.target.value && event.target.value.length > 2){
      API.post(`/api/feed/get_sap_Product_code`,{material_id:event.target.value})
      .then((res) => {
        
        let ret_html = res.data.data.map((val,index)=>{
          return (<li style={{cursor:'pointer',marginTop:'6px'}} onClick={()=>this.setMaterialIDAutoSuggest(val.value,setFieldValue)} >{val.label}<hr style={{marginTop:'6px',marginBottom:'0px'}} /></li>)
        })
        this.setState({product_code_auto_suggest:ret_html});
      })
      .catch((err) => {
        console.log(err);
      });
    }
  };

  setMaterialIDAutoSuggest = (event, setFieldValue) => {
    setFieldValue("product_code", event.toString());
    this.setState({product_code_auto_suggest:'' });
  };

  checkPODeliveryDate(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (
        request_type === 23 &&
        parent_id === 0
      ) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkPoNumber = (request_type, parent_id) => {
    if (request_type === null) {
      return false;
    } else {
      if (
        request_type === 23  &&
        parent_id === 0
      ) {
        return true;
      } else {
        return false;
      }
    }
  };

  setDateFormat(date_value) {
    var mydate = localDate(date_value);
    return dateFormat(mydate, "dd/mm/yyyy");
  }

  checkSampleRequest(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 1 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkProductRequest = (request_type, parent_id,order_verified_status) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type !== 24 && parent_id === 0) {
        if(request_type == 23 && order_verified_status == 0){
          return false;
        }else{
          return true;
        }
      } else {
        return false;
      }
    }
  };

  checkMarketRequest = (request_type, parent_id) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type !== 24 && request_type !== 40 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  };

  submitEditTask = (values, actions) => {
    this.setState({ updateTaskLoader: true });
    var err_cnt = 0;
    let req_type = this.state.taskDetails.request_type,
      pid = this.state.taskDetails.parent_id,
      order_verified_status = this.state.taskDetails.order_verified_status,
      endpoint = "",
      postObject = {};

    if (req_type === "27") {
      if (values.product_id === undefined || values.product_id === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ updateTaskLoader: false });
      }
    } else if (req_type === "28") {
      if (values.product_id === undefined || values.product_id === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ updateTaskLoader: false });
      } else if (values.country_id === null || values.country_id === "") {
        err_cnt++;
        actions.setErrors({ country_id: "Please select atleast one country" });
        actions.setSubmitting(false);
        this.setState({ updateTaskLoader: false });
      }
    } else if (req_type === "29") {
      if (values.product_id === undefined || values.product_id === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ updateTaskLoader: false });
      } else if (values.country_id === null || values.country_id === "") {
        err_cnt++;
        actions.setErrors({ country_id: "Please select atleast one country" });
        actions.setSubmitting(false);
        this.setState({ updateTaskLoader: false });
      }
    } else if (req_type === "30") {
      if (values.product_id === undefined || values.product_id === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ updateTaskLoader: false });
      }
    } else if (req_type === "31") {
      let numbers = /^(\d*\.)?\d+$/;
      if (values.product_id === undefined || values.product_id === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ updateTaskLoader: false });
      } else if (values.country_id === null || values.country_id === "") {
        err_cnt++;
        actions.setErrors({ country_id: "Please select atleast one country" });
        actions.setSubmitting(false);
        this.setState({ updateTaskLoader: false });
      } else if (
        values.quantity === undefined ||
        values.quantity.trim() === ""
      ) {
        err_cnt++;
        actions.setErrors({ quantity: "Please enter quantity" });
        actions.setSubmitting(false);
        this.setState({ updateTaskLoader: false });
      } else if (!values.quantity.match(numbers)) {
        err_cnt++;
        actions.setErrors({ quantity: "Invalid quantity" });
        actions.setSubmitting(false);
        this.setState({ updateTaskLoader: false });
      } else if (values.units === undefined || values.units === "") {
        err_cnt++;
        actions.setErrors({ units: "Please select unit" });
        actions.setSubmitting(false);
        this.setState({ updateTaskLoader: false });
      }
    } else if (req_type === "32") {
      if (values.product_id === undefined || values.product_id === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ updateTaskLoader: false });
      } else if (values.country_id === null || values.country_id === "") {
        err_cnt++;
        actions.setErrors({ country_id: "Please select atleast one country" });
        actions.setSubmitting(false);
        this.setState({ updateTaskLoader: false });
      }
    } else if (req_type === "33") {
      if (values.product_id === undefined || values.product_id === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ updateTaskLoader: false });
      } else if (values.country_id === null || values.country_id === "") {
        err_cnt++;
        actions.setErrors({ country_id: "Please select atleast one country" });
        actions.setSubmitting(false);
        this.setState({ updateTaskLoader: false });
      } else if (this.state.files.length === 0) {
        err_cnt++;
        actions.setErrors({ file_name: "Please attach file" });
        actions.setSubmitting(false);
        this.setState({ updateTaskLoader: false });
      }
    }

    if (err_cnt === 0) {
      postObject.ccCust = values.ccCustId;

      postObject.share_with_agent = values.share_with_agent;

      if (this.checkProductRequest(req_type, pid,order_verified_status) === true) {
        postObject.product_id = values.product_id;
      }

      if (this.checkMarketRequest(req_type, pid) === true) {
        if (values.country_id !== "") {
          postObject.country_id = JSON.stringify(values.country_id);
        } else {
          postObject.country_id = "";
        }
      }

      if (this.checkPharmacopoeia(req_type, pid) === true) {
        postObject.pharmacopoeia = values.pharmacopoeia;
      }
      if (this.checkTGA(req_type, pid) === true) {
        postObject.gmp_clearance_id = values.gmp_clearance_id;
        postObject.tga_email_id = values.tga_email_id;
        postObject.applicant_name = values.applicant_name;
        postObject.doc_required = values.doc_required;
      }

      if (this.checkPolymorphicForm(req_type, pid) === true) {
        postObject.polymorphic_form = values.polymorphic_form;
      }

      if (this.checkNatureOfIssue(req_type, pid) === true) {
        postObject.nature_of_issue = values.nature_of_issue;
      }

      if (this.checkBatchNo(req_type, pid) === true) {
        postObject.batch_number = values.batch_number;
      }

      if (this.checkQuantity(req_type, pid, order_verified_status) === true) {
        postObject.quantity = values.quantity + " " + values.units;
      }

      if (this.checkRDD(req_type, pid, order_verified_status) === true) {
        //postObject.rdd = dateFormat(values.rdd, "yyyy-mm-dd");
        if (values.rdd && values.rdd != "") {
          postObject.rdd = dateFormat(values.rdd, "yyyy-mm-dd");
        } else {
          postObject.rdd = "";
        }
      }

      if (this.checkPoNumber(req_type, pid) === true) {
        postObject.po_number = values.po_number
      }

      if (this.checkNotificationNumber(req_type, pid) === true) {
        postObject.notification_number = values.notification_number;
      }

      if (this.checkProductCode(req_type, pid,order_verified_status) === true) {
        postObject.product_code = values.product_code;
      }

      if (this.checkDMFNumber(req_type, pid) === true) {
        postObject.dmf_number = values.dmf_number;
      }

      if (this.checkRDORC(req_type, pid) === true) {
        postObject.rdfrc = dateFormat(values.rdfrc, "yyyy-mm-dd");
      }

      if (this.checkAuditVistDate(req_type, pid) === true) {
        postObject.request_audit_visit_date = dateFormat(
          values.request_audit_visit_date,
          "yyyy-mm-dd"
        );
      }

      if (this.checkStabilityDataType(req_type, pid) === true) {
        postObject.stability_data_type = values.stability_data_type;
      }

      if (this.checkAuditSiteName(req_type, pid) === true) {
        postObject.audit_visit_site_name = values.audit_visit_site_name;
      }

      if (this.checkAposDocumentType(req_type, pid) === true) {
        postObject.apos_document_type =
          values.apos_document_type != "" && values.apos_document_type != null
            ? JSON.stringify(values.apos_document_type)
            : "";
      }

      if (pid === 0) {
        postObject.content = values.content.trim();
      }
      //SATYAJIT
      if (this.checkSampleRequest(req_type, pid) === true) {
        if (this.state.service_request_type.check_sample) {
          postObject.samples = [
            { no_of_batch: values.sampleBatchNo },
            { quantity: values.sampleQuantity },
            { unit: values.sampleUnit },
          ];
        }

        if (this.state.service_request_type.check_working) {
          postObject.working = [
            { quantity: values.workingQuantity },
            { unit: values.workingUnit },
          ];
        }

        if (this.state.service_request_type.check_impurities) {
          postObject.impurities = [
            { requireText: values.impureRequired },
            { quantity: values.impureQuantity },
            { unit: values.impureUnit },
          ];
        }

        postObject.shipping_address = values.shipping_address
          ? values.shipping_address
          : "";
      }

      if (req_type === 24) {
        endpoint = `/api/my_team_tasks/update_forecast/${this.state.task_id}`;
      } else if(req_type === 23 && order_verified_status == 0) {
        postObject.swi_data = this.state.not_approved_tasks.length > 0 ? JSON.stringify(modifyArr(this.state.not_approved_tasks)) : JSON.stringify([]);
        endpoint = `/api/add_task/approve_order_tasks/${this.state.task_id}`;
      } else {
        endpoint = `/api/my_team_tasks/${this.state.task_id}`;
      }

      postObject.posted_for = this.state.posted_for;

      //console.log(this.state.service_request_type.check_sample);

      API.put(endpoint, postObject)
        .then((response) => {
          this.setState({ updateTaskLoader: false });
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: (req_type === 23 && order_verified_status == 0)?"Task has been Approved Successfully.":"Task has been updated successfully.",
            icon: "success",
          }).then(() => {
            this.props.history.push(
              `/user/team_task_details/${this.state.task_id}/${this.state.assign_id}/${this.state.posted_for}`
            );
          });
        })
        .catch((err) => {
          this.setState({ updateTaskLoader: false });
          if (err.data.status === 3) {
            var token_rm = 2;
            showErrorMessageFront(err, token_rm, this.props);
            //this.handleClose();
          } else {

            if(err.data.swi_err){
              this.setState({not_approved_tasks:JSON.parse(err.data.errors.SWI_ERR)});
            }

            actions.setErrors(err.data.errors);
            actions.setSubmitting(false);
          }
        });

      //console.log( postObject )
    }
  };

  changeCCCust = (event, setFieldValue) => {
    if (event === null) {
      setFieldValue("ccCustId", "");
    } else {
      setFieldValue("ccCustId", event);
    }
  };

  shareWithAgent = (event, setFieldValue) => {
    if (event.target.checked === true) {
      setFieldValue("share_with_agent", 1);
    } else {
      setFieldValue("share_with_agent", 0);
    }
  };

  setCountry = (event, setFieldValue) => {
    setFieldValue("country_id", event);
  };

  setAposDocumentType = (aposdate) => {
    if (aposdate && aposdate.length > 0) {
      var aposArr = aposdate.split(",");
      let ret = [];
      for (let index = 0; index < aposArr.length; index++) {
        const element = aposArr[index];
        ret.push({ label: element, value: element });
      }
      return ret;
    }
  };

  setADocType = (event, setFieldValue) => {
    setFieldValue("apos_document_type", event);
  };

    
  handleSWI = (event, index, key) => {
    let prev_state = this.state.not_approved_tasks;
    prev_state[index].common_err = "";

    if (key === "rdd") {
      prev_state[index].rdd = event;
    }

    if (key === "product_id") {
      console.log(event);
      prev_state[index].product_id = event.target.value;
    }

    if (key === "quantity") {
      prev_state[index].quantity = event.target.value;
    }

    if (key === "unit") {
      prev_state[index].units = event.target.value;
    }

    if (key === "material_id") {

      if(event.target.value.length == 0){
        prev_state[index].material_id_auto_suggest = '';
      }

      prev_state[index].material_id = event.target.value;
      {event.target.value.length > 2 && this.getAutoSuggestion(event.target.value,index)}
    }

    if(key === "material_id_set"){
      prev_state[index].material_id = event;
      prev_state[index].material_id_auto_suggest = '';
    }

    if(key === "material_id_auto_suggest"){
      prev_state[index].material_id_auto_suggest = event;
    }

    if(key === 'clear_material_id_auto_suggest'){
      prev_state[index].material_id_auto_suggest = '';
    }

    // console.log('prev_state',prev_state);
    this.setState({ not_approved_tasks: prev_state });
  };

  rmError = (index) => {
    let prev_state = this.state.selProducts;
    prev_state[index].common_err = "";
    this.setState({ selProducts: prev_state }, () => {
      this.getHtmlSIW(prev_state);
    });
  };

  getAutoSuggestion = (material_id,index_for) => {
    if(material_id && material_id.length > 2){
      API.post(`/api/feed/get_sap_Product_code`,{material_id})
      .then((res) => {
        
        let ret_html = res.data.data.map((val,index)=>{
          return (<li style={{cursor:'pointer',marginTop:'6px'}} onClick={()=>this.handleSWI(val.value, index_for, "material_id_set")} >{val.label}<hr style={{marginTop:'6px',marginBottom:'0px'}} /></li>)
        })
        this.handleSWI(ret_html, index_for, "material_id_auto_suggest")
      })
      .catch((err) => {
        console.log(err);
      });
    }
  }

  showUnApprovedTasks = () => {
      let ret_data = [];
      // console.log('not_approved_tasks',this.state.not_approved_tasks);
      ret_data = this.state.not_approved_tasks.map((val, index) => {
        return (

          <>
            <Col xs={12} sm={12} md={12}>
              <hr/>
            </Col>
            <Col xs={12} sm={3} md={3}>
              <div className="form-group">
                <label>{`Product`}<span className="required-field">*</span></label>
                <Field
                  name="product_id"
                  component="select"
                  className="form-control"
                  autoComplete="off"
                  value={val.product_id}
                  onChange={(e) => {
                    this.handleSWI(e, index, "product_id");
                  }}
                >
                  <option key="-1" value="">
                    Select Product
                  </option>
                  {this.state.product.map((products, i) => (
                    <option
                      key={i}
                      value={products.product_id}
                    >
                      {htmlDecode(products.product_name)}
                    </option>
                  ))}
                </Field>
              </div>
            </Col>

            <Col xs={12} sm={2} md={2}>
              <div className="form-group">
                <label>{`Quantity`}<span className="required-field">*</span></label>
                <Field
                  name="quantity"
                  type="text"
                  className="form-control"
                  autoComplete="off"
                  value={val.quantity}
                  onChange={(e) => {
                    this.handleSWI(e, index, "quantity");
                  }}
                ></Field>
              </div>
            </Col>
            <Col xs={12} sm={2} md={2}>
            <label>{`Units`}<span className="required-field">*</span></label>
              <Field
                name="units"
                component="select"
                className={`selectArowGray form-control`}
                autoComplete="off"
                value={val.units}
                onChange={(e) => {
                  this.handleSWI(e, index, "unit");
                }}
              >
                <option key="-1" value="">
                    Select
                </option>
                <option key="1" value="Mgs">
                    Mgs
                </option>

                <option key="2" value="Gms">
                    Gms
                </option>

                <option key="3" value="Kgs">
                    Kgs
                </option>
                <option key="8" value="Vials">
                  Vials
                </option>
              </Field>
            </Col>
            <Col xs={12} sm={2} md={2}>
              <div className="form-group">
                  <label>{`Requested Date Of Delivery`}<span className="required-field">*</span></label>
                  <div className="form-control">
                  <DatePicker
                      className="borderNone"
                      selected={(val.rdd !== null && val.rdd !== "") ? new Date(val.rdd) : ''}
                      name="deliveryDate"
                      showMonthDropdown
                      showYearDropdown
                      minDate={minDate}
                      maxDate={maxDate}
                      dropdownMode="select"
                      onChange={(e) => {
                        this.handleSWI(e, index, "rdd");
                      }}
                      dateFormat="dd/MM/yyyy"
                      autoComplete="off"
                      placeholderText={`RDD`}
                  />
                  </div>
              </div>
            </Col>

            <Col xs={12} sm={3} md={3}>
              <div className="form-group" onMouseLeave={(e)=>{
                    if(val.material_id_auto_suggest != ''){
                      document.getElementById(`material_id${val.temp_id}`).blur();
                      this.handleSWI(true, index, "clear_material_id_auto_suggest");
                    }
                  }} >
                <label>{`Product Code`}<span className="required-field">*</span></label>
                <Field
                  name="material_id"
                  type="text"
                  id={`material_id${val.temp_id}`}
                  className="form-control"
                  autoComplete="off"
                  value={val.material_id}
                  onChange={(e) => {
                    this.handleSWI(e, index, "material_id");
                  }}
                  onFocus={(e)=>{
                    if(val.material_id != '' && val.material_id.length > 2){
                      this.getAutoSuggestion(val.material_id,index);
                    }
                  }}
                ></Field>
                {val.material_id_auto_suggest != '' && <ul id={`material_id_${index}`} style={{zIndex:'30',position:'absolute',backgroundColor:'#d0d0d0',height:"100px",overflowY:'auto',listStyleType:'none',width:'100%'}} >
                  {val.material_id_auto_suggest}
                </ul>}
              </div>
            </Col>
            <Col xs={12} sm={12} md={12}>
              {val.common_err != '' ? (
                <div className="form-group">
                  <span className="errorMsg">
                    {ReactHtmlParser(val.common_err)}
                  </span>
                </div>
              ) : null}
            </Col>
          </>
        )
      })
      return ret_data;
  }

  render() {
    //console.log('log', this.state.taskDetails.request_type);
    //console.log('sss', this.state.taskDetails.number_of_batches);

    // SATYAJIT
    if (this.props.match.params.id === 0 || this.props.match.params.id === "") {
      return <Redirect to="/user/dashboard" />;
    }

    const { taskDetails, product, country, service_request_type } = this.state;

    //console.log('+++++',service_request_type);

    let validateEditTask = null;
    if (taskDetails.task_id > 0) {
      if (taskDetails.request_type === 1) {
        validateEditTask = Yup.object().shape({
          product_id: Yup.string().trim().required("Please select product name"),
          shipping_address: Yup.string().trim().required(
            "Please enter shipping address"
          ),
          /* service_request_type: Yup.lazy(obj => Yup.object(
                        mapValues(obj, (v, k) => { 

                            console.log('====k===', k, this.state.service_request_type.check_sample, v );
                            
                          if (k.startsWith('check_impurities')) {
                            return Yup.string().required("Hello")
                          }
                        })
                      )) */
          /* service_request_type: lazy(obj =>                             
                        Yup.object(
                                mapValues(obj, (value, key) => {
                                    console.log(value,key,'>>>>>');
                                    if (value === key || value === key || value === key) {
                                      return Yup.string().required();
                                    }
                                }
                            )
                        )
                    ) */
          /* samplesCheck: Yup.bool()
                        .test(
                          'samplesCheck',
                          'You have to agree with our Terms and Conditions!',
                          value => value === true
                        )
                        .required(
                          'You have to agree with our Terms and Conditions!'
                        ), */
          //samplesCheck: Yup.array().required('Permission not checked')
        });

        //console.log( validateEditTask );
      } else if (
        taskDetails.request_type === 2 ||
        taskDetails.request_type === 3 ||
        taskDetails.request_type === 4 ||
        taskDetails.request_type === 5 ||
        taskDetails.request_type === 6 ||
        taskDetails.request_type === 8 ||
        taskDetails.request_type === 10 ||
        taskDetails.request_type === 11 ||
        taskDetails.request_type === 12 ||
        taskDetails.request_type === 14 ||
        taskDetails.request_type === 15 ||
        taskDetails.request_type === 16 ||
        taskDetails.request_type === 21 ||
        taskDetails.request_type === 22
      ) {
        validateEditTask = Yup.object().shape({
          product_id: Yup.string().trim().required("Please select product name"),
        });
      } else if (
        taskDetails.request_type === 7 ||
        taskDetails.request_type === 34
      ) {
        validateEditTask = Yup.object().shape({
          product_id: Yup.string().trim().required("Please select product name"),
          batch_number: Yup.string().trim()
            .required("Please enter batch number")
            .min(10, "Batch Number cannot be less than 10 characters")
            .max(10, "Batch Number cannot be more than 10 characters")
            .matches(
              /[a-zA-Z]{4}\d{6}/,
              "Batch Number field is not in the right format"
            ),
          // quantity: Yup.string().trim().notRequired().test('quantity', 'Invalid quantity', function(value) {
          //         if (value !== '') {
          //           const schema = Yup.string().matches(/^[0-9]*$/);
          //           return schema.isValidSync(value);
          //         }
          //         return true;
          // })
        });
      } else if (taskDetails.request_type === 9) {
        validateEditTask = Yup.object().shape({
          product_id: Yup.string().trim().required("Please select product name"),
          audit_visit_site_name: Yup.string().trim().required(
            "Please enter site name"
          ),
          request_audit_visit_date: Yup.date()
            .nullable()
            .required("Audit/Visit date required"),
        });
      } else if (taskDetails.request_type === 13) {
        validateEditTask = Yup.object().shape({
          product_id: Yup.string().trim().required("Please select product name"),
          stability_data_type: Yup.string().trim().required("Please select data type"),
        });
      } else if (
        taskDetails.request_type === 17 ||
        taskDetails.request_type === 18 ||
        taskDetails.request_type === 19 ||
        taskDetails.request_type === 20
      ) {
        validateEditTask = Yup.object().shape({
          product_id: Yup.string().trim().required("Please select product name"),
          /* country_id: Yup.string()
                        .required("Please select country") */
        });
      } else if (taskDetails.request_type === 23) {
        validateEditTask = Yup.object().shape({
          product_id: Yup.string().trim().required("Please select product name"),
        });
      } else if (taskDetails.request_type === 39) {
        validateEditTask = Yup.object().shape({
          dmf_number: Yup.string().trim().required("Please enter DMF/CEP value"),
          country_id: Yup.string().trim().required("Market field is required."),
        });
      } else if (taskDetails.request_type === 40) {
        validateEditTask = Yup.object().shape({
          gmp_clearance_id: Yup.string().trim().required(
            "Please enter GMP Clearance ID"
          ),
          tga_email_id: Yup.string().trim().required("Please enter Email ID"),
          applicant_name: Yup.string().trim().required(
            "Please enter applicant's name and address"
          ),
          doc_required: Yup.string().trim().required(
            "Please enter Documents Required"
          ),
        });
      } else if (taskDetails.request_type === 41) {
        validateEditTask = Yup.object().shape({
          product_id: Yup.string().trim().required("Please select product name"),
        });
      }
    }

    if (taskDetails.task_id > 0 && this.state.editLoader === false) {
      return (
        <>
          {this.state.updateTaskLoader && (
            <div className="loderOuter">
              <div className="loader">
                <img src={loaderlogo} alt="logo" />
                <div className="loading">Loading...</div>
              </div>
            </div>
          )}
          <div className="content-wrapper">
            <section className="content-header">
              <h1>{taskDetails.req_name}</h1>
              <div
                className="customer-edit-button btn-fill"
                onClick={() =>
                  this.props.history.push(
                    `/user/team_task_details/${this.state.task_id}/${this.state.assign_id}/${this.state.posted_for}`
                  )
                }
              >
                Back
              </div>
            </section>

            <section className="content">
              <div className="boxPapanel content-padding form-min-hight">
                <div className="taskdetails">
                  {/*<div className="taskdetailsHeader">
                                        <h3>{taskDetails.req_name}</h3>
                                    </div>*/}

                  <div className="clearfix tdBtmlist">
                    <Formik
                      initialValues={initialValues}
                      validationSchema={validateEditTask}
                      onSubmit={this.submitEditTask}
                    >
                      {({
                        values,
                        errors,
                        touched,
                        isValid,
                        isSubmitting,
                        setFieldValue,
                      }) => {
                        //console.log("Errors === ", errors);
                        return (
                          <Form>
                            <Row className="eaditRow">
                              {this.checkProductRequest(
                                taskDetails.request_type,
                                taskDetails.parent_id,
                                taskDetails.order_verified_status
                              ) === true && (
                                <Col xs={12} sm={6} md={3}>
                                  <div className="form-group has-feedback">
                                    <label>
                                      Product{" "}
                                      <span class="required-field">*</span>
                                    </label>

                                    <Field
                                      name="product_id"
                                      component="select"
                                      className="form-control"
                                      autoComplete="off"
                                      value={values.product_id}
                                    >
                                      <option key="-1" value="">
                                        Select Product
                                      </option>
                                      {product.map((products, i) => (
                                        <option
                                          key={i}
                                          value={products.product_id}
                                        >
                                          {htmlDecode(products.product_name)}
                                        </option>
                                      ))}
                                    </Field>
                                    {errors.product_id && touched.product_id ? (
                                      <span className="errorMsg">
                                        {errors.product_id}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              )}

                              {this.checkMarketRequest(
                                taskDetails.request_type,
                                taskDetails.parent_id
                              ) === true && (
                                <Col xs={12} sm={6} md={3}>
                                  <div className="form-group  has-feedback">
                                    <label>
                                      Market{" "}
                                      {taskDetails.request_type === 17 ||
                                      taskDetails.request_type === 18 ||
                                      taskDetails.request_type === 19 ||
                                      taskDetails.request_type === 20 ||
                                      taskDetails.request_type === 32 ||
                                      taskDetails.request_type === 23 ||
                                      taskDetails.request_type === 41 ||
                                      taskDetails.request_type === 39 ||
                                      taskDetails.request_type === 42 ? (
                                        <span class="required-field">*</span>
                                      ) : (
                                        ""
                                      )}
                                    </label>

                                    <Select
                                      isMulti
                                      className=""
                                      classNamePrefix="select"
                                      defaultValue=""
                                      isClearable={true}
                                      isSearchable={true}
                                      name="country_id"
                                      options={country}
                                      value={
                                        values.country_id !== null &&
                                        values.country_id !== ""
                                          ? values.country_id
                                          : ""
                                      }
                                      onChange={(e) => {
                                        this.setCountry(e, setFieldValue);
                                      }}
                                    />
                                    {errors.country_id || touched.country_id ? (
                                      <span className="errorMsg">
                                        {errors.country_id}
                                      </span>
                                    ) : null}
                                    {errors.country_id && touched.country_id ? (
                                      <span className="errorMsg">
                                        {errors.country_id}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              )}

                              {this.checkPharmacopoeia(
                                taskDetails.request_type,
                                taskDetails.parent_id
                              ) === true && (
                                <Col xs={12} sm={6} md={3}>
                                  <div className="form-group">
                                    <label>Pharmacopoeia</label>
                                    <Field
                                      name="pharmacopoeia"
                                      type="text"
                                      className="form-control"
                                      autoComplete="off"
                                      value={
                                        values.pharmacopoeia !== null &&
                                        values.pharmacopoeia !== ""
                                          ? values.pharmacopoeia
                                          : ""
                                      }
                                    ></Field>
                                    {errors.pharmacopoeia &&
                                    touched.pharmacopoeia ? (
                                      <span className="errorMsg">
                                        {errors.pharmacopoeia}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              )}

                              {this.checkTGA(
                                taskDetails.request_type,
                                taskDetails.parent_id
                              ) === true && (
                                <>
                                  <Col xs={12} sm={6} md={3}>
                                    <div className="form-group">
                                      <label>
                                        GMP Clearance ID
                                        <span class="required-field">*</span>
                                      </label>

                                      <Field
                                        name="gmp_clearance_id"
                                        type="text"
                                        className="form-control"
                                        autoComplete="off"
                                        value={
                                          values.gmp_clearance_id !== null &&
                                          values.gmp_clearance_id !== ""
                                            ? values.gmp_clearance_id
                                            : ""
                                        }
                                      ></Field>
                                      {errors.gmp_clearance_id &&
                                      touched.gmp_clearance_id ? (
                                        <span className="errorMsg">
                                          {errors.gmp_clearance_id}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>

                                  <Col xs={12} sm={6} md={3}>
                                    <div className="form-group">
                                      <label>
                                        Email ID
                                        <span class="required-field">*</span>
                                      </label>

                                      <Field
                                        name="tga_email_id"
                                        type="text"
                                        className="form-control"
                                        autoComplete="off"
                                        value={
                                          values.tga_email_id !== null &&
                                          values.tga_email_id !== ""
                                            ? values.tga_email_id
                                            : ""
                                        }
                                      ></Field>
                                      {errors.tga_email_id &&
                                      touched.tga_email_id ? (
                                        <span className="errorMsg">
                                          {errors.tga_email_id}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>

                                  <Col xs={12} sm={6} md={3}>
                                    <div className="form-group">
                                      <label>
                                        Applicant's Name & Address
                                        <span class="required-field">*</span>
                                      </label>

                                      <Field
                                        name="applicant_name"
                                        type="text"
                                        className="form-control"
                                        autoComplete="off"
                                        value={
                                          values.applicant_name !== null &&
                                          values.applicant_name !== ""
                                            ? values.applicant_name
                                            : ""
                                        }
                                      ></Field>
                                      {errors.applicant_name &&
                                      touched.applicant_name ? (
                                        <span className="errorMsg">
                                          {errors.applicant_name}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>

                                  <Col xs={12} sm={6} md={3}>
                                    <div className="form-group">
                                      <label>
                                        Documents Required
                                        <span class="required-field">*</span>
                                      </label>

                                      <Field
                                        name="doc_required"
                                        type="text"
                                        className="form-control"
                                        autoComplete="off"
                                        value={
                                          values.doc_required !== null &&
                                          values.doc_required !== ""
                                            ? values.doc_required
                                            : ""
                                        }
                                      ></Field>
                                      {errors.doc_required &&
                                      touched.doc_required ? (
                                        <span className="errorMsg">
                                          {errors.doc_required}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </>
                              )}

                              {this.checkNatureOfIssue(
                                taskDetails.request_type,
                                taskDetails.parent_id
                              ) === true && (
                                <Col xs={12} sm={6} md={3}>
                                  <div className="form-group">
                                    <label>Nature Of Issue</label>

                                    <Field
                                      name="nature_of_issue"
                                      type="text"
                                      className="form-control"
                                      autoComplete="off"
                                      value={
                                        values.nature_of_issue !== null &&
                                        values.nature_of_issue !== ""
                                          ? values.nature_of_issue
                                          : ""
                                      }
                                    ></Field>
                                    {errors.nature_of_issue &&
                                    touched.nature_of_issue ? (
                                      <span className="errorMsg">
                                        {errors.nature_of_issue}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              )}

                              {this.checkBatchNo(
                                taskDetails.request_type,
                                taskDetails.parent_id
                              ) === true && (
                                <Col xs={12} sm={6} md={3}>
                                  <div className="form-group">
                                    <label>
                                      Batch No{" "}
                                      <span class="required-field">*</span>
                                    </label>

                                    <Field
                                      name="batch_number"
                                      type="text"
                                      className="form-control"
                                      autoComplete="off"
                                      value={
                                        values.batch_number !== null &&
                                        values.batch_number !== ""
                                          ? values.batch_number
                                          : ""
                                      }
                                    ></Field>
                                    {errors.batch_number &&
                                    touched.batch_number ? (
                                      <span className="errorMsg errorBatch">
                                        {errors.batch_number}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              )}

                              {this.checkQuantity(
                                taskDetails.request_type,
                                taskDetails.parent_id,
                                taskDetails.order_verified_status
                              ) === true && (
                                <>
                                  <Col xs={12} sm={6} md={3}>
                                    <div className="form-group">
                                      <label>
                                        Quantity{" "}
                                        {taskDetails.request_type === 23 ||
                                        taskDetails.request_type === 41 ? (
                                          <span class="required-field">*</span>
                                        ) : (
                                          ""
                                        )}
                                      </label>

                                      <Field
                                        name="quantity"
                                        type="text"
                                        className="form-control"
                                        autoComplete="off"
                                        value={
                                          values.quantity !== null &&
                                          values.quantity !== ""
                                            ? values.quantity
                                            : ""
                                        }
                                      ></Field>
                                      {errors.quantity && touched.quantity ? (
                                        <span className="errorMsg">
                                          {errors.quantity}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                  <Col xs={12} sm={6} md={3}>
                                    <div className="form-group">
                                      <label>
                                        Units{" "}
                                        {taskDetails.request_type === 23 ||
                                        taskDetails.request_type === 41 ? (
                                          <span class="required-field">*</span>
                                        ) : (
                                          ""
                                        )}
                                      </label>

                                      <Field
                                        name="units"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.units}
                                      >
                                        <option key="-1" value="">
                                          Select
                                        </option>
                                        <option key="1" value="Mgs">
                                          Mgs
                                        </option>

                                        {(taskDetails.request_type === 1 ||
                                          taskDetails.request_type === 7 ||
                                          taskDetails.request_type === 34 ||
                                          taskDetails.request_type === 23 ||
                                          taskDetails.request_type === 41 ||
                                          taskDetails.request_type === 31 ||
                                          taskDetails.request_type === 44) && (
                                          <option key="2" value="Gms">
                                            Gms
                                          </option>
                                        )}

                                        <option key="3" value="Kgs">
                                          Kgs
                                        </option>
                                        <option key="8" value="Vials">
                                          Vials
                                        </option>
                                      </Field>
                                      {errors.units && touched.units ? (
                                        <span className="errorMsg">
                                          {errors.units}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </>
                              )}

                              {this.checkRDORC(
                                taskDetails.request_type,
                                taskDetails.parent_id
                              ) && (
                                <Col xs={12} sm={6} md={3}>
                                  <div className="form-group">
                                    <label>
                                      Requested date of response/closure:
                                    </label>
                                    <div className="form-control">
                                      <DatePicker
                                        name={"rdfrc"}
                                        className="borderNone"
                                        selected={
                                          values.rdfrc ? values.rdfrc : null
                                        }
                                        onChange={(value) =>
                                          setFieldValue("rdfrc", value)
                                        }
                                        dateFormat="dd/MM/yyyy"
                                      />
                                    </div>
                                    {errors.rdfrc || touched.rdfrc ? (
                                      <span className="errorMsg">
                                        {errors.rdfrc}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              )}

                              {this.checkDMFNumber(
                                taskDetails.request_type,
                                taskDetails.parent_id
                              ) && (
                                <Col xs={12} sm={6} md={3}>
                                  <div className="form-group">
                                    <label>
                                      {taskDetails.request_type === 39
                                        ? "DMF/CEP:"
                                        : "DMF Number:"}
                                    </label>
                                    <Field
                                      name="dmf_number"
                                      type="text"
                                      className="form-control"
                                      autoComplete="off"
                                      value={
                                        values.dmf_number !== null &&
                                        values.dmf_number !== ""
                                          ? values.dmf_number
                                          : ""
                                      }
                                    ></Field>
                                    {errors.dmf_number || touched.dmf_number ? (
                                      <span className="errorMsg">
                                        {errors.dmf_number}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              )}

                              {this.checkAposDocumentType(
                                taskDetails.request_type,
                                taskDetails.parent_id
                              ) && (
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>Document Type</label>
                                      <Select
                                        isMulti
                                        className="basic-single"
                                        classNamePrefix="select"
                                        defaultValue=""
                                        isClearable={true}
                                        isSearchable={true}
                                        name="apos_document_type"
                                        options={[
                                          {
                                            label: "Site GMP",
                                            value: "Site GMP",
                                          },
                                          {
                                            label: "Site Manufacturing License",
                                            value: "Site Manufacturing License",
                                          },
                                          {
                                            label: "Written Confirmation",
                                            value: "Written Confirmation",
                                          },
                                        ]}
                                        value={
                                          values.apos_document_type !== null &&
                                          values.apos_document_type !== ""
                                            ? values.apos_document_type
                                            : ""
                                        }
                                        onChange={(e) => {
                                          this.setADocType(e, setFieldValue);
                                        }}
                                      />
                                      {errors.apos_document_type ||
                                      touched.apos_document_type ? (
                                        <span className="errorMsg">
                                          {errors.apos_document_type}
                                        </span>
                                      ) : null}
                                    </div>
                                  </Col>
                                </Row>
                              )}

                              {this.checkNotificationNumber(
                                taskDetails.request_type,
                                taskDetails.parent_id
                              ) && (
                                <Col xs={12} sm={6} md={3}>
                                  <div className="form-group">
                                    <label>Notification Number:</label>
                                    <Field
                                      name="notification_number"
                                      type="text"
                                      className="form-control"
                                      autoComplete="off"
                                      value={
                                        values.notification_number !== null &&
                                        values.notification_number !== ""
                                          ? values.notification_number
                                          : ""
                                      }
                                    ></Field>
                                    {errors.notification_number ||
                                    touched.notification_number ? (
                                      <span className="errorMsg">
                                        {errors.notification_number}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              )}

                              {this.checkRDD(
                                taskDetails.request_type,
                                taskDetails.parent_id,
                                taskDetails.order_verified_status
                              ) === true && (
                                <Col xs={12} sm={6} md={3}>
                                  <div className="form-group">
                                    <label>Request Date Of Delivery</label>
                                    <div className="form-control">
                                      {/* <DatePicker
                                                            name="rdd"
                                                            onChange={value =>
                                                                setFieldValue("rdd",value)
                                                            }
                                                            value={values.rdd}
                                                        /> */}

                                      <DatePicker
                                        name="rdd"
                                        className="borderNone"
                                        selected={
                                          values.rdd ? values.rdd : null
                                        }
                                        onChange={(value) =>
                                          setFieldValue("rdd", value)
                                        }
                                        dateFormat="dd/MM/yyyy"
                                        autoComplete="off"
                                      />

                                      {errors.rdd && touched.rdd ? (
                                        <span className="errorMsg">
                                          {errors.rdd}
                                        </span>
                                      ) : null}
                                    </div>
                                  </div>
                                </Col>
                              )}

                              {this.checkProductCode(
                                taskDetails.request_type,
                                taskDetails.parent_id,
                                taskDetails.order_verified_status
                              ) && (
                                <Col xs={12} sm={6} md={3}>
                                  <div className="form-group" onMouseLeave={(e)=>{
                                    if(this.state.product_code_auto_suggest != ''){
                                      document.getElementById(`product_code`).blur();
                                      this.setState({product_code_auto_suggest:''});
                                    }
                                  }} >
                                    <label>Product Code<span className="required-field">*</span></label>
                                    <Field
                                      name="product_code"
                                      type="text"
                                      className="form-control"
                                      id={`product_code`}
                                      autoComplete="off"
                                      value={
                                        values.product_code !== null &&
                                        values.product_code !== ""
                                          ? values.product_code
                                          : ""
                                      }
                                      onChange={(e) => {
                                        this.setMaterialID(e, setFieldValue);
                                      }}
                                      onFocus={(e)=>{
                                        if(values.product_code != '' && values.product_code.length > 2){
                                          API.post(`/api/feed/get_sap_Product_code`,{material_id:values.product_code})
                                          .then((res) => {
                                            
                                            let ret_html = res.data.data.map((val,index)=>{
                                              return (<li style={{cursor:'pointer',marginTop:'6px'}} onClick={()=>this.setMaterialIDAutoSuggest(val.value,setFieldValue)} >{val.label}<hr style={{marginTop:'6px',marginBottom:'0px'}} /></li>)
                                            })
                                            this.setState({product_code_auto_suggest:ret_html});
                                          })
                                          .catch((err) => {
                                            console.log(err);
                                          });
                                        }
                                      }}
                                    ></Field>

                                    {this.state.product_code_auto_suggest != '' && <ul id={`material_id_auto_suggect`} style={{zIndex:'30',position:'absolute',backgroundColor:'#d0d0d0',height:"100px",overflowY:'auto',listStyleType:'none',width:'100%'}} >
                                      {this.state.product_code_auto_suggest}
                                    </ul>}

                                    {errors.product_code ||
                                    touched.product_code ? (
                                      <span className="errorMsg">
                                        {errors.product_code}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              )}

                              {this.checkPoNumber(
                                taskDetails.request_type,
                                taskDetails.parent_id
                              ) === true && (
                                <Col xs={12} sm={6} md={3}>
                                  <div className="form-group">
                                  <label>
                                    PO Number
                                    <span className="required-field">*</span>
                                  </label>
                                    <div>
                                    <Field
                                      name="po_number"
                                      type="text"
                                      className="form-control"
                                      autoComplete="off"
                                    />
                                    {errors.po_number && touched.po_number ? (
                                      <span className="errorMsg">
                                        {errors.po_number}
                                      </span>
                                    ) : null}
                                    </div>
                                  </div>
                                </Col>
                              )}

                              {/* {this.checkPODeliveryDate(
                                taskDetails.request_type,
                                taskDetails.parent_id
                              ) === true && (
                                <Col xs={12} sm={6} md={3}>
                                  <div className="form-group">
                                    <label>PO Date:</label>
                                    <div className="form-control">

                                      <DatePicker
                                        name="rdd"
                                        className="borderNone"
                                        minDate={new Date()}
                                        selected={
                                          values.po_delivery_date != null ? values.po_delivery_date : ''
                                        }
                                        onChange={(value) =>
                                          setFieldValue("po_delivery_date", value)
                                        }
                                        dateFormat="dd/MM/yyyy"
                                        autoComplete="off"
                                      />

                                      {errors.po_delivery_date && touched.po_delivery_date ? (
                                        <span className="errorMsg">
                                          {errors.po_delivery_date}
                                        </span>
                                      ) : null}
                                    </div>
                                  </div>
                                </Col>
                              )} */}

                              {this.checkPolymorphicForm(
                                taskDetails.request_type,
                                taskDetails.parent_id
                              ) === true && (
                                <Col xs={12} sm={6} md={3}>
                                  <div className="form-group">
                                    <label>Polymorphic Form</label>

                                    <Field
                                      name="polymorphic_form"
                                      type="text"
                                      className="form-control"
                                      autoComplete="off"
                                      value={
                                        values.polymorphic_form !== null &&
                                        values.polymorphic_form !== ""
                                          ? values.polymorphic_form
                                          : ""
                                      }
                                    ></Field>
                                    {errors.polymorphic_form &&
                                    touched.polymorphic_form ? (
                                      <span className="errorMsg">
                                        {errors.polymorphic_form}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              )}
                            </Row>

                            {/*edit task details Start*/}
                            {taskDetails.request_type === 1 && (
                              <div className="edit_task_details">
                                <Row>
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group1">
                                      <Field
                                        type="checkbox"
                                        name="samplesCheck"
                                        checked={
                                          service_request_type.check_sample
                                        }
                                        value={
                                          values.service_request_type
                                            .field_sample
                                        }
                                        onChange={(e) => {
                                          this.toggleCheckbox(e, setFieldValue);
                                        }}
                                      />

                                      <label>Samples</label>
                                      {errors.samplesCheck &&
                                      touched.samplesCheck ? (
                                        <span className="errorMsg">
                                          {errors.samplesCheck}
                                        </span>
                                      ) : null}
                                    </div>

                                    {service_request_type.check_sample && (
                                      <Row>
                                        <Col xs={12} sm={6} md={3}>
                                          <fieldset>
                                            <legend>
                                              {" "}
                                              <span className="fieldset-legend">
                                                Number of Batches
                                              </span>{" "}
                                            </legend>

                                            <div className="fieldset-wrapper">
                                              <label className="radio-inline">
                                                <Field
                                                  type="radio"
                                                  name="sampleBatchNo"
                                                  checked={
                                                    values.sampleBatchNo === "1"
                                                      ? true
                                                      : false
                                                  }
                                                  value="1"
                                                />{" "}
                                                1
                                              </label>

                                              <label className="radio-inline">
                                                <Field
                                                  type="radio"
                                                  name="sampleBatchNo"
                                                  checked={
                                                    values.sampleBatchNo === "2"
                                                      ? true
                                                      : false
                                                  }
                                                  value="2"
                                                />
                                                2
                                              </label>

                                              <label className="radio-inline">
                                                <Field
                                                  type="radio"
                                                  name="sampleBatchNo"
                                                  checked={
                                                    values.sampleBatchNo === "3"
                                                      ? true
                                                      : false
                                                  }
                                                  value="3"
                                                />
                                                3
                                              </label>
                                            </div>
                                          </fieldset>
                                        </Col>
                                        <Col xs={12} sm={6} md={3}>
                                          <div className="form-group">
                                            <Field
                                              name="sampleQuantity"
                                              type="text"
                                              className="form-control"
                                              autoComplete="off"
                                              placeholder="Quantity"
                                              value={values.sampleQuantity}
                                            ></Field>
                                          </div>
                                        </Col>
                                        <Col xs={12} sm={6} md={3}>
                                          <div className="form-group">
                                            <Field
                                              name="sampleUnit"
                                              component="select"
                                              className={`selectArowGray form-control`}
                                              autoComplete="off"
                                              value={values.sampleUnit}
                                            >
                                              <option key="-1" value="">
                                                Select
                                              </option>
                                              <option key="1" value="Mgs">
                                                Mgs
                                              </option>
                                              <option key="2" value="Gms">
                                                Grams
                                              </option>
                                              <option key="3" value="Kgs">
                                                Kgs
                                              </option>
                                              <option key="8" value="Vials">
                                                Vials
                                              </option>
                                            </Field>
                                            {errors.sampleUnit &&
                                            touched.sampleUnit ? (
                                              <span className="errorMsg">
                                                {errors.sampleUnit}
                                              </span>
                                            ) : null}
                                          </div>
                                        </Col>
                                      </Row>
                                    )}
                                  </Col>

                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group1">
                                      <Field
                                        type="checkbox"
                                        name="workingCheck"
                                        checked={
                                          service_request_type.check_working
                                        }
                                        value={
                                          values.service_request_type
                                            .field_working
                                        }
                                        onChange={(e) => {
                                          this.toggleCheckbox(e, setFieldValue);
                                        }}
                                      />

                                      <label>Working Standards</label>
                                    </div>

                                    {service_request_type.check_working && (
                                      <Row>
                                        <Col xs={12} sm={6} md={3}>
                                          <div className="form-group">
                                            <Field
                                              name="workingQuantity"
                                              type="text"
                                              className="form-control"
                                              autoComplete="off"
                                              placeholder="Quantity"
                                              value={values.workingQuantity}
                                            ></Field>
                                          </div>
                                        </Col>
                                        <Col xs={12} sm={6} md={3}>
                                          <div className="form-group">
                                            <Field
                                              name="workingUnit"
                                              component="select"
                                              className={`selectArowGray form-control`}
                                              autoComplete="off"
                                              value={values.workingUnit}
                                            >
                                              <option key="-1" value="">
                                                Select
                                              </option>
                                              <option key="1" value="Mgs">
                                                Mgs
                                              </option>
                                              <option key="2" value="Gms">
                                                Grams
                                              </option>
                                              <option key="3" value="Kgs">
                                                Kgs
                                              </option>
                                              <option key="8" value="Vials">
                                                Vials
                                              </option>
                                            </Field>
                                            {errors.workingUnit &&
                                            touched.workingUnit ? (
                                              <span className="errorMsg">
                                                {errors.workingUnit}
                                              </span>
                                            ) : null}
                                          </div>
                                        </Col>
                                        <Col xs={12} sm={6} md={3}></Col>
                                      </Row>
                                    )}
                                  </Col>

                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group1">
                                      <Field
                                        type="checkbox"
                                        name="impureCheck"
                                        checked={
                                          service_request_type.check_impurities
                                        }
                                        value={
                                          values.service_request_type
                                            .field_impurites
                                        }
                                        onChange={(e) => {
                                          this.toggleCheckbox(e, setFieldValue);
                                        }}
                                      />
                                      {errors.typeCheck && touched.typeCheck ? (
                                        <span className="errorMsg">
                                          {errors.typeCheck}
                                        </span>
                                      ) : null}

                                      <label>Impurities</label>
                                      {errors.service_request_type &&
                                      touched.service_request_type ? (
                                        <span className="errorMsg">
                                          {errors.service_request_type}
                                        </span>
                                      ) : null}
                                    </div>

                                    {service_request_type.check_impurities && (
                                      <Row>
                                        <Col xs={12} sm={6} md={3}>
                                          <div className="form-group">
                                            <Field
                                              name="impureRequired"
                                              type="text"
                                              className="form-control"
                                              autoComplete="off"
                                              placeholder="Specify impurity required"
                                              value={values.impureRequired}
                                            ></Field>
                                          </div>
                                        </Col>
                                        <Col xs={12} sm={6} md={3}>
                                          <div className="form-group">
                                            <Field
                                              name="impureQuantity"
                                              type="text"
                                              className="form-control"
                                              autoComplete="off"
                                              placeholder="Quantity"
                                              value={values.impureQuantity}
                                            ></Field>
                                          </div>
                                        </Col>
                                        <Col xs={12} sm={6} md={3}>
                                          <div className="form-group">
                                            <Field
                                              name="impureUnit"
                                              component="select"
                                              className={`selectArowGray form-control`}
                                              autoComplete="off"
                                              value={values.impureUnit}
                                            >
                                              <option key="-1" value="">
                                                Select
                                              </option>
                                              <option key="1" value="Mgs">
                                                Mgs
                                              </option>
                                              <option key="2" value="Gms">
                                                Grams
                                              </option>
                                              <option key="3" value="Kgs">
                                                Kgs
                                              </option>
                                              <option key="8" value="Vials">
                                               Vials
                                             </option>
                                            </Field>
                                            {errors.impureUnit &&
                                            touched.impureUnit ? (
                                              <span className="errorMsg">
                                                {errors.impureUnit}
                                              </span>
                                            ) : null}
                                          </div>
                                        </Col>
                                      </Row>
                                    )}
                                  </Col>
                                </Row>
                              </div>
                            )}
                            {/*edit task details Start*/}

                            <Row>
                              <Col xs={12} sm={12} md={12}>
                                <div className="form-group">
                                  <input
                                    type="checkbox"
                                    className="form-check-input"
                                    value={1}
                                    defaultChecked={
                                      this.state.taskDetails.share_with_agent
                                    }
                                    onChange={(e) =>
                                      this.shareWithAgent(e, setFieldValue)
                                    }
                                  />{" "}
                                  Display this request to Agent
                                </div>
                              </Col>
                            </Row>

                            <Row>
                              {this.checkStabilityDataType(
                                taskDetails.request_type,
                                taskDetails.parent_id
                              ) === true && (
                                <Col xs={12} sm={6} md={3}>
                                  <div className="form-group">
                                    <label>
                                      Stability Data Type{" "}
                                      <span class="required-field">*</span>
                                    </label>

                                    <Field
                                      name="stability_data_type"
                                      component="select"
                                      className="form-control"
                                      autoComplete="off"
                                      value={values.stability_data_type}
                                    >
                                      <option key="-1" value="">
                                        Select Type
                                      </option>
                                      <option key="1" value="ACC">
                                        ACC
                                      </option>
                                      <option key="2" value="Long Term">
                                        Long Term
                                      </option>
                                      <option key="3" value="Others">
                                        Others
                                      </option>
                                    </Field>
                                    {errors.stability_data_type &&
                                    touched.stability_data_type ? (
                                      <span className="errorMsg">
                                        {errors.stability_data_type}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              )}

                              {this.checkAuditSiteName(
                                taskDetails.request_type,
                                taskDetails.parent_id
                              ) === true && (
                                <Col xs={12} sm={6} md={3}>
                                  <div className="form-group">
                                    <label>
                                      Site Name{" "}
                                      <span class="required-field">*</span>
                                    </label>

                                    <Field
                                      name="audit_visit_site_name"
                                      type="text"
                                      className="form-control"
                                      autoComplete="off"
                                      value={
                                        values.audit_visit_site_name !== null &&
                                        values.audit_visit_site_name !== ""
                                          ? values.audit_visit_site_name
                                          : ""
                                      }
                                    ></Field>

                                    {errors.audit_visit_site_name &&
                                    touched.audit_visit_site_name ? (
                                      <span className="errorMsg">
                                        {errors.audit_visit_site_name}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              )}

                              {this.checkAuditVistDate(
                                taskDetails.request_type,
                                taskDetails.parent_id
                              ) === true && (
                                <Col xs={12} sm={6} md={3}>
                                  <div className="form-group">
                                    <label>
                                      Audit/Visit Date{" "}
                                      <span class="required-field">*</span>
                                    </label>
                                    <div className="form-control">
                                      {/* <DatePicker
                                                            name="request_audit_visit_date"
                                                            onChange={value =>
                                                                setFieldValue("request_audit_visit_date",value)
                                                            }
                                                            value={values.request_audit_visit_date}
                                                        /> */}

                                      <DatePicker
                                        name="request_audit_visit_date"
                                        className="borderNone"
                                        selected={
                                          values.request_audit_visit_date
                                            ? values.request_audit_visit_date
                                            : null
                                        }
                                        onChange={(value) =>
                                          setFieldValue(
                                            "request_audit_visit_date",
                                            value
                                          )
                                        }
                                        dateFormat="dd/MM/yyyy"
                                        autoComplete="off"
                                      />
                                    </div>
                                    {errors.request_audit_visit_date &&
                                    touched.request_audit_visit_date ? (
                                      <span className="errorMsg">
                                        {errors.request_audit_visit_date}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              )}

                              {this.state.not_approved_tasks.length > 0 && this.showUnApprovedTasks()}

                              {taskDetails.parent_id === 0 && (
                                <Col xs={12}>
                                  <div className="form-group">
                                    <label>Requirement</label>
                                    {/* <Field
                                                                            name="content"
                                                                            component="textarea"
                                                                            className="form-control"
                                                                            autoComplete="off"
                                                                            value={(values.content !== null && values.content !== "") ? values.content : ''}
                                                                        >
                                                                        </Field> */}

                                    <Editor
                                      name="content"
                                      value={
                                        values.content !== null &&
                                        values.content !== ""
                                          ? values.content
                                          : ""
                                      }
                                      content={
                                        values.content !== null &&
                                        values.content !== ""
                                          ? values.content
                                          : ""
                                      }
                                      init={{
                                        menubar: false,
                                        branding: false,
                                        placeholder: "Enter requirements",
                                        plugins:
                                          "link table hr visualblocks code placeholder lists autoresize textcolor",
                                        toolbar:
                                          "bold italic strikethrough superscript subscript | forecolor backcolor | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | formatselect | visualblocks code | fontselect",
                                        font_formats:
                                          "Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats",
                                      }}
                                      onEditorChange={(value) =>
                                        setFieldValue("content", value)
                                      }
                                    />
                                    {errors.content && touched.content ? (
                                      <span className="errorMsg">
                                        {errors.content}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              )}

                              {taskDetails.request_type === 1 && (
                                <Col xs={12}>
                                  <div className="form-group">
                                    <label>
                                      Shipping Address{" "}
                                      <span class="required-field">*</span>
                                    </label>
                                    {/* <Field
                                                                        name="shipping_address"
                                                                        component="textarea"
                                                                        className="form-control"
                                                                        autoComplete="off"
                                                                        value={(values.shipping_address !== null && values.shipping_address !== "") ? values.shipping_address : ''}
                                                                    >
                                                                    </Field> */}

                                    <Editor
                                      name="shipping_address"
                                      value={
                                        values.shipping_address !== null &&
                                        values.shipping_address !== ""
                                          ? values.shipping_address
                                          : ""
                                      }
                                      content={
                                        values.shipping_address !== null &&
                                        values.shipping_address !== ""
                                          ? values.shipping_address
                                          : ""
                                      }
                                      init={{
                                        menubar: false,
                                        branding: false,
                                        placeholder: "Enter Shipping Address",
                                        plugins:
                                          "link table hr visualblocks code placeholder lists autoresize textcolor",
                                        toolbar:
                                          "bold italic strikethrough superscript subscript | forecolor backcolor | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | formatselect | visualblocks code ",
                                      }}
                                      onEditorChange={(value) =>
                                        setFieldValue("shipping_address", value)
                                      }
                                    />

                                    {errors.shipping_address &&
                                    touched.shipping_address ? (
                                      <span className="errorMsg">
                                        {errors.shipping_address}
                                      </span>
                                    ) : null}
                                  </div>
                                </Col>
                              )}

                              <Col xs={12} className="spec-moa-customer">
                                <label>
                                  External Email notification{` `}
                                  <LinkWithTooltipText
                                    tooltip={`Select list of users within Customer’s organization who will receive email notification`}
                                    href="#"
                                    id={`tooltip-menu`}
                                    clicked={(e) => this.checkHandler(e)}
                                  >
                                    <img
                                      src={exclamationImage}
                                      alt="exclamation"
                                    />
                                  </LinkWithTooltipText>
                                </label>
                                <div className="customers-group">
                                  <div className="customers-drodpwn mr-10">
                                    <Select
                                      isMulti
                                      className="basic-single"
                                      classNamePrefix="select"
                                      defaultValue={this.state.ccSelCust}
                                      isClearable={true}
                                      isSearchable={true}
                                      maxMenuHeight={110}
                                      name="ccCustId"
                                      options={this.state.ccCust}
                                      onChange={(e) => {
                                        this.changeCCCust(e, setFieldValue);
                                      }}
                                    />
                                    {errors.shipping_address &&
                                    touched.shipping_address ? (
                                      <span className="errorMsg">
                                        {errors.shipping_address}
                                      </span>
                                    ) : null}
                                  </div>

                                  <Button
                                    onClick={this.closeBrowserWindow}
                                    className={`btn-line`}
                                    type="button"
                                  >
                                    Cancel
                                  </Button>
                                  <Button
                                    className="btn-fill ml-10"
                                    type="submit"
                                  >
                                    {(this.state.taskDetails.request_type === 23 && this.state.taskDetails.order_verified_status == 0)?`Process Order`:`Update`}
                                  </Button>
                                </div>
                              </Col>
                            </Row>
                          </Form>
                        );
                      }}
                    </Formik>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </>
      );
    } else {
      return (
        <>
          <div className="loderOuter">
            <div className="loader">
              <img src={loaderlogo} alt="logo" />
              <div className="loading">Loading...</div>
            </div>
          </div>
        </>
      );
    }
  }
}

export default TeamEditTaskDetails;
