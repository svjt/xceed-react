import React, { Component } from "react";
import { Row, Col, ButtonToolbar, Button, Modal, Alert } from "react-bootstrap";

//import { FilePond } from 'react-filepond';
//import 'filepond/dist/filepond.min.css';
import Dropzone from "react-dropzone";

import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import dateFormat from "dateformat";
import DatePicker from "react-datepicker";
import { htmlDecode } from "../../shared/helper";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";
import API from "../../shared/axios";
import { showErrorMessageFront } from "../../shared/handle_error_front";

import swal from "sweetalert";

import { localDate, trimString, inArray } from "../../shared/helper";

import { Editor } from "@tinymce/tinymce-react";

//import uploadAxios from 'axios';

import TinyMCE from "react-tinymce";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";

const initialValues = {
  employeeId: "",
  comment: "",
  breach_reason: "",
  file: "",
  dueDate: "",
  showModalLoader: false,
  file_name: [],
};

//console.log( TinyMCE );

// const onlyUnique = (value, index, self) => {
//     return self.indexOf(value) === index;
// }

const removeDropZoneFiles = (fileName, objRef, setErrors) => {
  var newArr = [];
  for (let index = 0; index < objRef.state.files.length; index++) {
    const element = objRef.state.files[index];

    if (fileName === element.name) {
    } else {
      newArr.push(element);
    }
  }

  var fileListHtml = newArr.map((file) => (
    <Alert key={file.name}>
      <span onClick={() => removeDropZoneFiles(file.name, objRef, setErrors)}>
        <i className="far fa-times-circle"></i>
      </span>{" "}
      {file.name}
    </Alert>
  ));
  setErrors({ file_name: "" });
  objRef.setState({
    files: newArr,
    filesHtml: fileListHtml,
  });
};

class IncreaseSlaPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showIncreaseSla: false,
      files: [],
      sla_breach: false,
      showModalLoader: false,
      stateEmplId: "",
      acceptNewEmp: false,
      acceptNewEmpMsg: "",
      filesHtml: "",
      sub_tasks: [],
      checked_sub: true,
      commentApi: "",
    };
  }

  handleChange = (e, field) => {
    this.setState({
      [field]: e.target.value,
    });
  };

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.showIncreaseSla === true && nextProps.currRow.task_id > 0) {
      API.get(
        `/api/my_team_tasks/get_sub_tasks/${nextProps.currRow.task_id}/${nextProps.currRow.assigned_to}`
      )
        .then((res) => {
          this.setState({
            showIncreaseSla: nextProps.showIncreaseSla,
            currRow: nextProps.currRow,
            sub_tasks: res.data.data,
            commentApi: res.data.comment,
          });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  handleClose = () => {
    var close = {
      sla_breach: false,
      stateEmplId: "",
      new_employee: false,
      msg_new_employee: "",
      showModalLoader: false,
      showIncreaseSla: false,
      files: [],
      filesHtml: "",
    };
    this.setState(close);
    this.props.handleClose(close);
  };

  handleIncreaseSLAPopup = (values, actions) => {
    this.setState({ showModalLoader: true, msg_new_employee: false });

    var back_due_date = dateFormat(values.back_due_date, "yyyy-mm-dd");

    var formData = new FormData();

    formData.append("back_due_date", back_due_date);
    formData.append("assignment_id", this.state.currRow.assignment_id);
    formData.append("comment", values.comment);
    formData.append(
      "sub_tasks",
      this.state.sub_tasks.length > 0
        ? JSON.stringify(this.state.sub_tasks)
        : JSON.stringify([])
    );
    formData.append("posted_for", this.state.currRow.assigned_to);

    API.post(
      `/api/my_team_tasks/increase_sla/${this.state.currRow.task_id}`,
      formData
    )
      .then((res) => {
        this.handleClose();
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: "Due dates have been updated.",
          icon: "success",
        }).then(() => {
          this.setState({ showModalLoader: false });
          this.props.reloadTask();
        });
      })
      .catch((error) => {
        this.setState({ showModalLoader: false });
        if (error.data.status === 3) {
          var token_rm = 2;
          showErrorMessageFront(error, token_rm, this.props);
          this.handleClose();
        } else {
          actions.setErrors(error.data.errors);
          actions.setSubmitting(false);
        }
      });
  };

  getBackDueDate = () => {
    if (typeof this.state.currRow !== "undefined") {
      let given_date = this.state.currRow.new_due_date;
      return localDate(given_date);
    }
  };

  changeBackDate = (event, setFieldValue) => {
    //event.preventDefault();
    if (event === null) {
      setFieldValue("back_due_date", this.getBackDueDate());
    } else {
      setFieldValue("back_due_date", event);
    }
  };

  changeSubBackDate = (event, setFieldValue, name) => {
    console.log(event, setFieldValue, name);
    //event.preventDefault();
    if (event === null) {
    } else {
      let tempTaskArr = [];

      // update checked property to customer list
      this.state.sub_tasks.map((sub_tasks, i) => {
        if (`back_due_date_${sub_tasks.task_id}` === name) {
          sub_tasks.assign_due_date = dateFormat(event, "yyyy-mm-dd HH:MM:ss");
        }
        tempTaskArr.push(sub_tasks);
      });

      // // update state
      this.setState({
        sub_tasks: tempTaskArr,
      });

      //setFieldValue(name, event);
    }
  };

  selectSubTaskChecked = (event, setFieldValue, name) => {
    this.setState({ checked_sub: false });
    let tempTaskArr = [];

    // update checked property to customer list
    this.state.sub_tasks.map((sub_tasks, i) => {
      if (`sub_task_refs_${sub_tasks.task_id}` === event.target.name) {
        sub_tasks.checked = event.target.checked;
      }
      tempTaskArr.push(sub_tasks);
    });
    // // update state
    this.setState({
      sub_tasks: tempTaskArr,
      checked_sub: true,
    });

    setFieldValue(name, event.target.checked);
  };

  render() {
    const newInitialValues = Object.assign(initialValues, {
      employeeId: "",
      comment: this.state.commentApi,
      breach_reason: "",
      back_due_date: this.getBackDueDate(),
    });

    const validateStopFlag = Yup.object().shape({
      comment: Yup.string().trim().required("Please enter comment"),
      back_due_date: Yup.date().required("Due date is required"),
    });

    //console.log(this.state.employeeArr)

    return (
      <>
        <Modal
          show={this.state.showIncreaseSla}
          onHide={() => this.handleClose()}
          backdrop="static"
        >
          <Formik
            initialValues={newInitialValues}
            validationSchema={validateStopFlag}
            onSubmit={this.handleIncreaseSLAPopup}
          >
            {({
              values,
              errors,
              touched,
              isValid,
              isSubmitting,
              handleChange,
              setFieldValue,
              setFieldTouched,
              setErrors,
            }) => {
              return (
                <Form encType="multipart/form-data">
                  {this.state.showModalLoader === true ? (
                    <div className="loderOuter">
                      <div className="loader">
                        <img src={loaderlogo} alt="logo" />
                        <div className="loading">Loading...</div>
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                  <Modal.Header closeButton>
                    <Modal.Title>Increase SLA</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <div className="contBox">
                      <div className="form-group react-date-picker react-date-picker-with-0pos">
                        {Date.parse(new Date(this.state.currRow.new_due_date)) >
                          Date.parse(
                            new Date(
                              this.state.currRow.original_assignment_due_date
                            )
                          ) && (
                          <i style={{ "font-size": "12px" }}>
                            Original Due Date:{" "}
                            {dateFormat(
                              this.state.currRow.original_assignment_due_date,
                              "dd/mm/yyyy h:MM TT"
                            )}
                          </i>
                        )}
                        <div className="row">
                          <label className="col-md-5 m-t-10">
                            Select the due date for the main task
                          </label>
                          <div className="col-md-4">
                            <div className="form-control">
                              <DatePicker
                                name={"back_due_date"}
                                className="borderNone"
                                dayClassName={(date) =>
                                  date.getDay() === 0 || date.getDay() === 6
                                    ? "disabled-date"
                                    : undefined
                                }
                                minDate={localDate(
                                  this.state.currRow.new_due_date
                                )}
                                selected={
                                  values.back_due_date
                                    ? values.back_due_date
                                    : null
                                }
                                dateFormat="dd/MM/yyyy"
                                autoComplete="off"
                                onChange={(e) => {
                                  this.changeBackDate(e, setFieldValue);
                                }}
                              />
                            </div>
                          </div>
                        </div>

                        {errors.back_due_date && touched.back_due_date ? (
                          <p className="errorMsgPos1">{errors.back_due_date}</p>
                        ) : null}
                      </div>

                      {this.state.sub_tasks.length > 0 &&
                        this.state.checked_sub === true && (
                          <Row>
                            <Col
                              xs={12}
                              sm={6}
                              md={6}
                              className="form-group mb-15"
                            >
                              <label>
                                Select the due date for the sub task/s.
                              </label>

                              {this.state.sub_tasks.map((sub_tasks, index) => {
                                return (
                                  <div key={index} className="row">
                                    <div className="col-md-6">
                                      <div className="form-group checText mb-15 m-t-10">
                                        <input
                                          type="checkbox"
                                          name={`sub_task_refs_${sub_tasks.task_id}`}
                                          value={sub_tasks.task_id}
                                          checked={sub_tasks.checked}
                                          onChange={(e) => {
                                            this.selectSubTaskChecked(
                                              e,
                                              setFieldValue,
                                              `sub_task_refs_${sub_tasks.task_id}`
                                            );
                                          }}
                                        />
                                        <label>
                                          {htmlDecode(sub_tasks.task_ref)}
                                        </label>
                                      </div>
                                    </div>
                                    {sub_tasks.checked && (
                                      <div className="col-md-6">
                                        {Date.parse(
                                          new Date(sub_tasks.assign_due_date)
                                        ) >
                                          Date.parse(
                                            new Date(
                                              sub_tasks.original_assignment_due_date
                                            )
                                          ) && (
                                          <i style={{ "font-size": "12px" }}>
                                            (Original Due Date:{" "}
                                            {dateFormat(
                                              sub_tasks.original_assignment_due_date,
                                              "dd/mm/yyyy h:MM TT"
                                            )}
                                            )
                                          </i>
                                        )}
                                        <div className="form-group mb-0 react-date-picker react-date-picker-with-0pos">
                                          <div className="form-control react">
                                            <DatePicker
                                              name={`back_due_date_${sub_tasks.task_id}`}
                                              className="borderNone"
                                              dayClassName={(date) =>
                                                date.getDay() === 0 ||
                                                date.getDay() === 6
                                                  ? "disabled-date"
                                                  : undefined
                                              }
                                              minDate={localDate(
                                                sub_tasks.initial_due_date
                                              )}
                                              selected={localDate(
                                                sub_tasks.assign_due_date
                                              )}
                                              //minDate={this.setMinDateLoop(sub_tasks.initial_due_date)}
                                              //selected={this.setDate(sub_tasks.assign_due_date)}
                                              dateFormat="dd/MM/yyyy"
                                              autoComplete="off"
                                              onChange={(e) => {
                                                this.changeSubBackDate(
                                                  e,
                                                  setFieldValue,
                                                  `back_due_date_${sub_tasks.task_id}`
                                                );
                                              }}
                                            />
                                          </div>
                                        </div>
                                      </div>
                                    )}
                                    <div className="clearFix"></div>
                                  </div>
                                );
                              })}
                            </Col>
                          </Row>
                        )}

                      {errors.assignedEmployee || touched.assignedEmployee ? (
                        <span className="errorMsg">
                          {errors.assignedEmployee}
                        </span>
                      ) : null}

                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <label>
                              Comment <span className="required-field">*</span>
                            </label>

                            <Editor
                              name="content"
                              className={`selectArowGray form-control`}
                              value={
                                values.comment !== null && values.comment !== ""
                                  ? values.comment
                                  : ""
                              }
                              content={
                                values.comment !== null && values.comment !== ""
                                  ? values.comment
                                  : ""
                              }
                              init={{
                                menubar: false,
                                branding: false,
                                placeholder: "Enter comments",
                                plugins:
                                  "link table hr visualblocks code placeholder lists autoresize textcolor",
                                toolbar:
                                  "bold italic strikethrough superscript subscript | forecolor backcolor | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | visualblocks code | fontselect",
                                font_formats:
                                  "Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats",
                              }}
                              onEditorChange={(value) =>
                                setFieldValue("comment", value)
                              }
                            />
                            {errors.comment && touched.comment ? (
                              <span className="errorMsg">{errors.comment}</span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </Modal.Body>
                  <Modal.Footer>
                    <button
                      onClick={this.handleClose}
                      className={`btn-line`}
                      type="button"
                    >
                      Close
                    </button>
                    <button
                      className={`btn-fill ${
                        isValid ? "btn-custom-green" : "btn-disable"
                      } m-r-10`}
                      type="submit"
                      disabled={isValid ? false : true}
                    >
                      {this.state.stopflagId > 0
                        ? isSubmitting
                          ? "Updating..."
                          : "Update"
                        : isSubmitting
                        ? "Submitting..."
                        : "Submit"}
                    </button>
                  </Modal.Footer>
                </Form>
              );
            }}
          </Formik>
        </Modal>
      </>
    );
  }
}

export default IncreaseSlaPopup;
