import React, { Component } from 'react';
import { Row, Col, ButtonToolbar, Button, Modal } from "react-bootstrap";
import Select from "react-select";
import API from "../../shared/axios";
import { htmlDecode } from '../../shared/helper';

class MyCompanyPopup extends Component {

    state = {
        employeeArr: [],
        companyArr:[],
        showCompany:false,
        text:'View Users'
    }

    constructor(props){
        super(props);
    }

    componentWillMount = () => {
        API.get('/api/team/my_team_members')
        .then(res => {
            var empArr = [];
            for (let index = 0; index < res.data.data.length; index++) {
                const element = res.data.data[index];
                empArr.push({
                    value: element["employee_id"],
                    label: element["first_name"] +" "+ element["last_name"]
                });
            }
            this.setState({employeeArr:empArr});
        })
        .catch(err => {
            console.log(err);
        });
    }

    changeEmployee = (event) =>{
        console.log(event.value);
        

        API.get(`/api/team/employee_company/${event.value}`)
        .then(res => {
            var myCompany = [];
            for (let index = 0; index < res.data.data.length; index++) {
                const element = res.data.data[index];
                myCompany.push({
                    value: element["company_id"],
                    label: htmlDecode(element["company_name"])
                });
            }
            this.setState({companyArr:myCompany,emp_id:event.value,showCompany:true});
        })
        .catch(err => {
            console.log(err);
        });

    }

    getEmployeeComanyCustomer(event){
        this.props.history.push(`/user/my_team_customer/${this.state.emp_id}/${event.value}`);
    }

    handleClose = () => {
        var close = {employeeArr:[],companyArr:[],showCompany:false};
        this.setState(close);
        this.props.closePopup();
    };

    render() {
        return (
            <>
                {this.state.employeeArr.length > 0 && <Modal show={true} onHide={() => this.handleClose()} backdrop="static">
                   <div className="customer-modal-edit-35">
                    <Modal.Header closeButton>
                        <Modal.Title>
                        {this.state.text}
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="contBox">
                            <Row>
                                <Col xs={12} sm={12} md={12}>
                                <div className="form-group">
                                    <label>Select Employees</label>
                                    <Select
                                        className="basic-single"
                                        classNamePrefix="select"
                                        defaultValue={0}
                                        isClearable={false}
                                        isSearchable={true}
                                        name="employee_id"
                                        options={this.state.employeeArr}
                                        onChange={e => { this.changeEmployee(e) }}
                                    />
                                </div>

                                {this.state.showCompany && 
                                this.state.companyArr.length > 0 && 
                                <div className="form-group">
                                    <label>Select Customers</label>
                                    <Select
                                        className="basic-single"
                                        classNamePrefix="select"
                                        defaultValue={0}
                                        isClearable={false}
                                        isSearchable={true}
                                        name="company_id"
                                        options={this.state.companyArr}
                                        onChange={e => { this.getEmployeeComanyCustomer(e) }}
                                    />
                                </div>}

                                </Col>
                            </Row>
                        </div>
                    </Modal.Body>
                    </div>
                    </Modal>
                    }
                </>
        );
    }
}

export default MyCompanyPopup;