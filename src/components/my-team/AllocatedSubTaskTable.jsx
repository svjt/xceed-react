import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { getMyDrupalId, localDate, htmlDecode } from "../../shared/helper";
import { showErrorMessageFront } from "../../shared/handle_error_front";
import base64 from "base-64";

import dateFormat from "dateformat";

import StatusColumn from "./StatusColumn";
import { Link } from "react-router-dom";

import API from "../../shared/axios";

import exclamationImage from "../../assets/images/exclamation-icon.svg";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
import swal from "sweetalert";
//import { getMyId } from '../../shared/helper';
import AssignAssignedPopup from "./AssignAssignedPopup";
import RespondBackAssignedPopup from "./RespondBackAssignedPopup";
import ReAssignAssignedPopup from "./ReAssignAssignedPopup";
import Poke from "./Poke";

import ReactHtmlParser from "react-html-parser";

import VerticalMenuAllocatedTask from "./VerticalMenuAllocatedTask";

import { Tooltip, OverlayTrigger } from "react-bootstrap";
import "./Dashboard.css";

const portal_url = `${process.env.REACT_APP_PORTAL_URL}`; // SATYAJIT

const priority_arr = [
  { priority_id: 1, priority_value: "Low" },
  { priority_id: 2, priority_value: "Medium" },
  { priority_id: 3, priority_value: "High" },
];

const getStatusColumn = (refObj) => (cell, row) => {
  if (row.discussion !== 1) {
    return <StatusColumn rowData={row} />;
  } else {
    return "";
  }
};

const setCompanyName = (refObj) => (cell, row) => {
  return htmlDecode(cell);
};

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const setDescription = (refOBj) => (cell, row) => {
  if (row.parent_id > 0) {
    let title = htmlDecode(row.title);
    let stripHtml = title.replace(/<[^>]+>/g, "");
    return (
      <LinkWithTooltip
        tooltip={`${ReactHtmlParser(stripHtml)}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {ReactHtmlParser(stripHtml)}
      </LinkWithTooltip>
    );
  } else if (row.discussion === 1) {
    let comment = htmlDecode(row.comment);
    let stripHtml = comment.replace(/<[^>]+>/g, "");
    return (
      <LinkWithTooltip
        tooltip={`${ReactHtmlParser(stripHtml)}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {ReactHtmlParser(stripHtml)}
      </LinkWithTooltip>
    );
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${row.req_name}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {row.req_name}
      </LinkWithTooltip>
    );
  }
};

const setBlank = (refObj) => (cell, row) => {
  return "";
};

const setCreateDate = (refObj) => (cell) => {
  var date = localDate(cell);
  return dateFormat(date, "dd/mm/yyyy");
};

const setDaysPending = (refOBj) => (cell, row) => {
  if (row.discussion === 1) {
    return "";
  } else {
    return (
      <LinkWithTooltip
        tooltip={cell}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {cell}
      </LinkWithTooltip>
    );
  }
};

const hoverDate = (refOBj) => (cell, row) => {
  return (
    <LinkWithTooltip
      tooltip={cell}
      href="#"
      id="tooltip-1"
      clicked={(e) => refOBj.checkHandler(e)}
    >
      {cell}
    </LinkWithTooltip>
  );
};

const clickToShowTasks = (refObj) => (cell, row) => {
  if (row.discussion === 1) {
    return (
      <LinkWithTooltip
        tooltip={`${cell}`}
        href="#"
        id="tooltip-1"
        clicked={(e) =>
          refObj.redirectUrlTaskDiss(
            e,
            row.task_id,
            refObj.props.assignId,
            refObj.props.postedFor
          )
        }
      >
        {cell}
      </LinkWithTooltip>
    );
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${cell}`}
        href="#"
        id="tooltip-1"
        clicked={(e) =>
          refObj.redirectUrlTask(
            e,
            row.task_id,
            row.assignment_id,
            row.assigned_by
          )
        }
      >
        {cell}
      </LinkWithTooltip>
    );
  }
};

const setEmpCustName = (refObj) => (cell, row) => {
  if (row.customer_drupal_id > 0) {
    //hard coded customer id - SATYAJIT
    //row.customer_id = 2;
    return (
      /*<Link
        to={{
          pathname:
            portal_url + "customer-dashboard/" +
            row.customer_drupal_id
        }}
        target="_blank"
        style={{ cursor: "pointer" }}
      >*/
      <LinkWithTooltip
        tooltip={`${row.first_name + " " + row.last_name}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refObj.checkHandler(e)}
      >
        {row.first_name + " " + row.last_name}
      </LinkWithTooltip>
      /*</Link>*/
    );
  } else {
    return "";
  }
};

const setAssignedTo = (refOBj) => (cell, row) => {
  //return row.emp_first_name+' '+row.emp_last_name+' ('+row.desig_name+')';
  if (row.discussion === 1) {
    return "";
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${
          row.at_emp_first_name +
          " " +
          row.at_emp_last_name +
          " (" +
          row.at_emp_desig_name +
          ")"
        }`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {row.at_emp_first_name +
          " " +
          row.at_emp_last_name +
          " (" +
          row.at_emp_desig_name +
          ")"}
      </LinkWithTooltip>
    );
  }
};

const setAssignedBy = (refOBj) => (cell, row) => {
  if (row.discussion === 1) {
    return "";
  } else {
    if (row.assigned_by === -1) {
      return (
        <LinkWithTooltip
          tooltip={`System`}
          href="#"
          id="tooltip-1"
          clicked={(e) => refOBj.checkHandler(e)}
        >
          System
        </LinkWithTooltip>
      );
    } else {
      return (
        <LinkWithTooltip
          tooltip={`${
            row.ab_emp_first_name +
            " " +
            row.ab_emp_last_name +
            " (" +
            row.ab_emp_desig_name +
            ")"
          }`}
          href="#"
          id="tooltip-1"
          clicked={(e) => refOBj.checkHandler(e)}
        >
          {row.ab_emp_first_name +
            " " +
            row.ab_emp_last_name +
            " (" +
            row.ab_emp_desig_name +
            ")"}
        </LinkWithTooltip>
      );
    }
  }
};

const getBreach = (refOBj) => (cell, row) => {
  var breach = false;
  if (row.breach_reason !== null && row.breach_reason !== "") {
    breach = true;
  }

  return (
    <>
      {/* <div className="actionStyle"> */}
      <div className="btn-group">
        {breach && (
          <LinkWithTooltip
            tooltip={`${htmlDecode(row.breach_reason)}`}
            href="#"
            id={`tooltip-menu-${row.task_id}`}
            clicked={(e) => this.checkHandler(e)}
          >
            <img src={exclamationImage} alt="exclamation" />
          </LinkWithTooltip>
        )}
      </div>
      {/* </div> */}
    </>
  );
};

const getActions = (refObj) => (cell, row) => {
  return (
    <VerticalMenuAllocatedTask
      id={cell}
      currRow={row}
      showReAssignCQTPopup={refObj.showReAssignCQTPopup}
      showAssignPopup={refObj.showAssignPopup}
      showReAssignPopup={refObj.showReAssignPopup}
      showSubTaskPopup={refObj.showSubTaskPopup}
      showRespondCustomerPopup={refObj.showRespondCustomerPopup}
      showCloseTaskPopup={refObj.showCloseTaskPopup}
      showRespondAssignPopup={refObj.showRespondAssignPopup}
      showAuthorizeTaskPopup={refObj.showAuthorizeTaskPopup}
      teamArr={refObj.props.teamData}
      poke={refObj.showPoke}
    />
  );
};

class AllocatedSubTaskTable extends Component {
  state = {
    showCreateSubTask: false,
    showAssign: false,
    showReAssign: false,
    showCQTReAssign: false,
    showRespondBack: false,
    showModalLoader: false,
  };

  checkHandler = (event) => {
    event.preventDefault();
  };

  redirectUrl = (event, id) => {
    event.preventDefault();
    //http://reddy.indusnet.cloud/customer-dashboard?source=Mi02NTE=
    var emp_drupal_id = getMyDrupalId(localStorage.token);
    var base_encode = base64.encode(`${id}-${emp_drupal_id}`);
    window.open(
      portal_url + "customer-dashboard?source=" + base_encode,
      "_blank"
    );
  };

  redirectUrlTask = (event, task_id, assignment_id, posted_for) => {
    event.preventDefault();
    window.open(
      `/user/team_task_details/${task_id}/${assignment_id}/${posted_for}`,
      "_blank"
    );
  };

  redirectUrlTaskDiss = (event, task_id, assignment_id, posted_for) => {
    event.preventDefault();
    window.open(
      `/user/team_task_details/${task_id}/${assignment_id}/${posted_for}/#discussion`,
      "_blank"
    );
  };

  showCloseTaskPopup = (currRow) => {
    swal({
      closeOnClickOutside: false,
      title: "Close task",
      text: "Are you sure you want to close this task?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.setState({ showModalLoader: true });
        API.delete(
          `/api/my_team_tasks/check_close/${currRow.task_id}/${currRow.assigned_by}`
        )
          .then((res) => {
            this.setState({ showModalLoader: false });
            if (res.data.has_sub_task === 1) {
              swal({
                closeOnClickOutside: false,
                title: "Alert",
                text:
                  "This task has some open sub-tasks. \r\n Do you want to close them as well?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              }).then((willDelete) => {
                if (willDelete) {
                  this.setState({ showModalLoader: true });
                  API.delete(
                    `/api/my_team_tasks/close/${currRow.task_id}/${currRow.assigned_by}`
                  )
                    .then((res) => {
                      this.setState({ showModalLoader: false });
                      swal({
                        closeOnClickOutside: false,
                        title: "Success",
                        text: "Task has been closed!",
                        icon: "success",
                      }).then(() => {
                        this.props.reloadTaskSubTask();
                      });
                    })
                    .catch((err) => {
                      var token_rm = 2;
                      showErrorMessageFront(err, token_rm, this.props);
                    });
                }
              });
            } else {
              swal({
                closeOnClickOutside: false,
                title: "Success",
                text: "Task has been closed!",
                icon: "success",
              }).then(() => {
                this.props.reloadTaskSubTask();
              });
            }
          })
          .catch((err) => {
            var token_rm = 2;
            showErrorMessageFront(err, token_rm, this.props);
          });
      }
    });
  };

  showSubTaskAssignPopup = (id) => {
    this.setState({ showAssignSubTask: true, task_id: id });
  };

  showPoke = (currRow) => {
    this.setState({ showPoke: true, currRow: currRow });
  };

  showRespondAssignPopup = (currRow) => {
    console.log("Respond");
    this.setState({ showRespondBack: true, currRow });
  };

  showAssignPopup = (currRow) => {
    console.log("Assign Task");
    this.setState({ showAssign: true, currRow: currRow });
  };

  showReAssignPopup = (currRow) => {
    console.log("Re Assign Task");
    this.setState({ showReAssign: true, currRow: currRow });
  };

  showReAssignCQTPopup = (currRow) => {
    console.log("Re Assign CQT Task", currRow);
    this.setState({ showCQTReAssign: true, currRow: currRow });
  };

  showSubTaskRespondPopup = (id) => {
    this.setState({ showRespondSubTask: true, task_id: id });
  };

  refreshTable = () => {
    this.props.refreshTable();
  };

  handleClose = (closeObj) => {
    this.setState(closeObj);
  };

  tdClassName = (fieldValue, row) => {
    var dynamicClass = "width-150 ";
    if (row.vip_customer === 1) {
      dynamicClass += "bookmarked-column ";
    }
    return dynamicClass;
  };

  render() {
    const selectRowProp = {
      bgColor: "#fff8f6",
    };

    return (
      <>
        {this.state.showModalLoader === true ? (
          <div className="loderOuter">
            <div className="loader">
              <img src={loaderlogo} alt="logo" />
              <div className="loading">Loading...</div>
            </div>
          </div>
        ) : (
          ""
        )}
        <BootstrapTable
          data={this.props.tableData}
          selectRow={selectRowProp}
          tableHeaderClass={"col-hidden"}
          expandColumnOptions={{
            expandColumnVisible: true,
            expandColumnComponent: this.expandColumnComponent,
            columnWidth: 25,
          }}
          trClassName="tr-expandable"
        >
          <TableHeaderColumn
            isKey
            dataField="task_ref"
            dataSort={true}
            columnClassName={this.tdClassName}
            editable={false}
            dataFormat={clickToShowTasks(this)}
          >
            Tasks
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="request_type"
            dataSort={true}
            editable={false}
            dataFormat={setDescription(this)}
          >
            Description
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="request_type"
            dataSort={true}
            editable={false}
            dataFormat={setBlank(this)}
          >
            Description
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="display_date_added"
            dataSort={true}
            editable={false}
            dataFormat={hoverDate(this)}
          >
            Created
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="display_assign_date"
            //dataSort={true}
            editable={false}
            expandable={false}
            dataFormat={hoverDate(this)}
          >
            Assigned Date
          </TableHeaderColumn>

          {/* <TableHeaderColumn dataField='due_date' dataSort={ true } editable={ false }  dataFormat={ setCreateDate(this) } >Due Date</TableHeaderColumn> */}

          <TableHeaderColumn
            dataField="display_new_due_date"
            dataSort={true}
            editable={false}
            dataFormat={setDaysPending(this)}
          >
            Due Date
          </TableHeaderColumn>

          {/* <TableHeaderColumn dataField='assigned_to' dataSort={ true } editable={ false }  >Assigned To</TableHeaderColumn> */}

          <TableHeaderColumn
            dataField="dept_name"
            //dataSort={true}
            editable={false}
            expandable={false}
            dataFormat={setAssignedBy(this)}
          >
            Assigned By{" "}
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="dept_name"
            dataSort={true}
            editable={false}
            expandable={false}
            dataFormat={setAssignedTo(this)}
          >
            Assigned To{" "}
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="company_name"
            //dataSort={true}
            expandable={false}
            dataFormat={setCompanyName(this)}
          >
            Customer
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="cust_name"
            dataSort={true}
            editable={false}
            dataFormat={setEmpCustName(this)}
          >
            Customer Name
          </TableHeaderColumn>

          {/* <TableHeaderColumn dataField='dept_name' dataSort={ true } editable={ false } expandable={ false } >Department</TableHeaderColumn> */}

          <TableHeaderColumn
            dataField="status"
            dataFormat={getStatusColumn(this)}
            editable={false}
          >
            Status
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="task_id"
            dataFormat={getActions(this)}
            expandable={false}
            editable={false}
          />
        </BootstrapTable>

        <RespondBackAssignedPopup
          showRespondBack={this.state.showRespondBack}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.reloadTaskSubTask()}
        />

        <AssignAssignedPopup
          showAssign={this.state.showAssign}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.reloadTaskSubTask()}
        />

        <ReAssignAssignedPopup
          showReAssign={this.state.showReAssign}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.reloadTaskSubTask()}
        />

        <Poke
          from={`allocated`}
          showPoke={this.state.showPoke}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.reloadTaskSubTask()}
        />
      </>
    );
  }
}

export default AllocatedSubTaskTable;
