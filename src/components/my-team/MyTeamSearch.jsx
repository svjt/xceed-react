import React, { Component } from "react";

import API from "../../shared/axios";

//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";

import Select from "react-select";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import dateFormat from "dateformat";
import { htmlDecode } from "../../shared/helper";
import { Link } from "react-router-dom";

import { Row, Col, Tooltip, OverlayTrigger } from "react-bootstrap";

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="right"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

class BMDashboardSearch extends Component {
  state = {
    search_loader: false,
    my_customer_list: null,
    showCustomerBlock: false,
    sla_arr: [
      { value: 1, label: "Due Today" },
      { value: 3, label: "Due Tomorrow" },
      { value: 4, label: " Due This Week" },
      { value: 5, label: " Due Next Week" },
      { value: 2, label: "Overdue" },
    ],
    sla_task_arr: [
      { value: "1", label: "Normal SLA" },
      { value: "2", label: "SLA Increased" },
      { value: "3", label: "SLA Paused" },
    ],
    params: [],
    show_deleted_tasks: false,
    show_dummy_users: false,
  };

  componentDidMount() {
    API.get(`/api/team/my_team_members`)
      .then((res) => {
        var myEmployee = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          myEmployee.push({
            value: element["employee_id"],
            label: element["first_name"] + " " + element["last_name"],
          });
        }

        this.setState({ my_employee_list: myEmployee });

        API.get(`/api/feed/request_type`)
          .then((res) => {
            var requestTypes = [];
            for (let index = 0; index < res.data.data.length; index++) {
              const element = res.data.data[index];
              requestTypes.push({
                value: element["type_id"],
                label: element["req_name"],
              });
            }

            this.setState({ request_type: requestTypes });

            API.get(`/api/feed/designations`)
              .then((res) => {
                var desigTypes = [];
                for (let index = 0; index < res.data.data.length; index++) {
                  const element = res.data.data[index];
                  desigTypes.push({
                    value: element["desig_id"],
                    label: element["desig_name"],
                  });
                }
                this.setState({ designations: desigTypes, is_loading: false });

                API.get("/api/team/l1_companies")
                  .then((res) => {
                    var myCompany = [];
                    for (let index = 0; index < res.data.data.length; index++) {
                      const element = res.data.data[index];
                      myCompany.push({
                        value: element["company_id"],
                        label: htmlDecode(element["company_name"]),
                      });
                    }

                    this.setState({ company: myCompany });

                    API.get(`/api/feed/products`)
                      .then((res) => {
                        var productsList = [];
                        for (
                          let index = 0;
                          index < res.data.data.length;
                          index++
                        ) {
                          const element = res.data.data[index];
                          productsList.push({
                            value: element["product_id"],
                            label: element["product_name"],
                          });
                        }

                        this.setState({
                          product_list: productsList,
                          is_loading: false,
                          params: [{ name: "sla_arr", val: `0,0,0` }],
                        });
                      })
                      .catch((err) => {
                        console.log(err);
                      });
                  })
                  .catch((err) => {
                    console.log(err);
                  });
              })
              .catch((err) => {
                console.log(err);
              });
          })
          .catch((err) => {
            console.log(err);
          });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  filterSearch = (event, nextEV) => {
    this.setState({
      search_loader: true,
    });
    var currIndex = null;
    if (event == null) {
      for (let index = 0; index < this.state.params.length; index++) {
        const element = this.state.params[index];

        if (element.name === nextEV.name) {
          currIndex = index;
        }
      }
      if (currIndex !== null) {
        this.state.params.splice(currIndex, 1);
      }
    } else {
      for (let index = 0; index < this.state.params.length; index++) {
        const element = this.state.params[index];
        if (element.name === nextEV.name) {
          currIndex = index;
        }
      }

      if (currIndex == null) {
        this.state.params.push({ name: nextEV.name, val: event.value });
      } else {
        const myCurrIndex = this.state.params[currIndex];
        myCurrIndex.val = event.value;
      }
    }

    this.callApi();
  };

  filterWords = (event) => {
      var code = event.keyCode || event.which;
      if (code === 13) {
          this.setState({
            search_loader: true,
          });
          var currIndex = null;
          var value_ref = event.target.value.trim().toUpperCase();
          if (value_ref.length == 0) {
            for (let index = 0; index < this.state.params.length; index++) {
              const element = this.state.params[index];
              if (element.name === "task_ref") {
                currIndex = index;
              }
            }
            if (currIndex !== null) {
              this.state.params.splice(currIndex, 1);
            }
          } else {
            for (let index = 0; index < this.state.params.length; index++) {
              const element = this.state.params[index];
              if (element.name === "task_ref") {
                currIndex = index;
              }
            }
      
            if (currIndex == null) {
              this.state.params.push({ name: "task_ref", val: value_ref });
            } else {
              const myCurrIndex = this.state.params[currIndex];
              myCurrIndex.val = value_ref;
            }
          }
      
          this.callApi();
      }
  };

  filterSearchMulti = (event, nextEV) => {
    this.setState({
      search_loader: true,
    });
    var currIndex = null;
    if (event.length == 0) {
      for (let index = 0; index < this.state.params.length; index++) {
        const element = this.state.params[index];
        if (element.name === nextEV.name) {
          currIndex = index;
        }
      }
      if (currIndex !== null) {
        this.state.params.splice(currIndex, 1);
      }
      this.companyCustomers(nextEV.name, "");
    } else {
      for (let index = 0; index < this.state.params.length; index++) {
        const element = this.state.params[index];
        if (element.name === nextEV.name) {
          currIndex = index;
        }
      }

      var multi_val = this.getMultiValueString(event);

      if (currIndex == null) {
        this.state.params.push({ name: nextEV.name, val: multi_val });
      } else {
        const myCurrIndex = this.state.params[currIndex];
        myCurrIndex.val = multi_val;
      }
      this.companyCustomers(nextEV.name, multi_val);
    }

    this.callApi();
  };

  filterSearchMultiSLA = (event, nextEV) => {
    this.setState({
      search_loader: true,
    });
    let currIndex;

    for (let index = 0; index < this.state.params.length; index++) {
      const element = this.state.params[index];
      if (element.name === "sla_arr") {
        currIndex = index;
        break;
      }
    }

    this.state.params.splice(currIndex, 1);

    let normal = 0;
    let increased = 0;
    let paused = 0;

    for (let index = 0; index < event.length; index++) {
      const element = event[index];

      if (element.value === "1") {
        normal = 1;
      }

      if (element.value === "2") {
        increased = 1;
      }
      if (element.value === "3") {
        paused = 1;
      }
    }
    // console.log(`${normal},${increased}`);
    this.state.params.push({
      name: "sla_arr",
      val: `${normal},${increased},${paused}`,
    });

    this.callApi();
  };

  getMultiValueString = (selectedValues) => {
    var string = "";
    for (let index = 0; index < selectedValues.length; index++) {
      const element = selectedValues[index];
      string += `${element.value},`;
    }
    string = string.replace(/,+$/, "");
    return string;
  };

  companyCustomers = (name, val) => {
    // console.log(name, val);
    if (name === "compid") {
      if (val === "") {
        var currIndex;
        for (let index = 0; index < this.state.params.length; index++) {
          const element = this.state.params[index];
          if (element.name === "cid") {
            currIndex = index;
            break;
          }
        }
        if (currIndex >= 0) {
          this.state.params.splice(currIndex, 1);
        }
        this.setState({ showCustomerBlock: false, my_customer_list: [] });
      } else {
        API.get(`/api/team/my_team_customers?compid=${val}`)
          .then((res) => {
            var myCustomer = [];
            for (let index = 0; index < res.data.data.length; index++) {
              const element = res.data.data[index];
              var keyCustomer;
              if (element.vip_customer === 1) {
                keyCustomer = "*";
              } else {
                keyCustomer = "";
              }
              myCustomer.push({
                value: element["customer_id"],
                label:
                  element["first_name"] +
                  " " +
                  element["last_name"] +
                  " " +
                  keyCustomer,
              });
            }

            this.setState({
              showCustomerBlock: true,
              my_customer_list: myCustomer,
            });
          })
          .catch((err) => {
            console.log(err);
          });
      }
    }
  };

  filterDateFrom = (value) => {
    this.setState({ date_from: value, search_loader: true });
    var currIndex = null;
    for (let index = 0; index < this.state.params.length; index++) {
      const element = this.state.params[index];
      if (element.name === "date_from") {
        currIndex = index;
      }
    }

    if (currIndex == null) {
      this.state.params.push({
        name: "date_from",
        val: dateFormat(value, "yyyy-mm-dd"),
      });
    } else {
      const myCurrIndex = this.state.params[currIndex];
      myCurrIndex.val = dateFormat(value, "yyyy-mm-dd");
    }

    this.callApi();
  };

  filterDateTo = (value) => {
    this.setState({ date_to: value, search_loader: true });
    var currIndex = null;
    for (let index = 0; index < this.state.params.length; index++) {
      const element = this.state.params[index];
      if (element.name === "date_to") {
        currIndex = index;
      }
    }

    if (currIndex == null) {
      this.state.params.push({
        name: "date_to",
        val: dateFormat(value, "yyyy-mm-dd"),
      });
    } else {
      const myCurrIndex = this.state.params[currIndex];
      myCurrIndex.val = dateFormat(value, "yyyy-mm-dd");
    }

    this.callApi();
  };

  removeDateTo = () => {
    this.setState({ date_to: null, search_loader: true });
    var currIndex = null;
    for (let index = 0; index < this.state.params.length; index++) {
      const element = this.state.params[index];

      if (element.name === "date_to") {
        currIndex = index;
      }
    }
    if (currIndex !== null) {
      this.state.params.splice(currIndex, 1);
    }

    this.callApi();
  };

  removeDateFrom = () => {
    this.setState({ date_from: null, search_loader: true });
    var currIndex = null;
    for (let index = 0; index < this.state.params.length; index++) {
      const element = this.state.params[index];

      if (element.name === "date_from") {
        currIndex = index;
      }
    }
    if (currIndex !== null) {
      this.state.params.splice(currIndex, 1);
    }

    this.callApi();
  };

  showDeletedTasks = (event) => {
    let show_del_task = event.target.checked;
    this.setState({ show_deleted_tasks: show_del_task, search_loader: true });
    var currIndex = null;

    for (let index = 0; index < this.state.params.length; index++) {
      const element = this.state.params[index];
      if (element.name === "show_deleted") {
        currIndex = index;
      }
    }
    let param_del_task;
    if (show_del_task) {
      param_del_task = 1;
    } else {
      param_del_task = 0;
    }

    if (currIndex == null) {
      this.state.params.push({ name: "show_deleted", val: param_del_task });
    } else {
      const myCurrIndex = this.state.params[currIndex];
      myCurrIndex.val = param_del_task;
    }

    this.callApi();
  };

  showDummyTasks = (event) => {
    let show_del_task = event.target.checked;
    this.setState({ show_dummy_users: show_del_task, search_loader: true });
    var currIndex = null;

    for (let index = 0; index < this.state.params.length; index++) {
      const element = this.state.params[index];
      if (element.name === "show_dummy") {
        currIndex = index;
      }
    }
    let param_del_task;
    if (show_del_task) {
      param_del_task = 1;
    } else {
      param_del_task = 0;
    }

    if (currIndex == null) {
      this.state.params.push({ name: "show_dummy", val: param_del_task });
    } else {
      const myCurrIndex = this.state.params[currIndex];
      myCurrIndex.val = param_del_task;
    }

    this.callApi();
  };

  displayUnreadTask = (event) => {
    this.setState({
      show_unread_task: event.target.checked,
      search_loader: true,
    });
    var currIndex = null;

    for (let index = 0; index < this.state.params.length; index++) {
      const element = this.state.params[index];
      if (element.name === "show_unread") {
        currIndex = index;
      }
    }
    let param_del_task;
    if (event.target.checked) {
      param_del_task = 1;
    } else {
      param_del_task = 0;
    }

    if (currIndex == null) {
      this.state.params.push({ name: "show_unread", val: param_del_task });
    } else {
      const myCurrIndex = this.state.params[currIndex];
      myCurrIndex.val = param_del_task;
    }

    this.callApi();
  };

  callApi = () => {
    var retArr = [];
    var page = 1;
    if (this.state.params.length > 0) {
      var query_string = "";
      for (let index = 0; index < this.state.params.length; index++) {
        const element = this.state.params[index];
        query_string += `${element.name}=${element.val}&`;
      }
      query_string = query_string.substring(0, query_string.length - 1);
      API.get(`/api/team/open?page=${page}&${query_string}`)
        .then((res) => {
          retArr.push({ openTasks: res.data.data });
          if (res.data.count_open_tasks === undefined) {
            retArr.push({ countOpenTasks: 0 });
          } else {
            retArr.push({ countOpenTasks: res.data.count_open_tasks });
          }

          API.get(`/api/team/closed?page=${page}&${query_string}`)
            .then((res) => {
              retArr.push({ closedTasks: res.data.data });
              if (res.data.count_closed_tasks === undefined) {
                retArr.push({ countClosedTasks: 0 });
              } else {
                retArr.push({ countClosedTasks: res.data.count_closed_tasks });
              }

              API.get(`/api/team/allocated?page=${page}&${query_string}`)
                .then((res) => {
                  retArr.push({ allocatedTasks: res.data.data });
                  if (res.data.count_all_allocated_tasks === undefined) {
                    retArr.push({ countAllocatedTasks: "0" });
                  } else {
                    retArr.push({
                      countAllocatedTasks: res.data.count_all_allocated_tasks,
                    });
                  }
                  retArr.push({ queryString: query_string });
                  this.setState({
                    search_loader: false,
                  });
                  this.props.changeState(retArr);
                })
                .catch((err) => {});
            })
            .catch((err) => {});
        })
        .catch((err) => {});
    } else {
      API.get(`/api/team/open?page=${page}`)
        .then((res) => {
          retArr.push({ openTasks: res.data.data });
          if (res.data.count_open_tasks === undefined) {
            retArr.push({ countOpenTasks: 0 });
          } else {
            retArr.push({ countOpenTasks: res.data.count_open_tasks });
          }
          API.get(`/api/team/closed?page=${page}`)
            .then((res) => {
              retArr.push({ closedTasks: res.data.data });
              if (res.data.count_closed_tasks === undefined) {
                retArr.push({ countClosedTasks: 0 });
              } else {
                retArr.push({ countClosedTasks: res.data.count_closed_tasks });
              }

              API.get(`/api/team/allocated?page=${page}`)
                .then((res) => {
                  retArr.push({ allocatedTasks: res.data.data });
                  if (res.data.count_all_allocated_tasks === undefined) {
                    retArr.push({ countAllocatedTasks: "0" });
                  } else {
                    retArr.push({
                      countAllocatedTasks: res.data.count_all_allocated_tasks,
                    });
                  }
                  retArr.push({ queryString: "" });
                  this.setState({
                    search_loader: false,
                  });
                  this.props.changeState(retArr);
                })
                .catch((err) => {});
            })
            .catch((err) => {});
        })
        .catch((err) => {});
    }
  };

  checkHandler = (event) => {
    event.preventDefault();
  };

  render() {
    return (
      <>
        {this.state.search_loader === true && (
          <>
            <div className="loderOuter">
              <div className="loader">
                <img src={loaderlogo} alt="logo" />
                <div className="loading">Loading...</div>
              </div>
            </div>
          </>
        )}

        {/* <input type="checkbox" defaultChecked={this.state.show_deleted_tasks} onChange={(e)=>this.showDeletedTasks(e)} /> Display Deleted Tasks
            <br/>
            <input type="checkbox" defaultChecked={this.state.show_dummy_users} onChange={(e)=>this.showDummyTasks(e)} /> Display Dummy Users */}
        <div className="mb-15">
          <label class="customCheckBox">
            Display Cancelled Tasks
            <input
              type="checkbox"
              defaultChecked={this.state.show_deleted_tasks}
              onChange={(e) => this.showDeletedTasks(e)}
            />
            <span class="checkmark-check"></span>
          </label>

          <label class="customCheckBox">
            Display Inactive Users
            <input
              type="checkbox"
              defaultChecked={this.state.show_dummy_users}
              onChange={(e) => this.showDummyTasks(e)}
            />
            <span class="checkmark-check"></span>
          </label>
          {` `}

          <LinkWithTooltip
            tooltip={`List of users who has not yet activated account.`}
            href="#"
            id="tooltip-1"
            clicked={(e) => this.checkHandler(e)}
          >
            <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
          </LinkWithTooltip>

          <label className="customCheckBox">
            Display Unread Tasks
            <input
              type="checkbox"
              defaultChecked={this.state.show_unread_task}
              onChange={(e) => this.displayUnreadTask(e)}
            />
            <span className="checkmark-check"></span>
          </label>
        </div>

        <ul>
          <li>
            <div className="form-group">
              <label>PO / Ref No</label>
              <input
                type="text"
                name="ref_no"
                className="form-control"
                onKeyPress={this.filterWords}
                style={{ textTransform: "uppercase" }}
              />
            </div>
          </li>

          {this.state.request_type && (
            <li>
              <div className="form-group  has-feedback">
                <label>Request Type</label>
                <Select
                  isMulti
                  className="basic-single"
                  classNamePrefix="select"
                  defaultValue={0}
                  isClearable={true}
                  isSearchable={true}
                  name="rid"
                  options={this.state.request_type}
                  onChange={this.filterSearchMulti}
                />
              </div>
            </li>
          )}

          {this.state.product_list && (
            <li>
              <div className="form-group  has-feedback">
                <label>Products</label>
                <Select
                  isMulti
                  className="basic-single"
                  classNamePrefix="select"
                  defaultValue={0}
                  isClearable={true}
                  isSearchable={true}
                  name="prodid"
                  options={this.state.product_list}
                  onChange={this.filterSearchMulti}
                />
              </div>
            </li>
          )}

          {this.state.company && (
            <li>
              <div className="form-group  has-feedback">
                <label>Customer</label>
                <Select
                  isMulti
                  className="basic-single"
                  classNamePrefix="select"
                  defaultValue={0}
                  isClearable={true}
                  isSearchable={true}
                  name="compid"
                  options={this.state.company}
                  onChange={this.filterSearchMulti}
                />
              </div>
            </li>
          )}

          {this.state.showCustomerBlock && (
            <li>
              <div className="form-group  has-feedback">
                <label>User</label>
                <Select
                  isMulti
                  className="basic-single"
                  classNamePrefix="select"
                  defaultValue={0}
                  isClearable={true}
                  isSearchable={true}
                  name="cid"
                  options={this.state.my_customer_list}
                  onChange={this.filterSearchMulti}
                />
              </div>
            </li>
          )}

          {this.state.sla_task_arr && (
            <li>
              <div className="form-group  has-feedback">
                <label>SLA Status</label>
                <Select
                  isMulti
                  className="basic-single"
                  classNamePrefix="select"
                  isClearable={true}
                  isSearchable={true}
                  name="sla_arr"
                  //defaultValue={this.state.sla_task_arr}
                  options={this.state.sla_task_arr}
                  onChange={this.filterSearchMultiSLA}
                />
              </div>
            </li>
          )}

          {this.state.my_employee_list && (
            <li>
              <div className="form-group  has-feedback">
                <label>Employees</label>
                <Select
                  isMulti
                  className="basic-single"
                  classNamePrefix="select"
                  defaultValue={0}
                  isClearable={true}
                  isSearchable={true}
                  name="eid"
                  options={this.state.my_employee_list}
                  onChange={this.filterSearchMulti}
                />
              </div>
            </li>
          )}

          {this.state.designations && (
            <li>
              <div className="form-group  has-feedback">
                <label>Functions</label>
                <Select
                  isMulti
                  className="basic-single"
                  classNamePrefix="select"
                  defaultValue={0}
                  isClearable={true}
                  isSearchable={true}
                  name="desig"
                  options={this.state.designations}
                  onChange={this.filterSearchMulti}
                />
              </div>
            </li>
          )}

          <li>
            <div className="form-group react-date-picker">
              <label>From</label>
              <div className="form-control">
                <DatePicker
                  name={"date_from"}
                  className="borderNone"
                  dateFormat="dd/MM/yyyy"
                  autoComplete="off"
                  selected={this.state.date_from}
                  onChange={this.filterDateFrom}
                />
              </div>
              {this.state.date_from && (
                <button onClick={this.removeDateFrom} className="date-close">
                  x
                </button>
              )}
            </div>
          </li>

          <li>
            <div className="form-group react-date-picker">
              <label>To</label>
              <div className="form-control">
                <DatePicker
                  name={"date_to"}
                  className="borderNone"
                  dateFormat="dd/MM/yyyy"
                  autoComplete="off"
                  selected={this.state.date_to}
                  onChange={this.filterDateTo}
                />
              </div>
              {this.state.date_to && (
                <button onClick={this.removeDateTo} className="date-close">
                  x
                </button>
              )}
            </div>
          </li>

          {this.state.sla_arr && (
            <li>
              <div className="form-group  has-feedback">
                <label>Due On</label>
                <Select
                  className="basic-single"
                  classNamePrefix="select"
                  defaultValue={0}
                  isClearable={true}
                  isSearchable={true}
                  name="sla"
                  options={this.state.sla_arr}
                  onChange={this.filterSearch}
                />
              </div>
            </li>
          )}
        </ul>
      </>
    );
  }
}

export default BMDashboardSearch;
