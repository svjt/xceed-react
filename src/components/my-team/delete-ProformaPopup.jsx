import React, { Component } from "react";
import { Row, Col, ButtonToolbar, Button, Modal, Alert } from "react-bootstrap";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import TinyMCE from "react-tinymce";
import API from "../../shared/axios";
import swal from "sweetalert";
import Select from "react-select";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
import { showErrorMessageFront } from "../../shared/handle_error_front";
import { htmlDecode, localDate } from "../../shared/helper";
import DatePicker from "react-datepicker";
// import { Editor } from "@tinymce/tinymce-react";
import dateFormat from "dateformat";
import { Link } from "react-router-dom";

let initialValues_india = {
  drl_manufacture: "",
  invoice_date: "",
  consignee: "",
  buyer: "",
  terms_of_delivery: "",
  terms_of_payment: "",
  net_weight: 0,
  currency: "",
  price: 0,
  amount: 0,
  description: "",
  po_number: "",
  igst: 18,
  stateTax1: 9,
  stateTax2: 9,
  select_destination: { value: "1", label: "Within India" },
};

let initialValues_other = {
  drl_manufacture: "",
  invoice_date: "",
  consignee: "",
  buyer: "",
  freight: "",
  country_beneficiary: "",
  partial_shipment: "",
  origin_country: "",
  destination_country: "",
  terms_of_delivery: "",
  terms_of_payment: "",
  transport_mode: "",
  airport_discharge: "",
  packing: "",
  net_weight: 0,
  currency: "",
  price: 0,
  amount: 0,
  description: "",
  select_destination: { value: "1", label: "Within India" },
  po_number: "",
};

const validationSchema_india = Yup.object().shape({
  drl_manufacture: Yup.string().trim()
    .typeError("Please select drl manufacturing details")
    .required("Please select drl manufacturing details"),
  invoice_date: Yup.string().trim().required("Please enter proforma invoice date"),
  consignee: Yup.string().trim().required("Please enter consignee details"),
  buyer: Yup.string().trim().required("Please enter buyer details"),
  terms_of_delivery: Yup.string().trim()
    .typeError("Please select terms of delivery")
    .required("Please select terms of delivery"),
  terms_of_payment: Yup.string().trim()
    .typeError("Please select terms of payment")
    .required("Please select terms of payment"),
  net_weight: Yup.number()
    .typeError("net weight must be a number")
    .positive("net weight must be greater than zero")
    .required("Please enter net weight"),
  currency: Yup.string().trim()
    .typeError("Please select currency")
    .required("Please select currency"),
  price: Yup.number()
    .typeError("price must be a number")
    .positive("price must be greater than zero")
    .required("Please enter price"),

  igst: Yup.number().when("select_destination", {
    is: (select_destination) => select_destination.value === "1",
    then: Yup.number()
      .typeError("IGST must be a number")
      .positive("IGST must be greater than zero")
      .required("Please enter IGST"),
  }),

  stateTax1: Yup.number().when("select_destination", {
    is: (select_destination) => select_destination.value === "3",
    then: Yup.number()
      .typeError("IGST must be a number")
      .positive("IGST must be greater than zero")
      .required("Please enter IGST"),
  }),

  stateTax2: Yup.number().when("select_destination", {
    is: (select_destination) => select_destination.value === "3",
    then: Yup.number()
      .typeError("SGST must be a number")
      .positive("SGST must be greater than zero")
      .required("Please enter SGST"),
  }),
});

const validationSchema_other = Yup.object().shape({
  drl_manufacture: Yup.string().trim()
    .typeError("Please select drl manufacturing details")
    .required("Please select drl manufacturing details"),
  invoice_date: Yup.string().trim().required("Please enter proforma invoice date"),
  consignee: Yup.string().trim().required("Please enter consignee details"),
  buyer: Yup.string().trim().required("Please enter buyer details"),
  terms_of_delivery: Yup.string().trim()
    .typeError("Please select terms of delivery")
    .required("Please select terms of delivery"),
  terms_of_payment: Yup.string().trim()
    .typeError("Please select terms of payment")
    .required("Please select terms of payment"),
  net_weight: Yup.number()
    .typeError("net weight must be a number")
    .positive("net weight must be greater than zero")
    .required("Please enter net weight"),
  currency: Yup.string().trim().required("Please select currency"),
  price: Yup.number()
    .typeError("price must be a number")
    .positive("price must be greater than zero")
    .required("Please enter price"),
  country_beneficiary: Yup.string().trim().required(
    "Please select country of beneficiary"
  ),
  partial_shipment: Yup.string().trim().required("Please enter partial shipment"),
  origin_country: Yup.string().trim().required("Please enter country of origin"),
  destination_country: Yup.string().trim().required(
    "Please enter country of destination"
  ),
  transport_mode: Yup.string().trim().required("Please select transport mode"),
  airport_discharge: Yup.string().trim().required(
    "Please enter port/airport discharge"
  ),
  packing: Yup.string().trim().required("Please enter packing"),
});

const warningValues = {};
const warningSchema = {};

class ProformaPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showProformaForm: false,
      terms_of_payment_list: [],
      terms_of_delivery_list: [],
      drl_manufacture_list: [],
      transport_mode_list: [
        { value: "By Air", label: "By Air" },
        { value: "By Sea", label: "By Sea" },
        { value: "By Road", label: "By Road" },
      ],
      currency_list: [
        { value: "INR", label: "INR" },
        { value: "EUR", label: "EUR" },
        { value: "USD", label: "USD" },
      ],
      select_destination_list: [
        { value: "1", label: "Within India" },
        { value: "2", label: "Outside India" },
        { value: "3", label: "Within State (India)" },
      ],
      netWeight: 0,
      priceValue: 0,
      beforeSubmit: false,
      invoiceStartDate: new Date(),
      within_india: "1",
      igst: 18,
      stateTax1: 9,
      stateTax2: 9,
    };
  }

  componentDidMount = () => {
    API.get(`/api/feed/sap_payment_terms`)
      .then((res) => {
        var terms_of_payment_list = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          terms_of_payment_list.push({
            value: element["code"],
            label: htmlDecode(element["description"]),
          });
        }
        this.setState({ terms_of_payment_list: terms_of_payment_list });
      })
      .catch((err) => {
        console.log(err);
      });

    API.get(`/api/feed/sap_delivery_terms`)
      .then((res) => {
        var terms_of_delivery_list = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          terms_of_delivery_list.push({
            value: element["code"],
            label: htmlDecode(element["description"]),
          });
        }
        this.setState({ terms_of_delivery_list: terms_of_delivery_list });
      })
      .catch((err) => {
        console.log(err);
      });

    API.get(`/api/feed/drl_manufacturing_unit`)
      .then((res) => {
        this.setState({ drl_manufacture_list: res.data.data });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.showProformaForm === true && nextProps.currRow.task_id > 0) {
      this.setState({
        showProformaForm: nextProps.showProformaForm,
        currRow: nextProps.currRow,
      });
    }
  };

  handleClose = () => {
    var close = { showProformaForm: false };
    this.setState(close);
    this.props.handleClose(close);
    this.setState({ within_india: "1" });
  };

  closeWarning = () => {
    this.setState({ beforeSubmit: false });
  };

  changeDestination = (selDestination, setFieldValue, setFieldTouched) => {
    if (selDestination === null) {
      setFieldValue("select_destination", {
        value: "1",
        label: "Within India",
      });
      setFieldTouched("select_destination");
      this.setState({ within_india: "1" });
    } else {
      setFieldValue("select_destination", selDestination);
      setFieldTouched("select_destination");
      this.setState({ within_india: selDestination.value });
    }
  };

  proformaSubmit = (values, actions) => {
    let invoiceDate = dateFormat(values.invoice_date, "yyyy-mm-dd");

    if (
      values.select_destination.value.toString() === "1" ||
      values.select_destination.value.toString() === "3"
    ) {
      var formData = {
        amount: this.state.netWeight * this.state.priceValue,
        buyer: values.buyer,
        consignee: values.consignee,
        currency: values.currency.value,
        description: values.description,
        drl_manufacture: values.drl_manufacture.value,
        invoice_date: invoiceDate,
        net_weight: values.net_weight,
        price: values.price,
        terms_of_delivery: values.terms_of_delivery.value,
        terms_of_payment: values.terms_of_payment.value,
        within_india: values.select_destination.value,
        igst:
          values.select_destination.value.toString() === "1" ? values.igst : 0,
        stateTax1:
          values.select_destination.value.toString() === "3"
            ? values.stateTax1
            : 0,
        stateTax2:
          values.select_destination.value.toString() === "3"
            ? values.stateTax2
            : 0,
        po_number: values.po_number,
      };
    } else {
      var formData = {
        amount: this.state.netWeight * this.state.priceValue,
        airport_discharge: values.airport_discharge,
        buyer: values.buyer,
        consignee: values.consignee,
        country_beneficiary: values.country_beneficiary,
        currency: values.currency.value,
        description: values.description,
        freight: values.freight,
        drl_manufacture: values.drl_manufacture.value,
        destination_country: values.destination_country,
        invoice_date: invoiceDate,
        net_weight: values.net_weight,
        origin_country: values.origin_country,
        packing: values.packing,
        partial_shipment: values.partial_shipment,
        price: values.price,
        terms_of_delivery: values.terms_of_delivery.value,
        terms_of_payment: values.terms_of_payment.value,
        transport_mode: values.transport_mode.value,
        within_india: values.within_india,
        po_number: values.po_number,
      };
    }

    //API.post(`/api/my_team_tasks/proforma_invoice/${this.state.currRow.task_id}`,formData)

    API.post(
      `/api/tasks/proforma_invoice/${this.state.currRow.task_id}`,
      formData
    )
      .then((res) => {
        this.closeMainPopup();
        let prev_state = this.state.currRow;
        prev_state.file_name = res.data.file_name;
        prev_state.download_url = res.data.download_url;
        prev_state.select_genpact = 1;
        prev_state.proforma_id = res.data.proforma_id;

        this.setState({
          currRowNew: prev_state,
          beforeSubmit: true,
          download_url: res.data.download_url,
          file_name: res.data.file_name,
          netWeight: 0,
          priceValue: 0,
          within_india: 1,
        });
        // this.setState({ showModalLoader: false });
        // this.handleClose();
        // swal({
        //     closeOnClickOutside: false,
        //     title: "Success",
        //     text: "Response sent to customer.",
        //     icon: "success"
        // }).then(() => {
        //     this.props.reloadTask();
        // });
      })
      .catch((error) => {
        console.log(error);
        // this.setState({ showModalLoader: false });
        // if(error.data.status === 3){
        //     this.handleClose();
        //     var token_rm = 2;
        //     showErrorMessageFront(error,token_rm,this.props);
        // }else{
        //     //console.log(error.data.errors);
        //     actions.setErrors(error.data.errors);
        //     actions.setSubmitting(false);
        // }
      });

    //this.setState({ beforeSubmit: true })
  };

  warningSubmit = (values, action) => {
    console.log("values ===>", values);
  };

  handleInvoiceDate = (event, setFieldValue, setFieldTouched) => {
    if (event === null) {
      setFieldValue("invoice_date", new Date());
    } else {
      setFieldValue("invoice_date", event);
    }
  };

  handleWeight = (e, setFieldValue) => {
    let value = e.target.value;
    setFieldValue("net_weight", value);
    this.setState({ netWeight: value > 0 ? value : 0 });
  };

  handlePrice = (e, setFieldValue) => {
    let value = e.target.value;
    setFieldValue("price", value);
    this.setState({ priceValue: value > 0 ? value : 0 });
  };

  downloadFile = (e, file_url, file_name) => {
    e.preventDefault();
    let a = document.createElement("a");
    a.href = file_url;
    a.download = file_name;
    a.click();
  };

  previewFile = (e, file_name) => {
    e.preventDefault();
    let a = document.createElement("a");
    a.href = `${process.env.REACT_APP_API_URL}/proforma_invoice/${file_name}`;
    a.target = "_blank";
    a.click();
  };

  closeMainPopup = () => {
    this.setState({ showProformaForm: false });
  };

  showSubtaskPopup = (e, currRow) => {
    e.preventDefault();
    this.props.handleClose({ showProformaForm: false, beforeSubmit: false });
    this.setState({ showProformaForm: false, beforeSubmit: false });
    //this.props.showSubTaskPopup(currRow);
    this.props.showRespondCustomer(currRow);
  };

  textAreaOnchange = (e, setFieldValue, setFieldTouched) => {
    setFieldValue(e.target.name, e.target.value);
    setFieldTouched(e.target.name);
  };

  handleIGST = (e, setFieldValue) => {
    setFieldValue(e.target.name, e.target.value);
    this.setState({ igst: e.target.value });
  };

  handleStateTax1 = (e, setFieldValue) => {
    setFieldValue(e.target.name, e.target.value);
    this.setState({ stateTax1: e.target.value });
  };

  handleStateTax2 = (e, setFieldValue) => {
    setFieldValue(e.target.name, e.target.value);
    this.setState({ stateTax2: e.target.value });
  };

  render() {
    let newInitialValues = null,
      newValidationSchema = null;

    if (this.state.within_india === "2") {
      newInitialValues = Object.assign({}, initialValues_other);
      newValidationSchema = validationSchema_other;
    } else {
      newInitialValues = Object.assign({}, initialValues_india);
      newValidationSchema = validationSchema_india;
    }

    // calculate total amount
    let total_amount = this.state.netWeight * this.state.priceValue;
    total_amount = total_amount.toFixed(2);
    let gross_amount = total_amount; // set to display
    let igst_amount = 0;
    let sgst_amount = 0;

    if (this.state.within_india !== "2") {
      if (this.state.within_india === "1") {
        let with_tax = (total_amount * this.state.igst) / 100;
        with_tax = with_tax.toFixed(2);
        total_amount = parseFloat(total_amount) + parseFloat(with_tax);
        total_amount = total_amount.toFixed(2);
        igst_amount = with_tax; // set to display
      } else {
        let withtax1 = (total_amount * this.state.stateTax1) / 100;
        withtax1 = withtax1.toFixed(2);
        igst_amount = withtax1; // set to display

        let withtax2 = (total_amount * this.state.stateTax2) / 100;
        withtax2 = withtax2.toFixed(2);
        sgst_amount = withtax2; // set to display

        let total_tax = parseFloat(withtax1) + parseFloat(withtax2);
        total_amount = parseFloat(total_amount) + parseFloat(total_tax);
        total_amount = total_amount.toFixed(2);
      }
    }

    return (
      <>
        <Modal
          show={this.state.showProformaForm}
          onHide={() => this.handleClose()}
          backdrop="static"
          className="respondBackpop"
        >
          <Formik
            initialValues={newInitialValues}
            validationSchema={newValidationSchema}
            onSubmit={this.proformaSubmit}
          >
            {({
              values,
              errors,
              touched,
              isValid,
              isSubmitting,
              setFieldValue,
              setFieldTouched,
              setErrors,
            }) => {
              return (
                <Form>
                  {this.state.showProformaFormLoader === true ? (
                    <div className="loderOuter">
                      <div className="loader">
                        <img src={loaderlogo} alt="logo" />
                        <div className="loading">Loading...</div>
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                  <Modal.Header closeButton>
                    <Modal.Title>Create Proforma Invoice</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <div className="contBox">
                      <Row>
                        <Col xs={6} sm={6} md={6}>
                          <div className="form-group">
                            <label>
                              DRL manufacturing details{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              defaultValue={values.drl_manufacture}
                              name="drl_manufacture"
                              options={this.state.drl_manufacture_list}
                              value={values.drl_manufacture}
                              isClearable={true}
                              onChange={(value) => {
                                setFieldValue("drl_manufacture", value);
                                setFieldTouched("drl_manufacture");
                              }}
                            />

                            {errors.drl_manufacture &&
                            touched.drl_manufacture ? (
                              <span className="errorMsg">
                                {errors.drl_manufacture}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                        <Col xs={6} sm={6} md={6}>
                          <div className="form-group react-date-picker">
                            <label>
                              Proforma Invoice Date{" "}
                              <span className="required-field">*</span>
                            </label>
                            <DatePicker
                              name={"invoice_date"}
                              className={`selectArowGray form-control`}
                              selected={
                                values.invoice_date ? values.invoice_date : null
                              }
                              dateFormat="dd/MM/yyyy"
                              autoComplete="off"
                              value={values.invoice_date}
                              onChange={(e) => {
                                this.handleInvoiceDate(e, setFieldValue);
                              }}
                            />
                            {errors.invoice_date && touched.invoice_date ? (
                              <span className="errorMsg">
                                {errors.invoice_date}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <label>
                              Consignee Name & Address{" "}
                              <span className="required-field">*</span>
                            </label>
                            {/* <Field
                                            name="consignee"
                                            type="textarea"
                                            rows="4"
                                            cols="50"
                                            className='form-control'
                                            autoComplete="off"
                                            value={values.consignee}
                                        >
                                        </Field> */}

                            <textarea
                              name="consignee"
                              rows="4"
                              cols="50"
                              className="form-control"
                              value={values.consignee}
                              onChange={(e) =>
                                this.textAreaOnchange(
                                  e,
                                  setFieldValue,
                                  setFieldTouched
                                )
                              }
                            ></textarea>
                            {errors.consignee && touched.consignee ? (
                              <span className="errorMsg">
                                {errors.consignee}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>

                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <label>
                              Buyer Name & Address{" "}
                              <span className="required-field">*</span>
                            </label>
                            {/* <Field
                                            name="buyer"
                                            type="textarea"
                                            className={`selectArowGray form-control`}
                                            autoComplete="off"
                                            value={values.buyer}
                                        > 
                                        </Field> */}

                            <textarea
                              name="buyer"
                              rows="4"
                              cols="50"
                              className="form-control"
                              value={values.buyer}
                              onChange={(e) =>
                                this.textAreaOnchange(
                                  e,
                                  setFieldValue,
                                  setFieldTouched
                                )
                              }
                            ></textarea>
                            {errors.buyer && touched.buyer ? (
                              <span className="errorMsg">{errors.buyer}</span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs={6} sm={6} md={6}>
                          <div className="form-group">
                            <label>
                              Consignment destination{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              defaultValue={this.state.within_india}
                              name="select_destination"
                              options={this.state.select_destination_list}
                              value={values.select_destination}
                              isClearable={true}
                              onChange={(value) =>
                                this.changeDestination(
                                  value,
                                  setFieldValue,
                                  setFieldTouched
                                )
                              }
                            />

                            {errors.select_destination &&
                            touched.select_destination ? (
                              <span className="errorMsg">
                                {errors.select_destination}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                        <Col xs={6} sm={6} md={6}>
                          <div className="form-group">
                            <label>PO Number</label>
                            <Field
                              name="po_number"
                              type="text"
                              className={`selectArowGray form-control`}
                              autoComplete="off"
                              value={values.po_number}
                            ></Field>
                            {errors.po_number && touched.po_number ? (
                              <span className="errorMsg">
                                {errors.po_number}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>

                      {this.state.within_india === "2" && (
                        <Row>
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>
                                Freight Forwarder Name & Address{" "}
                                <span className="required-field">*</span>
                              </label>
                              {/* <Field
                                                name="freight"
                                                type="textarea"
                                                className={`selectArowGray form-control`}
                                                autoComplete="off"
                                                value={values.freight}
                                            > 
                                            </Field> */}

                              <textarea
                                name="freight"
                                rows="4"
                                cols="50"
                                className="form-control"
                                value={values.freight}
                                onChange={(e) =>
                                  this.textAreaOnchange(
                                    e,
                                    setFieldValue,
                                    setFieldTouched
                                  )
                                }
                              ></textarea>

                              {errors.freight && touched.freight ? (
                                <span className="errorMsg">
                                  {errors.freight}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                        </Row>
                      )}

                      {this.state.within_india === "2" && (
                        <Row>
                          <Col xs={6} sm={6} md={6}>
                            <div className="form-group">
                              <label>
                                Country of beneficiary{" "}
                                <span className="required-field">*</span>
                              </label>
                              <Field
                                name="country_beneficiary"
                                type="text"
                                className={`selectArowGray form-control`}
                                autoComplete="off"
                                value={values.country_beneficiary}
                              ></Field>
                              {errors.country_beneficiary &&
                              touched.country_beneficiary ? (
                                <span className="errorMsg">
                                  {errors.country_beneficiary}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                          <Col xs={6} sm={6} md={6}>
                            <div className="form-group">
                              <label>
                                Partial Shipment{" "}
                                <span className="required-field">*</span>
                              </label>
                              <Field
                                name="partial_shipment"
                                type="text"
                                className={`selectArowGray form-control`}
                                autoComplete="off"
                                value={values.partial_shipment}
                              ></Field>
                              {errors.partial_shipment &&
                              touched.partial_shipment ? (
                                <span className="errorMsg">
                                  {errors.partial_shipment}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                        </Row>
                      )}
                      {this.state.within_india === "2" && (
                        <Row>
                          <Col xs={6} sm={6} md={6}>
                            <div className="form-group">
                              <label>
                                Country of Origin{" "}
                                <span className="required-field">*</span>
                              </label>
                              <Field
                                name="origin_country"
                                type="text"
                                className={`selectArowGray form-control`}
                                autoComplete="off"
                                value={values.origin_country}
                              ></Field>
                              {errors.origin_country &&
                              touched.origin_country ? (
                                <span className="errorMsg">
                                  {errors.origin_country}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                          <Col xs={6} sm={6} md={6}>
                            <div className="form-group">
                              <label>
                                Country of destination{" "}
                                <span className="required-field">*</span>
                              </label>
                              <Field
                                name="destination_country"
                                type="text"
                                className={`selectArowGray form-control`}
                                autoComplete="off"
                                value={values.destination_country}
                              ></Field>
                              {errors.destination_country &&
                              touched.destination_country ? (
                                <span className="errorMsg">
                                  {errors.destination_country}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                        </Row>
                      )}
                      <Row>
                        <Col xs={6} sm={6} md={6}>
                          <div className="form-group">
                            <label>
                              Terms of Delivery{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              defaultValue={values.terms_of_delivery}
                              name="terms_of_delivery"
                              options={this.state.terms_of_delivery_list}
                              value={values.terms_of_delivery}
                              isClearable={true}
                              onChange={(value) =>
                                setFieldValue("terms_of_delivery", value)
                              }
                            />
                            {errors.terms_of_delivery &&
                            touched.terms_of_delivery ? (
                              <span className="errorMsg">
                                {errors.terms_of_delivery}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                        <Col xs={6} sm={6} md={6}>
                          <div className="form-group">
                            <label>
                              Terms of payment{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              defaultValue={values.terms_of_payment}
                              name="terms_of_payment"
                              options={this.state.terms_of_payment_list}
                              value={values.terms_of_payment}
                              isClearable={true}
                              onChange={(value) =>
                                setFieldValue("terms_of_payment", value)
                              }
                            />

                            {errors.terms_of_payment &&
                            touched.terms_of_payment ? (
                              <span className="errorMsg">
                                {errors.terms_of_payment}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>

                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <label>Description</label>
                            {/* <Field
                                                name="description"
                                                type="textarea"
                                                rows="4"
                                                cols="50"
                                                className={`selectArowGray form-control`}
                                                autoComplete="off"
                                                value={values.description}
                                            />  */}

                            <textarea
                              name="description"
                              rows="4"
                              cols="50"
                              className="form-control"
                              value={values.description}
                              onChange={(e) =>
                                this.textAreaOnchange(
                                  e,
                                  setFieldValue,
                                  setFieldTouched
                                )
                              }
                            ></textarea>

                            {errors.description && touched.description ? (
                              <span className="errorMsg">
                                {errors.description}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>

                      {this.state.within_india === "2" && (
                        <Row>
                          <Col xs={4} sm={4} md={4}>
                            <div className="form-group">
                              <label>
                                Transport Mode and Means{" "}
                                <span className="required-field">*</span>
                              </label>
                              <Select
                                className="basic-single"
                                classNamePrefix="select"
                                defaultValue={values.transport_mode}
                                name="transport_mode"
                                options={this.state.transport_mode_list}
                                value={values.transport_mode}
                                isClearable={true}
                                onChange={(value) =>
                                  setFieldValue("transport_mode", value)
                                }
                              />

                              {errors.transport_mode &&
                              touched.transport_mode ? (
                                <span className="errorMsg">
                                  {errors.transport_mode}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                          <Col xs={4} sm={4} md={4}>
                            <div className="form-group">
                              <label>
                                Port/airport of discharge{" "}
                                <span className="required-field">*</span>
                              </label>
                              <Field
                                name="airport_discharge"
                                type="text"
                                className={`selectArowGray form-control`}
                                autoComplete="off"
                                value={values.airport_discharge}
                              ></Field>
                              {errors.airport_discharge &&
                              touched.airport_discharge ? (
                                <span className="errorMsg">
                                  {errors.airport_discharge}
                                </span>
                              ) : null}
                            </div>
                          </Col>

                          <Col xs={4} sm={4} md={4}>
                            <div className="form-group">
                              <label>
                                Packing{" "}
                                <span className="required-field">*</span>
                              </label>
                              <Field
                                name="packing"
                                type="text"
                                className={`selectArowGray form-control`}
                                autoComplete="off"
                                value={values.packing}
                              ></Field>
                              {errors.packing && touched.packing ? (
                                <span className="errorMsg">
                                  {errors.packing}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                        </Row>
                      )}

                      <Row>
                        <Col xs={3} sm={3} md={3}>
                          <div className="form-group">
                            <label>
                              Net Weight (Kg){" "}
                              <span className="required-field">*</span>
                            </label>
                            <Field
                              name="net_weight"
                              type="text"
                              value={values.net_weight}
                              className={`selectArowGray form-control`}
                              autoComplete="off"
                              value={values.net_weight}
                              onChange={(e) =>
                                this.handleWeight(e, setFieldValue)
                              }
                            />
                            {errors.net_weight && touched.net_weight ? (
                              <span className="errorMsg">
                                {errors.net_weight}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                        <Col xs={3} sm={3} md={3}>
                          <div className="form-group">
                            <label>
                              Price <span className="required-field">*</span>
                            </label>
                            <Field
                              name="price"
                              type="text"
                              className={`selectArowGray form-control`}
                              autoComplete="off"
                              value={values.price}
                              onChange={(e) =>
                                this.handlePrice(e, setFieldValue)
                              }
                            />
                            {errors.price && touched.price ? (
                              <span className="errorMsg">{errors.price}</span>
                            ) : null}
                          </div>
                        </Col>
                        <Col xs={3} sm={3} md={3}>
                          <div className="form-group">
                            <label>
                              Currency <span className="required-field">*</span>
                            </label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              defaultValue={values.currency}
                              name="currency"
                              options={this.state.currency_list}
                              value={values.currency}
                              isClearable={true}
                              onChange={(value) =>
                                setFieldValue("currency", value)
                              }
                            />
                            {errors.currency && touched.currency ? (
                              <span className="errorMsg">
                                {errors.currency}
                              </span>
                            ) : null}
                          </div>
                        </Col>

                        {this.state.within_india !== "2" && (
                          <Col xs={3} sm={3} md={3}>
                            <div className="form-group">
                              <label>
                                Gross <span className="required-field">*</span>
                              </label>
                              <Field
                                name="amount"
                                type="text"
                                className={`selectArowGray form-control`}
                                autoComplete="off"
                                readOnly={true}
                                value={gross_amount}
                              ></Field>
                              {errors.amount && touched.amount ? (
                                <span className="errorMsg">
                                  {errors.amount}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                        )}
                      </Row>

                      {/* ---------------------------------------------------- */}

                      <Row>
                        {(this.state.within_india === "1" ||
                          this.state.within_india === "3") && (
                          <>
                            <Col xs={6} sm={6} md={6}></Col>

                            {this.state.within_india === "1" ? (
                              <Col xs={3} sm={3} md={3}>
                                <div className="form-group">
                                  <label>
                                    IGST(%){" "}
                                    <span className="required-field">*</span>
                                  </label>
                                  <Field
                                    name="igst"
                                    type="text"
                                    value={values.igst}
                                    className={`selectArowGray form-control`}
                                    autoComplete="off"
                                    value={values.igst}
                                    onChange={(e) =>
                                      this.handleIGST(e, setFieldValue)
                                    }
                                  />
                                  {errors.igst && touched.igst ? (
                                    <span className="errorMsg">
                                      {errors.igst}
                                    </span>
                                  ) : null}
                                </div>
                              </Col>
                            ) : (
                              <Col xs={3} sm={3} md={3}>
                                <div className="form-group">
                                  <label>
                                    IGST(%)
                                    <span className="required-field">*</span>
                                  </label>
                                  <Field
                                    name="stateTax1"
                                    type="text"
                                    value={values.stateTax1}
                                    className={`selectArowGray form-control`}
                                    autoComplete="off"
                                    value={values.stateTax1}
                                    onChange={(e) =>
                                      this.handleStateTax1(e, setFieldValue)
                                    }
                                  />
                                  {errors.stateTax1 && touched.stateTax1 ? (
                                    <span className="errorMsg">
                                      {errors.stateTax1}
                                    </span>
                                  ) : null}
                                </div>
                              </Col>
                            )}

                            <Col xs={3} sm={3} md={3}>
                              <div className="form-group">
                                <label>
                                  IGST(Tax){" "}
                                  <span className="required-field">*</span>
                                </label>
                                <Field
                                  name="igst_amount"
                                  type="text"
                                  className={`selectArowGray form-control`}
                                  autoComplete="off"
                                  readOnly={true}
                                  value={igst_amount}
                                ></Field>
                                {errors.igst_amount && touched.igst_amount ? (
                                  <span className="errorMsg">
                                    {errors.igst_amount}
                                  </span>
                                ) : null}
                              </div>
                            </Col>
                          </>
                        )}

                        {this.state.within_india === "3" && (
                          <>
                            <div>
                              <Col xs={6} sm={6} md={6}></Col>
                              <Col xs={3} sm={3} md={3}>
                                <div className="form-group">
                                  <label>
                                    SGST(%)
                                    <span className="required-field">*</span>
                                  </label>
                                  <Field
                                    name="stateTax2"
                                    type="text"
                                    value={values.stateTax2}
                                    className={`selectArowGray form-control`}
                                    autoComplete="off"
                                    value={values.stateTax2}
                                    onChange={(e) =>
                                      this.handleStateTax2(e, setFieldValue)
                                    }
                                  />
                                  {errors.stateTax2 && touched.stateTax2 ? (
                                    <span className="errorMsg">
                                      {errors.stateTax2}
                                    </span>
                                  ) : null}
                                </div>
                              </Col>
                              <Col xs={3} sm={3} md={3}>
                                <div className="form-group">
                                  <label>
                                    SGST(Tax){" "}
                                    <span className="required-field">*</span>
                                  </label>
                                  <Field
                                    name="sgst_amount"
                                    type="text"
                                    className={`selectArowGray form-control`}
                                    autoComplete="off"
                                    readOnly={true}
                                    value={sgst_amount}
                                  ></Field>
                                  {errors.sgst_amount && touched.sgst_amount ? (
                                    <span className="errorMsg">
                                      {errors.sgst_amount}
                                    </span>
                                  ) : null}
                                </div>
                              </Col>
                            </div>
                          </>
                        )}
                      </Row>

                      {/* ONLY FOR INDIA ================ SATYAJIT */}
                      {/* {this.state.within_india !== "2" &&
                                    <Row>
                                        {this.state.within_india === "3" &&
                                            <>
                                                <Col xs={3} sm={3} md={3}>
                                                    <div className="form-group">
                                                        <label>IGST(%)<span className="required-field">*</span></label>
                                                        <Field
                                                            name="stateTax1"
                                                            type="text"
                                                            value={values.stateTax1}
                                                            className={`selectArowGray form-control`}
                                                            autoComplete="off"
                                                            value={values.stateTax1}
                                                            onChange={(e) => this.handleStateTax1(e,setFieldValue)}
                                                        />
                                                        {errors.stateTax1 && touched.stateTax1 ? (
                                                        <span className="errorMsg">
                                                            {errors.stateTax1}
                                                        </span>
                                                        ) : null}
                                                    </div>
                                                </Col>
                                                <Col xs={3} sm={3} md={3}>
                                                    <div className="form-group">
                                                        <label>SGST(%)<span className="required-field">*</span></label>
                                                        <Field
                                                            name="stateTax2"
                                                            type="text"
                                                            value={values.stateTax2}
                                                            className={`selectArowGray form-control`}
                                                            autoComplete="off"
                                                            value={values.stateTax2}
                                                            onChange={(e) => this.handleStateTax2(e,setFieldValue)}
                                                        />
                                                        {errors.stateTax2 && touched.stateTax2 ? (
                                                        <span className="errorMsg">
                                                            {errors.stateTax2}
                                                        </span>
                                                        ) : null}
                                                    </div>
                                                </Col>
                                            </>
                                        }
                                    </Row>      
                                }  */}

                      {/* ----------------------------------------------------------------- */}

                      <Row>
                        <Col xs={9} sm={9} md={9}></Col>
                        <Col xs={3} sm={3} md={3}>
                          <div className="form-group">
                            <label>
                              Total <span className="required-field">*</span>
                            </label>
                            <Field
                              name="total_amount"
                              type="text"
                              className={`selectArowGray form-control`}
                              autoComplete="off"
                              readOnly={true}
                              value={total_amount}
                            ></Field>
                            {errors.total_amount && touched.total_amount ? (
                              <span className="errorMsg">
                                {errors.total_amount}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </Modal.Body>
                  <Modal.Footer>
                    <ButtonToolbar>
                      <Button
                        onClick={this.handleClose}
                        className={`btn-line`}
                        type="button"
                      >
                        Close
                      </Button>

                      <Button
                        className={`btn-fill ${
                          isValid ? "btn-custom-green" : "btn-disable"
                        } m-r-10`}
                        type="submit"
                        disabled={isValid ? false : true}
                      >
                        {isSubmitting ? "Submitting..." : "Submit"}
                      </Button>
                    </ButtonToolbar>
                  </Modal.Footer>
                </Form>
              );
            }}
          </Formik>
        </Modal>

        <Modal
          show={this.state.beforeSubmit}
          onHide={() => this.closeWarning()}
          backdrop="static"
          className="respondBackpop"
        >
          <Formik
            initialValues={warningValues}
            validationSchema={warningSchema}
            onSubmit={this.warningSubmit}
          >
            {({
              values,
              errors,
              touched,
              isValid,
              isSubmitting,
              setFieldValue,
              setFieldTouched,
              setErrors,
            }) => {
              return (
                <Form>
                  {this.state.showProformaFormLoader === true ? (
                    <div className="loderOuter">
                      <div className="loader">
                        <img src={loaderlogo} alt="logo" />
                        <div className="loading">Loading...</div>
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                  <Modal.Header closeButton>
                    <Modal.Title>Create Proforma Invoice</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <div className="contBox">
                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <center>
                              <Link
                                to={"#"}
                                className="btn-line ml-10 btn btn-default"
                                onClick={(e) => {
                                  this.showSubtaskPopup(
                                    e,
                                    this.state.currRowNew
                                  );
                                }}
                              >
                                <i
                                  className="fa fa-plus"
                                  aria-hidden="true"
                                ></i>{" "}
                                Respond To Customer
                              </Link>
                              <Link
                                to={"#"}
                                className="btn-line ml-10 btn btn-default"
                                onClick={(e) => {
                                  this.downloadFile(
                                    e,
                                    this.state.download_url,
                                    this.state.file_name
                                  );
                                }}
                              >
                                <i
                                  className="fa fa-download"
                                  aria-hidden="true"
                                ></i>{" "}
                                Download
                              </Link>
                            </center>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </Modal.Body>
                  <Modal.Footer>
                    <ButtonToolbar>
                      <Button
                        onClick={this.closeWarning}
                        className={`btn-line`}
                        type="button"
                      >
                        Close
                      </Button>
                    </ButtonToolbar>
                  </Modal.Footer>
                </Form>
              );
            }}
          </Formik>
        </Modal>
      </>
    );
  }
}

export default ProformaPopup;
