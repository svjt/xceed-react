import React, { Component } from "react";
import { Row, Col, ButtonToolbar, Button, Modal, Alert } from "react-bootstrap";

//import { FilePond } from 'react-filepond';
//import 'filepond/dist/filepond.min.css';
import Dropzone from "react-dropzone";

import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import dateFormat from "dateformat";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";
import API from "../../shared/axios";
import { showErrorMessageFront } from "../../shared/handle_error_front";

import swal from "sweetalert";

import { localDate, trimString } from "../../shared/helper";

//import uploadAxios from 'axios';

import TinyMCE from "react-tinymce";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";

const initialValues = {
  employeeId: "",
  comment: "",
  breach_reason: "",
  file: "",
  dueDate: "",
  showModalLoader: false,
  file_name: [],
};

//console.log( TinyMCE );

// const onlyUnique = (value, index, self) => {
//     return self.indexOf(value) === index;
// }

const removeDropZoneFiles = (fileName, objRef, setErrors) => {
  var newArr = [];
  for (let index = 0; index < objRef.state.files.length; index++) {
    const element = objRef.state.files[index];

    if (fileName === element.name) {
    } else {
      newArr.push(element);
    }
  }

  var fileListHtml = newArr.map((file) => (
    <Alert key={file.name}>
      <span onClick={() => removeDropZoneFiles(file.name, objRef, setErrors)}>
        <i className="far fa-times-circle"></i>
      </span>{" "}
      {file.name}
    </Alert>
  ));
  setErrors({ file_name: "" });
  objRef.setState({
    files: newArr,
    filesHtml: fileListHtml,
  });
};

class Poke extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPoke: false,
      files: [],
      sla_breach: false,
      showModalLoader: false,
      acceptNewEmp: false,
      acceptNewEmpMsg: "",
      filesHtml: "",
    };
  }

  handleChange = (e, field) => {
    this.setState({
      [field]: e.target.value,
    });
  };

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.showPoke === true && nextProps.currRow.task_id > 0) {
      API.get(`/api/employees/other`)
        .then((res) => {
          //console.log(res.data.data);

          var myTeam = [];
          let selTeam = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            var leave = "";
            if (element.on_leave == 1) {
              leave = "[On Leave]";
            }
            myTeam.push({
              value: element["employee_id"],
              label:
                element["first_name"] +
                " " +
                element["last_name"] +
                " (" +
                element["desig_name"] +
                ") " +
                leave,
            });
            console.log(nextProps.currRow.current_ownership);
            if (
              element["employee_id"] === nextProps.currRow.current_ownership
            ) {
              selTeam.push({
                value: element["employee_id"],
                label:
                  element["first_name"] +
                  " " +
                  element["last_name"] +
                  " (" +
                  element["desig_name"] +
                  ") " +
                  leave,
              });
            }
          }

          this.setState({
            showPoke: nextProps.showPoke,
            currRow: nextProps.currRow,
            employeeArr: myTeam,
            selectedEmployee: selTeam,
          });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  handleClose = () => {
    var close = {
      sla_breach: false,
      new_employee: false,
      msg_new_employee: "",
      showModalLoader: false,
      showPoke: false,
      files: [],
      filesHtml: "",
    };
    this.setState(close);
    this.props.handleClose(close);
  };

  handlePoke = (values, actions) => {
    this.setState({ showModalLoader: true, msg_new_employee: false });

    var post_data = {
      employees: values.employeeId,
      comment: values.comment,
      assign_id: this.state.currRow.assignment_id,
    };

    if (this.props.from === "allocated") {
      post_data.posted_for = this.state.currRow.assigned_by;
    } else if (this.props.from === "open") {
      post_data.posted_for = this.state.currRow.assigned_to;
    }

    API.post(`/api/my_team_tasks/poke/${this.state.currRow.task_id}`, post_data)
      .then((res) => {
        this.handleClose();
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: res.data.data,
          icon: "success",
        }).then(() => {
          this.setState({ showModalLoader: false });
          this.props.reloadTask();
        });
      })
      .catch((error) => {
        this.setState({ showModalLoader: false });
        if (error.data.status === 3) {
          var token_rm = 2;
          showErrorMessageFront(error, token_rm, this.props);
          this.handleClose();
        } else {
          actions.setErrors(error.data.errors);
          actions.setSubmitting(false);
        }
      });
  };

  changeEmployee = (event, setFieldValue) => {
    if (event === null) {
      setFieldValue("employeeId", "");
    } else {
      // var post_date = {
      //     assigned_to:event,
      //     customer_id:this.state.currRow.customer_id
      // };
      // API.post(`/api/tasks/check_leave_multi`,post_date).then(res => {
      //     if(res.data.status === 2){
      //        this.setState({
      //             new_employee:true,
      //             msg_new_employee:res.data.msg_new_employee
      //        });
      //     }else{
      //         this.setState({
      //             new_employee:false,
      //             msg_new_employee:""
      //         });
      //     }
      // }).catch(error => {

      // });

      setFieldValue("employeeId", event);
    }
  };

  render() {
    const newInitialValues = Object.assign(initialValues, {
      employeeId: this.state.selectedEmployee,
      comment: "",
    });

    const validateStopFlag = Yup.object().shape({
      employeeId: Yup.string().trim().required("Please select employee"),
      comment: Yup.string().trim().required("Please enter your comment"),
    });

    //console.log(this.state.employeeArr)

    return (
      <>
        {typeof this.state.employeeArr !== "undefined" && (
          <Modal
            show={this.state.showPoke}
            onHide={() => this.handleClose()}
            backdrop="static"
          >
            <Formik
              initialValues={newInitialValues}
              validationSchema={validateStopFlag}
              onSubmit={this.handlePoke}
            >
              {({
                values,
                errors,
                touched,
                isValid,
                isSubmitting,
                handleChange,
                setFieldValue,
                setFieldTouched,
                setErrors,
              }) => {
                /* console.log('values', values)
                        console.log('errors', errors)
                        console.log('touched', touched)
                        console.log('isValid', isValid) */
                return (
                  <Form encType="multipart/form-data">
                    {this.state.showModalLoader === true ? (
                      <div className="loderOuter">
                        <div className="loader">
                          <img src={loaderlogo} alt="logo" />
                          <div className="loading">Loading...</div>
                        </div>
                      </div>
                    ) : (
                      ""
                    )}
                    <Modal.Header closeButton>
                      <Modal.Title>
                        Follow Up | {this.state.currRow.task_ref}
                      </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <div className="contBox">
                        {/* {this.state.currRow.parent_id > 0 && <Row>
                                    <Col xs={12} sm={12} md={12}>
                                        <div className="form-group">
                                            <label>Task Tile:</label>
                                            <p>{this.state.currRow.title}</p>
                                        </div>
                                    </Col>
                                </Row>} */}
                        <Row>
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>
                                Select Employee{" "}
                                <span className="required-field">*</span>
                              </label>
                              <Select
                                isMulti
                                className="basic-single"
                                classNamePrefix="select"
                                isClearable={true}
                                isSearchable={true}
                                name="employeeId"
                                options={this.state.employeeArr}
                                defaultValue={this.state.selectedEmployee}
                                onChange={(e) => {
                                  this.changeEmployee(e, setFieldValue);
                                }}
                                onBlur={() => setFieldTouched("employeeId")}
                              />
                              {errors.employeeId && touched.employeeId ? (
                                <span className="errorMsg">
                                  {errors.employeeId}
                                </span>
                              ) : null}

                              {this.state.new_employee === true ? (
                                <span style={{ color: "#ffae42" }}>
                                  {this.state.msg_new_employee}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                        </Row>

                        <Row>
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>
                                Comment{" "}
                                <span className="required-field">*</span>
                              </label>
                              {/* <TinyMCE
                                            name="comment"
                                            content={values.comment}
                                            config={{
                                                menubar: false,
                                                branding: false,
                                                toolbar: 'undo redo | bold italic | alignleft aligncenter alignright'
                                            }}
                                            className={`selectArowGray form-control`}
                                            autoComplete="off"
                                            onChange={value =>
                                                setFieldValue("comment", value.level.content)
                                            }
                                            onBlur={() => setFieldTouched("comment")}  
                                        /> */}

                              <Field
                                name="comment"
                                component="textarea"
                                className="form-control"
                                autoComplete="off"
                                placeholder="Comment here"
                                onChange={handleChange}
                                style={{ height: "130px" }}
                              />

                              {errors.comment && touched.comment ? (
                                <span className="errorMsg">
                                  {errors.comment}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                        </Row>
                      </div>
                    </Modal.Body>
                    <Modal.Footer>
                      <button
                        onClick={this.handleClose}
                        className={`btn-line`}
                        type="button"
                      >
                        Cancel
                      </button>
                      <button
                        className={`btn-fill ${
                          isValid ? "btn-custom-green" : "btn-disable"
                        } m-r-10`}
                        type="submit"
                        disabled={isValid ? false : true}
                      >
                        {this.state.stopflagId > 0
                          ? isSubmitting
                            ? "Updating..."
                            : "Update"
                          : isSubmitting
                          ? "Submitting..."
                          : "Follow Up"}
                      </button>
                    </Modal.Footer>
                  </Form>
                );
              }}
            </Formik>
          </Modal>
        )}
      </>
    );
  }
}

export default Poke;
