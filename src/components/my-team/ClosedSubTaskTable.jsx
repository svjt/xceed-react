import React, { Component } from 'react';
import {BootstrapTable,TableHeaderColumn} from 'react-bootstrap-table';
import { getMyDrupalId,localDate,htmlDecode } from '../../shared/helper';
import base64 from 'base-64';

import StatusColumn from './StatusColumn';
import { Link } from 'react-router-dom';

import API from "../../shared/axios";
import swal from "sweetalert";
import ReactHtmlParser from 'react-html-parser';
import VerticalMenuClosedTask from './VerticalMenuClosedTask';

import {
  Tooltip,
  OverlayTrigger
} from "react-bootstrap";
import './Dashboard.css';
import dateFormat from "dateformat";
//import exclamationImage from '../../assets/images/exclamation-icon.svg';
const portal_url = `${process.env.REACT_APP_PORTAL_URL}`; // SATYAJIT


const getStatusColumn = refObj => (cell,row) => {
    return <StatusColumn rowData={row} />
}

const setCompanyName = refObj => (cell, row) => {
  return htmlDecode(cell);
}

const getActions = refObj => (cell, row) => {
  return (
    <VerticalMenuClosedTask
      id={cell}
      currRow={row}
      requestToReopen={refObj.requestToReopen}
      teamArr={refObj.props.teamData}
    />
  );
};

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const setDescription = refOBj => (cell,row) =>{
  if(row.parent_id > 0){
      let title = htmlDecode(row.title);
      let stripHtml = title.replace(/<[^>]+>/g, '');

      return (
          <LinkWithTooltip
              tooltip={`${ReactHtmlParser(stripHtml)}`}
              href="#"
              id="tooltip-1"
              clicked={e => refOBj.checkHandler(e)}
            >
            {ReactHtmlParser(stripHtml)}
          </LinkWithTooltip>
      );
  }else if(row.discussion === 1){
      let comment = htmlDecode(row.comment);
      let stripHtml = comment.replace(/<[^>]+>/g, '');
      return (
          <LinkWithTooltip
              tooltip={`${ReactHtmlParser(stripHtml)}`}
              href="#"
              id="tooltip-1"
              clicked={e => refOBj.checkHandler(e)}
            >
            {ReactHtmlParser(stripHtml)}
          </LinkWithTooltip>
      );
  }else{
      return (      
        <LinkWithTooltip
          tooltip={`${row.req_name}`}
          href="#"
          id="tooltip-1"
          clicked={e => refOBj.checkHandler(e)}
        >
          {row.req_name}
        </LinkWithTooltip>
      );
  }
}

const clickToShowTasks = refObj => (cell,row) =>{
   return <LinkWithTooltip tooltip={`${cell}`} href="#" id="tooltip-1" clicked={e => refObj.checkHandler(e)}>{cell}</LinkWithTooltip>
}

const setEmpCustName = refObj => (cell, row) => {
  if (row.customer_drupal_id > 0) {
    //hard coded customer id - SATYAJIT
    //row.customer_id = 2;
    return (
      /*<Link
        to={{
          pathname:
            portal_url + "customer-dashboard/" +
            row.customer_drupal_id
        }}
        target="_blank"
        style={{ cursor: "pointer" }}
      >*/
        <LinkWithTooltip
            tooltip={`${row.first_name + " " + row.last_name}`}
            href="#"
            id="tooltip-1"
            clicked={e => refObj.checkHandler(e)}
          >
          {row.first_name + " " + row.last_name}
        </LinkWithTooltip>        
      /*</Link>*/
    );
  } else {
    return "";
  }
};

const setAssignedTo = refOBj => (cell,row) => {
  //return row.emp_first_name+' '+row.emp_last_name+' ('+row.desig_name+')';
  if(row.discussion === 1){
      return '';
  }else{
      return (      
          <LinkWithTooltip
            tooltip={`${row.at_emp_first_name+' '+row.at_emp_last_name+' ('+row.at_emp_desig_name+')'}`}
            href="#"
            id="tooltip-1"
            clicked={e => refOBj.checkHandler(e)}
          >
            {row.at_emp_first_name+' '+row.at_emp_last_name+' ('+row.at_emp_desig_name+')'}
          </LinkWithTooltip>
          );
  }
}

const setAssignedBy = refOBj => (cell, row) => {
  if(row.discussion === 1){
      return '';
  }else{
    if(row.assigned_by === -1 ){
      return (    
        <LinkWithTooltip
            tooltip={`System`}
            href="#"
            id="tooltip-1"
            clicked={e => refOBj.checkHandler(e)}
          >
          System  
        </LinkWithTooltip>
      );
    }else{
      return (    
        <LinkWithTooltip
            tooltip={`${row.ab_emp_first_name + " " + row.ab_emp_last_name + " (" + row.ab_emp_desig_name + ")"}`}
            href="#"
            id="tooltip-1"
            clicked={e => refOBj.checkHandler(e)}
          >
            {row.ab_emp_first_name + " " + row.ab_emp_last_name + " (" + row.ab_emp_desig_name + ")"}
        </LinkWithTooltip>
      );
    }
  }
};

const setDateFormat = refObj => (cell,row) => {
  if(row.discussion === 1){
    return '';
  }else{
    var date = localDate(cell);
    return dateFormat(date, "dd/mm/yyyy");
  }
}


const setBlank = refObj => (cell,row) => {
  return "";    
};

const setClosedBy = refObj => (cell,row) => {
  if(row.close_status === 1 ){
    return (    
      <LinkWithTooltip
          tooltip={`${row.cb_emp_first_name + " " + row.cb_emp_last_name + " (" + row.cb_emp_desig_name + ")"}`}
          href="#"
          id="tooltip-1"
          clicked={e => refObj.checkHandler(e)}
        >
        {row.cb_emp_first_name + " " + row.cb_emp_last_name + " (" + row.cb_emp_desig_name + ")"}  
      </LinkWithTooltip>
    );
  }else{
    return null;
  }
}

const setDaysPending = refOBj => (cell,row) =>{
  if(row.discussion === 1){
      return '';
  }else{
    return (
      <LinkWithTooltip
          tooltip={cell}
          href="#"
          id="tooltip-1"
          clicked={e => refOBj.checkHandler(e)}
      >
          {cell}  
      </LinkWithTooltip>);
  }
}


const hoverDate = refOBj => (cell,row) => {
  return (    
    <LinkWithTooltip
        tooltip={cell}
        href="#"
        id="tooltip-1"
        clicked={e => refOBj.checkHandler(e)}
      >
      {cell}  
    </LinkWithTooltip>
  );
};


// const getBreach = refOBj => (cell,row) => {
//   var breach = false;
//   if(row.breach_reason !== null && row.breach_reason !== ''){
//       breach = true;
//   }

//   return (    
//     <>
//       {/* <div className="actionStyle"> */}
//           <div className="btn-group">
//               {breach && <LinkWithTooltip
//                   tooltip={`${htmlDecode(row.breach_reason)}`}
//                   href="#"
//                   id={`tooltip-menu-${row.task_id}`}
//                   clicked={e => this.checkHandler(e)}
//               >
//               <img src={exclamationImage} alt="exclamation" />
//               </LinkWithTooltip>}
//           </div>
//       {/* </div> */}
//     </>
//   );
// };

class ClosedSubTaskTable extends Component {

    state = {
        showCreateSubTask   : false,
        showAssign          : false,
        showRespondBack     : false
    };

    checkHandler = (event) => {
        event.preventDefault();
    };

    redirectUrl = (event, id) => {
        event.preventDefault();
        //http://reddy.indusnet.cloud/customer-dashboard?source=Mi02NTE=
        var emp_drupal_id = getMyDrupalId(localStorage.token);
        var base_encode   = base64.encode(`${id}-${emp_drupal_id}`); 
        window.open( portal_url + "customer-dashboard?source="+base_encode, '_blank');
    };

    showCloseTaskPopup = (currRow) => {
        swal({
            closeOnClickOutside: false,
            title: "Close task",
            text: "Are you sure you want to close this task?",
            icon: "warning",
            buttons: true,
            dangerMode: true
        }).then(willDelete => {
            if (willDelete) {
                API.delete(`/api/tasks/close/${currRow.task_id}`).then(res => {
                    swal({
                      closeOnClickOutside: false,
                      title: "Success",
                      text: "Task has been closed.",
                      icon: "success"
                    }).then(() => {
                        console.log('Task Closed');
                        this.props.reloadTaskSubTask();
                    });
                });
            }
        });
    }


    showSubTaskAssignPopup = (id) => {
        this.setState({showAssignSubTask:true,task_id:id});
    }

    poke = (id) => {
        console.log('poke subtask');
    }

    showRespondPopup = (currRow) => {
        console.log('Respond');
        this.setState({showRespondBack:true,currRow});
    }

    showSubTaskRespondPopup = (id) => {
        this.setState({showRespondSubTask:true,task_id:id});
    }

    refreshTable = () => {
        this.props.refreshTable();
    }

    handleClose = (closeObj) => {
        this.setState(closeObj);
    }

    tdClassName = (fieldValue, row) =>{
        var dynamicClass = ' ';
        if(row.vip_customer === 1){
            dynamicClass += 'bookmarked-column ';
        }
        return dynamicClass;
    }

    render(){

        const selectRowProp = {
            bgColor       : '#fff8f6'
        };

        return (
            <>
                <BootstrapTable 
                    data={this.props.tableData} 
                    selectRow={ selectRowProp } 
                    tableHeaderClass={"col-hidden"}
                    expandColumnOptions={ 
                        { 
                            expandColumnVisible: true,
                            expandColumnComponent: this.expandColumnComponent,
                            columnWidth: 25
                        } 
                    } 
                    trClassName="tr-expandable" 
                >
                    <TableHeaderColumn dataField='task_ref' dataSort={ true } columnClassName={ this.tdClassName } editable={ false }  dataFormat={ clickToShowTasks(this) } >Tasks</TableHeaderColumn>

                    <TableHeaderColumn dataField='request_type' dataSort={ true } editable={ false }  dataFormat={ setDescription(this) } >Description</TableHeaderColumn>

                    <TableHeaderColumn dataField='request_type' dataSort={ true } editable={ false }  dataFormat={ setBlank(this) } >Description</TableHeaderColumn>

                    <TableHeaderColumn dataField='dept_name' dataSort={ true } editable={ false } expandable={ false } dataFormat={ setAssignedTo(this) } >Assigned To </TableHeaderColumn>

                    <TableHeaderColumn
                    dataField="dept_name"
                      //dataSort={true}
                      editable={false}
                      expandable={false}
                      dataFormat={setAssignedBy(this)}
                      >
                      Assigned By{" "}
                      </TableHeaderColumn>

                    <TableHeaderColumn
                      dataField="company_name"
                      //dataSort={true}
                      expandable={false}
                      dataFormat={setCompanyName(this)}
                    >
                      Customer
                    </TableHeaderColumn>

                    <TableHeaderColumn dataField='cust_name' dataSort={ true } editable={ false }  dataFormat={ setEmpCustName(this) } >Customer Name</TableHeaderColumn>

                    <TableHeaderColumn dataField='display_add_date' editable={ false } expandable={ false } dataFormat={hoverDate(this)} >Created Date</TableHeaderColumn>

                    <TableHeaderColumn dataField='display_due_date' editable={ false } expandable={ false } dataFormat={hoverDate(this)} >Due Date</TableHeaderColumn>

                    <TableHeaderColumn dataField='display_date_added' editable={ false } expandable={ false } dataFormat={hoverDate(this)} >Assigned Date</TableHeaderColumn>

                    <TableHeaderColumn
                      dataField="close_status"
                      //dataSort={true}
                      editable={false}
                      expandable={false}
                      dataFormat={setClosedBy(this)}
                    >
                    Closed By{" "}
                    </TableHeaderColumn>

                    <TableHeaderColumn dataField='display_responded_date' editable={ false } dataFormat={ setDaysPending(this) } expandable={ false } >Respond Date</TableHeaderColumn>
                    
                    {/* <TableHeaderColumn
                      dataField="task_id"
                      dataFormat={getBreach(this)}
                      expandable={false}
                      editable={false}
                    /> */}
                    <TableHeaderColumn
                      isKey
                      dataField="task_id"
                      dataFormat={getActions(this)}
                      expandable={false}
                      editable={false}
                    />
                </BootstrapTable>
           
            </>
        );
    }

}

export default ClosedSubTaskTable;