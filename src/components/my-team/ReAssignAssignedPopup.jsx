import React, { Component } from "react";

import { Row, Col, ButtonToolbar, Button, Modal, Alert } from "react-bootstrap";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";

//import { FilePond } from 'react-filepond';
//import 'filepond/dist/filepond.min.css';
import Dropzone from "react-dropzone";
import TinyMCE from "react-tinymce";

import swal from "sweetalert";
import dateFormat from "dateformat";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";

import API from "../../shared/axios";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
import { showErrorMessageFront } from "../../shared/handle_error_front";

import { htmlDecode, localDate, trimString } from "../../shared/helper";

import { Editor } from "@tinymce/tinymce-react";

const initialValues = {
  employeeId: "",
  comment: "",
  breach_reason: "",
  file: "",
  dueDate: "",
  file_name: [],
};

const removeDropZoneFiles = (fileName, objRef, setErrors) => {
  var newArr = [];
  for (let index = 0; index < objRef.state.files.length; index++) {
    const element = objRef.state.files[index];

    if (fileName === element.name) {
    } else {
      newArr.push(element);
    }
  }

  var fileListHtml = newArr.map((file) => (
    <Alert key={file.name}>
      <span onClick={() => removeDropZoneFiles(file.name, objRef, setErrors)}>
        <i className="far fa-times-circle"></i>
      </span>{" "}
      {file.name}
    </Alert>
  ));
  setErrors({ file_name: "" });
  objRef.setState({
    files: newArr,
    filesHtml: fileListHtml,
  });
};

class ReAssignAssignedPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showReAssign: false,
      files: [],
      currRow: [],
      sla_breach: false,
      showModalLoader: false,
      filesHtml: "",
    };
  }

  handleChange = (e, field) => {
    this.setState({
      [field]: e.target.value,
    });
  };

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.showReAssign === true && nextProps.currRow.task_id > 0) {
      this.setState({
        files: [],
        currRow: [],
        taskDetails: [],
        employeeArr: [],
        stateEmplId: "",
        new_employee: false,
        msg_new_employee: "",
        showReAssign: false,
        sla_breach: false,
      });

      // var empObj = {
      //   value: nextProps.currRow.assigned_to,
      //   label: `${nextProps.currRow.emp_first_name} ${nextProps.currRow.emp_last_name} (${nextProps.currRow.desig_name})`
      // };

      // initialValues.employeeId = empObj;
      // initialValues.dueDate = new Date(nextProps.currRow.due_date);

      this.setState({
        currRow: nextProps.currRow,
      });

      var post_date = {};
      if (nextProps.currRow.parent_id > 0) {
        post_date = {
          due_date: nextProps.currRow.due_date,
          sub_task: 1,
          auto: 1,
        };
      } else {
        post_date = { due_date: nextProps.currRow.due_date, auto: 1 };
      }

      API.post(
        `/api/my_team_tasks/sla_breach/${nextProps.currRow.task_id}`,
        post_date
      )
        .then((res) => {
          //console.log(res.data);
          if (res.data.status === 1) {
            this.setState({ sla_breach: true, showModalLoader: false });
          } else {
            this.setState({ sla_breach: false, showModalLoader: false });
          }

          if (nextProps.currRow.parent_id > 0) {
            initialValues.dueDate = localDate(res.data.sub_task_due_date);
          } else {
            initialValues.dueDate = localDate(nextProps.currRow.due_date);
          }

          API.get(`/api/employees/other`)
            .then((res) => {
              //console.log(res.data.data);

              var myTeam = [];
              for (let index = 0; index < res.data.data.length; index++) {
                const element = res.data.data[index];
                var leave = "";
                if (element.on_leave == 1) {
                  leave = "[On Leave]";
                }
                myTeam.push({
                  value: element["employee_id"],
                  label:
                    element["first_name"] +
                    " " +
                    element["last_name"] +
                    " (" +
                    element["desig_name"] +
                    ") " +
                    leave,
                });
              }

              this.setState({ employeeArr: myTeam });

              API.get(
                `/api/my_team_tasks/${this.state.currRow.task_id}/${this.state.currRow.assignment_id}/${this.state.currRow.assigned_by}`
              )
                .then((res) => {
                  this.setState({
                    taskDetails: res.data.data.taskDetails,
                  });

                  var empObj = {
                    value: this.state.taskDetails.assigned_to,
                    label: `${this.state.taskDetails.emp_first_name} ${this.state.taskDetails.emp_last_name} (${this.state.taskDetails.desig_name})`,
                  };
                  // empObj.push({
                  //     value:this.state.taskDetails.assigned_to,
                  //     label:`${this.state.taskDetails.emp_first_name} ${this.state.taskDetails.emp_last_name} (${this.state.taskDetails.desig_name})`
                  // });

                  initialValues.employeeId = empObj;
                  //initialValues.comment = this.state.taskDetails.comment;

                  if (
                    this.state.taskDetails.breach_reason !== null &&
                    this.state.taskDetails.breach_reason !== ""
                  ) {
                    initialValues.breach_reason = htmlDecode(
                      this.state.taskDetails.breach_reason
                    );
                    this.setState({
                      sla_breach: true,
                    });
                  } else {
                    initialValues.breach_reason = "";
                  }

                  this.setState({
                    showReAssign: nextProps.showReAssign,
                    stateEmplId: empObj,
                    files: [],
                    filesHtml: "",
                  });

                  //console.log(initialValues);
                })
                .catch((err) => {
                  console.log(err);
                });
            })
            .catch((err) => {
              console.log(err);
            });
        })
        .catch((error) => {});
    }
  };

  changeDate = (event, setFieldValue) => {
    if (event === null) {
      setFieldValue("dueDate", this.getDueDate());
      this.setState({ sla_breach: false });
    } else {
      setFieldValue("dueDate", event);
      this.setState({ showModalLoader: true });
      var post_date = {};
      if (this.state.currRow.parent_id > 0) {
        post_date = { due_date: event, sub_task: 1, auto: 0 };
      } else {
        post_date = { due_date: event, auto: 0 };
      }

      API.post(
        `/api/my_team_tasks/sla_breach/${this.state.currRow.task_id}`,
        post_date
      )
        .then((res) => {
          //console.log(res.data);
          if (res.data.status === 1) {
            this.setState({ sla_breach: true, showModalLoader: false });
          } else {
            this.setState({ sla_breach: false, showModalLoader: false });
          }
        })
        .catch((error) => {});
    }
  };

  handleClose = () => {
    var close = {
      sla_breach: false,
      stateEmplId: "",
      showReAssign: false,
      files: [],
      filesHtml: "",
    };
    this.setState(close);
    this.props.handleClose(close);
  };

  handleSubmitAssignTask = (values, actions) => {
    this.setState({ showModalLoader: true, new_employee: false });
    if (this.state.sla_breach === true && values.breach_reason === "") {
      actions.setErrors({ breach_reason: "Please provide a reason." });
      actions.setSubmitting(false);
      this.setState({ showModalLoader: false });
    } else {
      var due_date = dateFormat(values.dueDate, "yyyy-mm-dd");

      var formData = new FormData();

      if (this.state.files && this.state.files.length > 0) {
        for (let index = 0; index < this.state.files.length; index++) {
          const element = this.state.files[index];
          formData.append("file", element);
        }
        //formData.append('file', this.state.file);
      } else {
        formData.append("file", "");
      }

      formData.append("assigned_to", values.employeeId.value);
      formData.append("comment", values.comment);
      if (this.state.sla_breach === false) {
        formData.append("breach_reason", "");
      } else {
        formData.append("breach_reason", values.breach_reason);
      }
      formData.append("due_date", due_date);
      formData.append("assign_id", this.state.currRow.assignment_id);
      formData.append("posted_for", this.state.currRow.assigned_by);

      const config = {
        headers: {
          "content-type": "multipart/form-data",
        },
      };

      API.post(
        `/api/my_team_tasks/reassign/${this.state.currRow.task_id}`,
        formData,
        config
      )
        .then((res) => {
          this.setState({ showModalLoader: false });
          this.handleClose();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Task has been reassigned.",
            icon: "success",
          }).then(() => {
            this.props.reloadTask();
          });
        })
        .catch((error) => {
          this.setState({ showModalLoader: false });
          if (error.data.status === 3) {
            var token_rm = 2;
            showErrorMessageFront(error, token_rm, this.props);
            this.handleClose();
          } else {
            actions.setErrors({
              dueDate: error.data.errors.due_date,
              employeeId: error.data.errors.assigned_to,
              comment: error.data.errors.comment,
              breach_reason: error.data.errors.breach_reason,
              file_name: error.data.errors.file_name,
            });
            actions.setSubmitting(false);
          }
        });
    }
  };

  changeDueDate = (value) => {
    initialValues.dueDate = value;
  };

  getDueDate = () => {
    if (typeof this.state.currRow !== "undefined") {
      // var due_date = dateFormat(localDate(this.state.currRow.due_date), "yyyy-mm-dd");
      // return new Date(due_date);
      return localDate(this.state.currRow.due_date);
    }
  };

  changeManager = (event, setFieldValue) => {
    if (event === null) {
      setFieldValue("employeeId", "");
      this.setState({ stateEmplId: "" });
    } else {
      var post_date = {
        assigned_to: event.value,
        customer_id: this.state.currRow.customer_id,
      };
      API.post(`/api/my_team_tasks/check_leave`, post_date)
        .then((res) => {
          if (res.data.status === 2) {
            this.setState({
              new_employee: true,
              msg_new_employee: res.data.msg_new_employee,
            });
          } else {
            this.setState({
              new_employee: false,
              msg_new_employee: "",
            });
          }
        })
        .catch((error) => {});

      setFieldValue("employeeId", event);
      this.setState({ stateEmplId: event });
    }
  };

  setDropZoneFiles = (acceptedFiles, setErrors, setFieldValue) => {
    //console.log(acceptedFiles);
    setErrors({ file_name: false });
    setFieldValue(this.state.files);

    var prevFiles = this.state.files;
    var newFiles;
    if (prevFiles.length > 0) {
      //newFiles = newConcatFiles = acceptedFiles.concat(prevFiles);

      for (let index = 0; index < acceptedFiles.length; index++) {
        var remove = 0;

        for (let index2 = 0; index2 < prevFiles.length; index2++) {
          if (acceptedFiles[index].name === prevFiles[index2].name) {
            remove = 1;
            break;
          }
        }

        //console.log('remove',acceptedFiles[index].name,remove);

        if (remove === 0) {
          prevFiles.push(acceptedFiles[index]);
        }
      }
      //console.log('acceptedFiles',acceptedFiles);
      //console.log('prevFiles',prevFiles);
      newFiles = prevFiles;
    } else {
      newFiles = acceptedFiles;
    }

    this.setState({
      files: newFiles,
    });

    var fileListHtml = newFiles.map((file) => (
      <Alert key={file.name}>
        <span onClick={() => removeDropZoneFiles(file.name, this, setErrors)}>
          <i className="far fa-times-circle"></i>
        </span>{" "}
        {trimString(25, file.name)}
      </Alert>
    ));

    this.setState({
      filesHtml: fileListHtml,
    });
  };

  render() {
    //var currRow = this.state.currRow;
    //console.log(currRow);
    // const validateStopFlag = Yup.object().shape({
    //   employeeId: Yup.string().required("Please enter employee name")
    // });

    const validateStopFlag = Yup.object().shape({
      employeeId: Yup.string().trim().required("Please select employee"),
      dueDate: Yup.date().required("Due date is required"),
      comment: Yup.string().trim().required("Please enter your comment"),
    });

    return (
      <>
        {typeof this.state.stateEmplId !== "undefined" && (
          <Modal
            show={this.state.showReAssign}
            onHide={() => this.handleClose()}
            backdrop="static"
          >
            <Formik
              initialValues={initialValues}
              validationSchema={validateStopFlag}
              onSubmit={this.handleSubmitAssignTask}
            >
              {({
                values,
                errors,
                touched,
                isValid,
                handleChange,
                isSubmitting,
                setFieldValue,
                setFieldTouched,
                setErrors,
              }) => {
                return (
                  <Form encType="multipart/form-data">
                    {this.state.showModalLoader === true ? (
                      <div className="loderOuter">
                        <div className="loader">
                          <img src={loaderlogo} alt="logo" />
                          <div className="loading">Loading...</div>
                        </div>
                      </div>
                    ) : (
                      ""
                    )}
                    <Modal.Header closeButton>
                      <Modal.Title>Re-Assign A Task</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <div className="contBox">
                        <Row>
                          {/* <Col xs={12} sm={6} md={6}>
                                <div className="form-group">
                                    <Field
                                    name="employeeId"
                                    component="select"
                                    className={`selectArowGray form-control`}
                                    autoComplete="off"
                                    value={values.employeeId}
                                    >
                                    <option key='-1' value="" >Employee Name</option>
                                    {this.state.employeeArr.map((emp, i) => (
                                        <option key={i} value={emp.employee_id}>
                                            {emp.first_name} {emp.last_name} ({emp.desig_name})
                                        </option>
                                    ))}
                                    </Field>
                                    {errors.employeeId && touched.employeeId ? (
                                    <span className="errorMsg">
                                        {errors.employeeId}
                                    </span>
                                    ) : null}
                                </div>
                                </Col> */}

                          <Col xs={12} sm={6} md={6}>
                            <div className="form-group">
                              <label>Select Employee</label>
                              <Select
                                className="basic-single"
                                classNamePrefix="select"
                                defaultValue={this.state.stateEmplId}
                                isClearable={true}
                                isSearchable={true}
                                name="employeeId"
                                options={this.state.employeeArr}
                                /* onChange={value =>
                                  setFieldValue("employeeId", value)
                                } */
                                value={this.state.stateEmplId}
                                onChange={(e) => {
                                  this.changeManager(e, setFieldValue);
                                }}
                                onBlur={() => setFieldTouched("employeeId")}
                              />

                              {errors.employeeId && touched.employeeId ? (
                                <span className="errorMsg">
                                  {errors.employeeId}
                                </span>
                              ) : null}

                              {this.state.new_employee === true ? (
                                <span style={{ color: "#ffae42" }}>
                                  {this.state.msg_new_employee}
                                </span>
                              ) : null}
                            </div>
                          </Col>

                          <Col xs={12} sm={6} md={6}>
                            <div className="form-group react-date-picker">
                              {/* <DatePicker
                                        name="dueDate"
                                        showLeadingZeros={true}
                                        format="dd/mm/yyyy"
                                        onChange={value =>
                                            setFieldValue("dueDate", value)
                                        }
                                        value={values.dueDate}
                                    /> */}
                              <label>
                                Due Date{" "}
                                {Date.parse(
                                  new Date(this.state.currRow.due_date)
                                ) >
                                  Date.parse(
                                    new Date(
                                      this.state.currRow.original_due_date
                                    )
                                  ) && (
                                  <i style={{ "font-size": "12px" }}>
                                    (Original Due Date:{" "}
                                    {dateFormat(
                                      this.state.currRow.original_due_date,
                                      "dd/mm/yyyy h:MM TT"
                                    )}
                                    )
                                  </i>
                                )}
                              </label>

                              <div className="form-control">
                                <DatePicker
                                  name={"dueDate"}
                                  className="borderNone"
                                  minDate={new Date()}
                                  selected={
                                    values.dueDate ? values.dueDate : null
                                  }
                                  /* onChange={value =>
                                    setFieldValue("dueDate", value)
                                  } */
                                  dateFormat="dd/MM/yyyy"
                                  onChange={(e) => {
                                    this.changeDate(e, setFieldValue);
                                  }}
                                />

                                {this.state.sla_breach && (
                                  <p className="errorMsgPos2">
                                    You are going to breach the SLA
                                  </p>
                                )}
                              </div>
                              {errors.dueDate && touched.dueDate ? (
                                <p className="errorMsgPos1">{errors.dueDate}</p>
                              ) : null}
                            </div>
                          </Col>
                        </Row>

                        {this.state.sla_breach && (
                          <Row>
                            <Col xs={12} sm={12} md={12}>
                              <div className="form-group">
                                <label>Please Provide Reason</label>
                                <Field
                                  name="breach_reason"
                                  component="textarea"
                                  className={`selectArowGray form-control`}
                                  autoComplete="off"
                                  onChange={handleChange}
                                />
                                {errors.breach_reason &&
                                touched.breach_reason ? (
                                  <span className="errorMsg">
                                    {errors.breach_reason}
                                  </span>
                                ) : null}
                              </div>
                            </Col>
                          </Row>
                        )}

                        <Row>
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>
                                Comment{" "}
                                <span className="required-field">*</span>
                              </label>
                              {/* <Field
                                name="comment"
                                component="textarea"
                                className={`selectArowGray form-control`}
                                autoComplete="off"
                                value={values.comment}
                              /> */}
                              {/* <TinyMCE
                                name="comment"
                                content={values.comment}
                                config={{
                                  menubar: false,
                                  branding: false,
                                  toolbar: 'undo redo | bold italic | alignleft aligncenter alignright'
                                }}
                                className={`selectArowGray form-control`}
                                autoComplete="off"
                                value={values.comment}
                                onChange={value =>
                                    setFieldValue("comment", value.level.content)
                                }  
                                onBlur={() => setFieldTouched("comment")}                                      
                                /> */}

                              <Editor
                                name="content"
                                value={
                                  values.comment !== null &&
                                  values.comment !== ""
                                    ? values.comment
                                    : ""
                                }
                                content={
                                  values.comment !== null &&
                                  values.comment !== ""
                                    ? values.comment
                                    : ""
                                }
                                init={{
                                  menubar: false,
                                  branding: false,
                                  placeholder: "Enter comments",
                                  plugins:
                                    "link table hr visualblocks code placeholder lists autoresize textcolor",
                                  toolbar:
                                    "bold italic strikethrough superscript subscript | forecolor backcolor | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | visualblocks code | fontselect",
                                  font_formats:
                                    "Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats",
                                }}
                                onEditorChange={(value) =>
                                  setFieldValue("comment", value)
                                }
                              />

                              {errors.comment && touched.comment ? (
                                <span className="errorMsg">
                                  {errors.comment}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                        </Row>

                        <Row>
                          <Col xs={12} sm={12} md={12}>
                            {/* <div className="form-group">
                                  <input 
                                      id="file" 
                                      name="file" 
                                      type="file" 
                                      onChange={(event) => {                                            
                                          this.setState({ file : event.target.files[0] })
                                          setFieldValue("file", event.target.files[0]);
                                      }} 
                                  />

                                  {errors.file && touched.file ? (
                                  <span className="errorMsg">
                                      {errors.file}
                                  </span>
                                  ) : null}
                            </div> */}

                            <div className="form-group custom-file-upload">
                              {/* <FilePond ref={ref => this.pond = ref}
                                        allowMultiple={true}
                                        maxFiles={10}
                                        onupdatefiles={fileItems => {
                                            // Set currently active file objects to this.state
                                            this.setState({
                                                files: fileItems.map(fileItem => fileItem.file)
                                            });
                                        }}>
                                </FilePond> */}

                              <Dropzone
                                onDrop={(acceptedFiles) =>
                                  this.setDropZoneFiles(
                                    acceptedFiles,
                                    setErrors,
                                    setFieldValue
                                  )
                                }
                              >
                                {({ getRootProps, getInputProps }) => (
                                  <section>
                                    <div
                                      {...getRootProps()}
                                      className="custom-file-upload-header"
                                    >
                                      <input {...getInputProps()} />
                                      <p>Upload file</p>
                                    </div>
                                    <div className="custom-file-upload-area">
                                      {this.state.filesHtml}
                                    </div>
                                  </section>
                                )}
                              </Dropzone>
                              {errors.file_name || touched.file_name ? (
                                <span className="errorMsg errorExpandView">
                                  {errors.file_name}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                        </Row>

                        {/* <Row>
                                <Col xs={12} sm={12} md={12}>
                                <div className="form-group">
                                    Comment
                                    <TinyMCE
                                        name="comment"
                                        content={values.comment}
                                        config={{
                                          branding: false,
                                            toolbar: 'undo redo | bold italic | alignleft aligncenter alignright'
                                        }}
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        onChange={value =>
                                            setFieldValue("comment", value.level.content)
                                        }
                                    />
                                    {errors.comment && touched.comment ? (
                                    <span className="errorMsg">
                                        {errors.comment}
                                    </span>
                                    ) : null}
                                </div>
                                </Col>
                            </Row> */}
                      </div>
                    </Modal.Body>
                    <Modal.Footer>
                      <button
                        onClick={this.handleClose}
                        className={`btn-line`}
                        type="button"
                      >
                        Close
                      </button>
                      <button
                        className={`btn-fill ${
                          isValid ? "btn-custom-green" : "btn-disable"
                        } m-r-10`}
                        type="submit"
                        disabled={isValid ? false : true}
                      >
                        {this.state.stopflagId > 0
                          ? isSubmitting
                            ? "Updating..."
                            : "Update"
                          : isSubmitting
                          ? "Submitting..."
                          : "Submit"}
                      </button>
                    </Modal.Footer>
                  </Form>
                );
              }}
            </Formik>
          </Modal>
        )}
      </>
    );
  }
}

export default ReAssignAssignedPopup;
