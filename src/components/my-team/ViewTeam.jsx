import React, { Component } from "react";
import "../../assets/css/MyCustomer.css";
import API from "../../shared/axios";
import commenLogo from "../../assets/images/image-edit.jpg";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
import { Redirect, Link } from "react-router-dom";

class ViewTeam extends Component {
  constructor(props) {
    super(props);
    this.textInput = React.createRef();

    this.state = {
      view_employee_list: [],
      isLoading: true,
    };
  }

  componentDidMount = () => {
    API.get(`/api/team/my_team_members`)
      .then((res) => {
        this.setState({
          view_employee_list: res.data.data,
          isLoading: false,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  render() {
    if (this.state.isLoading) {
      return (
        <>
          <div className="loderOuter">
            <div className="loader">
              <img src={loaderlogo} alt="logo" />
              <div className="loading">Loading...</div>
            </div>
          </div>
        </>
      );
    } else {
      if (this.state.view_employee_list.length > 0) {
        return (
          <>
            <div className="content-wrapper">
              <section class="content-header">
                <h1>View Team</h1>
                <div
                  className="back-btn"
                  onClick={() => this.props.history.push("/user/my_team")}
                >
                  Back
                </div>
              </section>
              <section className="content">
                <div className="row">
                  {this.state.view_employee_list.map((employee, k) => (
                    <div key={k} className="col-lg-6 col-sm-6 col-xs-12">
                      <div className="customer-box">
                        <div className="row clearfix">
                          <div className="col-xs-10">
                            <div className="customer-img-are">
                              <div className="customer-image">
                                {" "}
                                <span>
                                  {" "}
                                  <img src={commenLogo} alt="Logo" />
                                </span>{" "}
                              </div>
                            </div>
                            <div className="customer-content col-border">
                              <p className="cus-id">
                                <strong>Name:</strong> {employee.first_name}{" "}
                                {employee.last_name}
                              </p>
                              <p className="cus-id">
                                <strong>Email:</strong> {employee.email}
                              </p>
                              <p className="cus-id">
                                <strong>Designation:</strong>{" "}
                                {employee.desig_name}
                              </p>
                              <Link
                                to={{
                                  pathname:
                                    "/user/team_dashboard/" +
                                    employee.employee_id,
                                }}
                                className="cus-ach"
                              >
                                Team Dashboard
                              </Link>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </section>
            </div>
          </>
        );
      } else {
        return <Redirect to="/user/my_team" />;
      }
    }
  }
}

export default ViewTeam;
