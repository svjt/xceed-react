import React, { Component } from "react";

import MyTeamOpenTasksTable from "./MyTeamOpenTasksTable";
import MyTeamCloseTasksTable from "./MyTeamCloseTasksTable";
import MyTeamAllocatedTasksTable from "./MyTeamAllocatedTasksTable";


import CustomerApproval from "./CustomerApproval";
import ApprovalTasksTable from "./ApprovalTasksTable";
// import MyCompanyPopup from './MyCompanyPopup';
import AnalyticsModule from "./AnalyticsModule";

//import './Dashboard.css';

import API from "../../shared/axios";
import swal from "sweetalert";

//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
import MyTeamSearch from "./MyTeamSearch";
import { Link } from "react-router-dom";

import { isMobile } from "react-device-detect";

import { Tooltip, OverlayTrigger } from "react-bootstrap";

// var navbar = document.getElementById("team-search");
// var sticky = navbar.offsetTop;

// function myFunction() {
//   if (window.pageYOffset >= sticky) {
//     navbar.classList.add("sticky")
//   } else {
//     navbar.classList.remove("sticky");
//   }
// }

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

class BMOverview extends Component {
  state = {
    openTasks: [],
    closedTasks: [],
    isLoading: true,
    showCreateTasks: false,
    emp_id: 0,
    view_employee_list: [],
    countApprovalTasks: 0,
    countApprovalTasksClosed:0,
    approvalTasks: [],
    approvalTasksClosed:[],
    showCompanyPopup: false,
    noTeam: false,
  };

  checkHandler = (event) => {
    event.preventDefault();
  };

  componentDidMount() {
    API.get(`/api/team/my_team_members`)
      .then((res) => {
        if (res.data.data.length > 0) {
          var myEmpIdArr = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            myEmpIdArr.push(element["employee_id"]);
          }
          this.setState({ my_emp_arr: myEmpIdArr });
          this.allTaskDetails();
          if (isMobile) {
            var navbar = document.getElementById("team-search");
            navbar.classList.remove("sticky");
          } else {
            window.onscroll = function () {
              var navbar = document.getElementById("team-search");
              if (navbar !== null) {
                var sticky = navbar.offsetTop;
                if (window.pageYOffset > sticky) {
                  navbar.classList.add("sticky");
                } else {
                  navbar.classList.remove("sticky");
                }
              }
            };
          }
        } else {
          this.setState({ noTeam: true });
          swal({
            closeOnClickOutside: false,
            title: "My Teams",
            text: "You do not have any team assigned.",
            customClass:"zIndex",
            icon: "alert",
          }).then(() => {

            let hostname = window.location.hostname;

            if(hostname == 'demo71v2.indusnet.cloud'){
              window.location.href = "/dr_reddy_xceed/user/dashboard";
            }else{
              window.location.href = "/user/dashboard";
            }
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  allTaskDetails = (page = 1) => {
    this.setState({
      openTasks: [],
      closedTasks: [],
      queryString: "",
      countOpenTasks: 0,
      countClosedTasks: 0,
      approvalCustomer: [],
      countApprovalCustomer: 0,
      reloadAnalytics: false,
      isLoading: true,
      tabsClicked: "tab_1",
    });

    API.get(`/api/team/open?page=${page}`)
      .then((res) => {
        this.setState({isLoading: false}); //AKJ
        if (res.data.count_open_tasks === undefined) {
          this.setState({ countOpenTasks: "0" });
        } else {
          this.setState({ countOpenTasks: res.data.count_open_tasks });
        }
        this.setState({ openTasks: res.data.data,spoc_exists:res.data.spoc_in_teams });
        if(res.data.spoc_in_teams === true){
          API.get(`/api/team/approval?page=${page}`)
          .then((res) => {
            this.setState({
              approvalTasks: res.data.data,
              countApprovalTasks: res.data.count_approval_tasks,
              activeApprovalTasks:res.data.active_approval_tasks
            });
          })
          .catch((err) => {});
          API.get(`/api/team/approval_closed?page=${page}`)
          .then((res) => {
            this.setState({
              approvalTasksClosed: res.data.data,
              countApprovalTasksClosed: res.data.count_approval_tasks
            });
          })
          .catch((err) => {});
        }
        API.get(`/api/team/closed?page=${page}`)
          .then((res) => {
            if (res.data.count_closed_tasks === undefined) {
              this.setState({ countClosedTasks: "0" });
            } else {
              this.setState({ countClosedTasks: res.data.count_closed_tasks });
            }
            this.setState({ closedTasks: res.data.data });
            API.get(`/api/team/allocated?page=${page}`)
              .then((res) => {
                if (res.data.count_all_allocated_tasks === undefined) {
                  this.setState({ countAllocatedTasks: "0" });
                } else {
                  this.setState({
                    countAllocatedTasks: res.data.count_all_allocated_tasks,
                  });
                }
                this.setState({ allocatedTasks: res.data.data });
                API.get(`/api/team/my_team_members`)
                  .then((res) => {
                    this.setState({
                      view_employee_list: res.data.data,
                      // isLoading: false, //AKJ
                    });
                  })
                  .catch((err) => {});
              })
              .catch((err) => {});
          })
          .catch((err) => {});
      })
      .catch((err) => {});

      API.get(`/api/team/get_approval_customer?page=${page}`)
      .then((res) => {
        this.setState({
          approvalCustomer: res.data.data,
          countApprovalCustomer: res.data.count,
        })
      })
      .catch((err)=>{
        console.log({err});
      });;
  };

  updateState = (arr) => {
    if (arr !== undefined) {
      this.setState({
        openTasks: [],
        closedTasks: [],
        allocatedTasks: [],
        queryString: "",
        countOpenTasks: 0,
        countAllocatedTasks: 0,
        countClosedTasks: 0,
        reloadAnalytics: true,
      });

      this.setState({
        openTasks: arr[0].openTasks,
        countOpenTasks: arr[1].countOpenTasks,
        closedTasks: arr[2].closedTasks,
        countClosedTasks: arr[3].countClosedTasks,
        allocatedTasks: arr[4].allocatedTasks,
        countAllocatedTasks: arr[5].countAllocatedTasks,
        queryString: arr[6].queryString,
        reloadAnalytics: false,
      });
    }
  };

  handleTabs = (event) => {
    if (event.currentTarget.className === "active") {
      //DO NOTHING
    } else {
      var elems = document.querySelectorAll('[id^="tab_"]');
      var elemsContainer = document.querySelectorAll('[id^="show_tab_"]');
      var currId = event.currentTarget.id;

      for (var i = 0; i < elems.length; i++) {
        elems[i].classList.remove("active");
      }

      for (var j = 0; j < elemsContainer.length; j++) {
        elemsContainer[j].style.display = "none";
      }

      event.currentTarget.classList.add("active");
      event.currentTarget.classList.add("active");
      document.querySelector("#show_" + currId).style.display = "block";
      this.setState({ tabsClicked: currId });
    }
    // this.tabElem.addEventListener("click",function(event){
    //     alert(event.target);
    // }, false);
  };

  showCreateTaskPopup = () => {
    this.setState({ showCreateTasks: true });
  };

  handleClose = (closeObj) => {
    this.setState(closeObj);
  };

  // selectComapny = () =>{
  //     this.setState({showCompanyPopup:true});
  // }

  // closePopup = () => {
  //     this.setState({showCompanyPopup:false});
  // };

  downloadXLSX = (e) => {
    e.preventDefault();

    if (this.state.tabsClicked === "tab_1") {
      API.get(`/api/team/open_excel?${this.state.queryString}`, {
        responseType: "blob",
      })
        .then((res) => {
          //console.log(res);
          let url = window.URL.createObjectURL(res.data);
          let a = document.createElement("a");
          a.href = url;
          a.download = "open_tasks.xlsx";
          a.click();
        })
        .catch((err) => {});
    } else if (this.state.tabsClicked === "tab_2") {
      API.get(`/api/team/allocated_excel?${this.state.queryString}`, {
        responseType: "blob",
      })
        .then((res) => {
          //console.log(res);
          let url = window.URL.createObjectURL(res.data);
          let a = document.createElement("a");
          a.href = url;
          a.download = "allocated_tasks.xlsx";
          a.click();
        })
        .catch((err) => {});
    } else if (this.state.tabsClicked === "tab_3") {
      API.get(`/api/team/closed_excel?${this.state.queryString}`, {
        responseType: "blob",
      })
        .then((res) => {
          //console.log(res);
          let url = window.URL.createObjectURL(res.data);
          let a = document.createElement("a");
          a.href = url;
          a.download = "close_tasks.xlsx";
          a.click();
        })
        .catch((err) => {});
    }
  };

  render() {
    if (this.state.noTeam === true) {
      return (
        <>
          {/* <>
							<div className="loderOuter">
									<div className="loading_reddy_outer" style={{zIndex:'-1'}} >
											<div className="loading_reddy" >
													<img src={whitelogo} alt="logo" />
											</div>
									</div>
							</div>
					</> */}
          <div className="loderOuter">
            <div className="loader">
              <img src={loaderlogo} alt="logo" />
              <div className="loading">Loading...</div>
            </div>
          </div>
        </>
      );
    }

    if (this.state.isLoading) {
      return (
        <>
          {/* <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy">
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div> */}
          <div className="loderOuter">
            <div className="loader">
              <img src={loaderlogo} alt="logo" />
              <div className="loading">Loading...</div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <>
          <div className="content-wrapper">
            <section className="content-header heading-side-by-side">
              <h1>
                My Team
                {/* {this.state.view_employee_list.length > 0 ? (
                                    <small>
                                <Link to='/user/view_team'>
                                    View Team Members
                                </Link>
                                </small>
                            ) : null}  */}
                {/* {this.state.view_employee_list.length > 0 ? (
                                    <small style={{cursor:'pointer'}} onClick={()=>this.selectComapny()} >
                                    View My Team Customers
                                </small>
                            ) : null}  */}
              </h1>
            </section>
            <section className="content">
              <div className="clearfix serchapanel teamSearch" id="team-search">
                <MyTeamSearch changeState={this.updateState} />
              </div>

              {this.state.reloadAnalytics === false && (
                <AnalyticsModule
                  {...this.props}
                  queryString={this.state.queryString}
                  emp_list={this.state.view_employee_list}
                />
              )}

              {/*  {(this.state.tabsClicked === 'tab_1' && this.state.countOpenTasks > 0) && <a href="javascript:void(0);" onClick={(e) => this.downloadXLSX(e)} ><i className="fas fa-download"></i>Download Excel</a>}

                            {(this.state.tabsClicked === 'tab_2' && this.state.countAllocatedTasks > 0) && <a href="javascript:void(0);" onClick={(e) => this.downloadXLSX(e)} ><i className="fas fa-download"></i>Download Excel</a>}

                            {(this.state.tabsClicked === 'tab_3' && this.state.countClosedTasks > 0) && <a href="javascript:void(0);" onClick={(e) => this.downloadXLSX(e)} ><i className="fas fa-download"></i>Download Excel</a>} */}

              <div className="row">
                <div className="col-xs-12">
                  <div className="nav-tabs-custom">
                    <ul className="nav nav-tabs">
                      <li
                        className="active"
                        onClick={(e) => this.handleTabs(e)}
                        id="tab_1"
                      >
                        OPEN TASKS ({this.state.countOpenTasks}){" "}
                      </li>
                      <li onClick={(e) => this.handleTabs(e)} id="tab_2">
                        ALLOCATED TASKS ({this.state.countAllocatedTasks})
                      </li>
                      <li onClick={(e) => this.handleTabs(e)} id="tab_3">
                        CLOSED TASKS ({this.state.countClosedTasks})
                      </li>
                      {this.state.spoc_exists === true && <>
                        <li onClick={(e) => this.handleTabs(e)} id="tab_4">
                          PENDING REVIEWS ({this.state.countApprovalTasks})
                        </li>
                        <li onClick={(e) => this.handleTabs(e)} id="tab_5">
                          CLOSED REVIEWS ({this.state.countApprovalTasksClosed})
                        </li>
                      </>}
                      {this.state.countApprovalCustomer > 0 && <li id="tab_6" onClick={(e) => this.handleTabs(e)}>
                        CUSTOMER APPROVAL ({this.state.countApprovalCustomer})
                      </li>}
                      <li className="tabButtonSec  pull-right">
                        {this.state.tabsClicked === "tab_1" &&
                          this.state.countOpenTasks > 0 && (
                            <span onClick={(e) => this.downloadXLSX(e)}>
                              <LinkWithTooltip
                                tooltip={`Click here to download excel`}
                                href="#"
                                id="tooltip-open"
                                clicked={(e) => this.checkHandler(e)}
                              >
                                <i className="fas fa-download"></i>
                              </LinkWithTooltip>
                            </span>
                          )}

                        {this.state.tabsClicked === "tab_2" &&
                          this.state.countAllocatedTasks > 0 && (
                            <span onClick={(e) => this.downloadXLSX(e)}>
                              <LinkWithTooltip
                                tooltip={`Click here to download excel`}
                                href="#"
                                id="tooltip-allocated"
                                clicked={(e) => this.checkHandler(e)}
                              >
                                <i className="fas fa-download"></i>
                              </LinkWithTooltip>
                            </span>
                          )}

                        {this.state.tabsClicked === "tab_3" &&
                          this.state.countClosedTasks > 0 && (
                            <span onClick={(e) => this.downloadXLSX(e)}>
                              <LinkWithTooltip
                                tooltip={`Click here to download excel`}
                                href="#"
                                id="tooltip-closed"
                                clicked={(e) => this.checkHandler(e)}
                              >
                                <i className="fas fa-download"></i>
                              </LinkWithTooltip>
                            </span>
                          )}
                      </li>
                    </ul>

                    <div className="tab-content">
                      <div className="tab-pane active" id="show_tab_1">
                        {this.state.openTasks &&
                          this.state.openTasks.length > 0 && (
                            <MyTeamOpenTasksTable
                              teamData={this.state.my_emp_arr}
                              tableData={this.state.openTasks}
                              countOpenTasks={this.state.countOpenTasks}
                              queryString={this.state.queryString}
                              allTaskDetails={(e) => this.allTaskDetails()}
                            />
                          )}
                        {this.state.openTasks &&
                          this.state.openTasks.length === 0 && (
                            <div className="noData">No Data Found</div>
                          )}
                      </div>

                      <div className="tab-pane" id="show_tab_2">
                        {this.state.allocatedTasks &&
                          this.state.allocatedTasks.length > 0 && (
                            <MyTeamAllocatedTasksTable
                              teamData={this.state.my_emp_arr}
                              tableData={this.state.allocatedTasks}
                              countAllocatedTasks={
                                this.state.countAllocatedTasks
                              }
                              queryString={this.state.queryString}
                              allTaskDetails={(e) => this.allTaskDetails()}
                            />
                          )}
                        {this.state.allocatedTasks &&
                          this.state.allocatedTasks.length === 0 && (
                            <div className="noData">No Data Found</div>
                          )}
                      </div>

                      <div className="tab-pane" id="show_tab_3">
                        {this.state.closedTasks &&
                          this.state.closedTasks.length > 0 && (
                            <MyTeamCloseTasksTable
                              teamData={this.state.my_emp_arr}
                              tableData={this.state.closedTasks}
                              countClosedTasks={this.state.countClosedTasks}
                              queryString={this.state.queryString}
                              allTaskDetails={(e) => this.allTaskDetails()}
                            />
                          )}
                        {this.state.closedTasks &&
                          this.state.closedTasks.length === 0 && (
                            <div className="noData">No Data Found</div>
                          )}
                      </div>


                      <div className={`tab-pane`} id="show_tab_4">
                        {this.state.approvalTasks &&
                          this.state.approvalTasks.length > 0 && (
                            <ApprovalTasksTable
                              dashType="BM"
                              tableData={this.state.approvalTasks}
                              countApprovalTasks={this.state.countApprovalTasks}
                              queryString={this.state.queryString}
                              allTaskDetails={(e) => this.allTaskDetails()}
                            />
                          )}

                        {this.state.approvalTasks &&
                          this.state.approvalTasks.length === 0 && (
                          <div className="noData">No Data Found</div>
                          )}
                      </div>

                      <div className={`tab-pane`} id="show_tab_5">
                        {this.state.approvalTasksClosed &&
                          this.state.approvalTasksClosed.length > 0 && (
                            <ApprovalTasksTable
                              dashType="BM"
                              tableData={this.state.approvalTasksClosed}
                              countApprovalTasks={this.state.countApprovalTasksClosed}
                              queryString={this.state.queryString}
                              allTaskDetails={(e) => this.allTaskDetails()}
                            />
                          )}

                        {this.state.approvalTasksClosed &&
                          this.state.approvalTasksClosed.length === 0 && (
                          <div className="noData">No Data Found</div>
                          )}
                      </div>

                      {this.state.countApprovalCustomer > 0 && (
                        <div className={`tab-pane`} id="show_tab_6">
                          <CustomerApproval
                            tableData={this.state.approvalCustomer}
                            countApprovalCustomer={this.state.countApprovalCustomer}
                          />
                        </div>
                      )}

                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
          {/* {this.state.showCompanyPopup === true && <MyCompanyPopup closePopup={this.closePopup} {...this.props} />} */}
        </>
      );
    }
  }
}

export default BMOverview;
