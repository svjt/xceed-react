import React, { Component } from "react";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Modal,
  Alert,
  Tooltip,
  OverlayTrigger,
} from "react-bootstrap";

//import { FilePond } from 'react-filepond';
//import 'filepond/dist/filepond.min.css';
import Dropzone from "react-dropzone";
import exclamationImage from "../../assets/images/exclamation-icon.svg";

import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import dateFormat from "dateformat";
import { htmlDecode } from "../../shared/helper";

//import DatePicker from 'react-date-picker';
import DatePicker from "react-datepicker"; // SATYAJIT
import "react-datepicker/dist/react-datepicker.css"; // SATYAJIT

import API from "../../shared/axios";
import swal from "sweetalert";

//import TinyMCE from 'react-tinymce';
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
import { showErrorMessageFront } from "../../shared/handle_error_front";

import { localDate, trimString, inArray } from "../../shared/helper";
import { Editor } from "@tinymce/tinymce-react";
import { Link } from "react-router-dom";
import Switch from "react-switch";
import Select from "react-select";
import ReactHtmlParser from "react-html-parser";
import _ from "lodash";

const s3bucket_comment_diss_path = `${process.env.REACT_APP_API_URL}/api/tasks/download_comment_bucket/`; // SVJT

const initialValues = {
  comment: "",
  delivery_date: "",
  status: "",
  action_req: "",
  po_number: "",
  file_name: [],
  pause_sla: 0,
};

const removeDropZoneFiles = (fileName, objRef, setErrors) => {
  var newArr = [];
  for (let index = 0; index < objRef.state.files.length; index++) {
    const element = objRef.state.files[index];

    if (fileName === element.name) {
    } else {
      newArr.push(element);
    }
  }

  var fileListHtml = newArr.map((file) => (
    <Alert key={file.name}>
      <span onClick={() => removeDropZoneFiles(file.name, objRef, setErrors)}>
        <i className="far fa-times-circle"></i>
      </span>{" "}
      {file.name}
    </Alert>
  ));
  setErrors({ file_name: "" });
  objRef.setState({
    files: newArr,
    filesHtml: fileListHtml,
  });
};

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="right"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

class RespondCustomerPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      task_id: 0,
      showRespondCustomer: false,
      currRow: [],
      showModalLoader: false,
      filesHtml: "",
      showOnHoldSection: false,
      taskOnHold: [],
      selectedOnHoldTasks: [],
      warningMsg: "",
      show_pause_check: false,
      showNotification: false,
      newRespondCustomer: true,
      closureMinDate: new Date(),
      task_status:[],
      task_action_required:[],
      show_lang: "EN",
      switchDisabled:false,
      showSwitch:false,
      switchChecked:false,
      language_validation_text:0,
      sub_tasks_comments:[],
      sub_tasks_listing:[],
      sub_tasks_files:[],
      selected_sub_tasks:[],
      sel_subs:[],
      append_comment:'',
      suggestionText : [],
      suggested_value : ''
    };
  }

  componentWillReceiveProps = (nextProps) => {
    //console.log('props ===>', nextProps);
    if (
      nextProps.showRespondCustomer === true &&
      nextProps.currRow.task_id > 0
    ) {
      this.setState({sel_subs:[]});
      API.get(`/api/tasks/get_sub_tasks/${nextProps.currRow.task_id}`)
        .then((res) => {
          // console.log(res.data.data);

          API.get(`/api/feed/task_status`)
          .then((res) => {
            var task_status = [];
            for (let index = 0; index < res.data.data.length; index++) {
              const element = res.data.data[index];
              task_status.push({
                value: element["status_id"],
                label: htmlDecode(element["status_name"]),
              });
            }
            this.setState({ task_status: task_status });

            API.get(`/api/feed/task_action_required`)
            .then((res) => {
              var task_action_required = [];
              for (let index = 0; index < res.data.data.length; index++) {
                const element = res.data.data[index];
                task_action_required.push({
                  value: element["action_id"],
                  label: htmlDecode(element["action_name"]),
                });
              }
              this.setState({ task_action_required: task_action_required });

              

            })
            .catch((err) => {
              console.log(err);
            });

          })
          .catch((err) => {
            console.log(err);
          });

          let sub_tasks_comments = [];
          let sub_tasks_listing  = [];
          if(res.data.task_comments && res.data.task_comments.length > 0){
            sub_tasks_comments = res.data.task_comments;

            let arr = [];
            for (let index = 0; index < sub_tasks_comments.length; index++) {
              const element = sub_tasks_comments[index];
              if(!arr.includes(element.task_id)){
                sub_tasks_listing.push({label:element.task_ref,value:element.task_id});
                arr.push(element.task_id);
              }
            }

          }

          let sub_task_files = [];
          if(res.data.sub_task_files && res.data.sub_task_files.length > 0){
            sub_task_files = res.data.sub_task_files;
          }

          this.setState({
            showIncreaseSla: nextProps.showIncreaseSla,
            currRow: nextProps.currRow,
            sub_tasks: res.data.data,
            sub_tasks_comments: sub_tasks_comments,
            sub_tasks_listing: sub_tasks_listing,
            sub_tasks_files:sub_task_files
          });

          this.setState({ show_pause_check: false });

          if(nextProps.currRow.language != 'en'){
            this.setState({showSwitch:true});
            if(nextProps.currRow.is_spoc === true){
              this.setState({switchDisabled:false,switchChecked:true,show_lang:nextProps.currRow.language.toUpperCase()});
            }else{
              this.setState({switchDisabled:true,show_lang:'EN'});
            }
          }

          if (
            nextProps.currRow.select_genpact &&
            nextProps.currRow.select_genpact === 1
          ) {
            this.setState({
              showGenpactLoader: false,
            });
          }

          if (
            nextProps.custResp.action_req != "" &&
            inArray(nextProps.custResp.action_req, [
              "Document Required",
              "Details Required",
            ]) &&
            !inArray(nextProps.currRow.request_type, [24, 25, 26])
          ) {
            //this.setState({show_pause_check:true,showNotification:true,newRespondCustomer:false});
            initialValues.pause_sla = nextProps.custResp.pause_sla;
            if (nextProps.custResp.pause_sla === 1) {
              this.setState({
                show_pause_check: true,
                showNotification: true,
                newRespondCustomer: false,
              });
            } else {
              this.setState({
                show_pause_check: true,
                showNotification: false,
                newRespondCustomer: false,
              });
            }
          } else {
            this.setState({
              show_pause_check: false,
              showNotification: false,
              newRespondCustomer: true,
            });
          }

          if(this.state.currRow.language == 'en'){
            initialValues.comment = this.props.custResp.original_comment;
          }else{
            if(nextProps.currRow.is_spoc === true){
              if(this.state.currRow.language == this.props.custResp.original_language){
                initialValues.comment =this.props.custResp.original_comment;
              }else{
                initialValues.comment = this.props.custResp.translated_comment;
              }
            }else{
              if(this.state.currRow.language == this.props.custResp.original_language){
                initialValues.comment = this.props.custResp.translated_comment;
              }else{
                initialValues.comment =this.props.custResp.original_comment;
              }
            }
            
          }

          let new_comment = initialValues.comment.replace(/(\r\n|\n|\r)/gm,"");
          if(initialValues.comment != '' && sub_tasks_comments.length > 0){
            for (let index = 0; index < sub_tasks_comments.length; index++) {
        
              let search = initialValues.comment.search(`id="sub_task_${sub_tasks_comments[index].task_id}"`);
              
              if(search != '-1'){
                
                const reg = String.raw`<div\s+id="sub_task_${sub_tasks_comments[index].task_id}">`;
    
                let regex = new RegExp(reg, 'gi')
      
                new_comment = new_comment.replace(regex,"<div>");
                
              }
            }
          }

          this.setState({
            showRespondCustomer: nextProps.showRespondCustomer,
            currRow: nextProps.currRow,
            files: [],
            filesHtml: "",
            append_comment:new_comment,
            warningMsg:
              nextProps.currRow.customer_status === 3
                ? "Messages will not reach customer since the account is inactive"
                : "",
          });
        })
        .catch((err) => {
          console.log(err);
        });
        API.get(
          `/api/tasks/get_suggestions/3`
        )
          .then((res) => {
            this.setState({
              suggestionText: res.data.data,
              showRespondCustomer: nextProps.showRespondCustomer,
              currRow: nextProps.currRow
            });
          })
          .catch((err) => {
            console.log(err);
          });
    }
  };

  appendComments = (event,setFieldValue,prev_comment) => {
    //console.log(document.querySelector("[id^='tiny-react']").contentWindow.document.querySelectorAll("[id^='sub_task_']"));
    // console.log(document.querySelector("[id^='tiny-react']").contentWindow.document.querySelectorAll(`[id='tinymce']`).innerHTML)
    
    // console.log(event);
    if(event === null || event.length == 0){
      for (let index = 0; index < this.state.sub_tasks_listing.length; index++) {
        
        let search = prev_comment.search(`id="sub_task_${this.state.sub_tasks_listing[index].value}"`);
        
        if(search != '-1'){
          
          //document.querySelector("[id^='tiny-react']").contentWindow.document.querySelectorAll(`[id='sub_task_${this.state.sub_tasks_listing[index].value}']`)[0].remove();

          // let comment = this.state.sub_tasks_comments.filter((val)=>{
          //   return val.task_id == this.state.sub_tasks_listing[index].value
          // })
          
          // var sortedObjs = _.sortBy( comment, ['comment_id'],['asc'] ).map((obj)=>{
          //   return `${obj.posted_by}:${ReactHtmlParser(obj.comment)}-${obj.post_date}`;
          // }).join('');

          // let html = `<div id="sub_task_${this.state.sub_tasks_listing[index].value}">${sortedObjs}</div>`;

          const reg = String.raw`<div\s+id="sub_task_${this.state.sub_tasks_listing[index].value}">[\S\s]*?<\/div>`;

          let regex = new RegExp(reg, 'gi')

          let space_prev_comment = prev_comment.replace(/(\r\n|\n|\r)/gm,"");

          prev_comment = space_prev_comment.replace(regex,"");

          let prev_state = this.state.selected_sub_tasks;

          // console.log('prev_state of files',prev_state);

          if(prev_state.length > 0){
            let new_arr = [];
            for (let index2 = 0; index2 < prev_state.length; index2++) {
              if(prev_state[index2].task_id === this.state.sub_tasks_listing[index].value){
                
              }else{
                new_arr.push(prev_state[index2]);
              }
            }
            this.setState({selected_sub_tasks:new_arr});
          }

        }

      }

      setFieldValue("comment",`${prev_comment}`);
      this.setState({ sel_subs: [] });
    }else{
      let comment_arr = [];
      let current_events = []

      for (let index = 0; index < event.length; index++) {
        let search = prev_comment.search(`id="sub_task_${event[index].value}"`);
        // console.log(search)

        if(search == '-1'){
          let comment = this.state.sub_tasks_comments.filter((val)=>{
            return val.task_id == event[index].value
          })
          
          var sortedObjs = _.sortBy( comment, ['comment_id'],['asc'] ).map((obj)=>{
            return `<p style="margin-top: 40px" >${obj.posted_by}:${ReactHtmlParser(obj.comment)}-${obj.post_date}</p>`;
          }).join('');

          comment_arr.push(`<div id="sub_task_${event[index].value}">${sortedObjs}</div>`);

          if(this.state.sub_tasks_files.length > 0){
            let prev_state = this.state.selected_sub_tasks;

            for (let index2 = 0; index2 < this.state.sub_tasks_files.length; index2++) {
              if(this.state.sub_tasks_files[index2].task_id === event[index].value){
                prev_state.push(this.state.sub_tasks_files[index2]);
              }
            }
            // console.log('insert',prev_state);
            this.setState({selected_sub_tasks:prev_state});
          }

        }
        current_events.push(event[index].value);
      }

      
      if(current_events.length > 0){
        for (let index = 0; index < this.state.sub_tasks_listing.length; index++) {
        
          let search = prev_comment.search(`id="sub_task_${this.state.sub_tasks_listing[index].value}"`);
          
          if(search != '-1' && !current_events.includes(this.state.sub_tasks_listing[index].value)){
            
            //document.querySelector("[id^='tiny-react']").contentWindow.document.querySelectorAll(`[id='sub_task_${this.state.sub_tasks_listing[index].value}']`)[0].remove();
  
            // let comment = this.state.sub_tasks_comments.filter((val)=>{
            //   return val.task_id == this.state.sub_tasks_listing[index].value
            // })
            
            // var sortedObjs = _.sortBy( comment, ['comment_id'],['asc'] ).map((obj)=>{
            //   return `${obj.posted_by}:${ReactHtmlParser(obj.comment)}-${obj.post_date}`;
            // }).join('');
  
            //let html = `<div id="sub_task_${this.state.sub_tasks_listing[index].value}">${sortedObjs}</div>`;

            const reg = String.raw`<div\s+id="sub_task_${this.state.sub_tasks_listing[index].value}">[\S\s]*?<\/div>`;

            let regex = new RegExp(reg, 'gi')
  
            let space_prev_comment = prev_comment.replace(/(\r\n|\n|\r)/gm,"");
  
            prev_comment = space_prev_comment.replace(regex,"");

            let prev_state = this.state.selected_sub_tasks;
            if(prev_state.length > 0){
              let new_arr = [];
              for (let index2 = 0; index2 < prev_state.length; index2++) {
                if(prev_state[index2].task_id === this.state.sub_tasks_listing[index].value){
                  
                }else{
                  new_arr.push(prev_state[index2]);
                }
              }
              this.setState({selected_sub_tasks:new_arr});
            }
  
          }
  
        }
      }

      setFieldValue("comment",`${prev_comment}${comment_arr.join('')}`);

      this.setState({ sel_subs: event });
    }
  }

  handleClose = () => {
    var close = {
      showRespondCustomer: false,
      task_id: 0,
      files: [],
      filesHtml: "",
      language_validation_text:0
    };
    this.setState(close);
    this.props.handleClose(close);
    if(this.props.reloadOnClose){
      this.props.reloadTask();
    }
  };

  handleChange = (e, field) => {
    this.setState({
      [field]: e.target.value,
    });
  };

  handleRespond = (values, actions) => {
    this.setState({ showModalLoader: true });
    var delivery_date = dateFormat(values.delivery_date, "yyyy-mm-dd");
    var formData = new FormData();

    if (this.state.files && this.state.files.length > 0) {
      for (let index = 0; index < this.state.files.length; index++) {
        const element = this.state.files[index];
        formData.append("file", element);
      }
      //formData.append('file', this.state.file);
    } else {
      formData.append("file", "");
    }

    formData.append("status", values.status);
    formData.append("comment", values.comment);
    formData.append("delivery_date", delivery_date);
    formData.append("action_req", values.action_req);
    formData.append("po_number", values.po_number);
    formData.append("posted_for", this.state.currRow.assigned_to);
    formData.append("pause_sla", values.pause_sla == true ? 1 : 0);
    formData.append(
      "proforma_id",
      this.state.currRow.proforma_id > 0 ? this.state.currRow.proforma_id : 0
    );
    formData.append(
      "sub_tasks",
      this.state.sub_tasks.length > 0
        ? JSON.stringify(this.state.sub_tasks)
        : JSON.stringify([])
    );

    formData.append("spoc",(this.state.currRow.is_spoc)?1:0);
    formData.append("lang",this.state.show_lang);
    formData.append("assign_id", this.state.currRow.assignment_id);
    formData.append("language_validation",(values.language_validation)?values.language_validation:0);

    formData.append(
      "selected_sub_tasks",
      this.state.selected_sub_tasks.length > 0
        ? JSON.stringify(this.state.selected_sub_tasks)
        : JSON.stringify([])
    );

    // const postData = {
    //     status:values.status,
    //     comment:values.comment,
    //     delivery_date:delivery_date,
    //     action_req:values.action_req
    // };

    const config = {
      headers: {
        "content-type": "multipart/form-data",
      },
    };

    API.post(
      `/api/my_team_tasks/respond_customer/${this.state.currRow.task_id}`,
      formData,
      config
    )
      .then((res) => {
        this.setState({ showModalLoader: false });
        this.handleClose();
        if(res.data.show_message == 1){
          swal({
            closeOnClickOutside: false,
            title: "Awaiting review",
            text: res.data.message,
            icon: "success",
          }).then(() => {
            this.props.reloadTask();
          });
        }else{
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Response sent to customer.",
            icon: "success",
          }).then(() => {
            this.props.reloadTask();
          });
        }
      })
      .catch((error) => {
        this.setState({ showModalLoader: false });
        if (error.data.status === 3) {
          this.handleClose();
          var token_rm = 2;
          showErrorMessageFront(error, token_rm, this.props);
        } else {
          //console.log(error.data.errors);
          actions.setErrors(error.data.errors);
          actions.setSubmitting(false);
        }
      });
  };

  getDeliveryDate = () => {
    if (typeof this.state.currRow !== "undefined") {
      var delivery_date = dateFormat(
        localDate(this.state.currRow.delivery_date),
        "yyyy-mm-dd"
      );
      return new Date(delivery_date);
    }
  };
  getSuggestionText = (setFieldValue) => {
    return this.state.suggestionText.map((values) => {
      return (
        <>
          <span style={{ cursor: 'pointer', border: '1px solid black', padding: '5px 5px' }} onClick={() => this.addSuggestionText(values.name, setFieldValue)} >{htmlDecode(values.name)}</span>
        </>
      );
    })
  }
  addSuggestionText = (values, setFieldValue) => {
    let prev_state = this.state.append_comment;
    let newComment = `${prev_state} ${values}`;
    setFieldValue("comment", newComment);
  }

  changeDate = (event, setFieldValue) => {
    if (event === null) {
      setFieldValue("delivery_date", this.getDeliveryDate());
    } else {
      setFieldValue("delivery_date", event);
    }
  };

  setDropZoneFiles = (acceptedFiles, setErrors, setFieldValue) => {
    //console.log(acceptedFiles);
    setErrors({ file_name: false });
    setFieldValue(this.state.files);

    var prevFiles = this.state.files;
    var newFiles;
    if (prevFiles.length > 0) {
      //newFiles = newConcatFiles = acceptedFiles.concat(prevFiles);

      for (let index = 0; index < acceptedFiles.length; index++) {
        var remove = 0;

        for (let index2 = 0; index2 < prevFiles.length; index2++) {
          if (acceptedFiles[index].name === prevFiles[index2].name) {
            remove = 1;
            break;
          }
        }

        //console.log('remove',acceptedFiles[index].name,remove);

        if (remove === 0) {
          prevFiles.push(acceptedFiles[index]);
        }
      }
      //console.log('acceptedFiles',acceptedFiles);
      //console.log('prevFiles',prevFiles);
      newFiles = prevFiles;
    } else {
      newFiles = acceptedFiles;
    }

    this.setState({
      files: newFiles,
    });

    var fileListHtml = newFiles.map((file) => (
      <Alert key={file.name}>
        <span onClick={() => removeDropZoneFiles(file.name, this, setErrors)}>
          <i className="far fa-times-circle"></i>
        </span>{" "}
        {trimString(25, file.name)}
      </Alert>
    ));

    this.setState({
      filesHtml: fileListHtml,
    });
  };

  onHoldToggle = (event, setFieldValue) => {
    //console.log('task details',this.state.currRow);
    //setFieldValue('status',event.target.value);
    // if(event.target.value === 'On Hold' ){
    //     var task_refs = [];
    //     task_refs.push({task_ref:this.state.currRow.task_ref,task_id:this.state.currRow.task_id});
    //     for (let index = 0; index < this.state.currRow.sub_tasks.length; index++) {
    //         const element = this.state.currRow.sub_tasks[index];
    //         if(element.discussion == 1){
    //         }else{
    //             task_refs.push({task_ref:element.task_ref,task_id:element.task_id});
    //         }
    //     }
    //     this.setState({showOnHoldSection:true,taskOnHold:task_refs});
    // }else{
    //     this.setState({showOnHoldSection:false,taskOnHold:[]});
    // }
  };

  toggleCheckbox = (event, setFieldValue) => {
    let prevState = this.state.selectedOnHoldTasks;
    if (event.target.checked === true) {
      prevState.push(event.target.value);
    } else {
      for (let index = 0; index < prevState.length; index++) {
        const element = prevState[index];

        if (element === event.target.value) {
          prevState.splice(index, 1);
        }
      }
    }
    this.setState({ selectedOnHoldTasks: prevState });
  };

  showHidePauseSla = (e, setFieldValue, refObjState) => {
    this.setState({ showNotification: false });
    setFieldValue("action_req", e.target.value);
    if (
      inArray(e.target.value, ["Document Required", "Details Required"]) &&
      !inArray(this.state.currRow.request_type, [24, 25, 26])
    ) {
      this.setState({ show_pause_check: true });
      /* if(this.props.custResp.pause_sla === 1){
                setFieldValue('pause_sla',true);
            }else{
                setFieldValue('pause_sla',false);
            } */
    } else {
      this.setState({ show_pause_check: false });
      setFieldValue("pause_sla", false);
    }
  };

  handlePauseSla = (e, setFieldValue) => {
    if (e.target.checked) {
      this.setState({ showNotification: true });
    } else {
      this.setState({ showNotification: false });
    }
    setFieldValue("pause_sla", e.target.checked);
  };

  downloadFile = (e, file_url, file_name) => {
    e.preventDefault();
    let a = document.createElement("a");
    a.href = file_url;
    a.download = file_name;
    a.click();
  };

  removeProformaGenpactFile = () => {
    let prevState = this.state.currRow;
    prevState.download_url = "";
    prevState.file_name = "";
    prevState.proforma_id = 0;
    this.setState({ currRow: prevState });
  };

  showAttachments = () => {
    var fileListHtml = (
      <Alert key={this.state.currRow.file_name}>
        <span onClick={() => this.removeProformaGenpactFile()}>
          <i className="far fa-times-circle" style={{ cursor: "pointer" }}></i>
        </span>{" "}
        {/* <span onClick={e => this.downloadFile(e,this.state.currRow.download_url,this.state.currRow.file_name)} ><i className="fa fa-download" style={{cursor:'pointer'}}></i></span> */}
        <a href={this.state.currRow.save_file}>
          <i
            className="fa fa-download"
            aria-hidden="true"
            style={{ cursor: "pointer" }}
          ></i>{" "}
          {this.state.currRow.file_name}
        </a>
      </Alert>
    );

    return fileListHtml;
  };

  showAttachmentsSubTasks = () => {
    var fileListHtml = this.state.selected_sub_tasks.map((file) => (
      <Alert key={file.tcu_id}>
        <span onClick={() => this.removeSubtasksFiles(file.tcu_id)}>
          <i className="far fa-times-circle" style={{ cursor: "pointer" }}></i>
        </span>{" "}
        <span
          onClick={(e) =>
            this.redirectUrlTask(e, `${s3bucket_comment_diss_path}${file.tcu_id}`)
          }
        >
          <i className="fa fa-download" style={{ cursor: "pointer" }}></i>
        </span>{" "}
        {file.actual_file_name}
      </Alert>
    ));

    return fileListHtml;
  };

  redirectUrlTask = (event, path) => {
    event.preventDefault();
    window.open(path, "_self");
  };

  removeSubtasksFiles = (tcu_id) => {
    let prev_state = this.state.selected_sub_tasks;
    for (let index = 0; index < prev_state.length; index++) {
      if(tcu_id == prev_state[index].tcu_id){
        prev_state.splice(index,1);
      }
    }
    this.setState({selected_sub_tasks:prev_state});
  }

  changeSubBackDate = (event, setFieldValue, name) => {
    // console.log(event, setFieldValue, name);
    //event.preventDefault();
    if (event === null) {
    } else {
      let tempTaskArr = [];

      // update checked property to customer list
      this.state.sub_tasks.map((sub_tasks, i) => {
        if (`back_due_date_${sub_tasks.task_id}` === name) {
          sub_tasks.assign_due_date = dateFormat(event, "yyyy-mm-dd HH:MM:ss");
        }
        tempTaskArr.push(sub_tasks);
      });

      // // update state
      this.setState({
        sub_tasks: tempTaskArr,
      });

      //setFieldValue(name, event);
    }
  };

  selectSubTaskChecked = (event, setFieldValue, name) => {
    let tempTaskArr = [];

    // update checked property to customer list
    this.state.sub_tasks.map((sub_tasks, i) => {
      if (`sub_task_refs_${sub_tasks.task_id}` === event.target.name) {
        sub_tasks.pause_sla = event.target.checked ? 1 : 0;
      }
      tempTaskArr.push(sub_tasks);
    });
    // // update state
    this.setState({
      sub_tasks: tempTaskArr,
    });

    setFieldValue(name, event.target.checked);
  };

  checkHandler = (event) => {
    event.preventDefault();
  };

  toggleLanguage = (e,setFieldValue) => {
    //e.preventDefault();
    this.setState({sel_subs:[]});
    this.setState({ switchChecked: !this.state.switchChecked });

    if (this.state.show_lang === "EN") {
      if(this.state.currRow.language == this.props.custResp.original_language){
        setFieldValue('comment',this.changeComments(this.props.custResp.original_comment));
      }else{
        setFieldValue('comment',this.changeComments(this.props.custResp.translated_comment));
      }
      
      this.setState({
        show_lang: this.state.currRow.language.toUpperCase(),
      });
    } else {
      if(this.state.currRow.language == this.props.custResp.original_language){
        setFieldValue('comment',this.changeComments(this.props.custResp.translated_comment));
      }else{
        setFieldValue('comment',this.changeComments(this.props.custResp.original_comment));
      }
      this.setState({ show_lang: "EN" });
    }
  };

  changeComments = (comment) => {
    let new_comment = '';
    if(comment != '' && this.state.sub_tasks_comments.length > 0){
      new_comment = comment.replace(/(\r\n|\n|\r)/gm,"");
      for (let index = 0; index < this.state.sub_tasks_comments.length; index++) {
  
        let search = comment.search(`id="sub_task_${this.state.sub_tasks_comments[index].task_id}"`);
        
        if(search != '-1'){
          
          const reg = String.raw`<div\s+id="sub_task_${this.state.sub_tasks_comments[index].task_id}">`;

          let regex = new RegExp(reg, 'gi')

          new_comment = new_comment.replace(regex,"<div>");

          let prev_state = this.state.selected_sub_tasks;
          if(prev_state.length > 0){
            let new_arr = [];
            this.setState({selected_sub_tasks:new_arr});
          }

        }
      }
    }
    return new_comment
  }

  render() {
    let validateStopFlag = '';
    if(this.state.currRow.language != 'en' && !this.state.is_spoc){
      validateStopFlag = Yup.object().shape({
        comment: Yup.string().trim().required("Please enter description"),
        status: Yup.string().trim().required("Please select status"),
        delivery_date: Yup.string().trim().required("Please delivery date"),
        action_req: Yup.string().trim().required("Please enter required action"),
        language_validation: Yup.string().trim().required("Please select Review or Send Directly To Customer")
      });
    }else{
      validateStopFlag = Yup.object().shape({
        comment: Yup.string().trim().required("Please enter description"),
        status: Yup.string().trim().required("Please select status"),
        delivery_date: Yup.string().trim().required("Please delivery date"),
        action_req: Yup.string().trim().required("Please enter required action"),
      });
    }

    const newInitialValues = Object.assign(initialValues, this.props.custResp);
    // const newInitialValues = Object.assign(initialValues, {
    //     delivery_date    : this.getDeliveryDate()
    // });
    return (
      <>
        <Modal
          show={this.state.showRespondCustomer}
          onHide={() => this.handleClose()}
          backdrop="static"
        >
          <Formik
            initialValues={newInitialValues}
            validationSchema={validateStopFlag}
            onSubmit={this.handleRespond}
          >
            {({
              values,
              errors,
              touched,
              isValid,
              isSubmitting,
              setFieldValue,
              setFieldTouched,
              setErrors,
            }) => {
              return (
                <Form encType="multipart/form-data">
                  {this.state.showModalLoader === true ? (
                    <div className="loderOuter">
                      <div className="loader">
                        <img src={loaderlogo} alt="logo" />
                        <div className="loading">Loading...</div>
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                  <Modal.Header closeButton className="respondcus">
                    <Modal.Title>Respond To Customer
                    {this.state.showSwitch && <><LinkWithTooltip
                        tooltip={`${(this.state.switchDisabled)?'This feature is disabled for you':'Please click to toggle between languages'}`}
                        href="#"
                        id={`tooltip-menu-${this.state.currRow.task_id}`}
                        clicked={(e) => this.checkHandler(e)}
                      >
                        <Switch
                          checked={this.state.switchChecked}
                          onChange={(e) => this.toggleLanguage(e,setFieldValue)}
                          uncheckedIcon={"EN"}
                          checkedIcon={(this.state.currRow.language.toUpperCase() === 'ZH'?'MA':this.state.currRow.language.toUpperCase())}
                          className="react-switch"
                          id="icon-switch"
                          disabled={this.state.switchDisabled}
                        />
                      </LinkWithTooltip></>} 
                      </Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    {this.state.warningMsg !== "" && (
                      <p style={{ color: "#e6021d", fontSize: "12px" }}>
                        {this.state.warningMsg}
                      </p>
                    )}

                    <div className="contBox">
                      <Row>
                        {this.state.showNotification === true &&
                          this.state.newRespondCustomer === false && (
                            <Col xs={12} sm={12} md={12}>
                              <span className="errorMsg mb-20">
                                Request is on Hold.
                              </span>
                            </Col>
                          )}

                        <Col xs={12} sm={6} md={6}>
                          <div className="form-group react-date-picker react-date-picker-with-0pos expected-errorfiled">
                            <label>Expected Closure Date</label>
                            <div className="form-control">
                              {/* <DatePicker
                                                name="delivery_date"
                                                onChange={value =>                                                
                                                    setFieldValue("delivery_date", this.changeDateFormat(value))
                                                }
                                                value={values.delivery_date}
                                            /> */}

                              <DatePicker
                                name={"delivery_date"}
                                className="borderNone"
                                minDate={this.state.closureMinDate}
                                selected={
                                  values.delivery_date
                                    ? values.delivery_date
                                    : null
                                }
                                //onChange={value => setFieldValue('delivery_date', value)}
                                dateFormat="dd/MM/yyyy"
                                autoComplete="off"
                                onChange={(e) => {
                                  this.changeDate(e, setFieldValue);
                                }}
                              />

                              {errors.delivery_date && touched.delivery_date ? (
                                <span className="errorMsg">
                                  {errors.delivery_date}
                                </span>
                              ) : null}
                            </div>
                          </div>
                        </Col>

                        <Col xs={12} sm={6} md={6}>
                          <div className="form-group">
                            <label>Required Action</label>
                            <Field
                              name="action_req"
                              component="select"
                              className={`selectArowGray form-control`}
                              autoComplete="off"
                              value={values.action_req}
                              onChange={(e) => {
                                this.showHidePauseSla(
                                  e,
                                  setFieldValue,
                                  this.state
                                );
                              }}
                            >
                              <option key="-1" value="">
                                Select
                              </option>
                              <option key="1" value="Acknowledgment">
                                Acknowledgment
                              </option>
                              <option key="2" value="Document Required">
                                Document Required
                              </option>
                              <option key="3" value="Details Required">
                                Details Required
                              </option>
                              <option key="4" value="No Action Required">
                                No Action Required
                              </option>
                            </Field>

                            {errors.action_req && touched.action_req ? (
                              <span className="errorMsg">
                                {errors.action_req}
                              </span>
                            ) : null}
                          </div>
                        </Col>

                        {this.state.show_pause_check === true && (
                          <Col xs={12} sm={12} md={12} className="mb-20">
                            <div className="form-group mb-0">
                              <label className="checkbox-inline">
                                <Field
                                  type="checkbox"
                                  name="pause_sla"
                                  value="1"
                                  onChange={(e) =>
                                    this.handlePauseSla(e, setFieldValue)
                                  }
                                  defaultChecked={values.pause_sla}
                                />
                                Put this Request On Hold
                                <LinkWithTooltip
                                  tooltip={`Selecting this will put request on Hold and SLA will be paused. Once unchecked the due dates shall be adjusted accordingly.
                                                `}
                                  href="#"
                                  id={`tooltip-menu-${this.state.currRow.task_id}`}
                                  clicked={(e) => this.checkHandler(e)}
                                >
                                  <i
                                    className="fa fa-exclamation-circle"
                                    aria-hidden="true"
                                  ></i>
                                </LinkWithTooltip>
                              </label>
                            </div>
                            {this.state.showNotification === true &&
                              this.state.newRespondCustomer === true && (
                                <span className="errorMsg">
                                  Please note SLA for this request will be
                                  paused and notification email shall be sent to
                                  the Customer.
                                </span>
                              )}
                          </Col>
                        )}
                      </Row>

                      {this.state.show_pause_check === true &&
                        this.state.showNotification &&
                        this.state.sub_tasks.length > 0 && (
                          <Row>
                            {this.state.sub_tasks.map((sub_tasks, index) => {
                              return (
                                <div key={index} className="col-md-12">
                                  <div className="mobFull">
                                    <div className="form-group checText mb-15">
                                      <input
                                        type="checkbox"
                                        name={`sub_task_refs_${sub_tasks.task_id}`}
                                        value={sub_tasks.task_id}
                                        checked={
                                          sub_tasks.pause_sla === 1
                                            ? true
                                            : false
                                        }
                                        onChange={(e) => {
                                          this.selectSubTaskChecked(
                                            e,
                                            setFieldValue,
                                            `sub_task_refs_${sub_tasks.task_id}`
                                          );
                                        }}
                                      />
                                      <label>
                                        {htmlDecode(sub_tasks.task_ref)}
                                      </label>
                                    </div>
                                  </div>
                                  <div className="clearFix"></div>
                                </div>
                              );
                            })}
                          </Row>
                        )}

                      {this.state.currRow.request_type !== 23 &&
                        this.state.currRow.request_type !== 43 &&
                        this.state.currRow.request_type !== 41 &&
                        this.state.currRow.request_type !== 25 &&
                        this.state.currRow.request_type !== 7 &&
                        this.state.currRow.request_type !== 34 && (
                          <Row>
                            {" "}
                            <Col xs={12} sm={12} md={12}>
                              <div className="form-group">
                                <label>Status</label>
                                <Field
                                  name="status"
                                  component="select"
                                  className={`selectArowGray form-control`}
                                  autoComplete="off"
                                  value={values.status}
                                >
                                  <option key="-1" value="">
                                    Select
                                  </option>
                                  <option key="1" value="Acknowledge">
                                    Acknowledge
                                  </option>
                                  <option key="2" value="Confirmation">
                                    Confirmation
                                  </option>
                                  <option key="3" value="In Progress">
                                    In Progress
                                  </option>
                                  <option key="4" value="Closed">
                                    Closed
                                  </option>
                                </Field>

                                {errors.status && touched.status ? (
                                  <span className="errorMsg">
                                    {errors.status}
                                  </span>
                                ) : null}
                              </div>
                            </Col>
                          </Row>
                        )}

                      {this.state.currRow.request_type === 25 && (
                        <Row>
                          {" "}
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>Payment Status</label>
                              <Field
                                name="status"
                                component="select"
                                className={`selectArowGray form-control`}
                                autoComplete="off"
                                value={values.status}
                              >
                                <option key="-1" value="">
                                  Select
                                </option>
                                <option key="1" value="Pending">
                                  Pending
                                </option>
                                <option key="2" value="Payment Received">
                                  Payment Received
                                </option>
                              </Field>

                              {errors.status && touched.status ? (
                                <span className="errorMsg">
                                  {errors.status}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                        </Row>
                      )}

                      {(this.state.currRow.request_type === 23 || this.state.currRow.request_type === 43 ||
                        this.state.currRow.request_type === 41) && (
                        <Row>
                          {" "}
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>Status</label>
                              <Field
                                name="status"
                                component="select"
                                className={`selectArowGray form-control`}
                                autoComplete="off"
                                value={values.status}
                              >
                                <option key="-1" value="">
                                  Select
                                </option>
                                <option key="1" value="PO Acknowledged">
                                  PO Acknowledged
                                </option>
                                <option key="2" value="PO Accepted">
                                  PO Accepted
                                </option>
                                <option key="3" value="Production in-progress">
                                  Production In-Progress
                                </option>
                                {/* <option key='4' value="Batch Released" >Batch Released</option> */}
                                <option
                                  key="5"
                                  value="Shipping documents shared"
                                >
                                  Shipping Documents Shared
                                </option>
                                <option key="6" value="Dispatched">
                                  Dispatched
                                </option>
                              </Field>

                              {errors.status && touched.status ? (
                                <span className="errorMsg">
                                  {errors.status}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                        </Row>
                      )}

                      {(this.state.currRow.request_type === 23 || this.state.currRow.request_type === 43) && (
                        <Row>
                          {" "}
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>PO Number:</label>
                              <Field
                                name="po_number"
                                type="text"
                                className={`selectArowGray form-control`}
                                autoComplete="off"
                                value={values.po_number}
                              ></Field>
                            </div>
                          </Col>
                        </Row>
                      )}

                      {(this.state.currRow.request_type === 7 ||
                        this.state.currRow.request_type === 34) && (
                        <Row>
                          {" "}
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>Status</label>
                              <Field
                                name="status"
                                component="select"
                                className={`selectArowGray form-control`}
                                autoComplete="off"
                                value={values.status}
                              >
                                <option key="-1" value="">
                                  Select
                                </option>
                                <option key="1" value="Acknowledge">
                                  Acknowledge
                                </option>
                                <option key="2" value="Accepted / Rejected">
                                  Accepted / Rejected
                                </option>
                                <option key="3" value="In Progress">
                                  In Progress
                                </option>
                                <option key="4" value="Closed">
                                  Closed
                                </option>
                              </Field>

                              {errors.status && touched.status ? (
                                <span className="errorMsg">
                                  {errors.status}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                        </Row>
                      )}

                      {/* <Row>
                                    <Col xs={12} sm={12} md={12}>       
                                        <div className="form-group">
                                        <input 
                                            id="file" 
                                            name="file" 
                                            type="file" 
                                            onChange={(event) => {                                            
                                                this.setState({ file : event.target.files[0] })
                                                setFieldValue("file", event.target.files[0]);
                                            }} 
                                        />
                                        </div>
                                    </Col>         
                                </Row> */}

                      {this.state.showOnHoldSection && (
                        <Row>
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>
                                Check Tasks To Be Put On Hold{" "}
                                <span className="required-field">*</span>
                              </label>
                              {this.state.taskOnHold &&
                                this.state.taskOnHold.length > 0 &&
                                this.state.taskOnHold.map((i) => {
                                  return (
                                    <>
                                      <Field
                                        type="checkbox"
                                        name={`samplesCheck${i.task_id}`}
                                        value={i.task_id}
                                        onChange={(e) => {
                                          this.toggleCheckbox(e);
                                        }}
                                      />
                                      {` `}
                                      {i.task_ref}
                                      <br />
                                    </>
                                  );
                                })}
                            </div>
                          </Col>
                        </Row>
                      )}

                      {this.state.sub_tasks_comments.length > 0 ? <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <label>
                              Select Sub Tasks
                            </label>
                            <Select
                              isMulti
                              className="basic-single"
                              classNamePrefix="select"
                              isClearable={true}
                              isSearchable={true}
                              name="sub_task"
                              options={this.state.sub_tasks_listing}
                              value={this.state.sel_subs}
                              onChange={(e) => {
                                this.appendComments(e,setFieldValue,this.state.append_comment);
                              }}
                            />
                          </div>
                        </Col>
                      </Row>:null}  
                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group closeTaskBodyInfo">
                            <label>
                              Use below tags for quick comments
                            </label>
                            {this.getSuggestionText(setFieldValue)}
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <label>
                              Comment <span className="required-field">*</span>
                              {(this.state.show_lang === 'EN')?'':<>{`    `}<span className="task-details-language" style={{color:'rgb(0, 86, 179)'}} >(The below message will be sent to the customer without any review.)</span></>}
                            </label>
                            {/* <TinyMCE
                                        name="comment"
                                        content={values.comment}
                                        config={{
                                            menubar: false,
                                            toolbar: 'undo redo | bold italic | alignleft aligncenter alignright'
                                        }}
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.comment}
                                        onChange={value =>
                                            setFieldValue("comment", value.level.content)
                                        } 
                                        onBlur={() => setFieldTouched("comment")}                                       
                                        /> */}
                            {/* <Field
                                            name="comment"
                                            content={values.comment}
                                            component='textarea'
                                            className={`selectArowGray form-control`}
                                            autoComplete="off"
                                            onBlur={() => setFieldTouched("comment")}
                                        /> */}

                            <Editor
                              name="content"
                              className={`selectArowGray form-control`}
                              value={
                                values.comment !== null && values.comment !== ""
                                  ? values.comment
                                  : ""
                              }
                              content={
                                values.comment !== null && values.comment !== ""
                                  ? values.comment
                                  : ""
                              }
                              init={{
                                menubar: false,
                                branding: false,
                                placeholder: "Enter comments",
                                plugins:
                                  "link table hr visualblocks code placeholder lists autoresize textcolor",
                                toolbar:
                                  "bold italic strikethrough superscript subscript | forecolor backcolor | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | visualblocks code | fontselect",
                                font_formats:
                                  "Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats",
                              }}
                              onEditorChange={(value) => {
                                setFieldValue("comment", value);
                                this.setState({append_comment:value});
                              }}
                            />

                            {errors.comment && touched.comment ? (
                              <span className="errorMsg">{errors.comment}</span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>

                      {this.state.currRow.language != 'en' && !this.state.is_spoc && 
                      <>
                      <Row> 
                        <Col xs={12} sm={6} md={6}>
                          <div className="form-group">
                            <label>Review or Send Directly To Customer <span className="required-field">*</span></label>
                            <Field
                              name="language_validation"
                              component="select"
                              className={`selectArowGray form-control`}
                              autoComplete="off"
                              onChange={(e) => {
                                setFieldValue('language_validation',e.target.value)
                                this.setState({'language_validation_text':parseInt(e.target.value)})
                              }}
                              value={values.language_validation}
                            >
                              <option key="-1" value="">
                                Select
                              </option>
                              <option key="1" value="1">
                                Send To Customer After SPOC Verification
                              </option>
                              <option key="2" value="2">
                                Send To Customer Directly
                              </option>
                            </Field>
                            {errors.language_validation && touched.language_validation ? (
                              <span className="errorMsg">
                                {errors.language_validation}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                        {(this.state.language_validation_text > 0) && (
                          <>
                            <div
                              className="task-details-language"
                              style={{
                                color: "rgb(0, 86, 179)",
                                fontWeight: "700",
                              }}
                            >
                              {this.state.language_validation_text === 2 && 'The current content will be sent to customer without any review or language verification.'}
                              {this.state.language_validation_text === 1 && 'The current translated content will be sent to customer after language review by SPOC.'}
                            </div>
                            <br/>
                          </>
                          
                        )}
                      </>
                      }

                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group custom-file-upload">
                            {/* <FilePond ref={ref => this.pond = ref}
                                            allowMultiple={true}
                                            maxFiles={10}
                                            onupdatefiles={fileItems => {
                                                // Set currently active file objects to this.state
                                                this.setState({
                                                    files: fileItems.map(fileItem => fileItem.file)
                                                });
                                            }}>
                                    </FilePond> */}

                            <Dropzone
                              onDrop={(acceptedFiles) =>
                                this.setDropZoneFiles(
                                  acceptedFiles,
                                  setErrors,
                                  setFieldValue
                                )
                              }
                            >
                              {({ getRootProps, getInputProps }) => (
                                <section>
                                  <div
                                    {...getRootProps()}
                                    className="custom-file-upload-header"
                                  >
                                    <input {...getInputProps()} />
                                    <p>Upload file</p>
                                  </div>
                                  <div className="custom-file-upload-area">
                                    {this.state.currRow.proforma_id > 0 && this.showAttachments()}
                                    {this.state.selected_sub_tasks.length > 0 && this.showAttachmentsSubTasks()}
                                    {this.state.filesHtml}
                                  </div>
                                </section>
                              )}
                            </Dropzone>
                            {errors.file_name || touched.file_name ? (
                              <span className="errorMsg errorExpandView">
                                {errors.file_name}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </Modal.Body>
                  <Modal.Footer>
                    <button
                      onClick={this.handleClose}
                      className={`btn-line`}
                      type="button"
                    >
                      Close
                    </button>
                    <button
                      className={`btn-fill ${
                        isValid ? "btn-custom-green" : "btn-disable"
                      } m-r-10`}
                      type="submit"
                      disabled={isValid ? false : true}
                    >
                      {this.state.stopflagId > 0
                        ? isSubmitting
                          ? "Updating..."
                          : "Update"
                        : isSubmitting
                        ? "Submitting..."
                        : "Submit"}
                    </button>
                  </Modal.Footer>
                </Form>
              );
            }}
          </Formik>
        </Modal>
      </>
    );
  }
}

export default RespondCustomerPopup;
