import React, { Component } from 'react';
import { getDashboard,getMyId,localDate } from '../../shared/helper';

import ellipsisImage from '../../assets/images/ellipsis-icon.svg';

import {
    Tooltip,
    OverlayTrigger
  } from "react-bootstrap";

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
    return (
      <OverlayTrigger
        overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
        placement="left"
        delayShow={300}
        delayHide={150}
        trigger={["hover"]}
      >
        {children}
      </OverlayTrigger>
    );
  }
  /*For Tooltip*/

class VerticalMenuClosedTask extends Component {

    constructor(){
        super();
        document.querySelector('body').addEventListener('click',function(event){
            var elems = document.querySelectorAll('.btn-group');
            for (var i = 0; i < elems.length; i++){
                elems[i].classList.remove('open');
            }
        });
    }

    requestToReopen = () => {
        this.props.requestToReopen(this.props.currRow);
    }

    handleVerticalMenu = (event) => {
        var elems = document.querySelectorAll('.btn-group');
        for (var i = 0; i < elems.length; i++){
            elems[i].classList.remove('open');
        }

        if(event.target.parentNode.classList.contains("btn-group")){
            event.target.parentNode.classList.add("open");
        }else if(event.target.parentNode.classList.contains("dropdown-toggle")){
            event.target.parentNode.parentNode.classList.add("open");
        }
        
    }

    checkDueDate = () =>{
        //var replacedDate = this.props.currRow.due_date.split('T');
        var dueDate = localDate(this.props.currRow.due_date);
        var today  = new Date();
        var timeDiff = dueDate.getTime() - today.getTime();
        if(timeDiff > 0 ){
            return false;
        }else{
            return true;
        }
    }

    deleteTaskToolTip = () => {
        return (
            <LinkWithTooltip
                tooltip={`The task has been cancelled`}
                href="#"
                id={`tooltip-menu-${this.props.currRow.task_id}`}
                clicked={e => this.checkHandler(e)}
            >
                <div className="actionStyle">
                    <div className="btn-group">
                        <i class="fa fa-ban" aria-hidden="true"></i>
                    </div>
                </div>
            </LinkWithTooltip>

        );
    }

    render() {

        var req_to_open,hide_menu = false;
        var my_id = getMyId();
        // console.log(this.props.currRow.parent_id);
        if(this.props.currRow.duplicate_task === 1){
            hide_menu = true;
        }else{
            if(this.props.currRow.parent_id === 0){
                req_to_open = true;
                
            }else{
                hide_menu = true;
            }
        }
        
        if(hide_menu){
            if(this.props.currRow.duplicate_task === 1){
                return (
                    <>
                        {this.deleteTaskToolTip()}
                    </>
                );
            }else{
                return null;
            }
        }else{
            return (
                <>
                <div className="actionStyle">
                    <div className="btn-group">
                        
                        <button type="button" className="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" onClick = {(e) =>   this.handleVerticalMenu(e)} >
                            <img src={ellipsisImage} alt="ellipsisImage" />
                        </button>
                        
                        <ul className="dropdown-menu pull-right" role="menu">  
                            {req_to_open && <li><span onClick={()=>this.requestToReopen()} style={{cursor:'pointer'}} ><i className="fas fa-undo" />Re-open Task</span></li>} 
                        </ul>
                    </div>
                </div>
                </>
            );
        }
    }
}

export default VerticalMenuClosedTask;
