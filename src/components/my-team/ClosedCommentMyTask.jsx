import React, { Component } from "react";
import { Row, Col, ButtonToolbar, Button, Modal, Alert } from "react-bootstrap";

//import { FilePond } from 'react-filepond';
//import 'filepond/dist/filepond.min.css';
import Dropzone from "react-dropzone";

import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import dateFormat from "dateformat";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";
import API from "../../shared/axios";
import { showErrorMessageFront } from "../../shared/handle_error_front";

import swal from "sweetalert";

import { getMyId,localDate, trimString,localDateTime,htmlDecode } from "../../shared/helper";

//import uploadAxios from 'axios';

import TinyMCE from "react-tinymce";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";

//console.log( TinyMCE );

// const onlyUnique = (value, index, self) => {
//     return self.indexOf(value) === index;
// }

const removeDropZoneFiles = (fileName, objRef, setErrors) => {
  var newArr = [];
  for (let index = 0; index < objRef.state.files.length; index++) {
    const element = objRef.state.files[index];

    if (fileName === element.name) {
    } else {
      newArr.push(element);
    }
  }

  var fileListHtml = newArr.map((file) => (
    <Alert key={file.name}>
      <span onClick={() => removeDropZoneFiles(file.name, objRef, setErrors)}>
        <i className="far fa-times-circle"></i>
      </span>{" "}
      {file.name}
    </Alert>
  ));
  setErrors({ file_name: "" });
  objRef.setState({
    files: newArr,
    filesHtml: fileListHtml,
  });
};

const initialValues = {
  employeeId: "",
  comment: ""
};

class RequestToReopenMyTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showClosedComment: false,
      files: [],
      sla_breach: false,
      showModalLoader: false,
      acceptNewEmp: false,
      acceptNewEmpMsg: "",
      stateEmplId: "",
      posted_for:0,
      filesHtml: "",
      comment:'',
    };
  }

  handleChange = (e, field) => {
    this.setState({
      [field]: e.target.value,
    });
  };

  componentWillReceiveProps = (nextProps) => {
    console.log(nextProps.showClosedComment);
    if (nextProps.showClosedComment === true && nextProps.currRow.task_id > 0) {
      
      var task_date_added = localDate(nextProps.currRow.task_date_added);

      var dueDate = localDateTime(nextProps.currRow.original_due_date);
      var today = localDateTime(nextProps.currRow.today_date);
      var timeDiff = dueDate.getTime() - today.getTime();
      let suggestion_type;

      let current_date  = new Date(nextProps.currRow.today_date);
      let add_date_24 = new Date(new Date(nextProps.currRow.task_date_added).getTime() + 60 * 60 * 24 * 1000);

      if(current_date < add_date_24){
        suggestion_type = 1;
      }else if(timeDiff < 0){
        suggestion_type = 2;
      }

      

      API.get(
        `/api/tasks/get_suggestions/${suggestion_type}`
      )
      .then((res) => {
        this.setState({
          suggestion_type:suggestion_type,
          suggestionText:res.data.data,
          showClosedComment: nextProps.showClosedComment,
          currRow: nextProps.currRow,
          posted_for:nextProps.posted_for
        });
      })
      .catch((err) => {
        console.log(err);
      });
    }
  };

  handleClose = () => {
    var close = {
      sla_breach: false,
      stateEmplId: "",
      new_employee: false,
      msg_new_employee: "",
      showModalLoader: false,
      showClosedComment: false,
      posted_for:0,
      comment:'',
      files: [],
      filesHtml: "",
    };
    this.setState(close);
    this.props.handleClose(close);
    if(this.props.reloadOnClose){
      this.props.reloadTask();
    }
  };

  handleRequestToReopen = (values, actions) => {

    if(this.state.comment === ''){
      actions.setErrors({ comment: "Please enter comment." });
      actions.setSubmitting(false);
      this.setState({ showModalLoader: false });
    }else{
      this.setState({ showModalLoader: true, msg_new_employee: false });
      var post_data = {
        comment: this.state.comment,
        comment_type:this.state.suggestion_type
      };

      API.delete(`/api/my_team_tasks/check_close/${this.state.currRow.task_id}/${this.state.posted_for}`,{data: post_data})
      .then((res) => {
        if (res.data.has_sub_task === 1) {
          this.setState({ showModalLoader: false });
          swal({
            closeOnClickOutside: false,
            title: "Alert",
            text:
              "This task has some open sub-tasks. \r\n Do you want to close them as well?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          }).then((willDelete) => {
            if (willDelete) {
              this.setState({ showModalLoader: true });
              API.delete(`/api/my_team_tasks/close/${this.state.currRow.task_id}/${this.state.posted_for}`,{data: post_data})
                .then((res) => {
                  this.setState({ showModalLoader: false });
                  swal({
                    closeOnClickOutside: false,
                    title: "Success",
                    text: "Task has been closed!",
                    icon: "success",
                  }).then(() => {
                    this.handleClose();
                    this.props.reloadTask();
                  });
                })
                .catch((err) => {
                  var token_rm = 2;
                  showErrorMessageFront(err, token_rm, this.props);
                });
            }
          });
        } else {
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Task has been closed!",
            icon: "success",
          }).then(() => {
            this.handleClose();
            this.props.reloadTask();
          });
        }
      })
      .catch((err) => {
        var token_rm = 2;
        showErrorMessageFront(err, token_rm, this.props);
      });
    }
  };

  getSuggestionText = (setFieldValue) => {
    return this.state.suggestionText.map((values)=>{
      return (
        <>
          <span style={{cursor:'pointer',border:'1px solid black',padding:'5px 5px'}} onClick={()=>this.addSuggestionText(values.name,setFieldValue)} >{htmlDecode(values.name)}</span>
        </>
      );
    })
  }

  addSuggestionText = (values,setFieldValue) => {
    let prev_state = this.state.comment;
    let newComment = `${prev_state} ${values}`;
    this.setState({comment:htmlDecode(newComment)});
  }

  handleTextChange = (e) => {
    this.setState({comment:e.target.value});
  }

  render() {
    return (
      <>
        {typeof this.state.suggestionText !== "undefined" && (
          <Modal
            show={this.state.showClosedComment}
            onHide={() => this.handleClose()}
            backdrop="static"
          >
            <Formik
              initialValues={initialValues}
              validationSchema={false}
              onSubmit={this.handleRequestToReopen}
            >
              {({
                values,
                errors,
                touched,
                isValid,
                isSubmitting,
                handleChange,
                setFieldValue,
                setFieldTouched,
                setErrors,
              }) => {
                /* console.log('values', values)
                        console.log('errors', errors)
                        console.log('touched', touched)
                        console.log('isValid', isValid) */
                return (
                  <Form encType="multipart/form-data">
                    {this.state.showModalLoader === true ? (
                      <div className="loderOuter">
                        <div className="loader">
                          <img src={loaderlogo} alt="logo" />
                          <div className="loading">Loading...</div>
                        </div>
                      </div>
                    ) : (
                      ""
                    )}
                    <Modal.Header closeButton>
                      <Modal.Title>
                        Close Task | {this.state.currRow.task_ref}
                      </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <div className="contBox">
                        <Row>
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group closeTaskBodyInfo">
                              <label>
                                Use below tags for quick comments
                              </label>
                              {this.getSuggestionText(setFieldValue)}
                            </div>
                          </Col>
                        </Row>
                        <Row>
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>
                                Please Enter Comment{" "}
                                <span className="required-field">*</span>
                              </label>
                              <Field
                                name="comment"
                                component="textarea"
                                className="form-control"
                                autoComplete="off"
                                onChange={(e)=>this.handleTextChange(e)}
                                value={this.state.comment}
                                style={{ height: "130px" }}
                              />
                              {errors.comment && touched.comment ? (
                                <span className="errorMsg">
                                  {errors.comment}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                        </Row>

                      </div>
                    </Modal.Body>
                    <Modal.Footer>
                      <button
                        onClick={this.handleClose}
                        className={`btn-line`}
                        type="button"
                      >
                        Cancel
                      </button>
                      <button
                        className={`btn-fill btn-custom-green m-r-10`}
                        type="submit"
                      >
                        Submit
                      </button>
                    </Modal.Footer>
                  </Form>
                );
              }}
            </Formik>
          </Modal>
        )}
      </>
    );
  }
}

export default RequestToReopenMyTask;
