import React, { Component } from "react";
import { Row, Col, ButtonToolbar, Button, Modal, Alert } from "react-bootstrap";
import { Formik, Form } from "formik";
import * as Yup from "yup";

//import { FilePond } from 'react-filepond';
//import 'filepond/dist/filepond.min.css';
import Dropzone from "react-dropzone";
import TinyMCE from "react-tinymce";

import API from "../../shared/axios";
import swal from "sweetalert";

//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
import { showErrorMessageFront } from "../../shared/handle_error_front";
import { trimString } from "../../shared/helper";

import { Editor } from "@tinymce/tinymce-react";

const initialValues = {
  comment: "",
  file: "",
  file_name: [],
};

const removeDropZoneFiles = (fileName, objRef, setErrors) => {
  var newArr = [];
  for (let index = 0; index < objRef.state.files.length; index++) {
    const element = objRef.state.files[index];

    if (fileName === element.name) {
    } else {
      newArr.push(element);
    }
  }

  var fileListHtml = newArr.map((file) => (
    <Alert key={file.name}>
      <span onClick={() => removeDropZoneFiles(file.name, objRef, setErrors)}>
        <i className="far fa-times-circle"></i>
      </span>{" "}
      {file.name}
    </Alert>
  ));
  setErrors({ file_name: "" });
  objRef.setState({
    files: newArr,
    filesHtml: fileListHtml,
  });
};

class RespondBackAssignedPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showRespondBack: false,
      files: [],
      filesHtml: "",
    };
  }

  componentWillReceiveProps = (nextProps) => {
    //console.log(nextProps);
    if (nextProps.showRespondBack === true && nextProps.currRow.task_id > 0) {
      console.log(nextProps.currRow);
      this.setState({
        showRespondBack: nextProps.showRespondBack,
        currRow: nextProps.currRow,
        files: [],
        filesHtml: "",
      });
    }
  };

  handleClose = () => {
    var close = { showRespondBack: false, files: [], filesHtml: "" };
    this.setState(close);
    this.props.handleClose(close);
  };

  handleRespond = (values, actions) => {
    this.setState({ showRespondBackLoader: true });
    var formData = new FormData();

    formData.append("comment", values.comment);
    formData.append("assign_id", this.state.currRow.assignment_id);
    formData.append("posted_for", this.state.currRow.assigned_by);

    if (this.state.files && this.state.files.length > 0) {
      for (let index = 0; index < this.state.files.length; index++) {
        const element = this.state.files[index];
        formData.append("file", element);
      }
      //formData.append('file', this.state.file);
    } else {
      formData.append("file", "");
    }

    const config = {
      headers: {
        "content-type": "multipart/form-data",
      },
    };

    API.post(
      `/api/my_team_tasks/respond_back_assigned/${this.state.currRow.task_id}`,
      formData,
      config
    )
      .then((res) => {
        this.handleClose();
        this.setState({ showRespondBackLoader: false });
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: "Response sent to employee.",
          icon: "success",
        }).then(() => {
          this.props.reloadTask();
        });
      })
      .catch((error) => {
        this.setState({ showRespondBackLoader: false });
        if (error.data.status === 3) {
          var token_rm = 2;
          showErrorMessageFront(error, token_rm, this.props);
          this.handleClose();
        } else {
          actions.setErrors(error.data.errors);
          actions.setSubmitting(false);
        }
      });
  };

  handleComment = (values, actions) => {
    this.setState({ showRespondBackLoader: true });
    var formData = new FormData();

    formData.append("comment", values.comment);
    formData.append("assign_id", this.state.currRow.assignment_id);
    formData.append("posted_for", this.state.currRow.assigned_by);

    if (this.state.files.length > 0) {
      for (let index = 0; index < this.state.files.length; index++) {
        const element = this.state.files[index];
        formData.append("file", element);
      }
      //formData.append('file', this.state.file);
    } else {
      formData.append("file", "");
    }

    const config = {
      headers: {
        "content-type": "multipart/form-data",
      },
    };

    API.post(
      `/api/my_team_tasks/respond_back_comment/${this.state.currRow.task_id}`,
      formData,
      config
    )
      .then((res) => {
        this.handleClose();
        this.setState({ showRespondBackLoader: false });
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: "Response sent to employee.",
          icon: "success",
        }).then(() => {
          this.props.reloadTask();
        });
      })
      .catch((error) => {
        this.setState({ showRespondBackLoader: false });
        if (error.data.status === 3) {
          var token_rm = 2;
          showErrorMessageFront(error, token_rm, this.props);
          this.handleClose();
        } else {
          actions.setErrors(error.data.errors);
          actions.setSubmitting(false);
        }
      });
  };

  setDropZoneFiles = (acceptedFiles, setErrors, setFieldValue) => {
    //console.log(acceptedFiles);
    setErrors({ file_name: false });
    setFieldValue(this.state.files);

    var prevFiles = this.state.files;
    var newFiles;
    if (prevFiles.length > 0) {
      //newFiles = newConcatFiles = acceptedFiles.concat(prevFiles);

      for (let index = 0; index < acceptedFiles.length; index++) {
        var remove = 0;

        for (let index2 = 0; index2 < prevFiles.length; index2++) {
          if (acceptedFiles[index].name === prevFiles[index2].name) {
            remove = 1;
            break;
          }
        }

        //console.log('remove',acceptedFiles[index].name,remove);

        if (remove === 0) {
          prevFiles.push(acceptedFiles[index]);
        }
      }
      //console.log('acceptedFiles',acceptedFiles);
      //console.log('prevFiles',prevFiles);
      newFiles = prevFiles;
    } else {
      newFiles = acceptedFiles;
    }

    this.setState({
      files: newFiles,
    });

    var fileListHtml = newFiles.map((file) => (
      <Alert key={file.name}>
        <span onClick={() => removeDropZoneFiles(file.name, this, setErrors)}>
          <i className="far fa-times-circle"></i>
        </span>{" "}
        {trimString(25, file.name)}
      </Alert>
    ));

    this.setState({
      filesHtml: fileListHtml,
    });
  };

  render() {
    const validateStopFlag = Yup.object().shape({
      comment: Yup.string().trim().required("Please enter your comments"),
    });

    const newInitialValues = Object.assign(initialValues, {
      comment: "",
      file: "",
      file_name: [],
    });

    return (
      <>
        <Modal
          show={this.state.showRespondBack}
          onHide={() => this.handleClose()}
          backdrop="static"
          className="respondBackpop"
        >
          <Formik
            initialValues={newInitialValues}
            validationSchema={validateStopFlag}
            onSubmit={(values, actions) => {
              if (values.leaveComment) {
                this.handleComment(values, actions);
              } else {
                this.handleRespond(values, actions);
              }
            }}
          >
            {({
              values,
              errors,
              touched,
              isValid,
              isSubmitting,
              setFieldValue,
              setFieldTouched,
              setErrors,
            }) => {
              return (
                <Form encType="multipart/form-data">
                  {this.state.showRespondBackLoader === true ? (
                    <div className="loderOuter">
                      <div className="loader">
                        <img src={loaderlogo} alt="logo" />
                        <div className="loading">Loading...</div>
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                  <Modal.Header closeButton>
                    <Modal.Title>Respond Back</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <div className="contBox">
                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <label>
                              Comment <span className="required-field">*</span>
                            </label>
                            {/*  <Field
                                            name="comment"
                                            component="textarea"
                                            className={`selectArowGray form-control`}
                                            autoComplete="off"
                                            value={values.comment}
                                            >
                                        </Field> */}
                            {/* <TinyMCE
                                            name="comment"
                                            content={values.comment}
                                            config={{
                                                menubar: false,
                                                branding: false,
                                                toolbar: 'undo redo | bold italic | alignleft aligncenter alignright'
                                            }}
                                            className={`selectArowGray form-control`}
                                            autoComplete="off"
                                            onChange={value =>
                                                setFieldValue("comment", value.level.content)
                                            }
                                        /> */}

                            <Editor
                              name="content"
                              value={
                                values.comment !== null && values.comment !== ""
                                  ? values.comment
                                  : ""
                              }
                              content={
                                values.comment !== null && values.comment !== ""
                                  ? values.comment
                                  : ""
                              }
                              init={{
                                menubar: false,
                                branding: false,
                                placeholder: "Enter comments",
                                plugins:
                                  "link table hr visualblocks code placeholder lists autoresize textcolor",
                                toolbar:
                                  "bold italic strikethrough superscript subscript | forecolor backcolor | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | visualblocks code | fontselect",
                                font_formats:
                                  "Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats",
                              }}
                              onEditorChange={(value) =>
                                setFieldValue("comment", value)
                              }
                            />

                            {errors.comment && touched.comment ? (
                              <span className="errorMsg">{errors.comment}</span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group custom-file-upload">
                            {/* <input 
                                            id="file" 
                                            name="file" 
                                            type="file" 
                                            onChange={(event) => {                                            
                                                this.setState({ file : event.target.files })
                                                setFieldValue("file", event.target.files);
                                            }} 
                                            multiple
                                        /> */}

                            {/* <FilePond ref={ref => this.pond = ref}
                                                allowMultiple={true}
                                                maxFiles={10}
                                                onupdatefiles={fileItems => {
                                                    // Set currently active file objects to this.state
                                                    this.setState({
                                                        files: fileItems.map(fileItem => fileItem.file)
                                                    });
                                                }}>
                                        </FilePond> */}

                            <Dropzone
                              onDrop={(acceptedFiles) =>
                                this.setDropZoneFiles(
                                  acceptedFiles,
                                  setErrors,
                                  setFieldValue
                                )
                              }
                            >
                              {({ getRootProps, getInputProps }) => (
                                <section>
                                  <div
                                    {...getRootProps()}
                                    className="custom-file-upload-header"
                                  >
                                    <input {...getInputProps()} />
                                    <p>Upload file</p>
                                  </div>
                                  <div className="custom-file-upload-area">
                                    {this.state.filesHtml}
                                  </div>
                                </section>
                              )}
                            </Dropzone>

                            {errors.file_name || touched.file_name ? (
                              <span className="errorMsg errorExpandView">
                                {errors.file_name}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </Modal.Body>
                  <Modal.Footer>
                    <button
                      onClick={this.handleClose}
                      className={`btn-line`}
                      type="button"
                    >
                      Close
                    </button>
                    <button
                      className={`btn-line ${
                        isValid ? "btn-custom-green" : "btn-disable"
                      } m-r-10`}
                      type="submit"
                      id="respond_close"
                      onClick={(e) => {
                        setFieldValue("leaveComment", false);
                      }}
                      disabled={isValid ? false : true}
                    >
                      Respond And Close
                    </button>
                    <button
                      className={`btn-fill ${
                        isValid ? "btn-custom-green" : "btn-disable"
                      } m-r-10`}
                      type="submit"
                      id="leave_comment"
                      onClick={(e) => {
                        setFieldValue("leaveComment", true);
                      }}
                      disabled={isValid ? false : true}
                    >
                      Leave Comment
                    </button>
                  </Modal.Footer>
                </Form>
              );
            }}
          </Formik>
        </Modal>
      </>
    );
  }
}

export default RespondBackAssignedPopup;
