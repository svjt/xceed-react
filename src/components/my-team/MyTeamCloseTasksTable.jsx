import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { getMyDrupalId, localDate, htmlDecode,inArray } from "../../shared/helper";
import { showErrorMessageFront } from "../../shared/handle_error_front";
import base64 from "base-64";
import { Row, Col, Tooltip,Modal, OverlayTrigger } from "react-bootstrap";

import ReactHtmlParser from "react-html-parser";
import "./Dashboard.css";

import StatusColumn from "./StatusColumn";
import ClosedSubTaskTable from "./ClosedSubTaskTable";
import VerticalMenuClosedTask from "./VerticalMenuClosedTask";
import RequestToReopen from "./RequestToReopen";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
//import exclamationImage from '../../assets/images/exclamation-icon.svg';

import { Link } from "react-router-dom";
import API from "../../shared/axios";
import Pagination from "react-js-pagination";

import swal from "sweetalert";

import dateFormat from "dateformat";

import Rating from "react-rating";

const portal_url = `${process.env.REACT_APP_PORTAL_URL}`; // SATYAJIT

const setCompanyName = (refObj) => (cell, row) => {
  return htmlDecode(cell);
};

const getActions = (refObj) => (cell, row) => {
  return (
    <VerticalMenuClosedTask
      id={cell}
      currRow={row}
      requestToReopen={refObj.requestToReopen}
      teamArr={refObj.props.teamData}
    />
  );
};

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const setDescription = (refOBj) => (cell, row) => {
  if (row.parent_id > 0) {
    return (
      <LinkWithTooltip
        tooltip={`${ReactHtmlParser(row.title)}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {ReactHtmlParser(row.title)}
      </LinkWithTooltip>
    );
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${ReactHtmlParser(row.req_name)}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {ReactHtmlParser(row.req_name)}
      </LinkWithTooltip>
    );
  }
};

const setProductName = (refOBj) => (cell, row) => {
  if (cell === null) {
    return "";
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${cell}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {cell}
      </LinkWithTooltip>
    );
  }
};

const clickToShowTasks = (refObj) => (cell, row) => {
  //return <LinkWithTooltip tooltip={`${cell}`} href="#" id="tooltip-1" clicked={e => refObj.checkHandler(e)}>{cell}</LinkWithTooltip>

  if (row.discussion === 1) {
    return (
      <LinkWithTooltip
        tooltip={`${cell}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refObj.redirectUrlTask(e, row.task_id)}
      >
        {cell}
      </LinkWithTooltip>
    );
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${cell}`}
        href="#"
        id="tooltip-1"
        clicked={(e) =>
          refObj.redirectUrlTask(
            e,
            row.task_id,
            row.assignment_id,
            row.assigned_to
          )
        }
      >
        {cell}
      </LinkWithTooltip>
    );
  }
};

const setRating = refOBj => (cell,row) => {
  return (
    <>
      {(row.rating_skipped == 0)?(row.rating > 0 ?<span onClick={() => refOBj.setState({popupData:row,showFeedbackPopup:true})} ><Rating
        emptySymbol={
          <img
            src={require("../../assets/images/uncheck-star.svg")}
            alt=""
            className="icon"
          />
        }
        placeholderSymbol={
          <img
            src={require("../../assets/images/uncheck-stars.svg")}
            alt=""
            className="icon"
          />
        }
        fullSymbol={
          <img
            src={require("../../assets/images/uncheck-stars.svg")}
            alt=""
            className="icon"
          />
        }
        initialRating={row.rating}
        readonly
      /></span>:'Not Rated Yet'):('Skipped')}
      {/* {row.rated_by > 0 ? this.setRatedTooltip(row) : null} */}
    </>
  );
}

const setCustomerName = (refObj) => (cell, row) => {
  if (row.customer_id > 0) {
    //hard coded customer id - SATYAJIT
    //row.customer_id = 2;

    return (
      /*<Link
          to={{
            pathname:
              portal_url + "customer-dashboard/" +
              row.customer_drupal_id
          }}
          target="_blank"
          style={{ cursor: "pointer" }}
        >*/
      <LinkWithTooltip
        tooltip={`${row.first_name + " " + row.last_name}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refObj.redirectUrl(e, row.customer_id)}
      >
        {row.first_name + " " + row.last_name}
      </LinkWithTooltip>
      /*</Link>*/
    );
  } else {
    return "-";
  }
};

const setAssignedTo = (refOBj) => (cell, row) => {
  return (
    <LinkWithTooltip
      tooltip={`${
        row.at_emp_first_name +
        " " +
        row.at_emp_last_name +
        " (" +
        row.at_emp_desig_name +
        ")"
      }`}
      href="#"
      id="tooltip-1"
      clicked={(e) => refOBj.checkHandler(e)}
    >
      {row.at_emp_first_name +
        " " +
        row.at_emp_last_name +
        " (" +
        row.at_emp_desig_name +
        ")"}
    </LinkWithTooltip>
  );
};

const setAssignedBy = (refOBj) => (cell, row) => {
  if (row.assigned_by == "-1") {
    return (
      <LinkWithTooltip
        tooltip={`System`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        System
      </LinkWithTooltip>
    );
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${
          row.ab_emp_first_name +
          " " +
          row.ab_emp_last_name +
          " (" +
          row.ab_emp_desig_name +
          ")"
        }`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {row.ab_emp_first_name +
          " " +
          row.ab_emp_last_name +
          " (" +
          row.ab_emp_desig_name +
          ")"}
      </LinkWithTooltip>
    );
  }
};

const setDateFormat = (refObj) => (cell, row) => {
  var date = localDate(cell);
  return dateFormat(date, "dd/mm/yyyy");
};

const setClosedBy = (refObj) => (cell, row) => {
  if (row.close_status === 1) {
    return (
      <LinkWithTooltip
        tooltip={`${
          row.cb_emp_first_name +
          " " +
          row.cb_emp_last_name +
          " (" +
          row.cb_emp_desig_name +
          ")"
        }`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refObj.checkHandler(e)}
      >
        {row.cb_emp_first_name +
          " " +
          row.cb_emp_last_name +
          " (" +
          row.cb_emp_desig_name +
          ")"}
      </LinkWithTooltip>
    );
  } else {
    return null;
  }
};

const hoverDate = (refOBj) => (cell, row) => {
  return (
    <LinkWithTooltip
      tooltip={cell}
      href="#"
      id="tooltip-1"
      clicked={(e) => refOBj.checkHandler(e)}
    >
      {cell}
    </LinkWithTooltip>
  );
};

// const getBreach = refOBj => (cell,row) => {
//   var breach = false;
//   if(row.breach_reason !== null && row.breach_reason !== ''){
//       breach = true;
//   }

//   return (
//     <>
//       {/* <div className="actionStyle"> */}
//           <div className="btn-group">
//               {breach && <LinkWithTooltip
//                   tooltip={`${htmlDecode(row.breach_reason)}`}
//                   href="#"
//                   id={`tooltip-menu-${row.task_id}`}
//                   clicked={e => this.checkHandler(e)}
//               >
//               <img src={exclamationImage} alt="exclamation" />
//               </LinkWithTooltip>}
//           </div>
//       {/* </div> */}
//     </>
//   );
// };

class MyTeamCloseTasksTable extends Component {
  state = {
    tableData: [],

    activePage: 1,
    totalCount: 0,
    itemPerPage: 20,
    showModalLoader: false,
    showFeedbackPopup:false,
    popupData:{},
    showReopen: false,
  };

  checkHandler = (event) => {
    event.preventDefault();
  };

  redirectUrl = (event, id) => {
    event.preventDefault();
    //http://reddy.indusnet.cloud/customer-dashboard?source=Mi02NTE=
    API.post(`/api/employees/shlogin`, { customer_id: parseInt(id) })
      .then((res) => {
        if (res.data.shadowToken) {
          var url =
            process.env.REACT_APP_CUSTOMER_PORTAL +
            `setToken/` +
            res.data.shadowToken;
          window.open(url, "_blank");
        }
      })
      .catch((error) => {
        showErrorMessageFront(error, 0, this.props);
      });
  };

  taskDetails = (row) => {
    this.setState({ showTaskDetails: true, currRow: row });
  };

  showSubTaskPopup = (currRow) => {
    //console.log('Create Sub Task');
    if(currRow.po_no === null && currRow.request_type == 23){
      swal({
        closeOnClickOutside: false,
        title: "Warning !!",
        text: "Please set Purchase Order Number first.",
        icon: "warning",
      }).then(() => {
        
      });
    }else{
      this.setState({ showCreateSubTask: true, currRow: currRow });
    }
  };

  redirectUrlTask = (event, task_id, assignment_id = "", posted_for = "") => {
    event.preventDefault();
    if (assignment_id === "" && posted_for === "") {
      window.open(`/user/team_discussion_details/${task_id}`, "_blank");
    } else {
      window.open(
        `/user/team_task_details/${task_id}/${assignment_id}/${posted_for}`,
        "_blank"
      );
    }
  };

  showFeedBack = (e) => {
    e.preventDefault();
    this.setState({showFeedbackPopup:true});
  }

  constructOptions = (selected_options,language) => {
    let options = [];
    console.log('selected_options',selected_options);
    if(selected_options != null && selected_options != ''){
      let options_arr = selected_options.split(',');
      console.log('options_arr',options_arr);
      for (let index = 0; index < this.state.taskOptionArr.length; index++) {
        const element = this.state.taskOptionArr[index];
        if(inArray(element.o_id,options_arr)){
          options.push(element[`option_name_en`]);
        }
      }
    }
    return options.join(', ');
  }

  requestToReopen = (currRow) => {
    // swal({
    //     closeOnClickOutside: false,
    //     title: "Reopen task",
    //     text: "Are you sure you want to re-open this task?",
    //     icon: "warning",
    //     buttons: true,
    //     dangerMode: true
    // }).then(willDelete => {
    //     if (willDelete) {
    //         this.setState({ showModalLoader: true });
    //         API.post(`/api/my_team_tasks/re_open_task/${currRow.task_id}/${currRow.assignment_id}`).then(res => {
    //         this.setState({ showModalLoader: false });
    //         swal({
    //             closeOnClickOutside: false,
    //             title:"Success",
    //             text:"Task has been re-opened!",
    //             icon: "success"
    //         }).then(() => {
    //             this.props.allTaskDetails();
    //         });
    //         }).catch((err)=>{
    //             var token_rm = 2;
    //             showErrorMessageFront(err,token_rm,this.props);
    //         });
    //     }
    // });

    this.setState({ showReopen: true, currRow: currRow });
  };

  componentDidMount() {
    //console.log('closed task table',this.props.tableData);
    this.setState({
      tableData: this.props.tableData,
      countClosedTasks: this.props.countClosedTasks,
      options: {
        clearSearch: true,
        expandBy: "column",
        page: !this.state.start_page ? 1 : this.state.start_page, // which page you want to show as default
        sizePerPageList: [
          {
            text: "10",
            value: 10,
          },
          {
            text: "20",
            value: 20,
          },
          {
            text: "All",
            value: !this.state.tableData ? 1 : this.state.tableData,
          },
        ], // you can change the dropdown list for size per page
        sizePerPage: 10, // which size per page you want to locate as default
        pageStartIndex: 1, // where to start counting the pages
        paginationSize: 3, // the pagination bar size.
        prePage: "‹", // Previous page button text
        nextPage: "›", // Next page button text
        firstPage: "«", // First page button text
        lastPage: "»", // Last page button text
        //paginationShowsTotal: this.renderShowsTotal,  // Accept bool or function
        paginationPosition: "bottom", // default is bottom, top and both is all available
        // hideSizePerPage: true > You can hide the dropdown for sizePerPage
        // alwaysShowAllBtns: true // Always show next and previous button
        // withFirstAndLast: false > Hide the going to First and Last page button
      },
      cellEditProp: {
        mode: "click",
        beforeSaveCell: this.onBeforeSetPriority, // a hook for before saving cell
        afterSaveCell: this.onAfterSetPriority, // a hook for after saving cell
      },
    });
    API.get(`api/tasks/employee_task_ratings_options`)
        .then((res) => {
          console.log('taskOptionArr',res.data.data);
          this.setState({taskOptionArr:res.data.data});
        })
        .catch((err) => {
          console.log(err);
        });
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getMyTasks(pageNumber > 0 ? pageNumber : 1);
  };

  getMyTasks(page = 1) {
    let url;

    if (this.props.graphURL && this.props.graphURL != "") {
      url = `${this.props.graphURL}&page=${page}`;
    } else {
      if (this.props.queryString !== "") {
        url = `/api/team/closed?page=${page}&${this.props.queryString}`;
      } else {
        url = `/api/team/closed?page=${page}`;
      }
    }
    API.get(url)
      .then((res) => {
        if (this.props.graphURL && this.props.graphURL != "") {
          this.setState({
            tableData: res.data.data.closed,
            countClosedTasks: res.data.count.count_closed,
          });
        } else {
          this.setState({
            tableData: res.data.data,
            countClosedTasks: res.data.count_closed_tasks,
          });
        }
      })
      .catch((err) => {
        showErrorMessageFront(err, this.props);
      });
  }

  getSubTasks = (row) => {
    return <ClosedSubTaskTable tableData={row.sub_tasks} />;
  };

  checkSubTasks = (row) => {
    //console.log("subtask")

    if (typeof row.sub_tasks !== "undefined" && row.sub_tasks.length > 0) {
      return true;
    } else {
      return false;
    }
  };

  handleClose = (closeObj) => {
    this.setState(closeObj);
  };

  tdClassName = (fieldValue, row) => {
    var dynamicClass = "width-150 increased-column ";
    if (row.vip_customer === 1) {
      dynamicClass += "bookmarked-column ";
    }
    return dynamicClass;
  };

  trClassName = (row, rowIndex) => {
    var ret = " ";

    // var selDueDate;
    // if(row.assigned_by > 0){
    //     selDueDate = row.new_due_date;
    // }else{
    //     selDueDate = row.due_date;
    // }

    var dueDate = localDate(row.due_date);
    var responded_date = localDate(row.responded_date);
    var timeDiff = dueDate.getTime() - responded_date.getTime();

    if (timeDiff > 0) {
    } else {
      ret += "tr-red";
      /* if(row.vip_customer === 1){
              ret += 'tr-red';
          } */
    }

    return ret;
  };

  expandColumnComponent({ isExpandableRow, isExpanded }) {
    let content = "";

    if (isExpandableRow) {
      content = isExpanded ? "-" : "+";
    } else {
      content = " ";
    }
    return <div> {content} </div>;
  }

  render() {
    /* const paginationOptions = {
          page: 1, // which page you want to show as default
          sizePerPageList: [
            {
              text: "10",
              value: 10
            },
            {
              text: "20",
              value: 20
            },
            {
              text: "All",
              value: this.props.tableData.length > 0 ? this.props.tableData.length : 1
            }
          ], // you can change the dropdown list for size per page
          sizePerPage: 10, // which size per page you want to locate as default
          pageStartIndex: 1, // where to start counting the pages
          paginationSize: 3, // the pagination bar size.
          prePage: '‹', // Previous page button text
          nextPage: '›', // Next page button text
          firstPage: '«', // First page button text
          lastPage: '»', // Last page button text
          //paginationShowsTotal: this.renderShowsTotal, // Accept bool or function
          paginationPosition: "bottom" // default is bottom, top and both is all available
          // hideSizePerPage: true //> You can hide the dropdown for sizePerPage
          // alwaysShowAllBtns: true // Always show next and previous button
          // withFirstAndLast: false //> Hide the going to First and Last page button
        }; */
    return (
      <>
        {this.state.showModalLoader === true ? (
          <div className="loderOuter">
            <div className="loader">
              <img src={loaderlogo} alt="logo" />
              <div className="loading">Loading...</div>
            </div>
          </div>
        ) : (
          ""
        )}
        <BootstrapTable
          data={this.state.tableData}
          /* options={ paginationOptions } 
                    pagination */

          expandableRow={this.checkSubTasks}
          expandComponent={this.getSubTasks}
          expandColumnOptions={{
            expandColumnVisible: true,
            expandColumnComponent: this.expandColumnComponent,
            columnWidth: 25,
          }}
          cellEdit={this.state.cellEditProp}
          trClassName={this.trClassName}
        >
          <TableHeaderColumn
            dataField="task_ref"
            columnClassName={this.tdClassName}
            editable={false}
            expandable={false}
            dataFormat={clickToShowTasks(this)}
          >
            <LinkWithTooltip
              tooltip={`Tasks `}
              href="#"
              id="tooltip-1"
              clicked={(e) => this.checkHandler(e)}
            >
              Tasks{" "}
            </LinkWithTooltip>
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="request_type"
            editable={false}
            expandable={false}
            dataFormat={setDescription(this)}
          >
            <LinkWithTooltip
              tooltip={`Description `}
              href="#"
              id="tooltip-1"
              clicked={(e) => this.checkHandler(e)}
            >
              Description{" "}
            </LinkWithTooltip>
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="product_name"
            editable={false}
            expandable={false}
            dataFormat={setProductName(this)}
          >
            <LinkWithTooltip
              tooltip={`Product Name `}
              href="#"
              id="tooltip-1"
              clicked={(e) => this.checkHandler(e)}
            >
              Product Name{" "}
            </LinkWithTooltip>
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="dept_name"
            //dataSort={true}
            editable={false}
            expandable={false}
            dataFormat={setAssignedBy(this)}
          >
            <LinkWithTooltip
              tooltip={`Assigned By `}
              href="#"
              id="tooltip-1"
              clicked={(e) => this.checkHandler(e)}
            >
              Assigned By{" "}
            </LinkWithTooltip>
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="dept_name"
            editable={false}
            expandable={false}
            dataFormat={setAssignedTo(this)}
          >
            <LinkWithTooltip
              tooltip={`Assigned To `}
              href="#"
              id="tooltip-1"
              clicked={(e) => this.checkHandler(e)}
            >
              Assigned To{" "}
            </LinkWithTooltip>
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="company_name"
            //dataSort={true}
            expandable={false}
            dataFormat={setCompanyName(this)}
          >
            <LinkWithTooltip
              tooltip={`Customer `}
              href="#"
              id="tooltip-1"
              clicked={(e) => this.checkHandler(e)}
            >
              Customer{" "}
            </LinkWithTooltip>
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="cust_name"
            editable={false}
            expandable={false}
            dataFormat={setCustomerName(this)}
          >
            <LinkWithTooltip
              tooltip={`User `}
              href="#"
              id="tooltip-1"
              clicked={(e) => this.checkHandler(e)}
            >
              User{" "}
            </LinkWithTooltip>
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="display_add_date"
            editable={false}
            expandable={false}
            dataFormat={hoverDate(this)}
          >
            <LinkWithTooltip
              tooltip={`Created Date `}
              href="#"
              id="tooltip-1"
              clicked={(e) => this.checkHandler(e)}
            >
              Created Date{" "}
            </LinkWithTooltip>
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="display_due_date"
            editable={false}
            expandable={false}
            dataFormat={hoverDate(this)}
          >
            <LinkWithTooltip
              tooltip={`Due Date `}
              href="#"
              id="tooltip-1"
              clicked={(e) => this.checkHandler(e)}
            >
              Due Date{" "}
            </LinkWithTooltip>
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="display_date_added"
            editable={false}
            expandable={false}
            dataFormat={hoverDate(this)}
          >
            <LinkWithTooltip
              tooltip={`Assigned Date `}
              href="#"
              id="tooltip-1"
              clicked={(e) => this.checkHandler(e)}
            >
              Assigned Date{" "}
            </LinkWithTooltip>
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="close_status"
            //dataSort={true}
            editable={false}
            expandable={false}
            dataFormat={setClosedBy(this)}
          >
            <LinkWithTooltip
              tooltip={`Closed By `}
              href="#"
              id="tooltip-1"
              clicked={(e) => this.checkHandler(e)}
            >
              Closed By{" "}
            </LinkWithTooltip>
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="display_responded_date"
            editable={false}
            expandable={false}
            dataFormat={hoverDate(this)}
          >
            <LinkWithTooltip
              tooltip={`Closed Date `}
              href="#"
              id="tooltip-1"
              clicked={(e) => this.checkHandler(e)}
            >
              Closed Date{" "}
            </LinkWithTooltip>
          </TableHeaderColumn>

          <TableHeaderColumn dataField='rating' editable={ false } expandable={ false } dataFormat={ setRating(this) } style={{'width':'130px'}} >
                      
            <LinkWithTooltip
                tooltip={`Feedback `}
                href="#"
                id="tooltip-1"
                clicked={e => this.checkHandler(e)}
              >
              Feedback{" "}
            </LinkWithTooltip>
          </TableHeaderColumn>

          <TableHeaderColumn
            isKey
            dataField="task_id"
            dataFormat={getActions(this)}
            expandable={false}
            editable={false}
          />
        </BootstrapTable>

        <RequestToReopen
          showReopen={this.state.showReopen}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.allTaskDetails()}
        />

        {this.state.showFeedbackPopup && 
          <>
            <Modal
              show={this.state.showFeedbackPopup}
              onHide={() => this.handleClose({showFeedbackPopup:false,popupData:[]})}
              backdrop="static"
            >
              <Modal.Header closeButton>
                <Modal.Title>{this.state.popupData.task_ref}</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <div className="contBox">

                  <Row>
                    <Col xs={12} sm={12} md={12}>
                    <Rating
                      emptySymbol={
                        <img
                          src={require("../../assets/images/uncheck-star.svg")}
                          alt=""
                          className="icon"
                        />
                      }
                      placeholderSymbol={
                        <img
                          src={require("../../assets/images/uncheck-stars.svg")}
                          alt=""
                          className="icon"
                        />
                      }
                      fullSymbol={
                        <img
                          src={require("../../assets/images/uncheck-stars.svg")}
                          alt=""
                          className="icon"
                        />
                      }
                      initialRating={this.state.popupData.rating}
                      readonly
                    />
                    </Col>
                  </Row>

                  <Row>    
                    <Col xs={12} sm={12} md={12}>
                      <p><strong>Rated By</strong>: {this.state.popupData.rated_by_type === 1 ?`${this.state.popupData.first_name_rated_by} ${this.state.popupData.last_name_rated_by}`:`${this.state.popupData.first_name_rated_by_agent} ${this.state.popupData.last_name_rated_by_agent}`}</p>
                    </Col>
                  </Row>

                  <Row>    
                    <Col xs={12} sm={12} md={12}>
                    <p><strong>Selected Options</strong>: {this.constructOptions(this.state.popupData.rating_options,this.state.popupData.language)}</p>
                    </Col>
                  </Row>

                  <Row>    
                    <Col xs={12} sm={12} md={12}>
                    <p><strong>Comment</strong>: {this.state.popupData.rating_comment}</p>
                    </Col>
                  </Row>

                  

                </div>
              </Modal.Body>
              <Modal.Footer>
                <button
                  onClick={() => this.handleClose({showFeedbackPopup:false,popupData:[]})}
                  className={`btn-line`}
                  type="button"
                >
                  Close
                </button>
              </Modal.Footer>
            </Modal>
            </>}


        {this.state.countClosedTasks > 20 ? (
          <Row>
            <Col md={12}>
              <div className="paginationOuter text-right">
                <Pagination
                  activePage={this.state.activePage}
                  itemsCountPerPage={this.state.itemPerPage}
                  totalItemsCount={this.state.countClosedTasks}
                  itemClass="nav-item"
                  linkClass="nav-link"
                  activeClass="active"
                  onChange={this.handlePageChange}
                />
              </div>
            </Col>
          </Row>
        ) : null}
      </>
    );
  }
}

export default MyTeamCloseTasksTable;
