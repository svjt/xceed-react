import React, { Component } from "react";
import { getMyDrupalId } from "../../shared/helper";
import base64 from "base-64";
import "../../assets/css/MyCustomer.css";
import Loader from "react-loader-spinner";
import API from "../../shared/axios";
import commenLogo from "../../assets/images/image-edit.jpg";
import Select from "react-select";
import jsonpath from "jsonpath";
import { htmlDecode } from "../../shared/helper";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
import swal from "sweetalert";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Modal,
  Alert,
  Tooltip,
  OverlayTrigger,
} from "react-bootstrap";
import { showErrorMessageFront } from "../../shared/handle_error_front";

const portal_url = `${process.env.REACT_APP_PORTAL_URL}`; // SATYAJIT

class MyCustomer extends Component {
  constructor(props) {
    super(props);
    this.textInput = React.createRef();

    this.state = {
      vipBlock: [],
      normalBlock: [],
      changeInitiated: false,
      hideSection: true,
      emp_id: this.props.match.params.emp_id,
      comp_id: this.props.match.params.comp_id,
      showCompany: false,
      showModal: false,
    };
  }

  focusTextInput = () => {
    if (this.textInput.current.value) {
      let keyword = this.textInput.current.value;
      this.setState({
        vipBlock: this.state.vipBlock.filter((customer, m) => {
          return (
            customer.first_name === keyword || customer.last_name === keyword
          );
        }),
        normalBlock: this.state.normalBlock.filter((customer, m) => {
          return (
            customer.first_name === keyword || customer.last_name === keyword
          );
        }),
      });
    }
    //console.log(this.state.vipBlock)
    //console.log(this.state.normalBlock)
    //this.textInput.current.focus();
  };

  componentDidMount = () => {
    //console.log('company');
    API.get(
      `/api/team/employee_company_customers/${this.state.emp_id}/${this.state.comp_id}`
    )
      .then((res) => {
        this.setState({
          vipBlock: res.data.customerArr.filter((customer, m) => {
            return customer.vip_customer === 1;
          }),
          normalBlock: res.data.customerArr.filter((customer, m) => {
            return customer.vip_customer === 0;
          }),
          agenBlock: res.data.agentArr,
          hideSection: false,
        });

        API.get("/api/team/my_team_members")
          .then((res) => {
            var empArr = [];
            for (let index = 0; index < res.data.data.length; index++) {
              const element = res.data.data[index];
              empArr.push({
                value: element["employee_id"],
                label: element["first_name"] + " " + element["last_name"],
              });

              if (
                parseInt(this.state.emp_id) === parseInt(element["employee_id"])
              ) {
                this.setState({
                  defEmp: {
                    value: element["employee_id"],
                    label: element["first_name"] + " " + element["last_name"],
                  },
                });
              }
            }
            this.setState({ employeeArr: empArr });

            API.get(`/api/team/employee_company/${this.state.emp_id}`)
              .then((res) => {
                var myCompany = [];
                for (let index = 0; index < res.data.data.length; index++) {
                  const element = res.data.data[index];
                  myCompany.push({
                    value: element["company_id"],
                    label: htmlDecode(element["company_name"]),
                  });

                  if (
                    parseInt(this.state.comp_id) ===
                    parseInt(element["company_id"])
                  ) {
                    this.setState({
                      defComp: {
                        value: element["company_id"],
                        label: htmlDecode(element["company_name"]),
                      },
                    });
                  }
                }
                this.setState({ companyArr: myCompany, showCompany: true });
              })
              .catch((err) => {
                console.log(err);
              });
          })
          .catch((err) => {
            console.log(err);
          });
      })
      .catch((err) => {
        var errText = "Invalid Access To This Page.";
        this.setState({ invalid_access: true });
        swal({
          closeOnClickOutside: false,
          title: "Error in page",
          text: errText,
          icon: "error",
        }).then(() => {
          this.props.history.push(`/user/my_team`);
        });
      });
  };

  searchCustomer = (e) => {
    e.preventDefault();
    console.log(e);
  };

  changeCompany = (event) => {
    if (event === null) {
      //console.log('here');
      this.setState({
        hideSection: true,
        comp_id: 0,
      });
    } else {
      if (event.value > 0) {
        this.setState({
          comp_id: event.value,
        });

        if (parseInt(this.state.emp_id) > 0) {
          API.get(
            `/api/team/employee_company_customers/${this.state.emp_id}/${event.value}`
          )
            .then((res) => {
              this.setState({
                vipBlock: res.data.customerArr.filter((customer, m) => {
                  return customer.vip_customer === 1;
                }),
                normalBlock: res.data.customerArr.filter((customer, m) => {
                  return customer.vip_customer === 0;
                }),
                agenBlock: res.data.agentArr,
                hideSection: false,
                changeInitiated: false,
              });
            })
            .catch((err) => {
              console.log(err);
            });
        }
      } else {
        this.setState({
          hideSection: true,
          comp_id: 0,
        });
      }
    }
  };

  changeEmployee = (event) => {
    if (event === null) {
      //console.log('here');
      this.setState({
        hideSection: true,
        emp_id: 0,
        comp_id: 0,
        vipBlock: null,
        normalBlock: null,
        agenBlock: null,
        myCompany: null,
        defComp: null,
        showCompany: false,
      });
    } else {
      if (event.value > 0) {
        this.setState({
          showCompany: false,
        });
        API.get(`/api/team/employee_company/${event.value}`)
          .then((res) => {
            var myCompany = [];
            for (let index = 0; index < res.data.data.length; index++) {
              const element = res.data.data[index];
              myCompany.push({
                value: element["company_id"],
                label: htmlDecode(element["company_name"]),
              });
            }
            this.setState({
              companyArr: myCompany,
              emp_id: event.value,
              comp_id: 0,
              showCompany: true,
              defComp: { label: "", value: 0 },
              hideSection: true,
            });
          })
          .catch((err) => {
            console.log(err);
          });
      } else {
        this.setState({
          hideSection: true,
          emp_id: 0,
          comp_id: 0,
          vipBlock: null,
          normalBlock: null,
          agenBlock: null,
          myCompany: null,
          defComp: null,
          showCompany: false,
        });
      }
    }
  };

  handleTabs = (event) => {
    if (event.currentTarget.className === "active") {
      //DO NOTHING
    } else {
      var elems = document.querySelectorAll('[id^="tab_"]');
      var elemsContainer = document.querySelectorAll('[id^="show_tab_"]');
      var currId = event.currentTarget.id;

      for (var i = 0; i < elems.length; i++) {
        elems[i].classList.remove("active");
      }

      for (var j = 0; j < elemsContainer.length; j++) {
        elemsContainer[j].style.display = "none";
      }

      event.currentTarget.classList.add("active");
      event.currentTarget.classList.add("active");
      document.querySelector("#show_" + currId).style.display = "block";
    }
    // this.tabElem.addEventListener("click",function(event){
    //     alert(event.target);
    // }, false);
  };

  filterSearch = (event) => {
    if (event.target.value !== "") {
      var filerAgent = jsonpath.query(
        this.state.oldState,
        `$.agenBlock..[?(@ && @.first_name && /^${event.target.value}/i.test(@.first_name) )]`
      );

      //console.log('filerAgent',filerAgent);

      var filerNormal = jsonpath.query(
        this.state.oldState,
        `$.normalBlock..[?(@ && @.first_name && /^${event.target.value}/i.test(@.first_name) )]`
      );

      //console.log('filerNormal',filerNormal);

      var filerVip = jsonpath.query(
        this.state.oldState,
        `$.vipBlock..[?(@ && @.first_name && /^${event.target.value}/i.test(@.first_name) )]`
      );

      //console.log('filerVip',filerVip);
      this.setState({
        normalBlock: filerNormal,
        agenBlock: filerAgent,
        vipBlock: filerVip,
        searchInitiated: true,
      });
    } else {
      this.setState({
        normalBlock: this.state.oldState.normalBlock,
        agenBlock: this.state.oldState.agenBlock,
        vipBlock: this.state.oldState.vipBlock,
        changeInitiated: false,
      });
    }
  };

  render() {
    if (this.state.changeInitiated === false) {
      return (
        <>
          <div className="content-wrapper">
            {/*<!-- Content Header (Page header) -->*/}
            <section className="content-header">
              <div className="row">
                {/* <h1>{taskDetails.req_name}</h1> */}
                {this.state.defEmp && this.state.defEmp != "" && (
                  <div className="col-lg-3 col-sm-5 col-xs-12">
                    <div className="form-group has-feedback">
                      <Select
                        className="basic-single"
                        classNamePrefix="select"
                        defaultValue={this.state.defEmp}
                        isClearable={true}
                        isSearchable={true}
                        name="employee_id"
                        options={this.state.employeeArr}
                        onChange={(e) => {
                          this.changeEmployee(e);
                        }}
                      />
                    </div>
                  </div>
                )}
                {this.state.showCompany === true && (
                  <div className="col-lg-3 col-sm-5 col-xs-12">
                    <div className="form-group has-feedback">
                      <Select
                        className="basic-single"
                        classNamePrefix="select"
                        defaultValue={this.state.defComp}
                        isClearable={true}
                        isSearchable={true}
                        name="employee_id"
                        options={this.state.companyArr}
                        onChange={(e) => {
                          this.changeCompany(e);
                        }}
                      />
                    </div>
                  </div>
                )}

                {/* <div className="col-lg-3 col-sm-5 col-xs-12 pull-right">
                  <div className="form-group has-feedback">
                    <div className="input-group">
                      <input
                        className="form-control"
                        placeholder="Search by Users/Agent Name"
                        onChange={this.filterSearch}
                      />
                      <div className="input-group-btn">
                        <button
                          className="btn btn-default"
                          type="button"
                          onClick={this.focusTextInput}
                        >
                          <i className="glyphicon glyphicon-search"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                </div> */}
              </div>
              <div
                className="customer-edit-button btn-fill"
                onClick={() => this.props.history.push(`/user/my_team`)}
              >
                Back
              </div>
            </section>
            {this.state.hideSection === false && (
              <section className="content">
                <div className="nav-tabs-custom">
                  <ul className="nav nav-tabs">
                    <li
                      className="active"
                      onClick={(e) => this.handleTabs(e)}
                      id="tab_1"
                    >
                      My Users
                    </li>
                    <li onClick={(e) => this.handleTabs(e)} id="tab_2">
                      {" "}
                      My Agents
                    </li>
                  </ul>
                  <div className="tab-content my-customer-tab-content">
                    <div className="tab-pane active" id="show_tab_1">
                      {this.state.normalBlock.length > 0 ? (
                        <div className="row">
                          {this.state.normalBlock.map((customer, j) => (
                            <NormalAccount
                              key={j}
                              index={j}
                              first_name={customer.first_name}
                              last_name={customer.last_name}
                              email={htmlDecode(customer.email)}
                              company_name={htmlDecode(customer.company_name)}
                              customer_id={customer.customer_id}
                              activated={customer.activated}
                              status={customer.status}
                            />
                          ))}
                        </div>
                      ) : (
                        <div className="row">No Users Found</div>
                      )}
                      {this.state.vipBlock.length > 0 ? (
                        <div className="row">
                          {this.state.vipBlock.map((customer, m) => (
                            <KeyAccounts
                              key={m}
                              index={m}
                              first_name={customer.first_name}
                              last_name={customer.last_name}
                              email={htmlDecode(customer.email)}
                              company_name={htmlDecode(customer.company_name)}
                              customer_id={customer.customer_id}
                              activated={customer.activated}
                              status={customer.status}
                            />
                          ))}
                        </div>
                      ) : (
                        <div className="row">No Key Users Found</div>
                      )}
                    </div>
                    <div className="tab-pane" id="show_tab_2">
                      {this.state.agenBlock.length > 0 ? (
                        <div className="row">
                          {this.state.agenBlock.map((agent, k) => (
                            <AgentAccount
                              key={k}
                              index={k}
                              first_name={agent.first_name}
                              last_name={agent.last_name}
                              email={htmlDecode(agent.email)}
                              customer_id={agent.agent_id}
                            />
                          ))}
                        </div>
                      ) : (
                        <div className="row">No Agents Found</div>
                      )}
                    </div>
                  </div>
                </div>
              </section>
            )}
          </div>
        </>
      );
    } else {
      return (
        <>
          <div className="loderOuter">
            <div className="loader">
              <img src={loaderlogo} alt="logo" />
              <div className="loading">Loading...</div>
            </div>
          </div>
        </>
      );
    }
  }
}

class KeyAccounts extends Component {
  state = {
    showModal: false,
  };
  redirectUrl = (event, id) => {
    event.preventDefault();
    //http://reddy.indusnet.cloud/customer-dashboard?source=Mi02NTE=
    API.post(`/api/employees/shlogin`, { customer_id: parseInt(id) })
      .then((res) => {
        if (res.data.shadowToken) {
          var url =
            process.env.REACT_APP_CUSTOMER_PORTAL +
            `setToken/` +
            res.data.shadowToken;
          window.open(url, "_blank");
        }
      })
      .catch((error) => {
        showErrorMessageFront(error, 0, this.props);
      });
  };

  modalShowHandler = (event) => {
    event.preventDefault();
    this.setState({ showModal: true });
  };
  modalCloseHandler = () => {
    this.setState({ showModal: false });
  };

  render() {
    return (
      <>
        <div key={this.props.index} className="col-lg-6 col-sm-6 col-xs-12">
          <div className="customer-box">
            <div className="customer-key">Key Users</div>
            <div className="row clearfix">
              <div className="col-xs-10">
                <div className="customer-img-are">
                  {this.props.activated === 1 && this.props.status === 1 ? (
                    <div className="customer-image">
                      {" "}
                      <span
                        onClick={(e) =>
                          this.redirectUrl(e, this.props.customer_id)
                        }
                        style={{ cursor: "pointer" }}
                      >
                        {" "}
                        <img src={commenLogo} alt="Logo" />
                      </span>{" "}
                    </div>
                  ) : (
                    <div className="customer-image">
                      {" "}
                      <span
                        onClick={(e) => this.modalShowHandler(e)}
                        style={{ cursor: "pointer" }}
                      >
                        {" "}
                        <img src={commenLogo} alt="Logo" />
                      </span>{" "}
                    </div>
                  )}

                  {/* <div className="customer-reivew"> <i className="fa fa-star"></i><i className="fa fa-star"></i><i className="fa fa-star"></i> </div> */}
                </div>
                {this.props.activated === 1 && this.props.status === 1 ? (
                  <div className="customer-content col-border">
                    <h5>
                      <span
                        onClick={(e) =>
                          this.redirectUrl(e, this.props.customer_id)
                        }
                        style={{ cursor: "pointer" }}
                      >
                        {this.props.company_name}
                      </span>
                    </h5>
                    <p
                      onClick={(e) =>
                        this.redirectUrl(e, this.props.customer_id)
                      }
                      style={{ cursor: "pointer" }}
                      className="cus-id"
                    >
                      <strong>Name:</strong> {this.props.first_name}{" "}
                      {this.props.last_name}
                    </p>
                    <p className="cus-id">
                      <strong>Email:</strong> {this.props.email}
                    </p>
                  </div>
                ) : (
                  <div className="customer-content col-border">
                    <h5>
                      <span
                        onClick={(e) => this.modalShowHandler(e)}
                        style={{ cursor: "pointer" }}
                      >
                        {this.props.company_name}
                      </span>
                    </h5>
                    <p
                      onClick={(e) => this.modalShowHandler(e)}
                      style={{ cursor: "pointer" }}
                      className="cus-id"
                    >
                      <strong>Name:</strong> {this.props.first_name}{" "}
                      {this.props.last_name}
                    </p>
                    <p className="cus-id">
                      <strong>Email:</strong> {this.props.email}
                    </p>
                  </div>
                )}
              </div>
              <div className="col-xs-2">
                {" "}
                <div className="customer-icon-holder">
                  <i className="far fa-calendar-check"></i>{" "}
                  <i className="far fa-envelope"></i>{" "}
                </div>
              </div>
            </div>
          </div>
        </div>
        <Modal
          show={this.state.showModal}
          onHide={() => this.modalCloseHandler()}
          backdrop="static"
        >
          <Modal.Header>
            <Modal.Title>My Customers</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <div className="contBox">
              <span>
                You cannot access this account as the user is yet to activate
                his/her account.
              </span>
            </div>
          </Modal.Body>

          <Modal.Footer>
            <button
              className={`btn-fill m-r-10`}
              type="button"
              onClick={(e) => this.modalCloseHandler()}
            >
              Close
            </button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

class NormalAccount extends Component {
  state = {
    showModal: false,
  };
  redirectUrl = (event, id) => {
    event.preventDefault();
    //http://reddy.indusnet.cloud/customer-dashboard?source=Mi02NTE=
    API.post(`/api/employees/shlogin`, { customer_id: parseInt(id) })
      .then((res) => {
        if (res.data.shadowToken) {
          var url =
            process.env.REACT_APP_CUSTOMER_PORTAL +
            `setToken/` +
            res.data.shadowToken;
          window.open(url, "_blank");
        }
      })
      .catch((error) => {
        showErrorMessageFront(error, 0, this.props);
      });
  };

  modalShowHandler = (event) => {
    event.preventDefault();
    this.setState({ showModal: true });
  };
  modalCloseHandler = () => {
    this.setState({ showModal: false });
  };

  render() {
    return (
      <>
        <div key={this.props.index} className="col-lg-4 col-sm-6 col-xs-12">
          <div className="customer-box customer-box-small">
            <div className="row clearfix">
              <div className="col-xs-10">
                <div className="customer-img-are">
                  {this.props.activated === 1 && this.props.status === 1 ? (
                    <div className="customer-image">
                      {" "}
                      <span
                        onClick={(e) =>
                          this.redirectUrl(e, this.props.customer_id)
                        }
                        style={{ cursor: "pointer" }}
                      >
                        {" "}
                        <img src={commenLogo} alt="Logo" />
                      </span>{" "}
                    </div>
                  ) : (
                    <div className="customer-image">
                      {" "}
                      <span
                        onClick={(e) => this.modalShowHandler(e)}
                        style={{ cursor: "pointer" }}
                      >
                        {" "}
                        <img src={commenLogo} alt="Logo" />
                      </span>{" "}
                    </div>
                  )}
                </div>
                <div className="customer-content col-border">
                  {this.props.activated === 1 && this.props.status === 1 ? (
                    <div>
                      <h5>
                        <span
                          onClick={(e) =>
                            this.redirectUrl(e, this.props.customer_id)
                          }
                          style={{ cursor: "pointer" }}
                        >
                          {this.props.company_name}
                        </span>
                      </h5>
                      <p
                        onClick={(e) =>
                          this.redirectUrl(e, this.props.customer_id)
                        }
                        style={{ cursor: "pointer" }}
                        className="cus-id"
                      >
                        <strong>Name:</strong> {this.props.first_name}{" "}
                        {this.props.last_name}
                      </p>
                    </div>
                  ) : (
                    <div>
                      <h5>
                        <span
                          onClick={(e) => this.modalShowHandler(e)}
                          style={{ cursor: "pointer" }}
                        >
                          {this.props.company_name}
                        </span>
                      </h5>
                      <p
                        onClick={(e) => this.modalShowHandler(e)}
                        style={{ cursor: "pointer" }}
                        className="cus-id"
                      >
                        <strong>Name:</strong> {this.props.first_name}{" "}
                        {this.props.last_name}
                      </p>
                    </div>
                  )}

                  <p className="cus-id">
                    <strong>Email:</strong> {this.props.email}
                  </p>
                </div>
              </div>
              <div className="col-xs-2">
                {" "}
                <div className="customer-icon-holder">
                  <i className="far fa-calendar-check"></i>{" "}
                  <i className="far fa-envelope"></i>{" "}
                </div>
              </div>
            </div>
          </div>
        </div>
        <Modal
          show={this.state.showModal}
          onHide={() => this.modalCloseHandler()}
          backdrop="static"
        >
          <Modal.Header>
            <Modal.Title>My Customers</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <div className="contBox">
              <span>Sorry, the user is yet to activate his/ her account.</span>
            </div>
          </Modal.Body>

          <Modal.Footer>
            <button
              className={`btn-fill m-r-10`}
              type="button"
              onClick={(e) => this.modalCloseHandler()}
            >
              Close
            </button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

class AgentAccount extends Component {
  redirectUrl = (event, id) => {
    event.preventDefault();
    //http://reddy.indusnet.cloud/customer-dashboard?source=Mi02NTE=
    API.post(`/api/employees/shlogin`, { customer_id: parseInt(id) })
      .then((res) => {
        if (res.data.shadowToken) {
          var url =
            process.env.REACT_APP_CUSTOMER_PORTAL +
            `setToken/` +
            res.data.shadowToken;
          window.open(url, "_blank");
        }
      })
      .catch((error) => {
        showErrorMessageFront(error, 0, this.props);
      });
  };

  render() {
    return (
      <div key={this.props.index} className="col-lg-4 col-sm-6 col-xs-12">
        <div className="customer-box customer-box-small">
          <div className="row clearfix">
            <div className="col-xs-10">
              <div className="customer-img-are">
                <div className="customer-image">
                  {" "}
                  <span
                    onClick={(e) => this.redirectUrl(e, this.props.customer_id)}
                    style={{ cursor: "pointer" }}
                  >
                    {" "}
                    <img src={commenLogo} alt="Logo" />
                  </span>{" "}
                </div>
              </div>
              <div className="customer-content col-border">
                <h5>
                  <span
                    onClick={(e) => this.redirectUrl(e, this.props.customer_id)}
                    style={{ cursor: "pointer" }}
                  >
                    {this.props.company_name}
                  </span>
                </h5>
                <p
                  onClick={(e) => this.redirectUrl(e, this.props.customer_id)}
                  style={{ cursor: "pointer" }}
                  className="cus-id"
                >
                  <strong>Name:</strong> {this.props.first_name}{" "}
                  {this.props.last_name}
                </p>
                <p className="cus-id">
                  <strong>Email:</strong> {this.props.email}
                </p>
              </div>
            </div>
            <div className="col-xs-2">
              {" "}
              <div className="customer-icon-holder">
                <i className="far fa-calendar-check"></i>{" "}
                <i className="far fa-envelope"></i>{" "}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default MyCustomer;
