import React, { Component } from "react";
import { Row, Col, ButtonToolbar, Button, Modal, Alert } from "react-bootstrap";

//import { FilePond } from 'react-filepond';
//import 'filepond/dist/filepond.min.css';
import Dropzone from "react-dropzone";

import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import dateFormat from "dateformat";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";
import API from "../../shared/axios";
import { showErrorMessageFront } from "../../shared/handle_error_front";

import swal from "sweetalert";

import { localDate, trimString } from "../../shared/helper";

//import uploadAxios from 'axios';

import TinyMCE from "react-tinymce";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";

const initialValues = {
  employeeId: "",
  comment: "",
  breach_reason: "",
  file: "",
  dueDate: "",
  showModalLoader: false,
  file_name: [],
};

//console.log( TinyMCE );

// const onlyUnique = (value, index, self) => {
//     return self.indexOf(value) === index;
// }

const removeDropZoneFiles = (fileName, objRef, setErrors) => {
  var newArr = [];
  for (let index = 0; index < objRef.state.files.length; index++) {
    const element = objRef.state.files[index];

    if (fileName === element.name) {
    } else {
      newArr.push(element);
    }
  }

  var fileListHtml = newArr.map((file) => (
    <Alert key={file.name}>
      <span onClick={() => removeDropZoneFiles(file.name, objRef, setErrors)}>
        <i className="far fa-times-circle"></i>
      </span>{" "}
      {file.name}
    </Alert>
  ));
  setErrors({ file_name: "" });
  objRef.setState({
    files: newArr,
    filesHtml: fileListHtml,
  });
};

class AssignAssignedPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showAssign: false,
      files: [],
      sla_breach: false,
      showModalLoader: false,
      stateEmplId: "",
      acceptNewEmp: false,
      acceptNewEmpMsg: "",
      filesHtml: "",
    };
  }

  handleChange = (e, field) => {
    this.setState({
      [field]: e.target.value,
    });
  };

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.showAssign === true && nextProps.currRow.task_id > 0) {
      var post_date = {};
      if (nextProps.currRow.parent_id > 0) {
        post_date = {
          due_date: nextProps.currRow.due_date,
          sub_task: 1,
          auto: 1,
        };
      } else {
        post_date = { due_date: nextProps.currRow.due_date, auto: 1 };
      }

      API.post(
        `/api/my_team_tasks/sla_breach/${nextProps.currRow.task_id}`,
        post_date
      )
        .then((res) => {
          //console.log(res.data);
          if (res.data.status === 1) {
            this.setState({ sla_breach: true, showModalLoader: false });
          } else {
            this.setState({ sla_breach: false, showModalLoader: false });
          }

          this.setState({
            showAssign: nextProps.showAssign,
            currRow: nextProps.currRow,
          });

          API.get(`/api/employees/other`)
            .then((res) => {
              //console.log(res.data.data);

              var myTeam = [];
              for (let index = 0; index < res.data.data.length; index++) {
                const element = res.data.data[index];
                var leave = "";
                if (element.on_leave == 1) {
                  leave = "[On Leave]";
                }
                myTeam.push({
                  value: element["employee_id"],
                  label:
                    element["first_name"] +
                    " " +
                    element["last_name"] +
                    " (" +
                    element["desig_name"] +
                    ") " +
                    leave,
                });
              }

              this.setState({ employeeArr: myTeam, files: [], filesHtml: "" });
              initialValues.dueDate = localDate(nextProps.currRow.due_date);
            })
            .catch((err) => {
              console.log(err);
            });
        })
        .catch((error) => {});
    }
  };

  handleClose = () => {
    var close = {
      sla_breach: false,
      stateEmplId: "",
      new_employee: false,
      msg_new_employee: "",
      showModalLoader: false,
      showAssign: false,
      files: [],
      filesHtml: "",
    };
    this.setState(close);
    this.props.handleClose(close);
  };

  handleSubmitAssignTask = (values, actions) => {
    this.setState({ showModalLoader: true, msg_new_employee: false });
    if (this.state.sla_breach === true && values.breach_reason === "") {
      actions.setErrors({ breach_reason: "Please provide a reason." });
      actions.setSubmitting(false);
      this.setState({ showModalLoader: false });
    } else {
      var due_date = dateFormat(values.dueDate, "yyyy-mm-dd");

      var formData = new FormData();

      if (this.state.files && this.state.files.length > 0) {
        for (let index = 0; index < this.state.files.length; index++) {
          const element = this.state.files[index];
          formData.append("file", element);
        }
        // for (const file of this.state.files) {
        //     alert(file);
        //     formData.append('file', file)
        // }
        //formData.append('file', this.state.file);
      } else {
        formData.append("file", "");
      }

      //console.log(this.state.files);
      // console.log(values.employeeId);
      formData.append("assigned_to", values.employeeId.value);
      formData.append("comment", values.comment);
      if (this.state.sla_breach === false) {
        formData.append("breach_reason", "");
      } else {
        formData.append("breach_reason", values.breach_reason);
      }

      formData.append("due_date", due_date);
      formData.append("parent_assign_id", this.state.currRow.assignment_id);
      formData.append("posted_for", this.state.currRow.assigned_by);

      //console.log(formData);

      const config = {
        headers: {
          "content-type": "multipart/form-data",
        },
      };

      API.post(
        `/api/my_team_tasks/assign/${this.state.currRow.task_id}`,
        formData,
        config
      )
        .then((res) => {
          this.handleClose();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Task has been assigned.",
            icon: "success",
          }).then(() => {
            this.setState({ showModalLoader: false });
            this.props.reloadTask();
          });
        })
        .catch((error) => {
          this.setState({ showModalLoader: false });
          if (error.data.status === 3) {
            var token_rm = 2;
            showErrorMessageFront(error, token_rm, this.props);
            this.handleClose();
          } else {
            actions.setErrors({
              dueDate: error.data.errors.due_date,
              employeeId: error.data.errors.assigned_to,
              comment: error.data.errors.comment,
              breach_reason: error.data.errors.breach_reason,
              file_name: error.data.errors.file_name,
            });
            //actions.setErrors(error.data.errors);
            actions.setSubmitting(false);
          }
        });
    }
  };

  changeDueDate = (value) => {
    initialValues.dueDate = value;
  };

  getDueDate = () => {
    if (typeof this.state.currRow !== "undefined") {
      //console.log(this.state.currRow.due_date);
      //var due_date = dateFormat(localDate(this.state.currRow.due_date), "yyyy-mm-dd");
      //var replacedDate = this.state.currRow.due_date.split('T');
      //var dateFormat = replacedDate[0];
      //return new Date(due_date);
      return localDate(this.state.currRow.due_date);
    }
  };

  changeDate = (event, setFieldValue) => {
    if (event === null) {
      setFieldValue("dueDate", this.getDueDate());
      this.setState({ sla_breach: false });
    } else {
      setFieldValue("dueDate", event);
      this.setState({ showModalLoader: true });
      var post_date = {};
      if (this.state.currRow.parent_id > 0) {
        post_date = { due_date: event, sub_task: 1, auto: 0 };
      } else {
        post_date = { due_date: event, auto: 0 };
      }

      API.post(
        `/api/my_team_tasks/sla_breach/${this.state.currRow.task_id}`,
        post_date
      )
        .then((res) => {
          //console.log(res.data);
          if (res.data.status === 1) {
            this.setState({ sla_breach: true, showModalLoader: false });
          } else {
            this.setState({ sla_breach: false, showModalLoader: false });
          }
        })
        .catch((error) => {});
    }
  };
  changeManager = (event, setFieldValue) => {
    if (event === null) {
      setFieldValue("employeeId", "");
      this.setState({ stateEmplId: "" });
    } else {
      var post_date = {
        assigned_to: event.value,
        customer_id: this.state.currRow.customer_id,
      };
      API.post(`/api/my_team_tasks/check_leave`, post_date)
        .then((res) => {
          if (res.data.status === 2) {
            this.setState({
              new_employee: true,
              msg_new_employee: res.data.msg_new_employee,
            });
          } else {
            this.setState({
              new_employee: false,
              msg_new_employee: "",
            });
          }
        })
        .catch((error) => {});

      setFieldValue("employeeId", event);
      this.setState({ stateEmplId: event });
    }
  };

  setDropZoneFiles = (acceptedFiles, setErrors, setFieldValue) => {
    //console.log(acceptedFiles);
    setErrors({ file_name: false });
    setFieldValue(this.state.files);
    var prevFiles = this.state.files;
    var newFiles;
    if (prevFiles.length > 0) {
      //newFiles = newConcatFiles = acceptedFiles.concat(prevFiles);

      for (let index = 0; index < acceptedFiles.length; index++) {
        var remove = 0;

        for (let index2 = 0; index2 < prevFiles.length; index2++) {
          if (acceptedFiles[index].name === prevFiles[index2].name) {
            remove = 1;
            break;
          }
        }

        //console.log('remove',acceptedFiles[index].name,remove);

        if (remove === 0) {
          prevFiles.push(acceptedFiles[index]);
        }
      }
      //console.log('acceptedFiles',acceptedFiles);
      //console.log('prevFiles',prevFiles);
      newFiles = prevFiles;
    } else {
      newFiles = acceptedFiles;
    }

    var fileListHtml = newFiles.map((file) => (
      <Alert key={file.name}>
        <span onClick={() => removeDropZoneFiles(file.name, this, setErrors)}>
          <i className="far fa-times-circle"></i>
        </span>{" "}
        {trimString(25, file.name)}
      </Alert>
    ));

    this.setState({
      files: newFiles,
      filesHtml: fileListHtml,
    });
  };

  render() {
    const newInitialValues = Object.assign(initialValues, {
      employeeId: "",
      comment: "",
      breach_reason: "",
      file: "",
      dueDate: this.getDueDate(),
      file_name: [],
    });

    const validateStopFlag = Yup.object().shape({
      employeeId: Yup.string().trim().required("Please select employee"),
      dueDate: Yup.date().required("Due date is required"),
      comment: Yup.string().trim().required("Please enter your comment"),
    });

    //console.log(this.state.employeeArr)

    return (
      <>
        {typeof this.state.employeeArr !== "undefined" && (
          <Modal
            show={this.state.showAssign}
            onHide={() => this.handleClose()}
            backdrop="static"
          >
            <Formik
              initialValues={newInitialValues}
              validationSchema={validateStopFlag}
              onSubmit={this.handleSubmitAssignTask}
            >
              {({
                values,
                errors,
                touched,
                isValid,
                isSubmitting,
                handleChange,
                setFieldValue,
                setFieldTouched,
                setErrors,
              }) => {
                /* console.log('values', values)
                        console.log('errors', errors)
                        console.log('touched', touched)
                        console.log('isValid', isValid) */
                return (
                  <Form encType="multipart/form-data">
                    {this.state.showModalLoader === true ? (
                      <div className="loderOuter">
                        <div className="loader">
                          <img src={loaderlogo} alt="logo" />
                          <div className="loading">Loading...</div>
                        </div>
                      </div>
                    ) : (
                      ""
                    )}
                    <Modal.Header closeButton>
                      <Modal.Title>Assign A Task</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <div className="contBox">
                        <Row>
                          {/* <Col xs={12} sm={6} md={6}>
                                <div className="form-group">
                                    <Field
                                    name="employeeId"
                                    component="select"
                                    className={`selectArowGray form-control`}
                                    autoComplete="off"
                                    value={values.employeeId}
                                    >
                                    <option key='-1' value="" >Employee Name</option>
                                    {this.state.employeeArr.map((emp, i) => (
                                        <option key={i} value={emp.employee_id}>
                                            {emp.first_name} {emp.last_name} ({emp.desig_name})
                                        </option>
                                    ))}
                                    </Field>
                                    {errors.employeeId && touched.employeeId ? (
                                    <span className="errorMsg">
                                        {errors.employeeId}
                                    </span>
                                    ) : null}
                                </div>
                                </Col> */}

                          <Col xs={12} sm={6} md={6}>
                            <div className="form-group">
                              <label>
                                Select Employee{" "}
                                <span className="required-field">*</span>
                              </label>
                              <Select
                                className="basic-single"
                                classNamePrefix="select"
                                isClearable={true}
                                isSearchable={true}
                                name="employeeId"
                                options={this.state.employeeArr}
                                /* onChange={value =>
                                            setFieldValue("employeeId", value)
                                        } */
                                value={this.state.stateEmplId}
                                onChange={(e) => {
                                  this.changeManager(e, setFieldValue);
                                }}
                                onBlur={() => setFieldTouched("employeeId")}
                              />
                              {errors.employeeId && touched.employeeId ? (
                                <span className="errorMsg">
                                  {errors.employeeId}
                                </span>
                              ) : null}

                              {this.state.new_employee === true ? (
                                <span style={{ color: "#ffae42" }}>
                                  {this.state.msg_new_employee}
                                </span>
                              ) : null}
                            </div>
                          </Col>

                          <Col xs={12} sm={6} md={6}>
                            <div className="form-group react-date-picker">
                              {/* <DatePicker
                                        name="dueDate"
                                        showLeadingZeros={true}
                                        format="dd/mm/yyyy"
                                        onChange={value =>
                                            setFieldValue("dueDate", value)
                                        }
                                        value={values.dueDate}
                                    /> */}
                              <label>
                                Due Date{" "}
                                {Date.parse(
                                  new Date(this.state.currRow.due_date)
                                ) >
                                  Date.parse(
                                    new Date(
                                      this.state.currRow.original_due_date
                                    )
                                  ) && (
                                  <i style={{ "font-size": "12px" }}>
                                    (Original Due Date:{" "}
                                    {dateFormat(
                                      this.state.currRow.original_due_date,
                                      "dd/mm/yyyy h:MM TT"
                                    )}
                                    )
                                  </i>
                                )}
                              </label>

                              <div className="form-control">
                                <DatePicker
                                  name={"dueDate"}
                                  className="borderNone"
                                  selected={
                                    values.dueDate ? values.dueDate : null
                                  }
                                  //onChange={value => setFieldValue('dueDate', value)}
                                  dateFormat="dd/MM/yyyy"
                                  autoComplete="off"
                                  onChange={(e) => {
                                    this.changeDate(e, setFieldValue);
                                  }}
                                />
                              </div>
                              {this.state.sla_breach && (
                                <p className="errorMsgPos2">
                                  You are going to breach the SLA
                                </p>
                              )}
                              {errors.dueDate && touched.dueDate ? (
                                <p className="errorMsgPos1">{errors.dueDate}</p>
                              ) : null}
                            </div>
                          </Col>
                        </Row>
                        {/* <Row>
                                <Col xs={12} sm={12} md={12}>
                                <div className="form-group">
                                    Comment*
                                    <Field
                                    name="comment"
                                    component="textarea"
                                    className={`selectArowGray form-control`}
                                    autoComplete="off"
                                    value={values.comment}
                                    > 
                                    </Field>
                                    {errors.comment && touched.comment ? (
                                    <span className="errorMsg">
                                        {errors.comment}
                                    </span>
                                    ) : null}
                                </div>
                                </Col>
                            </Row> */}

                        {this.state.sla_breach && (
                          <Row>
                            <Col xs={12} sm={12} md={12}>
                              <div className="form-group">
                                <label>Please Provide Reason</label>
                                <Field
                                  name="breach_reason"
                                  component="textarea"
                                  className={`selectArowGray form-control`}
                                  autoComplete="off"
                                  onChange={handleChange}
                                />
                                {errors.breach_reason &&
                                touched.breach_reason ? (
                                  <span className="errorMsg">
                                    {errors.breach_reason}
                                  </span>
                                ) : null}
                              </div>
                            </Col>
                          </Row>
                        )}

                        <Row>
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>
                                Comment{" "}
                                <span className="required-field">*</span>
                              </label>
                              <TinyMCE
                                name="comment"
                                content={values.comment}
                                config={{
                                  menubar: false,
                                  branding: false,
                                  toolbar:
                                    "undo redo | bold italic | alignleft aligncenter alignright",
                                }}
                                className={`selectArowGray form-control`}
                                autoComplete="off"
                                onChange={(value) =>
                                  setFieldValue("comment", value.level.content)
                                }
                                onBlur={() => setFieldTouched("comment")}
                              />
                              {errors.comment && touched.comment ? (
                                <span className="errorMsg">
                                  {errors.comment}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                        </Row>
                        {/* <Row>
                                <Col xs={12} sm={12} md={12}>
                                <div className="form-group">
                                   
                                    <input 
                                        id="file" 
                                        name="file" 
                                        type="file" 
                                        onChange={(event) => {                                            
                                            this.setState({ file : event.target.files[0] })
                                            setFieldValue("file", event.target.files[0]);
                                        }} 
                                    />

                                    {errors.file && touched.file ? (
                                    <span className="errorMsg">
                                        {errors.file}
                                    </span>
                                    ) : null}
                                </div>
                                </Col>
                            </Row> */}

                        <Row>
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group custom-file-upload">
                              {/* <FilePond ref={ref => this.pond = ref}
                                        allowMultiple={true}
                                        maxFiles={10}
                                        onupdatefiles={fileItems => {
                                            // Set currently active file objects to this.state
                                            this.setState({
                                                files: fileItems.map(fileItem => fileItem.file)
                                            });
                                        }}>
                                </FilePond> */}

                              <Dropzone
                                onDrop={(acceptedFiles) =>
                                  this.setDropZoneFiles(
                                    acceptedFiles,
                                    setErrors,
                                    setFieldValue
                                  )
                                }
                              >
                                {({ getRootProps, getInputProps }) => (
                                  <section>
                                    <div
                                      {...getRootProps()}
                                      className="custom-file-upload-header"
                                    >
                                      <input {...getInputProps()} />
                                      <p>Upload file</p>
                                    </div>
                                    <div className="custom-file-upload-area">
                                      {this.state.filesHtml}
                                    </div>
                                  </section>
                                )}
                              </Dropzone>
                              {errors.file_name || touched.file_name ? (
                                <span className="errorMsg errorExpandView">
                                  {errors.file_name}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                        </Row>
                      </div>
                    </Modal.Body>
                    <Modal.Footer>
                      <button
                        onClick={this.handleClose}
                        className={`btn-line`}
                        type="button"
                      >
                        Close
                      </button>
                      <button
                        className={`btn-fill ${
                          isValid ? "btn-custom-green" : "btn-disable"
                        } m-r-10`}
                        type="submit"
                        disabled={isValid ? false : true}
                      >
                        {this.state.stopflagId > 0
                          ? isSubmitting
                            ? "Updating..."
                            : "Update"
                          : isSubmitting
                          ? "Submitting..."
                          : "Submit"}
                      </button>
                    </Modal.Footer>
                  </Form>
                );
              }}
            </Formik>
          </Modal>
        )}
      </>
    );
  }
}

export default AssignAssignedPopup;
