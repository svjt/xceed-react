import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  getMyDrupalId,
  localDate,
  htmlDecode,
  inArray,
} from "../../shared/helper";
import { showErrorMessageFront } from "../../shared/handle_error_front";
import exclamationImage from "../../assets/images/exclamation-icon.svg";
import Select from "react-select";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
import { Row, Col, Tooltip, OverlayTrigger } from "react-bootstrap";
import "./Dashboard.css";

import dateFormat from "dateformat";
import API from "../../shared/axios";
import { Link } from "react-router-dom";

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const setDescription = (refOBj) => (cell, row) => {
  if (row.parent_id > 0) {
    return (
      <LinkWithTooltip
        tooltip={`${row.title}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {row.title}
      </LinkWithTooltip>
    );
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${row.req_name}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {row.req_name}
      </LinkWithTooltip>
    );
  }
};

const setProductName = (refOBj) => (cell, row) => {
  if (cell === null) {
    return "";
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${cell}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {cell}
      </LinkWithTooltip>
    );
  }
};

const setCreateDate = (refObj) => (cell) => {
  var date = localDate(cell);
  return dateFormat(date, "dd/mm/yyyy");
};

const setDaysPending = (refObj) => (cell, row) => {
  var date = localDate(cell);
  return dateFormat(date, "dd/mm/yyyy");
};

const clickToShowTasks = (refObj) => (cell, row) => {
  if (row.discussion === 1) {
    return (
      <LinkWithTooltip
        tooltip={`${cell}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refObj.redirectUrlTask(e, row.task_id)}
      >
        {cell}
      </LinkWithTooltip>
    );
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${cell}`}
        href="#"
        id="tooltip-1"
        clicked={(e) =>
          refObj.redirectUrlTask(
            e,
            row.task_id,
            row.assignment_id,
            row.assigned_to
          )
        }
      >
        {cell}
      </LinkWithTooltip>
    );
  }
};

const setCompanyName = (refObj) => (cell, row) => {
  return htmlDecode(cell);
};

const setCustomerName = (refObj) => (cell, row) => {
  if (row.customer_id > 0) {
    //hard coded customer id - SATYAJIT
    //row.customer_id = 2;
    return (
      <LinkWithTooltip
        tooltip={`${row.first_name + " " + row.last_name}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refObj.redirectUrl(e, row.customer_id)}
      >
        {row.first_name + " " + row.last_name}
      </LinkWithTooltip>
    );
  } else {
    return "";
  }
};

const setAssignedTo = (refOBj) => (cell, row) => {
  return (
    <LinkWithTooltip
      tooltip={`${
        row.at_emp_first_name +
        " " +
        row.at_emp_last_name +
        " (" +
        row.at_emp_desig_name +
        ")"
      }`}
      href="#"
      id="tooltip-1"
      clicked={(e) => refOBj.checkHandler(e)}
    >
      {row.at_emp_first_name +
        " " +
        row.at_emp_last_name +
        " (" +
        row.at_emp_desig_name +
        ")"}
    </LinkWithTooltip>
  );
};

const setAssignedBy = (refOBj) => (cell, row) => {
  console.log(row.assigned_by);
  if (row.assigned_by === "-1") {
    return (
      <LinkWithTooltip
        tooltip={`System`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        System
      </LinkWithTooltip>
    );
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${
          row.ab_emp_first_name +
          " " +
          row.ab_emp_last_name +
          " (" +
          row.ab_emp_desig_name +
          ")"
        }`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {row.ab_emp_first_name +
          " " +
          row.ab_emp_last_name +
          " (" +
          row.ab_emp_desig_name +
          ")"}
      </LinkWithTooltip>
    );
  }
};

const hoverDate = (refOBj) => (cell, row) => {
  return (
    <LinkWithTooltip
      tooltip={cell}
      href="#"
      id="tooltip-1"
      clicked={(e) => refOBj.checkHandler(e)}
    >
      {cell}
    </LinkWithTooltip>
  );
};

const getBreach = (refOBj) => (cell, row) => {
  var breach = false;
  if (row.breach_reason !== null && row.breach_reason !== "") {
    breach = true;
  }

  return (
    <>
      {/* <div className="actionStyle"> */}
      <div className="btn-group">
        {breach && (
          <LinkWithTooltip
            tooltip={`${htmlDecode(row.breach_reason)}`}
            href="#"
            id={`tooltip-menu-${row.task_id}`}
            clicked={(e) => this.checkHandler(e)}
          >
            <img src={exclamationImage} alt="exclamation" />
          </LinkWithTooltip>
        )}
      </div>
      {/* </div> */}
    </>
  );
};

class MyTeamOpenTasksTable extends Component {
  state = {
    task_id: 0,
    tableData: [],
    params: [],
    activePage: 1,
    totalCount: 0,
    itemPerPage: 20,
    showDeleteTaskPopup: false,
    node_url: "",
    selectedProducts: [],
    selectedCustomers: [],
    selectedFunctions: [],
    selectedUser: [],
    selectedRequests: [],
    selectedEmployee: [],
    selectedTaskRef: "",
    showEmployee: true,
    showRequest: true,
    show_deleted_tasks: false,
    show_dummy_users: false,
    is_loading: true,
  };

  checkHandler = (event) => {
    event.preventDefault();
  };

  redirectUrl = (event, id) => {
    event.preventDefault();
    API.post(`/api/employees/shlogin`, { customer_id: parseInt(id) })
      .then((res) => {
        if (res.data.shadowToken) {
          var url =
            process.env.REACT_APP_CUSTOMER_PORTAL +
            `setToken/` +
            res.data.shadowToken;
          window.open(url, "_blank");
        }
      })
      .catch((error) => {
        showErrorMessageFront(error, 0, this.props);
      });
  };

  redirectUrlTask = (event, task_id, assignment_id = "", posted_for = "") => {
    event.preventDefault();
    if (assignment_id === "" && posted_for === "") {
      window.open(`/user/team_discussion_details/${task_id}`, "_blank");
    } else {
      window.open(
        `/user/team_task_details/${task_id}/${assignment_id}/${posted_for}`,
        "_blank"
      );
    }
  };

  filterSearch = (event, nextEV) => {
    this.setState({
      search_loader: true,
    });
    var currIndex = null;
    if (event == null) {
      for (let index = 0; index < this.state.params.length; index++) {
        const element = this.state.params[index];

        if (element.name === nextEV.name) {
          currIndex = index;
        }
      }
      if (currIndex !== null) {
        this.state.params.splice(currIndex, 1);
      }
    } else {
      for (let index = 0; index < this.state.params.length; index++) {
        const element = this.state.params[index];
        if (element.name === nextEV.name) {
          currIndex = index;
        }
      }

      if (currIndex == null) {
        this.state.params.push({ name: nextEV.name, val: event.value });
      } else {
        const myCurrIndex = this.state.params[currIndex];
        myCurrIndex.val = event.value;
      }
    }

    this.callApi();
  };

  filterWords = (event) => {
    this.setState({
      search_loader: true,
    });
    var currIndex = null;
    var value_ref = event.target.value.trim().toUpperCase();
    this.setState({ selectedTaskRef: value_ref });
    if (value_ref.length == 0) {
      for (let index = 0; index < this.state.params.length; index++) {
        const element = this.state.params[index];
        if (element.name === "task_ref") {
          currIndex = index;
        }
      }

      if (currIndex !== null) {
        this.state.params.splice(currIndex, 1);
      }
    } else {
      for (let index = 0; index < this.state.params.length; index++) {
        const element = this.state.params[index];
        if (element.name === "task_ref") {
          currIndex = index;
        }
      }

      if (currIndex == null) {
        this.state.params.push({ name: "task_ref", val: value_ref });
      } else {
        const myCurrIndex = this.state.params[currIndex];
        myCurrIndex.val = value_ref;
      }
    }

    this.callApi();
  };

  filterSearchMulti = (event, nextEV) => {
    this.setState({
      search_loader: true,
    });
    var currIndex = null;
    if (event.length == 0) {
      for (let index = 0; index < this.state.params.length; index++) {
        const element = this.state.params[index];
        if (element.name === nextEV.name) {
          currIndex = index;
        }
      }
      if (currIndex !== null) {
        this.state.params.splice(currIndex, 1);
      }
      this.companyCustomers(nextEV.name, "");
    } else {
      for (let index = 0; index < this.state.params.length; index++) {
        const element = this.state.params[index];
        if (element.name === nextEV.name) {
          currIndex = index;
        }
      }

      var multi_val = this.getMultiValueString(event);

      if (currIndex == null) {
        this.state.params.push({ name: nextEV.name, val: multi_val });
      } else {
        const myCurrIndex = this.state.params[currIndex];
        myCurrIndex.val = multi_val;
      }
      this.companyCustomers(nextEV.name, multi_val);
    }

    this.callApi();
  };

  getMultiValueString = (selectedValues) => {
    var string = "";
    for (let index = 0; index < selectedValues.length; index++) {
      const element = selectedValues[index];
      string += `${element.value},`;
    }
    string = string.replace(/,+$/, "");
    return string;
  };

  companyCustomers = (name, val) => {
    if (name === "compid") {
      if (val === "") {
        var currIndex;
        for (let index = 0; index < this.state.params.length; index++) {
          const element = this.state.params[index];
          if (element.name === "cid") {
            currIndex = index;
            break;
          }
        }
        if (currIndex >= 0) {
          this.state.params.splice(currIndex, 1);
        }
        this.setState({ showCustomerBlock: false, my_customer_list: [] });
      } else {
        API.get(`/api/team/my_team_customers?compid=${val}`)
          .then((res) => {
            var myCustomer = [];
            for (let index = 0; index < res.data.data.length; index++) {
              const element = res.data.data[index];
              var keyCustomer;
              if (element.vip_customer === 1) {
                keyCustomer = "*";
              } else {
                keyCustomer = "";
              }
              myCustomer.push({
                value: element["customer_id"],
                label:
                  element["first_name"] +
                  " " +
                  element["last_name"] +
                  " " +
                  keyCustomer,
              });
            }

            this.setState({
              showCustomerBlock: true,
              my_customer_list: myCustomer,
            });
          })
          .catch((err) => {
            console.log(err);
          });
      }
    }
  };

  filterDateFrom = (value) => {
    this.setState({ date_from: value, search_loader: true });
    var currIndex = null;
    for (let index = 0; index < this.state.params.length; index++) {
      const element = this.state.params[index];
      if (element.name === "date_from") {
        currIndex = index;
      }
    }

    if (currIndex == null) {
      this.state.params.push({
        name: "date_from",
        val: dateFormat(value, "yyyy-mm-dd"),
      });
    } else {
      const myCurrIndex = this.state.params[currIndex];
      myCurrIndex.val = dateFormat(value, "yyyy-mm-dd");
    }

    this.callApi();
  };

  filterDateTo = (value) => {
    this.setState({ date_to: value, search_loader: true });
    var currIndex = null;
    for (let index = 0; index < this.state.params.length; index++) {
      const element = this.state.params[index];
      if (element.name === "date_to") {
        currIndex = index;
      }
    }

    if (currIndex == null) {
      this.state.params.push({
        name: "date_to",
        val: dateFormat(value, "yyyy-mm-dd"),
      });
    } else {
      const myCurrIndex = this.state.params[currIndex];
      myCurrIndex.val = dateFormat(value, "yyyy-mm-dd");
    }

    this.callApi();
  };

  removeDateTo = () => {
    this.setState({ date_to: null, search_loader: true });
    var currIndex = null;
    for (let index = 0; index < this.state.params.length; index++) {
      const element = this.state.params[index];

      if (element.name === "date_to") {
        currIndex = index;
      }
    }
    if (currIndex !== null) {
      this.state.params.splice(currIndex, 1);
    }

    this.callApi();
  };

  removeDateFrom = () => {
    this.setState({ date_from: null, search_loader: true });
    var currIndex = null;
    for (let index = 0; index < this.state.params.length; index++) {
      const element = this.state.params[index];

      if (element.name === "date_from") {
        currIndex = index;
      }
    }
    if (currIndex !== null) {
      this.state.params.splice(currIndex, 1);
    }

    this.callApi();
  };

  showDeletedTasks = (event) => {
    let show_del_task = event.target.checked;
    this.setState({ show_deleted_tasks: show_del_task, search_loader: true });
    var currIndex = null;

    for (let index = 0; index < this.state.params.length; index++) {
      const element = this.state.params[index];
      if (element.name === "show_deleted") {
        currIndex = index;
      }
    }
    let param_del_task;
    if (show_del_task) {
      param_del_task = 1;
    } else {
      param_del_task = 0;
    }

    if (currIndex == null) {
      this.state.params.push({ name: "show_deleted", val: param_del_task });
    } else {
      const myCurrIndex = this.state.params[currIndex];
      myCurrIndex.val = param_del_task;
    }

    this.callApi();
  };

  showDummyTasks = (event) => {
    let show_del_task = event.target.checked;
    this.setState({ show_dummy_users: show_del_task, search_loader: true });
    var currIndex = null;

    for (let index = 0; index < this.state.params.length; index++) {
      const element = this.state.params[index];
      if (element.name === "show_dummy") {
        currIndex = index;
      }
    }
    let param_del_task;
    if (show_del_task) {
      param_del_task = 1;
    } else {
      param_del_task = 0;
    }

    if (currIndex == null) {
      this.state.params.push({ name: "show_dummy", val: param_del_task });
    } else {
      const myCurrIndex = this.state.params[currIndex];
      myCurrIndex.val = param_del_task;
    }

    this.callApi();
  };

  callApi = () => {
    var retArr = [];
    if (this.state.params.length > 0) {
      var query_string = "";
      for (let index = 0; index < this.state.params.length; index++) {
        const element = this.state.params[index];
        query_string += `${element.name}=${element.val}&`;
      }
      query_string = query_string.substring(0, query_string.length - 1);

      this.getMyGraphTasks(query_string);
    } else {
      this.getMyGraphTasks();
    }
  };

  downloadXLSX = (e) => {
    e.preventDefault();
    if (this.state.params.length > 0) {
      var query_string = "";
      for (let index = 0; index < this.state.params.length; index++) {
        const element = this.state.params[index];
        query_string += `${element.name}=${element.val}&`;
      }
      query_string = query_string.substring(0, query_string.length - 1);

      this.getMyGraphTasks(query_string, true);
    } else {
      this.getMyGraphTasks("", true);
    }
  };

  getMyGraphTasks = (manual_search = "", excel_download = false) => {
    let url_id = this.props.match.params.url_id;
    let search_criteria = this.props.match.params.search_criteria;
    let bar_graph = this.props.match.params.bar_graph;

    if (manual_search === "") {
      search_criteria.split("&").forEach((element) => {
        let sub_arr = element.split("=");
        this.state.params.push({ name: sub_arr[0], val: sub_arr[1] });
        if (sub_arr[0] === "task_ref") {
          this.setState({ selectedTaskRef: sub_arr[1] });
        }

        if (sub_arr[0] === "date_from") {
          this.setState({ date_from: new Date(sub_arr[1]) });
        }

        if (sub_arr[0] === "date_to") {
          this.setState({ date_to: new Date(sub_arr[1]) });
        }

        if (sub_arr[0] === "show_dummy" && sub_arr[1] == 1) {
          this.setState({ show_dummy_users: true });
        }

        if (sub_arr[0] === "show_deleted" && sub_arr[1] == 1) {
          this.setState({ show_deleted_tasks: true });
        }
      });

      if (search_criteria === "q=no") {
        search_criteria = "";
      } else {
        search_criteria = `?${search_criteria}`;
      }
    } else {
      search_criteria = `?${manual_search}`;
    }

    if (excel_download) {
      if (search_criteria === "q=no") {
        search_criteria = `?excel=1`;
      } else {
        search_criteria = `${search_criteria}&excel=1`;
      }
    }

    let reg = /^\d+$/;
    let url = "";
    if (reg.test(url_id)) {
      switch (url_id) {
        case "1":
          url = `/api/reports/open_tasks/sla_eighty${search_criteria}`;
          break;

        case "2":
          url = `/api/reports/open_tasks/sla_breached${search_criteria}`;
          break;

        case "3":
          url = `/api/reports/open_tasks/within_sla${search_criteria}`;
          break;

        case "4":
          url = `/api/reports/allocated_tasks/sla_eighty${search_criteria}`;
          break;

        case "5":
          url = `/api/reports/allocated_tasks/sla_breached${search_criteria}`;
          break;

        case "6":
          url = `/api/reports/allocated_tasks/within_sla${search_criteria}`;
          break;

        case "7":
          url = `/api/reports/closed_tasks/sla_met${search_criteria}`;
          break;

        case "8":
          url = `/api/reports/closed_tasks/sla_breached${search_criteria}`;
          break;

        case "9":
          url = `/api/reports/task_overdue/by_days${search_criteria}`;
          break;

        case "10":
          url = `/api/reports/task_overdue/by_weeks${search_criteria}`;
          break;

        case "11":
          url = `/api/reports/task_overdue/by_months${search_criteria}`;
          break;

        case "12":
          url = `/api/reports/met_sla/one_day${search_criteria}`;
          break;

        case "13":
          url = `/api/reports/met_sla/two_days${search_criteria}`;
          break;

        case "14":
          url = `/api/reports/met_sla/three_days${search_criteria}`;
          break;

        case "15":
          this.setState({ showRequest: true, showEmployee: false });
          if (search_criteria === "") {
            bar_graph = `?bs=${bar_graph}`;
          } else {
            bar_graph = `&bs=${bar_graph}`;
          }
          url = `/api/reports/employee/distribution${search_criteria}${bar_graph}`;
          break;

        case "16":
          this.setState({ showRequest: false, showEmployee: true });
          if (search_criteria === "") {
            bar_graph = `?bs=${bar_graph}`;
          } else {
            bar_graph = `&bs=${bar_graph}`;
          }
          url = `/api/reports/request_type/distribution${search_criteria}${bar_graph}`;
          break;

        case "17":
          url = `/api/reports/open_tasks/sla_breached_reason${search_criteria}`;
          break;

        case "18":
          url = `/api/reports/allocated_tasks/sla_breached_reason${search_criteria}`;
          break;

        case "19":
          url = `/api/reports/closed_tasks/sla_breached_reason${search_criteria}`;
          break;

        default:
          break;
      }

      if (url != "") {
        let options = {};
        if (excel_download) {
          options = { responseType: "blob" };
        }

        API.get(`${url}`, options)
          .then((res) => {
            if (res.data && res.data.count && res.data.count > 0) {
              console.log("here");
              this.setState({
                tableData: res.data.data,
                countOpenTasks: res.data.count,
                pageOptions: {
                  clearSearch: true,
                  expandBy: "column",
                  page: !this.state.start_page ? 1 : this.state.start_page,
                  sizePerPageList: [
                    {
                      text: "10",
                      value: 10,
                    },
                    {
                      text: "20",
                      value: 20,
                    },
                    {
                      text: "All",
                      value: !res.data.count ? 1 : res.data.count,
                    },
                  ],
                  sizePerPage: 20,
                  pageStartIndex: 1,
                  paginationSize: 3,
                  prePage: "‹",
                  nextPage: "›",
                  firstPage: "«",
                  lastPage: "»",
                  paginationPosition: "bottom",
                },
              });
            } else {
              console.log("here xlsx");
              let file_name = "";
              switch (url_id) {
                case "1":
                  file_name = `open_tasks_sla_eighty.xlsx`;
                  break;

                case "2":
                  file_name = `open_tasks_sla_breached.xlsx`;
                  break;

                case "3":
                  file_name = `open_tasks_within_sla.xlsx`;
                  break;

                case "4":
                  file_name = `allocated_tasks_sla_eighty.xlsx`;
                  break;

                case "5":
                  file_name = `allocated_tasks_sla_breached.xlsx`;
                  break;

                case "6":
                  file_name = `allocated_tasks_within_sla.xlsx`;
                  break;

                case "7":
                  file_name = `closed_tasks_sla_met.xlsx`;
                  break;

                case "8":
                  file_name = `closed_tasks_sla_breached.xlsx`;
                  break;

                case "9":
                  file_name = `task_overdue_by_days.xlsx`;
                  break;

                case "10":
                  file_name = `task_overdue_by_weeks.xlsx`;
                  break;

                case "11":
                  file_name = `task_overdue_by_months.xlsx`;
                  break;

                case "12":
                  file_name = `met_sla_one_day.xlsx`;
                  break;

                case "13":
                  file_name = `met_sla_two_days.xlsx`;
                  break;

                case "14":
                  file_name = `met_sla_three_days.xlsx`;
                  break;

                case "15":
                  file_name = `employee_distribution.xlsx`;
                  break;

                case "16":
                  file_name = `request_type_distribution.xlsx`;
                  break;

                case "17":
                  file_name = `open_tasks_sla_breached_reason.xlsx`;
                  break;

                case "18":
                  file_name = `allocated_tasks_sla_breached_reason.xlsx`;
                  break;

                case "19":
                  file_name = `closed_tasks_sla_breached_reason.xlsx`;
                  break;

                default:
                  break;
              }
              console.log(file_name);
              let uri = window.URL.createObjectURL(res.data);
              let a = document.createElement("a");
              a.href = uri;
              a.download = file_name;
              a.click();
            }
          })
          .catch((err) => {
            console.log(err);
          });
      }

      this.setState({ node_url: url }, () => {});
    } else {
    }
  };

  componentDidMount() {
    this.getMyGraphTasks();

    API.get(`/api/team/my_team_members`)
      .then((res) => {
        var myEmployee = [];
        let currIndex = null;
        for (let index = 0; index < this.state.params.length; index++) {
          const element = this.state.params[index];
          if (element.name === "eid") {
            currIndex = index;
          }
        }
        let selArr = [];
        if (currIndex == null) {
        } else {
          const myCurrIndex = this.state.params[currIndex];
          selArr = myCurrIndex.val.split(",");
        }
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];

          if (currIndex != null && inArray(element["employee_id"], selArr)) {
            this.state.selectedEmployee.push({
              value: element["employee_id"],
              label: element["first_name"] + " " + element["last_name"],
            });
          }

          myEmployee.push({
            value: element["employee_id"],
            label: element["first_name"] + " " + element["last_name"],
          });
        }

        this.setState({ my_employee_list: myEmployee });

        API.get(`/api/feed/request_type`)
          .then((res) => {
            var requestTypes = [];
            let currIndex = null;
            for (let index = 0; index < this.state.params.length; index++) {
              const element = this.state.params[index];
              if (element.name === "rid") {
                currIndex = index;
              }
            }
            let selArr = [];
            if (currIndex == null) {
            } else {
              const myCurrIndex = this.state.params[currIndex];
              selArr = myCurrIndex.val.split(",");
            }
            for (let index = 0; index < res.data.data.length; index++) {
              const element = res.data.data[index];

              if (currIndex != null && inArray(element["type_id"], selArr)) {
                this.state.selectedRequests.push({
                  value: element["type_id"],
                  label: element["req_name"],
                });
              }

              requestTypes.push({
                value: element["type_id"],
                label: element["req_name"],
              });
            }

            this.setState({ request_type: requestTypes });

            API.get(`/api/feed/designations`)
              .then((res) => {
                var desigTypes = [];
                let currIndex = null;
                for (let index = 0; index < this.state.params.length; index++) {
                  const element = this.state.params[index];
                  if (element.name === "desig") {
                    currIndex = index;
                  }
                }
                let selArr = [];
                if (currIndex == null) {
                } else {
                  const myCurrIndex = this.state.params[currIndex];
                  selArr = myCurrIndex.val.split(",");
                }
                for (let index = 0; index < res.data.data.length; index++) {
                  const element = res.data.data[index];

                  if (
                    currIndex != null &&
                    inArray(element["desig_id"], selArr)
                  ) {
                    this.state.selectedFunctions.push({
                      value: element["desig_id"],
                      label: element["desig_name"],
                    });
                  }

                  desigTypes.push({
                    value: element["desig_id"],
                    label: element["desig_name"],
                  });
                }
                this.setState({ designations: desigTypes });

                API.get("/api/team/l1_companies")
                  .then((res) => {
                    var myCompany = [];
                    let currIndex = null;
                    for (
                      let index = 0;
                      index < this.state.params.length;
                      index++
                    ) {
                      const element = this.state.params[index];
                      if (element.name === "compid") {
                        currIndex = index;
                      }
                    }
                    let selArr = [];
                    if (currIndex == null) {
                    } else {
                      const myCurrIndex = this.state.params[currIndex];
                      selArr = myCurrIndex.val.split(",");
                    }
                    for (let index = 0; index < res.data.data.length; index++) {
                      const element = res.data.data[index];

                      if (
                        currIndex != null &&
                        inArray(element["company_id"], selArr)
                      ) {
                        this.state.selectedCustomers.push({
                          value: element["company_id"],
                          label: element["company_name"],
                        });
                      }

                      myCompany.push({
                        value: element["company_id"],
                        label: htmlDecode(element["company_name"]),
                      });
                    }

                    this.setState({ company: myCompany });

                    if (selArr && selArr.length > 0) {
                      API.get(
                        `/api/team/my_team_customers?compid=${selArr.join(",")}`
                      )
                        .then((res) => {
                          var myCustomer = [];
                          let currIndex = null;
                          for (
                            let index = 0;
                            index < this.state.params.length;
                            index++
                          ) {
                            const element = this.state.params[index];
                            if (element.name === "cid") {
                              currIndex = index;
                            }
                          }
                          let selArr = [];
                          if (currIndex == null) {
                          } else {
                            const myCurrIndex = this.state.params[currIndex];
                            selArr = myCurrIndex.val.split(",");
                          }
                          for (
                            let index = 0;
                            index < res.data.data.length;
                            index++
                          ) {
                            const element = res.data.data[index];
                            var keyCustomer;
                            if (element.vip_customer === 1) {
                              keyCustomer = "*";
                            } else {
                              keyCustomer = "";
                            }

                            if (
                              currIndex != null &&
                              inArray(element["customer_id"], selArr)
                            ) {
                              this.state.selectedUser.push({
                                value: element["customer_id"],
                                label:
                                  element["first_name"] +
                                  " " +
                                  element["last_name"] +
                                  " " +
                                  keyCustomer,
                              });
                            }

                            myCustomer.push({
                              value: element["customer_id"],
                              label:
                                element["first_name"] +
                                " " +
                                element["last_name"] +
                                " " +
                                keyCustomer,
                            });
                          }

                          this.setState({
                            showCustomerBlock: true,
                            my_customer_list: myCustomer,
                          });
                        })
                        .catch((err) => {
                          console.log(err);
                        });
                    }

                    API.get(`/api/feed/products`)
                      .then((res) => {
                        var productsList = [];
                        let currIndex = null;
                        for (
                          let index = 0;
                          index < this.state.params.length;
                          index++
                        ) {
                          const element = this.state.params[index];
                          if (element.name === "prodid") {
                            currIndex = index;
                          }
                        }
                        let selArr = [];
                        if (currIndex == null) {
                        } else {
                          const myCurrIndex = this.state.params[currIndex];
                          selArr = myCurrIndex.val.split(",");
                        }

                        for (
                          let index = 0;
                          index < res.data.data.length;
                          index++
                        ) {
                          const element = res.data.data[index];

                          if (
                            currIndex != null &&
                            inArray(element["product_id"], selArr)
                          ) {
                            this.state.selectedProducts.push({
                              value: element["product_id"],
                              label: element["product_name"],
                            });
                          }

                          productsList.push({
                            value: element["product_id"],
                            label: element["product_name"],
                          });
                        }

                        this.setState({
                          product_list: productsList,
                          is_loading: false,
                        });
                      })
                      .catch((err) => {
                        console.log(err);
                      });
                  })
                  .catch((err) => {
                    console.log(err);
                  });
              })
              .catch((err) => {
                console.log(err);
              });
          })
          .catch((err) => {
            console.log(err);
          });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  tdClassName = (fieldValue, row) => {
    //var dynamicClass = "width-150 ";
    var dynamicClass = "";
    if (row.vip_customer === 1) {
      dynamicClass += "bookmarked-column ";
    }
    return dynamicClass;
  };

  trClassName = (row, rowIndex) => {
    var ret = " ";
    var selDueDate = row.assignemnt_due_date;
    var dueDate = localDate(selDueDate);
    var today = localDate(row.today_date);
    var timeDiff = dueDate.getTime() - today.getTime();

    if (timeDiff > 0) {
    } else {
      ret += "tr-red";
    }

    return ret;
  };

  render() {
    return (
      <>
        {this.state.is_loading === true && (
          <>
            <div className="loderOuter">
              <div className="loader">
                <img src={loaderlogo} alt="logo" />
                <div className="loading">Loading...</div>
              </div>
            </div>
          </>
        )}
        {this.state.is_loading === false && (
          <div className="content-wrapper">
            <section className="content">
              <div className="boxPapanel content-padding form-min-hight">
                <div className="clearfix serchapanel teamSearch">
                  <div className="mb-15">
                    <label class="customCheckBox">
                      Display Deleted Tasks
                      <input
                        type="checkbox"
                        defaultChecked={this.state.show_deleted_tasks}
                        onChange={(e) => this.showDeletedTasks(e)}
                      />
                      <span class="checkmark-check"></span>
                    </label>

                    <label class="customCheckBox">
                      Display Inactive Users
                      <input
                        type="checkbox"
                        defaultChecked={this.state.show_dummy_users}
                        onChange={(e) => this.showDummyTasks(e)}
                      />
                      <span class="checkmark-check"></span>
                    </label>
                    {` `}

                    <LinkWithTooltip
                      tooltip={`List of users who has not yet activated account.`}
                      href="#"
                      id="tooltip-1"
                      clicked={(e) => this.checkHandler(e)}
                    >
                      <i
                        class="fa fa-exclamation-circle"
                        aria-hidden="true"
                      ></i>
                    </LinkWithTooltip>
                  </div>

                  <ul>
                    <li>
                      <div className="form-group">
                        <label>Ref No</label>
                        <input
                          type="text"
                          name="ref_no"
                          className="form-control"
                          value={this.state.selectedTaskRef}
                          onChange={(e) => this.filterWords(e)}
                          style={{ textTransform: "uppercase" }}
                        />
                      </div>
                    </li>

                    {this.state.request_type &&
                      this.state.showRequest === true && (
                        <li>
                          <div className="form-group  has-feedback">
                            <label>Request Type</label>
                            <Select
                              isMulti
                              className="basic-single"
                              classNamePrefix="select"
                              defaultValue={this.state.selectedRequests}
                              isClearable={true}
                              isSearchable={true}
                              name="rid"
                              options={this.state.request_type}
                              onChange={this.filterSearchMulti}
                            />
                          </div>
                        </li>
                      )}

                    {this.state.product_list && (
                      <li>
                        <div className="form-group  has-feedback">
                          <label>Products</label>
                          <Select
                            isMulti
                            className="basic-single"
                            classNamePrefix="select"
                            defaultValue={this.state.selectedProducts}
                            isClearable={true}
                            isSearchable={true}
                            name="prodid"
                            options={this.state.product_list}
                            onChange={this.filterSearchMulti}
                          />
                        </div>
                      </li>
                    )}

                    {this.state.company && (
                      <li>
                        <div className="form-group  has-feedback">
                          <label>Customer</label>
                          <Select
                            isMulti
                            className="basic-single"
                            classNamePrefix="select"
                            defaultValue={this.state.selectedCustomers}
                            isClearable={true}
                            isSearchable={true}
                            name="compid"
                            options={this.state.company}
                            onChange={this.filterSearchMulti}
                          />
                        </div>
                      </li>
                    )}

                    {this.state.showCustomerBlock && (
                      <li>
                        <div className="form-group  has-feedback">
                          <label>User</label>
                          <Select
                            isMulti
                            className="basic-single"
                            classNamePrefix="select"
                            defaultValue={this.state.selectedUser}
                            isClearable={true}
                            isSearchable={true}
                            name="cid"
                            options={this.state.my_customer_list}
                            onChange={this.filterSearchMulti}
                          />
                        </div>
                      </li>
                    )}

                    {this.state.my_employee_list &&
                      this.state.showEmployee === true && (
                        <li>
                          <div className="form-group  has-feedback">
                            <label>Employees</label>
                            <Select
                              isMulti
                              className="basic-single"
                              classNamePrefix="select"
                              defaultValue={this.state.selectedEmployee}
                              isClearable={true}
                              isSearchable={true}
                              name="eid"
                              options={this.state.my_employee_list}
                              onChange={this.filterSearchMulti}
                            />
                          </div>
                        </li>
                      )}

                    {this.state.designations &&
                      this.state.showEmployee === true && (
                        <li>
                          <div className="form-group  has-feedback">
                            <label>Functions</label>
                            <Select
                              isMulti
                              className="basic-single"
                              classNamePrefix="select"
                              defaultValue={this.state.selectedFunctions}
                              isClearable={true}
                              isSearchable={true}
                              name="desig"
                              options={this.state.designations}
                              onChange={this.filterSearchMulti}
                            />
                          </div>
                        </li>
                      )}

                    <li>
                      <div className="form-group react-date-picker">
                        <label>Date From</label>
                        <div className="form-control">
                          <DatePicker
                            name={"date_from"}
                            className="borderNone"
                            dateFormat="dd/MM/yyyy"
                            autoComplete="off"
                            selected={this.state.date_from}
                            onChange={this.filterDateFrom}
                          />
                        </div>
                        {this.state.date_from && (
                          <button
                            onClick={this.removeDateFrom}
                            className="date-close"
                          >
                            x
                          </button>
                        )}
                      </div>
                    </li>

                    <li>
                      <div className="form-group react-date-picker">
                        <label>Date To</label>
                        <div className="form-control">
                          <DatePicker
                            name={"date_to"}
                            className="borderNone"
                            dateFormat="dd/MM/yyyy"
                            autoComplete="off"
                            selected={this.state.date_to}
                            onChange={this.filterDateTo}
                          />
                        </div>
                        {this.state.date_to && (
                          <button
                            onClick={this.removeDateTo}
                            className="date-close"
                          >
                            x
                          </button>
                        )}
                      </div>
                    </li>

                    {this.state.sla_arr && (
                      <li>
                        <div className="form-group  has-feedback">
                          <label>Due On</label>
                          <Select
                            className="basic-single"
                            classNamePrefix="select"
                            defaultValue={0}
                            isClearable={true}
                            isSearchable={true}
                            name="sla"
                            options={this.state.sla_arr}
                            onChange={this.filterSearch}
                          />
                        </div>
                      </li>
                    )}
                  </ul>
                </div>
                {this.state.tableData && (
                  <>
                    <span
                      onClick={(e) => this.downloadXLSX(e)}
                      className="pull-right"
                    >
                      <LinkWithTooltip
                        tooltip={`Click here to download excel`}
                        href="#"
                        id="tooltip-open"
                        clicked={(e) => this.checkHandler(e)}
                      >
                        <i className="fas fa-download"></i>
                      </LinkWithTooltip>
                    </span>
                    <BootstrapTable
                      data={this.state.tableData}
                      trClassName={this.trClassName}
                      options={this.state.pageOptions}
                      pagination
                    >
                      <TableHeaderColumn
                        isKey
                        dataField="task_ref"
                        columnClassName={this.tdClassName}
                        editable={false}
                        expandable={false}
                      >
                        <LinkWithTooltip
                          tooltip={`Tasks `}
                          href="#"
                          id="tooltip-1"
                          clicked={(e) => this.checkHandler(e)}
                        >
                          Tasks{" "}
                        </LinkWithTooltip>
                      </TableHeaderColumn>

                      <TableHeaderColumn
                        dataField="request_type"
                        //dataSort={true}
                        editable={false}
                        expandable={false}
                        dataFormat={setDescription(this)}
                      >
                        <LinkWithTooltip
                          tooltip={`Description `}
                          href="#"
                          id="tooltip-1"
                          clicked={(e) => this.checkHandler(e)}
                        >
                          Description{" "}
                        </LinkWithTooltip>
                      </TableHeaderColumn>

                      <TableHeaderColumn
                        dataField="product_name"
                        editable={false}
                        expandable={false}
                        dataFormat={setProductName(this)}
                      >
                        <LinkWithTooltip
                          tooltip={`Product Name `}
                          href="#"
                          id="tooltip-1"
                          clicked={(e) => this.checkHandler(e)}
                        >
                          Product Name{" "}
                        </LinkWithTooltip>
                      </TableHeaderColumn>

                      <TableHeaderColumn
                        dataField="display_date_added"
                        //dataSort={true}
                        editable={false}
                        expandable={false}
                        dataFormat={hoverDate(this)}
                      >
                        <LinkWithTooltip
                          tooltip={`Created `}
                          href="#"
                          id="tooltip-1"
                          clicked={(e) => this.checkHandler(e)}
                        >
                          Created{" "}
                        </LinkWithTooltip>
                      </TableHeaderColumn>

                      <TableHeaderColumn
                        dataField="display_assign_date"
                        //dataSort={true}
                        editable={false}
                        expandable={false}
                        dataFormat={hoverDate(this)}
                      >
                        <LinkWithTooltip
                          tooltip={`Assigned Date `}
                          href="#"
                          id="tooltip-1"
                          clicked={(e) => this.checkHandler(e)}
                        >
                          Assigned Date{" "}
                        </LinkWithTooltip>
                      </TableHeaderColumn>

                      {/* <TableHeaderColumn dataField='rdd' dataSort={ true } dataFormat={ setCreateDate(this) } editable={ false } expandable={ false }>RDD</TableHeaderColumn> */}

                      <TableHeaderColumn
                        dataField="display_new_due_date"
                        //dataSort={true}
                        editable={false}
                        expandable={false}
                        dataFormat={hoverDate(this)}
                      >
                        <LinkWithTooltip
                          tooltip={`Due Date `}
                          href="#"
                          id="tooltip-1"
                          clicked={(e) => this.checkHandler(e)}
                        >
                          Due Date{" "}
                        </LinkWithTooltip>
                      </TableHeaderColumn>

                      {/* <TableHeaderColumn dataField='assigned_to' dataSort={ true } editable={ false } expandable={ false } >Assigned To</TableHeaderColumn> */}

                      <TableHeaderColumn
                        dataField="dept_name"
                        //dataSort={true}
                        editable={false}
                        expandable={false}
                        dataFormat={setAssignedBy(this)}
                      >
                        <LinkWithTooltip
                          tooltip={`Assigned By `}
                          href="#"
                          id="tooltip-1"
                          clicked={(e) => this.checkHandler(e)}
                        >
                          Assigned By{" "}
                        </LinkWithTooltip>
                      </TableHeaderColumn>

                      <TableHeaderColumn
                        dataField="dept_name"
                        //dataSort={true}
                        editable={false}
                        expandable={false}
                        dataFormat={setAssignedTo(this)}
                      >
                        <LinkWithTooltip
                          tooltip={`Assigned To `}
                          href="#"
                          id="tooltip-1"
                          clicked={(e) => this.checkHandler(e)}
                        >
                          Assigned To{" "}
                        </LinkWithTooltip>
                      </TableHeaderColumn>

                      <TableHeaderColumn
                        dataField="company_name"
                        //dataSort={true}
                        expandable={false}
                        dataFormat={setCompanyName(this)}
                      >
                        <LinkWithTooltip
                          tooltip={`Customer `}
                          href="#"
                          id="tooltip-1"
                          clicked={(e) => this.checkHandler(e)}
                        >
                          Customer{" "}
                        </LinkWithTooltip>
                      </TableHeaderColumn>

                      <TableHeaderColumn
                        dataField="cust_name"
                        //dataSort={true}
                        editable={false}
                        expandable={false}
                        dataFormat={setCustomerName(this)}
                      >
                        <LinkWithTooltip
                          tooltip={`User `}
                          href="#"
                          id="tooltip-1"
                          clicked={(e) => this.checkHandler(e)}
                        >
                          User{" "}
                        </LinkWithTooltip>
                      </TableHeaderColumn>
                    </BootstrapTable>
                  </>
                )}
              </div>
            </section>
          </div>
        )}
      </>
    );
  }
}

export default MyTeamOpenTasksTable;
