import React, { Component } from 'react';
import { Formik, Field, Form } from "formik";

import eUser from '../../assets/images/e-user.png';
import eCalendar from '../../assets/images/e-calendar.png';
import eRun from '../../assets/images/e-run.png';
import eChart from '../../assets/images/e-chart.png';
import editImage from '../../assets/images/image-edit.jpg';
import plusSign from '../../assets/images/plus-sign.png';

class EmailCustomer extends Component {
    constructor() {
        super()
        this.state = {
            isHidden: false
        }
    }

    toggleHidden() {
        this.setState({
            isHidden: !this.state.isHidden
        })
    }

    render() {
        return (
            <>
                <div className="content-wrapper">
                    {/*<!-- Content Header (Page header) -->*/}
                    <section className="content-header">
                        <div className="row">
                            <div className="col-lg-9 col-sm-6 col-xs-12">
                                <h1>
                                    Email Customer <small></small>
                                </h1>
                            </div>
                            <div className="col-lg-3 col-sm-6 col-xs-12"></div>
                        </div>
                    </section>

                    {/*<!-- Main content -->*/}
                    <section className="content">
                        <div className="email-customer">

                            <h4>Title</h4>

                            {/*<!-- email-sec -->*/}
                            <div className="email-sec">
                                <div className="email-sec-head">Purchase Order</div>
                                <div className="email-sec-details">

                                    <div className="purchase-top">
                                        <div className="row">

                                            <div className="col-md-3 col-sm-6 col-xs-12">
                                                <div className="p-box">
                                                    <div className="p-img-box"> <img src={eUser} alt="Assigned" /></div>
                                                    <div className="p-con-box">
                                                        <p><small>Assigned To</small></p>
                                                        <p>Angad Uberoi</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-md-3 col-sm-6 col-xs-12">
                                                <div className="p-box">
                                                    <div className="p-img-box"> <img src={eCalendar} alt="Due Date" /></div>
                                                    <div className="p-con-box">
                                                        <p><small>Due Date</small></p>
                                                        <p>Thursday, 31 Jan</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-md-3 col-sm-6 col-xs-12">
                                                <div className="p-box">
                                                    <div className="p-img-box"> <img src={eChart} alt="Status" /></div>
                                                    <div className="p-con-box">
                                                        <p><small>Status</small></p>
                                                        <p>In Progress</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-md-3 col-sm-6 col-xs-12">
                                                <div className="p-box">
                                                    <div className="p-img-box"> <img src={eRun} alt="Required Action" /></div>
                                                    <div className="p-con-box">
                                                        <p><small>Required Action</small></p>
                                                        <p>To-Do</p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div className="purchase-btm">
                                        <textarea></textarea>
                                    </div>

                                </div>
                            </div>


                            {/*<!-- email-sec -->*/}
                            <div className="email-sec">
                                <div className="email-sec-head dropdown" onClick={this.toggleHidden.bind(this)}>Sub Tasks (2)</div>
                                {!this.state.isHidden === false ? (
                                    <div className="email-sec-details dropContentTask">
                                        <Formik>
                                            <Form>
                                                <div className="sub-task-main">

                                                    <div className="sub-task">
                                                        <div className="row">
                                                            <div className="col-md-10 col-sm-10 col-xs-12">
                                                                <label className="checkBoxcontainer">
                                                                    <Field
                                                                        type="checkbox"
                                                                        name="cifList"
                                                                        value="abc"
                                                                    />
                                                                    <span className="checkmark"></span>
                                                                    <span className="fieldTitle">Create a project plan for module 1</span>
                                                                </label>
                                                            </div>
                                                            <div className="col-md-2 col-sm-2 col-xs-12">
                                                                <div className="e-icon-holder pull-right">
                                                                    <i className="far fa-calendar-alt"></i>
                                                                    <i className="far fa-user"></i>
                                                                    <i className="fas fa-paperclip"></i>
                                                                    <span>2</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="sub-task">
                                                        <div className="row">
                                                            <div className="col-md-10 col-sm-10 col-xs-12">
                                                                <label className="checkBoxcontainer">
                                                                    <Field
                                                                        type="checkbox"
                                                                        name="cifList"
                                                                        value="abc"
                                                                    />
                                                                    <span className="checkmark"></span>
                                                                    <span className="fieldTitle">Create a project plan for module 2</span>
                                                                </label>
                                                            </div>
                                                            <div className="col-md-2 col-sm-2 col-xs-12">
                                                                <div className="e-icon-holder pull-right">
                                                                    <i className="far fa-calendar-alt"></i>
                                                                    <i className="far fa-user"></i>
                                                                    <i className="fas fa-paperclip"></i>
                                                                    <span>2</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </Form>
                                        </Formik>
                                    </div>
                                ) : (
                                        ""
                                    )}
                            </div>

                            {/*<!-- email-sec -->*/}
                            <div className="email-sec">
                                <div className="email-sec-head dropdown">Attachments</div>
                                <div className="email-sec-details dropContentAttach">
                                    <div className="attch-box">

                                        <div className="e-image-bx">
                                            <img src={editImage} alt="Dr.Reddy's" />
                                        </div>

                                        <div className="e-image-bx">
                                            <label className="btn-bs-file btn">
                                                <img src={plusSign} alt="Dr.Reddy's" />
                                                <input type="file" />
                                            </label>
                                        </div>

                                    </div>
                                </div>
                            </div>


                        </div>
                    </section>
                    {/*<!-- Main content -->*/}
                </div>
            </>
        );
    }
}

export default EmailCustomer;