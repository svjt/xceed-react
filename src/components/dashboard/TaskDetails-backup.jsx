import React, { Component } from 'react';
import { Row, Col, ButtonToolbar, Button, Modal,Panel } from "react-bootstrap";
import dateFormat from 'dateformat';
import API from "../../shared/axios";

import { Redirect } from 'react-router-dom';
import Loader from 'react-loader-spinner';


const priority_arr = [
    {priority_id:1,priority_value:'Low'},
    {priority_id:2,priority_value:'Medium'},
    {priority_id:3,priority_value:'High'}
];

const req_category_arr = [
    {request_category_id:1,request_category_value:'New Order'},
    {request_category_id:2,request_category_value:'Other'},
    {request_category_id:3,request_category_value:'Complaint'},
    {request_category_id:4,request_category_value:'Payment'}
];

class TaskDetails extends Component {

    constructor(props){
        super(props);

        //const {changeStatus} = this.props.location.state;

        //console.log(this.props);
        this.state = {
            taskDetails: [],
            task_id : this.props.match.params.id
        };
    }

    setDescription = (customer_id) =>{
        // if(customer_id > 0){
        //     return row.req_name;
        // }else{
        //     return row.title;
        // }
    }
    
    setCreateDate = (date_added) =>{

        if( date_added !== undefined){
            //var replacedDate = date_added.split('T');
            var mydate = new Date(date_added);
            //console.log(mydate);
            return dateFormat(mydate, "dd/mm/yyyy");
        }        
    }
    
    setDaysPending = (due_date) =>{
        //var replacedDate = due_date.split('T');
        var dueDate = new Date(due_date);
        var today  = new Date();
        var timeDiff = dueDate.getTime() - today.getTime();
        //console.log(timeDiff);
        if(timeDiff > 0 ){
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
            return diffDays;
        }else{
            return 0;
        }
        
    }

    setPriorityName = (priority_id) =>{
        var ret = 'Not Set';
        for (let index = 0; index < priority_arr.length; index++) {
            const element = priority_arr[index];
            if(element['priority_id'] == priority_id ){
                ret = element['priority_value'];
            }
        }
    
        return ret
    
    }

    setReqCategoryName = (req_cate_id) => {
        var ret = 'Not Set';
        for (let index = 0; index < req_category_arr.length; index++) {
            const element = req_category_arr[index];
            if(element['request_category_id'] == req_cate_id ){
                ret = element['request_category_value'];
            }
        }
    
        return ret
    }

    checkPharmacopoeia(request_type){

        if(request_type === null){
            return false;
        }else{
            if(request_type === 22 || request_type === 21 || request_type === 19 || request_type === 18 || request_type === 17 || request_type === 2 || request_type === 3 || request_type === 4 || request_type === 10 || request_type === 12){
                return true;
            }else{
                return false;
            }
        }
        
    }

    checkPolymorphicForm(request_type){
        if(request_type === null){
            return false;
        }else{
            if(request_type === 2 || request_type === 3){
                return true;
            }else{
                return false;
            }
        }
    }

    checkStabilityDataType(request_type){
        if(request_type === null){
            return false;
        }else{
            if(request_type === 13){
                return true;
            }else{
                return false;
            }
        }
    }

    checkAuditSiteName(request_type){
        if(request_type === null){
            return false;
        }else{
            if(request_type === 9){
                return true;
            }else{
                return false;
            }
        }
    }

    checkAuditVistDate(request_type){
        if(request_type === null){
            return false;
        }else{
            if(request_type === 9){
                return true;
            }else{
                return false;
            }
        }
    }

    setDateFormat(date_value){
        var mydate = new Date(date_value);
        //console.log(mydate);
        return dateFormat(mydate, "dd/mm/yyyy");
    }

    componentDidMount = () => {
       
        if(this.state.task_id > 0){
            
            API.get(`/api/tasks/${this.state.task_id}`)
            .then(res => {
               
                this.setState({
                    taskDetails: res.data.data
                });

                console.log(res.data.data);

                //STOP THIS CALL FOR ALLOCATED TASK
                //if(res.data.data.status === 1){
                var post_data = {};
                API.put(`/api/tasks/status/${this.state.task_id}`,post_data)
                .then(response => {
                    //console.log(response.data.data);
                    //nextProps.refreshTableStatus(nextProps.currRow);
                })
                .catch(err => {
                    console.log(err);
                });
                //}
            })
            .catch(err => {
                console.log(err);
            });
        }
    }

    // componentWillReceiveProps = (nextProps) => {

    //     console.log("next props", nextProps );

    //     if(nextProps.currRow.task_id > 0){
    //         //console.log(nextProps.currRow.task_id);
    //         API.get(`/api/tasks/${nextProps.currRow.task_id}`)
    //         .then(res => {
    //             console.log('MY TASKS');
    //             console.log(res.data.data);
    //             this.setState({
    //                 taskDetails: res.data.data
    //             });

    //             if(typeof nextProps.allocated !== 'undefined' && nextProps.allocated == 1){

    //             }else{
    //                 if(res.data.data.status === 1){
    //                     var post_data = {};
    //                     API.put(`/api/tasks/status/${nextProps.currRow.task_id}`,post_data)
    //                     .then(res => {
    //                         //console.log(res.data.data);
    //                         nextProps.refreshTableStatus(nextProps.currRow);
    //                     })
    //                     .catch(err => {
    //                         console.log(err);
    //                     });
    //                 }
    //             }
    //         })
    //         .catch(err => {
    //             console.log(err);
    //         });
    //     }
    // }

    /* handleClose = () => {
        var close = {showTaskDetails: false};
        this.setState(close);
        this.props.handleClose(close);
    }; */

    createAccordion = () => {

        let accordion = [];
        var sub_tasks = this.state.taskDetails.sub_tasks;
        
        for (let index = 0; index < sub_tasks.length; index++) {
            
            accordion.push(
                <Panel eventKey={sub_tasks[index].task_id} key={index}>
                    <Panel.Heading>
                    <Panel.Title toggle>{sub_tasks[index].task_ref_id}</Panel.Title>
                    </Panel.Heading>
                    <Panel.Body collapsible>
                        <div className="form-group">
                            <label>
                                Task:
                            </label>
                                {sub_tasks[index].task_ref_id}
                        </div> 
                        <div className="form-group">
                            <label>
                                Customer Name:
                            </label>
                                {sub_tasks[index].cust_name}
                        </div>
                        <div className="form-group">
                            <label>
                                Created:
                            </label>
                                {sub_tasks[index].add_date}
                        </div>
                        <div className="form-group">
                            <label>
                                Days Pending:
                            </label>
                                {sub_tasks[index].days_pending}
                        </div>
                        <div className="form-group">
                            <label>
                                Assigned To:
                            </label>
                                {sub_tasks[index].assigned_to}
                        </div>
                        <div className="form-group">
                            <label>
                                Priority:
                            </label>
                                {sub_tasks[index].priority_value}
                        </div>
                        <div className="form-group">
                            <label>
                                Description:
                            </label>
                                {sub_tasks[index].description}
                        </div>
                    </Panel.Body>
                </Panel>
            );
        }

        return accordion;

        // return (
        //     <>
        //     <Panel eventKey="1">
        //         <Panel.Heading>
        //         <Panel.Title toggle>Collapsible Group Item #1</Panel.Title>
        //         </Panel.Heading>
        //         <Panel.Body collapsible>
        //         Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
        //         richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard
        //         dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
        //         moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
        //         assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore
        //         wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur
        //         butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim
        //         aesthetic synth nesciunt you probably haven't heard of them accusamus
        //         labore sustainable VHS.
        //         </Panel.Body>
        //     </Panel>
        //     <Panel eventKey="2">
        //         <Panel.Heading>
        //         <Panel.Title toggle>Collapsible Group Item #2</Panel.Title>
        //         </Panel.Heading>
        //         <Panel.Body collapsible>
        //         Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
        //         richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard
        //         dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
        //         moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
        //         assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore
        //         wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur
        //         butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim
        //         aesthetic synth nesciunt you probably haven't heard of them accusamus
        //         labore sustainable VHS.
        //         </Panel.Body>
        //     </Panel>
        //     <Panel eventKey="3">
        //         <Panel.Heading>
        //         <Panel.Title toggle>Collapsible Group Item #3</Panel.Title>
        //         </Panel.Heading>
        //         <Panel.Body collapsible>
        //         Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
        //         richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard
        //         dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
        //         moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
        //         assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore
        //         wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur
        //         butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim
        //         aesthetic synth nesciunt you probably haven't heard of them accusamus
        //         labore sustainable VHS.
        //         </Panel.Body>
        //     </Panel>
        //     </>
        // );
    }

    render(){

        // SATYAJIT
        if(this.props.match.params.id === 0 || this.props.match.params.id === ""){
            return <Redirect to='/user/dashboard'/>
        }

        if(this.state.task_id > 0) {

            return(
                <>
                    <div className="content-wrapper">
                        <section className="content-header">
                            <h1>
                                Task Details
                            </h1>
                        </section>
                        <section className="content">
                        <div className="boxPapanel content-padding">
                            <Row>
                                <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                        <label>Task:</label> {this.state.taskDetails.task_ref}
                                    </div>
                                </Col>

                                <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                        <label>Created On:</label> {this.setCreateDate(this.state.taskDetails.date_added)}
                                    </div>
                                </Col>

                                <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                        <label>Days Pending:</label> {this.setDaysPending(this.state.taskDetails.due_date)}
                                    </div>
                                </Col>

                                <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                        <label>Priority:</label> {this.setPriorityName(this.state.taskDetails.priority)}
                                    </div>
                                </Col>

                                {/* REQUEST ID > 0 */}
                                
                                {this.state.taskDetails.request_type > 0 && <Col xs={12} sm={6} md={4}><div className="form-group">
                                    <label>Customer Name:</label> {this.state.taskDetails.first_name} {this.state.taskDetails.last_name}
                                </div></Col> }
                                
                                {this.state.taskDetails.request_type > 0 && <Col xs={12} sm={6} md={4}><div className="form-group">
                                    <label>Request Type:</label> {this.state.taskDetails.req_name}
                                </div></Col>}
                                
                                
                                {this.state.taskDetails.request_type > 0 && <Col xs={12} sm={6} md={4}><div className="form-group">
                                    <label>Requirement:</label> {this.state.taskDetails.content}
                                </div></Col>}
                                
                                
                                {this.state.taskDetails.request_type > 0 && <Col xs={12} sm={6} md={4}><div className="form-group">
                                    <label>Request Category:</label> {this.setReqCategoryName(this.state.taskDetails.request_category)}
                                </div></Col>}
                                
                                
                                {this.state.taskDetails.request_type > 0 && <Col xs={12} sm={6} md={4}><div className="form-group">
                                    <label>Country:</label> {this.state.taskDetails.country_name}
                                </div></Col>}
                                
                                
                                {this.state.taskDetails.request_type > 0 && <Col xs={12} sm={6} md={4}><div className="form-group">
                                    <label>Product Name:</label> {this.state.taskDetails.product_name}
                                </div></Col>}
                                
                                
                                {this.checkPharmacopoeia(this.state.taskDetails.request_type) === true && <Col xs={12} sm={6} md={4}><div className="form-group">
                                    <label>Pharmacopoeia:</label> {this.state.taskDetails.pharmacopoeia}
                                </div></Col>}
                                
                                {this.checkPolymorphicForm(this.state.taskDetails.request_type) === true && <Col xs={12} sm={6} md={4}><div className="form-group">
                                    <label>Polymorphic Form:</label> {this.state.taskDetails.polymorphic_form}
                                </div></Col>}
                                
                                {this.checkStabilityDataType(this.state.taskDetails.request_type) === true && <Col xs={12} sm={6} md={4}><div className="form-group">
                                    <label>Stability Data Type:</label> {this.state.taskDetails.stability_data_type}
                                </div></Col>}

                                {this.checkAuditSiteName(this.state.taskDetails.request_type) === true && <Col xs={12} sm={6} md={4}><div className="form-group">
                                    <label>Site Name:</label> {this.state.taskDetails.audit_visit_site_name}
                                </div></Col>}

                                {this.checkAuditVistDate(this.state.taskDetails.request_type) === true && <Col xs={12} sm={6} md={4}><div className="form-group">
                                    <label>Audit/Visit Date:</label> {this.setDateFormat(this.state.taskDetails.request_audit_visit_date)}
                                </div></Col>}
                                
                                {(this.state.taskDetails.request_type == 0 || this.state.taskDetails.request_type == null) && <Col xs={12} sm={6} md={4}><div className="form-group">
                                    <label>Title:</label> {this.state.taskDetails.title}
                                </div></Col>}
                                
                                {(this.state.taskDetails.request_type == 0 || this.state.taskDetails.request_type == null) && <Col xs={12} sm={6} md={4}><div className="form-group">
                                    <label>Content:</label> {this.state.taskDetails.content}
                                </div></Col>}
                                
                            </Row>
                        </div>
                        </section>
                    </div>

                    {/* { this.state.taskDetails.sub_tasks.length > 0 && 
                    <PanelGroup accordion id="task_panel" defaultActiveKey={this.state.taskDetails.sub_tasks[0].task_id} >
                        {this.createAccordion()}
                    </PanelGroup>
                    } */}
                </>    
            );
        } else {
            
            return (
                <>
                    <div className="loading_reddy_outer">
                        <div className="loading_reddy" >
                            <Loader 
                                type="Puff"
                                color="#00BFFF"
                                height="50"	
                                width="50"
                                verticalAlign="middle"
                            />
                        </div>
                    </div>
                </>
            );
        }
        
    }
}

export default TaskDetails;