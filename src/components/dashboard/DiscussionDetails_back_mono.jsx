import React, { Component } from "react";
import { Button } from "react-bootstrap";
import dateFormat from "dateformat";
import API from "../../shared/axios";

//import Dropzone from 'react-dropzone'
//import classNames from 'classnames'

import { Redirect } from "react-router-dom";
import Loader from "react-loader-spinner";
import commenLogo from "../../assets/images/drreddylogosmall.png";
//import paperClip from '../../assets/images/paper-clip.jpg';
//import pdfIcon from '../../assets/images/pdf-icon.png';
//import docIcon from '../../assets/images/doc-icon.png';
//import imgIcon from '../../assets/images/img-icon.png';
//import {Link} from "react-router-dom"

import * as Yup from "yup";
import { Formik, Field, Form } from "formik";
import swal from "sweetalert";

const path = `${process.env.REACT_APP_API_URL}/api/tasks/download_discussion/`; // SATYAJIT

// initialize form and their validation
const initialValues = {
  comment: ""
};

const commentSchema = Yup.object().shape({
  comment: Yup.string()
    .trim("Please remove whitespace")
    .strict()
    .required("Please enter your comment")
});
// end here

class DiscussionDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      comment: "",
      discussDetails: [],
      task_id: this.props.match.params.did
    };
  }

  handleChange = (e, field) => {
    this.setState({
      [field]: e.target.value
    });
  };

  getCommentList = id => {
    API.get(`api/tasks/discussions/${id}`)
      .then(res => {

        // console.log("download list", res);

        this.setState({
          discussDetails : res.data.data.discussion,
          discussStatus  : res.data.data.status,
          discussTaskRef : res.data.data.task_ref,
          discussReqName : res.data.data.req_name,
          discussFile    : res.data.data.files
        });
        //console.log("state",  this.state );
      })
      .catch(err => {
        console.log(err);
      });
  };

  componentDidMount = () => {
    if (this.state.task_id > 0) {
      this.getCommentList(this.state.task_id);
      initialValues.comment = this.state.comment;
    }
  };

  submitComment = (values, actions) => {
    let tid = this.props.match.params.did;

    if (tid) {
      API.post(`api/tasks/discussions/${tid}`, { comment: values.comment })
        .then(res => {
          this.getCommentList(tid);
          values.comment = "";
          swal("Comment posted successfully!", {
            icon: "success"
          });
        })
        .catch(err => {
          console.log(err);
        });
    }
  };

  closeBrowserWindow = () => {
    window.close();
  };

  onDrop = (acceptedFiles, rejectedFiles) => {
    console.log( acceptedFiles, rejectedFiles );
  }

  handleTabs = (event) =>{
        
      if(event.currentTarget.className === "active" ){
          //DO NOTHING
      }else{

          var elems          = document.querySelectorAll('[id^="tab_"]');
          var elemsContainer = document.querySelectorAll('[id^="show_tab_"]');
          var currId         = event.currentTarget.id;

          for (var i = 0; i < elems.length; i++){
              elems[i].classList.remove('active');
          }

          for (var j = 0; j < elemsContainer.length; j++){
              elemsContainer[j].style.display = 'none';
          }

          event.currentTarget.classList.add('active');
          event.currentTarget.classList.add('active');
          document.querySelector('#show_'+currId).style.display = 'block';
      }
      // this.tabElem.addEventListener("click",function(event){
      //     alert(event.target);
      // }, false);
  }


  render() {
    //SATYAJIT
    if (
      this.props.match.params.did === 0 ||
      this.props.match.params.did === ""
    ) {
      return <Redirect to="/user/dashboard" />;
    }

    if (this.state.task_id > 0) {
      return (
        <>
        <div className="row">
          <div className="col-xs-12">
          
              <div className="nav-tabs-custom">

                  <ul className="nav nav-tabs">
                      <li className="active" onClick = {(e) => this.handleTabs(e)} id="tab_1" >DISCUSSION DETAILS</li>
                      <li onClick = {(e) => this.handleTabs(e)}  id="tab_2">FILES</li>
                  </ul>

                  <div className="tab-content">

                      <div className="tab-pane active" id="show_tab_1">
                                                
                        <div className="content-wrapper">
                          <section className="content-header">
                            <h1>Discussion Details</h1>
                          </section>
                          <section className="content">
                            <div className="boxPapanel content-padding">
                              <div className="disHead">
                                <h3>
                                  {this.state.discussReqName} | {this.state.discussTaskRef}
                                </h3>
                              </div>

                              <div className="comment-list-main-wrapper">
                                {this.state.discussDetails.map((comments, i) => (
                                  <div key={i} className="comment">
                                    <div className="row">
                                      <div className="col-md-10 col-sm-10 col-xs-9">
                                        <div className="imageArea">
                                          <div className="imageBorder">
                                            <img alt="noimage" src={commenLogo} />
                                          </div>
                                        </div>
                                        <div className="conArea">
                                          <p>
                                            {comments.posted_by_type === "C"
                                              ? comments.cust_first_name +
                                                " " +
                                                comments.cust_last_name
                                              : comments.emp_first_name +
                                                " " +
                                                comments.emp_last_name +
                                                " (DRL)"}
                                          </p>
                                          <span>{comments.comment}</span>
                                          {/* <ul className="conDocList">
                                                                  <li><img alt="noimage" src={pdfIcon}/><Link to="#">Document 01</Link></li>
                                                                  <li><img alt="noimage" src={docIcon}/><Link to="#">Document 02</Link></li>
                                                                  <li><img alt="noimage" src={docIcon}/><Link to="#">Document 02</Link></li>
                                                              </ul> */}
                                        </div>
                                        <div className="clearfix" />
                                      </div>

                                      <div className="col-md-2 col-sm-2 col-xs-3">
                                        <div className="dateArea">
                                          <p>
                                            {dateFormat(
                                              new Date(comments.date_added),
                                              "ddd, mmm dS, yyyy"
                                            )}
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                ))}
                              </div>

                              <Formik
                                initialValues={initialValues}
                                validationSchema={commentSchema}
                                onSubmit={this.submitComment}
                              >
                                {({
                                  values,
                                  errors,
                                  touched,
                                  handleChange,
                                  handleBlur,
                                  setFieldValue
                                }) => {
                                  // console.log(errors);

                                  return (
                                    <div className="clearfix tdBtmlist">
                                      <Form>
                                        <div className="commentBox">
                                          <div className="form-group">
                                            {/*<label>Enter your comment</label>*/}

                                            <Field
                                              name="comment"
                                              component="textarea"
                                              className="form-control"
                                              autoComplete="off"
                                              placeholder="Enter your comment"
                                              onChange={handleChange}
                                            />
                                            {errors.comment && touched.comment && (
                                              <label className="redError">
                                                {errors.comment}
                                              </label>
                                            )}
                                          </div>
                                          {/* <div className="browse-btn">
                                                                  <label class="btn-bs-file btn">
                                                                      <img 
                                                                      alt="noimage"
                                                                      src={paperClip}/>
                                                                      <input type="file" />
                                                                  </label>
                                                              </div> */}

                                          {/* <div className="browse-btn">
                                              <Dropzone onDrop={this.onDrop}>
                                                  {({getRootProps, getInputProps, isDragActive}) => {
                                                    return (
                                                      <div
                                                        {...getRootProps()}
                                                        className={classNames('dropzone', {'dropzone--isActive': isDragActive})}
                                                      >
                                                        <input {...getInputProps()} />
                                                        {
                                                          isDragActive ?
                                                            <p>Drop files here...</p> :
                                                            <p>Upload file</p>
                                                        }
                                                      </div>
                                                    )
                                                  }}
                                                </Dropzone>
                                          </div> */}

                                          <div className="button-footer commentBox-footer">
                                            <Button
                                              type="submit"
                                              className="btn btn-danger btn-flat"
                                            >
                                              {" "}
                                              Send{" "}
                                            </Button>
                                          </div>
                                        </div>
                                      </Form>
                                    </div>
                                  );
                                }}
                              </Formik>
                            </div>
                          </section>
                        </div>

                      </div>

                      <div className="tab-pane" id="show_tab_2">

                      <div className="content-wrapper">
                          <section className="content-header">
                            <h1>File Details</h1>
                          </section>
                          <section className="content">
                            <div className="boxPapanel content-padding">
                              <div className="disHead">
                                <h3>
                                  {this.state.discussReqName} | {this.state.discussTaskRef}
                                </h3>
                              </div>

                              <div className="comment-list-main-wrapper">

                                {this.state.discussFile && 
                                this.state.discussFile !== '' &&
                                this.state.discussFile.map((file, i) => (
                                  <div key={i} className="comment">
                                    <div className="row">
                                      <div className="col-md-10 col-sm-10 col-xs-9">
                                        <div className="imageArea">
                                          <div className="imageBorder">
                                            <img alt="noimage" src={commenLogo} />
                                          </div>
                                        </div>
                                        <div className="conArea">


  <a href={`${path}${file.upload_id}`} target="_blank" download rel="noopener noreferrer">
              {file.actual_file_name}
  </a>

                                        </div>
                                        <div className="clearfix" />
                                      </div>

                                      <div className="col-md-2 col-sm-2 col-xs-3">
                                        <div className="dateArea">
                                          <p>
                                            {dateFormat(
                                              new Date(file.date_added),
                                              "ddd, mmm dS, yyyy"
                                            )}
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                ))}
                              </div>
                            </div>
                          </section>
                        </div>
                          
                      </div>
                  </div>
              </div>
          </div>
      </div>
      </>                          
        
      );
    } else {
      return (
        <>
          <div className="loading_reddy_outer">
            <div className="loading_reddy">
              <Loader
                type="Puff"
                color="#00BFFF"
                height="50"
                width="50"
                verticalAlign="middle"
              />
            </div>
          </div>
        </>
      );
    }
  }
}

export default DiscussionDetails;
