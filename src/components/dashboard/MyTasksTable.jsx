import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  getMyDrupalId,
  localDate,
  localDateTime,
  htmlDecode,
} from "../../shared/helper";
import base64 from "base-64";
import { Row, Col, Tooltip, OverlayTrigger } from "react-bootstrap";
import "./Dashboard.css";

import BMVerticalMenu from "../bmoverview/BMVerticalMenu";
import CSCVerticalMenu from "../cscoverview/CSCVerticalMenu";
import RAVerticalMenu from "../raoverview/RAVerticalMenu";
import OTHRVerticalMenu from "../othroverview/OTHRVerticalMenu";

import StatusColumn from "./StatusColumn";
import SubTaskTable from "./SubTaskTable";

import CreateSubTaskPopup from "./CreateSubTaskPopup";
import AssignPopup from "./AssignPopup";
import RespondCustomerPopup from "./RespondCustomerPopup";
import RespondBackPopup from "./RespondBackPopup";
import ProformaPopup from "./ProformaPopup";
import AuthorizePopup from "./AuthorizePopup";
import IncreaseSlaPopup from "./IncreaseSlaPopup";
import IncreaseSlaSubTaskPopup from "./IncreaseSlaSubTaskPopup";
import ClosedCommentMyTask from './ClosedCommentMyTask';
import Poke from "./Poke";
import PokeSPOC from "./PokeSPOC";

import DeleteTaskPopup from "./DeleteTaskPopup";
import IPDOPopup from "./IPDOPopup";



import exclamationImage from "../../assets/images/exclamation-icon.svg";
import CopyCreateMyTasksPopup from "./CopyCreateMyTasksPopup";
import ReclassifyMyTasksPopup from "./ReclassifyMyTasksPopup";



//import TaskDetails from './TaskDetails';

import API from "../../shared/axios";
import swal from "sweetalert";

import { Link } from "react-router-dom";
import Pagination from "react-js-pagination";

import { showErrorMessageFront } from "../../shared/handle_error_front";
import { format } from "path";

import dateFormat from "dateformat";

import ReactHtmlParser from "react-html-parser";

//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";

const portal_url = `${process.env.REACT_APP_PORTAL_URL}`; // SATYAJIT

const priority_arr = [
  { priority_id: 1, priority_value: "Low" },
  { priority_id: 2, priority_value: "Medium" },
  { priority_id: 3, priority_value: "High" },
];

const ipdoTypes = [1,2,3,4,5,8,9,10,12,13,14,15,16,21,38,42,35,22];

const getActions = (refObj) => (cell, row) => {
  if (refObj.props.dashType === "BM") {
    return (
      <BMVerticalMenu
        id={cell}
        currRow={row}
        review_task={refObj.review_task}
        showAssignPopup={refObj.showAssignPopup}
        showSubTaskPopup={refObj.showSubTaskPopup}
        showRespondCustomerPopup={refObj.showRespondCustomerPopup}
        showCloseTaskPopup={refObj.showCloseTaskPopup}
        showDeleteTaskPopup={refObj.showDeleteTaskPopup}
        showRespondPopup={refObj.showRespondPopup}
        showProforma={refObj.showProforma}
        showAuthorizeTaskPopup={refObj.showAuthorizeTaskPopup}
        showIncreaseSlaPopup={refObj.showIncreaseSlaPopup}
        poke={refObj.showPoke}
        poke_spoc={refObj.showPokeSPOC}
        showCloneMainTaskPopup={refObj.showCloneMainTaskPopup}
        reclassify={refObj.reclassify}
        cloneSubTask={refObj.showCloneSubTaskPopup}
        showApproveTaskPopup={refObj.showApproveTaskPopup}
        for_review_task={row.for_review_task}
        is_spoc_review={row.is_spoc_review}
      />
    );
  } else if (refObj.props.dashType === "CSC") {
    return (
      <CSCVerticalMenu
        id={cell}
        currRow={row}
        review_task={refObj.review_task}
        showAssignPopup={refObj.showAssignPopup}
        showSubTaskPopup={refObj.showSubTaskPopup}
        showRespondCustomerPopup={refObj.showRespondCustomerPopup}
        showCloseTaskPopup={refObj.showCloseTaskPopup}
        showRespondPopup={refObj.showRespondPopup}
        showProforma={refObj.showProforma}
        showAuthorizeTaskPopup={refObj.showAuthorizeTaskPopup}
        showIncreaseSlaPopup={refObj.showIncreaseSlaPopup}
        poke={refObj.showPoke}
        poke_spoc={refObj.showPokeSPOC}
        showCloneMainTaskPopup={refObj.showCloneMainTaskPopup}
        reclassify={refObj.reclassify}
        cloneSubTask={refObj.showCloneSubTaskPopup}
        showApproveTaskPopup={refObj.showApproveTaskPopup}
        for_review_task={row.for_review_task}
        is_spoc_review={row.is_spoc_review}
      />
    );
  } else if (refObj.props.dashType === "RA") {
    return (
      <RAVerticalMenu
        id={cell}
        currRow={row}
        review_task={refObj.review_task}
        showAssignPopup={refObj.showAssignPopup}
        showSubTaskPopup={refObj.showSubTaskPopup}
        showRespondCustomerPopup={refObj.showRespondCustomerPopup}
        showCloseTaskPopup={refObj.showCloseTaskPopup}
        showRespondPopup={refObj.showRespondPopup}
        showProforma={refObj.showProforma}
        showAuthorizeTaskPopup={refObj.showAuthorizeTaskPopup}
        showIncreaseSlaPopup={refObj.showIncreaseSlaPopup}
        poke={refObj.showPoke}
        poke_spoc={refObj.showPokeSPOC}
        showCloneMainTaskPopup={refObj.showCloneMainTaskPopup}
        reclassify={refObj.reclassify}
        cloneSubTask={refObj.showCloneSubTaskPopup}
        showApproveTaskPopup={refObj.showApproveTaskPopup}
        for_review_task={row.for_review_task}
        is_spoc_review={row.is_spoc_review}
      />
    );
  } else if (refObj.props.dashType === "OTHR") {
    return (
      <OTHRVerticalMenu
        id={cell}
        currRow={row}
        review_task={refObj.review_task}
        showAssignPopup={refObj.showAssignPopup}
        showSubTaskPopup={refObj.showSubTaskPopup}
        showRespondCustomerPopup={refObj.showRespondCustomerPopup}
        showCloseTaskPopup={refObj.showCloseTaskPopup}
        showRespondPopup={refObj.showRespondPopup}
        showProforma={refObj.showProforma}
        showAuthorizeTaskPopup={refObj.showAuthorizeTaskPopup}
        poke={refObj.showPoke}
        poke_spoc={refObj.showPokeSPOC}
        reclassify={refObj.reclassify}
        showApproveTaskPopup={refObj.showApproveTaskPopup}
        for_review_task={row.for_review_task}
        is_spoc_review={row.is_spoc_review}
      />
    );
  }
};

const getStatusColumn = (refObj) => (cell, row) => {
  return <StatusColumn rowData={row} />;
};

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const setDescription = (refOBj) => (cell, row) => {
  if (row.parent_id > 0) {
    let stripHtml = row.title.replace(/<[^>]+>/g, "");

    return (
      <LinkWithTooltip
        tooltip={`${ReactHtmlParser(stripHtml)}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {ReactHtmlParser(stripHtml)}
      </LinkWithTooltip>
    );
  } else {
    let stripHtml = row.req_name.replace(/<[^>]+>/g, "");

    return (
      <LinkWithTooltip
        tooltip={`${ReactHtmlParser(stripHtml)}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {ReactHtmlParser(stripHtml)}
      </LinkWithTooltip>
    );
  }
};

const setCreateDate = (refObj) => (cell) => {
  var date = localDate(cell);
  return dateFormat(date, "dd/mm/yyyy");
};

const setDaysPending = (refObj) => (cell, row) => {
  var date = localDate(cell);
  return dateFormat(date, "dd/mm/yyyy");
};

const clickToShowTasks = (refObj) => (cell, row) => {
  if (row.discussion === 1) {
    /* return (
      <Link
        to={{ pathname: "/user/discussion_details/" + row.task_id }}
        target="_blank"
        style={{ cursor: "pointer" }}
      >
        {cell}
      </Link>
    ); */
    return (
      <LinkWithTooltip
        tooltip={`${cell}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refObj.redirectUrlTask(e, row.task_id)}
      >
        {cell}
      </LinkWithTooltip>
    );
  } else {
    /* return (
      <Link
        to={{ pathname: `/user/task_details/${row.task_id}/${row.assignment_id}` }}
        target="_blank"
        style={{ cursor: "pointer" }}
      >
        {cell}
      </Link>
    ); */
    return (
      <LinkWithTooltip
        tooltip={`${cell}`}
        href="#"
        id="tooltip-1"
        clicked={(e) =>
          refObj.redirectUrlTask(e, row.task_id, row.assignment_id)
        }
      >
        {cell}
      </LinkWithTooltip>
    );
  }
};

const priorityEditor = (refObj) => (onUpdate, props) => {
  ///console.log(props.row.priority);
  //refObj.setState({ updt_priority:props.row.priority,updt_priority_value:props.row.priority_value });
  return (
    <>
      <select
        value={
          refObj.state.updt_priority > 0
            ? refObj.state.updt_priority
            : props.row.priority
        }
        onChange={(ev) => {
          refObj.setState({ updt_priority: ev.currentTarget.value });
        }}
      >
        {priority_arr.map((priority) => (
          <option key={priority.priority_id} value={priority.priority_id}>
            {priority.priority_value}
          </option>
        ))}
      </select>
      <button
        className="btn btn-info btn-xs textarea-save-btn"
        onClick={() => {
          //AXIOS CALL TO UPDATE THE PRIORITY

          props.row.priority = refObj.state.updt_priority;
          onUpdate(refObj.state.updt_priority);
        }}
      >
        save
      </button>
    </>
  );
};

const setProductName = (refOBj) => (cell, row) => {
  if (cell === null) {
    return "";
  } else {

    if(row.order_verified_status == 0 && row.request_type == 23){
      return (

        <LinkWithTooltip
          tooltip={`${row.unverified_products}`}
          href="#"
          id="tooltip-1"
          clicked={(e) => refOBj.checkHandler(e)}
        >
          {row.unverified_products}
        </LinkWithTooltip>
      );
    }else{
      return (

        <LinkWithTooltip
          tooltip={`${cell}`}
          href="#"
          id="tooltip-1"
          clicked={(e) => refOBj.checkHandler(e)}
        >
          {cell}
        </LinkWithTooltip>
      );
    }
  }
};

const setCustomerName = (refObj) => (cell, row) => {
  if (row.customer_id > 0) {
    //hard coded customer id - SATYAJIT
    //row.customer_id = 2;
    return (
      /*<Link
        to={{
          pathname:
            portal_url + "customer-dashboard/" +
            row.customer_drupal_id
        }}
        target="_blank"
        style={{ cursor: "pointer" }}
      >*/
      <LinkWithTooltip
        tooltip={`${row.first_name + " " + row.last_name}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refObj.redirectUrl(e, row.customer_id)}
      >
        {row.first_name + " " + row.last_name}
      </LinkWithTooltip>
      /*</Link>*/
    );
  } else {
    return "";
  }
};

const setPriorityName = (refObj) => (cell, row) => {
  var ret = "Set Priority";
  for (let index = 0; index < priority_arr.length; index++) {
    const element = priority_arr[index];
    if (element["priority_id"] === cell) {
      ret = element["priority_value"];
    }
  }

  return ret;
};

const setAssignedTo = (refOBj) => (cell, row) => {
  return (
    <LinkWithTooltip
      tooltip={`${
        row.at_emp_first_name +
        " " +
        row.at_emp_last_name +
        " (" +
        row.at_emp_desig_name +
        ")"
      }`}
      href="#"
      id="tooltip-1"
      clicked={(e) => refOBj.checkHandler(e)}
    >
      {row.at_emp_first_name +
        " " +
        row.at_emp_last_name +
        " (" +
        row.at_emp_desig_name +
        ")"}
    </LinkWithTooltip>
  );
};

const setAssignedBy = (refOBj) => (cell, row) => {
  if (row.assigned_by == "-1") {
    return (
      <LinkWithTooltip
        tooltip={`System`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        System
      </LinkWithTooltip>
    );
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${
          row.ab_emp_first_name +
          " " +
          row.ab_emp_last_name +
          " (" +
          row.ab_emp_desig_name +
          ")"
        }`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {row.ab_emp_first_name +
          " " +
          row.ab_emp_last_name +
          " (" +
          row.ab_emp_desig_name +
          ")"}
      </LinkWithTooltip>
    );
  }
};

const hoverDate = (refOBj) => (cell, row) => {
  return (
    <LinkWithTooltip
      tooltip={cell}
      href="#"
      id="tooltip-1"
      clicked={(e) => refOBj.checkHandler(e)}
    >
      {cell}
    </LinkWithTooltip>
  );
};

const setCompanyName = (refObj) => (cell, row) => {
  return htmlDecode(cell);
};

let time_interval = null;
// const interval_time = 300000; // Milliseconds i.e 5min ( 5 x 60 x 1000 ) 
// const interval_time = 120000; // Milliseconds i.e 2min ( 2 x 60 x 1000 ) 
// const interval_time = 60000; // Milliseconds i.e 1min ( 1 x 60 x 1000 ) 
// const interval_time = 30000; // Milliseconds i.e 30 sec ( 30 x 1000 ) 
// const interval_time = 2700000; // Milliseconds i.e 45min ( 45 x 60 x 1000 ) 
const interval_time = 900000; // Milliseconds i.e 15min ( 15 x 60 x 1000 ) 

class MyTasksTable extends Component {
  state = {
    showCreateSubTask: false,
    showIPDO: false,
    showCloneCreateSubTask: false,
    fromPopup:0,

    showPoke: false,
    showPokeSPOC: false,
    showClosedComment: false,

    showIncreaseSla: false,
    showIncreaseSlaSubTask: false,
    showCreateTasks: false,
    showReclassifyTasks:false,
    showAssign: false,
    showRespondBack: false,
    showProformaForm: false,
    showTaskDetails: false,
    showRespondCustomer: false,
    showAuthorizeBack: false,
    changeData: true,
    task_id: 0,
    tableData: [],

    activePage: 1,
    totalCount: 0,
    itemPerPage: 20,
    showModalLoader: false,
    showDeleteTaskPopup: false,
    reloadOnClose:false
  };

  checkHandler = (event) => {
    event.preventDefault();
  };

  redirectUrl = (event, id) => {
    event.preventDefault();
    //http://reddy.indusnet.cloud/customer-dashboard?source=Mi02NTE=
    API.post(`/api/employees/shlogin`, { customer_id: parseInt(id) })
      .then((res) => {
        if (res.data.shadowToken) {
          var url =
            process.env.REACT_APP_CUSTOMER_PORTAL +
            `setToken/` +
            res.data.shadowToken;
          window.open(url, "_blank");
        }
      })
      .catch((error) => {
        showErrorMessageFront(error, 0, this.props);
      });
  };

  redirectUrlTask = (event, task_id, assignment_id = "") => {
    event && event.preventDefault();
    if (assignment_id === "") {
      window.open(`/user/discussion_details/${task_id}`, "_blank");
    } else {
      window.open(`/user/task_details/${task_id}/${assignment_id}`, "_blank");
    }
  };

  taskDetails = (row) => {
    this.setState({ showTaskDetails: true, currRow: row });
  };

  showSubTaskPopup = (currRow) => {
    //console.log("Create Sub Task");
    if(currRow.order_verified_status == 0 && currRow.request_type == 23){
      swal({
        closeOnClickOutside: false,
        title: "Warning !!",
        text: "Please process the order first.",
        icon: "warning",
      }).then(() => {
        this.redirectUrlTask(null, currRow.task_id, currRow.assignment_id);
      });
    }else if (currRow.request_related_to === null && ipdoTypes.includes(currRow.request_type)) {
      this.setState({ showIPDO: true, currRow: currRow, fromPopup:1 });
    }else if(currRow.po_no === null && currRow.request_type == 23){
      swal({
        closeOnClickOutside: false,
        title: "Warning !!",
        text: "Please set Purchase Order Number first.",
        icon: "warning",
      }).then(() => {
        this.redirectUrlTask(null, currRow.task_id, currRow.assignment_id);
      });
    }else {
      this.setState({ showCreateSubTask: true, currRow: currRow });
    }
  };

  showCloneSubTaskPopup = (currRow) => {
    //console.log("Create Sub Task");

    if(currRow.order_verified_status == 0 && currRow.request_type == 23){
      swal({
        closeOnClickOutside: false,
        title: "Warning !!",
        text: "Please process the order first.",
        icon: "warning",
      }).then(() => {
        this.redirectUrlTask(null, currRow.task_id, currRow.assignment_id);
      });
    }else if (currRow.request_related_to === null && ipdoTypes.includes(currRow.request_type)) {
      this.setState({ showIPDO: true, currRow: currRow, fromPopup:2 });
    }else if(currRow.po_no === null && currRow.request_type == 23){
      swal({
        closeOnClickOutside: false,
        title: "Warning !!",
        text: "Please set Purchase Order Number first.",
        icon: "warning",
      }).then(() => {
        this.redirectUrlTask(null, currRow.task_id, currRow.assignment_id);
      });
    }else {
      this.setState({ showCloneCreateSubTask: true, currRow: currRow });
    }
  };

  showIncreaseSlaPopup = (currRow) => {
    if (currRow.parent_id > 0) {
      this.setState({ showIncreaseSlaSubTask: true, currRow: currRow });
    } else {
      this.setState({ showIncreaseSla: true, currRow: currRow });
    }
  };

  showDeleteTaskPopup = (currRow) => {
    this.setState({ showDeleteTaskPopup: true, currRow: currRow });
  };

  showDeleteTaskPopupOld = (currRow) => {
    swal({
      closeOnClickOutside: false,
      title: "Cancel Request",
      text: "Are you sure you want to cancel this request?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.setState({ showModalLoader: true });
        API.delete(`/api/tasks/check_delete/${currRow.task_id}`)
          .then((res) => {
            if (res.data.has_sub_task === 1) {
              this.setState({ showModalLoader: false });
              swal({
                closeOnClickOutside: false,
                title: "Alert",
                text:
                  "This task has some open sub-tasks. \r\n Do you want to delete them as well?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              }).then((willDelete) => {
                if (willDelete) {
                  this.setState({ showModalLoader: true });
                  API.delete(`/api/tasks/delete/${currRow.task_id}`)
                    .then((res) => {
                      this.setState({ showModalLoader: false });
                      swal({
                        closeOnClickOutside: false,
                        title: "Success",
                        text: "Task has been canceled!",
                        icon: "success",
                      }).then(() => {
                        this.props.allTaskDetails();
                      });
                    })
                    .catch((err) => {
                      var token_rm = 2;
                      showErrorMessageFront(err, token_rm, this.props);
                    });
                }
              });
            } else {
              swal({
                closeOnClickOutside: false,
                title: "Success",
                text: "Task has been canceled!",
                icon: "success",
              }).then(() => {
                this.props.allTaskDetails();
              });
            }
          })
          .catch((err) => {
            var token_rm = 2;
            showErrorMessageFront(err, token_rm, this.props);
          });
      }
    });
  };

  showApproveTaskPopup = (currRow) => {
    swal({
      closeOnClickOutside: false,
      title: "Request Confirmation",
      text: "Are you sure you have reviewed the task ?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.setState({ showModalLoader: true });
        API.post(
          `/api/tasks/approve_translated_task/${this.state.for_review_task}`
        )
          .then((res) => {
            swal({
              closeOnClickOutside: false,
              title: "Success",
              text: "Task content has been reviewed!",
              icon: "success",
            }).then(() => {
              this.reloadTaskDetails();
            });
          })
          .catch((err) => {
            var token_rm = 2;
            showErrorMessageFront(err, token_rm, this.props);
          });
      }
    });
  };

  showCloseTaskPopup = (currRow) => {

    if(currRow.new_order_type === null && currRow.request_type == 23){
      swal({
        closeOnClickOutside: false,
        title: "Warning !!",
        text: "Please set Order Type first.",
        icon: "warning",
      }).then(() => {
        this.redirectUrlTask(null, currRow.task_id, currRow.assignment_id);
      });
    }else if (currRow.request_related_to === null && ipdoTypes.includes(currRow.request_type)) {
      this.setState({ showIPDO: true, currRow: currRow, fromPopup:3 });
    }else{
      var task_date_added = localDate(currRow.task_date_added);

      var dueDate = localDateTime(currRow.original_due_date);
      var today = localDateTime(currRow.today_date);
      var timeDiff = dueDate.getTime() - today.getTime();

      let current_date  = new Date(currRow.today_date);
      let add_date_24 = new Date(new Date(currRow.task_date_added).getTime() + 60 * 60 * 24 * 1000);
      
      if(current_date < add_date_24 || timeDiff < 0){
        //alert('change Here');
        this.setState({ showClosedComment: true, currRow: currRow });
      }else{
        swal({
          closeOnClickOutside: false,
          title: "Close task",
          text: "Are you sure you want to close this task?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        }).then((willDelete) => {
          if (willDelete) {
            this.setState({ showModalLoader: true });
            API.delete(`/api/tasks/check_close/${currRow.task_id}`)
              .then((res) => {
                if (res.data.has_sub_task === 1) {
                  this.setState({ showModalLoader: false });
                  swal({
                    closeOnClickOutside: false,
                    title: "Alert",
                    text:
                      "This task has some open sub-tasks. \r\n Do you want to close them as well?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                  }).then((willDelete) => {
                    if (willDelete) {
                      this.setState({ showModalLoader: true });
                      API.delete(`/api/tasks/close/${currRow.task_id}`)
                        .then((res) => {
                          this.setState({ showModalLoader: false });
                          swal({
                            closeOnClickOutside: false,
                            title: "Success",
                            text: "Task has been closed!",
                            icon: "success",
                          }).then(() => {
                            this.props.allTaskDetails();
                          });
                        })
                        .catch((err) => {
                          var token_rm = 2;
                          showErrorMessageFront(err, token_rm, this.props);
                        });
                    }
                  });
                } else {
                  swal({
                    closeOnClickOutside: false,
                    title: "Success",
                    text: "Task has been closed!",
                    icon: "success",
                  }).then(() => {
                    this.props.allTaskDetails();
                  });
                }
              })
              .catch((err) => {
                var token_rm = 2;
                showErrorMessageFront(err, token_rm, this.props);
              });
          }
        });
      }
    }
  };

  showAuthorizeTaskPopup = (currRow) => {
    //console.log("Authorize");
    this.setState({ showAuthorizeBack: true, currRow: currRow });
  };

  showAssignPopup = (currRow) => {
    //console.log("Assign Task");
    this.setState({ showAssign: true, currRow: currRow });
  };

  showCloneMainTaskPopup = (currRow) => {
    //console.log("Assign Task");
    if(currRow.order_verified_status == 0 && currRow.request_type == 23){
      swal({
        closeOnClickOutside: false,
        title: "Warning !!",
        text: "Please process the order first.",
        icon: "warning",
      }).then(() => {
        this.redirectUrlTask(null, currRow.task_id, currRow.assignment_id);
      });
    }else{
      this.setState({ showCreateTasks: true, currRow: currRow });
    }
  };

  reclassify = (currRow) => {
    //console.log("Assign Task");

    if(currRow.order_verified_status == 0 && currRow.request_type == 23){
      swal({
        closeOnClickOutside: false,
        title: "Warning !!",
        text: "Please process the order first.",
        icon: "warning",
      }).then(() => {
        this.redirectUrlTask(null, currRow.task_id, currRow.assignment_id);
      });
    }else{
      this.setState({ showReclassifyTasks: true, currRow: currRow }); 
    }
  };

  showRespondPopup = (currRow) => {
    //console.log("Respond");
    this.setState({ showRespondBack: true, currRow: currRow });
  };

  showProforma = (currRow) => {
    this.setState({ showProformaForm: true, currRow: currRow });
  };

  showPoke = (currRow) => {
    this.setState({ showPoke: true, currRow: currRow });
  };

  showPokeSPOC = (currRow) => {
    this.setState({ showPokeSPOC: true, currRow: currRow });
  };

  review_task = (currRow) => {
    swal({
      closeOnClickOutside: false,
      title: "Request review",
      text: "Are you sure you want this request to be reviewed ?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.setState({ showModalLoader: true });
        API.post(`/api/tasks/spoc_approval_request/${currRow.task_id}/${currRow.assignment_id}`)
          .then((res) => {
            swal({
              closeOnClickOutside: false,
              title: "Success",
              text: "Notification sent to Regional Spoc to review Request",
              icon: "success",
            }).then(() => {
              this.props.allTaskDetails();
            });
          })
          .catch((err) => {
            var token_rm = 2;
            showErrorMessageFront(err, token_rm, this.props);
          });
      }
    });
  }

  showRespondCustomerPopup = (currRow) => {
    //console.log("Respond Customer");

    // if(currRow.order_type && 23){
    //   alert()
    // }else{

    // }
    if(currRow.new_order_type === null && currRow.request_type == 23){
      swal({
        closeOnClickOutside: false,
        title: "Warning !!",
        text: "Please set Order Type first.",
        icon: "warning",
      }).then(() => {
        this.redirectUrlTask(null, currRow.task_id, currRow.assignment_id);
      });
    }else if (currRow.request_related_to === null && ipdoTypes.includes(currRow.request_type)) {
      this.setState({ showIPDO: true, currRow: currRow, fromPopup:4 });
    }else{
      API.get(`/api/tasks/get_respond_customer/${currRow.task_id}`).then(
        (res) => {
          let custResp;
          if (res.data.data.rs_id > 0) {
            custResp = {
              original_comment: htmlDecode(res.data.data.original_comment),
              delivery_date: localDate(res.data.data.expected_closure_date),
              status: res.data.data.status,
              status_id: res.data.data.status_id,
              action_req: res.data.data.required_action,
              action_id: res.data.data.action_id,
              po_number: res.data.data.po_number,
              pause_sla: res.data.data.pause_sla,
              translated_comment:htmlDecode(res.data.data.translated_comment),
              original_language: res.data.data.original_language
            };
          } else {
            custResp = {
              original_comment: "",
              delivery_date: "",
              status: "",
              status_id: 0,
              action_req: "",
              action_id: 0,
              po_number: "",
              pause_sla: 0,
              translated_comment:"",
              original_language: ""
            };
          }
          this.setState({
            showRespondCustomer: true,
            currRow: currRow,
            custResp: custResp,
          });
        }
      );
    }
  };

  componentDidMount() {
    //console.log('>>>>>',this.props.countMyTasks);
    this.setState({
      tableData: this.props.tableData,
      countMyTasks: this.props.countMyTasks,
      options: {
        clearSearch: true,
        expandBy: "column",
        page: !this.state.start_page ? 1 : this.state.start_page, // which page you want to show as default
        sizePerPageList: [
          {
            text: "10",
            value: 10,
          },
          {
            text: "20",
            value: 20,
          },
          {
            text: "All",
            value: !this.state.tableData ? 1 : this.state.tableData,
          },
        ], // you can change the dropdown list for size per page
        sizePerPage: 10, // which size per page you want to locate as default
        pageStartIndex: 1, // where to start counting the pages
        paginationSize: 3, // the pagination bar size.
        prePage: "‹", // Previous page button text
        nextPage: "›", // Next page button text
        firstPage: "«", // First page button text
        lastPage: "»", // Last page button text
        //paginationShowsTotal: this.renderShowsTotal,  // Accept bool or function
        paginationPosition: "bottom", // default is bottom, top and both is all available
        // hideSizePerPage: true > You can hide the dropdown for sizePerPage
        // alwaysShowAllBtns: true // Always show next and previous button
        // withFirstAndLast: false > Hide the going to First and Last page button
      },
      cellEditProp: {
        mode: "click",
        beforeSaveCell: this.onBeforeSetPriority, // a hook for before saving cell
        afterSaveCell: this.onAfterSetPriority, // a hook for after saving cell
      },
    });
    
    time_interval = setInterval(()=>{
      this.getMyTasks(1);
    }, interval_time);
  }

  componentWillUnmount () {
    clearInterval(time_interval);
  }

  componentDidUpdate(prevProps) {
    const { 
      showCreateSubTask, 
      showAssign, 
      showAuthorizeBack, 
      showCloneCreateSubTask, 
      showClosedComment, 
      showCreateTasks, 
      showDeleteTaskPopup,
      showIncreaseSla,
      showIncreaseSlaSubTask,
      showModalLoader,
      showPoke,
      showPokeSPOC,
      showProformaForm,
      showReclassifyTasks,
      showRespondBack,
      showRespondCustomer,
      showTaskDetails,
      showIPDO,
    } = this.state;
    const allPopups = [
      showCreateSubTask, 
      showAssign, 
      showAuthorizeBack, 
      showCloneCreateSubTask, 
      showClosedComment, 
      showCreateTasks, 
      showDeleteTaskPopup,
      showIncreaseSla,
      showIncreaseSlaSubTask,
      showModalLoader,
      showPoke,
      showPokeSPOC,
      showProformaForm,
      showReclassifyTasks,
      showRespondBack,
      showRespondCustomer,
      showTaskDetails,
      showIPDO,
    ];

    // console.log("1",prevProps.currentTab !== "tab_1");
    // console.log("2",time_interval != null);
    // console.log("3",allPopups.includes(true));

    if((prevProps.currentTab !== "tab_1" && time_interval != null) || (allPopups.includes(true))){
      clearInterval(time_interval);
      time_interval = null;
    }else{
      if(time_interval === null){
        time_interval = setInterval(()=>{
          this.getMyTasks(1);
        }, interval_time);
      }
    }
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getMyTasks(pageNumber > 0 ? pageNumber : 1);
  };

  getMyTasks(page = 1) {
    let url;
    if(this.props.graphURL && this.props.graphURL != ""){
      url = `${this.props.graphURL}&page=${page}`;
    }else{
      if(this.props.queryString !== ""){
        url = `/api/tasks?page=${page}&${this.props.queryString}`;
      }else{
        url = `/api/tasks?page=${page}`;
      }
    }
    API.get(url)
      .then((res) => {

        if (this.props.graphURL && this.props.graphURL != "") {
          this.setState({
            tableData: res.data.data.open,
            countOpenTasks: res.data.count.count_open,
          });
        } else {

          this.setState({
            tableData: res.data.data,
            countMyTasks: res.data.count_my_tasks,
          });
        }
      })
      .catch((err) => {
        showErrorMessageFront(err, this.props);
      });
  }

  onBeforeSetPriority = (row, cellName, cellValue) => {};

  onAfterSetPriority = (row, cellName, cellValue) => {
    this.setState({ updt_priority: 0 });
  };

  getSubTasks = (row) => {
    return (
      <SubTaskTable
        tableData={row.sub_tasks}
        assignId={row.assignment_id}
        dashType={this.props.dashType}
        reloadTaskSubTask={() => this.props.allTaskDetails()}
      />
    );
  };

  checkSubTasks = (row) => {
    //console.log("subtask")

    if (typeof row.sub_tasks !== "undefined" && row.sub_tasks.length > 0) {
      return true;
    } else {
      return false;
    }
  };

  refreshTableStatus = (currRow) => {
    currRow.status = 2;
  };

  handleClose = (closeObj) => {
    this.setState(closeObj);
  };

  showNextPopup = (nextPopupObj) => {
    if(nextPopupObj.fromPopup == 1){
      this.setState({ showCreateSubTask: true, currRow: nextPopupObj.currRow,reloadOnClose:true });
    }else if(nextPopupObj.fromPopup == 2){
      this.setState({ showCloneCreateSubTask: true, currRow: nextPopupObj.currRow,reloadOnClose:true });
    }else if(nextPopupObj.fromPopup == 3){
      
      var task_date_added = localDate(nextPopupObj.currRow.task_date_added);

      var dueDate = localDateTime(nextPopupObj.currRow.original_due_date);
      var today = localDateTime(nextPopupObj.currRow.today_date);
      var timeDiff = dueDate.getTime() - today.getTime();

      let current_date  = new Date(nextPopupObj.currRow.today_date);
      let add_date_24 = new Date(new Date(nextPopupObj.currRow.task_date_added).getTime() + 60 * 60 * 24 * 1000);
      
      if(current_date < add_date_24 || timeDiff < 0){
        //alert('change Here');
        this.setState({ showClosedComment: true, currRow: nextPopupObj.currRow,reloadOnClose:true });
      }else{
        swal({
          closeOnClickOutside: false,
          title: "Close task",
          text: "Are you sure you want to close this task?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        }).then((willDelete) => {
          if (willDelete) {
            this.setState({ showModalLoader: true });
            API.delete(`/api/tasks/check_close/${nextPopupObj.currRow.task_id}`)
              .then((res) => {
                if (res.data.has_sub_task === 1) {
                  this.setState({ showModalLoader: false });
                  swal({
                    closeOnClickOutside: false,
                    title: "Alert",
                    text:
                      "This task has some open sub-tasks. \r\n Do you want to close them as well?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                  }).then((willDelete) => {
                    if (willDelete) {
                      this.setState({ showModalLoader: true });
                      API.delete(`/api/tasks/close/${nextPopupObj.currRow.task_id}`)
                        .then((res) => {
                          this.setState({ showModalLoader: false });
                          swal({
                            closeOnClickOutside: false,
                            title: "Success",
                            text: "Task has been closed!",
                            icon: "success",
                          }).then(() => {
                            this.props.allTaskDetails();
                          });
                        })
                        .catch((err) => {
                          var token_rm = 2;
                          showErrorMessageFront(err, token_rm, this.props);
                        });
                    }
                  });
                } else {
                  swal({
                    closeOnClickOutside: false,
                    title: "Success",
                    text: "Task has been closed!",
                    icon: "success",
                  }).then(() => {
                    this.props.allTaskDetails();
                  });
                }
              })
              .catch((err) => {
                var token_rm = 2;
                showErrorMessageFront(err, token_rm, this.props);
              });
          }else{
            this.props.allTaskDetails();
          }
        });
      }

    }else if(nextPopupObj.fromPopup == 4){
      API.get(`/api/tasks/get_respond_customer/${nextPopupObj.currRow.task_id}`).then(
        (res) => {
          let custResp;
          if (res.data.data.rs_id > 0) {
            custResp = {
              original_comment: htmlDecode(res.data.data.original_comment),
              delivery_date: localDate(res.data.data.expected_closure_date),
              status: res.data.data.status,
              status_id: res.data.data.status_id,
              action_req: res.data.data.required_action,
              action_id: res.data.data.action_id,
              po_number: res.data.data.po_number,
              pause_sla: res.data.data.pause_sla,
              translated_comment:htmlDecode(res.data.data.translated_comment),
              original_language: res.data.data.original_language
            };
          } else {
            custResp = {
              original_comment: "",
              delivery_date: "",
              status: "",
              status_id: 0,
              action_req: "",
              action_id: 0,
              po_number: "",
              pause_sla: 0,
              translated_comment:"",
              original_language: ""
            };
          }
          this.setState({
            showRespondCustomer: true,
            currRow: nextPopupObj.currRow,
            custResp: custResp,
            reloadOnClose:true
          });
        }
      );
    }

  };

  tdClassName = (fieldValue, row) => {
    var dynamicClass = "width-150 increased-column ";
    if (row.vip_customer === 1) {
      dynamicClass += "bookmarked-column ";
    }
    // if(row.sub_tasks.length > 0 && typeof row.sub_tasks !== 'undefined' && typeof row.bookmarked !== 'undefined' ){
    //     return 'sub-task-column bookmarked-column';
    // }else if( typeof row.bookmarked !== 'undefined' ){
    //     return 'bookmarked-column';
    // }else if(row.sub_tasks.length > 0 && typeof row.sub_tasks !== 'undefined'){
    //     return 'sub-task-column';
    // }else{
    //     return '';
    // }
    return dynamicClass;
  };

  trClassName = (row, rowIndex) => {
    var ret = " ";
    var selDueDate = row.new_due_date;
    var dueDate = localDateTime(selDueDate);
    var today = localDateTime(row.today_date);
    var timeDiff = dueDate.getTime() - today.getTime();
    var addDate = localDateTime(row.date_added);

    if (timeDiff > 0) {
      var parseEighty =
        ((dueDate.getTime() - addDate.getTime()) * 80) / 100 +
        addDate.getTime();
      if (today.getTime() > parseEighty) {
        ret += " tr-yellow ";
      }
    } else {
      ret += "tr-red";
      /* if (row.vip_customer === 1) {
        ret += "tr-red";
      } */
    }

    if (row.status === 1) {
      ret += " tr-pink ";
    }

    // if(row.order_verified_status == 0 && row.request_type == 23){
    //   ret += " tr-blue ";
    // }

    // if(row.rdd_pending && row.request_type == 23){
    //   ret += " tr-yellow ";
    // }

    return ret;
  };

  expandColumnComponent({ isExpandableRow, isExpanded }) {
    let content = "";

    if (isExpandableRow) {
      content = isExpanded ? "-" : "+";
    } else {
      content = " ";
    }
    return <div> {content} </div>;
  }



  render() {
    /* const paginationOptions = {
      page: 1, // which page you want to show as default
      sizePerPageList: [
        {
          text: "10",
          value: 10
        },
        {
          text: "20",
          value: 20
        },
        {
          text: "All",
          value: this.props.tableData.length > 0 ? this.props.tableData.length : 1
        }
      ], // you can change the dropdown list for size per page
      sizePerPage: 10, // which size per page you want to locate as default
      pageStartIndex: 1, // where to start counting the pages
      paginationSize: 3, // the pagination bar size.
      prePage: '‹', // Previous page button text
      nextPage: '›', // Next page button text
      firstPage: '«', // First page button text
      lastPage: '»', // Last page button text
      //paginationShowsTotal: this.renderShowsTotal, // Accept bool or function
      paginationPosition: "bottom" // default is bottom, top and both is all available
      // hideSizePerPage: true //> You can hide the dropdown for sizePerPage
      // alwaysShowAllBtns: true // Always show next and previous button
      // withFirstAndLast: false //> Hide the going to First and Last page button
    }; */
    return (
      <>
        {this.state.showModalLoader === true ? (
          <div className="loderOuter">
            <div className="loader">
              <img src={loaderlogo} alt="logo" />
              <div className="loading">Loading...</div>
            </div>
          </div>
        ) : (
          ""
        )}

        {this.state.changeData && (
          <BootstrapTable
            data={this.state.tableData}
            /* options={paginationOptions}
            pagination */
            expandableRow={this.checkSubTasks}
            expandComponent={this.getSubTasks}
            expandColumnOptions={{
              expandColumnVisible: true,
              expandColumnComponent: this.expandColumnComponent,
              columnWidth: 25,
            }}
            trClassName={this.trClassName}
            cellEdit={this.state.cellEditProp}
          >
            <TableHeaderColumn
              dataField="task_ref"
              //dataSort={true}
              columnClassName={this.tdClassName}
              editable={false}
              expandable={false}
              dataFormat={clickToShowTasks(this)}
            >
              <LinkWithTooltip
                tooltip={`Tasks `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Tasks{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="request_type"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={setDescription(this)}
            >
              <LinkWithTooltip
                tooltip={`Description `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Description{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="product_name"
              editable={false}
              expandable={false}
              dataFormat={setProductName(this)}
            >
              <LinkWithTooltip
                tooltip={`Product Name `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Product Name{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="display_assign_date"
              editable={false}
              expandable={false}
              dataFormat={hoverDate(this)}
            >
              <LinkWithTooltip
                tooltip={`Assigned Date `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Assigned Date{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="display_date_added"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={hoverDate(this)}
            >
              <LinkWithTooltip
                tooltip={`Created `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Created{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            {/* <TableHeaderColumn dataField='rdd' dataSort={ true } dataFormat={ setCreateDate(this) } editable={ false } expandable={ false }>RDD</TableHeaderColumn> */}

            <TableHeaderColumn
              dataField="display_new_due_date"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={hoverDate(this)}
            >
              <LinkWithTooltip
                tooltip={`Due Date `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Due Date{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            {/* <TableHeaderColumn dataField='assigned_to' dataSort={ true } editable={ false } expandable={ false } >Assigned To</TableHeaderColumn> */}

            <TableHeaderColumn
              dataField="dept_name"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={setAssignedBy(this)}
            >
              <LinkWithTooltip
                tooltip={`Assigned By `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Assigned By{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="dept_name"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={setAssignedTo(this)}
            >
              <LinkWithTooltip
                tooltip={`Assigned To `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Assigned To{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="company_name"
              editable={false}
              expandable={false}
              dataFormat={setCompanyName(this)}
            >
              <LinkWithTooltip
                tooltip={`Customer `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Customer{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="cust_name"
              //dataSort={true}
              editable={false}
              expandable={false}
              dataFormat={setCustomerName(this)}
            >
              <LinkWithTooltip
                tooltip={`User `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                User{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="status"
              dataFormat={getStatusColumn(this)}
              expandable={false}
              editable={false}
            >
              <LinkWithTooltip
                tooltip={`Status `}
                href="#"
                id="tooltip-1"
                clicked={(e) => this.checkHandler(e)}
              >
                Status{" "}
              </LinkWithTooltip>
            </TableHeaderColumn>

            <TableHeaderColumn
              isKey
              dataField="task_id"
              dataFormat={getActions(this)}
              expandable={false}
              editable={false}
            />
          </BootstrapTable>
        )}

        {/*DONE*/}
        <AssignPopup
          showAssign={this.state.showAssign}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.allTaskDetails()}
        />

        {/*DONE*/}
        <CreateSubTaskPopup
          showCreateSubTask={this.state.showCreateSubTask}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.allTaskDetails()}
          reloadOnClose={this.state.reloadOnClose}
        />

        <CreateSubTaskPopup
          showCreateSubTask={this.state.showCloneCreateSubTask}
          currRow={this.state.currRow}
          clone_main={1}
          handleClose={this.handleClose}
          reloadTask={() => this.props.allTaskDetails()}
          reloadOnClose={this.state.reloadOnClose}
        />

        {/*DONE*/}
        <RespondCustomerPopup
          showRespondCustomer={this.state.showRespondCustomer}
          currRow={this.state.currRow}
          custResp={this.state.custResp}
          handleClose={this.handleClose}
          reloadTask={() => this.props.allTaskDetails()}
          reloadOnClose={this.state.reloadOnClose}
        />

        {/*DONE*/}
        <RespondBackPopup
          showRespondBack={this.state.showRespondBack}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.allTaskDetails()}
        />

        <ProformaPopup
          showProformaForm={this.state.showProformaForm}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.allTaskDetails()}
          showSubTaskPopup={this.showSubTaskPopup}
          showRespondCustomer={this.showRespondCustomerPopup}
        />

        <Poke
          showPoke={this.state.showPoke}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.allTaskDetails()}
        />

        <PokeSPOC
          showPokeSPOC={this.state.showPokeSPOC}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.allTaskDetails()}
        />

        <DeleteTaskPopup
          showDeleteTaskPopup={this.state.showDeleteTaskPopup}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.allTaskDetails()}
        />

        <IPDOPopup
          showIPDO={this.state.showIPDO}
          fromPopup={this.state.fromPopup}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          showNextPopup={this.showNextPopup}
          reloadTask={() => this.props.allTaskDetails()}
        />

        {/*DONE*/}
        <AuthorizePopup
          showAuthorizeBack={this.state.showAuthorizeBack}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.allTaskDetails()}
        />

        <IncreaseSlaPopup
          showIncreaseSla={this.state.showIncreaseSla}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.allTaskDetails()}
        />

        <IncreaseSlaSubTaskPopup
          showIncreaseSla={this.state.showIncreaseSlaSubTask}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.allTaskDetails()}
        />

        <CopyCreateMyTasksPopup
          showCreateTasks={this.state.showCreateTasks}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          allTaskDetails={(e) => this.props.allTaskDetails()}
        />

        <ReclassifyMyTasksPopup
          showReclassifyTasks={this.state.showReclassifyTasks}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          allTaskDetails={(e) => this.props.allTaskDetails()}
        />

        <ClosedCommentMyTask
            showClosedComment={this.state.showClosedComment}
            currRow={this.state.currRow}
            handleClose={this.handleClose}
            reloadTask={() => this.props.allTaskDetails()}
            reloadOnClose={this.state.reloadOnClose}
        />

        {this.state.countMyTasks > 20 ? (
          <Row>
            <Col md={12}>
              <div className="paginationOuter text-right">
                <Pagination
                  activePage={this.state.activePage}
                  itemsCountPerPage={this.state.itemPerPage}
                  totalItemsCount={this.state.countMyTasks}
                  itemClass="nav-item"
                  linkClass="nav-link"
                  activeClass="active"
                  hideNavigation="false"
                  onChange={this.handlePageChange}
                />
              </div>
            </Col>
          </Row>
        ) : null}

        {/*DONE*/}
        {/* <TaskDetails showTaskDetails={this.state.showTaskDetails} currRow={this.state.currRow} handleClose={this.handleClose} refreshTableStatus={this.refreshTableStatus} /> */}
      </>
    );
  }
}

export default MyTasksTable;
