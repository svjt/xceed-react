import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { getMyDrupalId, localDate, htmlDecode } from "../../shared/helper";
import base64 from "base-64";
import BMVerticalMenu from "../bmoverview/BMVerticalMenu";
import CSCVerticalMenu from "../cscoverview/CSCVerticalMenu";
import RAVerticalMenu from "../raoverview/RAVerticalMenu";
import OTHRVerticalMenu from "../othroverview/OTHRVerticalMenu";

import CreateSubTaskPopup from "./CreateSubTaskPopup";
import AssignPopup from "./AssignPopup";
import RespondBackPopup from "./RespondBackPopup";
import ReAssignPopup from "./ReAssignPopup";
import CQTReAssignPopup from "./CQTReAssignPopup";
import Poke from "./Poke";

import RequestCaseChanges from './RequestCaseChanges';
//import RequestCaseChangesAfterSO from './RequestCaseChangesAfterSO';



import IncreaseSlaSubTaskPopup from "./IncreaseSlaSubTaskPopup";

import StatusColumn from "./StatusColumn";
import { Link } from "react-router-dom";

import API from "../../shared/axios";
import swal from "sweetalert";
//import { getMyId } from '../../shared/helper';
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";

import { Tooltip, OverlayTrigger } from "react-bootstrap";
import "./Dashboard.css";

import { showErrorMessageFront } from "../../shared/handle_error_front";

import dateFormat from "dateformat";

import ReactHtmlParser from "react-html-parser";

const portal_url = `${process.env.REACT_APP_PORTAL_URL}`; // SATYAJIT

const priority_arr = [
  { priority_id: 1, priority_value: "Low" },
  { priority_id: 2, priority_value: "Medium" },
  { priority_id: 3, priority_value: "High" },
];

const getActions = (refObj) => (cell, row) => {
  if (row.discussion !== 1) {
    if (refObj.props.dashType === "BM") {
      return (
        <BMVerticalMenu
          id={cell}
          currRow={row}
          showAssignPopup={refObj.showAssignPopup}
          showReAssignPopup={refObj.showReAssignPopup}
          showReAssignCQTPopup={refObj.showReAssignCQTPopup}
          showSubTaskPopup={refObj.showSubTaskPopup}
          showRespondCustomerPopup={refObj.showRespondCustomerPopup}
          showCloseTaskPopup={refObj.showCloseTaskPopup}
          showIncreaseSlaPopup={refObj.showIncreaseSlaPopup}
          showRespondPopup={refObj.showRespondPopup}
          requestCaseChanges={refObj.requestCaseChangesNew}
          requestCaseChangesAfterSO={refObj.requestCaseChangesAfterSONew}
          poke={refObj.showPoke}
          clone={refObj.showClone}
        />
      );
    } else if (refObj.props.dashType === "CSC") {
      return (
        <CSCVerticalMenu
          id={cell}
          currRow={row}
          showAssignPopup={refObj.showAssignPopup}
          showReAssignPopup={refObj.showReAssignPopup}
          showReAssignCQTPopup={refObj.showReAssignCQTPopup}
          showSubTaskPopup={refObj.showSubTaskPopup}
          showRespondCustomerPopup={refObj.showRespondCustomerPopup}
          showCloseTaskPopup={refObj.showCloseTaskPopup}
          showIncreaseSlaPopup={refObj.showIncreaseSlaPopup}
          showRespondPopup={refObj.showRespondPopup}
          requestCaseChanges={refObj.requestCaseChangesNew}
          requestCaseChangesAfterSO={refObj.requestCaseChangesAfterSONew}
          poke={refObj.showPoke}
          clone={refObj.showClone}
        />
      );
    } else if (refObj.props.dashType === "RA") {
      return (
        <RAVerticalMenu
          id={cell}
          currRow={row}
          showAssignPopup={refObj.showAssignPopup}
          showReAssignPopup={refObj.showReAssignPopup}
          showReAssignCQTPopup={refObj.showReAssignCQTPopup}
          showSubTaskPopup={refObj.showSubTaskPopup}
          showRespondCustomerPopup={refObj.showRespondCustomerPopup}
          showCloseTaskPopup={refObj.showCloseTaskPopup}
          showIncreaseSlaPopup={refObj.showIncreaseSlaPopup}
          showRespondPopup={refObj.showRespondPopup}
          poke={refObj.showPoke}
          clone={refObj.showClone}
        />
      );
    } else if (refObj.props.dashType === "OTHR") {
      return (
        <OTHRVerticalMenu
          id={cell}
          currRow={row}
          showReAssignCQTPopup={refObj.showReAssignCQTPopup}
          showAssignPopup={refObj.showAssignPopup}
          showReAssignPopup={refObj.showReAssignPopup}
          showSubTaskPopup={refObj.showSubTaskPopup}
          showRespondCustomerPopup={refObj.showRespondCustomerPopup}
          showCloseTaskPopup={refObj.showCloseTaskPopup}
          showIncreaseSlaPopup={refObj.showIncreaseSlaPopup}
          showRespondPopup={refObj.showRespondPopup}
          poke={refObj.showPoke}
          clone={refObj.showClone}
        />
      );
    }
  } else {
    return "";
  }
};

const getStatusColumn = (refObj) => (cell, row) => {
  if (row.discussion !== 1) {
    return <StatusColumn rowData={row} />;
  } else {
    return "";
  }
};

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const setDescription = (refOBj) => (cell, row) => {
  //console.log('====',row);
  if (row.parent_id > 0) {
    let title = htmlDecode(row.title);
    let stripHtml = title.replace(/<[^>]+>/g, "");
    return (
      <LinkWithTooltip
        tooltip={`${ReactHtmlParser(stripHtml)}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {ReactHtmlParser(stripHtml)}
      </LinkWithTooltip>
    );
  } else if (row.discussion === 1) {
    let comment = htmlDecode(row.comment);
    let stripHtml = comment.replace(/<[^>]+>/g, "");
    return (
      <LinkWithTooltip
        tooltip={`${ReactHtmlParser(stripHtml)}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {ReactHtmlParser(stripHtml)}
      </LinkWithTooltip>
    );
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${ReactHtmlParser(row.req_name)}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {ReactHtmlParser(row.req_name)}
      </LinkWithTooltip>
    );
  }
};

const setCreateDate = (refObj) => (cell) => {
  var date = localDate(cell);
  return dateFormat(date, "dd/mm/yyyy");
};

const setBlank = (refObj) => (cell, row) => {
  return "";
};

const setDaysPending = (refOBj) => (cell, row) => {
  if (row.discussion === 1) {
    return "";
  } else {
    return (
      <LinkWithTooltip
        tooltip={cell}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {cell}
      </LinkWithTooltip>
    );
  }
};

const clickToShowTasks = (refObj) => (cell, row) => {
  if (row.discussion === 1) {
    //return <Link to={{ pathname: '/user/discussion_details/'+row.task_id }} target="_blank" style={{cursor:'pointer'}}>{cell}</Link>
    //return <Link to={{ pathname: `/user/task_details/${row.task_id}/${refObj.props.assignId}` }} target="_blank" style={{cursor:'pointer'}}>{cell}</Link>
    return (
      <LinkWithTooltip
        tooltip={`${cell}`}
        href="#"
        id="tooltip-1"
        clicked={(e) =>
          refObj.redirectUrlTask(e, row.task_id, refObj.props.assignId)
        }
      >
        {cell}
      </LinkWithTooltip>
    );
  } else {
    //return <Link to={{ pathname: `/user/task_details/${row.task_id}/${row.assignment_id}` }} target="_blank" style={{cursor:'pointer'}}>{cell}</Link>
    return (
      <LinkWithTooltip
        tooltip={`${cell}`}
        href="#"
        id="tooltip-1"
        clicked={(e) =>
          refObj.redirectUrlTask(e, row.task_id, "", row.assignment_id)
        }
      >
        {cell}
      </LinkWithTooltip>
    );
  }
};

const setEmpCustName = (refObj) => (cell, row) => {
  if (row.customer_id > 0) {
    //hard coded customer id - SATYAJIT
    //row.customer_id = 2;
    return (
      /*<Link
        to={{
          pathname:
            portal_url + "customer-dashboard/" +
            row.customer_drupal_id
        }}
        target="_blank"
        style={{ cursor: "pointer" }}
      >*/
      <LinkWithTooltip
        tooltip={`${row.first_name + " " + row.last_name}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refObj.redirectUrl(e, row.customer_drupal_id)}
      >
        {row.first_name + " " + row.last_name}
      </LinkWithTooltip>
      /*</Link>*/
    );
  } else {
    return "";
  }
};

/*const setEmpCustName = refObj => (cell,row) =>{
    if(row.customer_drupal_id > 0){
        //hard coded customer id - SATYAJIT
        //row.customer_id = 2;

        return <Link to={{ pathname: '/user/customer-dashboard/'+row.customer_drupal_id }} target="_blank" style={{cursor:'pointer'}}>{row.first_name+' '+row.last_name}</Link>
    }else{
        var my_id = getMyId();
        if(row.assigned_to === my_id){
            return '';
        }else{
            return row.emp_first_name+' '+row.emp_last_name+' ('+row.dept_name+')';
        }
    }
}*/

const setPriorityName = (refObj) => (cell, row) => {
  if (row.discussion === 1) {
    return "";
  } else {
    var ret = "Set Priority";
    for (let index = 0; index < priority_arr.length; index++) {
      const element = priority_arr[index];
      if (element["priority_id"] === cell) {
        ret = element["priority_value"];
      }
    }

    return ret;
  }
};

const setAssignedTo = (refOBj) => (cell, row) => {
  //return row.emp_first_name+' '+row.emp_last_name+' ('+row.desig_name+')';
  if (row.discussion === 1) {
    return "";
  } else {
    if (row.cqt > 0) {
      return "CQT";
    } else {
      return (
        <LinkWithTooltip
          tooltip={`${
            row.at_emp_first_name +
            " " +
            row.at_emp_last_name +
            " (" +
            row.at_emp_desig_name +
            ")"
          }`}
          href="#"
          id="tooltip-1"
          clicked={(e) => refOBj.checkHandler(e)}
        >
          {row.at_emp_first_name +
            " " +
            row.at_emp_last_name +
            " (" +
            row.at_emp_desig_name +
            ")"}
        </LinkWithTooltip>
      );
    }
  }
};

const setAssignedBy = (refOBj) => (cell, row) => {
  if (row.discussion === 1) {
    return "";
  } else {
    if (row.assigned_by === -1) {
      return (
        <LinkWithTooltip
          tooltip={`System`}
          href="#"
          id="tooltip-1"
          clicked={(e) => refOBj.checkHandler(e)}
        >
          System
        </LinkWithTooltip>
      );
    } else {
      return (
        <LinkWithTooltip
          tooltip={`${
            row.ab_emp_first_name +
            " " +
            row.ab_emp_last_name +
            " (" +
            row.ab_emp_desig_name +
            ")"
          }`}
          href="#"
          id="tooltip-1"
          clicked={(e) => refOBj.checkHandler(e)}
        >
          {row.ab_emp_first_name +
            " " +
            row.ab_emp_last_name +
            " (" +
            row.ab_emp_desig_name +
            ")"}
        </LinkWithTooltip>
      );
    }
  }
};

const hoverDate = (refOBj) => (cell, row) => {
  return (
    <LinkWithTooltip
      tooltip={cell}
      href="#"
      id="tooltip-1"
      clicked={(e) => refOBj.checkHandler(e)}
    >
      {cell}
    </LinkWithTooltip>
  );
};

const setCompanyName = (refObj) => (cell, row) => {
  return htmlDecode(cell);
};

class SubTaskTable extends Component {
  state = {
    showCreateSubTask: false,
    showAssign: false,
    showReAssign: false,
    showCQTReAssign: false,
    showRespondBack: false,
    showPoke: false,
    showIncreaseSla: false,
    showModalLoader: false,
    requestCaseChanges:false,
    requestCaseChangesAfterSO:false,
    CQT_customer:"",
    CQT_product:"",
    CQT_query:"",
    CQT_country:"",
    CQT_unit:"",
    CQT_spoc:"",
    cqtResponse:""
  };

  checkHandler = (event) => {
    event.preventDefault();
  };

  redirectUrl = (event, id) => {
    event.preventDefault();
    //http://reddy.indusnet.cloud/customer-dashboard?source=Mi02NTE=
    var emp_drupal_id = getMyDrupalId(localStorage.token);
    var base_encode = base64.encode(`${id}-${emp_drupal_id}`);
    window.open(
      portal_url + "customer-dashboard?source=" + base_encode,
      "_blank"
    );
  };

  redirectUrlTask = (event, task_id, assignId = "", assignment_id = "") => {
    event.preventDefault();
    if (assignId !== "") {
      window.open(
        `/user/task_details/${task_id}/${assignId}/#discussion`,
        "_blank"
      );
    } else {
      window.open(`/user/task_details/${task_id}/${assignment_id}`, "_blank");
    }
  };

  showCloseTaskPopup = (currRow) => {
    swal({
      closeOnClickOutside: false,
      title: "Close task",
      text: "Are you sure you want to close this task?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.setState({ showModalLoader: true });
        API.delete(`/api/tasks/check_close/${currRow.task_id}`)
          .then((res) => {
            this.setState({ showModalLoader: false });
            if (res.data.has_sub_task === 1) {
              swal({
                closeOnClickOutside: false,
                title: "Alert",
                text:
                  "This task has some open sub-tasks. \r\n Do you want to close them as well?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              }).then((willDelete) => {
                if (willDelete) {
                  this.setState({ showModalLoader: true });
                  API.delete(`/api/tasks/close/${currRow.task_id}`)
                    .then((res) => {
                      this.setState({ showModalLoader: false });
                      swal({
                        closeOnClickOutside: false,
                        title: "Success",
                        text: "Task has been closed.",
                        icon: "success",
                      }).then(() => {
                        this.props.reloadTaskSubTask();
                      });
                    })
                    .catch((err) => {
                      var token_rm = 2;
                      showErrorMessageFront(err, token_rm, this.props);
                    });
                }
              });
            } else {
              swal({
                closeOnClickOutside: false,
                title: "Success",
                text: "Task has been closed.",
                icon: "success",
              }).then(() => {
                this.props.reloadTaskSubTask();
              });
            }
          })
          .catch((err) => {
            var token_rm = 2;
            showErrorMessageFront(err, token_rm, this.props);
          });
      }
    });
  };

  showSubTaskAssignPopup = (id) => {
    this.setState({ showAssignSubTask: true, task_id: id });
  };

  showPoke = (currRow) => {
    this.setState({ showPoke: true, currRow: currRow });
  };

  showClone = (currRow) => {

    if(currRow.cqt > 0){

      API.get(`/api/drl/sub_task/${currRow.task_id}/${currRow.cqt}`)
      .then((cqtResponse) => {

        this.setState({
          
          CQT_customer      : cqtResponse.data.data.customer,
          CQT_product       : cqtResponse.data.data.product,
          CQT_query         : cqtResponse.data.data.query,
          CQT_country       : cqtResponse.data.data.country,
          CQT_unit          : cqtResponse.data.data.unit,
          CQT_spoc          : cqtResponse.data.data.spoc,

          cqtResponse       : cqtResponse.data.selected_data, 
          showCreateSubTask : true, 
          currRow           : currRow });
      })
      .catch((error) => {
          if (error.data.status === "3") {
          var token_rm = 2;
          showErrorMessageFront(error, token_rm, this.props);
          } else {
          console.log("Error", error);
          }
      });
    }else if(currRow.genpact === 1){
      this.setState({ showCreateSubTask: true, currRow: currRow });
    }else{
      this.setState({ showCreateSubTask: true, currRow: currRow });
    }
  };

  showRespondPopup = (currRow) => {
    // console.log('Respond');
    this.setState({ showRespondBack: true, currRow });
  };

  showAssignPopup = (currRow) => {
    //console.log('Assign Task');
    this.setState({ showAssign: true, currRow: currRow });
  };

  showReAssignPopup = (currRow) => {
    //console.log('Re Assign Task');
    this.setState({ showReAssign: true, currRow: currRow });
  };

  showReAssignCQTPopup = (currRow) => {
    //console.log('Re Assign CQT Task', currRow);
    this.setState({ showCQTReAssign: true, currRow: currRow });
  };

  showSubTaskRespondPopup = (id) => {
    this.setState({ showRespondSubTask: true, task_id: id });
  };

  showIncreaseSlaPopup = (currRow) => {
    this.setState({ showIncreaseSla: true, currRow: currRow });
  };

  refreshTable = () => {
    this.props.refreshTable();
  };

  handleClose = (closeObj) => {
    this.setState(closeObj);
  };

  tdClassName = (fieldValue, row) => {
    var dynamicClass = "width-150 ";
    if (row.vip_customer === 1) {
      dynamicClass += "bookmarked-column ";
    }
    return dynamicClass;
  };

  requestCaseChangesNew = currRow => {
    if(currRow.genpact == 1){
      API.get(`/api/add_task/check_active_case_dashboard/${currRow.task_id}`).then(
        (res) => {
          if (res.data.data.length > 0) {
            this.setState({
              requestCaseChanges: true,
              currRow: currRow,
              caseData: res.data.data
            });
          }else{
            swal({
              closeOnClickOutside: false,
              title: "Warning !!",
              text: "Task has no active cases.",
              icon: "warning",
            }).then(() => {
              
            });
          }
        }
      );
    }
  }

  requestCaseChangesAfterSONew = currRow => {
    if(currRow.genpact == 1){
      API.get(`/api/add_task/check_active_case_dashboard_so/${currRow.task_id}`).then(
        (res) => {
          if (res.data.active_case == true) {
            
            swal({
              closeOnClickOutside: false,
              title: "Warning !!",
              text: `Active case is found, please initiate changes using " Request Case changes" option.`,
              icon: "warning",
            }).then(() => {
              
            });

          }else{
            this.setState({
              requestCaseChangesAfterSO : true,
              currRow                   : currRow,
              sales_order_number        : res.data.sales_order_number
            });
          }
        }
      );
    }
  }

  render() {
    const selectRowProp = {
      bgColor: "#fff8f6",
    };

    return (
      <>
        {this.state.showModalLoader === true ? (
          <div className="loderOuter">
            <div className="loader">
              <img src={loaderlogo} alt="logo" />
              <div className="loading">Loading...</div>
            </div>
          </div>
        ) : (
          ""
        )}
        <BootstrapTable
          data={this.props.tableData}
          selectRow={selectRowProp}
          tableHeaderClass={"col-hidden"}
          expandColumnOptions={{
            expandColumnVisible: true,
            expandColumnComponent: this.expandColumnComponent,
            columnWidth: 25,
          }}
          trClassName="tr-expandable"
        >
          <TableHeaderColumn
            dataField="task_ref"
            dataSort={true}
            columnClassName={this.tdClassName}
            editable={false}
            dataFormat={clickToShowTasks(this)}
          >
            Tasks
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="request_type"
            dataSort={true}
            editable={false}
            dataFormat={setDescription(this)}
          >
            Description
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="request_type"
            dataSort={true}
            editable={false}
            dataFormat={setBlank(this)}
          >
            Description
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="display_assign_date"
            editable={false}
            expandable={false}
            dataFormat={hoverDate(this)}
          >
            Assigned Date
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="display_date_added"
            dataSort={true}
            editable={false}
            dataFormat={hoverDate(this)}
          >
            Created
          </TableHeaderColumn>

          {/* <TableHeaderColumn dataField='due_date' dataSort={ true } editable={ false }  dataFormat={ setCreateDate(this) } >Due Date</TableHeaderColumn> */}

          <TableHeaderColumn
            dataFormat={setDaysPending(this)}
            dataField="display_new_due_date"
            dataSort={true}
            editable={false}
          >
            Due Date
          </TableHeaderColumn>

          {/* <TableHeaderColumn dataField='assigned_to' dataSort={ true } editable={ false }  >Assigned To</TableHeaderColumn> */}

          <TableHeaderColumn
            dataField="dept_name"
            //dataSort={true}
            editable={false}
            expandable={false}
            dataFormat={setAssignedBy(this)}
          >
            Assigned By{" "}
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="dept_name"
            dataSort={true}
            editable={false}
            expandable={false}
            dataFormat={setAssignedTo(this)}
          >
            Assigned To{" "}
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="company_name"
            editable={false}
            expandable={false}
            dataFormat={setCompanyName(this)}
          >
            Customer
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="cust_name"
            dataSort={true}
            editable={false}
            dataFormat={setEmpCustName(this)}
          >
            Customer Name
          </TableHeaderColumn>

          {/* <TableHeaderColumn dataField='dept_name' dataSort={ true } editable={ false } expandable={ false } >Department</TableHeaderColumn> */}

          <TableHeaderColumn
            dataField="status"
            dataFormat={getStatusColumn(this)}
            editable={false}
          >
            Status
          </TableHeaderColumn>

          <TableHeaderColumn
            isKey
            dataField="task_id"
            dataFormat={getActions(this)}
            editable={false}
          ></TableHeaderColumn>
        </BootstrapTable>

        {/*DONE*/}
        <RespondBackPopup
          showRespondBack={this.state.showRespondBack}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.reloadTaskSubTask()}
        />

        {/*DONE*/}
        <AssignPopup
          showAssign={this.state.showAssign}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.reloadTaskSubTask()}
        />

        {/*DONE*/}
        <ReAssignPopup
          showReAssign={this.state.showReAssign}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.reloadTaskSubTask()}
        />

        {/*DONE*/}
        <CQTReAssignPopup
          showCQTReAssign={this.state.showCQTReAssign}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.reloadTaskSubTask()}
        />

        {/*DONE*/}
        <CreateSubTaskPopup
          showCreateSubTask={this.state.showCreateSubTask}
          currRow={this.state.currRow}
          clone={1}
          CQT_customer={this.state.CQT_customer}
          CQT_product={this.state.CQT_product}
          CQT_query={this.state.CQT_query}
          CQT_country={this.state.CQT_country}
          CQT_unit={this.state.CQT_unit}
          CQT_spoc={this.state.CQT_spoc}
          cqtResponse={this.state.cqtResponse}
          handleClose={this.handleClose}
          reloadTask={() => this.props.reloadTaskSubTask()}
        />

        <IncreaseSlaSubTaskPopup
          showIncreaseSla={this.state.showIncreaseSla}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.reloadTaskSubTask()}
        />

        <Poke
          showPoke={this.state.showPoke}
          currRow={this.state.currRow}
          handleClose={this.handleClose}
          reloadTask={() => this.props.reloadTaskSubTask()}
        />

        <RequestCaseChanges
            requestCaseChanges={this.state.requestCaseChanges}
            currRow={this.state.currRow}
            caseData={this.state.caseData}
            handleClose={this.handleClose}
            reloadTask={() => this.props.reloadTaskSubTask()}
        />

        <RequestCaseChanges
            requestCaseChangesAfterSO={this.state.requestCaseChangesAfterSO}
            currRow={this.state.currRow}
            sales_order_number={this.state.sales_order_number}
            handleClose={this.handleClose}
            reloadTask={() => this.props.reloadTaskSubTask()}
        />



      </>
    );
  }
}

export default SubTaskTable;
