import React, { Component } from "react";
import { Row, Col, ButtonToolbar, Button, Modal, Alert } from "react-bootstrap";

//import { FilePond } from 'react-filepond';
//import 'filepond/dist/filepond.min.css';
import Dropzone from "react-dropzone";

import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import dateFormat from "dateformat";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";
import API from "../../shared/axios";
import { showErrorMessageFront } from "../../shared/handle_error_front";

import swal from "sweetalert";

import { localDate, trimString } from "../../shared/helper";

//import uploadAxios from 'axios';

import TinyMCE from "react-tinymce";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";

const initialValues = {
  comment: "",
  reason: null,
};

const ipdoTypes_options = [
  { label: "IPDO", value: "IPDO" },
  { label: "EMQA", value: "EMQA" },
  { label: "Plant", value: "Plant" },
  { label: "Others", value: "Others" } 
];

class IPDOPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPoke: false,
      files: [],
      sla_breach: false,
      IPDO_disabled: true,
      showModalLoader: false,
      acceptNewEmp: false,
      acceptNewEmpMsg: "",
      filesHtml: "",
      showIPDO: false,
      reason: null
    };
  }

  handleCommentChange = (e, setFieldValue) => {
    setFieldValue(e.target.name, e.target.value);
  };

  componentWillReceiveProps = (nextProps) => {
    if (
      nextProps.showIPDO === true &&
      nextProps.currRow.task_id > 0
    ) {
      this.setState({
        showIPDO: nextProps.showIPDO,
        currRow: nextProps.currRow,
        fromPopup:nextProps.fromPopup,
        IPDO_disabled:true
      });
    }
  };

  handleClose = () => {
    var close = {
      showModalLoader: false,
      showIPDO: false,
      reason: null,
    };
    this.setState(close);
    this.props.handleClose(close);
  };

  changeReason = (e, setFieldValue, setFieldTouched) => {
    setFieldTouched("comment");
    if (e === null) {
      setFieldValue("reason", null);
      this.setState({ reason: "" });
    } else {
      setFieldValue("reason", e.value);
      this.setState({ reason: e });
    }
  };

  changeIPDO = (event, setFieldValue) => {
    if (event === null) {
      setFieldValue("request_related_to", "");
      this.setState({ IPDO_disabled: true });
    } else {
      setFieldValue("request_related_to", event);
      this.setState({ IPDO_disabled: false });
    }

  };

  handleDeleteTask = (values, actions) => {
    this.setState({ showModalLoader: true });
    var post_data = {
      reason: values.reason,
      cancel_comment: values.comment,
    };

    API.put(`/api/tasks/check_delete/${this.state.currRow.task_id}`, post_data)
      .then((res) => {
        this.handleClose();
        if (res.data.has_sub_task === 1) {
          this.setState({ showModalLoader: false, showIPDO: false });
          swal({
            closeOnClickOutside: false,
            title: "Alert",
            text:
              "This task has some open sub-tasks. \r\n Do you want to cancel them as well?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          }).then((willDelete) => {
            if (willDelete) {
              this.setState({ showModalLoader: true });
              API.put(
                `/api/tasks/delete/${this.state.currRow.task_id}`,
                post_data
              )
                .then((res) => {
                  this.setState({ showModalLoader: false });
                  swal({
                    closeOnClickOutside: false,
                    title: "Success",
                    text: "Task has been cancelled!",
                    icon: "success",
                  }).then(() => {
                    this.props.reloadTask();
                  });
                })
                .catch((err) => {
                  var token_rm = 2;
                  showErrorMessageFront(err, token_rm, this.props);
                });
            } else {
            }
          });
        } else {
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Task has been cancelled!",
            icon: "success",
          }).then(() => {
            this.props.reloadTask();
          });
        }
      })
      .catch((err) => {
        this.setState({ showModalLoader: false });
        if (err.data.status === 3) {
          var token_rm = 2;
          showErrorMessageFront(err, token_rm, this.props);
        } else {
          actions.setErrors(err.data.errors);
        }
      });
  };

  submitIPDOType = (values, actions) => {

    this.setState({ showModalLoader: true });

    var formData = new FormData();

    formData.append("request_related_to", values.request_related_to.value);

    let sel_arr = values;

    API.put(`/api/add_task/update_related_to/${this.state.currRow.task_id}`, formData)
    .then((res) => {
      this.handleClose()
      swal({
        closeOnClickOutside: false,
        title: "Success",
        text: "Request Category Has Been Set.",
        icon: "success",
      }).then(() => {
        
        let ret_obj = {
          currRow:this.state.currRow,
          fromPopup:this.state.fromPopup
        }
        this.props.showNextPopup(ret_obj);
      });
    })
    .catch((error) => {
      this.setState({ showModalLoader: false });
      if (error.data.status === 3) {
        var token_rm = 2;
        showErrorMessageFront(error, token_rm, this.props);
      } else {
        actions.setErrors(error.data.errors);
        actions.setSubmitting(false);
      }
    });
  };

  render() {
    const newInitialValues = Object.assign(initialValues, {
    });

    return (
      <>
        <Modal
          show={this.state.showIPDO}
          onHide={() => this.handleClose()}
          backdrop="static"
        >
          <Formik
            initialValues={newInitialValues}
            onSubmit={this.submitIPDOType}
          >
            {({
              values,
              errors,
              touched,
              isValid,
              isSubmitting,
              handleChange,
              setFieldValue,
              setFieldTouched,
              setErrors,
            }) => {
              // console.log("errors", errors);
              // console.log("touched", touched);
              return (
                <Form encType="multipart/form-data">
                  {this.state.showModalLoader === true ? (
                    <div className="loderOuter">
                      <div className="loader">
                        <img src={loaderlogo} alt="logo" />
                        <div className="loading">Loading...</div>
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                  <Modal.Header closeButton>
                    <Modal.Title>
                      Set Request Category | {this.state.currRow.task_ref}
                    </Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <div className="contBox">
                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <label>
                              Please Set Request Category{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Select
                              className="basic-single"
                              classNamePrefix="Select Request Type"
                              defaultValue={values.request_related_to}
                              isClearable={true}
                              isSearchable={true}
                              name="request_related_to"
                              options={ipdoTypes_options}
                              onChange={(e) => {
                                this.changeIPDO(e, setFieldValue);
                              }}
                            />
                            {errors.reason && touched.reason ? (
                              <span className="errorMsg">{errors.reason}</span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </Modal.Body>
                  <Modal.Footer>
                    <button
                      onClick={this.handleClose}
                      className={`btn-line`}
                      type="button"
                    >
                      Close
                    </button>
                    <button type="submit" className={`btn-fill ml-10 ${this.state.IPDO_disabled && 'btn-disable'}`} disabled={this.state.IPDO_disabled} >
                      {" "}
                      Submit{" "}
                    </button>
                  </Modal.Footer>
                </Form>
              );
            }}
          </Formik>
        </Modal>
      </>
    );
  }
}

export default IPDOPopup;
