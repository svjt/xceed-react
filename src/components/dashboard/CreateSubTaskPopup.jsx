import React, { Component } from "react";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Modal,
  Alert,
  Tooltip,
  OverlayTrigger,
} from "react-bootstrap";
import Dropzone from "react-dropzone";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";

import dateFormat from "dateformat";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";
import { showErrorMessageFront } from "../../shared/handle_error_front";
import API from "../../shared/axios";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
import Autosuggest from "react-autosuggest";

import { localDate, trimString, htmlDecode } from "../../shared/helper";

import { Editor } from "@tinymce/tinymce-react";
import { Link } from "react-router-dom";

const s3bucket_comment_diss_path = `${process.env.REACT_APP_API_URL}/api/tasks/download_comment_bucket/`;
const task_path = `${process.env.REACT_APP_API_URL}/api/tasks/download_task_bucket/`;
// SVJT

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const removeDropZoneFiles = (fileName, objRef, setErrors) => {
  var newArr = [];
  for (let index = 0; index < objRef.state.files.length; index++) {
    const element = objRef.state.files[index];

    if (fileName === element.name) {
    } else {
      newArr.push(element);
    }
  }

  var fileListHtml = newArr.map((file) => (
    <Alert key={file.name}>
      <span onClick={() => removeDropZoneFiles(file.name, objRef, setErrors)}>
        <i className="far fa-times-circle"></i>
      </span>{" "}
      {file.name}
    </Alert>
  ));
  setErrors({ file_name: "" });
  objRef.setState({
    files: newArr,
    filesHtml: fileListHtml,
  });
};

const initialValues = {
  title: "",
  file: "",
  employee_id: "",
  authorization: "",
  breach_reason: "",
  sendCQT: false,
  sendGenpact: false,
  sendDefault: true,
  dueDate: "",
  display_discussion: false,
  file_name: [],
  clonePrevFiles: [],
};

class CreateSubTaskPopup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showCreateSubTask: false,
      task_id: 0,
      stateEmplId: "",
      new_employee: false,
      msg_new_employee: "",
      sendCQT: false,
      sendGenpact: false,
      sendDefault: true,
      sentGenpactType: 0,
      files: [],
      CQT_customer: [],
      CQT_product: [],
      CQT_query: [],
      CQT_criticality: [
        { value: "High", label: "High" },
        { value: "Medium", label: "Medium" },
        { value: "Low", label: "Low" },
      ],
      CQT_country: [],
      CQT_unit: [],
      CQT_reference: [],
      CQT_spoc: [],
      showLoader: false,
      showGenpactLoader: false,
      sla_breach: false,
      value: "", // for auto-suggest
      suggestions: [], // for auto-suggest
      ref_loader: false, // for auto-suggest
      currRow: [],
      clone_sub: 0,
      clone_main_task: 0,
      dynamic_form: [],
      bm_arr: [],
      region_arr: [],
      sapIncoterms_arr: [],
      sapCountry_arr: [],
      sapOrderReason_arr: [],
      sapShipment_arr: [],
      sapOrgDocs_arr: [],
      sapProdCode_arr: [],
      sapPaymentTerms_arr: [],
      sapCurrency_arr: [],
      sapPharmaRef_arr: [],
      sapPlantCode_arr: [],
      sapShipTo_arr: [],
      defaultValue_genpact: '',
      genpact_options: [
        { value: "1", label: "Related to Sales Order" },
        { value: "2", label: "Related to STO" },
      ],
      selected_genpact_option: "",
      prev_files: [],
      product_code_auto_suggest: "",
      genpact_task_auto_suggest: "",
      genpact_task_options: [],
      parent_genpact_tasks: [],
      buyer_sold_auto_suggest: "",
      buyer_cust_code_auto_suggest: "",
      prefill_genpact_task: ""
    };
  }

  checkHandler = (event) => {
    event.preventDefault();
  };

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.showCreateSubTask === true && nextProps.currRow.task_id > 0) {
      // if(nextProps.currRow.select_genpact && nextProps.currRow.select_genpact === 1){
      //     this.setState({
      //         sendGenpact : true,
      //         sendDefault : false,
      //         sendCQT: false,
      //         showGenpactLoader: false
      //     });
      //     initialValues.sendGenpact = true;
      //     initialValues.sendDefault = false;
      //     initialValues.sendCQT = false;
      // }

      //console.log("currentROW",nextProps.currRow,this.props.clone);

      if (this.props.clone && this.props.clone === 1) {

        if (nextProps.currRow.genpact === 1) {
          let dynamic_form_db = JSON.parse(nextProps.currRow.genpact_checklist);
          let dynamic_arr = [];
          for (let index = 0; index < dynamic_form_db.length; index++) {
            if (dynamic_form_db[index].field_type === "date") {
              dynamic_form_db[index].selected_value = new Date(
                dynamic_form_db[index].selected_value
              );
            }

            dynamic_arr.push(dynamic_form_db[index]);
          }

          this.setState({
            sendGenpact: true,
            sendDefault: false,
            sendCQT: false,
            showGenpactLoader: false,
            dynamic_form: dynamic_arr,
          });
          initialValues.sendGenpact = true;
          initialValues.sendDefault = false;
          initialValues.sendCQT = false;
        } else if (nextProps.currRow.cqt > 0) {
          initialValues.sendGenpact = false;
          initialValues.sendDefault = false;
          initialValues.sendCQT = true;

          this.setState({
            CQT_customer: nextProps.CQT_customer,
            CQT_product: nextProps.CQT_product,
            CQT_query: nextProps.CQT_query,
            CQT_country: nextProps.CQT_country,
            CQT_unit: nextProps.CQT_unit,
            CQT_spoc: nextProps.CQT_spoc,
            sendGenpact: false,
            sendDefault: false,
            sendCQT: true,
            value: nextProps.cqtResponse.cqt_reference_id.title
          });


          initialValues.cqt_customer_id = nextProps.cqtResponse.cqt_customer_id;
          initialValues.cqt_product_id = nextProps.cqtResponse.cqt_product_id;
          initialValues.cqt_query_id = nextProps.cqtResponse.cqt_query_id;
          initialValues.cqt_criticality_value = nextProps.cqtResponse.cqt_criticality_value;
          initialValues.cqt_reference_id = nextProps.cqtResponse.cqt_reference_id.id;
          //document.getElementById("reference_query").value = nextProps.cqtResponse.cqt_reference_id.title;
          initialValues.cqt_country_id = nextProps.cqtResponse.cqt_country_id;
          initialValues.cqt_unit_id = nextProps.cqtResponse.cqt_unit_id;


        } else {
          this.setState({
            sendGenpact: false,
            sendDefault: true,
            sendCQT: false,
            showGenpactLoader: false,
          });
          initialValues.sendGenpact = false;
          initialValues.sendDefault = true;
          initialValues.sendCQT = false;
        }

        initialValues.title = nextProps.currRow.title;
        initialValues.display_discussion =
          nextProps.currRow.display_discussion === 1 ? true : false;

        if (
          nextProps.currRow.breach_reason !== "" &&
          nextProps.currRow.breach_reason !== null
        ) {
          this.setState({ sla_breach: true });
          initialValues.breach_reason = nextProps.currRow.breach_reason;
        }

        initialValues.content = htmlDecode(nextProps.currRow.content);
        this.setState({ clone_sub: 1 });

        API.get(`/api/tasks/get_sub_task_files/${nextProps.currRow.task_id}`)
          .then((res) => {
            this.setState({ clonePrevFiles: res.data.data });
          })
          .catch((error) => { });
      } else if (this.props.clone_main && this.props.clone_main === 1) {
        this.setState({
          sendGenpact: false,
          sendDefault: true,
          sendCQT: false,
          showGenpactLoader: false,
        });
        initialValues.sendGenpact = false;
        initialValues.sendDefault = true;
        initialValues.sendCQT = false;

        if (nextProps.currRow.language != 'en') {
          initialValues.content = htmlDecode(nextProps.currRow.english_content);
        } else {
          initialValues.content = htmlDecode(nextProps.currRow.content);
        }
        this.setState({ clone_main_task: 1 });

        API.get(`/api/tasks/get_task_files/${nextProps.currRow.task_id}`)
          .then((res) => {
            this.setState({ clonePrevFiles: res.data.data });
          })
          .catch((error) => { });
      } else {
        this.setState({
          sendGenpact: false,
          sendDefault: true,
          sendCQT: false,
          showGenpactLoader: false,
        });

        initialValues.sendGenpact = false;
        initialValues.sendDefault = true;
        initialValues.sendCQT = false;

        initialValues.title = "";
        initialValues.breach_reason = "";
        initialValues.content = "";
        initialValues.display_discussion = false;
      }

      var post_date = {};
      if (nextProps.currRow.parent_id > 0) {
        post_date = {
          due_date: nextProps.currRow.due_date,
          sub_task: 1,
          auto: 0,
        };
      } else {
        post_date = { due_date: nextProps.currRow.due_date, auto: 1 };
      }

      API.post(`/api/tasks/sla_breach/${nextProps.currRow.task_id}`, post_date)
        .then((res) => {
          //console.log(res.data);
          if (res.data.status === 1) {
            this.setState({ sla_breach: true, showModalLoader: false });
          } else {
            this.setState({ sla_breach: false, showModalLoader: false });
          }

          //var id = 1;
          this.setState({
            showCreateSubTask: nextProps.showCreateSubTask,
            currRow: nextProps.currRow,
            post_arr: [{
              breach_reason: '',
              due_date: localDate(nextProps.currRow.due_date),
              employee_id: '',
              new_employee: '',
              breached: (res.data.status === 1) ? true : false,
              display_discussion: false,
              error_msg: {
                employee_id: '',
                due_date: '',
                breach_reason: ''
              },
              errors: 0
            }],
            cqt_post_arr: [{
              breach_reason: '',
              dueDate: localDate(nextProps.currRow.due_date),
              cqt_spoc_id: '',
              breached: (res.data.status === 1) ? true : false,
              error_msg: {
                cqt_spoc_id: '',
                dueDate: '',
                breach_reason: ''
              },
              errors: 0
            }],
          });

          API.get(`/api/employees/other`)
            .then((res) => {
              //console.log(res.data.data);

              var myTeam = [];
              for (let index = 0; index < res.data.data.length; index++) {
                const element = res.data.data[index];
                var leave = "";
                if (element.on_leave == 1) {
                  leave = "[On Leave]";
                }
                myTeam.push({
                  value: element["employee_id"],
                  label:
                    element["first_name"] +
                    " " +
                    element["last_name"] +
                    " (" +
                    element["desig_name"] +
                    ") " +
                    leave,
                });

                if (
                  this.props.clone &&
                  this.props.clone === 1 &&
                  nextProps.currRow.assigned_to == element["employee_id"]
                ) {
                  this.setState({
                    stateEmplId: {
                      value: element["employee_id"],
                      label:
                        element["first_name"] +
                        " " +
                        element["last_name"] +
                        " (" +
                        element["desig_name"] +
                        ") " +
                        leave,
                    },
                  });
                  initialValues.employee_id = {
                    value: element["employee_id"],
                    label:
                      element["first_name"] +
                      " " +
                      element["last_name"] +
                      " (" +
                      element["desig_name"] +
                      ") " +
                      leave,
                  };
                }
              }

              this.setState({ employeeArr: myTeam });
              initialValues.dueDate = localDate(nextProps.currRow.due_date);
            })
            .catch((error) => { });
        })
        .catch((error) => { });

      API.post(`/api/feed/genpact_task`, { task_id: nextProps.currRow.task_id })
        .then((res) => {
          this.setState({ genpact_task_options: res.data.data });
        })
        .catch((err) => {
          console.log(err);
        });

      API.post(`/api/tasks/get_parent_order_ids/${nextProps.currRow.task_id}`)
        .then((res) => {
          this.setState({ parent_genpact_tasks: res.data.data });
        })
        .catch((err) => {
          console.log(err);
        });

    }
  };

  setDropZoneFiles = (acceptedFiles, setErrors, setFieldValue) => {
    setErrors({ file_name: false });
    setFieldValue(this.state.files);
    var prevFiles = this.state.files;
    var newFiles;
    if (prevFiles.length > 0) {
      //newFiles = newConcatFiles = acceptedFiles.concat(prevFiles);

      for (let index = 0; index < acceptedFiles.length; index++) {
        var remove = 0;

        for (let index2 = 0; index2 < prevFiles.length; index2++) {
          if (acceptedFiles[index].name === prevFiles[index2].name) {
            remove = 1;
            break;
          }
        }

        //console.log('remove',acceptedFiles[index].name,remove);

        if (remove === 0) {
          prevFiles.push(acceptedFiles[index]);
        }
      }
      //console.log('acceptedFiles',acceptedFiles);
      //console.log('prevFiles',prevFiles);
      newFiles = prevFiles;
    } else {
      newFiles = acceptedFiles;
    }

    var fileListHtml = newFiles.map((file) => (
      <Alert key={file.name}>
        <span onClick={() => removeDropZoneFiles(file.name, this, setErrors)}>
          <i className="far fa-times-circle"></i>
        </span>{" "}
        {trimString(25, file.name)}
      </Alert>
    ));

    this.setState({
      files: newFiles,
      filesHtml: fileListHtml,
    });
  };



  handleClose = () => {
    //reset cqt data
    this.setState({
      sendCQT: false,
      sendGenpact: false,
      sendDefault: true,
      sentGenpactType: 0,
      CQT_customer: [],
      CQT_product: [],
      CQT_query: [],
      CQT_country: [],
      CQT_unit: [],
      CQT_reference: [],
      CQT_spoc: [],
      stateEmplId: "",
      new_employee: false,
      msg_new_employee: "",
      showLoader: false,
      showGenpactLoader: false,
      currRow: [],
      clone_sub: 0,
      clone_main_task: 0,
      clonePrevFiles: [],
      files: [],
      filesHtml: [],
      dynamic_form: [],
      bm_arr: [],
      region_arr: [],
      sapIncoterms_arr: [],
      sapCountry_arr: [],
      sapOrderReason_arr: [],
      sapShipment_arr: [],
      sapOrgDocs_arr: [],
      sapProdCode_arr: [],
      sapPaymentTerms_arr: [],
      sapCurrency_arr: [],
      sapPharmaRef_arr: [],
      sapPlantCode_arr: [],
      sapShipTo_arr: [],
      defaultValue_genpact: '',
      post_arr: [{
        breach_reason: '',
        due_date: localDate(this.state.currRow.due_date),
        employee_id: '',
        new_employee: '',
        breached: this.state.sla_breach,
        error_msg: {
          employee_id: '',
          due_date: '',
          breach_reason: ''
        },
        errors: 0
      }],
      cqt_post_arr: [{
        breach_reason: '',
        dueDate: localDate(this.state.currRow.due_date),
        cqt_spoc_id: '',
        breached: this.state.sla_breach,
        error_msg: {
          cqt_spoc_id: '',
          dueDate: '',
          breach_reason: ''
        },
        errors: 0
      }]
    });
    //end here
    var close = {
      sla_breach: false,
      showCreateSubTask: false,
      showCloneCreateSubTask: false,
      showCreateSubTaskNew: false,
      task_id: 0,
    };
    this.setState(close);
    this.props.handleClose(close);
    if(this.props.reloadOnClose){
      this.props.reloadTask();
    }
  };

  modifyArr = (arr) => {
    for (let index = 0; index < arr.length; index++) {
      if (arr[index].rdd != null) {
        arr[index].rdd = dateFormat(arr[index].rdd, "yyyy-mm-dd");
      }
    }
    return arr;
  }

  handleSubmitCreateTask = (values, actions) => {
    this.setState({ showLoader: true });
    // if (this.state.sla_breach === true && values.breach_reason === "") {
    //   actions.setErrors({ breach_reason: "Please provide a reason." });
    //   actions.setSubmitting(false);
    //   this.setState({ showLoader: false });
    // } else {
    let err_cnt = 0;
    var prev_post_arr = [];

    if (values.sendGenpact) {
      let prev_data = this.state.dynamic_form;
      for (let index = 0; index < prev_data.length; index++) {
        const element = prev_data[index];
        //console.log(element.required, element.selected_value);
        if (element.required == 1 && element.selected_value == "") {
          prev_data[index].err_msg = "This field is required";
          err_cnt++;
        }
      }

      // console.log({prev_data});

      this.setState({ dynamic_form: prev_data });
    }

    if (this.state.sendDefault) {
      prev_post_arr = this.state.post_arr;
      for (let index_new = 0; index_new < prev_post_arr.length; index_new++) {
        if (prev_post_arr[index_new].breached && prev_post_arr[index_new].breach_reason === '') {
          prev_post_arr[index_new].error_msg.breach_reason = 'Please provide a reason.';
          err_cnt++;
          prev_post_arr[index_new].errors = err_cnt;
        }

        if (prev_post_arr[index_new].due_date === '') {
          prev_post_arr[index_new].error_msg.due_date = 'Please provide due date.';
          err_cnt++;
          prev_post_arr[index_new].errors = err_cnt;
        }

        if (prev_post_arr[index_new].employee_id === '') {
          prev_post_arr[index_new].error_msg.employee_id = 'Please select employee.';
          err_cnt++;
          prev_post_arr[index_new].errors = err_cnt;
        }
      }
    }

    if (this.state.sendCQT) {
      prev_post_arr = this.state.cqt_post_arr;
      for (let index_new = 0; index_new < prev_post_arr.length; index_new++) {
        if (prev_post_arr[index_new].breached && prev_post_arr[index_new].breach_reason === '') {
          prev_post_arr[index_new].error_msg.breach_reason = 'Please provide a reason.';
          err_cnt++;
          prev_post_arr[index_new].errors = err_cnt;
        }

        if (prev_post_arr[index_new].dueDate === '') {
          prev_post_arr[index_new].error_msg.dueDate = 'Please provide due date.';
          err_cnt++;
          prev_post_arr[index_new].errors = err_cnt;
        }

        if (prev_post_arr[index_new].cqt_spoc_id === '') {
          prev_post_arr[index_new].error_msg.cqt_spoc_id = 'Please select assign to.';
          err_cnt++;
          prev_post_arr[index_new].errors = err_cnt;
        }
      }
    }

    let parent_genpact_tasks = this.state.parent_genpact_tasks;
    if (values.sendGenpact) {
      if (parent_genpact_tasks.length > 0) {

        for (let index = 0; index < parent_genpact_tasks.length; index++) {

          if (parent_genpact_tasks[index].checked == true) {
            if (parent_genpact_tasks[index].quantity == '') {
              parent_genpact_tasks[index].errors = 'Quantity cannot be blank';
              err_cnt++;
            }

            if (parent_genpact_tasks[index].units == '') {
              parent_genpact_tasks[index].errors = 'Units cannot be blank';
              err_cnt++;
            }

            if (parent_genpact_tasks[index].rdd == '') {
              parent_genpact_tasks[index].errors = 'Rdd cannot be blank';
              err_cnt++;
            }
          }

        }
      }
    }

    if (err_cnt == 0) {
      var formData = new FormData();

      // console.log('values.dueDate', values.dueDate);

      var due_date = dateFormat(values.dueDate, "yyyy-mm-dd");

      formData.append("title", values.title);
      formData.append("content", values.content);
      formData.append("need_authorization", 0);
      formData.append(
        "display_discussion",
        values.display_discussion ? 1 : 0
      );


      if (this.state.sla_breach === false) {
        formData.append("breach_reason", "");
      } else {
        formData.append("breach_reason", values.breach_reason);
      }
      if (values.sendGenpact) {
        if (this.state.selected_genpact_option) {
          formData.append("genpact_type", this.state.selected_genpact_option.value); // "1" => Related to Sales Order, "2" => Related to STO 
        }
      }

      formData.append("sendCQT", values.sendCQT);
      formData.append("sendGenpact", values.sendGenpact);

      if (this.props.clone_main && this.props.clone_main === 1) {
        formData.append(
          "prev_main_files",
          this.state.clonePrevFiles && this.state.clonePrevFiles.length > 0
            ? JSON.stringify(this.state.clonePrevFiles)
            : JSON.stringify([])
        );

        formData.append("prev_files", JSON.stringify([]));
      } else {
        formData.append(
          "prev_files",
          this.state.clonePrevFiles && this.state.clonePrevFiles.length > 0
            ? JSON.stringify(this.state.clonePrevFiles)
            : JSON.stringify([])
        );

        formData.append("prev_main_files", JSON.stringify([]));
      }

      var reference_id =
        values.cqt_reference_id !== undefined ? values.cqt_reference_id : 0;

      if (values.sendCQT) {
        formData.append("cqt_country_name", values.cqt_country_id.label);
        formData.append("cqt_customer_id", values.cqt_customer_id.value);
        formData.append("cqt_product_id", values.cqt_product_id.value);
        formData.append("cqt_query_id", values.cqt_query_id.value);
        formData.append(
          "cqt_criticality",
          values.cqt_criticality_value.value
        );
        formData.append("cqt_reference_id", reference_id);
        formData.append("cqt_unit_name", values.cqt_unit_id.label);
        //formData.append("cqt_spoc_id", values.cqt_spoc_id.value);
        //formData.append("due_date", due_date);

        var prev_post_arr = this.state.cqt_post_arr;
        for (let index_new = 0; index_new < prev_post_arr.length; index_new++) {
          prev_post_arr[index_new].dueDate = dateFormat(prev_post_arr[index_new].dueDate, "yyyy-mm-dd");
        }


        formData.append("proforma_id", 0);
        formData.append("cqt_post_arr", JSON.stringify(prev_post_arr));
      } else if (values.sendGenpact) {
        var prev_post_arr = [];
        for (let index_new = 0; index_new < this.state.dynamic_form.length; index_new++) {
          prev_post_arr.push({ ...this.state.dynamic_form[index_new], values: '' });
        }
        //formData.append("assigned_to", 0);
        formData.append("due_date", due_date);
        //formData.append('proforma_id',(this.state.currRow.proforma_id > 0)?this.state.currRow.proforma_id:0);
        formData.append("proforma_id", 0);
        formData.append(
          "check_list_data",
          JSON.stringify(prev_post_arr)
        );

        if (this.state.prev_files.length && this.state.prev_files.length > 0) {
          var prev_arr = [];
          for (let index = 0; index < this.state.prev_files.length; index++) {
            const element = this.state.prev_files[index];
            prev_arr.push({ key: element.upload_id });
          }
          formData.append("copied_attachments", JSON.stringify(prev_arr));
        }

        formData.append("parent_genpact_tasks", JSON.stringify(this.modifyArr(parent_genpact_tasks)));

      } else {

        var prev_post_arr = this.state.post_arr;
        for (let index_new = 0; index_new < prev_post_arr.length; index_new++) {
          prev_post_arr[index_new].due_date = dateFormat(prev_post_arr[index_new].due_date, "yyyy-mm-dd");
        }

        //formData.append("assigned_to", values.employee_id.value);
        //formData.append("due_date", due_date);
        formData.append("proforma_id", 0);
        formData.append("post_arr", JSON.stringify(prev_post_arr));
      }

      // file upload
      if (this.state.files && this.state.files.length > 0) {
        for (let index = 0; index < this.state.files.length; index++) {
          const element = this.state.files[index];
          formData.append("file", element);
        }
        // for (const file of this.state.files) {
        //     alert(file);
        //     formData.append('file', file)
        // }
        //formData.append('file', this.state.file);
      } else {
        formData.append("file", "");
      }

      const config = {
        headers: {
          "content-type": "multipart/form-data",
        },
      };

      let task_id = this.state.currRow.task_id;

      if (this.state.clone_sub == 1) {
        task_id = this.state.currRow.parent_id;
      }
      console.log(formData);
      API.post(`/api/tasks/create_sub/${task_id}`, formData, config)
        .then((res) => {
          this.handleClose();
          var succ_text;
          if (this.state.sendCQT === true) {
            succ_text =
              "Sub Task created successfully! Please check after a few minutes.";
          } else {
            succ_text = "Sub Task created successfully!";
          }
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: succ_text,
            icon: "success",
          }).then(() => {
            this.props.reloadTask();
          });
        })
        .catch((error) => {
          this.setState({ showLoader: false });
          actions.setSubmitting(false);
          console.log({ error });
          if (error.data && error.data.status === 3) {
            var token_rm = 2;
            showErrorMessageFront(error, token_rm, this.props);
            this.handleClose();
          } else if (error.data && error.data.status === 2) {
            actions.setErrors(error.data.errors);
          }
        });
    } else {
      actions.setSubmitting(false);
      this.setState({ showLoader: false });
      if (this.state.sendDefault) {
        this.setState({ post_arr: prev_post_arr });
      }
      if (this.state.sendCQT) {
        this.setState({ cqt_post_arr: prev_post_arr });
      }

      if (values.sendGenpact && parent_genpact_tasks.length > 0) {
        this.setState({ parent_genpact_tasks: parent_genpact_tasks });
      }

    }
    //}
  };

  // ==== Autosuggest for Reference Query DRL ==== //
  onReferenceChange = (event, { newValue }) => {
    this.setState({
      value: newValue,
    });
  };
  // ==== Autosuggest for Reference Query DRL ==== //
  onSuggestionsFetchRequested = ({ value }) => {
    const inputValue = value.toLowerCase();
    const inputLength = inputValue.length;

    if (inputLength === 0) {
      return [];
    } else {
      if (inputLength > 3) {
        this.setState({ ref_loader: true });

        API.get(`/api/drl/reference/${encodeURIComponent(inputValue)}`)
          .then((res) => {
            this.setState({
              suggestions: res.data.data,
              ref_loader: false,
            });
            return this.state.suggestions;
          })
          .catch((err) => { });
      }
    }
  };
  // ==== Autosuggest for Reference Query DRL ==== //
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
    });
  };

  getDueDate = () => {
    if (
      typeof this.state.currRow !== "undefined" &&
      this.state.currRow.length > 0
    ) {
      //console.log(this.state.currRow.due_date);
      //var due_date = dateFormat(localDate(this.state.currRow.due_date), "yyyy-mm-dd");
      //var replacedDate = this.state.currRow.due_date.split('T');
      //var dateFormat = replacedDate[0];
      //return new Date(due_date);
      return localDate(this.state.currRow.due_date);
    }
  };

  changeDate = (event, setFieldValue) => {
    if (event === null) {
      setFieldValue("dueDate", this.getDueDate());
      this.setState({ sla_breach: false });
    } else {
      setFieldValue("dueDate", event);
      this.setState({ showLoader: true });
      var post_date = {};

      let post_due_date = `${dateFormat(event, "yyyy-mm-dd HH:MM:ss")}`;

      if (this.state.currRow.parent_id > 0) {
        post_date = { due_date: post_due_date, sub_task: 1, auto: 0 };
      } else {
        post_date = { due_date: post_due_date, auto: 0 };
      }

      API.post(`/api/tasks/sla_breach/${this.state.currRow.task_id}`, post_date)
        .then((res) => {
          //console.log(res.data);
          if (res.data.status === 1) {
            this.setState({ sla_breach: true, showLoader: false });
          } else {
            this.setState({ sla_breach: false, showLoader: false });
          }
        })
        .catch((error) => { });
    }
  };

  handleCQTbox = (e, setFieldValue) => {
    this.setState({ showLoader: true });

    setFieldValue("sendCQT", e.target.checked);
    setFieldValue("sendGenpact", false);
    setFieldValue("sendDefault", false);

    if (e.target.checked) {
      API.get("/api/drl/")
        .then((res) => {
          this.setState({
            sendCQT: true,
            sendGenpact: false,
            sendDefault: false,
            showLoader: false,
            CQT_customer: res.data.data.customer,
            CQT_product: res.data.data.product,
            CQT_query: res.data.data.query,
            CQT_country: res.data.data.country,
            CQT_unit: res.data.data.unit,
            CQT_spoc: res.data.data.spoc,
          });
        })
        .catch((error) => {
          console.log("=========>", error);
          if (error.data.status === "3") {
            var token_rm = 2;
            showErrorMessageFront(error, token_rm, this.props);
          } else {
            console.log("Error", error);
          }
        });
    } else {
      this.setState({
        sendCQT: false,
        sendGenpact: false,
        sendDefault: true,
        showLoader: false,
      });
    }
  };

  handleGenpact = (e, setFieldValue) => {
    setFieldValue("sendGenpact", e.target.checked);
    setFieldValue("sendCQT", false);
    setFieldValue("sendDefault", false);

    this.setState({
      sendGenpact: e.target.checked,
      sendDefault: false,
      sendCQT: false,
      showGenpactLoader: false,
      selected_genpact_option: "",
    });

    if (this.state.clone_sub !== 1 && this.state.clone_main_task !== 1) {

      API.get(`/api/feed/task_details/${this.state.currRow.task_id}`)
        .then((res_c) => {
          this.setState({
            prev_files: res_c.data.task_attachments
          });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  getNewDynamicFormData = (old_dynamic_form) => {
    const { defaultValue_genpact, sapOrderReason_arr, bm_arr } = this.state;
    //console.log(defaultValue_genpact);
    let new_dynamic_form = old_dynamic_form;
    for (let index = 0; index < old_dynamic_form.length; index++) {
      const element = old_dynamic_form[index];
      if (element.field_type === "text") {
        if (element.field_name == "quantity" && defaultValue_genpact.quantity) {
          new_dynamic_form[index].selected_value = defaultValue_genpact.quantity;
        }
        if (element.field_name == "po_number") {
          new_dynamic_form[index].selected_value = defaultValue_genpact.po_no
        }
        if (element.field_name == "product_name") {
          new_dynamic_form[index].selected_value = defaultValue_genpact.product_name
        }
        if (element.field_name == "customer_name") {
          //new_dynamic_form[index].selected_value = `${defaultValue_genpact.first_name} ${defaultValue_genpact.last_name}`
          new_dynamic_form[index].selected_value = `${defaultValue_genpact.company_name}`
        }
        if (element.field_name == "buyer_sold" && defaultValue_genpact.company_name) {
          let sold_label = " - ";
          if (defaultValue_genpact.street != '' && defaultValue_genpact.street != null) {
            sold_label += `${defaultValue_genpact.street},`;
          }
          if (defaultValue_genpact.city != '' && defaultValue_genpact.city != null) {
            sold_label += `${defaultValue_genpact.city},`;
          }
          if (defaultValue_genpact.country != '' && defaultValue_genpact.country != null) {
            sold_label += `${defaultValue_genpact.country},`;
          }
          if (defaultValue_genpact.postalcode != '' && defaultValue_genpact.postalcode != null) {
            sold_label += defaultValue_genpact.postalcode
          }
          sold_label += ")";
          new_dynamic_form[index].selected_value = (defaultValue_genpact.sap_ref_no) ? `${defaultValue_genpact.sap_ref_no} (${defaultValue_genpact.company_name}${sold_label})` : `${defaultValue_genpact.company_name}${sold_label}`;
        }
        if (element.field_name == "buyer_cust_code" && defaultValue_genpact.company_name) {
          let sold_label = " - ";
          if (defaultValue_genpact.street != '' && defaultValue_genpact.street != null) {
            sold_label += `${defaultValue_genpact.street},`;
          }
          if (defaultValue_genpact.city != '' && defaultValue_genpact.city != null) {
            sold_label += `${defaultValue_genpact.city},`;
          }
          if (defaultValue_genpact.country != '' && defaultValue_genpact.country != null) {
            sold_label += `${defaultValue_genpact.country},`;
          }
          if (defaultValue_genpact.postalcode != '' && defaultValue_genpact.postalcode != null) {
            sold_label += defaultValue_genpact.postalcode
          }
          sold_label += ")";
          new_dynamic_form[index].selected_value = (defaultValue_genpact.sap_ref_no) ? `${defaultValue_genpact.sap_ref_no} (${defaultValue_genpact.company_name}${sold_label})` : defaultValue_genpact.company_name;
        }
        if (element.field_name == "product_code" && defaultValue_genpact.material_code) {
          new_dynamic_form[index].selected_value = defaultValue_genpact.material_code;
        }
      } else if (element.field_type === "dropdown") {
        if (element.field_name == "order_reason" && defaultValue_genpact.order_reason) {
          new_dynamic_form[index].selected_value = this.getDataByValue(defaultValue_genpact.order_reason, sapOrderReason_arr);
        }

        // if(element.field_name == "plant_code" && this.state.initial_plant_code.length > 0){
        //   new_dynamic_form[index].values = this.state.initial_plant_code;
        // }

        if (element.field_name == "bm_name" && defaultValue_genpact.assigned_to > 0) {
          new_dynamic_form[index].selected_value = this.getDataByValue(`${defaultValue_genpact.emp_first_name} ${defaultValue_genpact.emp_last_name} (${defaultValue_genpact.employee_ref})`, bm_arr);
        }

        if (element.field_name == "ship_to" && this.state.initial_sapShipTo_arr.length > 0) {
          new_dynamic_form[index].values = this.state.initial_sapShipTo_arr;
        }

      } else if (element.field_type === "date") {
        if (element.field_name === 'date_dispatch') {
          new_dynamic_form[index].selected_value = new Date(defaultValue_genpact.rdd)
        } else if (element.field_name === 'po_date' && defaultValue_genpact.po_delivery_date != null && defaultValue_genpact.po_delivery_date != '') {
          new_dynamic_form[index].selected_value = new Date(defaultValue_genpact.po_delivery_date)
        } else {
          new_dynamic_form[index].selected_value = '';
        }
      }
    }
    return new_dynamic_form;
  }

  converToValidDate = (invalidDate, seperator) => {
    const [day, month, year] = invalidDate.split(seperator);
    return new Date(year, month - 1, day);
  }

  getDataByValue = (selectedValue, dataArr) => {
    const selected = dataArr.filter((details) => details.label === selectedValue);
    if (selected && selected.length > 0) {
      return selected[0];
    }
    return '';
  }

  // Fill All Options (Select DropDown)
  fillAllOptionsArr = () => {
    API.get(`/api/feed/all_bm`)
      .then((res) => {
        this.setState({
          bm_arr: res.data.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
    API.get(`/api/feed/get_sap_regions`)
      .then((res) => {
        this.setState({
          region_arr: res.data.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
    API.get(`/api/feed/get_sap_incoterms`)
      .then((res) => {
        let temp_data = res.data.data;
        temp_data.push({ 'value': "Others", "label": "Others" });
        this.setState({
          sapIncoterms_arr: temp_data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
    API.get(`/api/feed/get_sap_countries`)
      .then((res) => {
        let temp_data = res.data.data;
        temp_data.push({ 'value': "Others", "label": "Others" });
        temp_data.push({ 'value': "ROW", "label": "ROW" });
        this.setState({
          sapCountry_arr: temp_data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
    API.get(`/api/feed/get_sap_ord_reason`)
      .then((res) => {
        this.setState({
          sapOrderReason_arr: res.data.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
    API.get(`/api/feed/get_sap_shipment`)
      .then((res) => {
        let temp_data = res.data.data;
        temp_data.push({ 'value': "Others", "label": "Others" });
        this.setState({
          sapShipment_arr: temp_data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
    API.get(`/api/feed/get_sap_org_docs`)
      .then((res) => {
        let temp_data = res.data.data;
        temp_data.push({ 'value': "Others", "label": "Others" });
        this.setState({
          sapOrgDocs_arr: temp_data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
    API.get(`/api/feed/get_sap_Product_code`)
      .then((res) => {
        this.setState({
          sapProdCode_arr: res.data.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
    API.get(`/api/feed/get_sap_payment_terms`)
      .then((res) => {
        let temp_data = res.data.data;
        temp_data.push({ 'value': "Others", "label": "Others" });
        this.setState({
          sapPaymentTerms_arr: temp_data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
    API.get(`/api/feed/get_sap_currency`)
      .then((res) => {
        let temp_data = res.data.data;
        temp_data.push({ 'value': "Others", "label": "Others" });
        this.setState({
          sapCurrency_arr: temp_data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
    API.get(`/api/feed/get_sap_pharmacopeial`)
      .then((res) => {
        let temp_data = res.data.data;
        temp_data.push({ 'value': "Others", "label": "Others" });
        this.setState({
          sapPharmaRef_arr: temp_data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
    API.get(`/api/feed/get_sap_plant_code`)
      .then((res) => {
        this.setState({
          sapPlantCode_arr: res.data.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  fetchSalesOrder = (e) => {
    //console.log("this is state : ",this.state.currRow);
    this.setState({
      selected_genpact_option: e,
      dynamic_form: [],
      regionArr: [],
    }, () => {
      if (e && e.value != '') {
        API.get(`/api/feed/get_dynamic_form/${e.value}`)
          .then((dynamicFormResponse) => {

            // Fill All Options (Select DropDown)
            this.fillAllOptionsArr();

            API.get(`/api/feed/task_details_genpact/${this.state.currRow.task_id}`)
              .then((genpactResponse) => {
                //console.log("this is resp : ", genpactResponse.data.data);  

                if (genpactResponse.data && genpactResponse.data.data && genpactResponse.data.data.length > 0) {

                  if (genpactResponse.data.data[0].sap_ref_no != null && genpactResponse.data.data[0].sap_ref_no != '') {
                    API.get(`/api/feed/get_sap_shipto/${genpactResponse.data.data[0].sap_ref_no}`)
                      .then((res2) => {

                        let sapShipTo_arr = [];
                        if (res2.data.data.length > 0) {
                          sapShipTo_arr = res2.data.data;
                        }

                        this.setState({
                          initial_sapShipTo_arr: sapShipTo_arr,
                        }, () => {


                          // API.get(`/api/feed/get_sap_plant_code/${genpactResponse.data.data[0].material_code}`)
                          // .then((res3) => {
                          //   let plant_code = [];
                          //   if(res3.data.data.length>0){
                          //     plant_code = res3.data.data;
                          //   }

                          //   this.setState({
                          //     initial_plant_code:plant_code
                          //   },()=>{
                          this.setState({
                            defaultValue_genpact: genpactResponse.data.data[0],
                          }, () => {
                            // console.log(dynamicFormResponse.data.data);
                            const new_dynamic_form = this.getNewDynamicFormData(dynamicFormResponse.data.data);
                            //console.log({new_dynamic_form});
                            this.setState({
                              dynamic_form: new_dynamic_form,
                              selected_genpact_option: e,
                            });
                          });
                          //   });
                          // })
                          // .catch((err) => {
                          //   console.log(err);
                          // });
                        });
                      })
                      .catch((err) => {
                        console.log(err);
                      });
                  } else {
                    this.setState({
                      initial_sapShipTo_arr: [],
                    }, () => {


                      // API.get(`/api/feed/get_sap_plant_code/${genpactResponse.data.data[0].material_code}`)
                      // .then((res3) => {
                      //   let plant_code = [];
                      //   if(res3.data.data.length>0){
                      //     plant_code = res3.data.data;
                      //   }

                      //   this.setState({
                      //     initial_plant_code:plant_code
                      //   },()=>{
                      this.setState({
                        defaultValue_genpact: genpactResponse.data.data[0],
                      }, () => {
                        // console.log(dynamicFormResponse.data.data);
                        // console.log(this.state.defaultValue_genpact);
                        const new_dynamic_form = this.getNewDynamicFormData(dynamicFormResponse.data.data);
                        //console.log({new_dynamic_form});
                        this.setState({
                          dynamic_form: new_dynamic_form,
                          selected_genpact_option: e,
                        });
                      });
                      //   });
                      // })
                      // .catch((err) => {
                      //   console.log(err);
                      // });
                    });
                  }
                } else {
                  this.setState({
                    dynamic_form: dynamicFormResponse.data.data,
                    selected_genpact_option: e,
                  });
                }
              })
              .catch((err) => {
                console.log(err);
              });


          })
          .catch((error) => {
            //console.log("=========>", error);
            if (error.data.status === "3") {
              var token_rm = 2;
              showErrorMessageFront(error, token_rm, this.props);
            } else {
              console.log("Error", error);
            }
          });
      }
    });
  }

  handleDefault = (e, setFieldValue) => {
    setFieldValue("sendDefault", e.target.checked);
    setFieldValue("sendGenpact", false);
    setFieldValue("sendCQT", false);
    this.setState({
      sendGenpact: false,
      sendDefault: e.target.checked,
      sendCQT: false,
      showGenpactLoader: false,
    });
  };

  handleDisplayDiscussion = (e, setFieldValue) => {
    setFieldValue("display_discussion", e.target.checked);
  };

  getSuggestionValue = (suggestion, setFieldValue) => {
    setFieldValue("cqt_reference_id", suggestion.id);
    this.setState({ cqt_reference_id: suggestion.id });
    return suggestion.title;
  };

  renderSuggestion = (suggestion) => {
    return <div>{suggestion.title}</div>;
  };

  changeManager = (event, setFieldValue) => {
    if (event === null) {
      setFieldValue("employee_id", "");
      this.setState({ stateEmplId: "" });
    } else {
      var post_date = {
        assigned_to: event.value,
        customer_id: this.state.currRow.customer_id,
      };
      API.post(`/api/tasks/check_leave`, post_date)
        .then((res) => {
          if (res.data.status === 2) {
            this.setState({
              new_employee: true,
              msg_new_employee: res.data.msg_new_employee,
            });
          } else {
            this.setState({
              new_employee: false,
              msg_new_employee: "",
            });
          }
        })
        .catch((error) => { });

      setFieldValue("employee_id", event);
      this.setState({ stateEmplId: event });
    }
  };

  changePostArr = async (event, index, key_name) => {
    let prev_arr = this.state.post_arr;
    if (key_name === 'employee_id') {

      if (event === null) {
        //prev_arr[index].
        prev_arr[index].employee_id = '';
        prev_arr[index].new_employee = '';
      } else {
        var post_date = {
          assigned_to: event.value,
          customer_id: this.state.currRow.customer_id,
        };
        // let res = await API.post(`/api/tasks/check_leave`, post_date)
        //   .then((res) => {
        //     if (res.data.status === 2) {
        //       prev_arr[index].new_employee = res.data.msg_new_employee;
        //     } else {
        //       prev_arr[index].new_employee = '';
        //     }
        //   })
        //   .catch((error) => {});

        let res = await API.post(`/api/tasks/check_leave`, post_date);

        if (res.data.status === 2) {
          prev_arr[index].new_employee = res.data.msg_new_employee;
        } else {
          prev_arr[index].new_employee = '';
        }

        prev_arr[index].employee_id = event;
        if (prev_arr[index].error_msg.employee_id !== '') {
          prev_arr[index].error_msg.employee_id = '';
        }
      }

    } else if (key_name === 'due_date') {
      this.setState({ showLoader: true });
      var post_date = {};

      let post_due_date = `${dateFormat(event, "yyyy-mm-dd HH:MM:ss")}`;

      if (this.state.currRow.parent_id > 0) {
        post_date = { due_date: post_due_date, sub_task: 1, auto: 0 };
      } else {
        post_date = { due_date: post_due_date, auto: 0 };
      }

      API.post(`/api/tasks/sla_breach/${this.state.currRow.task_id}`, post_date)
        .then((res) => {
          //console.log(res.data);
          if (res.data.status === 1) {
            prev_arr[index].breached = true;
            this.setState({ showLoader: false });
          } else {
            prev_arr[index].breached = false;
            this.setState({ showLoader: false });
          }
        })
        .catch((error) => { });
      prev_arr[index].due_date = event;
      if (prev_arr[index].error_msg.due_date !== '') {
        prev_arr[index].error_msg.due_date = '';
      }
    } else if (key_name === 'breach_reason') {
      prev_arr[index].breach_reason = event.target.value;
      if (prev_arr[index].error_msg.breach_reason !== '') {
        prev_arr[index].error_msg.breach_reason = '';
      }
    } else if (key_name === 'display_discussion') {
      prev_arr[index].display_discussion = event.target.checked;
    }



    this.setState({ post_arr: prev_arr });

  }

  getDynamicHtml = () => {
    let html = [];
    let prev_post_arr = this.state.post_arr;

    for (let index = 0; index < prev_post_arr.length; index++) {

      html.push(
        <>

          <Col xs={12} sm={index > 0 ? 5 : 6} md={index > 0 ? 5 : 6}>
            <div className="form-group">
              <label>
                Employee <span className="required-field">*</span>
              </label>
              <Select
                className="basic-single"
                classNamePrefix="select"
                defaultValue={prev_post_arr[index].employee_id}
                value={prev_post_arr[index].employee_id}
                isClearable={true}
                isSearchable={true}
                name="employee_id"
                options={this.state.employeeArr}
                onChange={(e) => {
                  this.changePostArr(e, index, 'employee_id');
                }}
              />

              {prev_post_arr[index].error_msg.employee_id && prev_post_arr[index].error_msg.employee_id != '' ? (
                <span className="errorMsg">
                  {prev_post_arr[index].error_msg.employee_id}
                </span>
              ) : null}

              {prev_post_arr[index].new_employee && prev_post_arr[index].new_employee != '' ? (
                <span style={{ color: "#ffae42" }}>
                  {prev_post_arr[index].new_employee}
                </span>
              ) : null}

            </div>
          </Col>

          <Col xs={12} sm={index > 0 ? 5 : 6} md={index > 0 ? 5 : 6}>
            <div className="form-group react-date-picker sub-task-area">
              <label>
                Due Date{" "}
                {Date.parse(new Date(this.state.currRow.due_date)) >
                  Date.parse(
                    new Date(this.state.currRow.original_due_date)
                  ) && (
                    <i style={{ "fontSize": "12px" }}>
                      (Original Due Date:{" "}
                      {dateFormat(
                        this.state.currRow.original_due_date,
                        "dd/mm/yyyy h:MM TT"
                      )}
                      )
                    </i>
                  )}
              </label>
              <div className="form-control">
                <DatePicker
                  name={"dueDate"}
                  className="borderNone"
                  minDate={new Date()}
                  selected={prev_post_arr[index].due_date}
                  onChange={(e) => {
                    this.changePostArr(e, index, 'due_date');
                  }}
                  dateFormat="dd/MM/yyyy"
                  autoComplete="off"
                />
              </div>
              {prev_post_arr[index].breached && (
                <p className="sub-task-er-msg sub-task-er-msg2">
                  You are going to breach the SLA
                </p>
              )}
              {prev_post_arr[index].error_msg.due_date && prev_post_arr[index].error_msg.due_date != '' ? (
                <span className="errorMsg">
                  {prev_post_arr[index].error_msg.due_date}
                </span>
              ) : null}
            </div>
          </Col>
          {index > 0 && <Col xs={12} sm={2} md={2} className="removeSection">
            <div className="form-group">
              <label>
                <button
                  className={`btn-fill btn-custom-green m-r-10 removeBtn`}
                  type="button"
                  onClick={() => this.removePostArr(index)}
                >
                  Remove
                </button>
              </label>
            </div>
          </Col>}
          <Col xs={12} sm={12} md={12}>
            <div className="form-group checText">
              <Field
                type="checkbox"
                name="display_discussion"
                value="1"
                onChange={(e) => {
                  this.changePostArr(e, index, 'display_discussion');
                }}
                defaultChecked={prev_post_arr[index].display_discussion}
              />
              <label>
                Display Customer Discussions
                <LinkWithTooltip
                  tooltip={`This will allow customer discussion to be visible along with this subtask`}
                  href="#"
                  id="tooltip-1"
                  clicked={(e) => this.checkHandler(e)}
                >
                  <i
                    className="fa fa-exclamation-circle"
                    aria-hidden="true"
                  ></i>
                </LinkWithTooltip>
              </label>
            </div>
          </Col>
          {prev_post_arr[index].breached && <Col xs={12} sm={12} md={12}>
            <div className="form-group">
              <label>Please Provide Reason<span className="required-field">*</span></label>
              <Field
                name="breach_reason"
                component="textarea"
                className={`selectArowGray form-control`}
                autoComplete="off"
                onChange={(e) => {
                  this.changePostArr(e, index, 'breach_reason');
                }}
                value={prev_post_arr[index].breach_reason}
              />
              {prev_post_arr[index].error_msg.breach_reason && prev_post_arr[index].error_msg.breach_reason != '' ? (
                <span className="errorMsg">
                  {prev_post_arr[index].error_msg.breach_reason}
                </span>
              ) : null}
            </div>
          </Col>}



        </>
      );

    }

    html.push(<Col xs={12} sm={12} md={12}>
      <div className="form-group">
        <label>
          <button
            className={`btn-fill btn-custom-green m-r-10`}
            type="button"
            onClick={() => {
              let prev_state = this.state.post_arr;
              prev_state.push({
                breach_reason: '',
                due_date: localDate(this.state.currRow.due_date),
                employee_id: '',
                new_employee: '',
                display_discussion: false,
                breached: this.state.sla_breach,
                error_msg: {
                  employee_id: '',
                  due_date: '',
                  breach_reason: ''
                },
                errors: 0
              });
              this.setState({ post_arr: prev_state });
            }}
          >
            Add More
          </button>
        </label>
      </div>
    </Col>)

    return html;

  }

  genpactDynamicHTML = () => {
    let html = [];
    let parent_genpact_tasks = this.state.parent_genpact_tasks;

    for (let index = 0; index < parent_genpact_tasks.length; index++) {

      html.push(
        <>
          <Row>
            <Col xs={12} sm={3} md={3}>
              <div className="form-group react-date-picker sub-task-area">
                <label>
                  {parent_genpact_tasks[index].task_ref}
                </label>
                <Field
                  type="checkbox"
                  name="checked"
                  checked={
                    parent_genpact_tasks[index].checked
                  }
                  value={1}
                  onChange={(e) => {
                    this.changeGenpactArr(e, index, 'checked');
                  }}
                />
              </div>
            </Col>
            <Col xs={12} sm={3} md={3}>
              <div className="form-group react-date-picker sub-task-area">
                <label>
                  RDD
                </label>
                <div className="form-control">
                  <DatePicker
                    name={"dueDate"}
                    className="borderNone"
                    selected={new Date(parent_genpact_tasks[index].rdd)}
                    onChange={(e) => {
                      this.changeGenpactArr(e, index, 'rdd');
                    }}
                    dateFormat="dd/MM/yyyy"
                    autoComplete="off"
                  />
                </div>
              </div>
            </Col>
            <Col xs={12} sm={3} md={3}>
              <div className="form-group react-date-picker sub-task-area">
                <label>
                  Quantity
                </label>
                <Field
                  name="quantity"
                  type="text"
                  className={`selectArowGray form-control`}
                  autoComplete="off"
                  value={parent_genpact_tasks[index].quantity}
                  onChange={(e) => {
                    this.changeGenpactArr(e, index, 'quantity');
                  }}
                ></Field>
              </div>
            </Col>
            <Col xs={12} sm={3} md={3}>
              <div className="form-group react-date-picker sub-task-area">
                <label>
                  Unit
                </label>
                <Field
                  name="units"
                  component="select"
                  className={`selectArowGray form-control`}
                  autoComplete="off"
                  value={parent_genpact_tasks[index].units}
                  onChange={(e) => {
                    this.changeGenpactArr(e, index, 'units');
                  }}
                >
                  <option key="-1" value="">
                    Select
                  </option>
                  <option key="1" value="Mgs">
                    Mgs
                  </option>

                  <option key="2" value="Gms">
                    Gms
                  </option>

                  <option key="3" value="Kgs">
                    Kgs
                  </option>
                  <option key="8" value="Vials">
                    Vials
                  </option>
                </Field>
              </div>
            </Col>
            {parent_genpact_tasks[index].errors != '' && <Col xs={12} sm={12} md={12}>
              <div className="form-group react-date-picker sub-task-area">
                <p className="sub-task-er-msg sub-task-er-msg1">
                  {parent_genpact_tasks[index].errors}
                </p>
              </div>
            </Col>}
          </Row>
        </>
      );

    }

    return html;
  }

  changeGenpactArr = async (event, index, key) => {
    let prev_state = this.state.parent_genpact_tasks;

    if (key === "checked") {
      prev_state[index].checked = event.target.checked;
    }

    if (key === "rdd") {
      prev_state[index].rdd = event;
    }

    if (key === "quantity") {
      prev_state[index].quantity = event.target.value;
    }

    if (key === "units") {
      prev_state[index].units = event.target.value;
    }

    this.setState({ parent_genpact_tasks: prev_state });

  }

  removePostArr = (index) => {
    let prev_state = this.state.post_arr;
    let new_arr = [];
    for (let index_new = 0; index_new < prev_state.length; index_new++) {
      if (index !== index_new) {
        new_arr.push(prev_state[index_new]);
      }
    }
    this.setState({ post_arr: new_arr });
  }


  changeCQTPostArr = async (event, index, key_name) => {
    let prev_arr = this.state.cqt_post_arr;
    if (key_name === 'cqt_spoc_id') {

      if (event === null) {
        //prev_arr[index].
        prev_arr[index].cqt_spoc_id = '';
      } else {
        prev_arr[index].cqt_spoc_id = event;
        if (prev_arr[index].error_msg.cqt_spoc_id !== '') {
          prev_arr[index].error_msg.cqt_spoc_id = '';
        }
      }

    } else if (key_name === 'dueDate') {
      if (event === null) {
        prev_arr[index].dueDate = this.getDueDate();
        prev_arr[index].breached = false;
      } else {
        this.setState({ showLoader: true });
        var post_date = {};

        let post_due_date = `${dateFormat(event, "yyyy-mm-dd HH:MM:ss")}`;

        if (this.state.currRow.parent_id > 0) {
          post_date = { due_date: post_due_date, sub_task: 1, auto: 0 };
        } else {
          post_date = { due_date: post_due_date, auto: 0 };
        }

        API.post(`/api/tasks/sla_breach/${this.state.currRow.task_id}`, post_date)
          .then((res) => {
            //console.log(res.data);
            if (res.data.status === 1) {
              prev_arr[index].breached = true;
              this.setState({ showLoader: false });
            } else {
              prev_arr[index].breached = false;
              this.setState({ showLoader: false });
            }
          })
          .catch((error) => { });
        prev_arr[index].dueDate = event;
        if (prev_arr[index].error_msg.dueDate !== '') {
          prev_arr[index].error_msg.dueDate = '';
        }
      }
    } else if (key_name === 'breach_reason') {
      prev_arr[index].breach_reason = event.target.value;
      if (prev_arr[index].error_msg.breach_reason !== '') {
        prev_arr[index].error_msg.breach_reason = '';
      }
    }

    // console.log(prev_arr)

    this.setState({ cqt_post_arr: prev_arr });

  }

  getDynamicCQTHtml = () => {
    let html = [];
    let prev_post_arr = this.state.cqt_post_arr;  //console.log(this.state.cqt_post_arr);

    for (let index = 0; index < prev_post_arr.length; index++) {
      html.push(
        <>
          <Col xs={12} sm={6} md={6}>
            <div className="form-group">
              <label>Assign To</label>
              <Select
                className="basic-single"
                classNamePrefix="select"
                defaultValue={prev_post_arr[index].cqt_spoc_id}
                name="cqt_spoc_id"
                options={this.state.CQT_spoc}
                onChange={(e) =>
                  //setFieldValue("cqt_spoc_id", value)

                  this.changeCQTPostArr(e, index, 'cqt_spoc_id')
                }
              />

              {prev_post_arr[index].error_msg.cqt_spoc_id && prev_post_arr[index].error_msg.cqt_spoc_id ? (
                <span className="errorMsg">
                  {prev_post_arr[index].error_msg.cqt_spoc_id}
                </span>
              ) : null}
            </div>
          </Col>

          <Col xs={12} sm={4} md={4}>
            <div className="form-group react-date-picker sub-task-area">
              <label>
                Due Date{" "}
                {Date.parse(new Date(this.state.currRow.due_date)) >
                  Date.parse(
                    new Date(this.state.currRow.original_due_date)
                  ) && (
                    <i style={{ "fontSize": "12px" }}>
                      (Original Due Date:{" "}
                      {dateFormat(
                        this.state.currRow.original_due_date,
                        "dd/mm/yyyy h:MM TT"
                      )}
                      )
                    </i>
                  )}
              </label>
              <div className="form-control">
                <DatePicker
                  name={"dueDate"}
                  className="borderNone"
                  minDate={new Date()}
                  selected={prev_post_arr[index].dueDate ? prev_post_arr[index].dueDate : null}
                  onChange={(e) => {
                    //this.changeDate(e, setFieldValue);

                    this.changeCQTPostArr(e, index, 'dueDate')
                  }}
                  dateFormat="dd/MM/yyyy"
                  autoComplete="off"
                />
              </div>
              {prev_post_arr[index].breached && (
                <p className="sub-task-er-msg sub-task-er-msg2">
                  You are going to breach the SLA
                </p>
              )}
              {prev_post_arr[index].error_msg.dueDate && prev_post_arr[index].error_msg.dueDate ? (
                <p className="sub-task-er-msg sub-task-er-msg1">
                  {prev_post_arr[index].error_msg.dueDate}
                </p>
              ) : null}
            </div>
          </Col>
          {index > 0 && <Col xs={12} sm={2} md={2} className="removeSection">
            <div className="form-group">
              <label>
                <button
                  className={`btn-fill btn-custom-green m-r-10 removeBtn`}
                  type="button"
                  onClick={() => this.removeCQTPostArr(index)}
                >
                  Remove
                </button>
              </label>
            </div>
          </Col>}

          {prev_post_arr[index].breached && (
            <Col xs={12} sm={12} md={12}>
              <div className="form-group">
                <label>Please Provide Reason</label>
                <Field
                  name="breach_reason"
                  component="textarea"
                  className={`selectArowGray form-control`}
                  autoComplete="off"
                  onChange={(e) => {
                    this.changeCQTPostArr(e, index, 'breach_reason');
                  }}
                  value={prev_post_arr[index].breach_reason}
                />
                {prev_post_arr[index].error_msg.breach_reason && prev_post_arr[index].error_msg.breach_reason ? (
                  <span className="errorMsg">
                    {prev_post_arr[index].error_msg.breach_reason}
                  </span>
                ) : null}
              </div>
            </Col>
          )}
        </>
      );
    }

    html.push(<Col xs={12} sm={12} md={12}>
      <div className="form-group">
        <label>
          <button
            className={`btn-fill btn-custom-green m-r-10`}
            type="button"
            onClick={() => {
              let prev_state = this.state.cqt_post_arr;
              prev_state.push({
                breach_reason: '',
                dueDate: localDate(this.state.currRow.due_date),
                cqt_spoc_id: '',
                breached: this.state.sla_breach,
                error_msg: {
                  cqt_spoc_id: '',
                  dueDate: '',
                  breach_reason: ''
                },
                errors: 0
              });
              this.setState({ cqt_post_arr: prev_state });
            }}
          >
            Add More
          </button>
        </label>
      </div>
    </Col>)
    return html;
  }

  removeCQTPostArr = (index) => {
    let prev_state = this.state.cqt_post_arr;
    let new_arr = [];
    for (let index_new = 0; index_new < prev_state.length; index_new++) {
      if (index !== index_new) {
        new_arr.push(prev_state[index_new]);
      }
    }
    this.setState({ cqt_post_arr: new_arr });
  }


  loadDefaultForm = () => {
    const validateStopFlag = Yup.object().shape({
      title: Yup.string().trim()
        .required("Please enter title")
        .min(2, `Minimum 2 characters long.`)
        .max(100, `Maximum 100 characters are allowed.`),

      content: Yup.string().trim().required("Please give description"),

      sendCQT: Yup.bool(),

      cqt_customer_id: Yup.string().trim().when("sendCQT", {
        is: true,
        then: Yup.string().trim().required("Please select customer"),
      }),

      cqt_country_id: Yup.string().trim().when("sendCQT", {
        is: true,
        then: Yup.string().trim().required("Please select country"),
      }),

      cqt_product_id: Yup.string().trim().when("sendCQT", {
        is: true,
        then: Yup.string().trim().required("Please select product"),
      }),

      cqt_query_id: Yup.string().trim().when("sendCQT", {
        is: true,
        then: Yup.string().trim().required("Please select query category"),
      }),

      cqt_criticality_value: Yup.string().trim().when("sendCQT", {
        is: true,
        then: Yup.string().trim().required("Please select criticality"),
      }),

      cqt_unit_id: Yup.string().trim().when("sendCQT", {
        is: true,
        then: Yup.string().trim().required("Please select unit name"),
      }),

      // cqt_spoc_id: Yup.string().when("sendCQT", {
      //   is: true,
      //   then: Yup.string().required("Please select assigned to"),
      // }),
    });

    // ==== Autosuggest for Reference Query DRL ==== //
    const { value, suggestions, ref_loader } = this.state;

    const inputProps = {
      placeholder: "Reference query min 3 characters",
      value,
      type: "search",
      onChange: this.onReferenceChange,
    };

    const request_type = this.state.currRow.request_type;

    return (
      <Modal
        show={this.state.showCreateSubTask}
        onHide={() => this.handleClose()}
        backdrop="static"
      >
        <Formik
          initialValues={initialValues}
          validationSchema={validateStopFlag}
          onSubmit={this.handleSubmitCreateTask}
        >
          {({
            values,
            errors,
            touched,
            isValid,
            isSubmitting,
            setFieldValue,
            setFieldTouched,
            setErrors,
          }) => {
            //console.log("errors", errors);
            //console.log("values", values);

            return (
              <Form encType="multipart/form-data">
                {this.state.showLoader === true ? (
                  <div className="loderOuter">
                    <div className="loader">
                      <img src={loaderlogo} alt="logo" />
                      <div className="loading">Loading...</div>
                    </div>
                  </div>
                ) : (
                  ""
                )}

                <Modal.Header closeButton>
                  <Modal.Title>Create Sub Task</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <div className="contBox boxflex contBoxMaxHeight">
                    <div className="form-group checText">
                      <label>
                        Select type of sub task you would like to create
                      </label>
                    </div>
                    <Row>
                      <Col xs={12} sm={4} md={4}>
                        <div className="form-group checText">
                          <input
                            type="radio"
                            name="sendDefault"
                            value="1"
                            onChange={(e) =>
                              this.handleDefault(e, setFieldValue)
                            }
                            checked={
                              (values.sendDefault || this.state.sendDefault) ===
                                true
                                ? "checked"
                                : this.state.sendDefault
                            }
                          />
                          <label>
                            XCEED Sub Task{" "}
                            <LinkWithTooltip
                              tooltip={`This task shall be created in XCEED`}
                              href="#"
                              id="tooltip-1"
                              clicked={(e) => this.checkHandler(e)}
                            >
                              <i
                                className="fa fa-exclamation-circle"
                                aria-hidden="true"
                              ></i>
                            </LinkWithTooltip>
                          </label>
                        </div>
                      </Col>

                      <Col xs={12} sm={4} md={4}>
                        <div className="form-group checText">
                          <input
                            type="radio"
                            name="sendCQT"
                            value={false}
                            onChange={(e) =>
                              this.handleCQTbox(e, setFieldValue)
                            }
                            checked={values.sendCQT}
                          />
                          <label>
                            CQT Sub Task{" "}
                            <LinkWithTooltip
                              tooltip={`This task shall be created in CQT`}
                              href="#"
                              id="tooltip-1"
                              clicked={(e) => this.checkHandler(e)}
                            >
                              <i
                                className="fa fa-exclamation-circle"
                                aria-hidden="true"
                              ></i>
                            </LinkWithTooltip>
                          </label>
                        </div>
                      </Col>
                      {/* Monosom */}
                      {request_type === 23 && (
                        <Col xs={12} sm={4} md={4}>
                          <div className="form-group checText">
                            <input
                              type="radio"
                              name="sendGenpact"
                              value="1"
                              onChange={(e) =>
                                this.handleGenpact(e, setFieldValue)
                              }
                              checked={
                                (values.sendGenpact ||
                                  this.state.sendGenpact) === true
                                  ? "checked"
                                  : ""
                              }
                            />
                            <label>
                              Genpact Sub Task{" "}
                              <LinkWithTooltip
                                tooltip={`This task will be created for Genpact`}
                                href="#"
                                id="tooltip-1"
                              >
                                <i
                                  className="fa fa-exclamation-circle"
                                  aria-hidden="true"
                                ></i>
                              </LinkWithTooltip>
                            </label>
                          </div>
                        </Col>
                      )}
                      {/* <Col xs={12} sm={6} md={6}></Col> */}

                      {/* Monosom */}
                      {/* {request_type === 23 && <Col xs={12} sm={6} md={6}></Col>} */}
                      <Col xs={12} sm={12} md={12}>
                        <div className="form-group">
                          <label>
                            Title <span className="required-field">*</span>
                          </label>
                          <Field
                            name="title"
                            type="text"
                            className={`selectArowGray form-control`}
                            autoComplete="off"
                            value={values.title}
                          ></Field>
                          {errors.title && touched.title ? (
                            <span className="errorMsg">{errors.title}</span>
                          ) : null}
                        </div>
                      </Col>

                      {this.state.sendCQT && (
                        <>
                          <Col xs={12} sm={6} md={6}>
                            <div className="form-group">
                              <label>Username</label>
                              <Select
                                className="basic-single"
                                classNamePrefix="select"
                                defaultValue={values.cqt_customer_id}
                                name="cqt_customer_id"
                                options={this.state.CQT_customer}
                                onChange={(value) =>
                                  setFieldValue("cqt_customer_id", value)
                                }
                              />

                              {errors.cqt_customer_id &&
                                touched.cqt_customer_id ? (
                                <span className="errorMsg">
                                  {errors.cqt_customer_id}
                                </span>
                              ) : null}
                            </div>
                          </Col>

                          <Col xs={12} sm={6} md={6}>
                            <div className="form-group">
                              <label>Product</label>
                              <Select
                                className="basic-single"
                                classNamePrefix="select"
                                defaultValue={values.cqt_product_id}
                                name="cqt_product_id"
                                options={this.state.CQT_product}
                                onChange={(value) =>
                                  setFieldValue("cqt_product_id", value)
                                }
                              />

                              {errors.cqt_product_id &&
                                touched.cqt_product_id ? (
                                <span className="errorMsg">
                                  {errors.cqt_product_id}
                                </span>
                              ) : null}
                            </div>
                          </Col>

                          <Col xs={12} sm={6} md={6}>
                            <div className="form-group">
                              <label>Query Category</label>
                              <Select
                                className="basic-single"
                                classNamePrefix="select"
                                defaultValue={values.cqt_query_id}
                                name="cqt_query_id"
                                options={this.state.CQT_query}
                                onChange={(value) =>
                                  setFieldValue("cqt_query_id", value)
                                }
                              />

                              {errors.cqt_query_id && touched.cqt_query_id ? (
                                <span className="errorMsg">
                                  {errors.cqt_query_id}
                                </span>
                              ) : null}
                            </div>
                          </Col>

                          <Col xs={12} sm={6} md={6}>
                            <div className="form-group">
                              <label>Criticality</label>
                              <Select
                                className="basic-single"
                                classNamePrefix="select"
                                defaultValue={values.cqt_criticality_value}
                                name="cqt_criticality_value"
                                options={this.state.CQT_criticality}
                                onChange={(value) => {
                                  console.log(value);
                                  setFieldValue("cqt_criticality_value", value)
                                }
                                }
                              />

                              {errors.cqt_criticality_value &&
                                touched.cqt_criticality_value ? (
                                <span className="errorMsg">
                                  {errors.cqt_criticality_value}
                                </span>
                              ) : null}
                            </div>
                          </Col>

                          <Col xs={12} sm={6} md={6}>
                            <div className="form-group form-control-auto">
                              <label>Reference Query</label>

                              <Autosuggest
                                id={`reference_query`}
                                suggestions={suggestions}
                                onSuggestionsFetchRequested={
                                  this.onSuggestionsFetchRequested
                                }
                                onSuggestionsClearRequested={
                                  this.onSuggestionsClearRequested
                                }
                                getSuggestionValue={(suggestion) =>
                                  this.getSuggestionValue(
                                    suggestion,
                                    setFieldValue
                                  )
                                }
                                renderSuggestion={this.renderSuggestion}
                                inputProps={inputProps}
                              />
                              {ref_loader === true && (
                                <div className="form-loader">
                                  <div className="loader4"></div>
                                </div>
                              )}

                              {errors.cqt_reference_id &&
                                touched.cqt_reference_id ? (
                                <span className="errorMsg">
                                  {errors.cqt_reference_id}
                                </span>
                              ) : null}
                            </div>
                          </Col>

                          <Col xs={12} sm={6} md={6}>
                            <div className="form-group">
                              <label>Country</label>
                              <Select
                                className="basic-single"
                                classNamePrefix="select"
                                defaultValue={values.cqt_country_id}
                                name="cqt_country_id"
                                options={this.state.CQT_country}
                                onChange={(value) =>
                                  setFieldValue("cqt_country_id", value)
                                }
                              />

                              {errors.cqt_country_id &&
                                touched.cqt_country_id ? (
                                <span className="errorMsg">
                                  {errors.cqt_country_id}
                                </span>
                              ) : null}
                            </div>
                          </Col>

                          <Col xs={12} sm={6} md={6}>
                            <div className="form-group">
                              <label>Unit Name</label>
                              <Select
                                className="basic-single"
                                classNamePrefix="select"
                                defaultValue={values.cqt_unit_id}
                                name="cqt_unit_id"
                                options={this.state.CQT_unit}
                                onChange={(value) =>
                                  setFieldValue("cqt_unit_id", value)
                                }
                              />

                              {errors.cqt_unit_id && touched.cqt_unit_id ? (
                                <span className="errorMsg">
                                  {errors.cqt_unit_id}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                          <Col xs={12} sm={6} md={6}></Col>
                        </>
                      )}

                      {this.state.sendCQT === false ?
                        this.getDynamicHtml()
                        : this.getDynamicCQTHtml()}



                      <Col xs={12} sm={12} md={12}>
                        <div className="form-group">
                          <label>
                            Description{" "}
                            <span className="required-field">*</span>
                          </label>
                          {/* <Field
                                            name="content"
                                            component="textarea"
                                            className={`selectArowGray form-control`}
                                            autoComplete="off"
                                            value={values.content}
                                            > 
                                            </Field> */}

                          {/* <TinyMCE
                                                name="content"
                                                content={values.comment}
                                                config={{
                                                    branding: false,
                                                    toolbar: 'undo redo | bold italic | alignleft aligncenter alignright'
                                                }}
                                                className={`selectArowGray form-control`}
                                                autoComplete="off"
                                                onChange={value =>
                                                    setFieldValue("content", value.level.content)
                                                }
                                            /> */}

                          <Editor
                            name="content"
                            value={
                              values.content !== null && values.content !== ""
                                ? values.content
                                : ""
                            }
                            content={
                              values.content !== null && values.content !== ""
                                ? values.content
                                : ""
                            }
                            init={{
                              menubar: false,
                              branding: false,
                              placeholder: "Enter description",
                              plugins:
                                "link table hr visualblocks code placeholder lists autoresize textcolor",
                              toolbar:
                                "bold italic strikethrough superscript subscript | forecolor backcolor | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | visualblocks code | fontselect",
                              font_formats:
                                "Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats",
                            }}
                            onEditorChange={(value) =>
                              setFieldValue("content", value)
                            }
                          />

                          {errors.content && touched.content ? (
                            <span className="errorMsg">{errors.content}</span>
                          ) : null}
                        </div>
                      </Col>



                      <Col xs={12} sm={6} md={6}>
                        <div className="form-group custom-file-upload">
                          <label>&nbsp;</label>
                          <Dropzone
                            onDrop={(acceptedFiles) =>
                              this.setDropZoneFiles(
                                acceptedFiles,
                                setErrors,
                                setFieldValue
                              )
                            }
                            multiple={true}
                          >
                            {({ getRootProps, getInputProps }) => (
                              <section>
                                <div
                                  {...getRootProps()}
                                  className="custom-file-upload-header"
                                >
                                  <input {...getInputProps()} />
                                  <p>Upload file</p>
                                </div>
                                <div className="custom-file-upload-area">
                                  {/* {this.state.currRow.proforma_id > 0 && this.showAttachments()} */}
                                  {this.state.clone_sub === 1 &&
                                    this.showAttachmentsClone()}
                                  {this.state.clone_main_task === 1 &&
                                    this.showAttachmentsCloneMain()}
                                  {this.state.filesHtml}
                                </div>
                              </section>
                            )}
                          </Dropzone>

                          {errors.file_name || touched.file_name ? (
                            <p className="sub-task-er-msg sub-task-er-msg3">
                              {errors.file_name}
                            </p>
                          ) : null}
                        </div>
                      </Col>

                      {/* <Col xs={12} sm={3} md={3}>
                                        <div className="form-group checText">
                                            <input
                                                type="radio"
                                                name="sendDefault"
                                                value="1"
                                                onChange={(e) => this.handleDefault(e, setFieldValue)}
                                                checked={(values.sendDefault || this.state.sendDefault ) === true ? 'checked' : this.state.sendDefault}
                                            />
                                            <label>Default</label>
                                        </div>
                                        </Col>
                                                                                
                                        <Col xs={12} sm={3} md={3}>
                                        <div className="form-group checText">                                            
                                            <input
                                                type="radio"
                                                name="sendCQT"
                                                value={false}
                                                onChange={(e) => this.handleCQTbox(e, setFieldValue)}
                                                checked={values.sendCQT}
                                            />
                                            <label>Send to CQT</label>
                                        </div>
                                        </Col>
                                        
                                        {request_type === 23 &&
                                            <Col xs={12} sm={3} md={3}>
                                            <div className="form-group checText">
                                                <input
                                                    type="radio"
                                                    name="sendGenpact"
                                                    value="1"
                                                    onChange={(e) => this.handleGenpact(e, setFieldValue)}
                                                    checked={(values.sendGenpact || this.state.sendGenpact ) === true ? 'checked' : ''}
                                                />
                                                <label>Send to Genpact</label>
                                            </div>
                                            </Col>
                                        }*/}
                      {/* {this.state.sendCQT === false && (
                        <Col xs={12} sm={6} md={6}>
                          <div className="form-group checText">
                            <Field
                              type="checkbox"
                              name="display_discussion"
                              value="1"
                              onChange={(e) =>
                                this.handleDisplayDiscussion(e, setFieldValue)
                              }
                              defaultChecked={values.display_discussion}
                            />
                            <label>
                              Display Customer Discussions{" "}
                              <LinkWithTooltip
                                tooltip={`This will allow customer discussion to be visible along with this subtask`}
                                href="#"
                                id="tooltip-1"
                                clicked={(e) => this.checkHandler(e)}
                              >
                                <i
                                  className="fa fa-exclamation-circle"
                                  aria-hidden="true"
                                ></i>
                              </LinkWithTooltip>
                            </label>
                          </div>
                        </Col>
                      )} */}
                    </Row>
                  </div>
                </Modal.Body>
                <Modal.Footer>
                  <button
                    onClick={this.handleClose}
                    className={`btn-line`}
                    type="button"
                  >
                    Close
                  </button>
                  {this.state.clone_sub === 1 ? (
                    <button
                      className={`btn-fill btn-custom-green m-r-10`}
                      type="submit"
                    >
                      Submit
                    </button>
                  ) : (
                    <button
                      className={`btn-fill ${isValid ? "btn-custom-green" : "btn-disable"
                        } m-r-10`}
                      type="submit"
                      disabled={isValid ? false : true}
                    >
                      Submit
                    </button>
                  )}
                </Modal.Footer>
              </Form>
            );
          }}
        </Formik>
      </Modal>
    );
  };

  downloadFile = (e, file_url, file_name) => {
    e.preventDefault();
    let a = document.createElement("a");
    a.href = file_url;
    a.download = file_name;
    a.click();
  };

  removeProformaGenpactFile = () => {
    let prevState = this.state.currRow;
    prevState.download_url = "";
    prevState.file_name = "";
    //prevState.proforma_id = 0;
    this.setState({ currRow: prevState });
  };

  showAttachments = () => {
    var fileListHtml = (
      <Alert key={this.state.currRow.file_name}>
        <span onClick={() => this.removeProformaGenpactFile()}>
          <i className="far fa-times-circle" style={{ cursor: "pointer" }}></i>
        </span>{" "}
        <span
          onClick={(e) =>
            this.downloadFile(
              e,
              this.state.currRow.download_url,
              this.state.currRow.file_name
            )
          }
        >
          <i className="fa fa-download" style={{ cursor: "pointer" }}></i>
        </span>{" "}
        {this.state.currRow.file_name.length > 50
          ? `${this.state.currRow.file_name.substr(0, 40)}...`
          : this.state.currRow.file_name}
      </Alert>
    );

    return fileListHtml;
  };

  removeProformaGenpactFileClone = (tcu_id) => {
    let prevState = this.state.clonePrevFiles;
    let newState = [];
    for (let index = 0; index < prevState.length; index++) {
      const element = prevState[index];
      if (tcu_id !== element.tcu_id) {
        newState.push(element);
      }
    }

    this.setState({ clonePrevFiles: newState });
  };

  removeCloneMainFiles = (upload_id) => {
    let prevState = this.state.clonePrevFiles;
    let newState = [];
    for (let index = 0; index < prevState.length; index++) {
      const element = prevState[index];
      if (upload_id !== element.upload_id) {
        newState.push(element);
      }
    }

    this.setState({ clonePrevFiles: newState });
  };

  redirectUrlTask = (event, path) => {
    event.preventDefault();
    window.open(path, "_self");
  };

  showAttachmentsClone = () => {
    let fileListHtml = "";
    if (this.state.clonePrevFiles && this.state.clonePrevFiles.length > 0) {
      fileListHtml = this.state.clonePrevFiles.map((file) => (
        <Alert key={file.tcu_id}>
          <span
            onClick={() => this.removeProformaGenpactFileClone(file.tcu_id)}
          >
            <i
              className="far fa-times-circle"
              style={{ cursor: "pointer" }}
            ></i>
          </span>{" "}
          <span
            onClick={(e) =>
              this.redirectUrlTask(
                e,
                `${s3bucket_comment_diss_path}${file.tcu_id}`
              )
            }
          >
            <i className="fa fa-download" style={{ cursor: "pointer" }}></i>
          </span>{" "}
          {file.actual_file_name.length > 50
            ? `${file.actual_file_name.substr(0, 40)}...`
            : file.actual_file_name}
        </Alert>
      ));
    }

    return fileListHtml;
  };

  showAttachmentsCloneMain = () => {
    let fileListHtml = "";
    if (this.state.clonePrevFiles && this.state.clonePrevFiles.length > 0) {
      fileListHtml = this.state.clonePrevFiles.map((file) => (
        <Alert key={file.upload_id}>
          <span onClick={() => this.removeCloneMainFiles(file.upload_id)}>
            <i
              className="far fa-times-circle"
              style={{ cursor: "pointer" }}
            ></i>
          </span>{" "}
          <span
            onClick={(e) =>
              this.redirectUrlTask(e, `${task_path}${file.upload_id}`)
            }
          >
            <i className="fa fa-download" style={{ cursor: "pointer" }}></i>
          </span>{" "}
          {file.actual_file_name.length > 50
            ? `${file.actual_file_name.substr(0, 40)}...`
            : file.actual_file_name}
        </Alert>
      ));
    }

    return fileListHtml;
  };

  getDynamicForm = (dynamic_form) => {
    let html = [];

    for (let index = 0; index < dynamic_form.length; index++) {
      const element = dynamic_form[index];

      //console.log("element",element);
      if (element.field_type === "text") {
        // if (element.field_name == 'product_code') {
        //   data_arr = this.state.sapProdCode_arr;
        // }
        if (element.field_name == 'product_code') {
          html.push(
            <Col xs={6} sm={6} md={6} key={index}>
              <div className="form-group" onMouseLeave={(e) => {
                if (this.state.product_code_auto_suggest != '') {
                  document.getElementById(`product_code`).blur();
                  this.setState({ product_code_auto_suggest: '' });
                }
              }} >
                <label>
                  {element.field_title}{" "}
                  {element.required === 1 && (
                    <span className="required-field">*</span>
                  )}
                </label>
                <Field
                  name={element.field_name}
                  type="text"
                  id={`product_code`}
                  className={`selectArowGray form-control`}
                  autoComplete="off"
                  disabled
                  value={element.selected_value ? element.selected_value : ''}
                  onChange={(e) => this.setMaterialID(element.id, e)}

                  onFocus={(e) => {
                    if (element.selected_value != '' && element.selected_value.length > 2) {
                      API.post(`/api/feed/get_sap_Product_code`, { material_id: element.selected_value })
                        .then((res) => {

                          let ret_html = res.data.data.map((val, index) => {
                            return (<li style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => this.setMaterialIDAutoSuggest(val.label, element.id)} >{val.label}<hr style={{ marginTop: '6px', marginBottom: '0px' }} /></li>)
                          })
                          this.setState({ product_code_auto_suggest: ret_html });
                        })
                        .catch((err) => {
                          console.log(err);
                        });
                    }
                  }}

                ></Field>

                {this.state.product_code_auto_suggest != '' && <ul id={`material_id_auto_suggect`} style={{ zIndex: '30', position: 'absolute', backgroundColor: '#d0d0d0', height: "100px", overflowY: 'auto', listStyleType: 'none', width: '100%' }} >
                  {this.state.product_code_auto_suggest}
                </ul>}

                {element.err_msg != "" && (
                  <span className="errorMsg">{element.err_msg}</span>
                )}
              </div>
            </Col>
          );
        } else if (element.field_name == 'buyer_sold') {
          html.push(
            <Col xs={6} sm={6} md={6} key={index}>
              <div className="form-group" onMouseLeave={(e) => {
                if (this.state.buyer_sold_auto_suggest != '') {
                  document.getElementById(`buyer_sold`).blur();
                  this.setState({ buyer_sold_auto_suggest: '' });
                }
              }} >
                <label>
                  {element.field_title}{" "}
                  {element.required === 1 && (
                    <span className="required-field">*</span>
                  )}
                </label>
                <Field
                  name={element.field_name}
                  type="text"
                  id={`buyer_sold`}
                  className={`selectArowGray form-control`}
                  autoComplete="off"
                  value={element.selected_value ? element.selected_value : ''}
                  onChange={(e) => this.setSOLDTO(element.id, e)}

                  onFocus={(e) => {
                    this.updateDynamicForm(element.id, '');
                    if (element.selected_value != '' && element.selected_value.length > 2) {
                      API.post(`/api/feed/find_sap_soldTo`, { soldto_id: element.selected_value })
                        .then((res) => {

                          let ret_html = res.data.data.map((val, index) => {
                            return (<li style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => this.setSOLDTOAutoSuggest(val.label, element.id)} >{val.label}<hr style={{ marginTop: '6px', marginBottom: '0px' }} /></li>)
                          })
                          this.setState({ buyer_sold_auto_suggest: ret_html });
                        })
                        .catch((err) => {
                          console.log(err);
                        });
                    }
                  }}

                ></Field>

                {this.state.buyer_sold_auto_suggest != '' && <ul id={`material_id_auto_suggect`} style={{ zIndex: '30', position: 'absolute', backgroundColor: '#d0d0d0', height: "100px", overflowY: 'auto', listStyleType: 'none', width: '100%' }} >
                  {this.state.buyer_sold_auto_suggest}
                </ul>}

                {element.err_msg != "" && (
                  <span className="errorMsg">{element.err_msg}</span>
                )}
              </div>
            </Col>
          );
        } else if (element.field_name == 'buyer_cust_code') {
          html.push(
            <Col xs={6} sm={6} md={6} key={index}>
              <div className="form-group" onMouseLeave={(e) => {
                if (this.state.buyer_cust_code_auto_suggest != '') {
                  document.getElementById(`buyer_cust_code`).blur();
                  this.setState({ buyer_cust_code_auto_suggest: '' });
                }
              }} >
                <label>
                  {element.field_title}{" "}
                  {element.required === 1 && (
                    <span className="required-field">*</span>
                  )}
                </label>
                <Field
                  name={element.field_name}
                  type="text"
                  id={`buyer_cust_code`}
                  className={`selectArowGray form-control`}
                  autoComplete="off"
                  value={element.selected_value ? element.selected_value : ''}
                  onChange={(e) => this.setSOLDTO(element.id, e)}

                  onFocus={(e) => {
                    this.updateDynamicForm(element.id, '');
                    if (element.selected_value != '' && element.selected_value.length > 2) {
                      API.post(`/api/feed/find_sap_soldTo`, { soldto_id: element.selected_value })
                        .then((res) => {

                          let ret_html = res.data.data.map((val, index) => {
                            return (<li style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => this.setSOLDTOAutoSuggest(val.label, element.id)} >{val.label}<hr style={{ marginTop: '6px', marginBottom: '0px' }} /></li>)
                          })
                          this.setState({ buyer_cust_code_auto_suggest: ret_html });
                        })
                        .catch((err) => {
                          console.log(err);
                        });
                    }
                  }}

                ></Field>

                {this.state.buyer_cust_code_auto_suggest != '' && <ul id={`buyer_cust_code_auto_suggect`} style={{ zIndex: '30', position: 'absolute', backgroundColor: '#d0d0d0', height: "100px", overflowY: 'auto', listStyleType: 'none', width: '100%' }} >
                  {this.state.buyer_cust_code_auto_suggest}
                </ul>}

                {element.err_msg != "" && (
                  <span className="errorMsg">{element.err_msg}</span>
                )}
              </div>
            </Col>
          );
        } else {
          html.push(
            <Col xs={6} sm={6} md={6} key={index}>
              <div className="form-group">
                <label>
                  {element.field_title}{" "}
                  {element.required === 1 && (
                    <span className="required-field">*</span>
                  )}
                </label>
                <Field
                  name={element.field_name}
                  type="text"
                  disabled={(element.field_name == 'product_name') ? true : false}
                  className={`selectArowGray form-control`}
                  autoComplete="off"
                  value={element.selected_value ? element.selected_value : ''}
                  onChange={(e) => this.updateDynamicForm(element.id, e)}
                ></Field>

                {element.err_msg != "" && (
                  <span className="errorMsg">{element.err_msg}</span>
                )}
              </div>
            </Col>
          );
        }
      } else if (element.field_type === "textarea") {
        html.push(
          <Col xs={12} sm={12} md={12} key={index}>
            <div className="form-group">
              <label>
                {element.field_title}{" "}
                {element.required === 1 && (
                  <span className="required-field">*</span>
                )}
              </label>

              <Field
                name={element.field_name}
                style={{ height: '80px' }}
                component="textarea"
                className={`selectArowGray form-control`}
                autoComplete="off"
                value={element.selected_value}
                onChange={(e) => this.updateDynamicForm(element.id, e)}
              ></Field>

              {element.err_msg != "" && (
                <span className="errorMsg">{element.err_msg}</span>
              )}
            </div>
          </Col>
        );
      }
      else if (element.field_type === "dropdown" && element.values != null && element.values != '') {

        if (element.field_name == 'ship_to') {
          html.push(
            <Col xs={6} sm={6} md={6} key={index}>
              <div className="form-group">
                <label>
                  {element.field_title}{" "}
                  {element.required === 1 && (
                    <span className="required-field">*</span>
                  )}
                </label>
                <Select
                  value={
                    element.selected_value == "" ? null : element.selected_value
                  }
                  className="basic-single"
                  classNamePrefix="select"
                  isClearable={true}
                  isSearchable={true}
                  name={element.field_name}
                  options={element.values ? element.values : []}
                  onChange={(e) => this.updateDynamicForm(element.id, e)}
                />

                {element.err_msg != "" && (
                  <span className="errorMsg">{element.err_msg}</span>
                )}
              </div>
            </Col>
          );
        } else {
          html.push(
            <Col xs={6} sm={6} md={6} key={index}>
              <div className="form-group">
                <label>
                  {element.field_title}{" "}
                  {element.required === 1 && (
                    <span className="required-field">*</span>
                  )}
                </label>
                <Select
                  value={
                    element.selected_value == "" ? null : element.selected_value
                  }
                  className="basic-single"
                  classNamePrefix="select"
                  isClearable={true}
                  isSearchable={true}
                  name={element.field_name}
                  options={element.values ? JSON.parse(htmlDecode(element.values)) : []}
                  onChange={(e) => this.updateDynamicForm(element.id, e)}
                />

                {element.err_msg != "" && (
                  <span className="errorMsg">{element.err_msg}</span>
                )}
              </div>
            </Col>
          );
        }
      }
      else if (element.field_type === "dropdown" && (element.values == null || element.values == '')) {
        let data_arr = [];
        // console.log("element no values",element);
        if (element.field_name == 'region') {
          data_arr = this.state.region_arr;
        } else if (element.field_name == 'inco_terms') {
          data_arr = this.state.sapIncoterms_arr;
        } else if (element.field_name == 'market') {
          data_arr = this.state.sapCountry_arr;
        } else if (element.field_name == 'order_reason') {
          data_arr = this.state.sapOrderReason_arr;
        } else if (element.field_name == 'mode_shipment') {
          data_arr = this.state.sapShipment_arr;
        } else if (element.field_name == 'original_documents') {
          data_arr = this.state.sapOrgDocs_arr;
        } else if (element.field_name == 'currency') {
          data_arr = this.state.sapCurrency_arr;
        } else if (element.field_name == 'pharma_ref') {
          data_arr = this.state.sapPharmaRef_arr;
        } else if (element.field_name == 'payment_terms') {
          data_arr = this.state.sapPaymentTerms_arr;
        } else if (element.field_name == 'ship_to') {
          data_arr = this.state.sapShipTo_arr;
        } else if (element.field_name == 'plant_supplier') {
          data_arr = this.state.sapPlantCode_arr;
        } else if (element.field_name == 'plant_delivery') {
          data_arr = this.state.sapPlantCode_arr;
        } else if (element.field_name == 'bm_name') {
          data_arr = this.state.bm_arr;
        } else if (element.field_name == 'plant_code') {
          data_arr = this.state.sapPlantCode_arr;
        }


        html.push(
          <Col xs={6} sm={6} md={6} key={index}>
            <div className="form-group">
              <label>
                {element.field_title}{" "}
                {element.required === 1 && (
                  <span className="required-field">*</span>
                )}
              </label>
              <Select
                value={
                  element.selected_value == "" ? null : element.selected_value
                }
                className="basic-single"
                classNamePrefix="select"
                isClearable={true}
                isSearchable={true}
                name={element.field_name}
                options={data_arr ? data_arr : []}
                onChange={(e) => this.updateDynamicForm(element.id, e)}
              />

              {element.err_msg != "" && (
                <span className="errorMsg">{element.err_msg}</span>
              )}
            </div>
          </Col>
        );

      }
      else if (element.field_type === "date") {
        // let def_value = '';

        // if(element.field_name == "date_dispatch"){
        //  def_value = this.state.defaultValue_genpact.rdd
        // }
        // let dateStr = "Thu Aug 26 2021 00:00:00 GMT+0530 (India Standard Time)"
        // console.log(element.field_name + " : ", element.selected_value);

        html.push(
          <Col xs={6} sm={6} md={6} key={index}>
            <div className="form-group react-date-picker sub-task-area">
              <label>
                {element.field_title}{" "}
                {/* {dateFormat(
                                  def_value,
                                  "dd/mm/yyyy h:MM TT"
                                )}) */}
                {element.required === 1 && (
                  <span className="required-field">*</span>
                )}
              </label>
              <div className="form-control">
                <DatePicker
                  name={element.field_name}
                  className="borderNone"
                  minDate={(element.field_name != 'po_date') ? new Date() : ''}
                  selected={
                    element.selected_value != '' ? new Date(element.selected_value) : null
                  }
                  onChange={(e) => (element.field_name == 'po_date') ? this.updateDynamicForm(element.id, e) : this.updateRDD(element.id, e)}
                  dateFormat="dd/MM/yyyy"
                  autoComplete="off"
                />

                {element.err_msg != "" && (
                  <span className="errorMsg">{element.err_msg}</span>
                )}
              </div>
            </div>
          </Col>
        );
      }
    }
    return html;
  };

  updateRDD = (id, e) => {

    swal({
      closeOnClickOutside: false,
      title: "RDD mismatch",
      text: "RDD mismatches with the main order, the change in RDD will update the main order. Do you wish to update ?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willUpdate) => {
      if (willUpdate) { //alert(dateFormat(e, "yyyy-mm-dd"));
        API.post(`/api/tasks/update_rdd/`, { rdd: dateFormat(e, "yyyy-mm-dd"), task_id: this.state.currRow.task_id })
          .then((res) => {
            if (res.data.status == 1) {
              this.updateDynamicForm(id, e);
            }
          })
          .catch((err) => {
            showErrorMessageFront(err, 2, this.props);
          });
      }
  
    });
  }

  updateDynamicForm = (id, e, product_name = '', plant_code = null, ship_to = null) => {

    let prev_state = this.state.dynamic_form;
    let field_changed = '';
    for (let index = 0; index < prev_state.length; index++) {
      const element = prev_state[index];

      if (element.id == id) {
        if (
          element.field_type === "dropdown" ||
          element.field_type === "date"
        ) {
          
            prev_state[index].selected_value = e;
            prev_state[index].err_msg = "";
          
        } else {

          if (element.field_name == 'product_code') {
            field_changed = element.field_name;
            prev_state[index].selected_value = e;
            prev_state[index].err_msg = "";
          } else if (element.field_name == 'buyer_sold') {
            field_changed = element.field_name;
            prev_state[index].selected_value = e;
            prev_state[index].err_msg = "";
          } else if (element.field_name == 'buyer_cust_code') {
            field_changed = element.field_name;
            prev_state[index].selected_value = e;
            prev_state[index].err_msg = "";
          } else {
            prev_state[index].selected_value = e.target.value;
            prev_state[index].err_msg = "";
          }

        }
      }
    }

    if (field_changed == 'product_code' || field_changed == 'buyer_sold' || field_changed == 'buyer_cust_code') {
      for (let index = 0; index < prev_state.length; index++) {
        const element = prev_state[index];
        if (element.field_name == 'product_name' && field_changed == 'product_code') {
          prev_state[index].selected_value = product_name;
          prev_state[index].err_msg = "";
        }
        if (element.field_name == 'plant_code' && field_changed == 'product_code') {
          //SET OPTIONS HERE
          //plant_code
          prev_state[index].selected_value = null;
          prev_state[index].values = plant_code;
        }
        if (element.field_name == 'ship_to' && (field_changed == 'buyer_sold' || field_changed == 'buyer_cust_code')) {
          //SET OPTIONS HERE
          //ship_to
          prev_state[index].selected_value = null;
          prev_state[index].values = ship_to;
        }
      }
    }

    //console.log('prev_state',prev_state);

    this.setState({ dynamic_form: prev_state });
  };

  showAttachmentsGenpact = () => {
    var fileListHtml = this.state.prev_files.map((file) => (
      <Alert key={file.upload_id}>
        <span onClick={() => this.removeFilesGenpact(file.upload_id)}>
          <i className="far fa-times-circle" style={{ cursor: "pointer" }}></i>
        </span>{" "}
        <span
          onClick={(e) =>
            this.redirectUrlTask(e, `${task_path}${file.upload_id}`)
          }
        >
          <i className="fa fa-download" style={{ cursor: "pointer" }}></i>
        </span>{" "}
        {file.actual_file_name}
      </Alert>
    ));

    return fileListHtml;
  };


  removeFilesGenpact = (upload_id) => {
    var new_state = [];
    for (let index = 0; index < this.state.prev_files.length; index++) {
      const element = this.state.prev_files[index];

      if (upload_id == element.upload_id) {
        //IGNORE
      } else {
        new_state.push(element);
      }
    }
    this.setState({ prev_files: new_state });
  };


  setMaterialID = (id, event) => {

    this.updateDynamicForm(id, event.target.value);

    if (event.target.value && event.target.value.length > 2) {
      API.post(`/api/feed/get_sap_Product_code`, { material_id: event.target.value })
        .then((res) => {

          let ret_html = res.data.data.map((val, index) => {
            return (<li style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => this.setMaterialIDAutoSuggest(val.label, id)} >{val.label}<hr style={{ marginTop: '6px', marginBottom: '0px' }} /></li>)
          })
          this.setState({ product_code_auto_suggest: ret_html });

        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  setMaterialIDAutoSuggest = (event, id) => {
    let arr = event.split(')');
    let material_code = arr[0].substring(1);
    let product_name = arr[1].substring(1);
    let plant_code = [];

    API.get(`/api/feed/get_sap_plant_code/${material_code}`)
      .then((res) => {
        if (res.data.data.length > 0) {
          plant_code = res.data.data;
        }

        this.updateDynamicForm(id, material_code, product_name, plant_code);
        this.setState({ product_code_auto_suggest: '' });

      })
      .catch((err) => {
        console.log(err);
      });

  };

  setSOLDTO = (id, event) => {

    this.updateDynamicForm(id, event.target.value);

    if (event.target.value && event.target.value.length > 2) {
      API.post(`/api/feed/find_sap_soldTo`, { soldto_id: event.target.value })
        .then((res) => {

          let ret_html = res.data.data.map((val, index) => {
            return (<li style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => this.setSOLDTOAutoSuggest(val.label, id)} >{val.label}<hr style={{ marginTop: '6px', marginBottom: '0px' }} /></li>)
          })
          this.setState({ buyer_sold_auto_suggest: ret_html });
          this.setState({ buyer_cust_code_auto_suggest: ret_html });



        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  setSOLDTOAutoSuggest = (event, id) => {
    //DEBA API
    let ship_to = [];
    let arr = event.split('(');
    API.get(`/api/feed/get_sap_shipto/${arr[0]}`)
      .then((res) => {
        if (res.data.data.length > 0) {
          ship_to = res.data.data;
        }

        this.updateDynamicForm(id, event, null, null, ship_to);
        this.setState({ buyer_sold_auto_suggest: '' });
        this.setState({ buyer_cust_code_auto_suggest: '' });

      })
      .catch((err) => {
        console.log(err);
      });
  };

  setGenpactTask = (event, setFieldValue) => {
    setFieldValue("prefill_genpact_task", event.target.value.toString().toUpperCase());

    if (event.target.value && event.target.value.length > 2) {
      API.post(`/api/feed/genpact_task`, { task_id: event.target.value })
        .then((res) => {

          let ret_html = res.data.data.map((val, index) => {
            return (<li style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => this.setGenpactTaskAutoSuggest(val.label, setFieldValue)} >{val.label}<hr style={{ marginTop: '6px', marginBottom: '0px' }} /></li>)
          })
          this.setState({ genpact_task_auto_suggest: ret_html });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  // Genpact Auto Suggest on select
  setGenpactTaskSelect = (event, setFieldValue) => {
    if (event == null) {
      // console.log("here clear");
      setFieldValue("prefill_genpact_task", '');
      this.setState({ dynamic_form: [], selected_genpact_option: '' });
    } else {
      // Fill All Options (Select DropDown)
      this.fillAllOptionsArr();
      API.post(`/api/feed/genpact_task_details`, { task_id: event.value.toString() })
        .then((res) => {
          setFieldValue("prefill_genpact_task", event);
          // console.log("res.data.data[0]",JSON.parse(res.data.data[0].genpact_checklist))
          const dynamicForm = JSON.parse(res.data.data[0].genpact_checklist);
          const genpactType = this.getSelectedGenpactOption(res.data.data[0].genpact_type);
          // console.log("dynamicForm",dynamicForm);
          this.setState({ dynamic_form: dynamicForm, selected_genpact_option: genpactType });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  setGenpactTaskAutoSuggest = (event, setFieldValue) => {
    // console.log(event.toString());

    API.post(`/api/feed/genpact_task_details`, { task_id: event.toString() })
      .then((res) => {

        // console.log('res.data.data',res.data.data);

        setFieldValue("prefill_genpact_task", event.toString());
        const dynamicForm = JSON.parse(res.data.data[0].genpact_checklist);
        const genpactType = this.getSelectedGenpactOption(res.data.data[0].genpact_type);
        // console.log("dynamicForm",dynamicForm);
        this.setState({ genpact_task_auto_suggest: '', dynamic_form: dynamicForm, selected_genpact_option: genpactType });

      })
      .catch((err) => {
        console.log(err);
      });
  };

  getSelectedGenpactOption = (value) => {
    const { genpact_options } = this.state;
    if (genpact_options && genpact_options.length > 0) {
      const selectedOption = genpact_options.find((item) => item.value == value);
      if (selectedOption && Object.keys(selectedOption).length > 0) {
        return selectedOption;
      }
      return {};
    }
  }

  loadGenpactForm = () => {
    const validateStopFlag = Yup.object().shape({
      title: Yup.string().trim()
        .required("Please enter title")
        .min(2, `Minimum 2 characters long.`)
        .max(100, `Maximum 100 characters are allowed.`),

      content: Yup.string().trim().required("Please give description"),

      dueDate: Yup.string().trim().required("Please enter due date"),
    });

    // ==== Autosuggest for Reference Query DRL ==== //
    const { value, suggestions, ref_loader } = this.state;

    const inputProps = {
      placeholder: "Reference query min 3 characters",
      value,
      type: "search",
      onChange: this.onReferenceChange,
    };

    const request_type = this.state.currRow.request_type;
    const { selected_genpact_option } = this.state;
    return (
      <Modal
        show={this.state.showCreateSubTask}
        onHide={() => this.handleClose()}
        backdrop="static"
      >
        <Formik
          initialValues={initialValues}
          validationSchema={validateStopFlag}
          onSubmit={this.handleSubmitCreateTask}
        >
          {({
            values,
            errors,
            touched,
            isValid,
            isSubmitting,
            setFieldValue,
            setFieldTouched,
            setErrors,
          }) => {
            return (
              <Form encType="multipart/form-data">
                {this.state.showLoader === true ? (
                  <div className="loderOuter">
                    <div className="loader">
                      <img src={loaderlogo} alt="logo" />
                      <div className="loading">Loading...</div>
                    </div>
                  </div>
                ) : (
                  ""
                )}

                <Modal.Header closeButton>
                  <Modal.Title>Create Sub Task</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <div className="contBox boxflex contBoxMaxHeight">
                    <div className="form-group checText">
                      <label>
                        Select type of sub task you would like to create
                      </label>
                    </div>
                    <Row>
                      <Col xs={12} sm={4} md={4}>
                        <div className="form-group checText">
                          <input
                            type="radio"
                            name="sendDefault"
                            value="1"
                            onChange={(e) =>
                              this.handleDefault(e, setFieldValue)
                            }
                            checked={
                              (values.sendDefault || this.state.sendDefault) ===
                                true
                                ? "checked"
                                : this.state.sendDefault
                            }
                          />
                          <label>
                            XCEED Sub Task{" "}
                            <LinkWithTooltip
                              tooltip={`This task shall be created in XCEED`}
                              href="#"
                              id="tooltip-1"
                              clicked={(e) => this.checkHandler(e)}
                            >
                              <i
                                className="fa fa-exclamation-circle"
                                aria-hidden="true"
                              ></i>
                            </LinkWithTooltip>
                          </label>
                        </div>
                      </Col>

                      <Col xs={12} sm={4} md={4}>
                        <div className="form-group checText">
                          <input
                            type="radio"
                            name="sendCQT"
                            value={this.state.sendCQT}
                            onChange={(e) =>
                              this.handleCQTbox(e, setFieldValue)
                            }
                            checked={values.sendCQT === true ? "checked" : ""}
                          />
                          <label>
                            CQT Sub Task{" "}
                            <LinkWithTooltip
                              tooltip={`This task shall be created in CQT`}
                              href="#"
                              id="tooltip-1"
                              clicked={(e) => this.checkHandler(e)}
                            >
                              <i
                                className="fa fa-exclamation-circle"
                                aria-hidden="true"
                              ></i>
                            </LinkWithTooltip>
                          </label>
                        </div>
                      </Col>
                      {/* Monosom */}
                      {request_type === 23 && (
                        <Col xs={12} sm={4} md={4}>
                          <div className="form-group checText">
                            <input
                              type="radio"
                              name="sendGenpact"
                              value="1"
                              onChange={(e) =>
                                this.handleGenpact(e, setFieldValue)
                              }
                              checked={
                                (values.sendGenpact ||
                                  this.state.sendGenpact) === true
                                  ? "checked"
                                  : ""
                              }
                            />
                            <label>
                              Genpact Sub Task{" "}
                              <LinkWithTooltip
                                tooltip={`This task will be created for Genpact`}
                                href="#"
                                id="tooltip-1"
                              >
                                <i
                                  className="fa fa-exclamation-circle"
                                  aria-hidden="true"
                                ></i>
                              </LinkWithTooltip>
                            </label>
                          </div>
                        </Col>
                      )}

                      <Col
                        xs={12}
                        sm={12}
                        md={12}
                      // onMouseLeave={(e)=>{
                      //   if(this.state.genpact_task_auto_suggest != ''){
                      //     document.getElementById(`prefill_genpact_task`).blur();
                      //     this.setState({genpact_task_auto_suggest:''});
                      //   }
                      // }} 
                      >
                        <div className="form-group">
                          <label>Clone Genpact Task {" "}
                            <LinkWithTooltip
                              tooltip={`Select the task ID you wish to prefill`}
                              href="#"
                              id="tooltip-1"
                            >
                              <i
                                className="fa fa-exclamation-circle"
                                aria-hidden="true"
                              ></i>
                            </LinkWithTooltip></label>
                          <Select
                            name="prefill_genpact_task"
                            className="basic-single"
                            classNamePrefix="select"
                            isClearable={true}
                            isSearchable={true}
                            options={this.state.genpact_task_options}
                            value={values.prefill_genpact_task !== null &&
                              values.prefill_genpact_task !== ""
                              ? values.prefill_genpact_task
                              : ""}
                            onChange={(e) => {
                              this.setGenpactTaskSelect(e, setFieldValue);
                            }}
                          />
                          {/* <Field
                            name="prefill_genpact_task"
                            type="text"
                            className="form-control"
                            id={`prefill_genpact_task`}
                            autoComplete="off"
                            value={
                              values.prefill_genpact_task !== null &&
                              values.prefill_genpact_task !== ""
                                ? values.prefill_genpact_task
                                : ""
                            }
                            onChange={(e) => {
                              this.setGenpactTask(e, setFieldValue);
                            }}
                            onFocus={(e)=>{
                              if(this.state.prefill_genpact_task != '' && this.state.prefill_genpact_task.length > 2){
                                API.post(`/api/feed/genpact_task`,{task_id:this.state.prefill_genpact_task})
                                .then((res) => {
                                  
                                  let ret_html = res.data.data.map((val,index)=>{
                                    return (<li style={{cursor:'pointer',marginTop:'6px'}} onClick={()=>this.setGenpactTaskAutoSuggest(val.value,setFieldValue)} >{val.label}<hr style={{marginTop:'6px',marginBottom:'0px'}} /></li>)
                                  })
                                  this.setState({genpact_task_auto_suggest:ret_html});
                                })
                                .catch((err) => {
                                  console.log(err);
                                });
                              }
                            }}
                          ></Field>

                          {this.state.genpact_task_auto_suggest != '' && <ul id={`material_id_auto_suggect`} style={{zIndex:'30',position:'absolute',backgroundColor:'#d0d0d0',height:"100px",overflowY:'auto',listStyleType:'none',width:'100%'}} >
                            {this.state.genpact_task_auto_suggest}
                          </ul>} */}
                        </div>
                      </Col>
                      <Col xs={12} sm={12} md={12}>
                        <center>OR</center>
                      </Col>
                      <Col xs={12} sm={12} md={12}>
                        <div className="form-group">
                          <label>Select Type</label>
                          <Select
                            className="basic-single"
                            classNamePrefix="select"
                            value={selected_genpact_option}
                            name="genpact_option"
                            options={this.state.genpact_options}
                            onChange={(e) =>
                              this.fetchSalesOrder(e)
                            }
                          />
                        </div>
                      </Col>

                      {selected_genpact_option.value === "1" && (
                        <>
                          {/* Monosom */}
                          <Col xs={12} sm={6} md={6}>
                            <div className="form-group">
                              <label>
                                Title <span className="required-field">*</span>
                              </label>
                              <Field
                                name="title"
                                type="text"
                                className={`selectArowGray form-control`}
                                autoComplete="off"
                                value={values.title}
                              ></Field>
                              {errors.title && touched.title ? (
                                <span className="errorMsg">{errors.title}</span>
                              ) : null}
                            </div>
                          </Col>

                          {this.state.dynamic_form.length > 0 &&
                            this.getDynamicForm(this.state.dynamic_form)}

                          <Col xs={12} sm={6} md={6}>
                            <div className="form-group react-date-picker sub-task-area">
                              <label>
                                Due Date{" "}
                                {Date.parse(new Date(this.state.currRow.due_date)) >
                                  Date.parse(
                                    new Date(this.state.currRow.original_due_date)
                                  ) && (
                                    <i style={{ "font-size": "12px" }}>
                                      (Original Due Date:{" "}
                                      {dateFormat(
                                        this.state.currRow.original_due_date,
                                        "dd/mm/yyyy h:MM TT"
                                      )}
                                      )
                                    </i>
                                  )}
                              </label>

                              {/* <DatePicker
                                                name="dueDate"
                                                onChange={value =>
                                                    setFieldValue("dueDate", value)
                                                }
                                                value={values.dueDate}
                                            /> */}
                              <div className="form-control">
                                <DatePicker
                                  name={"dueDate"}
                                  className="borderNone"
                                  selected={values.dueDate ? values.dueDate : null}
                                  onChange={(e) => {
                                    this.changeDate(e, setFieldValue);
                                  }}
                                  dateFormat="dd/MM/yyyy"
                                  autoComplete="off"
                                />
                              </div>
                              {this.state.sla_breach && (
                                <p className="sub-task-er-msg sub-task-er-msg2">
                                  You are going to breach the SLA
                                </p>
                              )}
                              {errors.dueDate && touched.dueDate ? (
                                <p className="sub-task-er-msg sub-task-er-msg1">
                                  {errors.dueDate}
                                </p>
                              ) : null}
                            </div>
                          </Col>
                          {this.state.sla_breach && (
                            <Col xs={12} sm={12} md={12}>
                              <div className="form-group">
                                <label>Please Provide Reason</label>
                                <Field
                                  name="breach_reason"
                                  component="textarea"
                                  className={`selectArowGray form-control`}
                                  autoComplete="off"
                                  value={values.breach_reason}
                                />
                                {errors.breach_reason && touched.breach_reason ? (
                                  <span className="errorMsg">
                                    {errors.breach_reason}
                                  </span>
                                ) : null}
                              </div>
                            </Col>
                          )}

                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>
                                Description{" "}
                                <span className="required-field">*</span>
                              </label>
                              {/* <Field
                                            name="content"
                                            component="textarea"
                                            className={`selectArowGray form-control`}
                                            autoComplete="off"
                                            value={values.content}
                                            > 
                                            </Field> */}

                              {/* <TinyMCE
                                                name="content"
                                                content={values.comment}
                                                config={{
                                                    branding: false,
                                                    toolbar: 'undo redo | bold italic | alignleft aligncenter alignright'
                                                }}
                                                className={`selectArowGray form-control`}
                                                autoComplete="off"
                                                onChange={value =>
                                                    setFieldValue("content", value.level.content)
                                                }
                                            /> */}

                              <Editor
                                name="content"
                                value={
                                  values.content !== null && values.content !== ""
                                    ? values.content
                                    : ""
                                }
                                content={
                                  values.content !== null && values.content !== ""
                                    ? values.content
                                    : ""
                                }
                                init={{
                                  menubar: false,
                                  branding: false,
                                  placeholder: "Enter description",
                                  plugins:
                                    "link table hr visualblocks code placeholder lists autoresize textcolor",
                                  toolbar:
                                    "bold italic strikethrough superscript subscript | forecolor backcolor | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | visualblocks code | fontselect",
                                  font_formats:
                                    "Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats",
                                }}
                                onEditorChange={(value) =>
                                  setFieldValue("content", value)
                                }
                              />

                              {errors.content && touched.content ? (
                                <span className="errorMsg">{errors.content}</span>
                              ) : null}
                            </div>
                          </Col>



                          <Col xs={12} sm={6} md={6}>
                            <div className="form-group custom-file-upload">
                              <label>&nbsp;</label>
                              <Dropzone
                                onDrop={(acceptedFiles) =>
                                  this.setDropZoneFiles(
                                    acceptedFiles,
                                    setErrors,
                                    setFieldValue
                                  )
                                }
                                multiple={true}
                              >
                                {({ getRootProps, getInputProps }) => (
                                  <section>
                                    <div
                                      {...getRootProps()}
                                      className="custom-file-upload-header"
                                    >
                                      <input {...getInputProps()} />
                                      <p>Upload file</p>
                                    </div>
                                    <div className="custom-file-upload-area">
                                      {/* {this.state.currRow.proforma_id > 0 && this.showAttachments()} */}
                                      {this.state.clone_sub === 1 &&
                                        this.showAttachmentsClone()}
                                      {this.state.clone_main_task === 1 &&
                                        this.showAttachmentsCloneMain()}
                                      {this.state.prev_files && (this.state.clone_sub !== 1 && this.state.clone_main_task !== 1) &&
                                        this.showAttachmentsGenpact()}
                                      {this.state.filesHtml}
                                    </div>
                                  </section>
                                )}
                              </Dropzone>

                              {errors.file_name || touched.file_name ? (
                                <p className="sub-task-er-msg sub-task-er-msg3">
                                  {errors.file_name}
                                </p>
                              ) : null}
                            </div>
                          </Col>


                        </>
                      )
                      }

                      {selected_genpact_option.value === "2" && (
                        <>
                          <Col xs={12} sm={6} md={6}>
                            <div className="form-group">
                              <label>
                                Title <span className="required-field">*</span>
                              </label>
                              <Field
                                name="title"
                                type="text"
                                className={`selectArowGray form-control`}
                                autoComplete="off"
                                value={values.title}
                              ></Field>
                              {errors.title && touched.title ? (
                                <span className="errorMsg">{errors.title}</span>
                              ) : null}
                            </div>
                          </Col>
                          {
                            this.state.dynamic_form.length > 0 &&
                            this.getDynamicForm(this.state.dynamic_form)
                          }

                          <Col xs={12} sm={6} md={6}>
                            <div className="form-group react-date-picker sub-task-area">
                              <label>
                                Due Date{" "}
                                {Date.parse(new Date(this.state.currRow.due_date)) >
                                  Date.parse(
                                    new Date(this.state.currRow.original_due_date)
                                  ) && (
                                    <i style={{ "font-size": "12px" }}>
                                      (Original Due Date:{" "}
                                      {dateFormat(
                                        this.state.currRow.original_due_date,
                                        "dd/mm/yyyy h:MM TT"
                                      )}
                                      )
                                    </i>
                                  )}
                              </label>

                              {/* <DatePicker
                                                name="dueDate"
                                                onChange={value =>
                                                    setFieldValue("dueDate", value)
                                                }
                                                value={values.dueDate}
                                            /> */}
                              <div className="form-control">
                                <DatePicker
                                  name={"dueDate"}
                                  className="borderNone"
                                  selected={values.dueDate ? values.dueDate : null}
                                  onChange={(e) => {
                                    this.changeDate(e, setFieldValue);
                                  }}
                                  dateFormat="dd/MM/yyyy"
                                  autoComplete="off"
                                />
                              </div>
                              {this.state.sla_breach && (
                                <p className="sub-task-er-msg sub-task-er-msg2">
                                  You are going to breach the SLA
                                </p>
                              )}
                              {errors.dueDate && touched.dueDate ? (
                                <p className="sub-task-er-msg sub-task-er-msg1">
                                  {errors.dueDate}
                                </p>
                              ) : null}
                            </div>
                          </Col>
                          {this.state.sla_breach && (
                            <Col xs={12} sm={12} md={12}>
                              <div className="form-group">
                                <label>Please Provide Reason</label>
                                <Field
                                  name="breach_reason"
                                  component="textarea"
                                  className={`selectArowGray form-control`}
                                  autoComplete="off"
                                  value={values.breach_reason}
                                />
                                {errors.breach_reason && touched.breach_reason ? (
                                  <span className="errorMsg">
                                    {errors.breach_reason}
                                  </span>
                                ) : null}
                              </div>
                            </Col>
                          )}

                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>
                                Description{" "}
                                <span className="required-field">*</span>
                              </label>
                              {/* <Field
                                              name="content"
                                              component="textarea"
                                              className={`selectArowGray form-control`}
                                              autoComplete="off"
                                              value={values.content}
                                              > 
                                              </Field> */}

                              {/* <TinyMCE
                                                  name="content"
                                                  content={values.comment}
                                                  config={{
                                                      branding: false,
                                                      toolbar: 'undo redo | bold italic | alignleft aligncenter alignright'
                                                  }}
                                                  className={`selectArowGray form-control`}
                                                  autoComplete="off"
                                                  onChange={value =>
                                                      setFieldValue("content", value.level.content)
                                                  }
                                              /> */}

                              <Editor
                                name="content"
                                value={
                                  values.content !== null && values.content !== ""
                                    ? values.content
                                    : ""
                                }
                                content={
                                  values.content !== null && values.content !== ""
                                    ? values.content
                                    : ""
                                }
                                init={{
                                  menubar: false,
                                  branding: false,
                                  placeholder: "Enter description",
                                  plugins:
                                    "link table hr visualblocks code placeholder lists autoresize textcolor",
                                  toolbar:
                                    "bold italic strikethrough superscript subscript | forecolor backcolor | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | visualblocks code | fontselect",
                                  font_formats:
                                    "Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats",
                                }}
                                onEditorChange={(value) =>
                                  setFieldValue("content", value)
                                }
                              />

                              {errors.content && touched.content ? (
                                <span className="errorMsg">{errors.content}</span>
                              ) : null}
                            </div>
                          </Col>

                          <Col xs={12} sm={6} md={6}>
                            <div className="form-group custom-file-upload">
                              <label>&nbsp;</label>
                              <Dropzone
                                onDrop={(acceptedFiles) =>
                                  this.setDropZoneFiles(
                                    acceptedFiles,
                                    setErrors,
                                    setFieldValue
                                  )
                                }
                                multiple={true}
                              >
                                {({ getRootProps, getInputProps }) => (
                                  <section>
                                    <div
                                      {...getRootProps()}
                                      className="custom-file-upload-header"
                                    >
                                      <input {...getInputProps()} />
                                      <p>Upload file</p>
                                    </div>
                                    <div className="custom-file-upload-area">
                                      {/* {this.state.currRow.proforma_id > 0 && this.showAttachments()} */}
                                      {this.state.clone_sub === 1 &&
                                        this.showAttachmentsClone()}
                                      {this.state.clone_main_task === 1 &&
                                        this.showAttachmentsCloneMain()}
                                      {this.state.prev_files && this.state.clone_sub !== 1 && this.state.clone_main_task !== 1 &&
                                        this.showAttachmentsGenpact()}
                                      {this.state.filesHtml}
                                    </div>
                                  </section>
                                )}
                              </Dropzone>

                              {errors.file_name || touched.file_name ? (
                                <p className="sub-task-er-msg sub-task-er-msg3">
                                  {errors.file_name}
                                </p>
                              ) : null}
                            </div>
                          </Col>
                        </>
                      )
                      }
                      {this.state.parent_genpact_tasks.length > 0 && (selected_genpact_option.value === "1" || selected_genpact_option.value === "2") && <Col xs={12} sm={12} md={12}>
                        <div className="form-group">
                          <label>
                            Create Genpact Sub Task
                          </label>
                          {this.genpactDynamicHTML()}
                        </div>
                      </Col>}
                    </Row>
                  </div>
                </Modal.Body>
                <Modal.Footer>
                  <button
                    onClick={this.handleClose}
                    className={`btn-line`}
                    type="button"
                  >
                    Close
                  </button>
                  {this.state.clone_sub === 1 ? (
                    <button
                      className={`btn-fill btn-custom-green m-r-10`}
                      type="submit"
                    >
                      Submit
                    </button>
                  ) : (
                    <button
                      className={`btn-fill ${isValid ? "btn-custom-green" : "btn-disable"
                        } m-r-10`}
                      type="submit"
                      disabled={isValid ? false : true}
                    >
                      Submit
                    </button>
                  )}
                </Modal.Footer>
              </Form>
            );
          }}
        </Formik>
      </Modal>
    );
  };

  render() {
    return (
      <>
        {typeof this.state.employeeArr !== "undefined" &&
          (this.state.sendDefault || this.state.sendCQT)
          ? this.loadDefaultForm()
          : this.loadGenpactForm()}
      </>
    );
  }
}

export default CreateSubTaskPopup;
