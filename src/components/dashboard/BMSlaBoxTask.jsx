import React, { Component } from 'react';
import API from "../../shared/axios";

class BMSlaBoxTask extends Component {

    state = {
        met_sla: '',
        missed_sla: ''
    };

    componentDidMount = () => {

        let query_params = '';

        if(this.props.queryString != ''){
            query_params = `?${this.props.queryString}`;
        }

        let met_sla,missed_sla,class_met_sla,class_missed_sla,met_num,missed_num;
        API.get(`/api/tasks/bm_task_sla_tiles${query_params}`)
        .then(res => {
            if(res.data.data.total_sla > 0){
                met_sla = Math.ceil((res.data.data.total_met_sla/res.data.data.total_sla) * 100); 
                //missed_sla = Math.ceil((res.data.data.total_missed_sla/res.data.data.total_sla) * 100);
                missed_sla = 100-met_sla;
                
                met_num          = res.data.data.total_met_sla;
                missed_num       = res.data.data.total_missed_sla;

                class_met_sla    = met_sla;
                class_missed_sla = missed_sla;
            }else{

                met_num = 0;
                missed_num = 0;

                met_sla = 0;
                missed_sla = 0;
                class_met_sla = 1;
                class_missed_sla = 1;
            }
            this.setState({
                met_sla          : met_sla,
                missed_sla       : missed_sla,
                met_num          : met_num,
                missed_num       : missed_num,
                class_met_sla    : class_met_sla,
                class_missed_sla : class_missed_sla
            });
        })
        .catch(err => {
            console.log(err);
        });
    }

    render(){
        return(
            <>
            {this.state.met_sla !== '' && this.state.missed_sla !== '' && <div className="col-lg-3 col-sm-6 col-xs-12">
                <div className="small-box bg-Green">
                    <div className="inner clearfix">

                            <div className="col-80">
                                <p><span className="roundShape green"></span>Met  SLA</p>                           
                                <p><span className="roundShape red"></span>Missed SLA  </p>
                            </div>
                            <div className="col-20">
                                {this.state.met_sla > 0 && 
                                <div className="progress-main">
                                    <div className="progress vertical">
                                        <div className="progress-bar progress-bar-green" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style={{height: `${this.state.class_met_sla}%`}}> </div> 
                                    </div>
                                    <span className="sr-only-new">{`${this.state.met_num}`}</span>
                                </div>}
                                
                                {this.state.missed_sla > 0 && 
                                <div className="progress-main">
                                    <div className="progress vertical">
                                        <div className="progress-bar progress-bar-red" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style={{height: `${this.state.class_missed_sla}%`}}> </div> 
                                    </div>
                                    <span className="sr-only-new">{`${this.state.missed_num}`}</span>
                                </div>}
                            </div>

                    </div>
                    <div className="small-box-footer clearfix">

                            <div className="col-80"><span className="slabox">Task SLA</span>Adherence Score</div>
                            {this.state.met_sla > 0 && 
                            <div className="col-20"><span>{`${this.state.met_sla}%`}</span></div>
                            }

                    </div>
                </div>
            </div>}
            </>
        );
    }
}

export default BMSlaBoxTask;