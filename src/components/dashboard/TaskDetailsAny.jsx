import React, { Component } from "react";
import {
  Row,
  Col,
  Button,
  Panel,
  PanelGroup,
  Alert,
  Tooltip,
  OverlayTrigger,
  Modal,
  ButtonToolbar,
} from "react-bootstrap";
import dateFormat from "dateformat";
import API from "../../shared/axios";
import TinyMCE from "react-tinymce";
import Dropzone from "react-dropzone";
//import { FilePond } from 'react-filepond';
//import 'filepond/dist/filepond.min.css';
import * as Yup from "yup";
import { Formik, Field, Form } from "formik";
import swal from "sweetalert";
import Select from "react-select";
import { Editor } from "@tinymce/tinymce-react";

import { Redirect, Link, withRouter } from "react-router-dom";
//import Loader from "react-loader-spinner";
import {
  getDashboard,
  getMyId,
  htmlDecode,
  localDate,
  localDateOnly,
  trimString,
  inArray,
} from "../../shared/helper";
import commenLogo from "../../assets/images/drreddylogosmall.png";
import commenLogo1 from "../../assets/images/drreddynoimg.png";
import exclamationImage from "../../assets/images/exclamation-icon-black.svg";
import downloadIcon from "../../assets/images/download-icon.svg";

import SPOCVerticalMenu from "./SPOCVerticalMenu";

import BMVerticalMenu from "../bmoverview/BMVerticalMenu";
import CSCVerticalMenu from "../cscoverview/CSCVerticalMenu";
import RAVerticalMenu from "../raoverview/RAVerticalMenu";
import OTHRVerticalMenu from "../othroverview/OTHRVerticalMenu";

import BMVerticalMenuAllocatedTask from "../bmoverview/BMVerticalMenuAllocatedTask";
import CSCVerticalMenuAllocatedTask from "../cscoverview/CSCVerticalMenuAllocatedTask";
import RAVerticalMenuAllocatedTask from "../raoverview/RAVerticalMenuAllocatedTask";
import OTHRVerticalMenuAllocatedTask from "../othroverview/OTHRVerticalMenuAllocatedTask";

import BMClosedMenu from "../bmoverview/BMClosedMenu";
import CSCClosedMenu from "../cscoverview/CSCClosedMenu";
import RAClosedMenu from "../raoverview/RAClosedMenu";
import OTHRClosedMenu from "../othroverview/OTHRClosedMenu";

import StatusColumn from "./StatusColumn";
import SubTaskTable from "./SubTaskTable";

import CreateSubTaskPopup from "./CreateSubTaskPopup";
import AssignPopup from "./AssignPopup";
import RespondCustomerPopup from "./RespondCustomerPopup";
import RespondBackPopup from "./RespondBackPopup";
import ProformaPopup from "./ProformaPopup";
import AuthorizePopup from "./AuthorizePopup";
import IncreaseSlaPopup from "./IncreaseSlaPopup";
import IncreaseSlaSubTaskPopup from "./IncreaseSlaSubTaskPopup";
import Poke from "./Poke";
import DeleteTaskPopup from "./DeleteTaskPopup";

import RespondBackAssignedPopup from "./RespondBackAssignedPopup";
import ReAssignPopup from "./ReAssignPopup";

import RequestToReopen from "./RequestToReopen";
import RequestToReopenMyTask from "./RequestToReopenMyTask";
import CopyCreateMyTasksPopup from "./CopyCreateMyTasksPopup";

import SPOCTranslatedCommentPopup from "./SPOCTranslatedCommentPopup";
import SPOCOriginalCommentPopup from "./SPOCOriginalCommentPopup";

import { showErrorMessageFront } from "../../shared/handle_error_front";

//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
//import Select from "react-select";

import ReactHtmlParser from "react-html-parser";
import jsPDF from "jspdf";
import "jspdf-autotable";
import Switch from "react-switch";

// connect to store
import { connect } from "react-redux";
import {
  authLogout,
  getNotifications,
  getNotificationCount,
} from "../../store/actions/auth";

const path = `${process.env.REACT_APP_API_URL}/api/tasks/download/`; // SATYAJIT
const comm_path = `${process.env.REACT_APP_API_URL}/api/tasks/download_comment/`; // SVJT
const diss_path = `${process.env.REACT_APP_API_URL}/api/tasks/download_discussion/`; // SVJT
const s3bucket_diss_path = `${process.env.REACT_APP_API_URL}/api/tasks/download_discussion_bucket/`; // SVJT
const s3bucket_comment_diss_path = `${process.env.REACT_APP_API_URL}/api/tasks/download_comment_bucket/`; // SVJT
const s3bucket_task_diss_path = `${process.env.REACT_APP_API_URL}/api/tasks/download_task_bucket/`; // SVJT
const download_path = `${process.env.REACT_APP_API_URL}/api/drl/download/`; // SATYAJIT
const pourl = `${process.env.REACT_APP_API_URL}/api/tasks/download_po_doc/`; //Deba

// initialize form and their validation
const initialValues = {
  comment: "",
  file_name: [],
};

const sapInitialValues = {
  order_number: "",
  file_name: [],
};

const cqtInitialValues = {
  cqt_task_reposne: "",
};

const commentSchema = Yup.object().shape({
  comment: Yup.string().trim().required("Please enter your comment"),
});

const sapSchema = Yup.object().shape({
  order_number: Yup.number().required("Please enter sales order number"),
});

const validateCqtFlag = Yup.object().shape({
  cqt_task_reposne: Yup.string().trim()
    .required("Please enter your comment")
    .min(2, "Minimum 2 characters are allowed")
    .max(200, "Maximum 200 characters are allowed"),
});

const priority_arr = [
  { priority_id: 1, priority_value: "Low" },
  { priority_id: 2, priority_value: "Medium" },
  { priority_id: 3, priority_value: "High" },
];

const req_category_arr = [
  { request_category_id: 1, request_category_value: "New Order" },
  { request_category_id: 2, request_category_value: "Other" },
  { request_category_id: 3, request_category_value: "Complaint" },
  { request_category_id: 4, request_category_value: "Forecast" },
  { request_category_id: 5, request_category_value: "Payment" },
  { request_category_id: 6, request_category_value: "Notification" },
  {
    request_category_id: 7,
    request_category_value: "Request for Proforma Invoice",
  },
];

const removeDropZoneFiles = (fileName, objRef, setErrors) => {
  var newArr = [];
  for (let index = 0; index < objRef.state.files.length; index++) {
    const element = objRef.state.files[index];

    if (fileName === element.name) {
    } else {
      newArr.push(element);
    }
  }

  var fileListHtml = newArr.map((file,indf) => (
    <Alert key={file.name}>
      <span onClick={() => removeDropZoneFiles(file.name, objRef, setErrors)}>
        <i className="far fa-times-circle"></i>
      </span>{" "}
      {file.name}
    </Alert>
  ));
  setErrors({ file_name: "" });
  objRef.setState({
    files: newArr,
    filesHtml: fileListHtml,
  });
};

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  // var file_name, file_extension;
  //   file_name = tooltip;
  //   file_extension = (file_name.substr((file_name.lastIndexOf('.') + 1))).toLowerCase();
  //console.log (file_extension);
  // if( file_extension === "jpg" || file_extension === "jpeg" || file_extension === "png" || file_extension === "gif" ){
  //   return (
  //     <OverlayTrigger
  //       overlay={<Tooltip id={id}>
  //       <img src={`${process.env.REACT_APP_API_URL}${tooltip}`} height="150px" width="150px" />
  //       </Tooltip>}
  //       placement="top"
  //       delayShow={300}
  //       delayHide={150}
  //       trigger={["hover"]}
  //     >
  //       <Link to={href} onClick={clicked}>
  //         {children}
  //       </Link>
  //     </OverlayTrigger>
  //   );
  // }else{
  return (
    // <OverlayTrigger
    //   overlay={<Tooltip id={id}></Tooltip>}
    //   placement="top"
    //   delayShow={300}
    //   delayHide={150}
    //   trigger={["hover"]}
    // >
    <Link to={href} onClick={clicked}>
      {children}
    </Link>
    // </OverlayTrigger>
  );
  // }
}
/*For Tooltip*/

/*For Tooltip*/
function LinkWithTooltipText({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

function LinkWithTooltipZip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <a href={href}>{children}</a>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

class TaskDetailsAny extends Component {
  constructor(props) {
    super(props);

    //console.log('props object',this.props)

    this.state = {
      taskDetails: [],
      show_lang: "EN",
      show_lang_diss: "EN",
      switchChecked: false,
      taskDetailsTranslated: [],
      taskDetailsTranslatedFiles: [],
      translate_api_complete: false,
      CQT_details: [],
      discussDetails: [],
      discussDetailsT: [],
      CQT_loader: false,
      sap_loader: false,
      invoice_list: "",
      files: [],
      filesHtml: "",
      order_number: "",
      loadComment: false,
      comLoader: false,
      loadDiscussion: false,
      invalid_access: false,
      task_id: this.props.match.params.id,
      apiCompleted: false,
      loaderCCemp: false,
      disscussion_comment: "",
      emp_comment: "",
      action_button: "",
      openCommentForm: false,
      activity_log: [],
      showDisplayCheckBox: 0,

      showCreateSubTask: false,
      showCloneCreateSubTask: false,
      showCreateSubTaskNew: false,

      showIncreaseSla: false,
      showIncreaseSlaSubTask: false,
      showCreateTasks: false,
      showAssign: false,
      showRespondBack: false,
      showProformaForm: false,
      showTaskDetails: false,
      showRespondCustomer: false,
      showReqToReopen: false,
      showReopen: false,
      showReAssign: false,
      showAuthorizeBack: false,
      taskEdit: true,
      showTranslateEditComment: false,
      showOriginalEditComment: false,
      taskTranslateEdit: false,
      dynamic_lang: {},
    };
  }

  checkHandler = (event) => {
    event.preventDefault();
  };

  showApprovalPopup = (currRow) => {
    swal({
      closeOnClickOutside: false,
      title: "Approve comment",
      text: "Are you sure you want to approve this comment?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.setState({ showModalLoader: true });
        API.post(
          `/api/tasks/approve_comments/${currRow.task_id}/${currRow.discussion_id}/${currRow.comment_id}`
        )
          .then((res) => {
            swal({
              closeOnClickOutside: false,
              title: "Success",
              text: "Comment has been approved!",
              icon: "success",
            }).then(() => {
              this.reloadTaskDetails();
            });
          })
          .catch((err) => {
            var token_rm = 2;
            showErrorMessageFront(err, token_rm, this.props);
          });
      }
    });
  };

  showTranslateCommentPopup = (currRow) => {
    //console.log("Respond");
    this.setState({ showTranslateEditComment: true, currRow: currRow });
  };

  showOriginalCommentPopup = (currRow) => {
    //console.log("Respond");
    this.setState({ showOriginalEditComment: true, currRow: currRow });
  };

  setCreateDate = (date_added) => {
    var mydate = localDate(date_added);
    return dateFormat(mydate, "dd/mm/yyyy");
  };

  setDaysPending = (due_date) => {
    // console.log('due_date', due_date);
    var dueDate = localDate(due_date);
    var today = new Date();
    var timeDiff = dueDate.getTime() - today.getTime();
    if (timeDiff > 0) {
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
      return diffDays;
    } else {
      return 0;
    }
  };

  setPriorityName = (priority_id) => {
    var ret = "Not Set";
    for (let index = 0; index < priority_arr.length; index++) {
      const element = priority_arr[index];
      if (element["priority_id"] === priority_id) {
        ret = element["priority_value"];
      }
    }

    return ret;
  };

  getSLAStatus = (sla_status) => {
    if (sla_status === 1) {
      return "Need Further Info";
    } else if (sla_status === 2) {
      return "Closed";
    }
  };

  setReqCategoryName = (req_cate_id) => {
    var ret = "Not Set";
    for (let index = 0; index < req_category_arr.length; index++) {
      const element = req_category_arr[index];
      if (element["request_category_id"] === req_cate_id) {
        ret = element["request_category_value"];
      }
    }

    return ret;
  };

  checkPharmacopoeia(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (
        (request_type === 35 ||
          request_type === 36 ||
          request_type === 37 ||
          request_type === 22 ||
          request_type === 21 ||
          request_type === 19 ||
          request_type === 18 ||
          request_type === 17 ||
          request_type === 2 ||
          request_type === 3 ||
          request_type === 4 ||
          request_type === 10 ||
          request_type === 12 ||
          request_type === 1 ||
          request_type === 30 ||
          request_type === 32 ||
          request_type === 33 ||
          request_type === 41 ||
          request_type === 42) &&
        parent_id === 0
      ) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkDMFNumber = (request_type, parent_id) => {
    if (request_type === null) {
      return false;
    } else {
      if ((request_type === 27 || request_type === 39) && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkAposDocumentType = (request_type, parent_id) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 38 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkNotificationNumber = (request_type, parent_id) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 29 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkRDORC = (request_type, parent_id) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 28 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkPolymorphicForm(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if ((request_type === 2 || request_type === 3) && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkStabilityDataType(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 13 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkAuditSiteName(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 9 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkAuditVistDate(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 9 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkQuantity(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (
        (request_type === 7 ||
          request_type === 34 ||
          request_type === 23 ||
          request_type === 25 ||
          request_type === 31 ||
          request_type === 41 ||
          request_type === 43) &&
        parent_id === 0
      ) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkBatchNo(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if ((request_type === 7 || request_type === 34) && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkNatureOfIssue(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if ((request_type === 7 || request_type === 34) && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkRDD(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (
        (request_type === 23 ||
          request_type === 39 ||
          request_type === 40 ||
          request_type === 41) &&
        parent_id === 0
      ) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkPricing(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if ((request_type === 23 || request_type === 41) && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkSamplesWorkingImpureRequest(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 1 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkProductRequest(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type !== 24 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkMarketRequest(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (
        request_type !== 24 &&
        request_type !== 25 &&
        request_type !== 26 &&
        request_type !== 40 &&
        parent_id === 0
      ) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkGMPRequest(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 40 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkTGAEmail(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 40 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkApplicant(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 40 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkDocRequired(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 40 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkForecastNotificationDate(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 24 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkAmountPending(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 25 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkDaysRemaining(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 25 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkNotificationRequestType(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 26 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkNotificationTargetApproval(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 26 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkNotificationTargetImplementation(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 26 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkPaymentStatus(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 25 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkPaymentDueDate(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 25 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  setDateOnly(date_value) {
    var mydate = localDateOnly(date_value);
    return dateFormat(mydate, "dd/mm/yyyy");
  }

  getTaskCommentDetails = () => {
    this.setState({ invalid_access: false });
    API.get(`/api/tasks/any/${this.state.task_id}`)
      .then((res) => {
        if (res.data.data.discussionExists) {
          API.get(`api/tasks/discussions/${this.state.task_id}`)
            .then((res) => {
              this.setState({
                discussDetails: res.data.data.discussion,
                discussTaskRef: res.data.data.task_ref,
                discussReqName: res.data.data.req_name,
                discussFile: res.data.data.files,
              });
            })
            .catch((err) => {
              console.log(err);
            });

          if (res.data.data.taskDetails.language != "en") {
            API.get(`api/tasks/translated_discussions/${this.state.task_id}`)
              .then((res) => {
                this.setState({
                  discussDetailsT: res.data.data.discussion,
                  discussTaskRefT: res.data.data.task_ref,
                  discussReqNameT: res.data.data.req_name,
                  discussFileT: res.data.data.files,
                });
              })
              .catch((err) => {
                console.log(err);
              });
          }
        }

        let lang_data = require(`../../assets/lang/${res.data.data.taskDetails.language}.json`);

        this.setState({ dynamic_lang: lang_data });

        //console.log(res.data.data.taskDetails);

        this.setState({
          taskDetails: res.data.data.taskDetails,
          multiTaskDetailsFiles: res.data.data.multi,
          taskDetailsFiles: res.data.data.taskDetailsFiles,
          comments: res.data.data.comments,
          subTasks: res.data.data.subTasks,
          commentExists: res.data.data.commentExists,
          discussionExists: res.data.data.discussionExists,
          showDisplayCheckBox: res.data.data.showDisplayCheckBox,
        });

        // // FOR SAP PAYMENT
        if (res.data.data.taskDetails.salesDetails != undefined) {
          this.setState({
            invoice_list: res.data.data.taskDetails.salesDetails,
            order_number:
              res.data.data.taskDetails.salesDetails[0].sales_order_number,
          });
        }

        // SATYAJIt CQT
        if (res.data.data.taskDetails.cqt > 0) {
          this.setState({ CQT_loader: true });

          let taskId = res.data.data.taskDetails.task_id;
          let cqtId = res.data.data.taskDetails.cqt;

          API.get(`/api/drl/${taskId}/${cqtId}`)
            .then((cqtResponse) => {
              this.setState({
                CQT_details: cqtResponse.data.data,
                CQT_loader: false,
              });
            })
            .catch((err) => {
              this.setState({
                CQT_loader: false
              });
              console.log(err);
            });
        }

        //STOP THIS CALL FOR ALLOCATED TASK
        // var post_data = {};
        // API.put(`/api/tasks/status/${this.state.task_id}`, post_data)
        // .then((response) => {
        //   //console.log(response.data.data);
        //   //nextProps.refreshTableStatus(nextProps.currRow);
        // })
        // .catch((err) => {
        //   console.log(err);
        // });

        if (
          res.data.data.taskDetails.language != "en" &&
          res.data.data.taskDetails.parent_id == 0
        ) {
          API.get(`/api/tasks/any/translate/${this.state.task_id}`)
            .then((res) => {
              this.setState({
                taskDetailsTranslated: res.data.data.taskDetails,
                multiTaskDetailsTranslatedFiles: res.data.data.multi,
                taskDetailsTranslatedFiles: res.data.data.taskDetailsFiles,
                translate_api_complete: true,
                taskEdit: false,
                taskTranslateEdit: true,
              });

              API.get(`/api/employees/cc_emp/${this.state.task_id}`)
                .then((res) => {
                  var selectedTeam = [];
                  for (let index = 0; index < res.data.data.length; index++) {
                    const element = res.data.data[index];
                    selectedTeam.push({
                      value: element["employee_id"],
                      label:
                        element["first_name"] +
                        " " +
                        element["last_name"] +
                        " (" +
                        element["desig_name"] +
                        ")",
                    });
                  }
                  this.setState({ selectedArr: selectedTeam });

                  API.get(`/api/customers/cc_cust/${this.state.task_id}`)
                    .then((res) => {
                      var selectedCCTeam = [];
                      for (
                        let index = 0;
                        index < res.data.data.length;
                        index++
                      ) {
                        const element = res.data.data[index];
                        selectedCCTeam.push({
                          value: element["customer_id"],
                          label:
                            element["first_name"] + " " + element["last_name"],
                        });
                      }
                      this.setState({ custCCArr: selectedCCTeam });

                      API.get(`/api/employees/other`)
                        .then((res) => {
                          var myTeam = [];
                          for (
                            let index = 0;
                            index < res.data.data.length;
                            index++
                          ) {
                            const element = res.data.data[index];
                            myTeam.push({
                              value: element["employee_id"],
                              label:
                                element["first_name"] +
                                " " +
                                element["last_name"] +
                                " (" +
                                element["desig_name"] +
                                ")",
                            });
                          }
                          this.setState({
                            employeeArr: myTeam,
                            apiCompleted: true,
                          });
                        })
                        .catch((err) => {
                          console.log(err);
                        });
                    })
                    .catch((err) => {
                      console.log(err);
                    });
                })
                .catch((err) => {
                  console.log(err);
                });
            })
            .catch((err) => {
              var errText = "Invalid Access To This Page.";
              this.setState({ invalid_access: true });
              swal({
                closeOnClickOutside: false,
                title: "Error in page",
                text: errText,
                icon: "error",
              }).then(() => {
                //this.props.history.push('/');
              });
            });
        } else {
          API.get(`/api/employees/cc_emp/${this.state.task_id}`)
            .then((res) => {
              var selectedTeam = [];
              for (let index = 0; index < res.data.data.length; index++) {
                const element = res.data.data[index];
                selectedTeam.push({
                  value: element["employee_id"],
                  label:
                    element["first_name"] +
                    " " +
                    element["last_name"] +
                    " (" +
                    element["desig_name"] +
                    ")",
                });
              }
              this.setState({ selectedArr: selectedTeam });

              API.get(`/api/customers/cc_cust/${this.state.task_id}`)
                .then((res) => {
                  var selectedCCTeam = [];
                  for (let index = 0; index < res.data.data.length; index++) {
                    const element = res.data.data[index];
                    selectedCCTeam.push({
                      value: element["customer_id"],
                      label: element["first_name"] + " " + element["last_name"],
                    });
                  }
                  this.setState({ custCCArr: selectedCCTeam });

                  API.get(`/api/employees/other`)
                    .then((res) => {
                      var myTeam = [];
                      for (
                        let index = 0;
                        index < res.data.data.length;
                        index++
                      ) {
                        const element = res.data.data[index];
                        myTeam.push({
                          value: element["employee_id"],
                          label:
                            element["first_name"] +
                            " " +
                            element["last_name"] +
                            " (" +
                            element["desig_name"] +
                            ")",
                        });
                      }
                      this.setState({
                        employeeArr: myTeam,
                        apiCompleted: true,
                      });
                    })
                    .catch((err) => {
                      console.log(err);
                    });
                })
                .catch((err) => {
                  console.log(err);
                });
            })
            .catch((err) => {
              console.log(err);
            });
        }
      })
      .catch((err) => {
        console.log(err);
        var errText = "Invalid Access To This Page.";
        this.setState({ invalid_access: true });
        swal({
          closeOnClickOutside: false,
          title: "Error in page",
          text: errText,
          icon: "error",
        }).then(() => {
          //this.props.history.push('/');
        });
      });
  };

  getActivityLog = () => {
    API.get(`/api/tasks/any/activity_log/${this.state.task_id}`)
      .then((res) => {
        this.setState({ activity_log: res.data.data });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getTaskTranslation = () => {
    this.setState({ invalid_access: false });
  };

  activityLogTable = () => {
    if (this.state.activity_log.length > 0) {
      const comment_html = this.state.activity_log.map((act, i) => (
        <div className="comment-list-wrap" key={i}>
          <div className="comment">
            <div className="row">
              <div className="col-md-10 col-sm-10 col-xs-12">
                <div className="imageArea">
                  <div className="imageBorder">
                    <img alt="noimage" src={commenLogo} />
                  </div>
                </div>
                <div className="conArea">
                  <span>{act.description}</span>
                </div>
                <div className="clearfix" />
              </div>
              <div className="col-md-2 col-sm-2 col-xs-12">
                <div className="dateArea">
                  <p>{act.display_date_added}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      ));
      return (
        <>
          <div className="disHead">
            <label>Activity Logs:</label>
          </div>
          <div className="comment-list-main-wrapper">{comment_html}</div>
        </>
      );
    }
  };

  componentDidMount = () => {
    if (this.state.task_id > 0) {
      //this.props.getNotificationCount();
      this.getTaskCommentDetails();
      if (this.props.location.hash === "#discussion") {
        this.setState({ taskDetailsClass: "", discussionClass: "active" });
      } else {
        this.setState({ taskDetailsClass: "active", discussionClass: "" });
      }

      // if(this.props &&  this.props.location.state.fromNotification === 2){
      //   this.setState({taskDetailsClass:'active',discussionClass:''});
      // }else{
      //   this.setState({taskDetailsClass:'',discussionClass:'active'});
      // }
    }
  };

  createAccordion = () => {
    let accordion = [];
    var sub_tasks = this.state.subTasks;

    for (let index = 0; index < sub_tasks.length; index++) {
      accordion.push(
        <Panel eventKey={sub_tasks[index].task_id} key={index}>
          <Panel.Heading>
            <Panel.Title toggle>{sub_tasks[index].task_ref}</Panel.Title>
          </Panel.Heading>
          <Panel.Body collapsible>
            <Col xs={12} sm={6} md={4}>
              <div className="form-group">
                <label>Task:</label>
                {sub_tasks[index].discussion === 1 && (
                  <Link
                    to={{
                      pathname:
                        "/user/discussion_details/" + sub_tasks[index].parent_id,
                    }}
                    target="_blank"
                    style={{ cursor: "pointer" }}
                  >
                    {sub_tasks[index].task_ref}
                  </Link>
                )}
                {sub_tasks[index].discussion !== 1 && (
                  <Link
                    to={{
                      pathname: `/user/task_details/${sub_tasks[index].task_id}/${sub_tasks[index].assignment_id}`,
                    }}
                    target="_blank"
                    style={{ cursor: "pointer" }}
                  >
                    {sub_tasks[index].task_ref}
                  </Link>
                )}
              </div>
            </Col>
            <Col xs={12} sm={6} md={4}>
              <div className="form-group">
                <label>Created:</label>
                {dateFormat(localDate(sub_tasks[index].date_added), "dd/mm/yyyy")}
              </div>
            </Col>
            {sub_tasks[index].first_name !== "" && (
              <Col xs={12} sm={6} md={4}>
                <div className="form-group">
                  <label>User Name:</label>
                  {`${sub_tasks[index].first_name + " " + sub_tasks[index].last_name
                    }`}
                </div>
              </Col>
            )}
            {sub_tasks[index].assigned_by === -1 && (
              <Col xs={12} sm={6} md={4}>
                <div className="form-group">
                  <label>Assigned By:</label>
                  System
                </div>
              </Col>
            )}
            {sub_tasks[index].assigned_by > 0 && (
              <Col xs={12} sm={6} md={4}>
                <div className="form-group">
                  <label>Assigned By:</label>
                  {`${sub_tasks[index].assigned_by_first_name +
                    " " +
                    sub_tasks[index].assigned_by_last_name +
                    " (" +
                    sub_tasks[index].assign_by_desig_name +
                    ")"
                    }`}
                </div>
              </Col>
            )}
            <Col xs={12} sm={6} md={4}>
              <div className="form-group">
                <label>Assigned To:</label>
                {`${sub_tasks[index].assigned_to_first_name +
                  " " +
                  sub_tasks[index].assigned_to_last_name +
                  " (" +
                  sub_tasks[index].assign_to_desig_name +
                  ")"
                  }`}
              </div>
            </Col>
            <Col xs={12} sm={12} md={12}>
              <div className="form-group">
                <label>Description:</label>

                {sub_tasks[index].parent_id > 0 &&
                  ReactHtmlParser(sub_tasks[index].title)}
                {sub_tasks[index].parent_id === 0 &&
                  ReactHtmlParser(sub_tasks[index].req_name)}
              </div>
            </Col>
            {sub_tasks[index].genpact_checklist != null && sub_tasks[index].genpact_checklist.length > 0 && (
              <>
              <Col xs={12} sm={12} md={12}>
                <h4>Genpact Sub Task Details: {(sub_tasks[index].genpact_type == 1 && sub_tasks[index].genpact_type != null) ? "Related to Sales Order" : "Related to STO"}</h4>
              </Col>

              {this.getGenpactDynamicHTML(sub_tasks[index].genpact_checklist)}
              </>
            )}
          </Panel.Body>
        </Panel>
      );
    }

    return accordion;
  };

  getGenpactDynamicHTML = (genpact_checklist) => {
    const genpact_html = genpact_checklist.map((act, i) => (
      <Col xs={12} sm={6} md={4}>
              <div className="form-group">
                <label>{act.field_title}:</label>
                {act.field_value}
              </div>
            </Col>
    ));
    return genpact_html;
  }

  openCQTattachment = (filepath) => {
    API.get(filepath)
      .then((res) => {
        window.open(res.data.data, "_blank");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getComments = () => {
    if (this.state.comments.length > 0) {
      const comment_html = this.state.comments.map((comm, i) => (
        <div className="comment-list-wrap" key={i}>
          <div className="comment">
            <div className="row">
              <div className="col-md-10 col-sm-10 col-xs-12">
                <div className="imageArea">
                  <div className="imageBorder">
                    {comm.profile_pic != "" && comm.profile_pic != null ? (
                      <img alt="noimage" src={comm.profile_pic} />
                    ) : (
                      <img alt="noimage" src={commenLogo} />
                    )}
                  </div>
                </div>
                <div className="conArea">
                  <p>
                    {comm.posted_from == "S"
                      ? `${comm.first_name} ${comm.last_name} (Sales Force)`
                      : `${comm.first_name} ${comm.last_name}`}
                  </p>
                  <span>{ReactHtmlParser(htmlDecode(comm.comment))}</span>
                </div>
                <div className="clearfix" />
              </div>
              <div className="col-md-2 col-sm-2 col-xs-12">
                <div className="dateArea">
                  <p>
                    {dateFormat(localDate(comm.post_date), "ddd, mmm dS, yyyy")}
                  </p>
                </div>
              </div>
            </div>
            {this.getCommentFile(comm.uploads, comm.multi)}
          </div>
        </div>
      ));

      return (
        <>
          <div className="disHead">
            <label>Task Comments:</label>
          </div>
          <div className="comment-list-main-wrapper">{comment_html}</div>
        </>
      );
    }
  };

  getCommentFile = (uploads, multi) => {
    if (typeof uploads === "undefined") {
      return "";
    } else {
      return (
        <ul className="conDocList">
          {uploads.map((files, j) => (
            <li key={j}>
              {/* <a href={`${comm_path}${files.tcu_id}`} target="_blank" download rel="noopener noreferrer">
                  {files.actual_file_name}
                </a> */}
              {process.env.NODE_ENV === "development" ? (
                <LinkWithTooltip
                  // tooltip={`/emp_uploads/${files.new_file_name}`}
                  href="#"
                  id="tooltip-1"
                  clicked={(e) =>
                    this.redirectUrlTask(
                      e,
                      `${s3bucket_comment_diss_path}${files.tcu_id}`
                    )
                  }
                >
                  {files.actual_file_name}
                </LinkWithTooltip>
              ) : (
                <LinkWithTooltip
                  // tooltip={`/emp_uploads/${files.new_file_name}`}
                  href="#"
                  id="tooltip-1"
                  clicked={(e) =>
                    this.redirectUrlTask(
                      e,
                      `${s3bucket_comment_diss_path}${files.tcu_id}`
                    )
                  }
                >
                  {files.actual_file_name}
                </LinkWithTooltip>
              )}
            </li>
          ))}
          {uploads.length > 1 && (
            <li style={{ background: "none", padding: "0px", margin: "0px" }}>
              <span>
                <LinkWithTooltipZip
                  tooltip={`Click here to download all files`}
                  href={`${multi}`}
                  id="tooltip-downlaod_alll"
                  clicked={(e) => this.checkHandler(e)}
                >
                  <i
                    className="fas fa-arrow-circle-down"
                    style={{ fontSize: "17px" }}
                  ></i>
                </LinkWithTooltipZip>
              </span>
            </li>
          )}
        </ul>
      );
    }
  };

  getDiscussionFile = (uploads, multi) => {
    if (typeof uploads === "undefined") {
      return "";
    } else {
      return (
        <ul className="conDocList">
          {/* {console.log("NODE ENV", process.env.NODE_ENV)} */}
          {uploads.map((files, j) => (
            <li key={j}>
              {/* <a href={`${diss_path}${files.upload_id}`} target="_blank" download rel="noopener noreferrer">
                    {files.actual_file_name}
                  </a> */}

              {process.env.NODE_ENV === "development" ? (
                <LinkWithTooltip
                  // tooltip={`/client_uploads/${files.new_file_name}`}
                  href="#"
                  id="tooltip-1"
                  clicked={(e) =>
                    this.redirectUrlTask(
                      e,
                      `${s3bucket_diss_path}${files.upload_id}`
                    )
                  }
                >
                  {files.actual_file_name}
                </LinkWithTooltip>
              ) : (
                <LinkWithTooltip
                  // tooltip={`/client_uploads/${files.new_file_name}`}
                  href="#"
                  id="tooltip-1"
                  clicked={(e) =>
                    this.redirectUrlTask(
                      e,
                      `${s3bucket_diss_path}${files.upload_id}`
                    )
                  }
                >
                  {files.actual_file_name}
                </LinkWithTooltip>
              )}
            </li>
          ))}

          {uploads.length > 1 && (
            <li style={{ background: "none", padding: "0px", margin: "0px" }}>
              <span>
                <LinkWithTooltipZip
                  tooltip={`Click here to download all files`}
                  href={`${multi}`}
                  id="tooltip-downlaod_alll"
                  clicked={(e) => this.checkHandler(e)}
                >
                  <i
                    className="fas fa-arrow-circle-down"
                    style={{ fontSize: "17px" }}
                  ></i>
                </LinkWithTooltipZip>
              </span>
            </li>
          )}
        </ul>
      );
    }
  };

  showTranslation = (e, translated_comment) => {
    e.preventDefault();
    swal({
      closeOnClickOutside: false,
      title: "Translated Comment",
      text: translated_comment,
    });
  };

  getCQT = () => {
    if (
      this.state.CQT_details &&
      this.state.CQT_details !== "" &&
      this.state.CQT_loader === false
    ) {
      const { CQT_details } = this.state;

      return (
        <>
          <div className="boxPapanel content-padding">
            <div className="taskdetails">
              <div className="taskdetailsHeader">
                <div className="disHead">
                  <h4>CQT Details:</h4>
                </div>
                <div className="cqtDetailsDeta-btm">
                  {" "}
                  <div className="row">
                    <div className="col-md-4 col-sm-6 col-xs-12">
                      <div className="form-group">
                        <label>Task Reference Number : </label>
                        {CQT_details.task_reference}
                      </div>
                    </div>

                    <div className="col-md-4 col-sm-6 col-xs-12">
                      <div className="form-group">
                        <label>Title : </label>
                        {CQT_details.title}
                      </div>
                    </div>

                    <div className="col-md-4 col-sm-6 col-xs-12">
                      <div className="form-group">
                        <label>Product Name : </label>
                        {CQT_details.product_name}
                      </div>
                    </div>

                    <div className="col-md-4 col-sm-6 col-xs-12">
                      <div className="form-group">
                        <label>Country Name : </label>
                        {CQT_details.country_name}
                      </div>
                    </div>

                    <div className="col-md-4 col-sm-6 col-xs-12">
                      <div className="form-group">
                        <label>Query Category : </label>
                        {CQT_details.query_category}
                      </div>
                    </div>

                    <div className="col-md-4 col-sm-6 col-xs-12">
                      <div className="form-group">
                        <label>Query Description : </label>
                        {CQT_details.query_description}
                      </div>
                    </div>

                    <div className="col-md-4 col-sm-6 col-xs-12">
                      <div className="form-group">
                        <label>Criticality : </label>
                        {CQT_details.criticality}
                      </div>
                    </div>

                    <div className="col-md-4 col-sm-6 col-xs-12">
                      <div className="form-group">
                        <label>Response Trail : </label>
                        <p
                          dangerouslySetInnerHTML={{
                            __html: CQT_details.response_trail,
                          }}
                        />
                      </div>
                    </div>
                  </div>
                </div>

                {CQT_details.files && CQT_details.files.length > 0 && (
                  <>
                    <hr />
                    <div className="disHead">
                      <h4>CQT Attachment:</h4>
                    </div>
                    <div className="cqtDetailsDeta-attachment">
                      <div className="row">
                        {CQT_details.files[0] &&
                          CQT_details.files[0].length > 0 && (
                            <div className="col-md-6 col-sm-6 col-xs-12">
                              <div className="form-group">
                                <label>Uploaded from XCEED</label>
                              </div>
                              <ul className="cqtAttachment">
                                {CQT_details.files[0].map((ccp_file, w) => (
                                  <li key={w}>
                                    <div className="file-base">
                                      <div className="file-baseName">
                                        {ccp_file.OriginalFileName}
                                      </div>
                                      <div className="file-attactment">
                                        {/* <a href="" onClick={this.openCQTattachment(`${download_path}${file.ID}`)} download rel="noopener noreferrer"> */}

                                        <span
                                          onClick={(e) =>
                                            this.openCQTattachment(
                                              `${download_path}${ccp_file.ID}`
                                            )
                                          }
                                        >
                                          <img
                                            alt="Download Icon"
                                            src={downloadIcon}
                                          />
                                        </span>

                                        {/* </a> */}
                                      </div>
                                    </div>
                                  </li>
                                ))}
                              </ul>
                            </div>
                          )}

                        {CQT_details.files[1] &&
                          CQT_details.files[1].length > 0 && (
                            <div className="col-md-6 col-sm-6 col-xs-12">
                              <div className="form-group">
                                <label>Uploaded from CQT</label>
                              </div>
                              <ul className="cqtAttachment">
                                {CQT_details.files[1].map((drl_file, q) => (
                                  <li key={q}>
                                    <div className="file-base">
                                      <div className="file-baseName">
                                        {drl_file.name}{" "}
                                      </div>
                                      <div className="file-attactment">
                                        <a
                                          href={drl_file.link}
                                          target="_blank"
                                          download
                                          rel="noopener noreferrer"
                                        >
                                          <span>
                                            <img
                                              alt="Download Icon"
                                              src={downloadIcon}
                                            />
                                          </span>
                                        </a>
                                      </div>
                                    </div>
                                  </li>
                                ))}
                              </ul>
                            </div>
                          )}

                        {/* <ul className='cqtAttachment'>

                                {CQT_details.files.map((all_file, w) => (

                                  w === 0 ?
                                      all_file.map((file, p) => (
                                        
                                        <li key={p}>
                                          <div className="file-base">
                                            <div className="file-baseName">{file.OriginalFileName}</div>
                                            <div className="file-attactment">
                                                <span onClick={(e) => this.openCQTattachment(`${download_path}${file.ID}`)}>
                                                    <i className="fa fa-download"></i>
                                                </span>
                                            </div>
                                          </div>
                                        </li> 
                                    ))
                                  : 
                                      all_file.map((file, q) => (
                                          
                                          <li key={q}>
                                            <div className="file-base">
                                              <div className="file-baseName">{file.name} </div>
                                              <div className="file-attactment">                                                
                                                <a href={file.link} target="_blank" download rel="noopener noreferrer">
                                                  <span>
                                                      <i className="fa fa-download"></i>
                                                  </span>
                                                </a>
                                              </div>
                                            </div>
                                          </li> 
                                      ))
                                     
                                  ))
                                }

                          </ul> */}
                      </div>
                    </div>
                  </>
                )}
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <div className="loderOuter">
          <div className="loader">
            <img src={loaderlogo} alt="logo" />
            <div className="loading">Loading...</div>
          </div>
        </div>
      );
    }
  };

  handleOrderChange = (e, setFieldValue) => {
    setFieldValue("order_number", e.target.value);
    this.setState({ order_number: e.target.value });
  };

  shareToCustomer = (event) => {
    // SATYAJIT
    let checked = 0;
    if (event.target.checked) {
      checked = 1;
    }

    this.setState({
      sap_loader: true,
    });
    API.post(`api/bundle/share`, {
      order_number: this.state.order_number,
      task_id: this.state.task_id,
      checked: checked,
    })
      .then((res) => {
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: checked
            ? "Shared with customer successfully."
            : "Successfully updated",
          icon: "success",
        }).then(() => {
          this.setState({
            invoice_list: res.data.data,
            sap_loader: false,
          });
          //this.getTaskCommentDetails();
        });
      })
      .catch((err) => {
        var errText = "Invalid Access To This Page.";
        this.setState({ invalid_access: true, sap_loader: false });
        swal({
          closeOnClickOutside: false,
          title: "Error in sharing to customer",
          text: errText,
          icon: "error",
        }).then(() => { });
      });
  }; // SATYAJIT

  cancelSalesOrder = (setFieldValue) => {
    setFieldValue("order_number", "");
    this.setState({ order_number: "" });
  };

  getSalesRecord = (values, actions) => {
    let order_no = values.order_number;

    if (order_no !== "" && this.state.task_id > 0) {
      this.setState({
        sap_loader: true,
      });

      API.post(`api/bundle/search`, {
        order_number: order_no,
        task_id: this.state.task_id,
      })
        .then((res) => {
          this.setState({
            order_number: order_no,
            invoice_list: res.data.data,
            sap_loader: false,
          });
        })
        .catch((err) => {
          //console.log("node ERROR => ", err);
          actions.setErrors(err.data.errors);
          this.setState({
            sap_loader: false,
          });
        });
    }
  };

  saveSearchReasult = (actions) => {
    const { order_number, task_id } = this.state;

    if (order_number != "" && task_id > 0) {
      this.setState({
        sap_loader: true,
      });

      API.post(`api/bundle/save_search`, {
        order_number: order_number,
        task_id: task_id,
      })
        .then((res) => {
          this.setState({
            invoice_list: res.data.data,
            sap_loader: false,
          });
        })
        .catch((err) => {
          //console.log( 'node error ===>', err );
          actions.setErrors(err.data.errors);
          this.setState({
            sap_loader: false,
          });
        });
    }
  };

  getSAP = () => {
    return "";

    const { invoice_list, sap_loader } = this.state; // SATYAJIT

    return (
      <>
        <div className="boxPapanel content-padding">
          <div className="taskdetails">
            <div className="taskdetailsHeader">
              <div className="disHead">
                <h4>SAP</h4>
              </div>
              <div className="cqtDetailsDeta-btm">
                <Formik
                  initialValues={sapInitialValues}
                  validationSchema={sapSchema}
                  onSubmit={this.getSalesRecord}
                >
                  {({
                    values,
                    errors,
                    touched,
                    isValid,
                    isSubmitting,
                    setFieldValue,
                    setFieldTouched,
                  }) => {
                    return (
                      <Form className="cqtDetailsDeta-serach-form">
                        <div className="cqtDetailsDeta-serach form-inline">
                          <div className="form-group">
                            <label>Sales Order Number:</label>
                          </div>
                          <div className="form-group">
                            <Field
                              name="order_number"
                              type="text"
                              className={`form-control`}
                              placeholder="order number"
                              autoComplete="off"
                              value={values.order_number}
                              onChange={(e) => {
                                this.handleOrderChange(e, setFieldValue);
                              }}
                            />

                            {errors.order_number && touched.order_number ? (
                              <span className="errorMsg">
                                {errors.order_number}
                              </span>
                            ) : null}
                          </div>

                          <div className="btn-group">
                            <input
                              className="btn-line"
                              type="button"
                              value="Cancel"
                              onClick={() =>
                                this.cancelSalesOrder(setFieldValue)
                              }
                            />
                            <input
                              className="btn-fill"
                              type="submit"
                              value="Search"
                            />
                          </div>
                        </div>
                      </Form>
                    );
                  }}
                </Formik>

                {sap_loader === true && (
                  <div className="loderOuter">
                    <div className="loader">
                      <img src={loaderlogo} alt="logo" />
                      <div className="loading">Loading...</div>
                    </div>
                  </div>
                )}

                {/* Dispaly Result Start*/}
                <div className="cqtDetailsDeta_result">
                  {/* Dispaly Result Row*/}

                  {invoice_list &&
                    invoice_list.map((invoiceRecord, i) => {
                      return (
                        <div key={i} className="cqtDeta_result_row">
                          {invoiceRecord.old_invoice > 0 && (
                            <div className="switch-group">
                              <span>
                                {invoiceRecord.share_to_customer === 1
                                  ? "Shared with customer"
                                  : "Share with customer"}
                              </span>
                              <label className="switch ">
                                <input
                                  type="checkbox"
                                  name="isChecked"
                                  checked={
                                    invoiceRecord.share_to_customer === 1
                                      ? true
                                      : false
                                  }
                                  onChange={(e) => this.shareToCustomer(e)}
                                  value={
                                    invoiceRecord.share_to_customer === 1
                                      ? true
                                      : false
                                  }
                                />
                                <span className="slider-x round"></span>
                              </label>
                            </div>
                          )}

                          <div className="cqtDeta_result_row_overflow">
                            <div className="cqtDeta_result_row_top">
                              <div className="row">
                                <div className="col-md-4 col-sm-4 col-xs-12">
                                  <p>
                                    <span>Credit Block Status :</span>{" "}
                                    {invoiceRecord.credit_block_status}{" "}
                                  </p>
                                </div>
                                <div className="col-md-4 col-sm-4 col-xs-12">
                                  <p>
                                    <span>Delivery Block Reason :</span>{" "}
                                    {invoiceRecord.delivery_block_reason}
                                  </p>
                                </div>
                                <div className="col-md-4 col-sm-4 col-xs-12">
                                  <p>
                                    <span>Order Status :</span>{" "}
                                    {invoiceRecord.order_status === "A"
                                      ? "Not yet processed"
                                      : invoiceRecord.order_status === "B"
                                        ? "Partially processed"
                                        : "Completely processed"}
                                  </p>{" "}
                                </div>
                              </div>
                              <div className="row">
                                <div className="col-md-4 col-sm-4 col-xs-12">
                                  <p>
                                    <span>Sales Order Number :</span>{" "}
                                    {invoiceRecord.sales_order_number}
                                  </p>
                                </div>
                                <div className="col-md-4 col-sm-4 col-xs-12">
                                  <p>
                                    <span>Sales Order Amount :</span>{" "}
                                    {invoiceRecord.sales_order_amount}{" "}
                                  </p>
                                </div>
                                <div className="col-md-4 col-sm-4 col-xs-12">
                                  <p>
                                    <span>Pl Status :</span>{" "}
                                    {invoiceRecord.pi_status}
                                  </p>{" "}
                                </div>
                              </div>
                              <div className="row">
                                <div className="col-md-4 col-sm-4 col-xs-12">
                                  <p>
                                    <span>Request Date of Delivery :</span>{" "}
                                    {invoiceRecord.requested_delivery_date}
                                  </p>
                                </div>
                              </div>
                            </div>
                            <div className="cqtDeta_result_row_btm">
                              <div className="table-responsive">
                                <table className="table table-hover">
                                  <thead className="thead-dark">
                                    <tr>
                                      <th>Invoice Number</th>
                                      <th>
                                        Invoice Amount <span>(Curr.)</span>
                                      </th>
                                      <th>Payment Due Date</th>
                                      <th>Dispatch Date</th>
                                      <th>Payment Status</th>
                                      <th>Days Remaining</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    {invoiceRecord.invoice.length > 0 ? (
                                      invoiceRecord.invoice.map(
                                        (invoice, k) => {
                                          return (
                                            <tr key={k}>
                                              <td>{invoice.invoice_number}</td>
                                              <td>{invoice.invoice_amount}</td>
                                              <td>{invoice.due_date}</td>
                                              <td>{invoice.dispatch_date}</td>
                                              <td>{invoice.payment_status}</td>
                                              <td>{invoice.days_remaining}</td>
                                            </tr>
                                          );
                                        }
                                      )
                                    ) : (
                                      <tr>
                                        <td colSpan="6"> Not processed yet </td>
                                      </tr>
                                    )}
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>

                          {invoiceRecord.old_invoice === 0 && (
                            <div className="button-footer">
                              <button
                                type="submit"
                                className="btn-fill btn btn-default"
                                onClick={this.saveSearchReasult}
                              >
                                {" "}
                                Save{" "}
                              </button>
                            </div>
                          )}
                        </div>
                      );
                    })}

                  {/* Dispaly Result Row*/}
                </div>
                {/* Dispaly Result End*/}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  };

  handleChange = (e, field) => {
    this.setState({
      [field]: e.target.value,
    });
  };

  splitQuantity = (quantity) => {
    if (quantity) {
      return quantity.split(" ");
    }
  };

  submitDiscussion = (values, actions) => {
    this.setState({ loadDiscussion: true });
    let tid = this.props.match.params.id;
    //console.log('files',this.state.files);
    var formData = new FormData();

    if (this.state.files && this.state.files.length > 0) {
      for (let index = 0; index < this.state.files.length; index++) {
        const element = this.state.files[index];
        formData.append("file", element);
      }
      //formData.append('file', this.state.file);
    } else {
      formData.append("file", "");
    }

    formData.append("comment", this.state.disscussion_comment.trim());

    const config = {
      headers: {
        "content-type": "multipart/form-data",
      },
    };

    if (tid) {
      API.post(`api/tasks/discussions/${tid}`, formData, config)
        .then((res) => {
          this.getTaskCommentDetails(tid);

          values.comment = "";
          this.setState({ files: [], filesHtml: "", disscussion_comment: "" });

          this.setState({ loadDiscussion: false });
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Comment posted successfully.",
            icon: "success",
          });
        })
        .catch((err) => {
          this.setState({ loadDiscussion: false });
          if (err.data.status === 3) {
            var token_rm = 2;
            showErrorMessageFront(err, token_rm, this.props);
          } else {
            //console.log(err.data.errors);
            //actions.setErrors(err.data.errors);
            actions.setSubmitting(false);

            var err_text = "";

            if (
              err.data.errors &&
              err.data.errors.comment &&
              err.data.errors.comment != ""
            ) {
              err_text += err.data.errors.comment;
            } else if (
              err.data.errors &&
              err.data.errors.file &&
              err.data.errors.file != ""
            ) {
              err_text += err.data.errors.file;
            }

            swal({
              closeOnClickOutside: false,
              title: "Error",
              text: err_text,
              icon: "alert",
            });
          }
        });
    }
  };

  changeCCemp = (event, setFieldValue) => {
    if (event === null) {
      setFieldValue("employeeId", "");
    } else {
      setFieldValue("employeeId", event);
    }
  };

  submitCCemployee = (values, actions) => {
    this.setState({ loaderCCemp: true, selectedArr: [] });
    var post_data = {
      employeeId: values.employeeId,
    };

    API.post(`/api/employees/cc_emp/${this.state.task_id}`, post_data)
      .then((res) => {
        this.setState({
          selectedArr: res.data.selectedEmp,
          loaderCCemp: false,
        });
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: "Notification list has been updated.",
          icon: "success",
        }).then(() => { });
      })
      .catch((error) => {
        this.setState({ loaderCCemp: false });
        if (error.data.status === 3) {
          var token_rm = 2;
          showErrorMessageFront(error, token_rm, this.props);
        } else {
          actions.setErrors(error.data.errors);
          actions.setSubmitting(false);
        }
      });
  };

  checkHandler = (event) => {
    event.preventDefault();
  };

  getCCCust = () => {
    if (
      this.state.apiCompleted === true &&
      this.state.custCCArr &&
      this.state.custCCArr.length > 0
    ) {
      return (
        <Col xs={12}>
          <div className="cc_customer mb-20">
            <label>
              External Email notification{` `}
              <LinkWithTooltipText
                tooltip={`Select list of users within Customer’s organization who will receive email notification`}
                href="#"
                id={`tooltip-menu`}
                clicked={(e) => this.checkHandler(e)}
              >
                <img src={exclamationImage} alt="exclamation" />
              </LinkWithTooltipText>
            </label>

            {this.state.custCCArr.map((cust, index) => (
              <p key={index}>{cust.label}</p>
            ))}
          </div>
        </Col>
      );
    }
  };

  closeBrowserWindow = () => {
    window.close();
  };

  handleTabs = (event) => {
    if (event.currentTarget.className === "active") {
      //DO NOTHING
    } else {
      var elems = document.querySelectorAll('[id^="tab_"]');
      var elemsContainer = document.querySelectorAll('[id^="show_tab_"]');
      var currId = event.currentTarget.id;

      for (var i = 0; i < elems.length; i++) {
        elems[i].classList.remove("active");
      }

      for (var j = 0; j < elemsContainer.length; j++) {
        elemsContainer[j].style.display = "none";
      }
      event.currentTarget.classList.add("active");
      event.currentTarget.classList.add("active");

      if (
        currId === "tab_1" &&
        this.state.taskDetails.language !== "en" &&
        this.state.taskDetails.parent_id === 0
      ) {
        if (this.state.show_lang === "EN") {
          document.querySelector("#show_tab_5").style.display = "block";
        } else {
          document.querySelector("#show_tab_1").style.display = "block";
        }
      } else {
        document.querySelector("#show_" + currId).style.display = "block";
      }

      if (currId === "tab_4") {
        this.getActivityLog();
      }

      this.setState({ files: [], filesHtml: "" });
    }
    // this.tabElem.addEventListener("click",function(event){
    //     alert(event.target);
    // }, false);
  };

  handleTabs2 = (event) => {
    if (event.currentTarget.className === "active") {
      //DO NOTHING
    } else {
      var elems = document.querySelectorAll('[id^="d_tab_"]');
      var elemsContainer = document.querySelectorAll('[id^="show_d_tab_"]');
      var currId = event.currentTarget.id;

      for (var i = 0; i < elems.length; i++) {
        elems[i].classList.remove("active");
      }

      for (var j = 0; j < elemsContainer.length; j++) {
        elemsContainer[j].style.display = "none";
      }

      event.currentTarget.classList.add("active");
      event.currentTarget.classList.add("active");

      if (
        currId === "d_tab_1" &&
        this.state.taskDetails.language !== "en" &&
        this.state.taskDetails.parent_id === 0
      ) {
        if (this.state.show_lang_diss === "EN") {
          document.querySelector("#show_d_tab_3").style.display = "block";
        } else {
          document.querySelector("#show_d_tab_1").style.display = "block";
        }
      } else {
        document.querySelector("#show_" + currId).style.display = "block";
      }

      this.setState({ files: [], filesHtml: "" });
    }
    // this.tabElem.addEventListener("click",function(event){
    //     alert(event.target);
    // }, false);
  };

  handleEmployeeComments = (value, setFieldValue) => {
    setFieldValue("comment", value);
    this.setState({ emp_comment: value });
  };

  handleDiscussionComments = (value, setFieldValue) => {
    this.setState({ disscussion_comment: value });
    setFieldValue("comment", value);
  };

  // downloadFile = (e,file_id) => {
  //   e.preventDefault();
  //   if(file_id){
  //       API
  //       .get(`api/tasks/download/${file_id}`)
  //       .then(res => {
  //           return true;
  //       })
  //       .catch(err => {
  //           console.log(err);
  //       });
  //   }
  // }

  setDropZoneFiles = (acceptedFiles, setErrors, setFieldValue) => {
    //console.log(acceptedFiles);
    setErrors({ file_name: false });
    setFieldValue(this.state.files);

    var prevFiles = this.state.files;
    var newFiles;
    if (prevFiles.length > 0) {
      //newFiles = newConcatFiles = acceptedFiles.concat(prevFiles);

      for (let index = 0; index < acceptedFiles.length; index++) {
        var remove = 0;

        for (let index2 = 0; index2 < prevFiles.length; index2++) {
          if (acceptedFiles[index].name === prevFiles[index2].name) {
            remove = 1;
            break;
          }
        }

        //console.log('remove',acceptedFiles[index].name,remove);

        if (remove === 0) {
          prevFiles.push(acceptedFiles[index]);
        }
      }
      //console.log('acceptedFiles',acceptedFiles);
      //console.log('prevFiles',prevFiles);
      newFiles = prevFiles;
    } else {
      newFiles = acceptedFiles;
    }

    this.setState({
      files: newFiles,
    });

    var fileListHtml = newFiles.map((file,indf) => (
      <Alert key={file.name}>
        <span onClick={() => removeDropZoneFiles(file.name, this, setErrors)}>
          <i className="far fa-times-circle"></i>
        </span>{" "}
        {trimString(25, file.name)}
      </Alert>
    ));

    this.setState({
      filesHtml: fileListHtml,
    });
  };

  redirectUrlTask = (event, path) => {
    event.preventDefault();
    window.open(path, "_self");
  };

  openTaskResponseForm = (event) => {
    let request_body = "";
    if (event.target.value === "Accept") {
      this.setState({ action_button: 1, openCommentForm: true });
    } else if (event.target.value === "Decline") {
      this.setState({ action_button: 0, openCommentForm: true });
    }
  };

  closeTaskCQT = (values) => {
    if (
      this.state.taskDetails.cqt > 0 &&
      this.state.taskDetails.task_id > 0 &&
      values.cqt_task_reposne != "" &&
      this.state.action_button !== ""
    ) {
      const request_body = {
        title: this.state.CQT_details.title,
        task_id: this.state.taskDetails.task_id,
        cqt: this.state.taskDetails.cqt,
        action: this.state.action_button,
        cqt_response: values.cqt_task_reposne,
      };

      this.setState({ CQT_loader: true, openCommentForm: false });

      API.post(`/api/drl/close_task/`, request_body)
        .then((resp) => {
          this.setState({ CQT_loader: false, action_button: "" });
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: resp.data.data,
            icon: "success",
          });

          this.getTaskCommentDetails();
        })
        .catch((err) => {
          showErrorMessageFront(err, 2, this.props);
        });
    }
  };

  handleClose = () => {
    this.setState({ openCommentForm: false, action_button: "" });
  };

  changeDisplayDiscussion = (e) => {
    this.setState({ loaderCCemp: true });
    API.put(`/api/tasks/display_discussion/${this.state.taskDetails.task_id}`)
      .then((resp) => {
        this.setState({ loaderCCemp: false });
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: resp.data.data,
          icon: "success",
        });
      })
      .catch((err) => {
        showErrorMessageFront(err, 2, this.props);
      });
  };

  getDisplayDiscussionForm = () => {
    return (
      <>
        <label className="customCheckBox">
          Display Customer Discussions{" "}
          <input
            type="checkbox"
            name="display_discussion"
            value="1"
            onChange={(e) => this.changeDisplayDiscussion(e)}
            defaultChecked={
              this.state.taskDetails.display_discussion === 1 ? true : false
            }
          />
          <span className="checkmark-check"></span>
        </label>
      </>
    );
  };

  // SATYAJIT
  downloadPDF = () => {
    // set common data - SATYAJIT
    let task_ref = this.state.taskDetails.task_ref;
    let user_name = `${this.state.taskDetails.first_name}${" "}${this.state.taskDetails.last_name
      }`;
    let request_type = this.state.taskDetails.request_type;

    // assign data rows - SATYAJIT
    var rows = [];

    rows.push([`Customer Name : ${user_name}`]);

    rows.push([`Request Type : ${this.state.taskDetails.req_name}`]);

    if (request_type !== 24) {
      rows.push([`Product : ${this.state.taskDetails.product_name}`]);
      rows.push([
        `Market : ${ReactHtmlParser(
          htmlDecode(this.state.taskDetails.country_name).replace(
            /<(.|\n)*?>/g,
            ""
          )
        )}`,
      ]);
    }

    rows.push([
      `Due Date : ${this.setCreateDate(this.state.taskDetails.due_date)}`,
    ]);

    rows.push([
      `Requirement : ${ReactHtmlParser(
        htmlDecode(this.state.taskDetails.content).replace(/<(.|\n)*?>/g, "")
      )}`,
    ]);

    // set dynamic values - SATYAJIT
    if (request_type === 1) {
      var req_type_arr = this.state.taskDetails.service_request_type.split(",");

      if (req_type_arr.indexOf("Samples") !== -1) {
        if (this.state.taskDetails.number_of_batches != null) {
          rows.push([
            `Number Of Batches : ${this.state.taskDetails.number_of_batches}`,
          ]);
        }
        if (this.state.taskDetails.quantity != null) {
          rows.push([
            `Sample Quantity : ${this.splitQuantity(this.state.taskDetails.quantity)[0]
            } ${this.splitQuantity(this.state.taskDetails.quantity)[1]}`,
          ]);
        }
      }

      if (req_type_arr.indexOf("Working Standards") !== -1) {
        if (this.state.taskDetails.working_quantity != null) {
          rows.push([
            `Working Quantity : ${this.splitQuantity(this.state.taskDetails.working_quantity)[0]
            } ${this.splitQuantity(this.state.taskDetails.working_quantity)[1]
            }`,
          ]);
        }
      }

      if (req_type_arr.indexOf("Impurities") !== -1) {
        if (this.state.taskDetails.impurities_quantity != null) {
          rows.push([
            `Impurities Quantity : ${this.splitQuantity(this.state.taskDetails.impurities_quantity)[0]
            } ${this.splitQuantity(this.state.taskDetails.impurities_quantity)[1]
            }`,
          ]);
        }
        if (this.state.taskDetails.specify_impurity_required != null) {
          rows.push([
            `Specify Impurity Required : ${ReactHtmlParser(
              htmlDecode(
                this.state.taskDetails.specify_impurity_required
              ).replace(/<(.|\n)*?>/g, "")
            )}`,
          ]);
        }
      }

      rows.push([
        `Shipping Address : ${ReactHtmlParser(
          htmlDecode(this.state.taskDetails.shipping_address).replace(
            /<(.|\n)*?>/g,
            ""
          )
        )}`,
      ]);
    }

    if (
      request_type === 7 ||
      request_type === 23 ||
      request_type === 41 ||
      request_type === 34 ||
      request_type === 31
    ) {
      rows.push([
        `Quantity : ${this.splitQuantity(this.state.taskDetails.quantity)[0]} ${this.splitQuantity(this.state.taskDetails.quantity)[1]
        }`,
      ]);
    }

    if (
      request_type === 23 ||
      request_type === 39 ||
      request_type === 40 ||
      request_type === 41
    ) {
      request_type === 39 || request_type === 40
        ? rows.push([
          `Requested Deadline :  ${this.setDateOnly(
            this.state.taskDetails.rdd
          )}`,
        ])
        : rows.push([
          `Requested Date Of Delivery :  ${this.setDateOnly(
            this.state.taskDetails.rdd
          )}`,
        ]);
    }

    if (request_type === 7 || request_type === 34) {
      rows.push([
        `Nature Of Issue : ${this.state.taskDetails.nature_of_issue}`,
      ]);
      rows.push([`Batch Number : ${this.state.taskDetails.batch_number}`]);
    }

    if (
      request_type === 1 ||
      request_type === 22 ||
      request_type === 21 ||
      request_type === 19 ||
      request_type === 18 ||
      request_type === 17 ||
      request_type === 2 ||
      request_type === 3 ||
      request_type === 4 ||
      request_type === 10 ||
      request_type === 12 ||
      request_type === 30 ||
      request_type === 32 ||
      request_type === 33 ||
      request_type === 35 ||
      request_type === 36 ||
      request_type === 37
    ) {
      rows.push([
        `Pharmacopoeia : ${ReactHtmlParser(
          htmlDecode(this.state.taskDetails.pharmacopoeia).replace(
            /<(.|\n)*?>/g,
            ""
          )
        )}`,
      ]);
    }

    if (request_type === 2 || request_type === 3) {
      rows.push([
        `Polymorphic Form : ${ReactHtmlParser(
          htmlDecode(this.state.taskDetails.polymorphic_form).replace(
            /<(.|\n)*?>/g,
            ""
          )
        )}`,
      ]);
    }

    if (request_type === 13) {
      rows.push([
        `Stability Data Type : ${this.state.taskDetails.stability_data_type}`,
      ]);
    }

    if (request_type === 9) {
      rows.push([
        `Site Name : ${ReactHtmlParser(
          htmlDecode(this.state.taskDetails.audit_visit_site_name).replace(
            /<(.|\n)*?>/g,
            ""
          )
        )}`,
      ]);
      rows.push([
        `Audit/Visit Date : ${this.setDateOnly(
          this.state.taskDetails.request_audit_visit_date
        )}`,
      ]);
    }

    if (request_type == 27 || request_type === 39) {
      if (request_type === 39) {
        rows.push([
          `DMF/CEP : ${ReactHtmlParser(
            htmlDecode(this.state.taskDetails.dmf_number).replace(
              /<(.|\n)*?>/g,
              ""
            )
          )}`,
        ]);
      } else {
        rows.push([
          `DMF number : ${ReactHtmlParser(
            htmlDecode(this.state.taskDetails.dmf_number).replace(
              /<(.|\n)*?>/g,
              ""
            )
          )}`,
        ]);
      }
    }

    if (request_type === 40) {
      rows.push([
        `GMP Clearance ID : ${ReactHtmlParser(
          htmlDecode(this.state.taskDetails.gmp_clearance_id).replace(
            /<(.|\n)*?>/g,
            ""
          )
        )}`,
      ]);
      rows.push([
        `Email ID : ${ReactHtmlParser(
          htmlDecode(this.state.taskDetails.tga_email_id).replace(
            /<(.|\n)*?>/g,
            ""
          )
        )}`,
      ]);
      rows.push([
        `Applicant's Name & Address : ${ReactHtmlParser(
          htmlDecode(this.state.taskDetails.applicant_name).replace(
            /<(.|\n)*?>/g,
            ""
          )
        )}`,
      ]);
      rows.push([
        `Documents Required : ${ReactHtmlParser(
          htmlDecode(this.state.taskDetails.doc_required).replace(
            /<(.|\n)*?>/g,
            ""
          )
        )}`,
      ]);
    }

    if (request_type == 29) {
      rows.push([
        `Notification number : ${ReactHtmlParser(
          htmlDecode(this.state.taskDetails.notification_number).replace(
            /<(.|\n)*?>/g,
            ""
          )
        )}`,
      ]);
    }

    if (request_type == 28) {
      if (
        this.state.taskDetails.rdfrc == "" ||
        this.state.taskDetails.rdfrc == null
      ) {
        // do nothing
      } else {
        rows.push([
          `Requested date of response/closure : ${this.state.taskDetails.rdfrc}`,
        ]);
      }
    }

    if (request_type == 38) {
      rows.push([
        `Document Type : ${ReactHtmlParser(
          htmlDecode(this.state.taskDetails.apos_document_type).replace(
            /<(.|\n)*?>/g,
            ""
          )
        )}`,
      ]);
    }

    // submitted by - SATYAJIT
    if (this.state.taskDetails.ccp_posted_by > 0) {
      let emp_first_name = `${ReactHtmlParser(
        htmlDecode(this.state.taskDetails.emp_posted_by_first_name).replace(
          /<(.|\n)*?>/g,
          ""
        )
      )}`;
      let emp_last_name = `${ReactHtmlParser(
        htmlDecode(this.state.taskDetails.emp_posted_by_last_name).replace(
          /<(.|\n)*?>/g,
          ""
        )
      )}`;
      let emp_designation = `${this.state.taskDetails.emp_posted_by_desig_name}`;
      let emp_name = `${emp_first_name}${" "}${emp_last_name}${" "}(${emp_designation})`;
      rows.push([`Submitted By : ${emp_name}`]);
    }
    // submitted by - SATYAJIT
    if (this.state.taskDetails.submitted_by > 0) {
      let agent_first_name = `${ReactHtmlParser(
        htmlDecode(this.state.taskDetails.agnt_first_name).replace(
          /<(.|\n)*?>/g,
          ""
        )
      )}`;
      let agent_last_name = `${ReactHtmlParser(
        htmlDecode(this.state.taskDetails.agnt_last_name).replace(
          /<(.|\n)*?>/g,
          ""
        )
      )}`;
      let agent_name = `${agent_first_name}${" "}${agent_last_name}${" "}(Agent)`;
      rows.push([`Submitted By : ${agent_name}`]);
    }

    // create pdf
    const doc = new jsPDF();
    // set column
    var columns = [`Task Ref : ${task_ref}`];
    // create table
    doc.autoTable(columns, rows, {
      //columnStyles: { paddingRight: 50, width: 1024},
      margin: { top: 10 },
    });
    // // save the pdf
    doc.save(`${task_ref}.pdf`);
  };

  // getActions = (refObj,comment,t) => {
  //     return (
  //       <SPOCVerticalMenu
  //         id={comment.comment_id}
  //         currRow={comment}
  //         showApprovalPopup={refObj.showApprovalPopup}
  //         showOriginalCommentPopup={refObj.showOriginalCommentPopup}
  //         showTranslateCommentPopup={refObj.showTranslateCommentPopup}
  //         t={t}
  //       />
  //     );
  // };

  showSubTaskPopup = (currRow) => {
    //console.log("Create Sub Task");
    if (currRow.po_no === null && currRow.request_type == 23) {
      swal({
        closeOnClickOutside: false,
        title: "Warning !!",
        text: "Please set Purchase Order Number first.",
        icon: "warning",
      }).then(() => {

      });
    } else {
      this.setState({ showCreateSubTask: true, currRow: currRow });
    }
  };

  showCloneSubTaskPopup = (currRow) => {
    //console.log("Create Sub Task");
    this.setState({ showCloneCreateSubTask: true, currRow: currRow });
  };

  showIncreaseSlaPopup = (currRow) => {
    if (currRow.parent_id > 0) {
      this.setState({ showIncreaseSlaSubTask: true, currRow: currRow });
    } else {
      this.setState({ showIncreaseSla: true, currRow: currRow });
    }
  };

  showDeleteTaskPopup = (currRow) => {
    this.setState({ showDeleteTaskPopup: true, currRow: currRow });
  };

  showDeleteTaskPopupOld = (currRow) => {
    swal({
      closeOnClickOutside: false,
      title: "Cancel Request",
      text: "Are you sure you want to cancel this request?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.setState({ showModalLoader: true });
        API.delete(`/api/tasks/check_delete/${currRow.task_id}`)
          .then((res) => {
            if (res.data.has_sub_task === 1) {
              this.setState({ showModalLoader: false });
              swal({
                closeOnClickOutside: false,
                title: "Alert",
                text:
                  "This task has some open sub-tasks. \r\n Do you want to delete them as well?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              }).then((willDelete) => {
                if (willDelete) {
                  this.setState({ showModalLoader: true });
                  API.delete(`/api/tasks/delete/${currRow.task_id}`)
                    .then((res) => {
                      this.setState({ showModalLoader: false });
                      swal({
                        closeOnClickOutside: false,
                        title: "Success",
                        text: "Task has been canceled!",
                        icon: "success",
                      }).then(() => {
                        this.reloadTaskDetails();
                      });
                    })
                    .catch((err) => {
                      var token_rm = 2;
                      showErrorMessageFront(err, token_rm, this.props);
                    });
                }
              });
            } else {
              swal({
                closeOnClickOutside: false,
                title: "Success",
                text: "Task has been canceled!",
                icon: "success",
              }).then(() => {
                this.reloadTaskDetails();
              });
            }
          })
          .catch((err) => {
            var token_rm = 2;
            showErrorMessageFront(err, token_rm, this.props);
          });
      }
    });
  };

  showCloseTaskPopup = (currRow) => {
    swal({
      closeOnClickOutside: false,
      title: "Close task",
      text: "Are you sure you want to close this task?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.setState({ showModalLoader: true });
        API.delete(`/api/tasks/check_close/${currRow.task_id}`)
          .then((res) => {
            if (res.data.has_sub_task === 1) {
              this.setState({ showModalLoader: false });
              swal({
                closeOnClickOutside: false,
                title: "Alert",
                text:
                  "This task has some open sub-tasks. \r\n Do you want to close them as well?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              }).then((willDelete) => {
                if (willDelete) {
                  this.setState({ showModalLoader: true });
                  API.delete(`/api/tasks/close/${currRow.task_id}`)
                    .then((res) => {
                      this.setState({ showModalLoader: false });
                      swal({
                        closeOnClickOutside: false,
                        title: "Success",
                        text: "Task has been closed!",
                        icon: "success",
                      }).then(() => {
                        this.reloadTaskDetails();
                      });
                    })
                    .catch((err) => {
                      var token_rm = 2;
                      showErrorMessageFront(err, token_rm, this.props);
                    });
                }
              });
            } else {
              swal({
                closeOnClickOutside: false,
                title: "Success",
                text: "Task has been closed!",
                icon: "success",
              }).then(() => {
                this.reloadTaskDetails();
              });
            }
          })
          .catch((err) => {
            var token_rm = 2;
            showErrorMessageFront(err, token_rm, this.props);
          });
      }
    });
  };

  showAuthorizeTaskPopup = (currRow) => {
    //console.log("Authorize");
    this.setState({ showAuthorizeBack: true, currRow: currRow });
  };

  showAssignPopup = (currRow) => {
    //console.log("Assign Task");
    this.setState({ showAssign: true, currRow: currRow });
  };

  showCloneMainTaskPopup = (currRow) => {
    //console.log("Assign Task");
    this.setState({ showCreateTasks: true, currRow: currRow });
  };

  showRespondPopup = (currRow) => {
    //console.log("Respond");
    this.setState({ showRespondBack: true, currRow: currRow });
  };

  showCloneSubTaskTable = (currRow) => {
    this.setState({ showCreateSubTaskNew: true, currRow: currRow });
  };

  showProforma = (currRow) => {
    this.setState({ showProformaForm: true, currRow: currRow });
  };

  showPoke = (currRow) => {
    this.setState({ showPoke: true, currRow: currRow });
  };

  requestToReopen = (currRow) => {
    this.setState({ showReqToReopen: true, currRow: currRow });
  };

  showReAssignPopup = (currRow) => {
    this.setState({ showReAssign: true, currRow: currRow });
  };

  showRespondCustomerPopup = (currRow) => {
    //console.log("Respond Customer");

    API.get(`/api/tasks/get_respond_customer/${currRow.task_id}`).then(
      (res) => {
        let custResp;
        if (res.data.data.rs_id > 0) {
          custResp = {
            comment: htmlDecode(res.data.data.comment),
            delivery_date: localDate(res.data.data.expected_closure_date),
            status: res.data.data.status,
            action_req: res.data.data.required_action,
            po_number: res.data.data.po_number,
            pause_sla: res.data.data.pause_sla,
          };
        } else {
          custResp = {
            comment: "",
            delivery_date: "",
            status: "",
            action_req: "",
            po_number: "",
            pause_sla: 0,
          };
        }
        this.setState({
          showRespondCustomer: true,
          currRow: currRow,
          custResp: custResp,
        });
      }
    );
  };

  handleClose = (closeObj) => {
    this.setState(closeObj);
  };

  reloadTaskDetails = () => {
    window.location.href = window.location.href;
  };

  checkGenpactCheckList = (genpact, genpact_checklist) => {
    if (genpact === 1) {
      let genpact_checklist_arr = JSON.parse(genpact_checklist);
      return genpact_checklist_arr.map((checklist, index) => (
        <>
          <Col xs={12} sm={4} md={4} key={index}>
            <div className="form-group">
              <label>{checklist.field_title}</label>{" "}
              {(checklist.field_type === "text" ||
                checklist.field_type === "textarea") &&
                `${(checklist.selected_value != '' && checklist.selected_value != undefined) ? checklist.selected_value : '-'}`}
              {checklist.field_type === "dropdown" &&
                `${(checklist.selected_value.label != '' && checklist.selected_value.label != undefined) ? checklist.selected_value.label : "-"}`}
              {checklist.field_type === "date" &&
                dateFormat(new Date(checklist.selected_value), "dd/mm/yyyy")}
            </div>
          </Col>
        </>
      ));
    } else {
      return null;
    }
  };

  toggleLanguage = (e) => {
    //e.preventDefault();
    this.setState({ switchChecked: !this.state.switchChecked });

    var elems = document.querySelectorAll('[id^="tab_"]');
    for (var i = 0; i < elems.length; i++) {
      elems[i].classList.remove("active");
    }
    document.querySelector("#tab_1").classList.add("active");

    if (this.state.show_lang === "EN") {
      this.setState({ taskEdit: true, taskTranslateEdit: false });
      var elemsContainer = document.querySelectorAll('[id^="show_tab_"]');
      for (var j = 0; j < elemsContainer.length; j++) {
        elemsContainer[j].style.display = "none";
      }
      document.querySelector("#show_tab_1").style.display = "block";

      this.setState({
        show_lang: this.state.taskDetails.language.toUpperCase(),
      });
    } else {
      this.setState({ taskEdit: false, taskTranslateEdit: true });
      var elemsContainer = document.querySelectorAll('[id^="show_tab_"]');
      for (var j = 0; j < elemsContainer.length; j++) {
        elemsContainer[j].style.display = "none";
      }
      document.querySelector("#show_tab_5").style.display = "block";
      this.setState({ show_lang: "EN" });
    }
  };

  toggleDiscussion = (e) => {
    //e.preventDefault();
    this.setState({ switchCheckedDiss: !this.state.switchCheckedDiss });

    var elems = document.querySelectorAll('[id^="d_tab_"]');
    for (var i = 0; i < elems.length; i++) {
      elems[i].classList.remove("active");
    }
    document.querySelector("#d_tab_1").classList.add("active");

    if (this.state.show_lang_diss === "EN") {
      var elemsContainer = document.querySelectorAll('[id^="show_d_tab_"]');
      for (var j = 0; j < elemsContainer.length; j++) {
        elemsContainer[j].style.display = "none";
      }
      document.querySelector("#show_d_tab_1").style.display = "block";

      this.setState({
        show_lang_diss: this.state.taskDetails.language.toUpperCase(),
      });
    } else {
      var elemsContainer = document.querySelectorAll('[id^="show_d_tab_"]');
      for (var j = 0; j < elemsContainer.length; j++) {
        elemsContainer[j].style.display = "none";
      }
      document.querySelector("#show_d_tab_3").style.display = "block";
      this.setState({ show_lang_diss: "EN" });
    }
  };

  render() {
    //console.log(this.props.location.hash);
    //console.log(window.location.origin)
    //SVJT
    var my_id = getMyId();
    // SATYAJIT
    if (this.props.match.params.id === 0 || this.props.match.params.id === "") {
      return <Redirect to="/user/dashboard" />;
    }

    const { taskDetails } = this.state;

    if (taskDetails.task_id > 0 && this.state.dynamic_lang != "") {
      if (this.state.taskDetailsFiles.length > 0) {
        var fileList = this.state.taskDetailsFiles.map((file, k) => (
          <li key={k}>
            {/* <a href={`${path}${file.upload_id}`} target="_blank" download rel="noopener noreferrer">
              {file.actual_file_name}
            </a> */}
            {process.env.NODE_ENV === "development" ? (
              <LinkWithTooltip
                // tooltip={`/client_uploads/${file.new_file_name}`}
                href="#"
                id="tooltip-1"
                clicked={(e) =>
                  this.redirectUrlTask(
                    e,
                    `${s3bucket_task_diss_path}${file.upload_id}`
                  )
                }
              >
                {file.actual_file_name}
              </LinkWithTooltip>
            ) : (
              <LinkWithTooltip
                // tooltip={`/client_uploads/${file.new_file_name}`}
                href="#"
                id="tooltip-1"
                clicked={(e) =>
                  this.redirectUrlTask(
                    e,
                    `${s3bucket_task_diss_path}${file.upload_id}`
                  )
                }
              >
                {file.actual_file_name}
              </LinkWithTooltip>
            )}
          </li>
        ));
      }
      return (
        <>
          <div className="content-wrapper">
            <section className="content-header task-accept-decline-main-top">
              {/* Accept Decline Text*/}
              <h1>
                {taskDetails.req_name} | {taskDetails.task_ref}
              </h1>

              {taskDetails.cqt > 0 && (
                <div className="task-accept-decline-holder-top">
                  <ul>
                    <li>
                      Assigned to:{" "}
                      <span>
                        {
                          // CQT Assign to modification -- 07/03/2019 -- SATYAJIT
                          this.state.CQT_details.status ===
                            "Review pending with Initiator"
                            ? this.state.CQT_details.initiator_name
                            : this.state.CQT_details.assigned_to
                        }
                      </span>
                    </li>
                    <li>
                      Status: <span>{this.state.CQT_details.status}</span>
                    </li>
                  </ul>
                </div>
              )}
              {/* Accept Decline Text*/}

              <span
                className="task-details-language"
                style={{ color: "rgb(0, 86, 179)" }}
              >
                Please note you can only view the task details and cannot edit
                it since the task is not assigned to you.
              </span>
            </section>

            <section className="content switch-class-new">
              <div className="nav-tabs-custom nav-tabs-custom2">
                <ul className="nav nav-tabs">
                  <li
                    className={this.state.taskDetailsClass}
                    onClick={(e) => this.handleTabs(e)}
                    id="tab_1"
                  >
                    TASK DETAILS
                  </li>
                  {/* {this.state.taskDetails.language != 'en' && this.state.taskDetails.parent_id === 0 && 
                        <li
                          onClick={(e) => this.handleTabs(e)}
                          id="tab_5"
                        >
                        TASK DETAILS TRANSLATED
                      </li>
                  } */}
                  {this.state.commentExists &&
                    (this.state.taskDetails.cqt === null ||
                      this.state.taskDetails.cqt === 0) && (
                      <li onClick={(e) => this.handleTabs(e)} id="tab_2">
                        INTERNAL COMMENTS
                      </li>
                    )}
                  {this.state.discussionExists && (
                    <li
                      onClick={(e) => this.handleTabs(e)}
                      id="tab_3"
                      className={this.state.discussionClass}
                    >
                      CUSTOMER DISCUSSIONS
                    </li>
                  )}
                  <li onClick={(e) => this.handleTabs(e)} id="tab_4">
                    ACTIVITY LOG
                  </li>

                  {/* <li className="pull-right" style={{ padding: 0 }}>
                    {this.checkPricing(
                      taskDetails.request_type,
                      taskDetails.parent_id
                    ) === true && (
                      <Link
                        to={{
                          pathname:
                            "http://apps.mydrreddys.com/sites/Finance/APR/Lists/Review%20Request/DispForm.aspx?ID=566",
                        }}
                        className=""
                        style={{ display: "inline-block" }}
                        target="_blank"
                      >
                        Check Pricing Quote
                        <LinkWithTooltipText
                          tooltip={`Clicking will redirect you to pricing portal`}
                          href="#"
                          id={`tooltip-menu`}
                          clicked={(e) => this.checkHandler(e)}
                        >
                          <img src={exclamationImage} alt="exclamation" />
                        </LinkWithTooltipText>
                      </Link>
                    )}

                    {this.state.taskEdit && taskDetails.parent_id === 0 &&
                      taskDetails.request_type !== 25 &&
                      taskDetails.request_type !== 24 &&
                      taskDetails.request_type !== 26 &&
                      taskDetails.closed_status !== 1 &&
                      (getDashboard() === "BM" ||
                        getDashboard() === "CSC" ||
                        getDashboard() === "RA") && (
                        <Link
                          to={`/user/edit_task_details/${taskDetails.task_id}/${taskDetails.assignment_id}`}
                          className="btn-fill"
                          style={{
                            display: "inline-block",
                            marginRight: "-2px",
                          }}
                        >
                          Edit
                        </Link>
                      )}

                    {this.state.taskTranslateEdit &&  taskDetails.parent_id === 0 &&
                      taskDetails.request_type !== 25 &&
                      taskDetails.request_type !== 24 &&
                      taskDetails.request_type !== 26 &&
                      taskDetails.closed_status !== 1 &&
                      (getDashboard() === "BM" ||
                        getDashboard() === "CSC" ||
                        getDashboard() === "RA") && (
                        <Link
                          to={`/user/edit_translate_task_details/${taskDetails.task_id}/${taskDetails.assignment_id}`}
                          className="btn-fill"
                          style={{
                            display: "inline-block",
                            marginRight: "-2px",
                          }}
                        >
                          Edit
                        </Link>
                      )}      

                  </li> */}
                  {/* <li className="pull-right" style={{ padding: 6 }}>
                    {taskDetails.request_type !== 25 &&
                      taskDetails.request_type !== 24 &&
                      taskDetails.request_type !== 26 &&
                      taskDetails.closed_status !== 1 && (
                        <div
                          className="clearfix tdBtmlist"
                          style={{
                            right: "25%",
                            position: "absolute",
                          }}
                        >
                          {this.getActions(taskDetails)}
                        </div>
                      )}
                  </li> */}

                  {/* {this.state.taskDetails.parent_id === 0 && <li className="pull-right downloadSec">

                  <span onClick={(e) => this.downloadPDF()} >
                      <LinkWithTooltip
                          tooltip={`Click here to download excel`}
                          href="#"
                          id="tooltip-my"
                          clicked={e => this.checkHandler(e)}
                      >
                          <i className="fas fa-download"></i>
                      </LinkWithTooltip>
                  </span>
                  </li>} */}

                  {this.state.taskDetails.language != "en" &&
                    this.state.taskDetails.parent_id === 0 && (
                      <li
                        className="pull-right imp-right"
                        style={{ padding: 6 }}
                      >
                        <Switch
                          checked={this.state.switchChecked}
                          onChange={(e) => this.toggleLanguage(e)}
                          onColor="#7145d3"
                          uncheckedIcon={"EN"}
                          checkedIcon={this.state.taskDetails.language.toUpperCase()}
                          className="react-switch"
                          id="icon-switch"
                        />
                      </li>
                    )}
                </ul>
              </div>
              <div className="tab-content">
                <div
                  className={`tab-pane ${this.state.taskDetails.language != "en" &&
                      this.state.taskDetails.parent_id === 0
                      ? ""
                      : "active"
                    }`}
                  id="show_tab_1"
                >
                  {taskDetails.parent_id > 0 && (
                    <div className="boxPapanel content-padding task-accept-decline-main">
                      {this.state.CQT_details &&
                        this.state.CQT_details.status ===
                        "Review pending with Initiator" && (
                          <div className="task-accept-decline-holder">
                            <input
                              className="btn-line"
                              type="button"
                              value="Decline"
                              onClick={(e) => this.openTaskResponseForm(e)}
                            />
                            <input
                              className="btn-fill"
                              type="button"
                              value="Accept"
                              onClick={(e) => this.openTaskResponseForm(e)}
                            />
                          </div>
                        )}

                      {this.state.action_button !== "" && (
                        <div>
                          <Modal
                            show={this.state.openCommentForm}
                            onHide={() => this.handleClose()}
                            backdrop="static"
                          >
                            <Formik
                              initialValues={cqtInitialValues}
                              validationSchema={validateCqtFlag}
                              onSubmit={this.closeTaskCQT}
                            >
                              {({
                                values,
                                errors,
                                touched,
                                isValid,
                                isSubmitting,
                                setFieldValue,
                              }) => {
                                return (
                                  <Form>
                                    <Modal.Header closeButton>
                                      <Modal.Title>
                                        Respond CQT Task
                                      </Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>
                                      <div className="contBox">
                                        <Row>
                                          <Col xs={12} sm={12} md={12}>
                                            <div className="form-group">
                                              {/* <Field
                                                  name="cqt_task_reposne"
                                                  component="textarea"
                                                  className={`selectArowGray form-control`}
                                                  autoComplete="off"
                                                  value={values.cqt_task_reposne}
                                                >
                                                </Field> */}

                                              <Editor
                                                name="cqt_task_reposne"
                                                className={`selectArowGray form-control`}
                                                value={
                                                  values.cqt_task_reposne !==
                                                    null &&
                                                    values.cqt_task_reposne !== ""
                                                    ? values.cqt_task_reposne
                                                    : ""
                                                }
                                                content={
                                                  values.cqt_task_reposne !==
                                                    null &&
                                                    values.cqt_task_reposne !== ""
                                                    ? values.cqt_task_reposne
                                                    : ""
                                                }
                                                init={{
                                                  menubar: false,
                                                  branding: false,
                                                  placeholder: "Enter comments",
                                                  plugins:
                                                    "link table hr visualblocks code placeholder lists autoresize textcolor",
                                                  toolbar:
                                                    "bold italic strikethrough superscript subscript | forecolor backcolor | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | visualblocks code | fontselect",
                                                  font_formats:
                                                    "Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats",
                                                }}
                                                onEditorChange={(value) =>
                                                  setFieldValue(
                                                    "cqt_task_reposne",
                                                    value
                                                  )
                                                }
                                              />

                                              {errors.cqt_task_reposne &&
                                                touched.cqt_task_reposne ? (
                                                <span className="errorMsg">
                                                  {errors.cqt_task_reposne}
                                                </span>
                                              ) : null}
                                            </div>
                                          </Col>
                                        </Row>
                                      </div>
                                    </Modal.Body>
                                    <Modal.Footer>
                                      <button
                                        className={`btn btn-success btn-sm ${isValid
                                            ? "btn-custom-green"
                                            : "btn-disable"
                                          } m-r-10`}
                                        type="submit"
                                        disabled={isValid ? false : true}
                                      >
                                        {this.state.stopflagId > 0
                                          ? isSubmitting
                                            ? "Updating..."
                                            : "Update"
                                          : isSubmitting
                                            ? "Submitting..."
                                            : "Submit"}
                                      </button>
                                      <button
                                        onClick={this.handleClose}
                                        className={`btn btn-danger btn-sm`}
                                        type="button"
                                      >
                                        Close
                                      </button>
                                    </Modal.Footer>
                                  </Form>
                                );
                              }}
                            </Formik>
                          </Modal>
                        </div>
                      )}

                      <div className="taskdetails">
                        <div className="taskdetailsHeader">
                          <div className="commentBox1">
                            <div className="row">
                              <div className="col-sm-12 col-xs-12">
                                {/* taskdetails Panel Top Listing*/}
                                <div className="tdTplist">
                                  <ul>
                                    <li>
                                      <div className="form-group">
                                        <label>Task:</label>{" "}
                                        {taskDetails.task_ref}
                                      </div>
                                    </li>
                                    <li>
                                      <div className="form-group">
                                        <label>Created On:</label>{" "}
                                        {this.setCreateDate(
                                          taskDetails.date_added
                                        )}
                                      </div>
                                    </li>
                                    <li>
                                      <div className="form-group">
                                        <label>Days Pending:</label>{" "}
                                        {this.setDaysPending(
                                          taskDetails.assign_due_date
                                        )}
                                      </div>
                                    </li>
                                    <li>
                                      <div className="form-group">
                                        <label>Priority:</label>{" "}
                                        {this.setPriorityName(
                                          taskDetails.priority
                                        )}
                                      </div>
                                    </li>
                                    {taskDetails.parent_id > 0 &&
                                      taskDetails.discussion !== 1 &&
                                      taskDetails.sla_status !== 0 && (
                                        <li>
                                          <div className="form-group">
                                            <label>SLA Status:</label>{" "}
                                            {this.getSLAStatus(
                                              taskDetails.sla_status
                                            )}
                                          </div>
                                        </li>
                                      )}
                                    {this.state.showDisplayCheckBox === 1 && (
                                      <li>
                                        <div className="form-group">
                                          {this.getDisplayDiscussionForm()}
                                        </div>
                                      </li>
                                    )}
                                  </ul>
                                </div>
                                {/* taskdetails Panel Top Listing*/}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                  {/* MONOSOM START HERE */}
                  <div className="boxPapanel content-padding">
                    <div className="taskdetails">
                      {/* taskdetails Panel Btm Listing Start*/}

                      <div className="clearfix tdBtmlist">
                        <Row className="task_details_complete">

                          {taskDetails.curr_owner_fname !== "" && (
                            <Col xs={12} sm={6} md={4}>
                              <div className="form-group">
                                <label>
                                  {
                                    this.state.dynamic_lang.task_details
                                      .current_owner
                                  }
                                </label>{" "}
                                {taskDetails.curr_owner_fname}{" "}
                                {taskDetails.curr_owner_lname}{" "}
                                {taskDetails.curr_owner_desig}
                              </div>
                            </Col>
                          )}

                          {taskDetails.company_name !== "" && (
                            <Col xs={12} sm={6} md={4}>
                              <div className="form-group">
                                <label>
                                  {
                                    this.state.dynamic_lang.task_details
                                      .customer_name
                                  }
                                </label>{" "}
                                {taskDetails.company_name}{" "}
                              </div>
                            </Col>
                          )}

                          {(taskDetails.first_name !== "" ||
                            taskDetails.last_name !== "") && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .user_name
                                    }
                                  </label>{" "}
                                  {htmlDecode(taskDetails.first_name)}{" "}
                                  {htmlDecode(taskDetails.last_name)}
                                </div>
                              </Col>
                            )}

                          {this.checkProductRequest(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {this.state.dynamic_lang.task_details.product}
                                  </label>{" "}
                                  {htmlDecode(taskDetails.product_name)}
                                </div>
                              </Col>
                            )}

                          {this.checkMarketRequest(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {this.state.dynamic_lang.task_details.market}
                                  </label>{" "}
                                  {htmlDecode(taskDetails.country_name)}
                                </div>
                              </Col>
                            )}

                          {this.checkGMPRequest(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .gmp_clearance_id
                                    }
                                  </label>{" "}
                                  {htmlDecode(taskDetails.gmp_clearance_id)}
                                </div>
                              </Col>
                            )}

                          {this.checkTGAEmail(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .email_id
                                    }
                                  </label>{" "}
                                  {htmlDecode(taskDetails.tga_email_id)}
                                </div>
                              </Col>
                            )}

                          {this.checkApplicant(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .applicants_address
                                    }
                                  </label>{" "}
                                  {htmlDecode(taskDetails.applicant_name)}
                                </div>
                              </Col>
                            )}

                          {this.checkDocRequired(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .documents_required
                                    }
                                  </label>{" "}
                                  {htmlDecode(taskDetails.doc_required)}
                                </div>
                              </Col>
                            )}

                          {this.checkSamplesWorkingImpureRequest(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true &&
                            taskDetails.service_request_type.indexOf(
                              "Samples"
                            ) !== -1 && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .samples
                                    }
                                  </label>
                                  <p>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .number_of_batches
                                    }{" "}
                                    {taskDetails.number_of_batches}
                                  </p>
                                  {taskDetails.quantity && (
                                    <p>
                                      {
                                        this.state.dynamic_lang.task_details
                                          .quantity
                                      }{" "}
                                      {
                                        this.splitQuantity(
                                          taskDetails.quantity
                                        )[0]
                                      }{" "}
                                      {
                                        this.splitQuantity(
                                          taskDetails.quantity
                                        )[1]
                                      }
                                    </p>
                                  )}
                                </div>
                              </Col>
                            )}

                          {this.checkSamplesWorkingImpureRequest(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true &&
                            taskDetails.service_request_type.indexOf(
                              "Working Standards"
                            ) !== -1 && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .working_standards
                                    }
                                  </label>
                                  {taskDetails.working_quantity && (
                                    <p>
                                      {
                                        this.state.dynamic_lang.task_details
                                          .quantity
                                      }{" "}
                                      {
                                        this.splitQuantity(
                                          taskDetails.working_quantity
                                        )[0]
                                      }{" "}
                                      {
                                        this.splitQuantity(
                                          taskDetails.working_quantity
                                        )[1]
                                      }{" "}
                                    </p>
                                  )}
                                </div>
                              </Col>
                            )}

                          {this.checkSamplesWorkingImpureRequest(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true &&
                            taskDetails.service_request_type.indexOf(
                              "Impurities"
                            ) !== -1 && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .impurities
                                    }
                                  </label>
                                  <p>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .impurities_required
                                    }{" "}
                                    {htmlDecode(
                                      taskDetails.specify_impurity_required
                                    )}
                                  </p>

                                  {taskDetails.impurities_quantity && (
                                    <p>
                                      {
                                        this.state.dynamic_lang.task_details
                                          .quantity
                                      }{" "}
                                      {
                                        this.splitQuantity(
                                          taskDetails.impurities_quantity
                                        )[0]
                                      }{" "}
                                      {
                                        this.splitQuantity(
                                          taskDetails.impurities_quantity
                                        )[1]
                                      }{" "}
                                    </p>
                                  )}
                                </div>
                              </Col>
                            )}

                          {this.checkSamplesWorkingImpureRequest(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .shipping_address
                                    }
                                  </label>
                                  <p>
                                    {ReactHtmlParser(
                                      htmlDecode(taskDetails.shipping_address)
                                    )}
                                  </p>
                                </div>
                              </Col>
                            )}

                          {/* post */}
                          {this.checkPharmacopoeia(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {" "}
                                    {
                                      this.state.dynamic_lang.task_details
                                        .impurities
                                    }
                                  </label>{" "}
                                  {htmlDecode(taskDetails.pharmacopoeia)}
                                </div>
                              </Col>
                            )}

                          {this.checkPolymorphicForm(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {" "}
                                    {
                                      this.state.dynamic_lang.task_details
                                        .polymorphic_form
                                    }
                                  </label>{" "}
                                  {htmlDecode(taskDetails.polymorphic_form)}
                                </div>
                              </Col>
                            )}

                          {this.checkDMFNumber(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {taskDetails.request_type === 39
                                      ? this.state.dynamic_lang.task_details
                                        .dmf_cmp
                                      : this.state.dynamic_lang.task_details
                                        .dmf_number}
                                  </label>{" "}
                                  {htmlDecode(taskDetails.dmf_number)}
                                </div>
                              </Col>
                            )}

                          {/* post */}
                          {this.checkNotificationNumber(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .notification_number
                                    }
                                  </label>{" "}
                                  {htmlDecode(taskDetails.notification_number)}
                                </div>
                              </Col>
                            )}

                          {this.checkAposDocumentType(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .document_type
                                    }
                                  </label>{" "}
                                  {htmlDecode(taskDetails.apos_document_type)}
                                </div>
                              </Col>
                            )}

                          {taskDetails.parent_id === 0 && taskDetails.request_type != 43 && (
                            <Col xs={12} sm={6} md={4}>
                              <div className="form-group">
                                <label>
                                  {
                                    this.state.dynamic_lang.task_details
                                      .request_category
                                  }
                                </label>{" "}
                                {this.setReqCategoryName(
                                  taskDetails.request_category
                                )}
                              </div>
                            </Col>
                          )}

                          {/* post */}
                          {this.checkStabilityDataType(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .stability_data_type
                                    }
                                  </label>{" "}
                                  {taskDetails.stability_data_type}
                                </div>
                              </Col>
                            )}

                          {/* post */}
                          {this.checkAuditSiteName(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {" "}
                                    {
                                      this.state.dynamic_lang.task_details
                                        .site_name
                                    }
                                  </label>{" "}
                                  {htmlDecode(taskDetails.audit_visit_site_name)}
                                </div>
                              </Col>
                            )}

                          {/* post */}
                          {this.checkAuditVistDate(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .audit_visit_date
                                    }
                                  </label>{" "}
                                  {taskDetails.request_audit_visit_date !==
                                    null &&
                                    this.setDateOnly(
                                      taskDetails.request_audit_visit_date
                                    )}
                                </div>
                              </Col>
                            )}

                          {this.checkQuantity(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .quantity
                                    }
                                  </label>{" "}
                                  {taskDetails.quantity}
                                </div>
                              </Col>
                            )}

                          {this.checkRDD(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {taskDetails.request_type === 39 ||
                                      taskDetails.request_type === 40
                                      ? this.state.dynamic_lang.task_details
                                        .requested_deadline
                                      : this.state.dynamic_lang.task_details
                                        .requested_date_delivery}
                                  </label>{" "}
                                  {taskDetails.rdd !== null &&
                                    this.setDateOnly(taskDetails.rdd)}
                                </div>
                              </Col>
                            )}

                          {this.checkRDORC(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .requested_date_response_closure
                                    }
                                  </label>{" "}
                                  {taskDetails.rdfrc &&
                                    taskDetails.rdfrc !== null &&
                                    taskDetails.rdfrc}
                                </div>
                              </Col>
                            )}

                          {taskDetails.request_type === 43 && taskDetails.sales_order_no != null && (
                            <Col xs={12} sm={6} md={4}>
                              <div className="form-group forecast-image">
                                <label>
                                  {this.state.dynamic_lang.task_details.sales_order_no}
                                </label>{" "}
                                {ReactHtmlParser(htmlDecode(taskDetails.sales_order_no))}
                              </div>
                            </Col>
                          )}

                          {taskDetails.request_type === 43 && taskDetails.sales_order_date != null && (
                            <Col xs={12} sm={6} md={4}>
                              <div className="form-group forecast-image">
                                <label>
                                  {this.state.dynamic_lang.task_details.sales_order_date}
                                </label>{" "}
                                {ReactHtmlParser(htmlDecode(taskDetails.sales_order_date))}
                              </div>
                            </Col>
                          )}

                          {(taskDetails.request_type == 43 || taskDetails.request_type == 23) && taskDetails.po_no != null && (
                            <Col xs={12} sm={6} md={4}>
                              <div className="form-group forecast-image">
                                <label>
                                  {this.state.dynamic_lang.task_details.po_no}
                                </label>{" "}
                                {taskDetails.po_no ? taskDetails.po_no : ""}
                              </div>
                            </Col>
                          )}

                          {taskDetails.request_type === 43 && taskDetails.SAPDetails.city != '' && taskDetails.SAPDetails.city != null && (
                            <Col xs={12} sm={6} md={4}>
                              <div className="form-group forecast-image">
                                <label>
                                  {
                                    this.state.dynamic_lang.task_details
                                      .shipto_address
                                  }
                                </label>{" "}
                                <div>{ReactHtmlParser(htmlDecode(taskDetails.SAPDetails.shipto_name))}
                                </div>
                                {ReactHtmlParser(htmlDecode(taskDetails.SAPDetails.street)) + ', '}
                                {ReactHtmlParser(htmlDecode(taskDetails.SAPDetails.city)) + ', '}
                                {ReactHtmlParser(htmlDecode(taskDetails.SAPDetails.post_code)) + ', '}
                                {ReactHtmlParser(htmlDecode(taskDetails.SAPDetails.country)) + ', '}
                              </div>
                            </Col>
                          )}

                          {taskDetails.request_type === 43 && taskDetails.po_no != null && taskDetails.SAPDetails.po_file_date != null && (
                            <Col xs={12} sm={6} md={4}>
                              <div className="form-group forecast-image">
                                <label>
                                  {
                                    this.state.dynamic_lang.task_details
                                      .po_file_date
                                  }
                                </label>{" "}
                                {ReactHtmlParser(htmlDecode(taskDetails.SAPDetails.po_file_date))}
                              </div>
                            </Col>
                          )}

                          {taskDetails.request_type === 43 && taskDetails.po_no != null && taskDetails.SAPDetails.po_file_name != null && (
                            <Col xs={12} sm={6} md={4}>
                              <div className="form-group forecast-image">
                                <label>
                                  {
                                    this.state.dynamic_lang.task_details
                                      .po_file
                                  }
                                </label>{" "}
                                <a href={`${pourl}${taskDetails.sap_request_id}`} target="_blank">
                                  {ReactHtmlParser(htmlDecode(taskDetails.SAPDetails.po_file_name))}
                                </a>
                              </div>
                            </Col>
                          )}

                          {this.checkBatchNo(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .batch_number
                                    }
                                  </label>{" "}
                                  {taskDetails.batch_number}
                                </div>
                              </Col>
                            )}

                          {this.checkForecastNotificationDate(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .notification_date
                                    }
                                  </label>{" "}
                                  {this.setCreateDate(taskDetails.date_added)}
                                </div>
                              </Col>
                            )}

                          {this.checkNatureOfIssue(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .nature_of_issue
                                    }
                                  </label>{" "}
                                  {htmlDecode(taskDetails.nature_of_issue)}
                                </div>
                              </Col>
                            )}

                          {this.checkGenpactCheckList(
                            taskDetails.genpact,
                            taskDetails.genpact_checklist
                          )}



                          {/* {this.checkPricing(
                          taskDetails.request_type,
                          taskDetails.parent_id
                          ) === true && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <a target="_blank" href="http://apps.mydrreddys.com/sites/Finance/APR/Lists/Review%20Request/DispForm.aspx?ID=566">Check Pricing Quote</a>
                            </div>
                          </Col>
                        )} */}



                          {taskDetails.parent_id > 0 && (
                            <Col xs={12} sm={6} md={4}>
                              <div className="form-group">
                                <label>
                                  {this.state.dynamic_lang.task_details.title}
                                </label>{" "}
                                {ReactHtmlParser(taskDetails.title)}
                              </div>
                            </Col>
                          )}

                          {taskDetails.parent_id > 0 && (
                            <Col xs={12} sm={12} md={12}>
                              <div className="form-group">
                                <label>
                                  {this.state.dynamic_lang.task_details.content}
                                </label>{" "}
                                {ReactHtmlParser(
                                  htmlDecode(taskDetails.content)
                                )}
                              </div>
                            </Col>
                          )}

                          {this.checkAmountPending(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .amount_pending
                                    }
                                  </label>{" "}
                                  {taskDetails.payment_pending}
                                </div>
                              </Col>
                            )}

                          {this.checkDaysRemaining(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .days_remaining
                                    }
                                  </label>{" "}
                                  {taskDetails.days_remaining}
                                </div>
                              </Col>
                            )}

                          {this.checkPaymentStatus(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {this.state.dynamic_lang.task_details.status}
                                  </label>{" "}
                                  {taskDetails.payment_status}
                                </div>
                              </Col>
                            )}

                          {this.checkPaymentDueDate(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .due_date
                                    }
                                  </label>{" "}
                                  {this.setCreateDate(taskDetails.due_date)}
                                </div>
                              </Col>
                            )}

                          {this.checkNotificationRequestType(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .request_type
                                    }
                                  </label>{" "}
                                  {taskDetails.notification_type}
                                </div>
                              </Col>
                            )}

                          {this.checkNotificationTargetApproval(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .approval_feedback
                                    }
                                  </label>{" "}
                                  {taskDetails.target_for_approval !== null &&
                                    this.setDateOnly(
                                      taskDetails.target_for_approval
                                    )}
                                </div>
                              </Col>
                            )}

                          {this.checkNotificationTargetImplementation(
                            taskDetails.request_type,
                            taskDetails.parent_id
                          ) === true && (
                              <Col xs={12} sm={6} md={4}>
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .implementation
                                    }
                                  </label>{" "}
                                  {taskDetails.target_for_implementation !==
                                    null &&
                                    this.setDateOnly(
                                      taskDetails.target_for_implementation
                                    )}
                                </div>
                              </Col>
                            )}

                          {/* post */}
                          {taskDetails.parent_id === 0 && (
                            <Col xs={12} sm={12} md={12}>
                              <div className="form-group forecast-image">
                                <label>
                                  {
                                    this.state.dynamic_lang.task_details
                                      .requirement
                                  }
                                </label>{" "}
                                {ReactHtmlParser(
                                  htmlDecode(taskDetails.content)
                                )}
                              </div>
                            </Col>
                          )}

                          {taskDetails.ccp_posted_by > 0 && (
                            <Col xs={12} sm={6} md={4}>
                              <div className="form-group">
                                <label>
                                  {
                                    this.state.dynamic_lang.task_details
                                      .submitted_by
                                  }
                                </label>{" "}
                                {taskDetails.emp_posted_by_first_name}{" "}
                                {taskDetails.emp_posted_by_last_name} (
                                {taskDetails.emp_posted_by_desig_name})
                              </div>
                            </Col>
                          )}

                          {taskDetails.submitted_by > 0 && (
                            <Col xs={12} sm={6} md={4}>
                              <div className="form-group">
                                <label>
                                  {
                                    this.state.dynamic_lang.task_details
                                      .submitted_by
                                  }
                                </label>{" "}
                                {taskDetails.agnt_first_name}{" "}
                                {taskDetails.agnt_last_name} (Agent)
                              </div>
                            </Col>
                          )}

                          {this.getCCCust()}

                          {this.state.taskDetailsFiles.length > 0 && (
                            <Col xs={12}>
                              <div className="mb-20">
                                <div className="form-group">
                                  <label>
                                    {
                                      this.state.dynamic_lang.task_details
                                        .attachment
                                    }
                                    {this.state.taskDetailsFiles.length > 1
                                      ? "s"
                                      : ""}
                                    :{" "}
                                    {this.state.taskDetailsFiles.length > 1 && (
                                      <span>
                                        <LinkWithTooltipZip
                                          tooltip={
                                            this.state.dynamic_lang.task_details
                                              .click_download_all_files
                                          }
                                          href={`${this.state.multiTaskDetailsFiles}`}
                                          id="tooltip-downlaod_alll"
                                          clicked={(e) => this.checkHandler(e)}
                                        >
                                          <i
                                            className="fas fa-arrow-circle-down"
                                            style={{ fontSize: "17px" }}
                                          ></i>
                                        </LinkWithTooltipZip>
                                      </span>
                                    )}
                                  </label>
                                  <div className="cqtDetailsDeta-btm">
                                    <ul className="conDocList">{fileList}</ul>
                                  </div>
                                </div>
                              </div>
                            </Col>
                          )}
                        </Row>

                        {/* {this.state.apiCompleted === true &&
                          this.state.employeeArr.length > 0 &&
                          (taskDetails.cqt === 0 || taskDetails.cqt === null) &&
                          (getDashboard() === "BM" ||
                            getDashboard() === "SPOC" ||
                            getDashboard() === "CSC" ||
                            getDashboard() === "RA" ||
                            getMyId() === taskDetails.owner) &&
                          this.getCCForm()} */}

                        {this.state.subTasks.length > 0 && (
                          <>
                            <div className="clearfix"></div>
                            <div className="form-group m-t-15 m-b-0">
                              <label>
                                {this.state.dynamic_lang.task_details.sub_tasks}
                                :
                              </label>
                            </div>

                            {taskDetails.parent_id === 0 &&
                              this.state.has_discussion === 1 && (
                                <Row>
                                  <Col xs={12} sm={6} md={12}>
                                    <div className="form-group">
                                      <Link
                                        to={{
                                          pathname:
                                            "/user/discussion_details/" +
                                            taskDetails.task_id,
                                        }}
                                        target="_blank"
                                        style={{ cursor: "pointer" }}
                                        className="plus-request edit-button"
                                      >
                                        {/*<i className="far fa-edit" aria-hidden="true" />*/}
                                        {
                                          this.state.dynamic_lang.task_details
                                            .discussions
                                        }
                                      </Link>
                                    </div>
                                  </Col>
                                </Row>
                              )}

                            <PanelGroup
                              accordion
                              id="task_panel"
                              className="task_panel_accordian"
                              defaultActiveKey={this.state.subTasks[0].task_id}
                            >
                              {this.createAccordion()}
                            </PanelGroup>
                          </>
                        )}
                      </div>

                      {/* taskdetails Panel Btm Listing End*/}
                    </div>
                  </div>
                  {/* MONOSOM END HERE */}
                  {taskDetails.request_type === 23 &&
                    (taskDetails.cqt === null || taskDetails.cqt === 0)
                    ? this.getSAP()
                    : ""}

                  {taskDetails.cqt > 0 ? this.getCQT() : ""}
                </div>

                {this.state.taskDetails.language != "en" &&
                  this.state.taskDetails.parent_id === 0 &&
                  this.state.translate_api_complete && (
                    <div className={`tab-pane active`} id="show_tab_5">
                      <div className="boxPapanel content-padding">
                        <div className="taskdetails">
                          <div className="clearfix tdBtmlist">
                            <Row className="task_details_complete">

                              {taskDetails.curr_owner_fname !== "" && (
                                <Col xs={12} sm={6} md={4}>
                                  <div className="form-group">
                                    <label>
                                      Current Owner:
                                    </label>{" "}
                                    {taskDetails.curr_owner_fname}{" "}
                                    {taskDetails.curr_owner_lname}{" "}
                                    ({taskDetails.curr_owner_desig})
                                  </div>
                                </Col>
                              )}

                              {this.state.taskDetailsTranslated.company_name !==
                                "" && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Customer Name:</label>{" "}
                                      {
                                        this.state.taskDetailsTranslated
                                          .company_name
                                      }{" "}
                                    </div>
                                  </Col>
                                )}

                              {(this.state.taskDetailsTranslated.first_name !==
                                "" ||
                                this.state.taskDetailsTranslated.last_name !==
                                "") && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>User Name:</label>{" "}
                                      {htmlDecode(
                                        this.state.taskDetailsTranslated
                                          .first_name
                                      )}{" "}
                                      {htmlDecode(
                                        this.state.taskDetailsTranslated.last_name
                                      )}
                                    </div>
                                  </Col>
                                )}

                              {this.checkProductRequest(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Product:</label>{" "}
                                      {htmlDecode(
                                        this.state.taskDetailsTranslated
                                          .product_name
                                      )}
                                    </div>
                                  </Col>
                                )}

                              {this.checkMarketRequest(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Market:</label>{" "}
                                      {htmlDecode(
                                        this.state.taskDetailsTranslated
                                          .country_name
                                      )}
                                    </div>
                                  </Col>
                                )}

                              {this.checkGMPRequest(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>GMP Clearance ID:</label>{" "}
                                      {htmlDecode(
                                        this.state.taskDetailsTranslated
                                          .gmp_clearance_id
                                      )}
                                    </div>
                                  </Col>
                                )}

                              {this.checkTGAEmail(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Email ID:</label>{" "}
                                      {htmlDecode(
                                        this.state.taskDetailsTranslated
                                          .tga_email_id
                                      )}
                                    </div>
                                  </Col>
                                )}

                              {this.checkApplicant(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Applicant's Name and Address:</label>{" "}
                                      {htmlDecode(
                                        this.state.taskDetailsTranslated
                                          .applicant_name
                                      )}
                                    </div>
                                  </Col>
                                )}

                              {this.checkDocRequired(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Documents Required:</label>{" "}
                                      {htmlDecode(
                                        this.state.taskDetailsTranslated
                                          .doc_required
                                      )}
                                    </div>
                                  </Col>
                                )}

                              {this.checkSamplesWorkingImpureRequest(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true &&
                                this.state.taskDetailsTranslated.service_request_type.indexOf(
                                  "Samples"
                                ) !== -1 && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Samples:</label>
                                      <p>
                                        Number of batches :{" "}
                                        {
                                          this.state.taskDetailsTranslated
                                            .number_of_batches
                                        }
                                      </p>
                                      {this.state.taskDetailsTranslated
                                        .quantity && (
                                          <p>
                                            Quantity :{" "}
                                            {
                                              this.splitQuantity(
                                                this.state.taskDetailsTranslated
                                                  .quantity
                                              )[0]
                                            }{" "}
                                            {
                                              this.splitQuantity(
                                                this.state.taskDetailsTranslated
                                                  .quantity
                                              )[1]
                                            }
                                          </p>
                                        )}
                                    </div>
                                  </Col>
                                )}

                              {this.checkSamplesWorkingImpureRequest(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true &&
                                this.state.taskDetailsTranslated.service_request_type.indexOf(
                                  "Working Standards"
                                ) !== -1 && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Working Standards:</label>
                                      {this.state.taskDetailsTranslated
                                        .working_quantity && (
                                          <p>
                                            Quantity :{" "}
                                            {
                                              this.splitQuantity(
                                                this.state.taskDetailsTranslated
                                                  .working_quantity
                                              )[0]
                                            }{" "}
                                            {
                                              this.splitQuantity(
                                                this.state.taskDetailsTranslated
                                                  .working_quantity
                                              )[1]
                                            }{" "}
                                          </p>
                                        )}
                                    </div>
                                  </Col>
                                )}

                              {this.checkSamplesWorkingImpureRequest(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true &&
                                this.state.taskDetailsTranslated.service_request_type.indexOf(
                                  "Impurities"
                                ) !== -1 && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Impurities:</label>
                                      <p>
                                        Required impurity :{" "}
                                        {htmlDecode(
                                          this.state.taskDetailsTranslated
                                            .specify_impurity_required
                                        )}
                                      </p>

                                      {this.state.taskDetailsTranslated
                                        .impurities_quantity && (
                                          <p>
                                            Quantity :{" "}
                                            {
                                              this.splitQuantity(
                                                this.state.taskDetailsTranslated
                                                  .impurities_quantity
                                              )[0]
                                            }{" "}
                                            {
                                              this.splitQuantity(
                                                this.state.taskDetailsTranslated
                                                  .impurities_quantity
                                              )[1]
                                            }{" "}
                                          </p>
                                        )}
                                    </div>
                                  </Col>
                                )}

                              {this.checkSamplesWorkingImpureRequest(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Shipping Address:</label>
                                      <p>
                                        {ReactHtmlParser(
                                          htmlDecode(
                                            this.state.taskDetailsTranslated
                                              .shipping_address
                                          )
                                        )}
                                      </p>
                                    </div>
                                  </Col>
                                )}

                              {/* post */}
                              {this.checkPharmacopoeia(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Pharmacopoeia:</label>{" "}
                                      {htmlDecode(
                                        this.state.taskDetailsTranslated
                                          .pharmacopoeia
                                      )}
                                    </div>
                                  </Col>
                                )}

                              {this.checkPolymorphicForm(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Polymorphic Form:</label>{" "}
                                      {htmlDecode(
                                        this.state.taskDetailsTranslated
                                          .polymorphic_form
                                      )}
                                    </div>
                                  </Col>
                                )}

                              {this.checkDMFNumber(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>
                                        {this.state.taskDetailsTranslated
                                          .request_type === 39
                                          ? "DMF/CEP:"
                                          : "DMF Number:"}
                                      </label>{" "}
                                      {htmlDecode(
                                        this.state.taskDetailsTranslated
                                          .dmf_number
                                      )}
                                    </div>
                                  </Col>
                                )}

                              {/* post */}
                              {this.checkNotificationNumber(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Notification Number:</label>{" "}
                                      {htmlDecode(
                                        this.state.taskDetailsTranslated
                                          .notification_number
                                      )}
                                    </div>
                                  </Col>
                                )}

                              {this.checkAposDocumentType(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Document Type:</label>{" "}
                                      {htmlDecode(
                                        this.state.taskDetailsTranslated
                                          .apos_document_type
                                      )}
                                    </div>
                                  </Col>
                                )}

                              {this.state.taskDetailsTranslated.parent_id === 0 && this.state.taskDetailsTranslated.request_type != 43 && (
                                <Col xs={12} sm={6} md={4}>
                                  <div className="form-group">
                                    <label>Request Category:</label>{" "}
                                    {this.setReqCategoryName(
                                      this.state.taskDetailsTranslated
                                        .request_category
                                    )}
                                  </div>
                                </Col>
                              )}

                              {/* post */}
                              {this.checkStabilityDataType(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Stability Data Type:</label>{" "}
                                      {
                                        this.state.taskDetailsTranslated
                                          .stability_data_type
                                      }
                                    </div>
                                  </Col>
                                )}

                              {/* post */}
                              {this.checkAuditSiteName(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Site Name:</label>{" "}
                                      {htmlDecode(
                                        this.state.taskDetailsTranslated
                                          .audit_visit_site_name
                                      )}
                                    </div>
                                  </Col>
                                )}

                              {/* post */}
                              {this.checkAuditVistDate(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Audit/Visit Date:</label>{" "}
                                      {this.state.taskDetailsTranslated
                                        .request_audit_visit_date !== null &&
                                        this.setDateOnly(
                                          this.state.taskDetailsTranslated
                                            .request_audit_visit_date
                                        )}
                                    </div>
                                  </Col>
                                )}

                              {this.checkQuantity(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Quantity:</label>{" "}
                                      {this.state.taskDetailsTranslated.quantity}
                                    </div>
                                  </Col>
                                )}

                              {this.checkRDD(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>
                                        {this.state.taskDetailsTranslated
                                          .request_type === 39 ||
                                          this.state.taskDetailsTranslated
                                            .request_type === 40
                                          ? "Requested Deadline:"
                                          : "Required Date Of Delivery:"}
                                      </label>{" "}
                                      {this.state.taskDetailsTranslated.rdd !==
                                        null &&
                                        this.setDateOnly(
                                          this.state.taskDetailsTranslated.rdd
                                        )}
                                    </div>
                                  </Col>
                                )}

                              {this.checkRDORC(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>
                                        Requested date of response/closure:
                                      </label>{" "}
                                      {this.state.taskDetailsTranslated.rdfrc &&
                                        this.state.taskDetailsTranslated.rdfrc !==
                                        null &&
                                        this.state.taskDetailsTranslated.rdfrc}
                                    </div>
                                  </Col>
                                )}

                              {this.state.taskDetailsTranslated.request_type === 43 && this.state.taskDetailsTranslated.sales_order_no != null && (
                                <Col xs={12} sm={6} md={4}>
                                  <div className="form-group forecast-image">
                                    <label>
                                      Sales Order No
                                    </label>{" "}
                                    {ReactHtmlParser(htmlDecode(this.state.taskDetailsTranslated.sales_order_no))}
                                  </div>
                                </Col>
                              )}

                              {this.state.taskDetailsTranslated.request_type === 43 && this.state.taskDetailsTranslated.sales_order_date != null && (
                                <Col xs={12} sm={6} md={4}>
                                  <div className="form-group forecast-image">
                                    <label>
                                      Sales Order Date
                                    </label>{" "}
                                    {ReactHtmlParser(htmlDecode(this.state.taskDetailsTranslated.sales_order_date))}
                                  </div>
                                </Col>
                              )}

                              {(this.state.taskDetailsTranslated.request_type == 43 || this.state.taskDetailsTranslated.request_type == 23) && this.state.taskDetailsTranslated.po_no != null && (
                                <Col xs={12} sm={6} md={4}>
                                  <div className="form-group forecast-image">
                                    <label>
                                      Purchase Order No.
                                    </label>{" "}
                                    {ReactHtmlParser(htmlDecode(this.state.taskDetailsTranslated.po_no))}
                                  </div>
                                </Col>
                              )}

                              {this.state.taskDetailsTranslated.request_type === 43 && this.state.taskDetailsTranslated.SAPDetails.city != '' && (
                                <Col xs={12} sm={6} md={4}>
                                  <div className="form-group forecast-image">
                                    <label>
                                      ShipTo Address
                                    </label>{" "}
                                    <div>{ReactHtmlParser(htmlDecode(this.state.taskDetailsTranslated.SAPDetails.shipto_name))}
                                    </div>
                                    {ReactHtmlParser(htmlDecode(this.state.taskDetailsTranslated.SAPDetails.street)) + ', '}
                                    {ReactHtmlParser(htmlDecode(this.state.taskDetailsTranslated.SAPDetails.city)) + ', '}
                                    {ReactHtmlParser(htmlDecode(this.state.taskDetailsTranslated.SAPDetails.post_code)) + ', '}
                                    {ReactHtmlParser(htmlDecode(this.state.taskDetailsTranslated.SAPDetails.country)) + ', '}
                                  </div>
                                </Col>
                              )}
                              {this.state.taskDetailsTranslated.request_type === 43 && this.state.taskDetailsTranslated.po_no != null && this.state.taskDetailsTranslated.SAPDetails.po_file_date != null && (
                                <Col xs={12} sm={6} md={4}>
                                  <div className="form-group forecast-image">
                                    <label>
                                      PO Uploaded Date
                                    </label>{" "}
                                    {ReactHtmlParser(htmlDecode(this.state.taskDetailsTranslated.SAPDetails.po_file_date))}
                                  </div>
                                </Col>
                              )}
                              {this.state.taskDetailsTranslated.request_type === 43 && this.state.taskDetailsTranslated.po_no != null && this.state.taskDetailsTranslated.SAPDetails.po_file_name != null && (
                                <Col xs={12} sm={6} md={4}>
                                  <div className="form-group forecast-image">
                                    <label>
                                      PO File
                                    </label>{" "}
                                    <a href={`${pourl}${this.state.taskDetailsTranslated.sap_request_id}`} target="_blank">
                                      {ReactHtmlParser(htmlDecode(this.state.taskDetailsTranslated.SAPDetails.po_file_name))}
                                    </a>
                                  </div>
                                </Col>
                              )}

                              {this.checkBatchNo(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Batch No:</label>{" "}
                                      {
                                        this.state.taskDetailsTranslated
                                          .batch_number
                                      }
                                    </div>
                                  </Col>
                                )}

                              {this.checkForecastNotificationDate(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Notification Date:</label>{" "}
                                      {this.setCreateDate(
                                        this.state.taskDetailsTranslated
                                          .date_added
                                      )}
                                    </div>
                                  </Col>
                                )}

                              {this.checkNatureOfIssue(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Nature Of Issue:</label>{" "}
                                      {htmlDecode(
                                        this.state.taskDetailsTranslated
                                          .nature_of_issue
                                      )}
                                    </div>
                                  </Col>
                                )}

                              {this.checkGenpactCheckList(
                                this.state.taskDetailsTranslated.genpact,
                                this.state.taskDetailsTranslated
                                  .genpact_checklist
                              )}

                              {this.state.taskDetailsTranslated.parent_id >
                                0 && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Title:</label>{" "}
                                      {ReactHtmlParser(
                                        this.state.taskDetailsTranslated.title
                                      )}
                                    </div>
                                  </Col>
                                )}

                              {this.state.taskDetailsTranslated.parent_id >
                                0 && (
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                      <label>Content:</label>{" "}
                                      {ReactHtmlParser(
                                        htmlDecode(
                                          this.state.taskDetailsTranslated.content
                                        )
                                      )}
                                    </div>
                                  </Col>
                                )}

                              {this.checkAmountPending(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Amount Pending:</label>{" "}
                                      {
                                        this.state.taskDetailsTranslated
                                          .payment_pending
                                      }
                                    </div>
                                  </Col>
                                )}

                              {this.checkDaysRemaining(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Days Remaining:</label>{" "}
                                      {
                                        this.state.taskDetailsTranslated
                                          .days_remaining
                                      }
                                    </div>
                                  </Col>
                                )}

                              {this.checkPaymentStatus(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>status:</label>{" "}
                                      {
                                        this.state.taskDetailsTranslated
                                          .payment_status
                                      }
                                    </div>
                                  </Col>
                                )}

                              {this.checkPaymentDueDate(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Due Date:</label>{" "}
                                      {this.setCreateDate(
                                        this.state.taskDetailsTranslated.due_date
                                      )}
                                    </div>
                                  </Col>
                                )}

                              {this.checkNotificationRequestType(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Request Type:</label>{" "}
                                      {
                                        this.state.taskDetailsTranslated
                                          .notification_type
                                      }
                                    </div>
                                  </Col>
                                )}

                              {this.checkNotificationTargetApproval(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Target for Approval/Feedback:</label>{" "}
                                      {this.state.taskDetailsTranslated
                                        .target_for_approval !== null &&
                                        this.setDateOnly(
                                          this.state.taskDetailsTranslated
                                            .target_for_approval
                                        )}
                                    </div>
                                  </Col>
                                )}

                              {this.checkNotificationTargetImplementation(
                                this.state.taskDetailsTranslated.request_type,
                                this.state.taskDetailsTranslated.parent_id
                              ) === true && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Target for Implementation:</label>{" "}
                                      {this.state.taskDetailsTranslated
                                        .target_for_implementation !== null &&
                                        this.setDateOnly(
                                          this.state.taskDetailsTranslated
                                            .target_for_implementation
                                        )}
                                    </div>
                                  </Col>
                                )}

                              {/* post */}
                              {this.state.taskDetailsTranslated.parent_id ===
                                0 && (
                                  <Col xs={12} sm={12} md={12}>
                                    <div className="form-group forecast-image">
                                      <label>Requirement:</label>{" "}
                                      {ReactHtmlParser(
                                        htmlDecode(
                                          this.state.taskDetailsTranslated.content
                                        )
                                      )}
                                    </div>
                                  </Col>
                                )}

                              {this.state.taskDetailsTranslated.ccp_posted_by >
                                0 && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Submitted By:</label>{" "}
                                      {
                                        this.state.taskDetailsTranslated
                                          .emp_posted_by_first_name
                                      }{" "}
                                      {
                                        this.state.taskDetailsTranslated
                                          .emp_posted_by_last_name
                                      }{" "}
                                      (
                                      {
                                        this.state.taskDetailsTranslated
                                          .emp_posted_by_desig_name
                                      }
                                      )
                                    </div>
                                  </Col>
                                )}

                              {this.state.taskDetailsTranslated.submitted_by >
                                0 && (
                                  <Col xs={12} sm={6} md={4}>
                                    <div className="form-group">
                                      <label>Submitted By:</label>{" "}
                                      {
                                        this.state.taskDetailsTranslated
                                          .agnt_first_name
                                      }{" "}
                                      {
                                        this.state.taskDetailsTranslated
                                          .agnt_last_name
                                      }{" "}
                                      (Agent)
                                    </div>
                                  </Col>
                                )}

                              {this.getCCCust()}

                              {this.state.taskDetailsTranslatedFiles.length >
                                0 && (
                                  <Col xs={12}>
                                    <div className="mb-20">
                                      <div className="form-group">
                                        <label>
                                          Attachment
                                          {this.state.taskDetailsTranslatedFiles
                                            .length > 1
                                            ? "s"
                                            : ""}
                                          :{" "}
                                          {this.state.taskDetailsTranslatedFiles
                                            .length > 1 && (
                                              <span>
                                                <LinkWithTooltipZip
                                                  tooltip={`Click here to download all files`}
                                                  href={`${this.state.multithis.state.taskDetailsTranslatedFiles}`}
                                                  id="tooltip-downlaod_alll"
                                                  clicked={(e) =>
                                                    this.checkHandler(e)
                                                  }
                                                >
                                                  <i
                                                    className="fas fa-arrow-circle-down"
                                                    style={{ fontSize: "17px" }}
                                                  ></i>
                                                </LinkWithTooltipZip>
                                              </span>
                                            )}
                                        </label>
                                        <div className="cqtDetailsDeta-btm">
                                          <ul className="conDocList">
                                            {fileList}
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                  </Col>
                                )}
                            </Row>

                            {this.state.subTasks.length > 0 && (
                              <>
                                <div className="clearfix"></div>
                                <div className="form-group m-t-15 m-b-0">
                                  <label>Sub Tasks:</label>
                                </div>

                                {this.state.taskDetailsTranslated.parent_id ===
                                  0 &&
                                  this.state.has_discussion === 1 && (
                                    <Row>
                                      <Col xs={12} sm={6} md={12}>
                                        <div className="form-group">
                                          <Link
                                            to={{
                                              pathname:
                                                "/user/discussion_details/" +
                                                this.state.taskDetailsTranslated
                                                  .task_id,
                                            }}
                                            target="_blank"
                                            style={{ cursor: "pointer" }}
                                            className="plus-request edit-button"
                                          >
                                            {/*<i className="far fa-edit" aria-hidden="true" />*/}
                                            Discussions
                                          </Link>
                                        </div>
                                      </Col>
                                    </Row>
                                  )}

                                <PanelGroup
                                  accordion
                                  id="task_panel"
                                  className="task_panel_accordian"
                                  defaultActiveKey={
                                    this.state.subTasks[0].task_id
                                  }
                                >
                                  {this.createAccordion()}
                                </PanelGroup>
                              </>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  )}

                {this.state.commentExists &&
                  (this.state.taskDetails.cqt === null ||
                    this.state.taskDetails.cqt === 0) && (
                    <div className="tab-pane" id="show_tab_2">
                      <div className="boxPapanel content-padding">
                        <div className="taskdetails">{this.getComments()}</div>
                      </div>
                    </div>
                  )}

                {this.state.discussionExists &&
                  this.state.discussDetails.length > 0 &&
                  this.state.discussDetailsT.length > 0 && (
                    <div
                      className={`tab-pane ${this.state.discussionClass}`}
                      id="show_tab_3"
                    >
                      <div className="row">
                        <div className="col-xs-12">
                          <div className="nav-tabs-custom discussion-tab">
                            <ul className="nav nav-tabs">
                              {/* {this.state.discussDetailsT.length > 0 && <li
                                className="active"
                                onClick={(e) => this.handleTabs2(e)}
                                id="d_tab_3"
                              >
                                Discussion Details
                              </li>} */}

                              <li
                                className={`active`}
                                onClick={(e) => this.handleTabs2(e)}
                                id="d_tab_1"
                              >
                                Discussion Details
                              </li>

                              <li
                                onClick={(e) => this.handleTabs2(e)}
                                id="d_tab_2"
                              >
                                Files
                              </li>
                              {this.state.discussDetailsT.length > 0 && (
                                // <li
                                //   onClick={(e) => this.toggleDiscussion(e)}
                                //   className={`active`}
                                // >
                                //   {this.state.show_lang_diss}
                                // </li>
                                <li>
                                  <Switch
                                    checked={this.state.switchCheckedDiss}
                                    onChange={(e) => this.toggleDiscussion(e)}
                                    onColor="#7145d3"
                                    uncheckedIcon={"EN"}
                                    checkedIcon={this.state.taskDetails.language.toUpperCase()}
                                    className="react-switch"
                                    id="icon-switch"
                                  />
                                </li>
                              )}
                            </ul>

                            <div className="tab-content">
                              <div
                                className={`tab-pane ${this.state.discussDetailsT.length > 0
                                    ? ""
                                    : "active"
                                  }`}
                                id="show_d_tab_1"
                              >
                                <section className="content-header">
                                  <h1>
                                    Discussion Details
                                    {taskDetails.customer_status === 3 && (
                                      <span
                                        style={{
                                          color: "#e6021d",
                                          fontSize: "12px",
                                          paddingLeft: "12px",
                                        }}
                                      >
                                        (Messages will not reach customer since
                                        the account is inactive)
                                      </span>
                                    )}
                                  </h1>
                                </section>
                                <section className="content">
                                  <div className="boxPapanel content-padding">
                                    <div className="disHead">
                                      <h3>
                                        {this.state.discussReqName} |{" "}
                                        {this.state.discussTaskRef}
                                      </h3>
                                    </div>

                                    <div className="comment-list-main-wrapper">
                                      {this.state.discussDetails.map(
                                        (comments, i) => (
                                          <div key={i} className="comment">
                                            <div className="row">
                                              <div className="col-md-10 col-sm-10 col-xs-12">
                                                <div className="imageArea">
                                                  <div className="imageBorder">
                                                    {comments.added_by_type ===
                                                      "E" &&
                                                      comments.emp_profile_pic !=
                                                      "" &&
                                                      comments.emp_profile_pic !=
                                                      null ? (
                                                      <img
                                                        alt="noimage"
                                                        src={
                                                          comments.emp_profile_pic
                                                        }
                                                      />
                                                    ) : (
                                                      ""
                                                    )}

                                                    {comments.added_by_type ===
                                                      "C" &&
                                                      comments.cust_profile_pic !=
                                                      "" &&
                                                      comments.cust_profile_pic !=
                                                      null ? (
                                                      <img
                                                        alt="noimage"
                                                        src={
                                                          comments.cust_profile_pic
                                                        }
                                                      />
                                                    ) : (
                                                      ""
                                                    )}

                                                    {comments.added_by_type ===
                                                      "A" &&
                                                      comments.agnt_profile_pic !=
                                                      "" &&
                                                      comments.agnt_profile_pic !=
                                                      null ? (
                                                      <img
                                                        alt="noimage"
                                                        src={
                                                          comments.agnt_profile_pic
                                                        }
                                                      />
                                                    ) : (
                                                      ""
                                                    )}

                                                    {(comments.emp_profile_pic ==
                                                      "" ||
                                                      comments.emp_profile_pic ==
                                                      null) &&
                                                      (comments.cust_profile_pic ==
                                                        "" ||
                                                        comments.cust_profile_pic ==
                                                        null) &&
                                                      (comments.agnt_profile_pic ==
                                                        "" ||
                                                        comments.agnt_profile_pic ==
                                                        null) && (
                                                        <img
                                                          alt="noimage"
                                                          src={commenLogo}
                                                        />
                                                      )}
                                                  </div>
                                                </div>
                                                <div className="conArea">
                                                  {comments.added_by_type ===
                                                    "C" && (
                                                      <p>{`${comments.cust_fname} ${comments.cust_lname}`}</p>
                                                    )}

                                                  {comments.added_by_type ===
                                                    "E" && (
                                                      <p>
                                                        {`${comments.emp_fname} ${comments.emp_lname} (DRL)`}{" "}
                                                        {comments.status ===
                                                          0 && (
                                                            <b
                                                              style={{
                                                                color: "red",
                                                              }}
                                                            >
                                                              (Needs Approval)
                                                            </b>
                                                          )}{" "}
                                                      </p>
                                                    )}

                                                  {comments.added_by_type ===
                                                    "A" && (
                                                      <p>{`${comments.agnt_first_name} ${comments.agnt_last_name} (Agent)`}</p>
                                                    )}
                                                  <span>
                                                    {ReactHtmlParser(
                                                      ReactHtmlParser(
                                                        comments.comment
                                                      )
                                                    )}
                                                  </span>
                                                  {this.getDiscussionFile(
                                                    comments.uploads,
                                                    comments.multi
                                                  )}
                                                </div>
                                                <div className="clearfix" />
                                              </div>

                                              <div className="col-md-2 col-sm-2 col-xs-12">
                                                <div className="dateArea">
                                                  <p>
                                                    {dateFormat(
                                                      localDate(
                                                        comments.date_added
                                                      ),
                                                      "ddd, mmm dS, yyyy"
                                                    )}
                                                  </p>
                                                  <div
                                                    className="clearfix tdBtmlist"
                                                    style={{
                                                      right: "25%",
                                                      position: "absolute",
                                                    }}
                                                  ></div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        )
                                      )}
                                    </div>
                                  </div>
                                </section>
                              </div>

                              <div className="tab-pane" id="show_d_tab_2">
                                <section className="content-header">
                                  <h1>File Details</h1>
                                </section>
                                <section className="content">
                                  <div className="boxPapanel content-padding">
                                    <div className="disHead">
                                      {/* <h3>
                                    {this.state.discussReqName} | {this.state.discussTaskRef}
                                  </h3> */}
                                    </div>

                                    <div className="comment-list-main-wrapper">
                                      {this.state.discussFile &&
                                        this.state.discussFile.length > 0
                                        ? this.state.discussFile.map(
                                          (file, i) => (
                                            <div key={i} className="comment">
                                              <div className="row">
                                                <div className="col-md-10 col-sm-10 col-xs-12">
                                                  <div className="imageArea">
                                                    <div className="imageBorder">
                                                      <img
                                                        alt="noimage"
                                                        src={commenLogo1}
                                                      />
                                                    </div>
                                                  </div>
                                                  <div className="conArea">
                                                    {/* <a href={`${diss_path}${file.upload_id}`} target="_blank" download rel="noopener noreferrer">
                                                {file.actual_file_name}
                                              </a> */}
                                                    {process.env.NODE_ENV ===
                                                      "development" ? (
                                                      <LinkWithTooltip
                                                        // tooltip={`/client_uploads/${file.new_file_name}`}
                                                        href="#"
                                                        id="tooltip-1"
                                                        clicked={(e) =>
                                                          this.redirectUrlTask(
                                                            e,
                                                            `${s3bucket_diss_path}${file.upload_id}`
                                                          )
                                                        }
                                                      >
                                                        {
                                                          file.actual_file_name
                                                        }
                                                      </LinkWithTooltip>
                                                    ) : (
                                                      <LinkWithTooltip
                                                        // tooltip={`/client_uploads/${file.new_file_name}`}
                                                        href="#"
                                                        id="tooltip-1"
                                                        clicked={(e) =>
                                                          this.redirectUrlTask(
                                                            e,
                                                            `${s3bucket_diss_path}${file.upload_id}`
                                                          )
                                                        }
                                                      >
                                                        {
                                                          file.actual_file_name
                                                        }
                                                      </LinkWithTooltip>
                                                    )}
                                                  </div>
                                                  <div className="clearfix" />
                                                </div>

                                                <div className="col-md-2 col-sm-2 col-xs-12">
                                                  <div className="dateArea">
                                                    <p>
                                                      {dateFormat(
                                                        localDate(
                                                          file.date_added
                                                        ),
                                                        "ddd, mmm dS, yyyy"
                                                      )}
                                                    </p>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          )
                                        )
                                        : `No Files Found`}
                                    </div>
                                  </div>
                                </section>
                              </div>

                              <div
                                className="tab-pane active"
                                id="show_d_tab_3"
                              >
                                <section className="content-header">
                                  <h1>
                                    Discussion Details
                                    {taskDetails.customer_status === 3 && (
                                      <span
                                        style={{
                                          color: "#e6021d",
                                          fontSize: "12px",
                                          paddingLeft: "12px",
                                        }}
                                      >
                                        (Messages will not reach customer since
                                        the account is inactive)
                                      </span>
                                    )}
                                  </h1>
                                </section>
                                <section className="content">
                                  <div className="boxPapanel content-padding">
                                    <div className="disHead">
                                      <h3>
                                        {this.state.discussReqNameT} |{" "}
                                        {this.state.discussTaskRefT}
                                      </h3>
                                    </div>

                                    <div className="comment-list-main-wrapper">
                                      {this.state.discussDetailsT.map(
                                        (comments, i) => (
                                          <div key={i} className="comment">
                                            <div className="row">
                                              <div className="col-md-10 col-sm-10 col-xs-12">
                                                <div className="imageArea">
                                                  <div className="imageBorder">
                                                    {comments.added_by_type ===
                                                      "E" &&
                                                      comments.emp_profile_pic !=
                                                      "" &&
                                                      comments.emp_profile_pic !=
                                                      null ? (
                                                      <img
                                                        alt="noimage"
                                                        src={
                                                          comments.emp_profile_pic
                                                        }
                                                      />
                                                    ) : (
                                                      ""
                                                    )}

                                                    {comments.added_by_type ===
                                                      "C" &&
                                                      comments.cust_profile_pic !=
                                                      "" &&
                                                      comments.cust_profile_pic !=
                                                      null ? (
                                                      <img
                                                        alt="noimage"
                                                        src={
                                                          comments.cust_profile_pic
                                                        }
                                                      />
                                                    ) : (
                                                      ""
                                                    )}

                                                    {comments.added_by_type ===
                                                      "A" &&
                                                      comments.agnt_profile_pic !=
                                                      "" &&
                                                      comments.agnt_profile_pic !=
                                                      null ? (
                                                      <img
                                                        alt="noimage"
                                                        src={
                                                          comments.agnt_profile_pic
                                                        }
                                                      />
                                                    ) : (
                                                      ""
                                                    )}

                                                    {(comments.emp_profile_pic ==
                                                      "" ||
                                                      comments.emp_profile_pic ==
                                                      null) &&
                                                      (comments.cust_profile_pic ==
                                                        "" ||
                                                        comments.cust_profile_pic ==
                                                        null) &&
                                                      (comments.agnt_profile_pic ==
                                                        "" ||
                                                        comments.agnt_profile_pic ==
                                                        null) && (
                                                        <img
                                                          alt="noimage"
                                                          src={commenLogo}
                                                        />
                                                      )}
                                                  </div>
                                                </div>
                                                <div className="conArea">
                                                  {comments.added_by_type ===
                                                    "C" && (
                                                      <p>{`${comments.cust_fname} ${comments.cust_lname}`}</p>
                                                    )}

                                                  {comments.added_by_type ===
                                                    "E" && (
                                                      <p>
                                                        {`${comments.emp_fname} ${comments.emp_lname} (DRL)`}{" "}
                                                        {comments.status ===
                                                          0 && (
                                                            <b
                                                              style={{
                                                                color: "red",
                                                              }}
                                                            >
                                                              (Needs Approval)
                                                            </b>
                                                          )}{" "}
                                                      </p>
                                                    )}

                                                  {comments.added_by_type ===
                                                    "A" && (
                                                      <p>{`${comments.agnt_first_name} ${comments.agnt_last_name} (Agent)`}</p>
                                                    )}
                                                  <span>
                                                    {ReactHtmlParser(
                                                      ReactHtmlParser(
                                                        comments.comment
                                                      )
                                                    )}
                                                  </span>
                                                  {this.getDiscussionFile(
                                                    comments.uploads,
                                                    comments.multi
                                                  )}
                                                </div>
                                                <div className="clearfix" />
                                              </div>

                                              <div className="col-md-2 col-sm-2 col-xs-12">
                                                <div className="dateArea">
                                                  <p>
                                                    {dateFormat(
                                                      localDate(
                                                        comments.date_added
                                                      ),
                                                      "ddd, mmm dS, yyyy"
                                                    )}
                                                    <div
                                                      className="clearfix tdBtmlist"
                                                      style={{
                                                        right: "25%",
                                                        position: "absolute",
                                                      }}
                                                    ></div>
                                                  </p>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        )
                                      )}
                                    </div>
                                  </div>
                                </section>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}

                <div className="tab-pane" id="show_tab_4">
                  <div className="boxPapanel content-padding">
                    <div className="taskdetails">{this.activityLogTable()}</div>
                  </div>
                </div>
              </div>

              {/*DONE*/}
              <AssignPopup
                showAssign={this.state.showAssign}
                currRow={this.state.currRow}
                handleClose={this.handleClose}
                reloadTask={() => this.reloadTaskDetails()}
              />

              {/*DONE*/}
              <CreateSubTaskPopup
                showCreateSubTask={this.state.showCreateSubTask}
                currRow={this.state.currRow}
                handleClose={this.handleClose}
                reloadTask={() => this.reloadTaskDetails()}
              />

              <CreateSubTaskPopup
                showCreateSubTask={this.state.showCloneCreateSubTask}
                currRow={this.state.currRow}
                clone_main={1}
                handleClose={this.handleClose}
                reloadTask={() => this.reloadTaskDetails()}
              />

              <CreateSubTaskPopup
                showCreateSubTask={this.state.showCreateSubTaskNew}
                currRow={this.state.currRow}
                clone={1}
                handleClose={this.handleClose}
                reloadTask={() => this.reloadTaskDetails()}
              />

              {/*DONE*/}
              <RespondCustomerPopup
                showRespondCustomer={this.state.showRespondCustomer}
                currRow={this.state.currRow}
                custResp={this.state.custResp}
                handleClose={this.handleClose}
                reloadTask={() => this.reloadTaskDetails()}
              />

              {/*DONE*/}
              <RespondBackPopup
                showRespondBack={this.state.showRespondBack}
                currRow={this.state.currRow}
                handleClose={this.handleClose}
                reloadTask={() => this.reloadTaskDetails()}
              />

              <ProformaPopup
                showProformaForm={this.state.showProformaForm}
                currRow={this.state.currRow}
                handleClose={this.handleClose}
                reloadTask={() => this.reloadTaskDetails()}
                showSubTaskPopup={this.showSubTaskPopup}
                showRespondCustomer={this.showRespondCustomerPopup}
              />

              <Poke
                showPoke={this.state.showPoke}
                currRow={this.state.currRow}
                handleClose={this.handleClose}
                reloadTask={() => this.reloadTaskDetails()}
              />

              <DeleteTaskPopup
                showDeleteTaskPopup={this.state.showDeleteTaskPopup}
                currRow={this.state.currRow}
                handleClose={this.handleClose}
                reloadTask={() => this.reloadTaskDetails()}
              />

              {/*DONE*/}
              <AuthorizePopup
                showAuthorizeBack={this.state.showAuthorizeBack}
                currRow={this.state.currRow}
                handleClose={this.handleClose}
                reloadTask={() => this.reloadTaskDetails()}
              />

              <IncreaseSlaPopup
                showIncreaseSla={this.state.showIncreaseSla}
                currRow={this.state.currRow}
                handleClose={this.handleClose}
                reloadTask={() => this.reloadTaskDetails()}
              />

              <IncreaseSlaSubTaskPopup
                showIncreaseSla={this.state.showIncreaseSlaSubTask}
                currRow={this.state.currRow}
                handleClose={this.handleClose}
                reloadTask={() => this.reloadTaskDetails()}
              />

              <RequestToReopen
                showReqToReopen={this.state.showReqToReopen}
                currRow={this.state.currRow}
                handleClose={this.handleClose}
                reloadTask={() => this.reloadTaskDetails()}
              />

              <RequestToReopenMyTask
                showReopen={this.state.showReopen}
                currRow={this.state.currRow}
                handleClose={this.handleClose}
                reloadTask={() => this.reloadTaskDetails()}
              />

              <ReAssignPopup
                showReAssign={this.state.showReAssign}
                currRow={this.state.currRow}
                handleClose={this.handleClose}
                reloadTask={() => this.reloadTaskDetails()}
              />

              <CopyCreateMyTasksPopup
                showCreateTasks={this.state.showCreateTasks}
                currRow={this.state.currRow}
                handleClose={this.handleClose}
                allTaskDetails={(e) => this.reloadTaskDetails()}
              />

              {/*DONE*/}
              <SPOCTranslatedCommentPopup
                showTranslateEditComment={this.state.showTranslateEditComment}
                currRow={this.state.currRow}
                handleClose={this.handleClose}
                reloadTask={() => this.reloadTaskDetails()}
              />

              <SPOCOriginalCommentPopup
                showOriginalEditComment={this.state.showOriginalEditComment}
                currRow={this.state.currRow}
                handleClose={this.handleClose}
                reloadTask={() => this.reloadTaskDetails()}
              />
            </section>
          </div>
        </>
      );
    } else {
      if (this.state.invalid_access) {
        return <></>;
      } else {
        return (
          <>
            <div className="loderOuter">
              <div className="loader">
                <img src={loaderlogo} alt="logo" />
                <div className="loading">Loading...</div>
              </div>
            </div>
          </>
        );
      }
    }
  }
}

export default TaskDetailsAny;
