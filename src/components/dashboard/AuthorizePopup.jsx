import React, { Component } from 'react';
import { Row, Col, ButtonToolbar, Button, Modal } from "react-bootstrap";
import { Formik, Field, Form } from "formik"; 
import * as Yup from "yup";

import API from "../../shared/axios";
import swal from "sweetalert";

const initialValues = {
    comment         : ""
  };

class AuthorizePopup extends Component {    

    constructor(props){
        super(props);
        this.state = {
            showAuthorizeBack : false
        };
    }

    componentWillReceiveProps = (nextProps) => {
        //console.log(nextProps);
        if(nextProps.showAuthorizeBack === true && nextProps.currRow.task_id > 0){
            // console.log(nextProps.currRow);
            this.setState({showAuthorizeBack: nextProps.showAuthorizeBack,currRow:nextProps.currRow});
        }
    }

    handleClose = () => {
        var close = {showAuthorizeBack:false};
        this.setState(close);
        this.props.handleClose(close);
    };

    handleRespond = (values, actions) => {
        //console.log(values);
        const postData = {
            comment:values.comment,
            assign_id:this.state.currRow.assignment_id
        };

        //console.log(postData);

        API.post(`/api/tasks/respond_back/${this.state.currRow.task_id}`,postData).then(res => {
            this.handleClose();
            swal({
                closeOnClickOutside: false,
                title: "Success",
                text: "Response send to employee.",
                icon: "success"
              }).then(() => {
                this.props.reloadTask();
            });
        });
    }

    render(){

        const validateStopFlag = Yup.object().shape({
            comment : Yup.string().trim()
                .required("Please enter description")
        });

        const newInitialValues = Object.assign(initialValues, {
            comment         : "",
            submitType      : ""
          });

            return(
                <>
                <Modal show={this.state.showAuthorizeBack} onHide={() => this.handleClose()} backdrop="static">
                    <Formik
                        initialValues    = {newInitialValues}
                        validationSchema = {validateStopFlag}
                        onSubmit         = {this.handleRespond}
                    >
                        {({ values, errors, touched, isValid, isSubmitting, setFieldValue }) => {
                        return (
                            <Form>
                            <Modal.Header closeButton>
                                <Modal.Title>
                                Respond Back
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div className="contBox">
                                <Row>
                                    <Col xs={12} sm={12} md={12}>
                                    Comment
                                    <div className="form-group">
                                        <Field
                                        name="comment"
                                        component="textarea"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.comment}
                                        >
                                        </Field>
                                        {errors.comment && touched.comment ? (
                                        <span className="errorMsg">
                                            {errors.comment}
                                        </span>
                                        ) : null}
                                    </div>
                                    </Col>
                                </Row>
                                </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <button
                                    className={`btn btn-success btn-sm btn-custom-green m-r-10`}
                                    type="submit"
                                    disabled={isValid ? false : true}
                                    value={values.submitType}
                                    onClick={value =>
                                        setFieldValue("submitType","Accept")
                                    }
                                >
                                    Accept
                                </button>
                                <button
                                    className={`btn btn-danger btn-sm`}
                                    type="submit"
                                    disabled={isValid ? false : true}
                                    value={values.submitType}
                                    onClick={value =>
                                        setFieldValue("submitType","Reject")
                                    }
                                >
                                    Reject
                                </button>
                            </Modal.Footer>
                            </Form>
                        );
                        }}
                    </Formik>
                    </Modal>
                </>
            );

    }
}

export default AuthorizePopup;