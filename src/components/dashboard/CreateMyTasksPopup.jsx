import React, { Component } from "react";

import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Modal,
  Alert,
  Tooltip,
  OverlayTrigger,
} from "react-bootstrap";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";
import API from "../../shared/axios";
import { Redirect, Link } from "react-router-dom";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";

import { showErrorMessageFront } from "../../shared/handle_error_front";
import swal from "sweetalert";

import {
  localDate,
  trimString,
  inArray,
  htmlDecode,
} from "../../shared/helper";

import Dropzone from "react-dropzone";
import exclamationImage from "../../assets/images/exclamation-icon-black.svg";
import { Editor } from "@tinymce/tinymce-react";
import ReactHtmlParser from "react-html-parser";
//import { url } from 'inspector';

//import jp from 'jsonpath';

const initialValues = {
  content: "",
  customer_list_id: "",
  employee_list_id: "",
  request_type_id: "",
  country_id: "",
  file_name: [],
  shipping_address: "",
  share_with_agent: 1,
};

const removeDropZoneFiles = (fileName, objRef) => {
  var newArr = [];
  for (let index = 0; index < objRef.state.files.length; index++) {
    const element = objRef.state.files[index];

    if (fileName === element.name) {
    } else {
      newArr.push(element);
    }
  }

  var fileListHtml = newArr.map((file) => (
    <Alert key={file.name}>
      <span
        onClick={() => removeDropZoneFiles(file.name, objRef)}
        style={{ cursor: "pointer" }}
      >
        <i className="far fa-times-circle"></i>
      </span>{" "}
      {file.name}
    </Alert>
  ));

  objRef.setState({
    files: newArr,
    filesHtml: fileListHtml,
  });
};

/*For Tooltip*/
function LinkWithTooltipText({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const options = [
  { value: "Mgs", label: "Mgs" },
  { value: "Gms", label: "Grams" },
  { value: "Kgs", label: "Kgs" },
  { value: "Vials", label: "Vials" },
  
];

class CreateMyTasksPopup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showCreateTasks: false,
      customer_list: [],
      company_list: [],
      employee_list: [],
      files: [],
      request_type: [],
      showRequestTypeHTML: false,
      showCustomerTypeHTML: false,
      subForm: {},
      is_loading: true,
      createTaskLoader: false,
      filesHtml: "",

      sampleBatchNo: 0,
      sampleQuantity: "",
      sampleUnit: "",

      workingQuantity: "",
      workingUnit: "",

      impureRequired: "",
      impureQuantity: "",
      impureUnit: "",

      service_check_type: {
        check_sample: false,
        check_working: false,
        check_impurities: false,
      },
      cust_status: 0,
      cust_activated: 0,
      SWIhtml: "",
      selProducts: null,
      material_id_auto_suggest:""
    };
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.showCreateTasks === true) {
      this.setState({ showCreateTasks: nextProps.showCreateTasks });

      API.get(`/api/employees/other`)
        .then((res) => {
          //console.log(res.data.data);

          var myTeam = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            myTeam.push({
              value: element["employee_id"],
              label:
                element["first_name"] +
                " " +
                element["last_name"] +
                " (" +
                element["desig_name"] +
                ")",
            });
          }

          this.setState({ employee_list: myTeam });

          API.get("/api/employees/company")
            .then((res) => {
              var myCompany = [];
              for (let index = 0; index < res.data.data.length; index++) {
                const element = res.data.data[index];
                myCompany.push({
                  value: element["company_id"],
                  label: htmlDecode(element["company_name"]),
                });
              }
              this.setState({ company_list: myCompany });

              API.get(`/api/feed/request_type`)
                .then((res) => {
                  var requestTypes = [];
                  for (let index = 0; index < res.data.data.length; index++) {
                    const element = res.data.data[index];
                    if (
                      element["type_id"] === 1 ||
                      element["type_id"] === 7 ||
                      element["type_id"] === 23 ||
                      element["type_id"] === 13 ||
                      element["type_id"] === 9 ||
                      element["type_id"] === 5 ||
                      element["type_id"] === 6 ||
                      element["type_id"] === 8 ||
                      element["type_id"] === 11 ||
                      element["type_id"] === 14 ||
                      element["type_id"] === 15 ||
                      element["type_id"] === 16 ||
                      element["type_id"] === 20 ||
                      element["type_id"] === 4 ||
                      element["type_id"] === 10 ||
                      element["type_id"] === 12 ||
                      element["type_id"] === 17 ||
                      element["type_id"] === 18 ||
                      element["type_id"] === 19 ||
                      element["type_id"] === 21 ||
                      element["type_id"] === 22 ||
                      element["type_id"] === 24 ||
                      element["type_id"] === 2 ||
                      element["type_id"] === 3 ||
                      element["type_id"] === 27 ||
                      element["type_id"] === 28 ||
                      element["type_id"] === 29 ||
                      element["type_id"] === 30 ||
                      element["type_id"] === 31 ||
                      element["type_id"] === 32 ||
                      element["type_id"] === 33 ||
                      element["type_id"] === 34 ||
                      element["type_id"] === 35 ||
                      element["type_id"] === 36 ||
                      element["type_id"] === 37 ||
                      element["type_id"] === 38 ||
                      element["type_id"] === 39 ||
                      element["type_id"] === 40 ||
                      element["type_id"] === 41 ||
                      element["type_id"] === 42 ||
                      element["type_id"] === 44
                    ) {
                      requestTypes.push({
                        value: element["type_id"],
                        label: element["req_name"],
                      });
                    }
                  }

                  this.setState({
                    request_type: requestTypes,
                    is_loading: false,
                  });
                })
                .catch((err) => {
                  console.log(err);
                });
            })
            .catch((err) => {
              console.log(err);
            });
        })
        .catch((err) => {
          console.log(err);
        });

      API.get(`/api/feed/country`)
        .then((res) => {
          var country = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            country.push({
              value: element["country_id"],
              label: htmlDecode(element["country_name"]),
            });
          }
          this.setState({ country_list: country });
        })
        .catch((err) => {
          console.log(err);
        });

      API.get(`/api/feed/products`)
        .then((res) => {
          var products = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            products.push({
              value: element["product_id"],
              label: htmlDecode(element["product_name"]),
            });
          }
          this.setState({ product_list: products });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  handleClose = () => {
    var close = {
      showCreateTasks: false,
      customer_list: [],
      employee_list: [],
      company_list: [],
      files: [],
      request_type: [],
      showRequestTypeHTML: false,
      showCustomerTypeHTML: false,
      subForm: {},
      createTaskLoader: false,
      filesHtml: "",
    };
    this.setState(close);

    this.setState({
      request_type_id: 0,
      subForm: {},
      pharmacopoeia: "",
      polymorphic_form: "",
      nature_issue: "",
      batch_number: "",
      quantity: "",
      units: "",
      audit_visit_site_name: "",
      delivery_date: "",
      request_audit_visit_date: "",
      stability_data_type: "",
      sampleBatchNo: 0,
      sampleQuantity: "",
      sampleUnit: "",
      workingQuantity: "",
      workingUnit: "",
      impureRequired: "",
      impureQuantity: "",
      impureUnit: "",
      service_check_type: {
        check_sample: false,
        check_working: false,
        check_impurities: false,
      },
      service_request_type: {
        field_sample: "Samples",
        field_working: "Working Standards",
        field_impurites: "Impurities",
      },
      date_of_response: "",
      po_delivery_date: "",
      dmf_number: "",
      apos_document_type: "",
      notification_number: "",
      material_id_auto_suggest:"",
      material_id: "",
      SWIhtml: "",
      selProducts: null,
    });

    this.setState({
      showRequestTypeHTML: false,
      showCustomerTypeHTML: false,
      customer_id: 0,
      request_type_id: 0,
      subForm: {},
    });

    this.props.handleClose(close);
  };

  handleSubmitCreateTask = (values, actions) => {
    // console.log("values", values);

    this.setState({ createTaskLoader: true });
    var err_cnt = 0;
    if (
      !values.request_type_id ||
      values.request_type_id === undefined ||
      values.request_type_id === ""
    ) {
      err_cnt++;
      actions.setErrors({ request_type_id: "Please select a service request" });
      actions.setSubmitting(false);
      this.setState({ createTaskLoader: false });
    }

    if (
      values.request_type_id === "2" ||
      values.request_type_id === "3" ||
      values.request_type_id === "4" ||
      values.request_type_id === "6" ||
      values.request_type_id === "10" ||
      values.request_type_id === "12" ||
      values.request_type_id === "14" ||
      values.request_type_id === "15" ||
      values.request_type_id === "16" ||
      values.request_type_id === "21" ||
      values.request_type_id === "22" ||
      values.request_type_id === "35" ||
      values.request_type_id === "36" ||
      values.request_type_id === "37" ||
      values.request_type_id === "42"
    ) {
      if (values.product_id.value === undefined || values.product_id.value === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
    } else if (values.request_type_id === "1") {
      if (!values.product_id ||  values.product_id.length === 0) {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (
        values.shipping_address === undefined ||
        values.shipping_address.trim() === ""
      ) {
        err_cnt++;
        actions.setErrors({
          shipping_address: "Please enter shipping address",
        });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else {
        let swiData = this.state.selProducts;
        let err = 0;
        for (let index = 0; index < swiData.length; index++) {
          const element = swiData[index];
          element.common_err = "";
          let err_arr = [];

          if (
            element.sample.chk === false &&
            element.impurities.chk === false &&
            element.working_standards.chk === false
          ) {
            err_arr.push(
              `Please select atleast one Samples/Working Standards/Impurities.`
            );
            err++;
          } else {
            let numbers = /^(\d*\.)?\d+$/;
            if (element.sample.chk === true) {
              if (+element.sample.batch === 0) {
                err_arr.push(`Samples: Invalid no of batch.`);
                err++;
              } else if (!inArray(+element.sample.batch, [1, 2, 3])) {
                err_arr.push(`Samples: Invalid no of batch.`);
                err++;
              }

              if (
                element.sample.quantity != "" ||
                element.sample.unit.length > 0
              ) {
                if (element.sample.quantity === 0) {
                  err_arr.push(`Samples: Quantity should be greater than 0.`);
                  err++;
                } else if (!element.sample.quantity.match(numbers)) {
                  err_arr.push(`Samples: Invalid quantity.`);
                  err++;
                }

                if (element.sample.unit.length > 0) {
                  if (
                    !inArray(element.sample.unit[0].value.toLowerCase(), [
                      "mgs",
                      "gms",
                      "kgs","vials"
                    ])
                  ) {
                    err_arr.push("Samples: Invalid units.");
                    err++;
                  }
                } else {
                  err_arr.push("Samples: Please enter units.");
                  err++;
                }
              }
            }

            if (element.working_standards.chk === true) {
              if (
                element.working_standards.quantity != "" ||
                element.working_standards.unit.length > 0
              ) {
                if (element.working_standards.quantity === 0) {
                  err_arr.push(
                    `Working Standards: Quantity should be greater than 0.`
                  );
                  err++;
                } else if (!element.working_standards.quantity.match(numbers)) {
                  err_arr.push(`Working Standards: Invalid quantity`);
                  err++;
                }

                if (element.working_standards.unit.length > 0) {
                  if (
                    !inArray(
                      element.working_standards.unit[0].value.toLowerCase(),
                      ["mgs", "gms", "kgs","vials"]
                    )
                  ) {
                    err_arr.push("Working Standards: Invalid units.");
                    err++;
                  }
                } else {
                  err_arr.push("Working Standards: Please enter units.");
                  err++;
                }
              } else {
                err_arr.push("Working Standards: Invalid quantity/units.");
                err++;
              }
            }

            if (element.impurities.chk === true) {
              if (
                element.impurities.quantity != "" ||
                element.impurities.unit.length > 0
              ) {
                if (element.impurities.quantity === 0) {
                  err_arr.push(
                    `Impurities: Quantity should be greater than 0.`
                  );
                  err++;
                } else if (!element.impurities.quantity.match(numbers)) {
                  err_arr.push(`Impurities: Invalid quantity`);
                  err++;
                }
                if (element.impurities.unit.length > 0) {
                  if (
                    !inArray(element.impurities.unit[0].value.toLowerCase(), [
                      "mgs",
                      "gms",
                      "kgs","vials"
                    ])
                  ) {
                    err_arr.push("Impurities: Invalid units.");
                    err++;
                  }
                } else {
                  err_arr.push("Impurities: Please enter units.");
                  err++;
                }
              } else {
                err_arr.push("Impurities: Invalid quantity/units.");
                err++;
              }
            }
          }

          if (err_arr.length > 0) {
            //err_arr.push(`${element.product_name}`);
            element.common_err = `${err_arr.join("<br/>")}`;
          }
        }

        if (err > 0) {
          err_cnt++;
          actions.setSubmitting(false);
          this.setState(
            { selProducts: swiData, createTaskLoader: false },
            () => {
              this.getHtmlSIW(swiData);
            }
          );
        }
      }
    } else if (values.request_type_id === "5") {
      if (values.product_id.value === undefined || values.product_id.value === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (this.state.files.length === 0) {
        err_cnt++;
        actions.setErrors({ file_name: "Please select attachment" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
    } else if (values.request_type_id === "8") {
      if (values.product_id.value === undefined || values.product_id.value === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (this.state.files.length === 0) {
        err_cnt++;
        actions.setErrors({ file_name: "Please select attachment" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
    } else if (values.request_type_id === "11") {
      if (values.product_id.value === undefined || values.product_id.value === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (this.state.files.length === 0) {
        err_cnt++;
        actions.setErrors({ file_name: "Please select attachment" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
    } else if (values.request_type_id === "7") {
      if (values.product_id.value === undefined || values.product_id.value === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (
        !values.batch_number ||
        values.batch_number === undefined ||
        values.batch_number.trim() === ""
      ) {
        err_cnt++;
        actions.setErrors({ batch_number: "Please enter batch number" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (values.batch_number.length < 10) {
        err_cnt++;
        actions.setErrors({
          batch_number: "Batch Number cannot be less than 10 characters",
        });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (
        (typeof values.quantity !== "undefined" && values.quantity !== "") ||
        (typeof values.units !== "undefined" && values.units !== "")
      ) {
        var numbers = /^(\d*\.)?\d+$/;
        if (values.quantity === undefined || values.quantity.trim() === "") {
          err_cnt++;
          actions.setErrors({ quantity: "Please enter quantity" });
          actions.setSubmitting(false);
          this.setState({ createTaskLoader: false });
        } else if (values.quantity == 0) {
          err_cnt++;
          actions.setErrors({ quantity: "Quantity should be greater than 0." });
          actions.setSubmitting(false);
          this.setState({ createTaskLoader: false });
        } else if (!values.quantity.match(numbers)) {
          err_cnt++;
          actions.setErrors({ quantity: "Please enter numeric value." });
          actions.setSubmitting(false);
          this.setState({ createTaskLoader: false });
        } else if (values.units === undefined || values.units === "") {
          err_cnt++;
          actions.setErrors({ units: "Please select unit" });
          actions.setSubmitting(false);
          this.setState({ createTaskLoader: false });
        }
      }
    } else if (values.request_type_id === "9") {
      if (values.product_id.value === undefined || values.product_id.value === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (
        values.audit_visit_site_name === undefined ||
        values.audit_visit_site_name.trim() === ""
      ) {
        err_cnt++;
        actions.setErrors({ audit_visit_site_name: "Please enter site name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (
        values.request_audit_visit_date === undefined ||
        values.request_audit_visit_date === ""
      ) {
        err_cnt++;
        actions.setErrors({
          request_audit_visit_date: "Audit/Visit date required",
        });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
    } else if (values.request_type_id === "13") {
      if (values.product_id.value === undefined || values.product_id.value === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (
        values.stability_data_type === undefined ||
        values.stability_data_type === ""
      ) {
        err_cnt++;
        actions.setErrors({ stability_data_type: "Please select data type" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
    } else if (
      values.request_type_id === "17" ||
      values.request_type_id === "18" ||
      values.request_type_id === "19" ||
      values.request_type_id === "20"
    ) {
      if (values.product_id.value === undefined || values.product_id.value === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (values.country_id === null || values.country_id === "") {
        err_cnt++;
        actions.setErrors({ country_id: "Market field is required." });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
    } else if (values.request_type_id === "23") {
      var numbers = /^(\d*\.)?\d+$/;
      // console.log(values.delivery_date);
      if (values.product_id.value === undefined || values.product_id.value === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (values.country_id === null || values.country_id === "") {
        err_cnt++;
        actions.setErrors({ country_id: "Market field is required." });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (
        values.quantity === undefined ||
        values.quantity.trim() === ""
      ) {
        err_cnt++;
        actions.setErrors({ quantity: "Please enter quantity" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (!values.quantity.match(numbers)) {
        err_cnt++;
        actions.setErrors({ quantity: "Please enter numeric value." });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (values.units === undefined || values.units === "") {
        err_cnt++;
        actions.setErrors({ units: "Please select unit" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } 
      else if (
        values.delivery_date === undefined ||
        values.delivery_date === ""
      ) {
        err_cnt++;
        actions.setErrors({ delivery_date: "Please enter RDD" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }else if (values.material_id === undefined || values.material_id === "") {
        err_cnt++;
        actions.setErrors({ material_id: "Please set Product Code" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }else if(!values.po_number || values.po_number == undefined || values.po_number.trim() == ''){
        err_cnt++;
        actions.setErrors({ po_number: "Please enter po number" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }

      // else if(values.po_delivery_date != undefined || (values.po_number != undefined || (values.po_number && values.po_number.trim() != ''))){
      //   if(values.po_delivery_date != '' && (values.po_number == undefined || (values.po_number && values.po_number.trim() == '')) ){
      //     err_cnt++;
      //     actions.setErrors({ po_number: "Please enter po number" });
      //     actions.setSubmitting(false);
      //     this.setState({ createTaskLoader: false });
      //   }else if((values.po_delivery_date == undefined) && (values.po_number && values.po_number.trim() != '')){
      //     err_cnt++;
      //     actions.setErrors({ po_delivery_date: "Please enter po date" });
      //     actions.setSubmitting(false);
      //     this.setState({ createTaskLoader: false });
      //   }
      // }
    } else if (values.request_type_id === "27") {
      if (values.product_id.value === undefined || values.product_id.value === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
    } else if (values.request_type_id === "28") {
      if (values.product_id.value === undefined || values.product_id.value === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (values.country_id === null || values.country_id === "") {
        err_cnt++;
        actions.setErrors({ country_id: "Market field is required." });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
    } else if (values.request_type_id === "29") {
      if (values.product_id.value === undefined || values.product_id.value === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (values.country_id === null || values.country_id === "") {
        err_cnt++;
        actions.setErrors({ country_id: "Market field is required." });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
    } else if (values.request_type_id === "30") {
      if (values.product_id.value === undefined || values.product_id.value === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
    } else if (values.request_type_id === "31") {
      let numbers = /^(\d*\.)?\d+$/;
      if (values.product_id.value === undefined || values.product_id.value === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (values.country_id === null || values.country_id === "") {
        err_cnt++;
        actions.setErrors({ country_id: "Market field is required." });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (
        values.quantity === undefined ||
        values.quantity.trim() === ""
      ) {
        err_cnt++;
        actions.setErrors({ quantity: "Please enter quantity" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (!values.quantity.match(numbers)) {
        err_cnt++;
        actions.setErrors({ quantity: "Invalid quantity" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (values.units === undefined || values.units === "") {
        err_cnt++;
        actions.setErrors({ units: "Please select unit" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
    } else if (values.request_type_id === "32") {
      if (values.product_id.value === undefined || values.product_id.value === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (values.country_id === null || values.country_id === "") {
        err_cnt++;
        actions.setErrors({ country_id: "Market field is required." });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
    } else if (values.request_type_id === "33") {
      if (values.product_id.value === undefined || values.product_id.value === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (values.country_id === null || values.country_id === "") {
        err_cnt++;
        actions.setErrors({ country_id: "Market field is required." });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (this.state.files.length === 0) {
        err_cnt++;
        actions.setErrors({ file_name: "Please attach file" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
    } else if (values.request_type_id === "34") {
      if (values.product_id.value === undefined || values.product_id.value === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (
        !values.batch_number ||
        values.batch_number === undefined ||
        values.batch_number.trim() === ""
      ) {
        err_cnt++;
        actions.setErrors({ batch_number: "Please enter batch number" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (values.batch_number.length < 10) {
        err_cnt++;
        actions.setErrors({
          batch_number: "Batch Number cannot be less than 10 characters",
        });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (
        (typeof values.quantity !== "undefined" && values.quantity !== "") ||
        (typeof values.units !== "undefined" && values.units !== "")
      ) {
        var numbers = /^(\d*\.)?\d+$/;
        if (values.quantity === undefined || values.quantity.trim() === "") {
          err_cnt++;
          actions.setErrors({ quantity: "Please enter quantity" });
          actions.setSubmitting(false);
          this.setState({ createTaskLoader: false });
        } else if (values.quantity == 0) {
          err_cnt++;
          actions.setErrors({ quantity: "Quantity should be greater than 0." });
          actions.setSubmitting(false);
          this.setState({ createTaskLoader: false });
        } else if (!values.quantity.match(numbers)) {
          err_cnt++;
          actions.setErrors({ quantity: "Please enter numeric value." });
          actions.setSubmitting(false);
          this.setState({ createTaskLoader: false });
        } else if (values.units === undefined || values.units === "") {
          err_cnt++;
          actions.setErrors({ units: "Please select unit" });
          actions.setSubmitting(false);
          this.setState({ createTaskLoader: false });
        }
      }
    } else if (values.request_type_id === "39") {
      if (values.product_id.value === undefined || values.product_id.value === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
      if (
        values.country_id === undefined ||
        values.country_id === "" ||
        values.country_id === null
      ) {
        err_cnt++;
        actions.setErrors({ country_id: "Market field is required." });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
      if (values.dmf_number === undefined || values.dmf_number === "") {
        err_cnt++;
        actions.setErrors({ dmf_number: "Please enter DMF/CEP value" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
      if (this.state.files.length === 0) {
        err_cnt++;
        actions.setErrors({ file_name: "Please upload a file" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
    } else if (values.request_type_id === "40") {
      if (values.product_id.value === undefined || values.product_id.value === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
      if (
        values.gmp_clearance_id === undefined ||
        values.gmp_clearance_id === ""
      ) {
        err_cnt++;
        actions.setErrors({
          gmp_clearance_id: "Please enter gmp clearance id",
        });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
      if (values.tga_email_id === undefined || values.tga_email_id === "") {
        err_cnt++;
        actions.setErrors({ tga_email_id: "Please enter email id" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
      if (values.applicant_name === undefined || values.applicant_name === "") {
        err_cnt++;
        actions.setErrors({
          applicant_name: "Please enter applicant's name and address",
        });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
      if (values.doc_required === undefined || values.doc_required === "") {
        err_cnt++;
        actions.setErrors({ doc_required: "Please enter documents required" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
    } else if (values.request_type_id === "41") {
      var numbers = /^(\d*\.)?\d+$/;
      if (values.product_id.value === undefined || values.product_id.value === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (values.country_id === null || values.country_id === "") {
        err_cnt++;
        actions.setErrors({ country_id: "Market field is required." });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (
        values.quantity === undefined ||
        values.quantity.trim() === ""
      ) {
        err_cnt++;
        actions.setErrors({ quantity: "Please enter quantity" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (!values.quantity.match(numbers)) {
        err_cnt++;
        actions.setErrors({ quantity: "Please enter numeric value." });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (values.units === undefined || values.units === "") {
        err_cnt++;
        actions.setErrors({ units: "Please select unit" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
      // else if( this.state.files.length === 0 ){
      //     err_cnt++;
      //     actions.setErrors({file_name:'Please attach file'});
      //     actions.setSubmitting(false);
      //     this.setState({createTaskLoader:false});
      // }
    } else if (values.request_type_id === "42") {
      var numbers = /^(\d*\.)?\d+$/;
      if (values.product_id.value === undefined || values.product_id.value === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (values.country_id === null || values.country_id === "") {
        err_cnt++;
        actions.setErrors({ country_id: "Market field is required." });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      }
    } else if (values.request_type_id === "44") {
      var numbers = /^(\d*\.)?\d+$/;
      if (values.product_id.value === undefined || values.product_id.value === "") {
        err_cnt++;
        actions.setErrors({ product_id: "Please select product name" });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (values.country_id === null || values.country_id === "") {
        err_cnt++;
        actions.setErrors({ country_id: "Market field is required." });
        actions.setSubmitting(false);
        this.setState({ createTaskLoader: false });
      } else if (
        (typeof values.quantity !== "undefined" && values.quantity !== "") ||
        (typeof values.units !== "undefined" && values.units !== "")
      ) {
        var numbers = /^(\d*\.)?\d+$/;
        if (values.quantity === undefined || values.quantity.trim() === "") {
          err_cnt++;
          actions.setErrors({ quantity: "Please enter quantity" });
          actions.setSubmitting(false);
          this.setState({ createTaskLoader: false });
        } else if (values.quantity == 0) {
          err_cnt++;
          actions.setErrors({ quantity: "Quantity should be greater than 0." });
          actions.setSubmitting(false);
          this.setState({ createTaskLoader: false });
        } else if (!values.quantity.match(numbers)) {
          err_cnt++;
          actions.setErrors({ quantity: "Please enter numeric value." });
          actions.setSubmitting(false);
          this.setState({ createTaskLoader: false });
        } else if (values.units === undefined || values.units === "") {
          err_cnt++;
          actions.setErrors({ units: "Please select unit" });
          actions.setSubmitting(false);
          this.setState({ createTaskLoader: false });
        }
      }
    } 

    if (err_cnt === 0) {
      var formData = new FormData();
      formData.append(
        "ccCustId",
        values.cc_customer_id != "" ? JSON.stringify(values.cc_customer_id) : ""
      );

      formData.append("request_type", values.request_type_id);
      if (values.request_type_id === "1") {
        formData.append("product_id", JSON.stringify(values.product_id));
      } else {
        formData.append("product_id", values.product_id.value);
      }
      formData.append("customer_id", values.customer_id);
      let post_country ="";
      if(values.country_id != "" && values.country_id != null){
        if(Array.isArray(values.country_id)){
          post_country = JSON.stringify(values.country_id);
        }else{
          post_country = JSON.stringify([values.country_id]);
        }
      }
      formData.append("country_id",post_country);

      formData.append("content", values.content);
      formData.append("share_with_agent", values.share_with_agent);

      if (this.state.files && this.state.files.length > 0) {
        for (let index = 0; index < this.state.files.length; index++) {
          const element = this.state.files[index];
          formData.append("file", element);
        }
      } else {
        formData.append("file", "");
      }

      if (values.request_type_id === "1") {
        //Samples/Working Standards/Impurities
        formData.append("pharmacopoeia", values.pharmacopoeia);
        formData.append("shipping_address", values.shipping_address);

        formData.append(
          "swi_data",
          this.state.selProducts.length > 0
            ? JSON.stringify(this.state.selProducts)
            : JSON.stringify([])
        );
      } else if (values.request_type_id === "2") {
        //Typical CoA
        formData.append("pharmacopoeia", values.pharmacopoeia);
        formData.append("polymorphic_form", values.polymorphic_form);
      } else if (values.request_type_id === "3") {
        //Spec and MOA
        formData.append("pharmacopoeia", values.pharmacopoeia);
        formData.append("polymorphic_form", values.polymorphic_form);
      } else if (values.request_type_id === "4") {
        //Method Related Queries
        formData.append("pharmacopoeia", values.pharmacopoeia);
      } else if (values.request_type_id === "5") {
        //Vendor Questionnaire
      } else if (values.request_type_id === "6") {
        //CDA/Sales Agreement
      } else if (values.request_type_id === "7") {
        //Complaints
        formData.append(
          "quantity",
          values.quantity && values.quantity !== "" ? values.quantity : ""
        );
        formData.append(
          "unit",
          values.units && values.units !== "" ? values.units : ""
        );
        formData.append(
          "batch_number",
          values.batch_number && values.batch_number !== ""
            ? values.batch_number
            : ""
        );
        formData.append(
          "nature_of_issue",
          values.nature_issue && values.nature_issue !== ""
            ? values.nature_issue
            : ""
        );
      } else if (values.request_type_id === "8") {
        //Recertification of CoA
      } else if (values.request_type_id === "9") {
        //Audit/Visit Request
        formData.append("audit_visit_site_name", values.audit_visit_site_name);
        formData.append(
          "request_audit_visit_date",
          values.request_audit_visit_date
        );
      } else if (values.request_type_id === "10") {
        //Customized Spec Request
        formData.append("pharmacopoeia", values.pharmacopoeia);
      } else if (values.request_type_id === "11") {
        //Quality Agreement
      } else if (values.request_type_id === "12") {
        //Quality Equivalence Request
        formData.append("pharmacopoeia", values.pharmacopoeia);
      } else if (values.request_type_id === "13") {
        //Stability Data
        formData.append("stability_data_type", values.stability_data_type);
      } else if (values.request_type_id === "14") {
        //Elemental Impurity (EI) Declaration
      } else if (values.request_type_id === "15") {
        //Residual Solvents Declaration
      } else if (values.request_type_id === "16") {
        //Storage and Transport Declaration
      } else if (values.request_type_id === "17") {
        //Request for DMF
        formData.append("pharmacopoeia", values.pharmacopoeia);
      } else if (values.request_type_id === "18") {
        //Request for CEP Copy
        formData.append("pharmacopoeia", values.pharmacopoeia);
      } else if (values.request_type_id === "19") {
        //Request for LOA
        formData.append("pharmacopoeia", values.pharmacopoeia);
      } else if (values.request_type_id === "20") {
        //Request for LOC/LOE
      } else if (values.request_type_id === "21") {
        //Request for Additional Declarations
        formData.append("pharmacopoeia", values.pharmacopoeia);
      } else if (values.request_type_id === "22") {
        //General Request
        formData.append("pharmacopoeia", values.pharmacopoeia);
      } else if (values.request_type_id === "23") {
        //New Order
        formData.append(
          "quantity",
          values.quantity && values.quantity !== "" ? values.quantity : ""
        );
        formData.append(
          "unit",
          values.units && values.units !== "" ? values.units : ""
        );
        formData.append(
          "po_number",
          values.po_number && values.po_number !== "" ? values.po_number : ""
        );
        // formData.append(
        //   "po_delivery_date",
        //   values.po_delivery_date && values.po_delivery_date !== "" ? values.po_delivery_date : ""
        // );
        formData.append(
          "rdd",
          values.delivery_date && values.delivery_date !== ""
            ? values.delivery_date
            : ""
        );
        formData.append(
          "material_id",
          values.material_id && values.material_id !== ""
            ? values.material_id
            : ""
        );
      } else if (values.request_type_id === "24") {
        //Forecast
      } else if (values.request_type_id === "27") {
        //Queries related to DMF
        formData.append(
          "dmf_number",
          values.dmf_number && values.dmf_number !== "" ? values.dmf_number : ""
        );
      } else if (values.request_type_id === "28") {
        //Queries related to Deficiencies
        formData.append(
          "date_of_response",
          values.date_of_response && values.date_of_response !== ""
            ? values.date_of_response
            : ""
        );
      } else if (values.request_type_id === "29") {
        //Queries related to Notifications
        formData.append(
          "notification_number",
          values.notification_number && values.notification_number !== ""
            ? values.notification_number
            : ""
        );
      } else if (values.request_type_id === "30") {
        //Quality Overall Summary (QOS)
        formData.append("pharmacopoeia", values.pharmacopoeia);
      } else if (values.request_type_id === "31") {
        //Request for a Price Quote
        formData.append(
          "quantity",
          values.quantity && values.quantity !== "" ? values.quantity : ""
        );
        formData.append(
          "unit",
          values.units && values.units !== "" ? values.units : ""
        );
      } else if (values.request_type_id === "32") {
        //IP Related Enquiries
        formData.append("pharmacopoeia", values.pharmacopoeia);
      } else if (values.request_type_id === "33") {
        //Master File Agreement
        formData.append("pharmacopoeia", values.pharmacopoeia);
      } else if (values.request_type_id === "34") {
        //Logistics Related Complaints
        formData.append(
          "quantity",
          values.quantity && values.quantity !== "" ? values.quantity : ""
        );
        formData.append(
          "unit",
          values.units && values.units !== "" ? values.units : ""
        );
        formData.append(
          "batch_number",
          values.batch_number && values.batch_number !== ""
            ? values.batch_number
            : ""
        );
        formData.append(
          "nature_of_issue",
          values.nature_issue && values.nature_issue !== ""
            ? values.nature_issue
            : ""
        );
      } else if (values.request_type_id === "35") {
        //Quality General Request
        formData.append("pharmacopoeia", values.pharmacopoeia);
      } else if (values.request_type_id === "36") {
        //Regulatory General Request
        formData.append("pharmacopoeia", values.pharmacopoeia);
      } else if (values.request_type_id === "37") {
        //Legal General Request
        formData.append("pharmacopoeia", values.pharmacopoeia);
      } else if (values.request_type_id === "38") {
        //Apostallation of Documents
        formData.append(
          "apos_document_type",
          values.apos_document_type != "" && values.apos_document_type != null
            ? JSON.stringify(values.apos_document_type)
            : ""
        );
      } else if (values.request_type_id === "39") {
        formData.append(
          "dmf_number",
          values.dmf_number && values.dmf_number !== "" ? values.dmf_number : ""
        );
        formData.append(
          "rdd",
          values.delivery_date && values.delivery_date !== ""
            ? values.delivery_date
            : ""
        );
      } else if (values.request_type_id === "40") {
        formData.append("gmp_clearance_id", values.gmp_clearance_id);
        formData.append("tga_email_id", values.tga_email_id);
        formData.append("applicant_name", values.applicant_name);
        formData.append("doc_required", values.doc_required);
        formData.append(
          "rdd",
          values.delivery_date && values.delivery_date !== ""
            ? values.delivery_date
            : ""
        );
      } else if (values.request_type_id === "41") {
        //New Order
        formData.append(
          "quantity",
          values.quantity && values.quantity !== "" ? values.quantity : ""
        );
        formData.append(
          "unit",
          values.units && values.units !== "" ? values.units : ""
        );
        formData.append(
          "rdd",
          values.delivery_date && values.delivery_date !== ""
            ? values.delivery_date
            : ""
        );
        formData.append(
          "pharmacopoeia",
          values.pharmacopoeia && values.pharmacopoeia !== ""
            ? values.pharmacopoeia
            : ""
        );
      } else if (values.request_type_id === "42") {
        //General Request
        formData.append("pharmacopoeia", values.pharmacopoeia);
      }  else if (values.request_type_id === "44") {
        //New Order
        formData.append(
          "quantity",
          values.quantity && values.quantity !== "" ? values.quantity : ""
        );
        formData.append(
          "unit",
          values.units && values.units !== "" ? values.units : ""
        );
        formData.append(
          "pharmacopoeia",
          values.pharmacopoeia && values.pharmacopoeia !== ""
            ? values.pharmacopoeia
            : ""
        );
      }

      const config = {
        headers: {
          "content-type": "multipart/form-data",
        },
      };
      let url = `/api/add_task`;
      if (values.request_type_id === "1") {
        url = `/api/add_task/sample`;
      }

      API.post(url, formData, config)
        .then((res) => {
          this.handleClose();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "New Task has been created.",
            icon: "success",
          }).then(() => {
            this.setState({ createTaskLoader: false });
            this.props.allTaskDetails();
          });
        })
        .catch((error) => {
          this.setState({
            createTaskLoader: false,
            cust_status: 0,
            cust_activated: 0,
          });
          if (error.data.status === 3) {
            var token_rm = 2;
            showErrorMessageFront(error, token_rm, this.props);
            this.props.allTaskDetails();
            this.handleClose();
          } else {
            console.log(error.data.errors);
            actions.setErrors(error.data.errors);
            actions.setSubmitting(false);
          }
        });
    }
  };

  showRequestType = (event, setFieldValue) => {
    if (event === null) {
      setFieldValue("customer_id", "");
      this.setState({
        showRequestTypeHTML: false,
        showCcCustomerTypeHTML: false,
        customer_id: 0,
        request_type_id: 0,
        subForm: {},
      });
    } else {
      this.setState({
        showRequestTypeHTML: false,
        showCcCustomerTypeHTML: false,
        customer_id: 0,
        request_type_id: 0,
        subForm: {},
      });

      API.get(
        `/api/employees/customers/${this.state.company_id}/${event.value}`
      )
        .then((res) => {
          var myCustomer = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            var keyCustomer, dummy;
            if (element.vip_customer === 1) {
              keyCustomer = "*";
            } else {
              keyCustomer = "";
            }
            // if(element.status === 1){
            //     dummy = 'Inactive User';
            // }else{
            //     dummy = '';
            // }
            dummy = "";
            myCustomer.push({
              value: element["customer_id"],
              label: htmlDecode(
                element["first_name"] +
                  " " +
                  element["last_name"] +
                  " (" +
                  element["company_name"] +
                  ") " +
                  keyCustomer +
                  " " +
                  dummy
              ),
            });
          }

          this.setState({
            cc_customer_list: myCustomer,
            cust_status: res.data.cust_status,
            cust_activated: res.data.cust_activated,
          });

          setFieldValue("customer_id", event.value.toString());
          this.setState({
            showRequestTypeHTML: true,
            showCcCustomerTypeHTML: true,
            customer_id: event.value,
          });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  showCustomerList = (event, setFieldValue, setErrors) => {
    if (event === null) {
      setFieldValue("company_id", "");
      this.setState({
        showCustomerTypeHTML: false,
        showRequestTypeHTML: false,
        showCcCustomerTypeHTML: false,
        company_id: 0,
        customer_id: 0,
        request_type_id: 0,
        subForm: {},
      });
    } else {
      setFieldValue("customer_id", "");
      setErrors({ customer_id: null });
      this.setState({
        showCustomerTypeHTML: false,
        showRequestTypeHTML: false,
        showCcCustomerTypeHTML: false,
        customer_id: 0,
        request_type_id: 0,
        subForm: {},
        createTaskLoader: true,
        customer_list: [],
      });

      API.get(`/api/employees/active_customers_create/${event.value}`)
        .then((res) => {
          var myCustomer = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            var keyCustomer, dummy;
            if (element.vip_customer === 1) {
              keyCustomer = "*";
            } else {
              keyCustomer = "";
            }
            if (element.status === 3) {
              dummy = "(Inactive User)";
            } else {
              dummy = "";
            }
            myCustomer.push({
              value: element["customer_id"],
              label: htmlDecode(
                element["first_name"] +
                  " " +
                  element["last_name"] +
                  " " +
                  keyCustomer +
                  " " +
                  dummy
              ),
            });
          }

          this.setState({
            customer_list: myCustomer,
            cust_status: 0,
            cust_activated: 0,
          });

          setFieldValue("company_id", event.value.toString());
          this.setState({
            showCustomerTypeHTML: true,
            company_id: event.value,
            createTaskLoader: false,
          });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  showCcCustomers = (event, setFieldValue, setErrors) => {
    if (event === null) {
      setFieldValue("cc_customer_id", "");
    } else {
      setFieldValue("cc_customer_id", event);
    }
  };

  onChangeRequestType = (event, setFieldValue) => {
    if (event === null) {
      setFieldValue("request_type_id", "");
    } else {
      setFieldValue("request_type_id", event.value.toString());
    }

    setFieldValue("product_id", "");

    this.setState({
      request_type_id: event.value,
      pharmacopoeia: "",
      polymorphic_form: "",
      nature_issue: "",
      batch_number: "",
      quantity: "",
      units: "",
      audit_visit_site_name: "",
      delivery_date: "",
      request_audit_visit_date: "",
      stability_data_type: "",
      sampleBatchNo: 0,
      sampleQuantity: "",
      sampleUnit: "",
      workingQuantity: "",
      workingUnit: "",
      impureRequired: "",
      impureQuantity: "",
      impureUnit: "",
      service_check_type: {
        check_sample: false,
        check_working: false,
        check_impurities: false,
      },
      service_request_type: {
        field_sample: "Samples",
        field_working: "Working Standards",
        field_impurites: "Impurities",
      },
      date_of_response: "",
      po_delivery_date:"",
      dmf_number: "",
      apos_document_type: "",
      notification_number: "",
      material_id_auto_suggest:"",
      material_id:"",
      gmp_clearance_id: "",
      tga_email_id: "",
      applicant_name: "",
      doc_required: "",
      selProducts: null,
      SWIhtml: "",
    });
  };

  toggleCheckboxSample = (event, setFieldValue) => {
    if (event.target.checked) {
      setFieldValue("samplesCheck", event.target.value);
    } else {
      setFieldValue("samplesCheck", "");
    }

    this.setState({
      service_check_type: {
        check_sample: !this.state.service_check_type.check_sample,
        check_working: this.state.service_check_type.check_working,
        check_impurities: this.state.service_check_type.check_impurities,
      },
    });
  };

  toggleCheckboxWorking = (event, setFieldValue) => {
    if (event.target.checked) {
      setFieldValue("workingCheck", event.target.value);
    } else {
      setFieldValue("workingCheck", "");
    }

    this.setState({
      service_check_type: {
        check_sample: this.state.service_check_type.check_sample,
        check_working: !this.state.service_check_type.check_working,
        check_impurities: this.state.service_check_type.check_impurities,
      },
    });
  };

  toggleCheckboxImpure = (event, setFieldValue) => {
    if (event.target.checked) {
      setFieldValue("impureCheck", event.target.value);
    } else {
      setFieldValue("impureCheck", "");
    }

    this.setState({
      service_check_type: {
        check_sample: this.state.service_check_type.check_sample,
        check_working: this.state.service_check_type.check_working,
        check_impurities: !this.state.service_check_type.check_impurities,
      },
    });
  };

  setSampleBatchNo = (event, setFieldValue) => {
    setFieldValue("sampleBatchNo", event.target.value);
    this.setState({ sampleBatchNo: event.target.value });
  };

  setSampleQuantity = (event, setFieldValue) => {
    setFieldValue("sampleQuantity", event.target.value.toString());
    this.setState({ sampleQuantity: event.target.value });
  };

  setSampleUnit = (event, setFieldValue) => {
    setFieldValue("sampleUnit", event.target.value);
    this.setState({ sampleUnit: event.target.value });
  };

  setWorkingQuantity = (event, setFieldValue) => {
    setFieldValue("workingQuantity", event.target.value);
    this.setState({ workingQuantity: event.target.value });
  };

  setWorkingUnit = (event, setFieldValue) => {
    setFieldValue("workingUnit", event.target.value);
    this.setState({ workingUnit: event.target.value });
  };

  setImpure = (event, setFieldValue) => {
    setFieldValue("impureRequired", event.target.value);
    this.setState({ impureRequired: event.target.value });
  };

  setImpureQuantity = (event, setFieldValue) => {
    setFieldValue("impureQuantity", event.target.value);
    this.setState({ impureQuantity: event.target.value });
  };

  setImpureUnit = (event, setFieldValue) => {
    setFieldValue("impureUnit", event.target.value);
    this.setState({ impureUnit: event.target.value });
  };

  setCountry = (event, setFieldValue) => {
    // console.log("select country", event);
    if (event.length == 0) {
      setFieldValue("country_id", "");
      this.setState({ country_id: "" });
    } else {
      setFieldValue("country_id", event);
      this.setState({ country_id: event });
    }
  };

  setADocType = (event, setFieldValue) => {
    setFieldValue("apos_document_type", event);
    this.setState({ apos_document_type: event });
  };

  setProduct = (event, setFieldValue) => {
    if (event === null) {
      setFieldValue("product_id", "");
      this.setState({ product_id: "" });
    } else {
      setFieldValue("product_id", event);
      this.setState({ product_id: event });
    }
  };

  setPolymorphic = (event, setFieldValue) => {
    setFieldValue("polymorphic_form", event.target.value.toString());
    this.setState({ polymorphic_form: event.target.value });
  };

  setPharma = (event, setFieldValue) => {
    setFieldValue("pharmacopoeia", event.target.value.toString());
    this.setState({ pharmacopoeia: event.target.value });
  };

  setDMF = (event, setFieldValue) => {
    setFieldValue("dmf_number", event.target.value.toString());
    this.setState({ dmf_number: event.target.value });
  };

  setNotificationNumber = (event, setFieldValue) => {
    setFieldValue("notification_number", event.target.value.toString());
    this.setState({ notification_number: event.target.value });
  };

  setMaterialID = (event, setFieldValue) => {
    setFieldValue("material_id", event.target.value.toString());
    this.setState({ material_id: event.target.value });


    if(event.target.value && event.target.value.length > 2){
      API.post(`/api/feed/get_sap_Product_code`,{material_id:event.target.value})
      .then((res) => {
        
        let ret_html = res.data.data.map((val,index)=>{
          return (<li style={{cursor:'pointer',marginTop:'6px'}} onClick={()=>this.setMaterialIDAutoSuggest(val.value,setFieldValue)} >{val.label}<hr style={{marginTop:'6px',marginBottom:'0px'}} /></li>)
        })
        this.setState({material_id_auto_suggest:ret_html});
      })
      .catch((err) => {
        console.log(err);
      });
    }
  };

  setMaterialIDAutoSuggest = (event, setFieldValue) => {
    setFieldValue("material_id", event.toString());
    this.setState({ material_id: event,material_id_auto_suggest:'' });
  };


  setBatchNumber = (event, setFieldValue) => {
    setFieldValue("batch_number", event.target.value);
    this.setState({ batch_number: event.target.value });
  };

  setNatureIssue = (event, setFieldValue) => {
    setFieldValue("nature_issue", event.target.value);
    this.setState({ nature_issue: event.target.value });
  };

  setQuantity = (event, setFieldValue) => {
    setFieldValue("quantity", event.target.value);
    this.setState({ quantity: event.target.value });
  };

  setUnits = (event, setFieldValue) => {
    setFieldValue("units", event.target.value);
    this.setState({ units: event.target.value });
  };

  handleSetState = (setStateVal) => {
    this.setState(setStateVal);
  };

  handleChangeSelect = (event, setFieldValue) => {
    /* var returnObj = {...this.state.subForm}
        returnObj[ObjName.name] = event.value;
        this.setState({subForm:returnObj}); */

    if (event === null) {
      setFieldValue("stability_data_type", "");
      this.setState({ stability_data_type: 0 });
    } else {
      setFieldValue("stability_data_type", event.value.toString());
      this.setState({ stability_data_type: event.value });
    }
  };

  handleChangeText = (event, setFieldValue) => {
    /* var returnObj = {...this.state.subForm}
        returnObj[event.target.name] = event.target.value;
        this.setState({subForm:returnObj}); */
    setFieldValue("audit_visit_site_name", event.target.value);
    this.setState({ audit_visit_site_name: event.target.value });
  };

  handleChangeDate = (event, setFieldValue) => {
    /* var returnObj = {...this.state.subForm}
        returnObj['request_audit_visit_date'] = new Date(event);
        this.setState({subForm:returnObj}); */
    setFieldValue("request_audit_visit_date", new Date(event));
    this.setState({ request_audit_visit_date: new Date(event) });
  };

  handleChangeDeliveryDate = (event, setFieldValue) => {
    /* var returnObj = {...this.state.subForm}
        returnObj['request_audit_visit_date'] = new Date(event);
        this.setState({subForm:returnObj}); */
    setFieldValue("delivery_date", new Date(event));
    this.setState({ delivery_date: new Date(event) });
  };

  handleChangeDateOfResponse = (event, setFieldValue) => {
    /* var returnObj = {...this.state.subForm}
        returnObj['request_audit_visit_date'] = new Date(event);
        this.setState({subForm:returnObj}); */
    setFieldValue("date_of_response", new Date(event));
    this.setState({ date_of_response: new Date(event) });
  };

  handleChangePoDeliveryDate = (event, setFieldValue) => {
    /* var returnObj = {...this.state.subForm}
        returnObj['request_audit_visit_date'] = new Date(event);
        this.setState({subForm:returnObj}); */
    setFieldValue("po_delivery_date", new Date(event));
    this.setState({ po_delivery_date: new Date(event) });
  };

  checkSamplesWorkingImput = (request_type) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 1) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkPharmacopoeia = (request_type) => {
    if (request_type === null) {
      return false;
    } else {
      if (
        request_type === 35 ||
        request_type === 36 ||
        request_type === 37 ||
        request_type === 22 ||
        request_type === 21 ||
        request_type === 19 ||
        request_type === 18 ||
        request_type === 17 ||
        request_type === 2 ||
        request_type === 3 ||
        request_type === 4 ||
        request_type === 10 ||
        request_type === 12 ||
        request_type === 1 ||
        request_type === 30 ||
        request_type === 32 ||
        request_type === 33 ||
        request_type === 41 ||
        request_type === 44 ||
        request_type === 42
      ) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkDMFNumber = (request_type) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 27 || request_type === 39) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkNotificationNumber = (request_type) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 29) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkRDORC = (request_type) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 28) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkNatureIssue = (request_type) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 7 || request_type === 34) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkAposDocumentType = (request_type) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 38) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkForecast = (request_type) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 24) {
        return false;
      } else {
        return true;
      }
    }
  };

  checkGMPClearanceID = (request_type) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 40) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkTGAemailID = (request_type) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 40) {
        return true;
      } else {
        return false;
      }
    }
  };

  applicantDetails = (request_type) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 40) {
        return true;
      } else {
        return false;
      }
    }
  };

  docRequired = (request_type) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 40) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkBatchNumber = (request_type) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 7 || request_type === 34) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkUnitQuantity = (request_type) => {
    if (request_type === null) {
      return false;
    } else {
      if (
        request_type === 7 ||
        request_type === 23 ||
        request_type === 31 ||
        request_type === 34 ||
        request_type === 41 ||
        request_type === 44
      ) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkPolymorphicForm = (request_type) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 2 || request_type === 3) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkStabilityDataType = (request_type) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 13) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkAuditSiteName = (request_type) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 9) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkAuditVistDate = (request_type) => {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 9) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkDeliveryDate = (request_type) => {
    if (request_type === null) {
      return false;
    } else {
      if (
        request_type === 23 ||
        request_type === 39 ||
        request_type === 40 ||
        request_type === 41
      ) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkPoNumber = (request_type) => {
    if (request_type === null) {
      return false;
    } else {
      if (
        request_type === 23
      ) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkMaterialID = (request_type) => {
    if (request_type === null) {
      return false;
    } else {
      if (
        request_type === 23
      ) {
        return true;
      } else {
        return false;
      }
    }
  };

  checkPoDeliveryDate = (request_type) => {
    if (request_type === null) {
      return false;
    } else {
      if (
        request_type === 23
      ) {
        return true;
      } else {
        return false;
      }
    }
  };

  setDropZoneFiles = (acceptedFiles, setErrors, setFieldValue) => {
    if (this.state.request_type_id === 24) {
      setErrors({ file_name: false });
      setFieldValue(this.state.files);

      newFiles = acceptedFiles;

      var fileListHtml = (
        <Alert key={newFiles[0].name}>
          <span
            onClick={() => removeDropZoneFiles(newFiles[0].name, this)}
            style={{ cursor: "pointer" }}
          >
            <i className="far fa-times-circle"></i>
          </span>{" "}
          {trimString(25, newFiles[0].name)}
        </Alert>
      );

      this.setState({
        files: newFiles,
        filesHtml: fileListHtml,
      });
    } else {
      setErrors({ file_name: false });
      setFieldValue(this.state.files);

      var prevFiles = this.state.files;

      var newFiles;
      if (prevFiles.length > 0) {
        //newFiles = newConcatFiles = acceptedFiles.concat(prevFiles);

        for (let index = 0; index < acceptedFiles.length; index++) {
          var remove = 0;

          for (let index2 = 0; index2 < prevFiles.length; index2++) {
            if (acceptedFiles[index].name === prevFiles[index2].name) {
              remove = 1;
              break;
            }
          }

          //console.log('remove',acceptedFiles[index].name,remove);

          if (remove === 0) {
            prevFiles.push(acceptedFiles[index]);
          }
        }
        //console.log('acceptedFiles',acceptedFiles);
        //console.log('prevFiles',prevFiles);
        newFiles = prevFiles;
      } else {
        newFiles = acceptedFiles;
      }

      var fileListHtml = newFiles.map((file) => (
        <Alert key={file.name}>
          <span
            onClick={() => removeDropZoneFiles(file.name, this)}
            style={{ cursor: "pointer" }}
          >
            <i className="far fa-times-circle"></i>
          </span>{" "}
          {trimString(25, file.name)}
        </Alert>
      ));

      this.setState({
        files: newFiles,
        filesHtml: fileListHtml,
      });
    }
  };

  selectMultiProducts = (event, setFieldValue) => {
    if (event && (event.length === 0 || event === null)) {
      setFieldValue("product_id", []);
      this.setState({ selProducts: null }, () => {
        this.getHtmlSIW(null);
      });
    } else {
      if (event.length > 5) {
      } else {
        setFieldValue("product_id", event);

        let selProducts = [];
        let prev_state =
          this.state.selProducts != null ? this.state.selProducts : [];
        //ADD NEW PRODUCTS
        for (let index = 0; index < event.length; index++) {
          const element = event[index];

          // let indexOfEvent = event.findIndex((product) => {
          //   return product.value == element.product_id
          // });

          let indexOfPrevState = prev_state.findIndex((product) => {
            return product.product_id == element.value;
          });

          // console.log(element.value, indexOfPrevState);

          if (indexOfPrevState == "-1") {
            prev_state.push({
              product_id: element.value,
              product_name: element.label,
              sample: {
                chk: false,
                batch: 0,
                quantity: "",
                unit: [],
              },
              impurities: {
                chk: false,
                specify_impurities: "",
                quantity: "",
                unit: [],
              },
              working_standards: {
                chk: false,
                quantity: "",
                unit: [],
              },
              common_err: "",
            });
          }

          selProducts.push(element.value);
        }

        //REMOVE EXCESS PRODUCTS
        for (let index = 0; index < prev_state.length; index++) {
          const element = prev_state[index];
          if (!inArray(element.product_id, selProducts)) {
            prev_state.splice(index, 1);
          }
        }
        this.setState({ selProducts: prev_state }, () => {
          this.getHtmlSIW(prev_state);
        });
      }
    }
  };

  handleSWI = (event, index, key) => {
    let prev_state = this.state.selProducts;
    prev_state[index].common_err = "";
    if (key === "sample_check") {
      prev_state[index].sample.chk = event.target.checked;
      if (event.target.checked === false) {
        prev_state[index].sample.batch = 0;
        prev_state[index].sample.quantity = "";
        prev_state[index].sample.unit = [];
      }
    }

    if (key === "sample_batch") {
      prev_state[index].sample.batch = event.target.value;
    }

    if (key === "sample_quantity") {
      prev_state[index].sample.quantity = event.target.value;
    }

    if (key === "sample_unit") {
      if (event === null) {
        prev_state[index].sample.unit = [];
      } else {
        prev_state[index].sample.unit = [event];
      }
    }

    if (key === "working_standards_check") {
      prev_state[index].working_standards.chk = event.target.checked;
      if (event.target.checked === false) {
        prev_state[index].working_standards.quantity = "";
        prev_state[index].working_standards.unit = [];
      }
    }

    if (key === "working_standards_quantity") {
      prev_state[index].working_standards.quantity = event.target.value;
    }

    if (key === "working_standards_unit") {
      if (event === null) {
        prev_state[index].working_standards.unit = [];
      } else {
        prev_state[index].working_standards.unit = [event];
      }
    }

    if (key === "impurities_check") {
      prev_state[index].impurities.chk = event.target.checked;
      if (event.target.checked === false) {
        prev_state[index].impurities.specify_impurities = "";
        prev_state[index].impurities.quantity = "";
        prev_state[index].impurities.unit = [];
      }
    }

    if (key === "specify_impurities") {
      prev_state[index].impurities.specify_impurities = event.target.value;
    }

    if (key === "impurities_quantity") {
      prev_state[index].impurities.quantity = event.target.value;
    }

    if (key === "impurities_unit") {
      if (event === null) {
        prev_state[index].impurities.unit = [];
      } else {
        prev_state[index].impurities.unit = [event];
      }
    }

    //console.log('prev_state',prev_state);
    this.setState({ selProducts: prev_state }, () => {
      this.getHtmlSIW(prev_state);
    });
  };

  getHtmlSIW = (theState) => {
    this.setState({ SWIhtml: "" });
    if (theState === null) {
    } else {
      let htmlData = [];
      for (let index = 0; index < theState.length; index++) {
        const element = theState[index];
        htmlData.push(
          <>
            <div className="sampleCheckSec" key={`sampleCheckSec${index}`}>
              <span
                className="errorMsg"
                style={{
                  display: element.common_err !== "" ? "block" : "none",
                }}
              >
                {ReactHtmlParser(htmlDecode(element.common_err))}
              </span>
              <Row>
                <Col xs={12} sm={12} md={12}>
                  <label className="form-check-label">
                    {element.product_name}
                  </label>
                </Col>
              </Row>
              <Row>
                <Col xs={12} sm={12} md={12}>
                  <div className="form-check">
                    <label className="form-check-label">
                      <Field
                        type="checkbox"
                        className="form-check-input"
                        value="1"
                        defaultChecked={element.sample.chk}
                        onClick={(e) => {
                          this.handleSWI(e, index, "sample_check");
                        }}
                      />
                      <span style={{ fontWeight: "2" }}>Samples</span>
                    </label>
                  </div>
                </Col>

                {element.sample.chk === true ? (
                  <>
                    <Col xs={12} sm={4} md={4}>
                      <span className="fieldset-legend">Number of Batches</span>
                      <div className="clearfix"></div>
                      <label className="radio-inline">
                        <input
                          type="radio"
                          name={`no_batch${element.product_id}`}
                          onClick={(e) => {
                            this.handleSWI(e, index, "sample_batch");
                          }}
                          defaultChecked={
                            element.sample.batch === 1 ? true : false
                          }
                          value="1"
                        />
                        1
                      </label>
                      <label className="radio-inline">
                        <input
                          type="radio"
                          name={`no_batch${element.product_id}`}
                          onClick={(e) => {
                            this.handleSWI(e, index, "sample_batch");
                          }}
                          defaultChecked={
                            element.sample.batch === 2 ? true : false
                          }
                          value="2"
                        />
                        2
                      </label>
                      <label className="radio-inline">
                        <input
                          type="radio"
                          name={`no_batch${element.product_id}`}
                          onClick={(e) => {
                            this.handleSWI(e, index, "sample_batch");
                          }}
                          defaultChecked={
                            element.sample.batch === 3 ? true : false
                          }
                          value="3"
                        />
                        3
                      </label>
                    </Col>

                    <Col xs={12} sm={4} md={4}>
                      <Field
                        type="number"
                        className="form-control"
                        placeholder="Quantity"
                        onChange={(e) => {
                          this.handleSWI(e, index, "sample_quantity");
                        }}
                        value={element.sample.quantity}
                      />
                    </Col>
                    <Col xs={12} sm={4} md={4}>
                      <Select
                        value={element.sample.unit}
                        isClearable={true}
                        onChange={(e) => {
                          this.handleSWI(e, index, "sample_unit");
                        }}
                        options={options}
                        placeholder="Select Unit"
                      />
                    </Col>
                  </>
                ) : (
                  ""
                )}
              </Row>

              <Row>
                <Col xs={12} sm={12} md={12}>
                  <div className="form-check">
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        className="form-check-input"
                        value="1"
                        defaultChecked={element.working_standards.chk}
                        onClick={(e) => {
                          this.handleSWI(e, index, "working_standards_check");
                        }}
                      />
                      <span style={{ fontWeight: "2" }}>Working Standards</span>
                    </label>
                  </div>
                </Col>

                {element.working_standards.chk ? (
                  <>
                    <Col xs={12} sm={4} md={4}>
                      <Field
                        name="workingStandardQuantity"
                        type="number"
                        step="any"
                        className="form-control"
                        placeholder="Quantity"
                        onChange={(e) => {
                          this.handleSWI(
                            e,
                            index,
                            "working_standards_quantity"
                          );
                        }}
                        value={element.working_standards.quantity}
                      />
                    </Col>
                    <Col xs={12} sm={4} md={4}>
                      <Select
                        value={element.working_standards.unit}
                        isClearable={true}
                        onChange={(e) => {
                          this.handleSWI(e, index, "working_standards_unit");
                        }}
                        options={options}
                        placeholder="Select Unit"
                      />
                    </Col>
                  </>
                ) : (
                  ""
                )}
              </Row>

              <Row>
                <Col xs={12} sm={12} md={12}>
                  <div className="form-check">
                    <label className="form-check-label">
                      <input
                        type="checkbox"
                        className="form-check-input"
                        value="1"
                        defaultChecked={element.impurities.chk}
                        onClick={(e) => {
                          this.handleSWI(e, index, "impurities_check");
                        }}
                      />
                      <span style={{ fontWeight: "2" }}>Impurities</span>
                    </label>
                  </div>
                </Col>
                {element.impurities.chk ? (
                  <>
                    <Col xs={12} sm={4} md={4}>
                      <Field
                        name="impuritiesRequired"
                        type="text"
                        className="form-control"
                        placeholder="Specify Impurity Required"
                        onChange={(e) => {
                          this.handleSWI(e, index, "specify_impurities");
                        }}
                        value={element.impurities.specify_impurities}
                      />
                    </Col>
                    <Col xs={12} sm={4} md={4}>
                      <Field
                        name="ImpuritiesQuantity"
                        type="number"
                        step="any"
                        className="form-control"
                        placeholder="Quantity"
                        onChange={(e) => {
                          this.handleSWI(e, index, "impurities_quantity");
                        }}
                        value={element.impurities.quantity}
                      />
                    </Col>
                    <Col xs={12} sm={4} md={4}>
                      <Select
                        value={element.impurities.unit}
                        isClearable={true}
                        onChange={(e) => {
                          this.handleSWI(e, index, "impurities_unit");
                        }}
                        options={options}
                        placeholder="Select Unit"
                      />
                    </Col>
                  </>
                ) : (
                  ""
                )}
              </Row>
            </div>
            <br />
          </>
        );
      }
      this.setState({ SWIhtml: htmlData });
    }
  };

  checkSingularCountry = () => {
    if([1,2,3,4,5,8,9,10,12,13,14,15,16,21,38,42,35,22].includes(this.state.request_type_id)){
      return true;
    }else{
      return false;
    }
  }

  showRequestForms = (
    errors,
    touched,
    setFieldValue,
    setFieldTouched,
    values
  ) => {
    const stability_data_type_arr = [
      { value: "ACC", label: "ACC" },
      { value: "Long Term", label: "Long Term" },
      { value: "Others", label: "Others" },
    ];

    // console.log(errors, touched);
    return (
      <>
        {this.checkForecast(this.state.request_type_id) && (
          <>
            {this.state.request_type_id === 1 ? (
              <Row>
                <Col xs={12} sm={12} md={12}>
                  <div className="form-group">
                    <label>
                      Product <span className="required-field">*</span>
                    </label>
                    <Select
                      isMulti
                      className="basic-single"
                      classNamePrefix="select"
                      options={this.state.product_list}
                      isSearchable={true}
                      isClearable={true}
                      name="product_id"
                      value={values.product_id}
                      onChange={(e) => {
                        this.selectMultiProducts(e, setFieldValue);
                      }}
                    />
                    {errors.product_id || touched.product_id ? (
                      <span className="errorMsg">{errors.product_id}</span>
                    ) : null}
                  </div>
                </Col>
              </Row>
            ) : (
              <Row>
                <Col xs={12} sm={12} md={12}>
                  <div className="form-group">
                    <label>
                      Product <span className="required-field">*</span>
                    </label>
                    <Select
                      //isMulti
                      className="basic-single"
                      classNamePrefix="select"
                      defaultValue=""
                      isClearable={true}
                      isSearchable={true}
                      name="product_id"
                      value={values.product_id}
                      options={this.state.product_list}
                      onChange={(e) => {
                        this.setProduct(e, setFieldValue);
                      }}
                    />
                    {errors.product_id || touched.product_id ? (
                      <span className="errorMsg">{errors.product_id}</span>
                    ) : null}
                  </div>
                </Col>
              </Row>
            )}

            {this.state.request_type_id === 1 && this.state.SWIhtml}

            {this.state.request_type_id !== 40 && (
              <Row>
                <Col xs={12} sm={12} md={12}>
                  <div className="form-group">
                    <label>
                      Market{" "}
                      {this.state.request_type_id === 17 ||
                      this.state.request_type_id === 18 ||
                      this.state.request_type_id === 19 ||
                      this.state.request_type_id === 20 ||
                      this.state.request_type_id === 23 ||
                      this.state.request_type_id === 41 ||
                      this.state.request_type_id === 44 ||
                      this.state.request_type_id === 28 ||
                      this.state.request_type_id === 29 ||
                      this.state.request_type_id === 31 ||
                      this.state.request_type_id === 32 ||
                      this.state.request_type_id === 33 ||
                      this.state.request_type_id === 39 ||
                      this.state.request_type_id === 42 ? (
                        <span className="required-field">*</span>
                      ) : (
                        ""
                      )}
                    </label>
                    {this.checkSingularCountry()?
                    <Select
                      className="basic-single"
                      classNamePrefix="select"
                      defaultValue=""
                      isClearable={true}
                      isSearchable={true}
                      name="country_id"
                      options={this.state.country_list}
                      onChange={(e) => {
                        this.setCountry(e, setFieldValue);
                      }}
                      onBlur={() => setFieldTouched("country_id")}
                    />
                    :
                    <Select
                      isMulti
                      className="basic-single"
                      classNamePrefix="select"
                      defaultValue=""
                      isClearable={true}
                      isSearchable={true}
                      name="country_id"
                      options={this.state.country_list}
                      onChange={(e) => {
                        this.setCountry(e, setFieldValue);
                      }}
                      onBlur={() => setFieldTouched("country_id")}
                    />}
                    {errors.country_id || touched.country_id ? (
                      <span className="errorMsg">{errors.country_id}</span>
                    ) : null}
                  </div>
                </Col>
              </Row>
            )}
          </>
        )}

        {this.checkGMPClearanceID(this.state.request_type_id) && (
          <>
            <Row>
              <Col xs={12} sm={12} md={12}>
                <div className="form-group">
                  <label>
                    GMP Clearance ID<span className="required-field">*</span>
                  </label>
                  <Field
                    name="gmp_clearance_id"
                    type="text"
                    className="form-control"
                    autoComplete="off"
                    //value={(this.state.gmp_clearance_id !== null && this.state.gmp_clearance_id !== "") ? this.state.gmp_clearance_id : ''}
                    onChange={(e) => {
                      setFieldTouched("gmp_clearance_id");
                      setFieldValue("gmp_clearance_id", e.target.value);
                    }}
                  ></Field>
                  {errors.gmp_clearance_id || touched.gmp_clearance_id ? (
                    <span className="errorMsg">{errors.gmp_clearance_id}</span>
                  ) : null}
                </div>
              </Col>
            </Row>
          </>
        )}

        {this.checkTGAemailID(this.state.request_type_id) && (
          <>
            <Row>
              <Col xs={12} sm={12} md={12}>
                <div className="form-group">
                  <label>
                    {" "}
                    Email ID<span className="required-field">*</span>
                  </label>
                  <Field
                    name="tga_email_id"
                    type="text"
                    className="form-control"
                    autoComplete="off"
                    //value={(this.state.tga_email_id !== null && this.state.tga_email_id !== "") ? this.state.tga_email_id : ''}
                    onChange={(e) => {
                      setFieldTouched("tga_email_id");
                      setFieldValue("tga_email_id", e.target.value);
                    }}
                  ></Field>
                  {errors.tga_email_id || touched.tga_email_id ? (
                    <span className="errorMsg">{errors.tga_email_id}</span>
                  ) : null}
                </div>
              </Col>
            </Row>
          </>
        )}

        {this.applicantDetails(this.state.request_type_id) && (
          <>
            <Row>
              <Col xs={12} sm={12} md={12}>
                <div className="form-group">
                  <label>
                    Applicant's Name and Address
                    <span className="required-field">*</span>
                  </label>
                  <textarea
                    className="form-control"
                    name="applicant_name"
                    onChange={(e) => {
                      setFieldTouched("applicant_name");
                      setFieldValue("applicant_name", e.target.value);
                    }}
                  ></textarea>

                  {errors.applicant_name || touched.applicant_name ? (
                    <span className="errorMsg">{errors.applicant_name}</span>
                  ) : null}
                </div>
              </Col>
            </Row>
          </>
        )}

        {this.docRequired(this.state.request_type_id) && (
          <>
            <Row>
              <Col xs={12} sm={12} md={12}>
                <div className="form-group">
                  <label>
                    Documents Required<span className="required-field">*</span>
                  </label>
                  <textarea
                    className="form-control"
                    name="doc_required"
                    onChange={(e) => {
                      setFieldTouched("doc_required");
                      setFieldValue("doc_required", e.target.value);
                    }}
                  ></textarea>

                  {errors.doc_required || touched.doc_required ? (
                    <span className="errorMsg">{errors.doc_required}</span>
                  ) : null}
                </div>
              </Col>
            </Row>
          </>
        )}

        {this.checkAposDocumentType(this.state.request_type_id) && (
          <Row>
            <Col xs={12} sm={12} md={12}>
              <div className="form-group">
                <label>Document Type</label>
                <Select
                  isMulti
                  className="basic-single"
                  classNamePrefix="select"
                  defaultValue=""
                  isClearable={true}
                  isSearchable={true}
                  name="apos_document_type"
                  options={[
                    { label: "Site GMP", value: "Site GMP" },
                    {
                      label: "Site Manufacturing License",
                      value: "Site Manufacturing License",
                    },
                    {
                      label: "Written Confirmation",
                      value: "Written Confirmation",
                    },
                  ]}
                  onChange={(e) => {
                    this.setADocType(e, setFieldValue);
                  }}
                  onBlur={() => setFieldTouched("apos_document_type")}
                />
                {errors.apos_document_type || touched.apos_document_type ? (
                  <span className="errorMsg">{errors.apos_document_type}</span>
                ) : null}
              </div>
            </Col>
          </Row>
        )}

        {this.checkNatureIssue(this.state.request_type_id) && (
          <Row>
            <Col xs={12} sm={12} md={12}>
              <div className="form-group">
                <label>Nature of Issue</label>
                <Field
                  name="nature_issue"
                  type="text"
                  className="form-control"
                  autoComplete="off"
                  value={
                    this.state.nature_issue !== null &&
                    this.state.nature_issue !== ""
                      ? this.state.nature_issue
                      : ""
                  }
                  onChange={(e) => {
                    this.setNatureIssue(e, setFieldValue);
                  }}
                ></Field>
                {errors.nature_issue && touched.nature_issue ? (
                  <span className="errorMsg">{errors.nature_issue}</span>
                ) : null}
              </div>
            </Col>
          </Row>
        )}

        {this.checkBatchNumber(this.state.request_type_id) && (
          <Row>
            <Col xs={12} sm={12} md={12}>
              <div className="form-group">
                <label>
                  Batch No <span className="required-field">*</span>
                </label>
                (ex: ABCD000000)
                <Field
                  name="batch_number"
                  type="text"
                  className="form-control"
                  autoComplete="off"
                  value={
                    this.state.batch_number !== null &&
                    this.state.batch_number !== ""
                      ? this.state.batch_number
                      : ""
                  }
                  onChange={(e) => {
                    this.setBatchNumber(e, setFieldValue);
                  }}
                ></Field>
                {errors.batch_number || touched.batch_number ? (
                  <span className="errorMsg">{errors.batch_number}</span>
                ) : null}
              </div>
            </Col>
          </Row>
        )}

        {this.checkUnitQuantity(this.state.request_type_id) && (
          <div>
            <Row>
              <Col xs={12} sm={12} md={12}>
                <div className="form-group">
                  <label>
                    Quantity{" "}
                    {this.state.request_type_id === 23 ||
                    this.state.request_type_id === 31 ||
                    this.state.request_type_id === 41 ? (
                      <span className="required-field">*</span>
                    ) : (
                      ""
                    )}
                  </label>
                  <Field
                    name="quantity"
                    type="text"
                    className="form-control"
                    autoComplete="off"
                    value={
                      this.state.quantity !== null && this.state.quantity !== ""
                        ? this.state.quantity
                        : ""
                    }
                    onChange={(e) => {
                      this.setQuantity(e, setFieldValue);
                    }}
                  ></Field>
                  {errors.quantity || touched.quantity ? (
                    <span className="errorMsg">{errors.quantity}</span>
                  ) : null}
                </div>
              </Col>
            </Row>
            <Row>
              <Col xs={12} sm={12} md={12}>
                <div className="form-group">
                  <label>
                    Units{" "}
                    {this.state.request_type_id === 23 ||
                    this.state.request_type_id === 31 ||
                    this.state.request_type_id === 41 ? (
                      <span className="required-field">*</span>
                    ) : (
                      ""
                    )}
                  </label>
                  <Field
                    name="units"
                    component="select"
                    className={`selectArowGray form-control`}
                    autoComplete="off"
                    value={
                      this.state.units !== null && this.state.units !== ""
                        ? this.state.units
                        : ""
                    }
                    onChange={(e) => {
                      this.setUnits(e, setFieldValue);
                    }}
                  >
                    <option key="-1" value="">
                      Select
                    </option>
                    <option key="1" value="Mgs">
                      Mgs
                    </option>
                    <option key="2" value="Gms">
                      Gms
                    </option>
                    <option key="3" value="Kgs">
                      Kgs
                    </option>
                    <option key="4" value="vials">
                      Vials
                    </option>
                  </Field>

                  {errors.units || touched.units ? (
                    <span className="errorMsg">{errors.units}</span>
                  ) : null}
                </div>
              </Col>
            </Row>
          </div>
        )}

        {this.checkPharmacopoeia(this.state.request_type_id) && (
          <Row>
            <Col xs={12} sm={12} md={12}>
              <div className="form-group">
                <label>Pharmacopoeia</label>
                <Field
                  name="pharmacopoeia"
                  type="text"
                  className="form-control"
                  autoComplete="off"
                  value={
                    this.state.pharmacopoeia !== null &&
                    this.state.pharmacopoeia !== ""
                      ? this.state.pharmacopoeia
                      : ""
                  }
                  onChange={(e) => {
                    this.setPharma(e, setFieldValue);
                  }}
                ></Field>
                {errors.pharmacopoeia && touched.pharmacopoeia ? (
                  <span className="errorMsg">{errors.pharmacopoeia}</span>
                ) : null}
              </div>
            </Col>
          </Row>
        )}
        {/* ======================================== */}

        {/* ================================================================= */}
        {this.checkPolymorphicForm(this.state.request_type_id) && (
          <Row>
            <Col xs={12} sm={12} md={12}>
              <div className="form-group">
                <label>Polymorphic Form</label>
                <Field
                  name="polymorphic_form"
                  type="text"
                  className="form-control"
                  autoComplete="off"
                  value={
                    this.state.polymorphic_form !== null &&
                    this.state.polymorphic_form !== ""
                      ? this.state.polymorphic_form
                      : ""
                  }
                  onChange={(e) => {
                    this.setPolymorphic(e, setFieldValue);
                  }}
                ></Field>
                {errors.polymorphic_form && touched.polymorphic_form ? (
                  <span className="errorMsg">{errors.polymorphic_form}</span>
                ) : null}
              </div>
            </Col>
          </Row>
        )}

        {this.checkStabilityDataType(this.state.request_type_id) && (
          <Row>
            <Col xs={12} sm={12} md={12}>
              <div className="form-group">
                <label>
                  Stability Data Type <span className="required-field">*</span>
                </label>
                <Select
                  className="basic-single"
                  classNamePrefix="select"
                  defaultValue=""
                  isClearable={true}
                  isSearchable={true}
                  name="stability_data_type"
                  options={stability_data_type_arr}
                  onChange={(e) => {
                    this.handleChangeSelect(e, setFieldValue);
                  }}
                  onBlur={() => setFieldTouched("stability_data_type")}
                />
                {errors.stability_data_type || touched.stability_data_type ? (
                  <span className="errorMsg">{errors.stability_data_type}</span>
                ) : null}
              </div>
            </Col>
          </Row>
        )}

        {this.checkAuditSiteName(this.state.request_type_id) && (
          <Row>
            <Col xs={12} sm={12} md={12}>
              <div className="form-group">
                <label>
                  Site Name <span className="required-field">*</span>
                </label>
                <Field
                  name="audit_visit_site_name"
                  type="text"
                  className="form-control"
                  autoComplete="off"
                  value={
                    this.state.audit_visit_site_name !== null &&
                    this.state.audit_visit_site_name !== ""
                      ? this.state.audit_visit_site_name
                      : ""
                  }
                  onChange={(e) => {
                    this.handleChangeText(e, setFieldValue);
                  }}
                ></Field>
                {errors.audit_visit_site_name ||
                touched.audit_visit_site_name ? (
                  <span className="errorMsg">
                    {errors.audit_visit_site_name}
                  </span>
                ) : null}
              </div>
            </Col>
          </Row>
        )}

        {this.checkAuditVistDate(this.state.request_type_id) && (
          <Row>
            <Col xs={12} sm={12} md={12}>
              <div className="form-group">
                <label>
                  Audit/Visit Date <span className="required-field">*</span>
                </label>
                <div className="form-control">
                  <DatePicker
                    name={"request_audit_visit_date"}
                    className="borderNone"
                    selected={
                      this.state.request_audit_visit_date
                        ? this.state.request_audit_visit_date
                        : null
                    }
                    //value={(typeof this.state.subForm.request_audit_visit_date === "undefined") ? '' : this.state.subForm.request_audit_visit_date}
                    value={
                      this.state.request_audit_visit_date !== null &&
                      this.state.request_audit_visit_date !== ""
                        ? this.state.request_audit_visit_date
                        : ""
                    }
                    //onChange={this.handleChangeDate}
                    onChange={(e) => {
                      this.handleChangeDate(e, setFieldValue);
                    }}
                    dateFormat="dd/MM/yyyy"
                    onChangeRaw={(e) => {
                      e.preventDefault();
                    }}
                  />
                </div>
                {errors.request_audit_visit_date ||
                touched.request_audit_visit_date ? (
                  <span className="errorMsg">
                    {errors.request_audit_visit_date}
                  </span>
                ) : null}
              </div>
            </Col>
          </Row>
        )}

        {this.checkDeliveryDate(this.state.request_type_id) && (
          <Row>
            <Col xs={12} sm={12} md={12}>
              <div className="form-group">
                <label>
                  {this.state.request_type_id === 23 ||
                  this.state.request_type_id === 41
                    ? `Requested date of delivery`
                    : "Requested Deadline:"}
                  {this.state.request_type_id === 23?<>{` `}<span className="required-field">*</span></>:''}  
                </label>

                <div className="form-control">
                  <DatePicker
                    name={"delivery_date"}
                    autoComplete="false"
                    className="borderNone"
                    minDate={new Date()}
                    selected={
                      this.state.delivery_date ? this.state.delivery_date : null
                    }
                    //value={(typeof this.state.subForm.delivery_date === "undefined") ? '' : this.state.subForm.delivery_date}
                    value={
                      this.state.delivery_date !== null &&
                      this.state.delivery_date !== ""
                        ? this.state.delivery_date
                        : ""
                    }
                    //onChange={this.handleChangeDate}
                    onChange={(e) => {
                      this.handleChangeDeliveryDate(e, setFieldValue);
                    }}
                    dateFormat="dd/MM/yyyy"
                    onChangeRaw={(e) => {
                      e.preventDefault();
                    }}
                  />
                </div>
                {errors.delivery_date || touched.delivery_date ? (
                  <span className="errorMsg">{errors.delivery_date}</span>
                ) : null}
              </div>
            </Col>
          </Row>
        )}

        {this.checkMaterialID(this.state.request_type_id) && (
          <Row>
            <Col xs={12} sm={12} md={12}>
              <div className="form-group" onMouseLeave={(e)=>{
                    if(this.state.material_id_auto_suggest != ''){
                      document.getElementById(`material_id`).blur();
                      this.setState({material_id_auto_suggest:''});
                    }
                  }} >
                <label>Product Code <span className="required-field">*</span></label>
                <Field
                  name="material_id"
                  id={`material_id`}
                  type="text"
                  className="form-control"
                  autoComplete="off"
                  value={
                    this.state.material_id !== null &&
                    this.state.material_id !== ""
                      ? this.state.material_id
                      : ""
                  }
                  onChange={(e) => {
                    this.setMaterialID(e, setFieldValue);
                  }}
                  onFocus={(e)=>{
                    if(this.state.material_id != '' && this.state.material_id.length > 2){
                      API.post(`/api/feed/get_sap_Product_code`,{material_id:this.state.material_id})
                      .then((res) => {
                        
                        let ret_html = res.data.data.map((val,index)=>{
                          return (<li style={{cursor:'pointer',marginTop:'6px'}} onClick={()=>this.setMaterialIDAutoSuggest(val.value,setFieldValue)} >{val.label}<hr style={{marginTop:'6px',marginBottom:'0px'}} /></li>)
                        })
                        this.setState({material_id_auto_suggest:ret_html});
                      })
                      .catch((err) => {
                        console.log(err);
                      });
                    }
                  }}

                ></Field>
                {this.state.material_id_auto_suggest != '' && <ul id={`material_id_auto_suggect`} style={{zIndex:'30',position:'absolute',backgroundColor:'#d0d0d0',height:"100px",overflowY:'auto',listStyleType:'none',width:'100%'}} >
                  {this.state.material_id_auto_suggest}
                </ul>}
                {errors.material_id || touched.material_id ? (
                  <span className="errorMsg">{errors.material_id}</span>
                ) : null}
              </div>
            </Col>
          </Row>
        )}

        {this.checkPoNumber(this.state.request_type_id) && (
            <Row>
              <Col xs={12} sm={12} md={12}>
                <div className="form-group">
                  <label>
                    PO Number 
                    <span className="required-field">*</span>
                  </label>
                  <div>
                    <Field
                      name="po_number"
                      type="text"
                      className="form-control"
                      autoComplete="off"
                    />                    
                  </div>
                  {errors.po_number || touched.po_number ? (
                    <span className="errorMsg">{errors.po_number}</span>
                  ) : null}
                </div>
              </Col>
            </Row>
        )}
        {/* {this.checkPoDeliveryDate(this.state.request_type_id) && (
            <Row>
            <Col xs={12} sm={12} md={12}>
              <div className="form-group">
                <label>PO Date:</label>
                <div className="form-control">
                  <DatePicker
                    name={"po_delivery_date"}
                    className="borderNone"
                    selected={
                      this.state.po_delivery_date
                        ? this.state.po_delivery_date
                        : null
                    }
                    //value={(typeof this.state.subForm.po_delivery_date === "undefined") ? '' : this.state.subForm.po_delivery_date}
                    value={
                      this.state.po_delivery_date !== null &&
                      this.state.po_delivery_date !== ""
                        ? this.state.po_delivery_date
                        : ""
                    }
                    //onChange={this.handleChangeDate}
                    onChange={(e) => {
                      this.handleChangePoDeliveryDate(e, setFieldValue);
                    }}
                    dateFormat="dd/MM/yyyy"
                    onChangeRaw={(e) => {
                      e.preventDefault();
                    }}
                  />
                </div>
                {errors.po_delivery_date || touched.po_delivery_date ? (
                  <span className="errorMsg">{errors.po_delivery_date}</span>
                ) : null}
              </div>
            </Col>
          </Row>
        )} */}
        {this.checkRDORC(this.state.request_type_id) && (
          <Row>
            <Col xs={12} sm={12} md={12}>
              <div className="form-group">
                <label>Requested date of response/closure:</label>
                <div className="form-control">
                  <DatePicker
                    name={"date_of_response"}
                    className="borderNone"
                    selected={
                      this.state.date_of_response
                        ? this.state.date_of_response
                        : null
                    }
                    //value={(typeof this.state.subForm.date_of_response === "undefined") ? '' : this.state.subForm.date_of_response}
                    value={
                      this.state.date_of_response !== null &&
                      this.state.date_of_response !== ""
                        ? this.state.date_of_response
                        : ""
                    }
                    //onChange={this.handleChangeDate}
                    onChange={(e) => {
                      this.handleChangeDateOfResponse(e, setFieldValue);
                    }}
                    dateFormat="dd/MM/yyyy"
                    onChangeRaw={(e) => {
                      e.preventDefault();
                    }}
                  />
                </div>
                {errors.date_of_response || touched.date_of_response ? (
                  <span className="errorMsg">{errors.date_of_response}</span>
                ) : null}
              </div>
            </Col>
          </Row>
        )}

        {this.checkDMFNumber(this.state.request_type_id) && (
          <Row>
            <Col xs={12} sm={12} md={12}>
              <div className="form-group">
                <label>
                  {this.state.request_type_id === 39 ? "DMF/CEP" : "DMF Number"}
                  {this.state.request_type_id === 39 && (
                    <span className="required-field">*</span>
                  )}
                </label>
                <Field
                  name="dmf_number"
                  type="text"
                  className="form-control"
                  autoComplete="off"
                  value={
                    this.state.dmf_number !== null &&
                    this.state.dmf_number !== ""
                      ? this.state.dmf_number
                      : ""
                  }
                  onChange={(e) => {
                    this.setDMF(e, setFieldValue);
                  }}
                ></Field>
                {errors.dmf_number || touched.dmf_number ? (
                  <span className="errorMsg">{errors.dmf_number}</span>
                ) : null}
              </div>
            </Col>
          </Row>
        )}

        {this.checkNotificationNumber(this.state.request_type_id) && (
          <Row>
            <Col xs={12} sm={12} md={12}>
              <div className="form-group">
                <label>Notification Number:</label>
                <Field
                  name="notification_number"
                  type="text"
                  className="form-control"
                  autoComplete="off"
                  value={
                    this.state.notification_number !== null &&
                    this.state.notification_number !== ""
                      ? this.state.notification_number
                      : ""
                  }
                  onChange={(e) => {
                    this.setNotificationNumber(e, setFieldValue);
                  }}
                ></Field>
                {errors.notification_number && touched.notification_number ? (
                  <span className="errorMsg">{errors.notification_number}</span>
                ) : null}
              </div>
            </Col>
          </Row>
        )}
      </>
    );
  };

  shareWithAgent = (event, setFieldValue) => {
    if (event.target.checked === true) {
      setFieldValue("share_with_agent", 1);
    } else {
      setFieldValue("share_with_agent", 0);
    }
  };

  render() {
    const newInitialValues = Object.assign(initialValues, {
      content: "",
      customer_id: this.state.customer_id,
      product_id: "",
      request_type_id: this.state.request_type_id,
      polymorphic_form: "",
      pharmacopoeia: "",
      cc_customer_id: "",
      country_id: "",
      file_name: [],
      shipping_address: "",
    });

    const validateAddFlag = Yup.object().shape({
      customer_id: Yup.string().trim().required("Please select customer"),
    });

    return (
      <>
        <Modal
          show={this.state.showCreateTasks}
          onHide={() => this.handleClose()}
          backdrop="static"
        >
          <Formik
            initialValues={newInitialValues}
            validationSchema={validateAddFlag}
            onSubmit={this.handleSubmitCreateTask}
          >
            {({
              values,
              errors,
              touched,
              isValid,
              isSubmitting,
              setFieldValue,
              setFieldTouched,
              setErrors,
            }) => {
              return (
                <Form>
                  {this.state.createTaskLoader && (
                    <div className="loderOuter">
                      <div className="loderOuter">
                        <div className="loader">
                          <img src={loaderlogo} alt="logo" />
                          <div className="loading">Loading...</div>
                        </div>
                      </div>
                    </div>
                  )}
                  <Modal.Header closeButton>
                    <Modal.Title>Create Task</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <div className="contBox taskcontBox">
                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <label>
                              Select Customer{" "}
                              <span className="required-field">*</span>
                            </label>

                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              defaultValue={values.company_id}
                              isClearable={true}
                              isSearchable={true}
                              name="company_id"
                              options={this.state.company_list}
                              onChange={(e) => {
                                this.showCustomerList(
                                  e,
                                  setFieldValue,
                                  setErrors
                                );
                              }}
                              onBlur={() => setFieldTouched("company_id")}
                            />
                            {errors.company_id && touched.company_id ? (
                              <span className="errorMsg">
                                {errors.company_id}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>

                      {this.state.showCustomerTypeHTML ? (
                        <Row>
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>
                                Select User{" "}
                                <span className="required-field">*</span>
                              </label>

                              <Select
                                className="basic-single"
                                classNamePrefix="select"
                                defaultValue={values.customer_id}
                                isClearable={true}
                                isSearchable={true}
                                name="customer_id"
                                options={this.state.customer_list}
                                onChange={(e) => {
                                  this.showRequestType(e, setFieldValue);
                                }}
                                onBlur={() => setFieldTouched("customer_id")}
                              />
                              {errors.customer_id && touched.customer_id ? (
                                <span className="errorMsg">
                                  {errors.customer_id}
                                </span>
                              ) : null}

                              {this.state.cust_status === 3 && (
                                <span className="errorMsg">
                                  Please note user account is not active but you
                                  can still raise a request
                                </span>
                              )}

                              {this.state.cust_status === 1 &&
                                this.state.cust_activated !== 1 && (
                                  <span className="errorMsg">
                                    Please note that user account is yet to activate his account
                                  </span>
                                )}
                            </div>
                          </Col>
                        </Row>
                      ) : null}

                      {this.state.showRequestTypeHTML ? (
                        <Row>
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>
                                + Raise a Service Request{" "}
                                <span className="required-field">*</span>
                              </label>

                              <Select
                                className="basic-single"
                                classNamePrefix="select"
                                defaultValue={values.request_type_id}
                                isClearable={true}
                                isSearchable={true}
                                name="request_type_id"
                                options={this.state.request_type}
                                //onChange={this.onChangeRequestType}
                                onChange={(e) => {
                                  this.onChangeRequestType(e, setFieldValue);
                                }}
                                onBlur={() =>
                                  setFieldTouched("request_type_id")
                                }
                              />

                              {errors.request_type_id &&
                              touched.request_type_id ? (
                                <span className="errorMsg">
                                  {errors.request_type_id}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                        </Row>
                      ) : null}

                      {this.state.request_type_id > 0
                        ? this.showRequestForms(
                            errors,
                            touched,
                            setFieldValue,
                            setFieldTouched,
                            values
                          )
                        : null}

                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <label>Requirements</label>
                            {/* <Field
                                    name="content"
                                    component="textarea"
                                    className={`selectArowGray form-control`}
                                    autoComplete="off"
                                    value={values.content}
                                    > 
                                    </Field> */}

                            <Editor
                              name="shipping_address"
                              className={`selectArowGray form-control`}
                              value={values.content}
                              content={
                                values.content !== null && values.content !== ""
                                  ? values.content
                                  : ""
                              }
                              init={{
                                menubar: false,
                                branding: false,
                                placeholder: "Enter Requirements",
                                plugins:
                                  "link table hr visualblocks code placeholder lists autoresize textcolor",
                                toolbar:
                                  "bold italic strikethrough superscript subscript | forecolor backcolor | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | visualblocks code | fontselect",
                                font_formats:
                                  "Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats",
                              }}
                              onEditorChange={(value) =>
                                setFieldValue("content", value)
                              }
                            />

                            {errors.content && touched.content ? (
                              <span className="errorMsg">{errors.content}</span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>

                      {this.state.request_type_id === 1 && (
                        <Row>
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>
                                Shipping Address{" "}
                                <span className="required-field">*</span>
                              </label>
                              {/* <Field
                                                name="shipping_address"
                                                component="textarea"
                                                className="form-control"
                                                autoComplete="off"
                                                value={values.shipping_address}
                                            >
                                            </Field> */}

                              <Editor
                                name="shipping_address"
                                className={`selectArowGray form-control`}
                                value={values.shipping_address}
                                content={
                                  values.shipping_address !== null &&
                                  values.shipping_address !== ""
                                    ? values.shipping_address
                                    : ""
                                }
                                init={{
                                  menubar: false,
                                  branding: false,
                                  placeholder: "Enter Shipping Address",
                                  plugins:
                                    "link table hr visualblocks code placeholder lists autoresize textcolor",
                                  toolbar:
                                    "bold italic strikethrough superscript subscript | forecolor backcolor | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | visualblocks code | fontselect",
                                  font_formats:
                                    "Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats",
                                }}
                                onEditorChange={(value) =>
                                  setFieldValue("shipping_address", value)
                                }
                              />

                              {errors.shipping_address &&
                              touched.shipping_address ? (
                                <span className="errorMsg">
                                  {errors.shipping_address}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                        </Row>
                      )}

                      {this.state.showCcCustomerTypeHTML ? (
                        <Row>
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>
                                External Email notification{` `}
                                <LinkWithTooltipText
                                  tooltip={`Select list of users within Customer’s organization who will receive email notification`}
                                  href="#"
                                  id={`tooltip-menu`}
                                  clicked={(e) => this.checkHandler(e)}
                                >
                                  <img
                                    src={exclamationImage}
                                    alt="exclamation"
                                  />
                                </LinkWithTooltipText>
                              </label>
                              <Select
                                isMulti
                                className="basic-single"
                                classNamePrefix="select"
                                defaultValue={null}
                                isClearable={true}
                                isSearchable={true}
                                name="cc_customer_id"
                                options={this.state.cc_customer_list}
                                onChange={(e) => {
                                  this.showCcCustomers(e, setFieldValue);
                                }}
                              />
                            </div>
                          </Col>
                        </Row>
                      ) : null}

                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group custom-file-upload">
                            {(this.state.request_type_id === 23 ||
                              this.state.request_type_id === 11 ||
                              this.state.request_type_id === 5 ||
                              this.state.request_type_id === 8 ||
                              this.state.request_type_id === 33 ||
                              this.state.request_type_id === 39) && (
                              <span className="required-field">*</span>
                            )}
                            {this.state.request_type_id > 0 && (
                              <Dropzone
                                onDrop={(acceptedFiles) =>
                                  this.setDropZoneFiles(
                                    acceptedFiles,
                                    setErrors,
                                    setFieldValue
                                  )
                                }
                              >
                                {({ getRootProps, getInputProps }) => (
                                  <section>
                                    <div
                                      {...getRootProps()}
                                      className="custom-file-upload-header"
                                    >
                                      <input {...getInputProps()} />
                                      <p>Upload file</p>
                                    </div>
                                    <div className="custom-file-upload-area">
                                      {this.state.filesHtml}
                                    </div>
                                  </section>
                                )}
                              </Dropzone>
                            )}
                            {errors.file_name && touched.file_name ? (
                              <span className="errorMsg errorExpandView">
                                {errors.file_name}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <input
                              type="checkbox"
                              className="form-check-input"
                              value={1}
                              defaultChecked={true}
                              onChange={(e) =>
                                this.shareWithAgent(e, setFieldValue)
                              }
                            />{" "}
                            Display this request to Agent
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </Modal.Body>
                  <Modal.Footer>
                    <button
                      onClick={this.handleClose}
                      className={`btn-line`}
                      type="button"
                    >
                      Close
                    </button>
                    <button
                      className={`btn-fill ${
                        isValid ? "btn-custom-green" : "btn-disable"
                      } m-r-10`}
                      type="submit"
                      disabled={isValid ? false : true}
                    >
                      {this.state.stopflagId > 0
                        ? isSubmitting
                          ? "Updating..."
                          : "Update"
                        : isSubmitting
                        ? "Submitting..."
                        : "Submit"}
                    </button>
                  </Modal.Footer>
                </Form>
              );
            }}
          </Formik>
        </Modal>
      </>
    );
  }
}

export default CreateMyTasksPopup;
