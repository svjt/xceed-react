import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import {
  getMyDrupalId,
  localDate,
  htmlDecode,
  inArray,
  getDashboard
} from "../../shared/helper";
import { showErrorMessageFront } from "../../shared/handle_error_front";
import exclamationImage from "../../assets/images/exclamation-icon.svg";
import Select from "react-select";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
import { Row, Col, Tooltip, OverlayTrigger } from "react-bootstrap";
import "./Dashboard.css";

import dateFormat from "dateformat";
import API from "../../shared/axios";
import { Link } from "react-router-dom";

import MyTasksTable from "../dashboard/MyTasksTable";
import AllocatedTasksTable from "../dashboard/AllocatedTasksTable";
import ClosedTasksTable from "../dashboard/ClosedTasksTable";

// import MyTeamOpenTasksTable from "./MyTeamOpenTasksTable";
// import MyTeamCloseTasksTable from "./MyTeamCloseTasksTable";
// import MyTeamAllocatedTasksTable from "./MyTeamAllocatedTasksTable";
//import MyTeamSearch from "./MyTeamSearch";

import Pagination from "react-js-pagination";

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

const setDescription = (refOBj) => (cell, row) => {
  if (row.parent_id > 0) {
    return (
      <LinkWithTooltip
        tooltip={`${row.title}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {row.title}
      </LinkWithTooltip>
    );
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${row.req_name}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {row.req_name}
      </LinkWithTooltip>
    );
  }
};

const setProductName = (refOBj) => (cell, row) => {
  if (cell === null) {
    return "";
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${cell}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {cell}
      </LinkWithTooltip>
    );
  }
};

const setCreateDate = (refObj) => (cell) => {
  var date = localDate(cell);
  return dateFormat(date, "dd/mm/yyyy");
};

const setDaysPending = (refObj) => (cell, row) => {
  var date = localDate(cell);
  return dateFormat(date, "dd/mm/yyyy");
};

const clickToShowTasks = (refObj) => (cell, row) => {
  if (row.discussion === 1) {
    return (
      <LinkWithTooltip
        tooltip={`${cell}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refObj.redirectUrlTask(e, row.task_id)}
      >
        {cell}
      </LinkWithTooltip>
    );
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${cell}`}
        href="#"
        id="tooltip-1"
        clicked={(e) =>
          refObj.redirectUrlTask(
            e,
            row.task_id,
            row.assignment_id,
            row.assigned_to
          )
        }
      >
        {cell}
      </LinkWithTooltip>
    );
  }
};

const setCompanyName = (refObj) => (cell, row) => {
  return htmlDecode(cell);
};

const setCustomerName = (refObj) => (cell, row) => {
  if (row.customer_id > 0) {
    //hard coded customer id - SATYAJIT
    //row.customer_id = 2;
    return (
      <LinkWithTooltip
        tooltip={`${row.first_name + " " + row.last_name}`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refObj.redirectUrl(e, row.customer_id)}
      >
        {row.first_name + " " + row.last_name}
      </LinkWithTooltip>
    );
  } else {
    return "";
  }
};

const setAssignedTo = (refOBj) => (cell, row) => {
  return (
    <LinkWithTooltip
      tooltip={`${
        row.at_emp_first_name +
        " " +
        row.at_emp_last_name +
        " (" +
        row.at_emp_desig_name +
        ")"
      }`}
      href="#"
      id="tooltip-1"
      clicked={(e) => refOBj.checkHandler(e)}
    >
      {row.at_emp_first_name +
        " " +
        row.at_emp_last_name +
        " (" +
        row.at_emp_desig_name +
        ")"}
    </LinkWithTooltip>
  );
};

const setAssignedBy = (refOBj) => (cell, row) => {
  if (row.assigned_by === "-1") {
    return (
      <LinkWithTooltip
        tooltip={`System`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        System
      </LinkWithTooltip>
    );
  } else {
    return (
      <LinkWithTooltip
        tooltip={`${
          row.ab_emp_first_name +
          " " +
          row.ab_emp_last_name +
          " (" +
          row.ab_emp_desig_name +
          ")"
        }`}
        href="#"
        id="tooltip-1"
        clicked={(e) => refOBj.checkHandler(e)}
      >
        {row.ab_emp_first_name +
          " " +
          row.ab_emp_last_name +
          " (" +
          row.ab_emp_desig_name +
          ")"}
      </LinkWithTooltip>
    );
  }
};

const hoverDate = (refOBj) => (cell, row) => {
  return (
    <LinkWithTooltip
      tooltip={cell}
      href="#"
      id="tooltip-1"
      clicked={(e) => refOBj.checkHandler(e)}
    >
      {cell}
    </LinkWithTooltip>
  );
};

const getBreach = (refOBj) => (cell, row) => {
  var breach = false;
  if (row.breach_reason !== null && row.breach_reason !== "") {
    breach = true;
  }

  return (
    <>
      {/* <div className="actionStyle"> */}
      <div className="btn-group">
        {breach && (
          <LinkWithTooltip
            tooltip={`${htmlDecode(row.breach_reason)}`}
            href="#"
            id={`tooltip-menu-${row.task_id}`}
            clicked={(e) => this.checkHandler(e)}
          >
            <img src={exclamationImage} alt="exclamation" />
          </LinkWithTooltip>
        )}
      </div>
      {/* </div> */}
    </>
  );
};

class graphData extends Component {
  constructor(props) {
    super(props);
    if (this.props.location && this.props.location.state != "") {
    }

    this.state = {
      task_id: 0,
      tableData: [],
      url_params: [],
      activePage: 1,
      totalCount: 0,
      itemPerPage: 20,
      showDeleteTaskPopup: false,
      node_url: "",
      selectedProducts: [],
      selectedCustomers: [],
      selectedFunctions: [],
      selectedUser: [],
      selectedRequests: [],
      selectedEmployee: [],
      selectedTaskRef: "",
      showCustomer: true,
      showRequest: true,
      show_deleted_tasks: false,
      show_dummy_users: false,
      show_unread_task: false,
      is_loading: true,
      countTasks: [],
      finished_api: false,
      stop_api_call: false,
      sel_sla_task_arr: [],
      sla_task_arr: [
        { value: "1", label: "Normal SLA" },
        { value: "2", label: "SLA Increased" },
        { value: "3", label: "SLA Paused" },
      ],
      sla_arr: [
        { value: 1, label: "Due Today" },
        { value: 3, label: "Due Tomorrow" },
        { value: 4, label: " Due This Week" },
        { value: 2, label: "Overdue" },
      ],
      tabsClicked: this.props.location.state.selectedTab
        ? `tab_${this.props.location.state.selectedTab}`
        : "tab_1",
      activeTab:
        this.props.location.state.selectedTab !== undefined &&
        this.props.location.state.selectedTab !== null
          ? this.props.location.state.selectedTab
          : 1,
    };
  }

  checkHandler = (event) => {
    event.preventDefault();
  };

  redirectUrl = (event, id) => {
    event.preventDefault();
    API.post(`/api/employees/shlogin`, { customer_id: parseInt(id) })
      .then((res) => {
        if (res.data.shadowToken) {
          var url =
            process.env.REACT_APP_CUSTOMER_PORTAL +
            `setToken/` +
            res.data.shadowToken;
          window.open(url, "_blank");
        }
      })
      .catch((error) => {
        showErrorMessageFront(error, 0, this.props);
      });
  };

  redirectUrlTask = (event, task_id, assignment_id = "", posted_for = "") => {
    event.preventDefault();
    if (assignment_id === "" && posted_for === "") {
      window.open(`/user/team_discussion_details/${task_id}`, "_blank");
    } else {
      window.open(
        `/user/team_task_details/${task_id}/${assignment_id}/${posted_for}`,
        "_blank"
      );
    }
  };

  filterSearch = (event, nextEV) => {
    this.setState({
      search_loader: true,
    });
    var currIndex = null;
    if (event == null) {
      for (let index = 0; index < this.state.url_params.length; index++) {
        const element = this.state.url_params[index];

        if (element.name === nextEV.name) {
          currIndex = index;
        }
      }
      if (currIndex !== null) {
        this.state.url_params.splice(currIndex, 1);
      }
    } else {
      for (let index = 0; index < this.state.url_params.length; index++) {
        const element = this.state.url_params[index];
        if (element.name === nextEV.name) {
          currIndex = index;
        }
      }

      if (currIndex == null) {
        this.state.url_params.push({ name: nextEV.name, val: event.value });
      } else {
        const myCurrIndex = this.state.url_params[currIndex];
        myCurrIndex.val = event.value;
      }
    }

    this.callApi();
  };

  filterWords = (event) => {
    this.setState({
      search_loader: true,
    });
    var currIndex = null;
    var value_ref = event.target.value.trim().toUpperCase();
    this.setState({ selectedTaskRef: value_ref });
    if (value_ref.length == 0) {
      for (let index = 0; index < this.state.url_params.length; index++) {
        const element = this.state.url_params[index];
        if (element.name === "task_ref") {
          currIndex = index;
        }
      }

      if (currIndex !== null) {
        this.state.url_params.splice(currIndex, 1);
      }
    } else {
      for (let index = 0; index < this.state.url_params.length; index++) {
        const element = this.state.url_params[index];
        if (element.name === "task_ref") {
          currIndex = index;
        }
      }

      if (currIndex == null) {
        this.state.url_params.push({ name: "task_ref", val: value_ref });
      } else {
        const myCurrIndex = this.state.url_params[currIndex];
        myCurrIndex.val = value_ref;
      }
    }

    this.callApi();
  };

  filterSearchMulti = (event, nextEV) => {
    this.setState({
      search_loader: true,
    });
    var currIndex = null;
    if (event.length == 0) {
      for (let index = 0; index < this.state.url_params.length; index++) {
        const element = this.state.url_params[index];
        if (element.name === nextEV.name) {
          currIndex = index;
        }
      }
      if (currIndex !== null) {
        this.state.url_params.splice(currIndex, 1);
      }
      this.companyCustomers(nextEV.name, "");
    } else {
      for (let index = 0; index < this.state.url_params.length; index++) {
        const element = this.state.url_params[index];
        if (element.name === nextEV.name) {
          currIndex = index;
        }
      }

      var multi_val = this.getMultiValueString(event);

      if (currIndex == null) {
        this.state.url_params.push({ name: nextEV.name, val: multi_val });
      } else {
        const myCurrIndex = this.state.url_params[currIndex];
        myCurrIndex.val = multi_val;
      }
      this.companyCustomers(nextEV.name, multi_val);
    }

    this.callApi();
  };

  filterSearchMultiSLA = (event, nextEV) => {
    this.setState({
      search_loader: true,
    });
    let currIndex;

    for (let index = 0; index < this.state.url_params.length; index++) {
      const element = this.state.url_params[index];
      if (element.name === "sla_arr") {
        currIndex = index;
        break;
      }
    }

    this.state.url_params.splice(currIndex, 1);

    let normal = 0;
    let increased = 0;
    let paused = 0;
    for (let index = 0; index < event.length; index++) {
      const element = event[index];

      if (element.value === "1") {
        normal = 1;
      }

      if (element.value === "2") {
        increased = 1;
      }

      if (element.value === "3") {
        paused = 1;
      }
    }
    this.state.url_params.push({
      name: "sla_arr",
      val: `${normal},${increased},${paused}`,
    });

    this.callApi();
  };

  getMultiValueString = (selectedValues) => {
    var string = "";
    for (let index = 0; index < selectedValues.length; index++) {
      const element = selectedValues[index];
      string += `${element.value},`;
    }
    string = string.replace(/,+$/, "");
    return string;
  };

  companyCustomers = (name, val) => {
    if (name === "compid") {
      if (val === "") {
        var currIndex;
        for (let index = 0; index < this.state.url_params.length; index++) {
          const element = this.state.url_params[index];
          if (element.name === "cid") {
            currIndex = index;
            break;
          }
        }
        if (currIndex >= 0) {
          this.state.url_params.splice(currIndex, 1);
        }
        this.setState({ showCustomerBlock: false, my_customer_list: [] });
      } else {
        API.get(`/api/team/my_team_customers?compid=${val}`)
          .then((res) => {
            var myCustomer = [];
            for (let index = 0; index < res.data.data.length; index++) {
              const element = res.data.data[index];
              var keyCustomer;
              if (element.vip_customer === 1) {
                keyCustomer = "*";
              } else {
                keyCustomer = "";
              }
              myCustomer.push({
                value: element["customer_id"],
                label:
                  element["first_name"] +
                  " " +
                  element["last_name"] +
                  " " +
                  keyCustomer,
              });
            }

            this.setState({
              showCustomerBlock: true,
              my_customer_list: myCustomer,
            });
          })
          .catch((err) => {
            console.log(err);
          });
      }
    }
  };

  filterDateFrom = (value) => {
    this.setState({ date_from: value, search_loader: true });
    var currIndex = null;
    for (let index = 0; index < this.state.url_params.length; index++) {
      const element = this.state.url_params[index];
      if (element.name === "date_from") {
        currIndex = index;
      }
    }

    if (currIndex == null) {
      this.state.url_params.push({
        name: "date_from",
        val: dateFormat(value, "yyyy-mm-dd"),
      });
    } else {
      const myCurrIndex = this.state.url_params[currIndex];
      myCurrIndex.val = dateFormat(value, "yyyy-mm-dd");
    }

    this.callApi();
  };

  filterDateTo = (value) => {
    this.setState({ date_to: value, search_loader: true });
    var currIndex = null;
    for (let index = 0; index < this.state.url_params.length; index++) {
      const element = this.state.url_params[index];
      if (element.name === "date_to") {
        currIndex = index;
      }
    }

    if (currIndex == null) {
      this.state.url_params.push({
        name: "date_to",
        val: dateFormat(value, "yyyy-mm-dd"),
      });
    } else {
      const myCurrIndex = this.state.url_params[currIndex];
      myCurrIndex.val = dateFormat(value, "yyyy-mm-dd");
    }

    this.callApi();
  };

  removeDateTo = () => {
    this.setState({ date_to: null, search_loader: true });
    var currIndex = null;
    for (let index = 0; index < this.state.url_params.length; index++) {
      const element = this.state.url_params[index];

      if (element.name === "date_to") {
        currIndex = index;
      }
    }
    if (currIndex !== null) {
      this.state.url_params.splice(currIndex, 1);
    }

    this.callApi();
  };

  removeDateFrom = () => {
    this.setState({ date_from: null, search_loader: true });
    var currIndex = null;
    for (let index = 0; index < this.state.url_params.length; index++) {
      const element = this.state.url_params[index];

      if (element.name === "date_from") {
        currIndex = index;
      }
    }
    if (currIndex !== null) {
      this.state.url_params.splice(currIndex, 1);
    }

    this.callApi();
  };

  showDeletedTasks = (event) => {
    let show_del_task = event.target.checked;
    this.setState({ show_deleted_tasks: show_del_task, search_loader: true });
    var currIndex = null;

    for (let index = 0; index < this.state.url_params.length; index++) {
      const element = this.state.url_params[index];
      if (element.name === "show_deleted") {
        currIndex = index;
      }
    }
    let param_del_task;
    if (show_del_task) {
      param_del_task = 1;
    } else {
      param_del_task = 0;
    }

    if (currIndex == null) {
      this.state.url_params.push({ name: "show_deleted", val: param_del_task });
    } else {
      const myCurrIndex = this.state.url_params[currIndex];
      myCurrIndex.val = param_del_task;
    }

    this.callApi();
  };

  showDummyTasks = (event) => {
    let show_del_task = event.target.checked;
    this.setState({ show_dummy_users: show_del_task, search_loader: true });
    var currIndex = null;

    for (let index = 0; index < this.state.url_params.length; index++) {
      const element = this.state.url_params[index];
      if (element.name === "show_dummy") {
        currIndex = index;
      }
    }
    let param_del_task;
    if (show_del_task) {
      param_del_task = 1;
    } else {
      param_del_task = 0;
    }

    if (currIndex == null) {
      this.state.url_params.push({ name: "show_dummy", val: param_del_task });
    } else {
      const myCurrIndex = this.state.url_params[currIndex];
      myCurrIndex.val = param_del_task;
    }

    this.callApi();
  };

  displayUnreadTask = (event) => {
    this.setState({
      show_unread_task: event.target.checked,
      search_loader: true,
    });
    var currIndex = null;

    for (let index = 0; index < this.state.url_params.length; index++) {
      const element = this.state.url_params[index];
      if (element.name === "show_unread") {
        currIndex = index;
      }
    }
    let param_del_task;
    if (event.target.checked) {
      param_del_task = 1;
    } else {
      param_del_task = 0;
    }

    if (currIndex == null) {
      this.state.url_params.push({ name: "show_unread", val: param_del_task });
    } else {
      const myCurrIndex = this.state.url_params[currIndex];
      myCurrIndex.val = param_del_task;
    }

    this.callApi();
  };

  callApi = () => {
    if (this.state.url_params.length > 0) {
      var query_string = "";
      for (let index = 0; index < this.state.url_params.length; index++) {
        const element = this.state.url_params[index];
        query_string += `${element.name}=${element.val}&`;
      }
      query_string = query_string.substring(0, query_string.length - 1);

      this.getMyGraphTasks(query_string);
    } else {
      this.getMyGraphTasks();
    }
  };

  downloadXLSX = (e) => {
    e.preventDefault();
    if (this.state.url_params.length > 0) {
      var query_string = "";
      for (let index = 0; index < this.state.url_params.length; index++) {
        const element = this.state.url_params[index];
        query_string += `${element.name}=${element.val}&`;
      }
      query_string = query_string.substring(0, query_string.length - 1);

      if (this.state.tabsClicked === "tab_1") {
        query_string += `&excel_for=1`;
      } else if (this.state.tabsClicked === "tab_2") {
        query_string += `&excel_for=2`;
      } else if (this.state.tabsClicked === "tab_3") {
        query_string += `&excel_for=3`;
      }

      this.getMyGraphTasks(query_string, true);
    } else {
      this.getMyGraphTasks("", true);
    }
  };

  handleTabs = (event) => {
    if (event.currentTarget.className === "active") {
      //DO NOTHING
    } else {
      var elems = document.querySelectorAll('[id^="tab_"]');
      // console.log("elems", elems);

      var elemsContainer = document.querySelectorAll('[id^="show_tab_"]');
      // console.log("elemsContainer", elemsContainer);

      var currId = event.currentTarget.id;
      // console.log("currId", currId);

      for (var i = 0; i < elems.length; i++) {
        elems[i].classList.remove("active");
      }

      for (var j = 0; j < elemsContainer.length; j++) {
        elemsContainer[j].style.display = "none";
      }

      // console.log("event.currentTarget", event.currentTarget);

      event.currentTarget.classList.add("active");
      event.currentTarget.classList.add("active");
      document.querySelector("#show_" + currId).style.display = "block";
      this.setState({ tabsClicked: currId }, () => {
        console.log("tabsClicked", this.state.tabsClicked);
      });
    }
    // this.tabElem.addEventListener("click",function(event){
    //     alert(event.target);
    // }, false);
  };

  graphDetailsPagination = () => {
    this.setState({ tableData: [] });
    if (this.state.node_url != "") {
      API.get(`${this.state.node_url}`)
        .then((res) => {
          this.setState({
            tableData: res.data.data,
            countTasks: res.data.count,
            finished_api: true,
            paginationOptions: {
              clearSearch: true,
              expandBy: "column",
              page: !this.state.start_page ? 1 : this.state.start_page,
              hideSizePerPage: true,
              sizePerPageList: [
                {
                  text: "10",
                  value: 10,
                },
                {
                  text: "20",
                  value: 20,
                },
                {
                  text: "All",
                  value: !res.data.count ? 1 : res.data.count,
                },
              ],
              sizePerPage: 20,
              pageStartIndex: 1,
              paginationSize: 3,
              prePage: "‹",
              nextPage: "›",
              firstPage: "«",
              lastPage: "»",
              paginationPosition: "bottom",
            },
          });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  getMyGraphTasks = (
    manual_search = "",
    excel_download = false,
    pageNumber = 0
  ) => {
    this.setState({ search_loader: true });
    let url_id = this.props.match.params.url_id;
    let search_criteria = "";
    if (
      this.props.location &&
      this.props.location.state &&
      this.props.location.state.urlStr != ""
    ) {
      search_criteria = this.props.location.state.urlStr;
    }
    let bar_graph = this.props.match.params.bar_graph;
    if (manual_search === "") {
      if (search_criteria !== "") {
        search_criteria.split("&").forEach((element) => {
          let sub_arr = element.split("=");
          this.state.url_params.push({ name: sub_arr[0], val: sub_arr[1] });
          if (sub_arr[0] === "task_ref") {
            this.setState({ selectedTaskRef: sub_arr[1] });
          }

          if (sub_arr[0] === "date_from") {
            this.setState({ date_from: new Date(sub_arr[1]) });
          }

          if (sub_arr[0] === "date_to") {
            this.setState({ date_to: new Date(sub_arr[1]) });
          }

          if (sub_arr[0] === "show_dummy" && sub_arr[1] == 1) {
            this.setState({ show_dummy_users: true });
          }

          if (sub_arr[0] === "show_deleted" && sub_arr[1] == 1) {
            this.setState({ show_deleted_tasks: true });
          }

          if (sub_arr[0] === "show_unread" && sub_arr[1] == 1) {
            this.setState({ show_unread_task: true });
          }

          if (sub_arr[0] === "sla_arr") {
            let sla_array = sub_arr[1].split(",");
            let new_arr = [];

            if (sla_array[0] === "1") {
              new_arr.push({ value: "1", label: "Normal SLA" });
            }

            if (sla_array[1] === "1") {
              new_arr.push({ value: "2", label: "SLA Increased" });
            }

            if (sla_array[2] === "1") {
              new_arr.push({ value: "3", label: "SLA Paused" });
            }
            this.setState({ sel_sla_task_arr: new_arr });
          }

          if (sub_arr[0] === "sla") {
            let new_arr = [];

            if (sub_arr[1] === "1") {
              new_arr.push({ value: 1, label: "Due Today" });
            }

            if (sub_arr[1] === "2") {
              new_arr.push({ value: 2, label: "Overdue" });
            }

            if (sub_arr[1] === "3") {
              new_arr.push({ value: 3, label: "Due Tomorrow" });
            }

            if (sub_arr[1] === "4") {
              new_arr.push({ value: 4, label: " Due This Week" });
            }

            this.setState({ sel_sla_arr: new_arr });
          }
        });
      }

      // if(search_criteria === 'q=no'){
      //   search_criteria = ``;
      // }else{
      //   search_criteria = `?${search_criteria}`;
      // }
      search_criteria = `?${search_criteria}`;
    } else {
      search_criteria = `?${manual_search}`;
    }

    if (excel_download) {
      if (search_criteria === "q=no") {
        search_criteria = `?excel=1`;
      } else {
        search_criteria = `${search_criteria}&excel=1`;
      }
    }

    let reg = /^\d+$/;
    let url = "";
    if (reg.test(url_id)) {
      switch (url_id) {
        case "1":
          url = `/api/reports/my_analytics_open_tasks/sla_eighty${search_criteria}`;
          break;

        case "2":
          url = `/api/reports/my_analytics_open_tasks/sla_breached${search_criteria}`;
          break;

        case "3":
          url = `/api/reports/my_analytics_open_tasks/within_sla${search_criteria}`;
          break;

        case "4":
          url = `/api/reports/my_analytics_allocated_tasks/sla_eighty${search_criteria}`;
          break;

        case "5":
          url = `/api/reports/my_analytics_allocated_tasks/sla_breached${search_criteria}`;
          break;

        case "6":
          url = `/api/reports/my_analytics_allocated_tasks/within_sla${search_criteria}`;
          break;

        case "7":
          url = `/api/reports/my_analytics_closed_tasks/sla_met${search_criteria}`;
          break;

        case "8":
          url = `/api/reports/my_analytics_closed_tasks/sla_breached${search_criteria}`;
          break;

        case "9":
          url = `/api/reports/my_analytics_task_overdue/by_days${search_criteria}`;
          break;

        case "10":
          url = `/api/reports/my_analytics_task_overdue/by_weeks${search_criteria}`;
          break;

        case "11":
          url = `/api/reports/my_analytics_task_overdue/by_months${search_criteria}`;
          break;

        case "12":
          url = `/api/reports/my_analytics_met_sla/one_day${search_criteria}`;
          break;

        case "13":
          url = `/api/reports/my_analytics_met_sla/two_days${search_criteria}`;
          break;

        case "14":
          url = `/api/reports/my_analytics_met_sla/three_days${search_criteria}`;
          break;

        case "15":
          this.setState({ showRequest: true, showCustomer: false });
          if (search_criteria === "") {
            bar_graph = `?bs=${bar_graph}`;
          } else {
            bar_graph = `&bs=${bar_graph}`;
          }
          url = `/api/reports/my_analytics/company_task_distribution${search_criteria}${bar_graph}`;
          break;

        case "16":
          this.setState({ showRequest: false, showCustomer: true });
          if (search_criteria === "") {
            bar_graph = `?bs=${bar_graph}`;
          } else {
            bar_graph = `&bs=${bar_graph}`;
          }
          url = `/api/reports/my_analytics/request_task_distribution${search_criteria}${bar_graph}`;
          break;

        case "17":
          url = `/api/reports/my_analytics_open_tasks/sla_breached_reason${search_criteria}`;
          break;

        case "18":
          url = `/api/reports/my_analytics_allocated_tasks/sla_breached_reason${search_criteria}`;
          break;

        case "19":
          url = `/api/reports/my_analytics_closed_tasks/sla_breached_reason${search_criteria}`;
          break;

        default:
          break;
      }

      if (url != "") {
        let options = {};
        if (excel_download) {
          options = { responseType: "blob" };
        }

        API.get(`${url}`, options)
          .then((res) => {
            if (res.data && res.data.data && res.data.count) {
              this.setState({ tableData: [], countTasks: 0 });
              this.setState({
                tableData: res.data.data,
                countTasks: res.data.count,
                search_loader: false,
                finished_api: true,
                paginationOptions: {
                  clearSearch: true,
                  expandBy: "column",
                  page: !this.state.start_page ? 1 : this.state.start_page,
                  sizePerPageList: [
                    {
                      text: "10",
                      value: 10,
                    },
                    {
                      text: "20",
                      value: 20,
                    },
                    {
                      text: "All",
                      value: !res.data.count ? 1 : res.data.count,
                    },
                  ],
                  sizePerPage: 20,
                  pageStartIndex: 1,
                  paginationSize: 3,
                  prePage: "‹",
                  nextPage: "›",
                  firstPage: "«",
                  lastPage: "»",
                  hideSizePerPage: true,
                  paginationPosition: "bottom",
                },
                node_url: url,
              });
            } else {
              this.setState({ search_loader: false, finished_api: true });
              // console.log("here xlsx");
              let file_name = "";
              switch (url_id) {
                case "1":
                  file_name = `open_tasks_sla_eighty.xlsx`;
                  break;

                case "2":
                  file_name = `open_tasks_sla_breached.xlsx`;
                  break;

                case "3":
                  file_name = `open_tasks_within_sla.xlsx`;
                  break;

                case "4":
                  file_name = `allocated_tasks_sla_eighty.xlsx`;
                  break;

                case "5":
                  file_name = `allocated_tasks_sla_breached.xlsx`;
                  break;

                case "6":
                  file_name = `allocated_tasks_within_sla.xlsx`;
                  break;

                case "7":
                  file_name = `closed_tasks_sla_met.xlsx`;
                  break;

                case "8":
                  file_name = `closed_tasks_sla_breached.xlsx`;
                  break;

                case "9":
                  file_name = `task_overdue_by_days.xlsx`;
                  break;

                case "10":
                  file_name = `task_overdue_by_weeks.xlsx`;
                  break;

                case "11":
                  file_name = `task_overdue_by_months.xlsx`;
                  break;

                case "12":
                  file_name = `met_sla_one_day.xlsx`;
                  break;

                case "13":
                  file_name = `met_sla_two_days.xlsx`;
                  break;

                case "14":
                  file_name = `met_sla_three_days.xlsx`;
                  break;

                case "15":
                  file_name = `employee_distribution.xlsx`;
                  break;

                case "16":
                  file_name = `request_type_distribution.xlsx`;
                  break;

                case "17":
                  file_name = `open_tasks_sla_breached_reason.xlsx`;
                  break;

                case "18":
                  file_name = `allocated_tasks_sla_breached_reason.xlsx`;
                  break;

                case "19":
                  file_name = `closed_tasks_sla_breached_reason.xlsx`;
                  break;

                default:
                  break;
              }

              let uri = window.URL.createObjectURL(res.data);
              let a = document.createElement("a");
              a.href = uri;
              a.download = file_name;
              a.click();
            }
          })
          .catch((err) => {
            console.log(err);
          });
      }
    } else {
    }
  };

  componentDidMount() {
    this.getMyGraphTasks();
    API.get(`/api/team/my_team_members`)
      .then((res) => {
        var myEmployee = [];
        var myEmpIdArr = [];
        let currIndex = null;
        for (let index = 0; index < this.state.url_params.length; index++) {
          const element = this.state.url_params[index];
          if (element.name === "eid") {
            currIndex = index;
          }
        }
        let selArr = [];
        if (currIndex == null) {
        } else {
          const myCurrIndex = this.state.url_params[currIndex];
          selArr = myCurrIndex.val.split(",");
        }
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];

          if (currIndex != null && inArray(element["employee_id"], selArr)) {
            this.state.selectedEmployee.push({
              value: element["employee_id"],
              label: element["first_name"] + " " + element["last_name"],
            });
          }

          myEmployee.push({
            value: element["employee_id"],
            label: element["first_name"] + " " + element["last_name"],
          });

          myEmpIdArr.push(element["employee_id"]);
        }

        this.setState({ my_employee_list: myEmployee, my_emp_arr: myEmpIdArr });

        API.get(`/api/feed/request_type`)
          .then((res) => {
            var requestTypes = [];
            let currIndex = null;
            for (let index = 0; index < this.state.url_params.length; index++) {
              const element = this.state.url_params[index];
              if (element.name === "rid") {
                currIndex = index;
              }
            }
            let selArr = [];
            if (currIndex == null) {
            } else {
              const myCurrIndex = this.state.url_params[currIndex];
              selArr = myCurrIndex.val.split(",");
            }
            for (let index = 0; index < res.data.data.length; index++) {
              const element = res.data.data[index];
              if (currIndex != null && inArray(element["type_id"], selArr)) {
                this.state.selectedRequests.push({
                  value: element["type_id"],
                  label: element["req_name"],
                });
              }

              requestTypes.push({
                value: element["type_id"],
                label: element["req_name"],
              });
            }

            this.setState({ request_type: requestTypes });

            API.get(`/api/feed/designations`)
              .then((res) => {
                var desigTypes = [];
                let currIndex = null;
                for (
                  let index = 0;
                  index < this.state.url_params.length;
                  index++
                ) {
                  const element = this.state.url_params[index];
                  if (element.name === "desig") {
                    currIndex = index;
                  }
                }
                let selArr = [];
                if (currIndex == null) {
                } else {
                  const myCurrIndex = this.state.url_params[currIndex];
                  selArr = myCurrIndex.val.split(",");
                }
                for (let index = 0; index < res.data.data.length; index++) {
                  const element = res.data.data[index];

                  if (
                    currIndex != null &&
                    inArray(element["desig_id"], selArr)
                  ) {
                    this.state.selectedFunctions.push({
                      value: element["desig_id"],
                      label: element["desig_name"],
                    });
                  }

                  desigTypes.push({
                    value: element["desig_id"],
                    label: element["desig_name"],
                  });
                }
                this.setState({ designations: desigTypes });

                API.get("/api/team/l1_companies")
                  .then((res) => {
                    var myCompany = [];
                    let currIndex = null;
                    for (
                      let index = 0;
                      index < this.state.url_params.length;
                      index++
                    ) {
                      const element = this.state.url_params[index];
                      if (element.name === "compid") {
                        currIndex = index;
                      }
                    }
                    let selArr = [];
                    if (currIndex == null) {
                    } else {
                      const myCurrIndex = this.state.url_params[currIndex];
                      selArr = myCurrIndex.val.split(",");
                    }
                    for (let index = 0; index < res.data.data.length; index++) {
                      const element = res.data.data[index];

                      if (
                        currIndex != null &&
                        inArray(element["company_id"], selArr)
                      ) {
                        this.state.selectedCustomers.push({
                          value: element["company_id"],
                          label: element["company_name"],
                        });
                      }

                      myCompany.push({
                        value: element["company_id"],
                        label: htmlDecode(element["company_name"]),
                      });
                    }

                    this.setState({ company: myCompany });

                    if (selArr && selArr.length > 0) {
                      API.get(
                        `/api/team/my_team_customers?compid=${selArr.join(",")}`
                      )
                        .then((res) => {
                          var myCustomer = [];
                          let currIndex = null;
                          for (
                            let index = 0;
                            index < this.state.url_params.length;
                            index++
                          ) {
                            const element = this.state.url_params[index];
                            if (element.name === "cid") {
                              currIndex = index;
                            }
                          }
                          let selArr = [];
                          if (currIndex == null) {
                          } else {
                            const myCurrIndex = this.state.url_params[
                              currIndex
                            ];
                            selArr = myCurrIndex.val.split(",");
                          }
                          for (
                            let index = 0;
                            index < res.data.data.length;
                            index++
                          ) {
                            const element = res.data.data[index];
                            var keyCustomer;
                            if (element.vip_customer === 1) {
                              keyCustomer = "*";
                            } else {
                              keyCustomer = "";
                            }

                            if (
                              currIndex != null &&
                              inArray(element["customer_id"], selArr)
                            ) {
                              this.state.selectedUser.push({
                                value: element["customer_id"],
                                label:
                                  element["first_name"] +
                                  " " +
                                  element["last_name"] +
                                  " " +
                                  keyCustomer,
                              });
                            }

                            myCustomer.push({
                              value: element["customer_id"],
                              label:
                                element["first_name"] +
                                " " +
                                element["last_name"] +
                                " " +
                                keyCustomer,
                            });
                          }

                          this.setState({
                            showCustomerBlock: true,
                            my_customer_list: myCustomer,
                          });
                        })
                        .catch((err) => {
                          console.log(err);
                        });
                    }

                    API.get(`/api/feed/products`)
                      .then((res) => {
                        var productsList = [];
                        let currIndex = null;
                        for (
                          let index = 0;
                          index < this.state.url_params.length;
                          index++
                        ) {
                          const element = this.state.url_params[index];
                          if (element.name === "prodid") {
                            currIndex = index;
                          }
                        }
                        let selArr = [];
                        if (currIndex == null) {
                        } else {
                          const myCurrIndex = this.state.url_params[currIndex];
                          selArr = myCurrIndex.val.split(",");
                        }

                        for (
                          let index = 0;
                          index < res.data.data.length;
                          index++
                        ) {
                          const element = res.data.data[index];

                          if (
                            currIndex != null &&
                            inArray(element["product_id"], selArr)
                          ) {
                            this.state.selectedProducts.push({
                              value: element["product_id"],
                              label: element["product_name"],
                            });
                          }

                          productsList.push({
                            value: element["product_id"],
                            label: element["product_name"],
                          });
                        }

                        this.setState({
                          product_list: productsList,
                          is_loading: false,
                          stop_api_call: true,
                        });
                      })
                      .catch((err) => {
                        console.log(err);
                      });
                  })
                  .catch((err) => {
                    console.log(err);
                  });
              })
              .catch((err) => {
                console.log(err);
              });
          })
          .catch((err) => {
            console.log(err);
          });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  tdClassName = (fieldValue, row) => {
    //var dynamicClass = "width-150 ";
    var dynamicClass = "";
    if (row.vip_customer === 1) {
      dynamicClass += "bookmarked-column ";
    }
    return dynamicClass;
  };

  trClassName = (row, rowIndex) => {
    var ret = " ";
    var selDueDate = row.assignemnt_due_date;
    var dueDate = localDate(selDueDate);
    var today = localDate(row.today_date);
    var timeDiff = dueDate.getTime() - today.getTime();

    if (timeDiff > 0) {
    } else {
      ret += "tr-red";
    }

    return ret;
  };

  render() {
    return (
      <>
        {this.state.search_loader === true && (
          <>
            <div className="loderOuter">
              <div className="loader">
                <img src={loaderlogo} alt="logo" />
                <div className="loading">Loading...</div>
              </div>
            </div>
          </>
        )}
        {this.state.finished_api === true && (
          <div className="content-wrapper">
            <section className="content-header heading-side-by-side">
              <h1>My Tasks</h1>
            </section>
            <section className="content">
              <div className="clearfix serchapanel teamSearch" id="team-search">
                <div className="mb-15">
                  <label className="customCheckBox">
                    Show Cancelled Tasks
                    <input
                      type="checkbox"
                      defaultChecked={this.state.show_deleted_tasks}
                      onChange={(e) => this.showDeletedTasks(e)}
                    />
                    <span className="checkmark-check"></span>
                  </label>

                  <label className="customCheckBox">
                    Display Inactive Users
                    <input
                      type="checkbox"
                      defaultChecked={this.state.show_dummy_users}
                      onChange={(e) => this.showDummyTasks(e)}
                    />
                    <span className="checkmark-check"></span>
                  </label>
                  {` `}
                  <LinkWithTooltip
                    tooltip={`List of users who has not yet activated account.`}
                    href="#"
                    id="tooltip-1"
                    clicked={(e) => this.checkHandler(e)}
                  >
                    <i
                      className="fa fa-exclamation-circle"
                      aria-hidden="true"
                    ></i>
                  </LinkWithTooltip>

                  <label className="customCheckBox">
                    Display Unread Tasks
                    <input
                      type="checkbox"
                      defaultChecked={this.state.show_unread_task}
                      onChange={(e) => this.displayUnreadTask(e)}
                    />
                    <span className="checkmark-check"></span>
                  </label>
                </div>

                <ul>
                  <li>
                    <div className="form-group">
                      <label>Ref No</label>
                      <input
                        type="text"
                        name="ref_no"
                        className="form-control"
                        value={this.state.selectedTaskRef}
                        onChange={(e) => this.filterWords(e)}
                        style={{ textTransform: "uppercase" }}
                      />
                    </div>
                  </li>

                  {this.state.request_type && this.state.showRequest === true && (
                    <li>
                      <div className="form-group  has-feedback">
                        <label>Request Type</label>
                        <Select
                          isMulti
                          className="basic-single"
                          classNamePrefix="select"
                          defaultValue={this.state.selectedRequests}
                          isClearable={true}
                          isSearchable={true}
                          name="rid"
                          options={this.state.request_type}
                          onChange={this.filterSearchMulti}
                        />
                      </div>
                    </li>
                  )}

                  {this.state.product_list && (
                    <li>
                      <div className="form-group  has-feedback">
                        <label>Products</label>
                        <Select
                          isMulti
                          className="basic-single"
                          classNamePrefix="select"
                          defaultValue={this.state.selectedProducts}
                          isClearable={true}
                          isSearchable={true}
                          name="prodid"
                          options={this.state.product_list}
                          onChange={this.filterSearchMulti}
                        />
                      </div>
                    </li>
                  )}

                  {this.state.company  &&
                    this.state.showCustomer === true && (
                    <li>
                      <div className="form-group  has-feedback">
                        <label>Customer</label>
                        <Select
                          isMulti
                          className="basic-single"
                          classNamePrefix="select"
                          defaultValue={this.state.selectedCustomers}
                          isClearable={true}
                          isSearchable={true}
                          name="compid"
                          options={this.state.company}
                          onChange={this.filterSearchMulti}
                        />
                      </div>
                    </li>
                  )}

                  {this.state.showCustomerBlock &&  
                    this.state.showCustomer === true && (
                    <li>
                      <div className="form-group  has-feedback">
                        <label>User</label>
                        <Select
                          isMulti
                          className="basic-single"
                          classNamePrefix="select"
                          defaultValue={this.state.selectedUser}
                          isClearable={true}
                          isSearchable={true}
                          name="cid"
                          options={this.state.my_customer_list}
                          onChange={this.filterSearchMulti}
                        />
                      </div>
                    </li>
                  )}

                  {this.state.stop_api_call && (
                    <li>
                      <div className="form-group  has-feedback">
                        <label>SLA Status</label>
                        <Select
                          isMulti
                          className="basic-single"
                          classNamePrefix="select"
                          isClearable={true}
                          isSearchable={true}
                          name="sla_arr"
                          defaultValue={this.state.sel_sla_task_arr}
                          options={this.state.sla_task_arr}
                          onChange={this.filterSearchMultiSLA}
                        />
                      </div>
                    </li>
                  )}

                  {this.state.my_employee_list && (
                      <li>
                        <div className="form-group  has-feedback">
                          <label>Employees</label>
                          <Select
                            isMulti
                            className="basic-single"
                            classNamePrefix="select"
                            defaultValue={this.state.selectedEmployee}
                            isClearable={true}
                            isSearchable={true}
                            name="eid"
                            options={this.state.my_employee_list}
                            onChange={this.filterSearchMulti}
                          />
                        </div>
                      </li>
                    )}

                  {this.state.designations && (
                    <li>
                      <div className="form-group  has-feedback">
                        <label>Functions</label>
                        <Select
                          isMulti
                          className="basic-single"
                          classNamePrefix="select"
                          defaultValue={this.state.selectedFunctions}
                          isClearable={true}
                          isSearchable={true}
                          name="desig"
                          options={this.state.designations}
                          onChange={this.filterSearchMulti}
                        />
                      </div>
                    </li>
                  )}

                  <li>
                    <div className="form-group react-date-picker">
                      <label>Date From</label>
                      <div className="form-control">
                        <DatePicker
                          name={"date_from"}
                          className="borderNone"
                          dateFormat="dd/MM/yyyy"
                          autoComplete="off"
                          selected={this.state.date_from}
                          onChange={this.filterDateFrom}
                        />
                      </div>
                      {this.state.date_from && (
                        <button
                          onClick={this.removeDateFrom}
                          className="date-close"
                        >
                          x
                        </button>
                      )}
                    </div>
                  </li>

                  <li>
                    <div className="form-group react-date-picker">
                      <label>Date To</label>
                      <div className="form-control">
                        <DatePicker
                          name={"date_to"}
                          className="borderNone"
                          dateFormat="dd/MM/yyyy"
                          autoComplete="off"
                          selected={this.state.date_to}
                          onChange={this.filterDateTo}
                        />
                      </div>
                      {this.state.date_to && (
                        <button
                          onClick={this.removeDateTo}
                          className="date-close"
                        >
                          x
                        </button>
                      )}
                    </div>
                  </li>

                  {this.state.stop_api_call && (
                    <li>
                      <div className="form-group  has-feedback">
                        <label>Due On</label>
                        <Select
                          className="basic-single"
                          classNamePrefix="select"
                          defaultValue={0}
                          isClearable={true}
                          isSearchable={true}
                          name="sla"
                          defaultValue={this.state.sel_sla_arr}
                          options={this.state.sla_arr}
                          onChange={this.filterSearch}
                        />
                      </div>
                    </li>
                  )}
                </ul>
              </div>

              <div className="row">
                <div className="col-xs-12">
                  <div className="nav-tabs-custom">
                    <ul className="nav nav-tabs">
                      <li
                        className={this.state.activeTab === 1 ? "active" : ""}
                        onClick={(e) => this.handleTabs(e)}
                        id="tab_1"
                      >
                        OPEN TASKS ({this.state.countTasks.count_open})
                      </li>

                      <li
                        className={this.state.activeTab === 2 ? "active" : ""}
                        onClick={(e) => this.handleTabs(e)}
                        id="tab_2"
                      >
                        ALLOCATED TASKS ({this.state.countTasks.count_allocated}
                        )
                      </li>

                      <li
                        className={this.state.activeTab === 3 ? "active" : ""}
                        onClick={(e) => this.handleTabs(e)}
                        id="tab_3"
                      >
                        CLOSED TASKS ({this.state.countTasks.count_closed})
                      </li>

                      <li className="tabButtonSec pull-right">
                        {this.state.tabsClicked === "tab_1" &&
                          this.state.countTasks.count_open > 0 && (
                            <span>
                              <LinkWithTooltip
                                tooltip={`Click here to download excel`}
                                href="#"
                                id="tooltip-open"
                                clicked={(e) => this.downloadXLSX(e)}
                              >
                                <i className="fas fa-download"></i>
                              </LinkWithTooltip>
                            </span>
                          )}

                        {this.state.tabsClicked === "tab_2" &&
                          this.state.countTasks.count_allocated > 0 && (
                            <span>
                              <LinkWithTooltip
                                tooltip={`Click here to download excel`}
                                href="#"
                                id="tooltip-allocated"
                                clicked={(e) => this.downloadXLSX(e)}
                              >
                                <i className="fas fa-download"></i>
                              </LinkWithTooltip>
                            </span>
                          )}

                        {this.state.tabsClicked === "tab_3" &&
                          this.state.countTasks.count_closed > 0 && (
                            <span>
                              <LinkWithTooltip
                                tooltip={`Click here to download excel`}
                                href="#"
                                id="tooltip-closed"
                                clicked={(e) => this.downloadXLSX(e)}
                              >
                                <i className="fas fa-download"></i>
                              </LinkWithTooltip>
                            </span>
                          )}
                      </li>
                    </ul>

                    <div className="tab-content">
                      <div
                        className={
                          this.state.activeTab === 1
                            ? "tab-pane active"
                            : "tab-pane"
                        }
                        id="show_tab_1"
                      >

                        {this.state.tableData.open &&
                          this.state.tableData.open.length > 0 && (
                            <MyTasksTable
                              dashType={`${getDashboard()}`}
                              graphURL={this.state.node_url}
                              tableData={this.state.tableData.open}
                              countMyTasks={this.state.countTasks.count_open}
                              queryString={this.state.url_params}
                              allTaskDetails={(e) => this.graphDetailsPagination()}
                            />
                          )
                        }
                        
                        {this.state.tableData.open &&
                          this.state.tableData.open.length === 0 && (
                            <div className="noData">No Data Found</div>
                          )}
                      </div>

                      <div
                        className={
                          this.state.activeTab === 2
                            ? "tab-pane active"
                            : "tab-pane"
                        }
                        id="show_tab_2"
                      >
                        {this.state.tableData.allocated &&
                          this.state.tableData.allocated.length > 0 && (
                          <AllocatedTasksTable
                            dashType={`${getDashboard()}`}
                            graphURL={this.state.node_url}
                            tableData={this.state.tableData.allocated}
                            countAllocatedTasks={
                              this.state.countTasks.count_allocated
                            }
                            queryString={this.state.queryString}
                            allTaskDetails={(e) =>
                              this.graphDetailsPagination()
                            }
                          />
                        )}
                        {this.state.tableData.allocated &&
                          this.state.tableData.allocated.length === 0 && (
                            <div className="noData">No Data Found</div>
                          )}
                      </div>

                      <div
                        className={
                          this.state.activeTab === 3
                            ? "tab-pane active"
                            : "tab-pane"
                        }
                        id="show_tab_3"
                      >
                        {this.state.tableData.closed &&
                          this.state.tableData.closed.length > 0 && (
                            <ClosedTasksTable
                              dashType={`${getDashboard()}`}
                              graphURL={this.state.node_url}
                              tableData={this.state.tableData.closed}
                              countClosedTasks={
                                this.state.countTasks.count_closed
                              }
                              queryString={this.state.queryString}
                              allTaskDetails={(e) =>
                                this.graphDetailsPagination()
                              }
                            />
                        )}
                        
                        {this.state.tableData.closed &&
                          this.state.tableData.closed.length === 0 && (
                            <div className="noData">No Data Found</div>
                          )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        )}
      </>
    );
  }
}

export default graphData;
