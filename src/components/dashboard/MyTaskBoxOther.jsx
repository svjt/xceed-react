import React, { Component } from 'react';
import API from "../../shared/axios";

import taskIcon from '../../assets/images/my-tasks-icon.svg';

class MyTaskBoxOther extends Component {

    state = {
        taskTile: {}
    };

    componentDidMount = () => {

        let query_params = '';

        if(this.props.queryString != ''){
            query_params = `?${this.props.queryString}`;
        }
            
        API.get(`/api/tasks/task_tiles${query_params}`)
        .then(res => {
            this.setState({
                taskTile : res.data.data
            });
            API.get(`/api/tasks/customer_tiles${query_params}`)
            .then(res => {
                console.log(res.data.data);
                this.setState({
                    custTile : res.data.data
                });
                let met_sla,missed_sla,class_met_sla,class_missed_sla,met_num,missed_num;
                API.get(`/api/tasks/my_sla_tiles${query_params}`)
                .then(res => {
                    if(res.data.data.total_sla > 0){
                        met_sla = Math.ceil((res.data.data.total_met_sla/res.data.data.total_sla) * 100); 
                        //missed_sla = Math.ceil((res.data.data.total_missed_sla/res.data.data.total_sla) * 100);
                        missed_sla = 100-met_sla;
                        
                        met_num          = res.data.data.total_met_sla;
                        missed_num       = res.data.data.total_missed_sla;

                        class_met_sla    = met_sla;
                        class_missed_sla = missed_sla;
                    }else{

                        met_num = 0;
                        missed_num = 0;

                        met_sla = 0;
                        missed_sla = 0;
                        class_met_sla = 1;
                        class_missed_sla = 1;
                    }
                    this.setState({
                        met_sla          : met_sla,
                        missed_sla       : missed_sla,
                        met_num          : met_num,
                        missed_num       : missed_num,
                        class_met_sla    : class_met_sla,
                        class_missed_sla : class_missed_sla
                    });
                })
                .catch(err => {
                    console.log(err);
                });
            })
            .catch(err => {
                console.log(err);
            });
        })
        .catch(err => {
            console.log(err);
        });
    }

    render(){
        //console.log('rendering', this.state.taskTile, this.state.taskTile.length);
        return(
            <>

            {this.state.taskTile && this.state.custTile && <div className="col-sm-12 col-xs-12">
                <div className="warehous-small-box other-small-box">
     
                            <div className="warehous-footer-item-main clearfix">
                                <div className="warehous-footer-item">
                                    <div className="first-item">
                                        <h3>{this.state.taskTile.total_tasks}</h3>
                                        <p><strong>My Tasks</strong></p>
                                    </div>
                                </div>
                                <div className="warehous-footer-item"><div className="footerColorBox">New Tasks <span>{this.state.custTile.other_new_task_tile}</span></div></div>
                                <div className="warehous-footer-item"><div className="footerColorBox">Due Today <span>{this.state.taskTile.overdue_today}</span></div></div>
                                <div className="warehous-footer-item"><div className="footerColorBox">Due this Week <span>{this.state.taskTile.overdue_week}</span></div></div>
                                <div className="warehous-footer-item"><div className="footerColorBox">Overdue <span>{this.state.taskTile.overdue_tasks}</span></div></div>

                                {this.state.met_sla !== '' && this.state.missed_sla !== '' && <div className="warehous-footer-item">
                                    <div className="inner-box-other bg-Green clearfix">
                                        <div className="col-60">
                                            <p><span className="roundShape green"></span>Met</p>
                                            <p><span className="roundShape red"></span>Missed</p>
                                        </div>
                                        <div className="col-40">
                                            {this.state.met_sla > 0 && <div className="progress-main">
                                                <div className="progress vertical">
                                                <div className="progress-bar progress-bar-green" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style={{height: `${this.state.class_met_sla}%`}} > </div>
                                                </div>
                                                <span className="sr-only-new">{`${this.state.met_num}`}</span> 
                                            </div>}
                                            {this.state.missed_sla > 0 && <div className="progress-main">
                                                <div className="progress vertical">
                                                <div className="progress-bar progress-bar-red" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style={{height: `${this.state.class_missed_sla}%`}}> </div>
                                                </div>
                                                <span className="sr-only-new">{`${this.state.missed_num}`}</span> 
                                            </div>}
                                        </div>
                                    </div>
                                    <div class="small-box-footer clearfix">
                                        <div class="col-80">
                                            <span class="slabox">My SLA</span>Adherence Score
                                        </div>
                                        {this.state.met_sla > 0 && 
                                        <div className="col-20"><span>{`${this.state.met_sla}%`}</span></div>
                                        }
                                    </div>
                                </div>}


 
                    </div>
                </div>
            </div>}


            
            </>
        );
    }
}

export default MyTaskBoxOther;