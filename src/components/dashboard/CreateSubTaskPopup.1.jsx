import React, { Component } from 'react';

import { Row, Col, ButtonToolbar, Button, Modal } from "react-bootstrap";
import { Formik, Field, Form } from "formik"; 
import * as Yup from "yup"; 
import swal from "sweetalert";

import dateFormat from 'dateformat';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";

import API from "../../shared/axios";
import whitelogo from '../../assets/images/drreddylogo_white.png'

const initialValues = {
    title            : "",
    content             : "",
    employee_list_id : "",
    authorization    : "",
    sendCQT          : false,
    dueDate          : ""
  };

class CreateSubTaskPopup extends Component {    

    constructor(props){
        super(props);

        this.state = {
            showCreateSubTask   : false,
            task_id             : 0,
            sendCQT             : false,
            CQT_customer        : [],
            CQT_product         : [],
            CQT_query           : [],
            CQT_country         : [],
            CQT_unit            : [],
            CQT_reference     : [],
            CQT_spoc            : [],
            showLoader: false
        };
    }

    componentWillReceiveProps = (nextProps) => {
        if(nextProps.showCreateSubTask === true && nextProps.currRow.task_id > 0){

            //var id = 1;
            this.setState({showCreateSubTask: nextProps.showCreateSubTask,currRow:nextProps.currRow});
            
            API.get(`/api/employees/other`)
            .then(res => {
                //console.log(res.data.data);

                var myTeam = [];
                for (let index = 0; index < res.data.data.length; index++) {
                    const element = res.data.data[index];
                    myTeam.push({
                    value: element["employee_id"],
                    label: element["first_name"] +" "+ element["last_name"] +" ("+ element["desig_name"] +")"
                    });
                }

                this.setState({employeeArr:myTeam});
            })
            .catch(err => {
                console.log(err);
            });
        }

    }

    handleClose = () => {
        //reset cqt data
        this.setState({
            sendCQT : false,
            CQT_customer        : [],
            CQT_product         : [],
            CQT_query           : [],
            CQT_country         : [],
            CQT_unit            : [],
            CQT_reference       : [],
            CQT_spoc            : [],
            showLoader          : false
        });
        //end here
        var close = {showCreateSubTask: false,task_id:0};
        this.setState(close);
        this.props.handleClose(close);
    };

    handleSubmitCreateTask = (values, actions) => {       
        
        const postData = {
            title:values.title,            
            content:values.content,
            need_authorization:values.authorization,
            sendCQT : values.sendCQT
        };

        if(values.sendCQT){
            postData.cqt_country_name = values.cqt_country_id.label;
            postData.cqt_customer_id = values.cqt_customer_id.value;
            postData.cqt_product_id = values.cqt_product_id.value;
            postData.cqt_query_id = values.cqt_query_id.value;
            postData.cqt_reference_id = values.cqt_reference_id.value;
            postData.cqt_unit_name = values.cqt_unit_id.label;
            postData.cqt_spoc_id = values.cqt_spoc_id.value;
            postData.due_date = values.dueDate;
        } else {
            var due_date = dateFormat(values.dueDate, "yyyy-mm-dd");            
            postData.assigned_to = values.employee_id.value;
            postData.due_date = due_date;
        }

        API.post(`/api/tasks/create_sub/${this.state.currRow.task_id}`,postData).then(res => {
            this.handleClose();
            swal("Sub Task created successfully!", {
                icon: "success"
            }).then(() => {
                this.props.reloadTask();
            });
        });
        
    }

    changeDueDate = (value) =>{
        initialValues.dueDate = value; 
    }

    getDueDate = () =>{
        if(typeof this.state.currRow !== 'undefined'){
            //console.log(this.state.currRow.due_date);
            var due_date = dateFormat(this.state.currRow.due_date, "yyyy-mm-dd");
            //var replacedDate = this.state.currRow.due_date.split('T');
            //var dateFormat = replacedDate[0];
            return new Date(due_date);
        }
    }    

    handleCQTbox = (e, setFieldValue) => {   
                   
        setFieldValue("sendCQT", e.target.checked);
        this.setState({showLoader: true});
        
        if(e.target.checked) {
            API.get('/api/drl/').then(res => {
               
                this.setState({
                    sendCQT : true,
                    showLoader : false,
                    CQT_customer : res.data.data.customer,
                    CQT_product : res.data.data.product,
                    CQT_query : res.data.data.query,
                    CQT_country : res.data.data.country,
                    CQT_unit : res.data.data.unit,
                    CQT_reference: res.data.data.reference,
                    CQT_spoc: res.data.data.spoc
                })
                
            }).catch(err => {
                console.log( "errors", err )
            });
        } else {
            this.setState({
                sendCQT : false
            });
        }
    }

    render(){

        const validateStopFlag = Yup.object().shape({
            title       : Yup.string()
                .required("Please enter title"),
            content        : Yup.string()
                .required("Please give description"),
            employee_id : Yup.string().when("sendCQT", {
                is: false,
                then: Yup.string()                    
                    .required("Please select employee")
            }),                  
            dueDate     : Yup.string()
                .required("Please enter due date"),
            sendCQT: Yup.bool()
            /*
            noOfAttendants: Yup.string().when("attendantReqd", {
                is: true,
                then: Yup.string()
                    .label("Number of attendant(s)")
                    .required("Please enter the number of Attendant(s)")
                    .matches(/^[1|2]$/, "It must be a numeric value and accept 1 or 2"),
            }),
            */ 
        });

        const newInitialValues = Object.assign(initialValues, {
            title            : "",
            content          : "",
            employee_id      : "",
            authorization    : 0,
            sendCQT          : false,
            dueDate          : this.getDueDate()
        });
          
            return(
                <>
                {typeof this.state.employeeArr !== 'undefined' && 
                <Modal show={this.state.showCreateSubTask} onHide={() => this.handleClose()} backdrop="static">
                    <Formik
                        initialValues    = {newInitialValues}
                        validationSchema = {validateStopFlag}
                        onSubmit         = {this.handleSubmitCreateTask}
                    >
                        {({ values, errors, touched, isValid, isSubmitting, setFieldValue }) => {
                        return (
                            <Form>
                            {this.state.showLoader === true ? ( 
                                <div className="loading_reddy_outer">
                                    <div className="loading_reddy" >
                                        <img src={whitelogo} />
                                    </div>
                                </div>
                             ) : ( "" )}
                             
                            <Modal.Header closeButton>
                                <Modal.Title>
                                Create Sub Task
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div className="contBox">
                                <Row>
                                    <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                        <label>Title</label>
                                        <Field
                                        name="title"
                                        type="text"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.title}
                                        > 
                                        </Field>
                                        {errors.title && touched.title ? (
                                        <span className="errorMsg">
                                            {errors.title}
                                        </span>
                                        ) : null}
                                    </div>
                                    </Col>

                                    {this.state.sendCQT === false && 
                                    <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                        <label>Employee</label>
                                        {/* <Field
                                        name="employee_id"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.employee_id}
                                        >
                                        <option key='-1' value="" >Employee</option>
                                            {this.state.employeeArr.map((emp, i) => (
                                                <option key={i} value={emp.employee_id}>
                                                    {emp.first_name} {emp.last_name} ({emp.desig_name})
                                                </option>
                                            ))}
                                        </Field> */}

                                        <Select
                                            className="basic-single"
                                            classNamePrefix="select"
                                            defaultValue={values.employee_id}
                                            isClearable={true}
                                            isSearchable={true}
                                            name="employee_id"
                                            options={this.state.employeeArr}
                                            onChange={value =>
                                                setFieldValue("employee_id", value)
                                            }
                                        />

                                        {errors.employee_id && touched.employee_id ? (
                                        <span className="errorMsg">
                                            {errors.employee_id}
                                        </span>
                                        ) : null}
                                    </div>
                                    </Col>
                                    }
                                    
                                    {this.state.sendCQT && 
                                        <Col xs={12} sm={6} md={6}>
                                        <div className="form-group">
                                            <label>Customer</label>                                            
                                            <Select
                                                className="basic-single"
                                                classNamePrefix="select"
                                                defaultValue={values.cqt_customer_id}                                                
                                                name="cqt_customer_id"
                                                options={this.state.CQT_customer}
                                                onChange={value =>
                                                    setFieldValue("cqt_customer_id", value)
                                                }
                                            />
    
                                            {errors.cqt_customer_id && touched.cqt_customer_id ? (
                                            <span className="errorMsg">
                                                {errors.cqt_customer_id}
                                            </span>
                                            ) : null}
                                        </div>
                                        </Col>
                                    }

                                    {this.state.sendCQT && 
                                        <Col xs={12} sm={6} md={6}>
                                        <div className="form-group">
                                            <label>Assign To</label>                                            
                                            <Select
                                                className="basic-single"
                                                classNamePrefix="select"
                                                defaultValue={values.cqt_spoc_id}                                                
                                                name="cqt_spoc_id"
                                                options={this.state.CQT_spoc}
                                                onChange={value =>
                                                    setFieldValue("cqt_spoc_id", value)
                                                }
                                            />
    
                                            {errors.cqt_spoc_id && touched.cqt_spoc_id ? (
                                            <span className="errorMsg">
                                                {errors.cqt_spoc_id}
                                            </span>
                                            ) : null}
                                        </div>
                                        </Col>
                                    }
                                </Row>

                                    {this.state.sendCQT && 
                                    <Row>
                                        <Col xs={12} sm={6} md={6}>
                                        <div className="form-group">
                                            <label>Product</label>                                            
                                            <Select
                                                className="basic-single"
                                                classNamePrefix="select"
                                                defaultValue={values.cqt_product_id}                                                
                                                name="cqt_product_id"
                                                options={this.state.CQT_product}
                                                onChange={value =>
                                                    setFieldValue("cqt_product_id", value)
                                                }
                                            />
    
                                            {errors.cqt_product_id && touched.cqt_product_id ? (
                                            <span className="errorMsg">
                                                {errors.cqt_product_id}
                                            </span>
                                            ) : null}
                                        </div>
                                        </Col>

                                        <Col xs={12} sm={6} md={6}>
                                        <div className="form-group">
                                            <label>Query Category</label>                                            
                                            <Select
                                                className="basic-single"
                                                classNamePrefix="select"
                                                defaultValue={values.cqt_query_id}                                                
                                                name="cqt_query_id"
                                                options={this.state.CQT_query}
                                                onChange={value =>
                                                    setFieldValue("cqt_query_id", value)
                                                }
                                            />
    
                                            {errors.cqt_query_id && touched.cqt_query_id ? (
                                            <span className="errorMsg">
                                                {errors.cqt_query_id}
                                            </span>
                                            ) : null}
                                        </div>
                                        </Col>

                                        <Col xs={12} sm={6} md={6}>
                                        <div className="form-group">
                                            <label>Reference Query</label>                                            
                                            <Select
                                                className="basic-single"
                                                classNamePrefix="select"
                                                defaultValue={values.cqt_reference_id}                                                
                                                name="cqt_reference_id"
                                                options={this.state.CQT_reference}
                                                onChange={value =>
                                                    setFieldValue("cqt_reference_id", value)
                                                }
                                            />
    
                                            {errors.cqt_reference_id && touched.cqt_reference_id ? (
                                            <span className="errorMsg">
                                                {errors.cqt_reference_id}
                                            </span>
                                            ) : null}
                                        </div>
                                        </Col>

                                        <Col xs={12} sm={6} md={6}>
                                        <div className="form-group">
                                            <label>Country</label>                                            
                                            <Select
                                                className="basic-single"
                                                classNamePrefix="select"
                                                defaultValue={values.cqt_country_id}                                                
                                                name="cqt_country_id"
                                                options={this.state.CQT_country}
                                                onChange={value =>
                                                    setFieldValue("cqt_country_id", value)
                                                }
                                            />
    
                                            {errors.cqt_country_id && touched.cqt_country_id ? (
                                            <span className="errorMsg">
                                                {errors.cqt_country_id}
                                            </span>
                                            ) : null}
                                        </div>
                                        </Col>


                                        <Col xs={12} sm={6} md={6}>
                                        <div className="form-group">
                                            <label>Unit Name</label>                                            
                                            <Select
                                                className="basic-single"
                                                classNamePrefix="select"
                                                defaultValue={values.cqt_unit_id}                                                
                                                name="cqt_unit_id"
                                                options={this.state.CQT_unit}
                                                onChange={value =>
                                                    setFieldValue("cqt_unit_id", value)
                                                }
                                            />
    
                                            {errors.cqt_unit_id && touched.cqt_unit_id ? (
                                            <span className="errorMsg">
                                                {errors.cqt_unit_id}
                                            </span>
                                            ) : null}
                                        </div>
                                        </Col>                                   
                                    </Row>
                                    }
                                

                                <Row>
                                    <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                        <label>Description*</label>
                                        <Field
                                        name="content"
                                        component="textarea"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.content}
                                        > 
                                        </Field>

                                        {/* <TinyMCE
                                            name="content"
                                            content={values.comment}
                                            config={{
                                                branding: false,
                                                toolbar: 'undo redo | bold italic | alignleft aligncenter alignright'
                                            }}
                                            className={`selectArowGray form-control`}
                                            autoComplete="off"
                                            onChange={value =>
                                                setFieldValue("content", value.level.content)
                                            }
                                        /> */}

                                        {errors.content && touched.content ? (
                                        <span className="errorMsg">
                                            {errors.content}
                                        </span>
                                        ) : null}
                                    </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                        <label>Due Date</label>
                                        {/* <DatePicker
                                            name="dueDate"
                                            onChange={value =>
                                                setFieldValue("dueDate", value)
                                            }
                                            value={values.dueDate}
                                        /> */}
                                        <div className="form-control">
                                            <DatePicker
                                                name={'dueDate'}
                                                className="borderNone"
                                                selected={values.dueDate ? values.dueDate : null}
                                                onChange={value => setFieldValue('dueDate', value)}
                                                dateFormat='dd/MM/yyyy'
                                                autoComplete="off"
                                            />

                                            {errors.dueDate && touched.dueDate ? (
                                            <span className="errorMsg">
                                                {errors.dueDate}
                                            </span>
                                            ) : null}
                                        </div>
                                    </div>
                                    </Col>
                                    <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                        <label>Need Approval</label>
                                        <Field
                                        name="authorization"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.authorization}
                                        >
                                        <option key='0' value="0" >No</option>
                                        <option key='1' value="1" >Yes</option>
                                        </Field>
                                    </div>
                                    </Col>

                                    <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                        <Field
                                            type="checkbox"
                                            name="sendCQT"
                                            value="1"
                                            onChange={(e) => this.handleCQTbox(e, setFieldValue)}
                                            checked={values.sendCQT === true ? 'checked' : ''}
                                        />
                                        <label>Send to CQT</label>
                                    </div>
                                    </Col>
                                </Row>

                                </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <ButtonToolbar>
                                <Button
                                    className={`btn btn-success btn-sm ${
                                    isValid ? "btn-custom-green" : "btn-disable"
                                    } m-r-10`}
                                    type="submit"
                                    disabled={isValid ? false : true}
                                >
                                    {this.state.stopflagId > 0
                                    ? isSubmitting
                                        ? "Updating..."
                                        : "Update"
                                    : isSubmitting
                                    ? "Submitting..."
                                    : "Submit"}
                                </Button>
                                <Button
                                    onClick={this.handleClose}
                                    className={`btn btn-danger btn-sm`}
                                    type="button"
                                >
                                    Close
                                </Button>
                                </ButtonToolbar>
                            </Modal.Footer>
                            </Form>
                        );
                        }}
                    </Formik>
                    </Modal>}
                </>
            );
    }
}

export default CreateSubTaskPopup;