import React, { Component } from 'react';

class MyProductsBox extends Component {
    render(){
        return(
            <>
            <div className="col-lg-3 col-sm-6 col-xs-12">
                <div className="small-box bg-Yellow">
                    <div className="inner">
                    <div className="row clearfix">
                        <div className="col-xs-8">
                            <h3>6</h3>
                            <p><strong>My Products</strong></p>
                        </div>
                        <div className="col-xs-4">
                            <div className="icon"><i className="fas fa-archive"></i></div>
                        </div>
                    </div>
                    </div>
                    <div className="small-box-footer clearfix">
                        <div className="row clearfix">
                            <div className="col-xs-8"></div>
                            <div className="col-xs-4 col-border">Active <span>4</span></div>
                        </div>
                    </div>
                </div>
            </div>
            </>
        );
    }
}

export default MyProductsBox;