import React, { Component } from 'react';
import { Row, Col, ButtonToolbar, Button, Modal, Alert } from "react-bootstrap";
import { Formik, Form } from "formik"; 
import * as Yup from "yup";

//import { FilePond } from 'react-filepond';
//import 'filepond/dist/filepond.min.css';
import Dropzone from 'react-dropzone';
//import TinyMCE from 'react-tinymce';

import { Editor } from "@tinymce/tinymce-react";

import API from "../../shared/axios";
import swal from "sweetalert";

import whitelogo from '../../assets/images/drreddylogo_white.png';
import {showErrorMessageFront} from "../../shared/handle_error_front";
import { htmlDecode,trimString } from "../../shared/helper";

const initialValues = {
    comment         : ""
  };

const removeDropZoneFiles = (fileName,objRef,setErrors) => {
    var newArr = [];
    for (let index = 0; index < objRef.state.files.length; index++) {
        const element = objRef.state.files[index];
        
        if(fileName === element.name){
            
        }else{
            newArr.push(element);
        }
    }

    var fileListHtml =  newArr.map(file => (
        <Alert key={file.name}>
            <span onClick={() => removeDropZoneFiles(file.name,objRef,setErrors)} ><i className="far fa-times-circle"></i></span>{" "}{file.name}
        </Alert>
    ));
    setErrors({ file_name: '' });
    objRef.setState({
        files:newArr,
        filesHtml:fileListHtml
    });

}

class OriginalCommentPopup extends Component {    

    constructor(props){
        super(props);
        this.state = {
            showOriginalEditComment: false,
            files          : [],
            filesHtml      : ''
        };
    }

    componentWillReceiveProps = (nextProps) => {
        //console.log(nextProps);
        if(nextProps.showOriginalEditComment === true && nextProps.currRow.task_id > 0){
            console.log(nextProps.currRow);
            this.setState({
                showOriginalEditComment: nextProps.showOriginalEditComment,
                currRow        : nextProps.currRow,
                files          : [],
                filesHtml      : ""
            });
            initialValues.comment = htmlDecode(nextProps.currRow.comment);
        }
    }

    handleClose = () => {
        var close = {showOriginalEditComment:false,files:[],filesHtml:""};
        this.setState(close);
        this.props.handleClose(close);
    };

    handleComment = (values, actions) => {

        this.setState({showOriginalEditCommentLoader: true});
        var formData = new FormData();

        formData.append('comment', values.comment);

        API.post(`/api/tasks/update_original_discussion_comment/${this.state.currRow.discussion_id}/${this.state.currRow.comment_id}`,formData).then(res => { 
            this.handleClose();
            this.setState({showTranslateEditCommentLoader: false});
            swal({
                closeOnClickOutside: false,
                title: "Success",
                text: "Data Updated.",
                icon: "success"
            }).then(() => {
                this.props.reloadTask();
            });
        }).catch(error => {
            this.setState({ showTranslateEditCommentLoader: false });
            if(error.data.status === 3){
                var token_rm = 2;
                showErrorMessageFront(error,token_rm,this.props);
                this.handleClose();
            }else{
                actions.setErrors(error.data.errors);
                actions.setSubmitting(false);
            }
        });
    }

    render(){

        const validateStopFlag = Yup.object().shape({
            comment : Yup.string().trim()
                .required("Please enter your comments")
        });

        return(
            <>
            <Modal show={this.state.showOriginalEditComment} onHide={() => this.handleClose()} backdrop="static" className="respondBackpop">
                <Formik
                    initialValues    = {initialValues}
                    validationSchema = {validateStopFlag}
                    onSubmit         = {(values, actions) => {
                            this.handleComment(values, actions);
                        }
                    }
                    
                >
                    {({ values, errors, touched, isValid, isSubmitting, setFieldValue, setFieldTouched, setErrors }) => {
                    return (
                        <Form encType="multipart/form-data">
                        {this.state.showOriginalEditCommentLoader === true ? ( 
                            <div className="loading_reddy_outer">
                                <div className="loading_reddy" >
                                    <img src={whitelogo} alt="loader"/>
                                </div>
                            </div>
                            ) : ( "" )}
                        <Modal.Header closeButton>
                            <Modal.Title>
                                Review Comment
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="contBox">
                            <Row>
                                <Col xs={12} sm={12} md={12}>                                    
                                <div className="form-group">
                                <label>Comment <span className="required-field">*</span></label>
                                    
                                    <Editor
                                        name="comment"
                                        className={`selectArowGray form-control`}
                                        value={
                                            (values.comment !== null && values.comment !== "") ? values.comment : 
                                            ''
                                        }
                                        content={
                                            (values.comment !== null && values.comment !== "") ? values.comment : 
                                            ''
                                        }
                                        init={{
                                        menubar: false,
                                        branding: false,
                                        placeholder: "Enter comments",
                                        plugins:
                                            "link table hr visualblocks code placeholder lists autoresize textcolor",
                                        toolbar:
                                            "bold italic strikethrough superscript subscript | forecolor backcolor | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | visualblocks code | fontselect",
                                        font_formats:
                                            'Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats'
                                        }}
                                        onEditorChange={value =>
                                            setFieldValue("comment", value)
                                        }
                                    />
                                    {errors.comment && touched.comment ? (
                                    <span className="errorMsg">
                                        {errors.comment}
                                    </span>
                                    ) : null}
                                </div>
                                </Col>
                            </Row>
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <button
                                onClick={this.handleClose}
                                className={`btn-line`}
                                type="button"
                            >
                                Close
                            </button>
                            <button
                                className={`btn-fill ${
                                isValid ? "btn-custom-green" : "btn-disable"
                                } m-r-10`}
                                type="submit"
                                id="leave_comment"
                                disabled={isValid ? false : true}
                            >
                                Update
                            </button>
                        </Modal.Footer>
                        </Form>
                    );
                    }}
                </Formik>
                </Modal>
            </>
        );

    }
}

export default OriginalCommentPopup;