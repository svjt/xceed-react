import React, { Component } from "react";
import { Row, Col, ButtonToolbar, Button, Modal, Alert } from "react-bootstrap";

//import { FilePond } from 'react-filepond';
//import 'filepond/dist/filepond.min.css';
import Dropzone from "react-dropzone";

import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import dateFormat from "dateformat";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";
import API from "../../shared/axios";
import { showErrorMessageFront } from "../../shared/handle_error_front";

import swal from "sweetalert";

import { localDate, trimString } from "../../shared/helper";

//import uploadAxios from 'axios';

import TinyMCE from "react-tinymce";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";

const initialValues = {
  employeeId: "",
  comment: "",
  breach_reason: "",
  file: "",
  dueDate: "",
  showModalLoader: false,
  file_name: [],
};

//console.log( TinyMCE );

// const onlyUnique = (value, index, self) => {
//     return self.indexOf(value) === index;
// }

const removeDropZoneFiles = (fileName, objRef, setErrors) => {
  var newArr = [];
  for (let index = 0; index < objRef.state.files.length; index++) {
    const element = objRef.state.files[index];

    if (fileName === element.name) {
    } else {
      newArr.push(element);
    }
  }

  var fileListHtml = newArr.map((file) => (
    <Alert key={file.name}>
      <span onClick={() => removeDropZoneFiles(file.name, objRef, setErrors)}>
        <i className="far fa-times-circle"></i>
      </span>{" "}
      {file.name}
    </Alert>
  ));
  setErrors({ file_name: "" });
  objRef.setState({
    files: newArr,
    filesHtml: fileListHtml,
  });
};

class RequestToReopen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showReqToReopen: false,
      files: [],
      sla_breach: false,
      showModalLoader: false,
      acceptNewEmp: false,
      acceptNewEmpMsg: "",
      filesHtml: "",
    };
  }

  handleChange = (e, field) => {
    this.setState({
      [field]: e.target.value,
    });
  };

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.showReqToReopen === true && nextProps.currRow.task_id > 0) {
      API.get(`/api/employees/my_manager`)
        .then((res) => {
          let manager_id = res.data.data.manager_id;
          let manager = `${res.data.data.manager_fname} ${res.data.data.manager_lname}`;
          if (manager_id && manager_id > 0) {
            this.setState({
              showReqToReopen: nextProps.showReqToReopen,
              currRow: nextProps.currRow,
              manager: manager,
              manager_id: manager_id,
            });

            // API.get(`/api/employees/other`)
            // .then(res => {

            //     var myTeam = [];
            //     let selTeam = '';
            //     for (let index = 0; index < res.data.data.length; index++) {
            //         const element = res.data.data[index];
            //         var leave = '';
            //         if(element.on_leave == 1){
            //             leave = '[On Leave]';
            //         }
            //         myTeam.push({
            //             value: element["employee_id"],
            //             label: element["first_name"] +" "+ element["last_name"] +" ("+ element["desig_name"] +") " +leave
            //         });

            //         if(parseInt(element["employee_id"]) === parseInt(manager_id)){

            //             selTeam= {
            //                 value: element["employee_id"],
            //                 label: element["first_name"] +" "+ element["last_name"] +" ("+ element["desig_name"] +") " +leave
            //             };
            //         }
            //     }

            //     this.setState({showReqToReopen: nextProps.showReqToReopen,currRow: nextProps.currRow,employeeArr:myTeam,selectedEmployee:selTeam});
            // })
            // .catch(err => {
            //     console.log(err);
            // });
          } else {
            swal({
              closeOnClickOutside: false,
              title: "Error",
              text:
                "In order to re-open this task please get in touch with your administrator.",
              icon: "success",
            }).then(() => {
              this.handleClose();
              this.props.reloadTask();
            });

            // API.get(`/api/tasks/get_task_bm/${nextProps.currRow.task_id}`)
            // .then(res => {

            //     if(res.data.data && res.data.data.length > 0){

            //         let assigned_bm   = res.data.data[0].assigned_to;
            //         let assigned_name = `${res.data.data[0].assigned_fname} ${res.data.data[0].assigned_lname}`;

            //         this.setState({showReqToReopen: nextProps.showReqToReopen,currRow: nextProps.currRow,manager:assigned_name,manager_id:assigned_bm});
            //     }else{
            //         swal({
            //             closeOnClickOutside: false,
            //             title: "Error",
            //             text: "Sorry You do not have a manager.",
            //             icon: "success"
            //         }).then(() => {
            //             this.handleClose();
            //             this.props.reloadTask();
            //         });
            //     }
            // })
            // .catch(err => {
            //     console.log(err);
            // });
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  handleClose = () => {
    var close = {
      sla_breach: false,
      new_employee: false,
      msg_new_employee: "",
      showModalLoader: false,
      showReqToReopen: false,
      files: [],
      filesHtml: "",
    };
    this.setState(close);
    this.props.handleClose(close);
  };

  handleRequestToReopen = (values, actions) => {
    //console.log(values);
    this.setState({ showModalLoader: true, msg_new_employee: false });
    var post_data = {
      employees: this.state.manager_id,
      comment: values.comment.trim(),
      assign_id: this.state.currRow.assignment_id,
    };

    API.post(
      `/api/tasks/request_reopen/${this.state.currRow.task_id}`,
      post_data
    )
      .then((res) => {
        this.handleClose();
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: res.data.data,
          icon: "success",
        }).then(() => {
          this.setState({ showModalLoader: false });
          this.props.reloadTask();
        });
      })
      .catch((error) => {
        this.setState({ showModalLoader: false });
        if (error.data.status === 3) {
          var token_rm = 2;
          showErrorMessageFront(error, token_rm, this.props);
          this.handleClose();
        } else {
          actions.setErrors(error.data.errors);
          actions.setSubmitting(false);
        }
      });
  };

  changeEmployee = (event, setFieldValue) => {
    if (event === null) {
      setFieldValue("employeeId", "");
    } else {
      setFieldValue("employeeId", event);
    }
  };

  render() {
    const newInitialValues = Object.assign(initialValues, {
      employeeId: this.state.selectedEmployee,
      comment: "",
    });

    const validateStopFlag = Yup.object().shape({
      comment: Yup.string().trim().required("Please enter your comment"),
    });

    //console.log(this.state.employeeArr)

    return (
      <>
        {typeof this.state.manager_id !== "undefined" && (
          <Modal
            show={this.state.showReqToReopen}
            onHide={() => this.handleClose()}
            backdrop="static"
          >
            <Formik
              initialValues={newInitialValues}
              validationSchema={validateStopFlag}
              onSubmit={this.handleRequestToReopen}
            >
              {({
                values,
                errors,
                touched,
                isValid,
                isSubmitting,
                handleChange,
                setFieldValue,
                setFieldTouched,
                setErrors,
              }) => {
                /* console.log('values', values)
                        console.log('errors', errors)
                        console.log('touched', touched)
                        console.log('isValid', isValid) */
                return (
                  <Form encType="multipart/form-data">
                    {this.state.showModalLoader === true ? (
                      <div className="loderOuter">
                        <div className="loader">
                          <img src={loaderlogo} alt="logo" />
                          <div className="loading">Loading...</div>
                        </div>
                      </div>
                    ) : (
                      ""
                    )}
                    <Modal.Header closeButton>
                      <Modal.Title>
                        Request To Reopen | {this.state.currRow.task_ref}
                      </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <div className="contBox">
                        {/* {this.state.currRow.parent_id > 0 && <Row>
                                    <Col xs={12} sm={12} md={12}>
                                        <div className="form-group">
                                            <label>Task Tile:</label>
                                            <p>{this.state.currRow.title}</p>
                                        </div>
                                    </Col>
                                </Row>} */}
                        <Row>
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>Manager: {this.state.manager}</label>
                            </div>
                          </Col>
                        </Row>

                        <Row>
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>
                                Comment{" "}
                                <span className="required-field">*</span>
                              </label>
                              {/* <TinyMCE
                                            name="comment"
                                            content={values.comment}
                                            config={{
                                                menubar: false,
                                                branding: false,
                                                toolbar: 'undo redo | bold italic | alignleft aligncenter alignright'
                                            }}
                                            className={`selectArowGray form-control`}
                                            autoComplete="off"
                                            onChange={value =>
                                                setFieldValue("comment", value.level.content)
                                            }
                                            onBlur={() => setFieldTouched("comment")}  
                                        /> */}

                              <Field
                                name="comment"
                                component="textarea"
                                className="form-control"
                                autoComplete="off"
                                placeholder="Comment here"
                                onChange={handleChange}
                                style={{ height: "130px" }}
                              />

                              {errors.comment && touched.comment ? (
                                <span className="errorMsg">
                                  {errors.comment}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                        </Row>
                      </div>
                    </Modal.Body>
                    <Modal.Footer>
                      <button
                        onClick={this.handleClose}
                        className={`btn-line`}
                        type="button"
                      >
                        Cancel
                      </button>
                      <button
                        className={`btn-fill ${
                          isValid ? "btn-custom-green" : "btn-disable"
                        } m-r-10`}
                        type="submit"
                        disabled={isValid ? false : true}
                      >
                        {this.state.stopflagId > 0
                          ? isSubmitting
                            ? "Updating..."
                            : "Update"
                          : isSubmitting
                          ? "Submitting..."
                          : "Submit"}
                      </button>
                    </Modal.Footer>
                  </Form>
                );
              }}
            </Formik>
          </Modal>
        )}
      </>
    );
  }
}

export default RequestToReopen;
