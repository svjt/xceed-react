import React, { Component } from 'react';
import { Row, Col, ButtonToolbar, Button, Modal, Alert } from "react-bootstrap";
import { Formik, Form } from "formik"; 
import * as Yup from "yup";

//import { FilePond } from 'react-filepond';
//import 'filepond/dist/filepond.min.css';
import Dropzone from 'react-dropzone';
//import TinyMCE from 'react-tinymce';

import { Editor } from "@tinymce/tinymce-react";

import API from "../../shared/axios";
import swal from "sweetalert";

import whitelogo from '../../assets/images/drreddylogo_white.png';
import {showErrorMessageFront} from "../../shared/handle_error_front";
import { htmlDecode,trimString } from "../../shared/helper";



const initialValues = {
    original_comment    : "",
    translated_comment  : "",
    file                : "",
    file_name           : []
  };

const removeDropZoneFiles = (fileName,objRef,setErrors) => {
    var newArr = [];
    for (let index = 0; index < objRef.state.files.length; index++) {
        const element = objRef.state.files[index];
        
        if(fileName === element.name){
            
        }else{
            newArr.push(element);
        }
    }

    var fileListHtml =  newArr.map(file => (
        <Alert key={file.name}>
            <span onClick={() => removeDropZoneFiles(file.name,objRef,setErrors)} ><i className="far fa-times-circle"></i></span>{" "}{file.name}
        </Alert>
    ));
    setErrors({ file_name: '' });
    objRef.setState({
        files:newArr,
        filesHtml:fileListHtml
    });

}

class TranslateCommentPopup extends Component {    

    constructor(props){
        super(props);
        this.state = {
            showTranslateEditComment: false,
            translate_language:false,
            other_language:false,
            files          : [],
            filesHtml      : ''
        };
    }

    componentWillReceiveProps = (nextProps) => {
        //console.log(nextProps);
        if(nextProps.showTranslateEditComment === true && nextProps.currRow.task_id > 0){
            // console.log(nextProps.currRow);
            this.setState({
                showTranslateEditComment: nextProps.showTranslateEditComment,
                currRow        : nextProps.currRow,
                files          : [],
                filesHtml      : ""
            });
            this.setState({original_translated_comment:nextProps.currRow.translated_comment,original_english_comment:nextProps.currRow.posted_comment,translate_language:false,other_language:false});
            initialValues.original_comment = htmlDecode(nextProps.currRow.posted_comment);
            initialValues.translated_comment = htmlDecode(nextProps.currRow.translated_comment);

        }
    }

    handleClose = () => {
        var close = {showTranslateEditComment:false,files:[],filesHtml:""};
        this.setState(close);
        this.props.handleClose(close);
    };

    handleComment = (values, actions) => {
        this.setState({showTranslateEditCommentLoader: true});
        var formData = new FormData();

        formData.append('original_comment', values.original_comment.trim());
        formData.append('translated_comment', values.translated_comment.trim());
        formData.append('approve', 0);

        API.post(`/api/tasks/update_discussion_comment/${this.state.currRow.discussion_id}/${this.state.currRow.comment_id}/${this.state.currRow.task_id}`,formData).then(res => { 
            this.handleClose();
            this.setState({showTranslateEditCommentLoader: false});
            swal({
                closeOnClickOutside: false,
                title: "Success",
                text: "Comments have been updated.",
                icon: "success"
            }).then(() => {
                this.props.reloadTask();
            });
        }).catch(error => {
            this.setState({ showTranslateEditCommentLoader: false });
            if(error.data.status === 3){
                var token_rm = 2;
                showErrorMessageFront(error,token_rm,this.props);
                this.handleClose();
            }else{
                actions.setErrors(error.data.errors);
                actions.setSubmitting(false);
            }
        });
    }

    handleRespond = (values, actions) => {
        this.setState({showTranslateEditCommentLoader: true});
        var formData = new FormData();

        formData.append('original_comment', values.original_comment.trim());
        formData.append('translated_comment', values.translated_comment.trim());
        formData.append('approve', 1);

        API.post(`/api/tasks/update_discussion_comment/${this.state.currRow.discussion_id}/${this.state.currRow.comment_id}/${this.state.currRow.task_id}`,formData).then(res => { 
            this.handleClose();
            this.setState({showTranslateEditCommentLoader: false});
            swal({
                closeOnClickOutside: false,
                title: "Success",
                text: "Comment has been approved!",
                icon: "success"
            }).then(() => {
                this.props.reloadTask();
            });
        }).catch(error => {
            this.setState({ showTranslateEditCommentLoader: false });
            if(error.data.status === 3){
                var token_rm = 2;
                showErrorMessageFront(error,token_rm,this.props);
                this.handleClose();
            }else{
                actions.setErrors(error.data.errors);
                actions.setSubmitting(false);
            }
        });
    }

    setDropZoneFiles = (acceptedFiles,setErrors,setFieldValue) => {
        //console.log(acceptedFiles);
        setErrors({ file_name: false });
        setFieldValue(this.state.files);

        var prevFiles = this.state.files;
        var newFiles;
        if(prevFiles.length > 0){
            //newFiles = newConcatFiles = acceptedFiles.concat(prevFiles);
    
            for (let index = 0; index < acceptedFiles.length; index++) {
                var remove  = 0;
    
                for (let index2 = 0; index2 < prevFiles.length; index2++) {
    
                    if(acceptedFiles[index].name === prevFiles[index2].name){
                        remove = 1;
                        break;
                    }
                    
                }
    
                //console.log('remove',acceptedFiles[index].name,remove);
    
                if(remove === 0){
                    prevFiles.push(acceptedFiles[index]);
                }
            }
            //console.log('acceptedFiles',acceptedFiles);
            //console.log('prevFiles',prevFiles);
            newFiles = prevFiles;
        }else{
            newFiles = acceptedFiles;
        }
    
        this.setState({
            files: newFiles
        });
    
        var fileListHtml =  newFiles.map(file => (
            <Alert key={file.name} >
              <span onClick={() => removeDropZoneFiles(file.name,this,setErrors)} ><i className="far fa-times-circle"></i></span>{" "}{trimString(25,file.name)} 
            </Alert>
        ));
        
        this.setState({
            filesHtml:fileListHtml
        });
    }   
    
    translateEnglishComment = (e,translated_comment,translated_language,setFieldValue) => {
        if(this.state.translate_language){
            e.preventDefault();
            //console.log(translated_comment,translated_language);
            var formData = new FormData();
            formData.append('content', translated_comment.trim());
            formData.append('posted_language', translated_language.trim());

            API.post(`/api/tasks/explicit_translate`,formData).then(res => { 
                
                setFieldValue('original_comment',res.data.data);
                this.setState({translate_language:false,other_language:false,original_translated_comment:translated_comment});
                
            }).catch(error => {
                
            });

        }
    }

    translateOtherLangComment = (e,translated_comment,translated_language,setFieldValue) => {
        if(this.state.other_language){
            e.preventDefault();
            //console.log(translated_comment,translated_language);
            var formData = new FormData();
            formData.append('content', translated_comment.trim());
            formData.append('posted_language', translated_language.trim());

            API.post(`/api/tasks/explicit_translate_english`,formData).then(res => { 
                
                setFieldValue('translated_comment',res.data.data);
                this.setState({translate_language:false,other_language:false,original_english_comment:translated_comment});
                
            }).catch(error => {
                
            });

        }
    }

    getDynamicLang = (lang) => {
        if(lang == 'ja'){
            return 'Japanese';
        }else if(lang == 'zh'){
            return 'Mandarin';
        }else if(lang == 'es'){
            return 'Spanish';
        }else if(lang == 'pt'){
            return 'Portuguese';
        }
    }

    render(){

        const validateStopFlag = Yup.object().shape({
            original_comment : Yup.string().trim()
                .required("Please enter your original comments"),
            translated_comment : Yup.string().trim()
                .required("Please enter your translated comments"),    
        });

          

            return(
                <>
                <Modal show={this.state.showTranslateEditComment} onHide={() => this.handleClose()} backdrop="static" className="respondBackpop">
                    <Formik
                        initialValues    = {initialValues}
                        validationSchema = {validateStopFlag}
                        onSubmit         = {(values, actions) => {

                            if(this.state.translate_language === false || this.state.other_language === false){
                                if(values.leaveComment){
                                    this.handleComment(values, actions);
                                }else{
                                    this.handleRespond(values, actions);
                                }
                            }else{
                                swal({
                                    closeOnClickOutside: false,
                                    title: "Proceed Without Auto Translation?",
                                    text: "",
                                    icon: "warning",
                                    buttons: true,
                                    dangerMode: true,
                                }).then((willDelete) => {
                                    if (willDelete) {
                                        if(values.leaveComment){
                                            this.handleComment(values, actions);
                                        }else{
                                            this.handleRespond(values, actions);
                                        }
                                    }
                                });
                            }
                        }
                        }
                        
                    >
                        {({ values, errors, touched, isValid, isSubmitting, setFieldValue, setFieldTouched, setErrors }) => {
                        return (
                            <Form encType="multipart/form-data">
                            {this.state.showTranslateEditCommentLoader === true ? ( 
                                <div className="loading_reddy_outer">
                                    <div className="loading_reddy" >
                                        <img src={whitelogo} alt="loader"/>
                                    </div>
                                </div>
                             ) : ( "" )}
                            <Modal.Header closeButton>
                                <Modal.Title>
                                Review Comment
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div className="contBox">
                                <Row>
                                    <Col xs={12} sm={12} md={12}>                                    
                                    <div className="form-group">
                                    <label>Original Comment ({(this.state.currRow.posted_language.toUpperCase() == 'ZH')?'MA':this.state.currRow.posted_language.toUpperCase()}) <span className="required-field">*</span>

                                    {this.state.other_language &&  
                                        <>{`  `}<a
                                            href='#'
                                            style={{ color: "rgb(0, 86, 179)",fontWeight:"700" }}
                                            onClick={(e)=>{e.preventDefault();this.translateOtherLangComment(e,values.original_comment,this.state.currRow.translated_language,setFieldValue)}}
                                        >
                                            Click To Update {this.getDynamicLang(this.state.currRow.translated_language)} Translation
                                        </a></>
                                    }

                                    </label>
                                        <Editor
                                            name="original_comment"
                                            className={`selectArowGray form-control`}
                                            value={
                                                (values.original_comment !== null && values.original_comment !== "") ? values.original_comment : 
                                                ''
                                            }
                                            content={
                                                (values.original_comment !== null && values.original_comment !== "") ? values.original_comment : 
                                                ''
                                            }
                                            init={{
                                            menubar: false,
                                            branding: false,
                                            placeholder: "Enter comments",
                                            plugins:
                                                "link table hr visualblocks code placeholder lists autoresize textcolor",
                                            toolbar:
                                                "bold italic strikethrough superscript subscript | forecolor backcolor | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | visualblocks code | fontselect",
                                            font_formats:
                                                'Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats'
                                            }}
                                            onEditorChange={value =>
                                                {
                                                    setFieldValue("original_comment", value)

                                                    if(htmlDecode(value) === htmlDecode(this.state.original_english_comment)){
                                                        this.setState({other_language:false});
                                                    }else{
                                                        this.setState({other_language:true});
                                                    }
                                                }
                                            }
                                        />
                                        {errors.original_comment && touched.original_comment ? (
                                        <span className="errorMsg">
                                            {errors.original_comment}
                                        </span>
                                        ) : null}
                                    </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={12} sm={12} md={12}>                                    
                                    <div className="form-group">
                                        <label>Translated Comment ({(this.state.currRow.translated_language.toUpperCase() == 'ZH')?'MA':this.state.currRow.translated_language.toUpperCase()}) <em>(Customer shall receive the below response)</em> <span className="required-field">*</span><br/>
                                            {this.state.translate_language &&  
                                                <>{`  `}<a
                                                    href='#'
                                                    style={{ color: "rgb(0, 86, 179)",fontWeight:"700" }}
                                                    onClick={(e)=>{e.preventDefault();this.translateEnglishComment(e,values.translated_comment,this.state.currRow.translated_language,setFieldValue)}}
                                                >
                                                    Click To Update English Translation
                                                </a></>
                                            }
                                        </label>
                                        <Editor
                                            name="translated_comment"
                                            className={`selectArowGray form-control`}
                                            value={
                                                (values.translated_comment !== null && values.translated_comment !== "") ? values.translated_comment : 
                                                ''
                                            }
                                            content={
                                                (values.translated_comment !== null && values.translated_comment !== "") ? values.translated_comment : 
                                                ''
                                            }
                                            init={{
                                            menubar: false,
                                            branding: false,
                                            placeholder: "Enter comments",
                                            plugins:
                                                "link table hr visualblocks code placeholder lists autoresize textcolor",
                                            toolbar:
                                                "bold italic strikethrough superscript subscript | forecolor backcolor | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | visualblocks code | fontselect",
                                            font_formats:
                                                'Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats'
                                            }}
                                            onEditorChange={value =>
                                                {
                                                    setFieldValue("translated_comment", value);
                                                    //console.log(htmlDecode(value),htmlDecode(this.state.currRow.translated_comment));
                                                    if(htmlDecode(value) === htmlDecode(this.state.original_translated_comment)){
                                                        this.setState({translate_language:false});
                                                    }else{
                                                        this.setState({translate_language:true});
                                                    }
                                                    
                                                }
                                            }
                                        />
                                        {errors.translated_comment && touched.translated_comment ? (
                                        <span className="errorMsg">
                                            {errors.translated_comment}
                                        </span>
                                        ) : null}
                                    </div>
                                    </Col>
                                </Row>
                                </div>
                            </Modal.Body>
                            <Modal.Footer>
                               
                                

                                <button
                                    className={`btn-fill m-r-10 wd220`}
                                    type="submit"
                                    id="leave_comment_share"
                                    onClick={(e)=>{
                                        setFieldValue('leaveComment',false);
                                    }}
                                >
                                    Approve and share to customer
                                </button>

                                <button
                                    className={`btn-fill ${
                                    isValid ? "btn-custom-green" : "btn-disable"
                                    } m-r-10`}
                                    type="submit"
                                    id="leave_comment"
                                    disabled={isValid ? false : true}
                                    onClick={(e)=>{
                                        setFieldValue('leaveComment',true);
                                    }}
                                >
                                    Update
                                </button>

                                <button
                                    onClick={this.handleClose}
                                    className={`btn-line`}
                                    type="button"
                                >
                                    Close
                                </button>
                            </Modal.Footer>
                            </Form>
                        );
                        }}
                    </Formik>
                    </Modal>
                </>
            );

    }
}

export default TranslateCommentPopup;