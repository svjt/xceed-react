import React, { Component } from "react";
import { Row, Col, ButtonToolbar, Button, Modal, Alert } from "react-bootstrap";

//import { FilePond } from 'react-filepond';
//import 'filepond/dist/filepond.min.css';
import Dropzone from "react-dropzone";
import Autosuggest from "react-autosuggest";

import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import dateFormat from "dateformat";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";
import API from "../../shared/axios";
import { showErrorMessageFront } from "../../shared/handle_error_front";

import swal from "sweetalert";

import { getMyId, localDate, trimString, htmlDecode, displaySoldTo } from "../../shared/helper";

//import uploadAxios from 'axios';

// import TinyMCE from "react-tinymce";
import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
import CustomerSoldToPopup from './CustomerSoldToPopup';

const newInitialValues = {};

const initialValues = {
  first_name: "",
  last_name: "",
  company_name: "",
  email: "",
  phone_no: "",
  status: "",
  vip_customer: "",
  fa_access: "",
  country_id: "",
  language_code: "",
  teams: "",
  customer_type: "BOTH",
};

class ReviewRequestPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPoke: false,
      files: [],
      sla_breach: false,
      showEditCustomer: false,
      showModalLoader: false,
      acceptNewEmp: false,
      acceptNewEmpMsg: "",
      filesHtml: "",
      customer_id: 0,
      assign_id: 0,
      reason: null,
      customerTeam: [],
      customers: [],
      customerDetails: [],
      countryList: [],
      languageList: [],
      companyList: [],
      productList: [],
      selectedTeamList: [],
      selectedRoleList: [],
      selectedTeams: {},
      selectedRole: {},
      isLoading: true,
      customerflagId: 0,
      showModal: false,
      teamPopup: false,
      teamList: [],
      selectStatus: [
        { id: "0", name: "Inactive" },
        { id: "1", name: "Active" },
        { id: "3", name: "Dummy" },
      ],
      vipStat: [
        { id: "0", name: "No" },
        { id: "1", name: "Yes" },
      ],
      faAccess: [
        { id: "0", name: "No" },
        { id: "1", name: "Yes" },
      ],
      isAdmin: false,
      isAdminType: [
        { id: "0", name: "No" },
        { id: "1", name: "Yes" },
      ],
      roleType: [
        { id: "1", name: "Admin" },
        { id: "2", name: "Regular" },
      ],
      employee_id: '',
      value: "", // for auto-suggest
      suggestions: [], // for auto-suggest
      company_has_soldTo: '',
      showCustomerSoldToPopup: false,
    };
  }

  componentDidMount = () => {
    this.setupPopup();
  }

  componentDidUpdate = (previousProps, previousState) => {
    if (previousProps.customer_id != this.props.customer_id || previousProps.assign_id != this.props.assign_id) {
      this.setupPopup();
    }
  };

  setupPopup = () => {
    if (
      this.props.showModal === true &&
      this.props.customer_id > 0 &&
      this.props.assign_id > 0
    ) {

      API.get(`/api/customers/view_customer_approval/${this.props.customer_id}`)
        .then((res) => {
          if (res.data.data) {
            // console.log("heree------",this.props.showModal)
            this.setState({ userData: res.data.data, showModal: true, customer_id: this.props.customer_id });
          }

          API.post(`/api/employees/get_approval_assign/`, { assign_id: this.props.assign_id })
            .then((res) => {
              if (res.data.status) {
                var employee = [];
                for (let index = 0; index < res.data.data.length; index++) {
                  const element = res.data.data[index];
                  employee.push({
                    value: element["employee_id"],
                    label: htmlDecode(element["employee_name"]),
                  });
                }
                this.setState({
                  employeeData: employee,
                  assignTo: res.data.assign_to,
                  showassignModal: true,
                  assign_id: this.props.assign_id
                });

                newInitialValues.assign_id = this.props.assign_id;
              } else {
                swal({
                  closeOnClickOutside: false,
                  title: "Error",
                  text: "Unable to assign to others!!!",
                  icon: "danger",
                }).then(() => {
                  window.location.reload();
                });
              }
              this.setState({ showModalLoader: false });
            })
            .catch((err) => {
              showErrorMessageFront(err, 2, this.props);
            });

          this.setState({ showModalLoader: false });
        })
        .catch((err) => {
          showErrorMessageFront(err, 2, this.props);
        });
    }
  }

  //EDIT POPUP
  reloadCustomerDetails = () => {
    API.get(`/api/customers/view_customer_approval/${this.state.customer_id}`)
      .then((res) => {
        if (res.data.data) {
          this.setState({ userData: res.data.data });
        }
      })
      .catch((err) => {
        showErrorMessageFront(err, 2, this.props);
      });
  }


  getIndividualCustomer(id) {
    API.get(`/api/customers/employee/${id}`)
      .then((res) => {
        this.setState({
          customerDetails: res.data.data,
          value: res.data.data.company_name
        });
        this.checkCompanySoldto(res.data.data.company_name);
      })
      .catch((err) => {
        showErrorMessageFront(err, this.props);
      });
  }

  handleClose = () => {
    var close = {
      showModalLoader: false,
      showModal: false,
      showCustomerApproval: false,
      reason: null,
      customer_id: 0,
      assign_id: 0
    };
    this.setState(close);
    this.props.handleClose(close);
  };

  handleSubmit = (values, actions) => {
    this.setState({ showModalLoader: true });

  };

  approveCustomer = (assign_id) => {
    //alert(assign_id)
    swal({
      closeOnClickOutside: false,
      title: "Customer Approval Request",
      text: "Are you sure you want to approved this user ?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.setState({ showModalLoader: true });
        API.post(`/api/employees/approve_customer_approval/`, { assign_id: assign_id })
          .then((res) => {
            swal({
              closeOnClickOutside: false,
              title: "Success",
              text: "User successfully approved.",
              icon: "success",
            }).then(() => {
              this.setState({ showModalLoader: false });
              //this.getApprovalCustomers(1);
              window.location.reload();
            });
          })
          .catch((err) => {
            showErrorMessageFront(err, 2, this.props);
          });
      }

    });
  };

  setEmployee = (event, setFieldValue) => {
    if (event && Object.keys(event).length > 0) {
      setFieldValue("employee_id", event);
      this.setState({ employee_id: event });
    } else {
      setFieldValue("employee_id", "");
      this.setState({ employee_id: "" });
    }
  };

  handleSubmitCustomerAssign = (values, actions) => {

    var err_cnt = 0;

    if (values.assign_to == 'employee' && (values.employee_id === undefined || values.employee_id === "")) {
      err_cnt++;
      actions.setErrors({ employee_id: "Please select employee." });
      actions.setSubmitting(false);
      this.setState({ createCustomerLoader: false });
    }

    if (err_cnt == 0) {

      this.setState({ createCustomerLoader: true });

      if (values.assign_to == 'approve') {
        this.setState({ showModalLoader: true });
        API.post(`/api/employees/approve_customer_approval/`, { assign_id: values.assign_id })
          .then((res) => {
            swal({
              closeOnClickOutside: false,
              title: "Success",
              text: "User successfully approved.",
              icon: "success",
            }).then(() => {
              this.setState({ showModalLoader: false, showModal : false });
              //this.getApprovalCustomers(1);
              window.location.reload();
            });
          })
          .catch((err) => {
            let erms = {};
            erms.data = err.data.errors;
            showErrorMessageFront(erms, 2, this.props);
          });
      } else {

        let post_data = {
          assign_id: values.assign_id,
          assign_to: values.assign_to,
        }
        if (values.assign_to == 'employee') {
          post_data.employee_id = values.employee_id.value;
        }
        //console.log(post_data);

        API.post(`/api/employees/set_approval_assign/`, post_data)
          .then((res) => {
            swal({
              closeOnClickOutside: false,
              title: "Success",
              text: `User successfully assigned to ${values.assign_to}.`,
              icon: "success",
            }).then(() => {
              this.setState({ createCustomerLoader: false });
              window.location.reload();
            });
          })
          .catch((err) => {
            let erms = {};
            erms.data = err.data.errors;
            showErrorMessageFront(erms, 2, this.props);
          });
      }
    }
  };

  modalCloseHandler = () => {
    this.setState({ customerflagId: 0 });
    this.setState({ showEditCustomer: false, isAdmin: false, is_admin: "", company_has_soldTo: '' });
  };

  getCustomerTeam(id, callBack) {
    var selMyTeam = [];
    var selTeams = [];
    var selMyRole = [];
    var selRole = [];
    var selMyProducts = [];
    var selProducts = [];
    API.get(`/api/customers/employee/${id}`)
      .then((res) => {
        for (let index = 0; index < res.data.data.teams.length; index++) {
          const element = res.data.data.teams[index];
          selMyTeam.push({
            value: element["employee_id"],
            label:
              element["first_name"] +
              " " +
              element["last_name"] +
              " (" +
              element["desig_name"] +
              ")",
          });
          selTeams.push(element["employee_id"]);
        }

        for (let index = 0; index < res.data.data.role.length; index++) {
          const element = res.data.data.role[index];
          selMyRole.push({
            value: element["role_id"],
            label: element["role_name"],
          });
          selRole.push(element["role_id"]);
        }

        for (let index = 0; index < res.data.data.products.length; index++) {
          const element = res.data.data.products[index];
          selMyProducts.push({
            value: element["product_id"],
            label: htmlDecode(element["product_name"]),
          });
          selProducts.push(element["product_id"]);
        }

        if (selRole[0] === 1) {
          this.setState({
            selectedTeamList: selMyTeam,
            selectedTeams: selTeams,
            selectedRoleList: "",
            selectedRole: "",
            selectedProductsList: selMyProducts,
            selectedProducts: selProducts,
            is_admin: 1,
            dnd: res.data.data.dnd,
            ignore_spoc: res.data.data.ignore_spoc,
            multi_lang_access: res.data.data.multi_lang_access,
            isAdmin: false,
          });
        } else {
          this.setState({
            selectedTeamList: selMyTeam,
            selectedTeams: selTeams,
            selectedRoleList: selMyRole,
            selectedRole: selRole,
            selectedProductsList: selMyProducts,
            selectedProducts: selProducts,
            is_admin: 0,
            dnd: res.data.data.dnd,
            ignore_spoc: res.data.data.ignore_spoc,
            multi_lang_access: res.data.data.multi_lang_access,
            isAdmin: true,
          });
        }
        // console.log(this.state);
        callBack();
      })
      .catch((err) => {
        showErrorMessageFront(err, this.props);
      });
  }

  roleList() {
    API.get("/api/customers/role_all")
      .then((res) => {
        var myRole = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          myRole.push({
            value: element["role_id"],
            label: element["role_name"],
          });
        }
        this.setState({
          roleList: myRole,
        });
      })
      .catch((err) => {
        showErrorMessageFront(err, this.props);
      });
  }

  enterPressed = (event) => {
    var code = event.keyCode || event.which;
    if (code === 13) {
      //13 is the enter keycode

      this.setState({
        searchkey: event.target.value,
      });
      //this.getRequests(1, event.target.value);
    }
  };

  onReferenceChange = (event, { newValue }, setFieldValue) => {
    this.setState({ value: newValue }, () =>
      setFieldValue("company_name", newValue)
    );
  };

  onSuggestionsFetchRequested = ({ value, reason }) => {
    const inputValue = value.toLowerCase();
    const inputLength = inputValue.length;

    if (inputLength === 0) {
      return [];
    } else {
      if (inputLength > 0) {
        this.checkCompanySoldto(value);
        API
          .get(`/api/employees/company-search/${value}`)
          .then((res) => {
            this.setState({
              suggestions: res.data.data,
            });
            return this.state.suggestions;
          })
          .catch((err) => {
            console.log("==========", err);
          });
      }
    }
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
    });
  };

  setSuggesstionValue = (suggestion, setFieldValue) => {
    setFieldValue("company_name", suggestion.company_name);
    this.checkCompanySoldto(suggestion.company_name);
    return suggestion.company_name;
  };

  checkCompanySoldto = (company_name) => {
    API
      .get(`/api/company/employee/company_has_soldto/${company_name}`)
      .then((res) => {
        if (res.data && Object.keys(res.data).length > 0) {
          const { company_id, has_soldto } = res.data;
          this.setState({ company_has_soldTo: has_soldto, company_id: company_id });
        } else {
          this.setState({ company_has_soldTo: '', company_id: '' });
        }
      })
      .catch((err) => {
        this.setState({ company_has_soldTo: '' });
        console.log("==========", err);
      });
  }

  renderSuggestion = (suggestion) => {
    return (
      <div>
        <p style={{ cursor: "pointer" }}>{suggestion.company_name}</p>
      </div>
    );
  };

  onChangeAdminType = (event) => {
    if (event.target.value === "0") {
      this.setState({ isAdmin: true });
    } else {
      this.setState({ isAdmin: false });
      this.setState({ selectedRoleList: "", selectedRole: "" });
    }
  };

  getCountryTeam = (country_id) => {
    API.get(`/api/customers/employee/country_team/${country_id}`)
      .then((res) => {
        if (res.data && res.data.data && res.data.data.length > 0) {
          const { emp_bm, emp_cscc, emp_csct, emp_ra } = res.data.data[0];
          const team_ids = [emp_bm, emp_cscc, emp_csct, emp_ra];
          const { teamList } = this.state;
          if (teamList && teamList.length > 0) {
            const country_team = [];
            const country_team_ids = [];
            team_ids.forEach(id => {
              const team = teamList.find((team) => id && team.value == id);
              if (team && Object.keys(team).length > 0) {
                country_team.push(team);
                country_team_ids.push(team.value);
              }
            });
            this.setState({ selectedTeams: country_team_ids, selectedTeamList: country_team });
          }
        }
      })
      .catch((err) => {
        showErrorMessageFront(err, this.props);
      });
  }

  setCountry = (event, setFieldValue) => {
    const { value } = event.target;
    if (value) {
      this.getCountryTeam(value);
      setFieldValue("country_id", value);
      this.setState({ country_id: value });
    } else {
      setFieldValue("country_id", "");
      this.setState({ country_id: "" });
    }
  };

  selectTeam = (event, setFieldValue) => {
    if (event && (event.length === 0 || event === null)) {
      setFieldValue("teams", []);
      this.setState({ selectedTeamList: [], selectedTeams: [] });
    } else {
      let teams = [];
      let teamIds = [];
      event.forEach(team => {
        teams.push(team);
        teamIds.push(team.value);
      });
      setFieldValue("teams", teams);
      this.setState({ selectedTeamList: teams, selectedTeams: teamIds });
    }
  };

  getProductList() {
    API.get("/api/feed/adm_products")
      .then((res) => {
        var prodArr = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          prodArr.push({
            value: element["product_id"],
            label: htmlDecode(element["product_name"]),
          });
        }
        this.setState({
          productList: prodArr,
        });
      })
      .catch((err) => {
        showErrorMessageFront(err, this.props);
      });
  }

  getCountryList() {
    API.get("/api/customers/employee/country")
      .then((res) => {
        this.setState({
          countryList: res.data.data,
        });
      })
      .catch((err) => {
        showErrorMessageFront(err, this.props);
      });
  }

  getLanguageList() {
    API.get("/api/feed/language")
      .then((res) => {
        this.setState({
          languageList: res.data.data,
        });
      })
      .catch((err) => {
        showErrorMessageFront(err, this.props);
      });
  }

  getEmployeeList() {
    API.get("/api/employees/emp_all")
      .then((res) => {
        var myTeam = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          if (element["desig_name"] == 'SPOC') {
            console.log(element["lang_code"]);
            let prefix = '';
            if (element["lang_code"] === 'zh') {
              prefix = 'M-'
            } else if (element["lang_code"] === 'es') {
              prefix = 'S-'
            } else if (element["lang_code"] === 'pt') {
              prefix = 'P-'
            } else if (element["lang_code"] === 'ja') {
              prefix = 'J-'
            }
            myTeam.push({
              value: element["employee_id"],
              label:
                element["first_name"] +
                " " +
                element["last_name"] +
                " (" +
                prefix +
                element["desig_name"] +
                ")",
            });
          } else {
            myTeam.push({
              value: element["employee_id"],
              label:
                element["first_name"] +
                " " +
                element["last_name"] +
                " (" +
                element["desig_name"] +
                ")",
            });
          }

        }
        this.setState({
          teamList: myTeam,
        });
      })
      .catch((err) => {
        showErrorMessageFront(err, this.props);
      });
  }

  handleShowCustomerSoldToPopup = () => {
    this.setState({ showCustomerSoldToPopup: true });
  }

  handleCloseCustomerSoldToPopup = () => {
    const company_name = this.state.value;
    this.checkCompanySoldto(company_name);
    this.setState({ showCustomerSoldToPopup: false });
  }


  handleSubmitEvent = (values, actions) => {
    var role_type;
    if (values.role_type > 0) {
      role_type = values.role_type;
    } else {
      role_type = "0";
    }

    const post_data = {
      first_name: values.first_name,
      last_name: values.last_name,
      country_id: values.country_id,
      language_code: values.language_code,
      company_name: values.company_name,
      status: this.state.customerDetails.status.toString(),
      email: values.email,
      vip_customer: values.vip_customer,
      fa_access: "0",
      phone_no: values.phone_no,
      teams: this.state.selectedTeams,
      products: [],
      sap_ref: values.sap_ref,
      customer_type: values.customer_type,
      is_admin: values.is_admin,
      role: values.role,
      role_type: role_type,
      dnd: values.dnd,
      ignore_spoc: "2",
      multi_lang_access: values.multi_lang_access
    };

    this.setState({ showModalLoader: true });
    const id = this.state.customerflagId;
    API.put(`/api/customers/edit_customer_by_employee/${id}`, post_data)
      .then((res) => {
        this.modalCloseHandler();
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: "Record updated successfully.",
          icon: "success",
        }).then(() => {
          if (this.state.selectedTeams.includes(getMyId())) {
            this.reloadCustomerDetails();
          } else {
            let ret_arr = { showModal: false, customer_id: 0, assign_id: 0 };
            this.props.reloadApprovalTable();
            this.setState(ret_arr);
            this.props.handleClose(ret_arr);
          }
          this.setState({ showModalLoader: false });
        });
      })
      .catch((err) => {
        this.setState({ showModalLoader: false });
        if (err.data.status === 3) {
          this.setState({
            showModal: false,
          });
          showErrorMessageFront(err, this.props);
        } else {
          actions.setErrors(err.data.errors);
          actions.setSubmitting(false);
        }
      });
  };


  render() {

    const { customerDetails, value, suggestions, selectedTeams } = this.state;

    const newInitialValuesCustomer = Object.assign(initialValues, {
      first_name: customerDetails.first_name
        ? htmlDecode(customerDetails.first_name)
        : "",
      last_name: customerDetails.last_name
        ? htmlDecode(customerDetails.last_name)
        : "",
      company_name: customerDetails.company_name ? htmlDecode(customerDetails.company_name) : "",
      email: customerDetails.email ? htmlDecode(customerDetails.email) : "",
      phone_no: customerDetails.phone_no
        ? htmlDecode(customerDetails.phone_no)
        : "",
      sap_ref: customerDetails.sap_ref
        ? htmlDecode(customerDetails.sap_ref)
        : "",
      status:
        customerDetails.status || +customerDetails.status === 0
          ? customerDetails.status.toString()
          : "",
      vip_customer:
        customerDetails.vip_customer || +customerDetails.vip_customer === 0
          ? customerDetails.vip_customer.toString()
          : "",
      fa_access:
        customerDetails.new_feature_fa || +customerDetails.new_feature_fa === 0
          ? customerDetails.new_feature_fa.toString()
          : "",
      country_id: customerDetails.country_id
        ? customerDetails.country_id.toString()
        : "",
      language_code: customerDetails.language_code
        ? customerDetails.language_code.toString()
        : "",

      company_id: customerDetails.company_id
        ? customerDetails.company_id.toString()
        : "",
      teams: this.state.selectedTeamList ? this.state.selectedTeamList : "",
      // customer_type: customerDetails.customer_type
      //   ? customerDetails.customer_type.toString()
      //   : "",
      is_admin:
        this.state.is_admin || +this.state.is_admin === 0
          ? this.state.is_admin.toString()
          : "",
      dnd:
        this.state.dnd || +this.state.dnd === 0
          ? this.state.dnd.toString()
          : "2",
      ignore_spoc:
        this.state.ignore_spoc || +this.state.ignore_spoc === 0
          ? this.state.ignore_spoc.toString()
          : "2",
      multi_lang_access:
        this.state.multi_lang_access || +this.state.multi_lang_access === 0
          ? this.state.multi_lang_access.toString()
          : "2",
      role: this.state.selectedRole ? this.state.selectedRole : "[]",
      role_type: customerDetails.role_type
        ? customerDetails.role_type.toString()
        : "",
      products: this.state.selectedProducts
        ? this.state.selectedProducts
        : "[]",
    });

    const validateStopFlag = Yup.object().shape({
      first_name: Yup.string()
        .trim()
        .required("Please enter first name")
        .min(1, "First name can be minimum 1 characters long")
        /*.matches(/^[A-Za-z0-9\s]*$/, "Invalid first name format! Only alphanumeric and spaces are allowed")*/

        .max(30, "First name can be maximum 30 characters long"),
      last_name: Yup.string()
        .trim()
        .required("Please enter last name")
        .min(1, "Last name can be minimum 1 characters long")
        /*.matches(/^[A-Za-z0-9\s]*$/, "Invalid last name format! Only alphanumeric and spaces are allowed")*/

        .max(30, "Last name can be maximum 30 characters long"),
      email: Yup.string()
        .trim()
        .required("Please enter email")
        .email("Invalid email")
        .max(80, "Email can be maximum 80 characters long"),
      phone_no: Yup.string()
        .trim()
        .required("Please enter phone number")
        .max(30, "Phone number cannot be more than 30 characters long"),
      country_id: Yup.string().trim().required("Please select country"),
      language_code: Yup.string().trim().required("Please select language"),
      // company_id: Yup.string().trim().required("Please select customer"),
      company_name: Yup.string().trim().required("Please select customer"),
      vip_customer: Yup.string()
        .trim()
        .required("Please select option")
        .matches(/^[0|1]$/, "Invalid option selected"),
      teams: Yup.array()
        .of(
          Yup.object().shape({
            label: Yup.string(),
            value: Yup.string()
          })
        )
        .test("teamSelected", "Please add at least one team.", () => this.state.selectedTeamList && this.state.selectedTeamList.length > 0),
      customer_type: Yup.string()
        .trim()
        .required("Please select the type of user"),
      is_admin: Yup.string()
        .trim()
        .required("Please select is admin")
        .matches(/^[0|1]$/, "Invalid admin selected"),
      dnd: Yup.string()
        .trim()
        .required("Please select status for do not disturb")
        .matches(/^[1|2]$/, "Invalid status for do not disturb"),
      multi_lang_access: Yup.string()
        .trim()
        .required("Please select Multi Language Access")
        .matches(/^[1|2]$/, "Invalid selection for  Multi Language Access")
      /* role: Yup.array().when('is_admin',
        {
          is: "0",
          then: Yup.array().ensure().min(1, "Please add at least one role.")
            .of(Yup.string().ensure().required("Role cannot be empty"))
        }),
      role_type: Yup.string().when('is_admin',
        {
          is: "0",
          then: Yup.string().required("Please select is role type")
        }), */
    });

    return (
      <>
        <Modal
          show={this.state.showModal}
          onHide={() => this.handleClose()}
          backdrop="static"
          className="reviewRequestPopup"
        >
          <Formik
            initialValues={newInitialValues}
            onSubmit={this.handleSubmitCustomerAssign}
          >
            {({
              values,
              errors,
              touched,
              isValid,
              isSubmitting,
              handleChange,
              setFieldValue,
              setFieldTouched,
              setErrors,
            }) => {
              return (
                <Form encType="multipart/form-data">
                  {this.state.showModalLoader === true ? (
                    <div className="loderOuter">
                      <div className="loader">
                        <img src={loaderlogo} alt="logo" />
                        <div className="loading">Loading...</div>
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                  <Modal.Header closeButton>
                    <Modal.Title>
                      Review Request
                    </Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <div className="contBox">

                      <button
                        className={`btn link-btn text-right`}
                        type="button"
                        onClick={(e) => {
                          let id = this.state.customer_id;
                          this.setState({ customerflagId: id });
                          this.getIndividualCustomer(id);
                          this.getEmployeeList();
                          this.roleList();
                          this.getCountryList();
                          this.getLanguageList();
                          this.getCustomerTeam(id, () => this.setState({ showEditCustomer: true }));
                        }}
                      >
                        Edit
                      </button>

                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <table className="table table-bordered">
                              <tr>
                                <td>User Name</td>
                                <td>{this.state.userData.first_name} {this.state.userData.last_name}</td>
                              </tr>
                              <tr>
                                <td>Customer Name</td>
                                <td>{this.state.userData.company_name}</td>
                              </tr>
                              <tr>
                                <td>Email</td>
                                <td>{this.state.userData.email}</td>
                              </tr>
                              <tr>
                                <td>Phone no</td>
                                <td>{this.state.userData.phone_no}</td>
                              </tr>
                              <tr>
                                <td>Country</td>
                                <td>{this.state.userData.country_name}</td>
                              </tr>
                              <tr>
                                <td>Language</td>
                                <td>{this.state.userData.language}</td>
                              </tr>

                              <tr>
                                <td>Key Account</td>
                                <td>{(this.state.userData.vip_customer == 1) ? 'Yes' : 'No'}</td>
                              </tr>
                              {/* <tr>
                                <td>Status</td>
                                <td>{(this.state.userData.status == 1) ? 'Active' : 'Inactive'}</td>
                              </tr> */}
                              {this.state.userData.customer_type != '' && this.state.userData.customer_type != null && (
                                <tr>
                                  <td>User Type</td>
                                  <td>{this.state.userData.customer_type}</td>
                                </tr>
                              )}
                              <tr>
                                <td>Assign Team</td>
                                <td>
                                  {this.state.userData.teams && this.state.userData.teams.map((answer, i) => {
                                    return (<p>{answer.first_name} {answer.last_name} ({answer.desig_name}) </p>)
                                  })}
                                </td>
                              </tr>
                              <tr>
                                <td>Enable Super admin Role</td>
                                <td>{(this.state.userData.user_is_admin == 1 && this.state.userData.role_type == 0) ? 'Yes' : 'No'}</td>
                              </tr>
                              <tr>
                                <td>Disable Notifications</td>
                                <td>{(this.state.userData.dnd == 1) ? 'Yes' : 'No'}</td>
                              </tr>
                              <tr>
                                <td>Multi Language Access</td>
                                <td>{(this.state.userData.multi_lang_access == 1) ? 'Yes' : 'No'}</td>
                              </tr>
                              {this.state.userData.role_type != 0 && (
                                <>
                                  <tr>
                                    <td>Role</td>
                                    <td>{this.state.userData.role && this.state.userData.role.map((rl, j) => {
                                      return (<p>{rl.role_name} </p>)
                                    })}</td>
                                  </tr>
                                  <tr>
                                    <td>Role Type</td>
                                    <td>{(this.state.userData.role_type == 1) ? 'Admin' : 'Regular'}</td>
                                  </tr>
                                </>
                              )}
                            </table>
                          </div>
                        </Col>
                      </Row>

                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <button
                            className={`btn btn-fill approveBtn`}
                            type="submit"
                            onClick={(e) => {
                              setFieldValue("assign_to", "approve");
                            }}
                            disabled={this.state.employee_id != ''? true : false}
                          >
                            Approve
                          </button>
                        </Col>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group mb-10">
                            <label>
                              Assign to Employee{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              name="employee_id"
                              isClearable
                              isSearchable
                              options={this.state.employeeData}
                              value={this.state.employee_id}
                              isDisabled={this.state.assignTo == 'others' ? false : true}
                              onChange={(e) => {
                                this.setEmployee(e, setFieldValue);
                              }}
                            />
                            {(errors.employee_id || touched.employee_id) ? (
                              <span className="errorMsg">{errors.employee_id}</span>
                            ) : null}
                          </div>
                        </Col>
                        <Col xs={12} sm={12} md={12}>
                          <button
                            className={`btn btn-fill m-r-10`}
                            type="submit"
                            disabled={this.state.assignTo == 'others'? false : true}
                            onClick={(e) => {
                              setFieldValue("assign_to", "employee");
                            }}
                          >
                            Assign to Selected Employee
                          </button>
                          <button
                            className={`btn btn-fill pink-button`}
                            type="submit"
                            onClick={(e) => {
                              setFieldValue("assign_to", "admin");
                            }}
                            disabled={this.state.employee_id != ''? true : false}
                          >
                            Assign back to Admin
                          </button>
                        </Col>
                      </Row>

                    </div>
                  </Modal.Body>
                  <Modal.Footer>

                  </Modal.Footer>
                </Form>
              );
            }}
          </Formik>
        </Modal>

        <Modal
          show={this.state.showEditCustomer}
          onHide={() => this.modalCloseHandler()}
          backdrop="static"
        >
          <Formik
            initialValues={newInitialValuesCustomer}
            validationSchema={validateStopFlag}
            onSubmit={this.handleSubmitEvent}
          >
            {({
              values,
              errors,
              touched,
              isValid,
              isSubmitting,
              setFieldValue,
              setFieldTouched,
              handleChange,
            }) => {
              return (
                <Form>
                  {/* {console.log({ errors })} */}
                  {this.state.showModalLoader === true ? (
                    <div className="loading_reddy_outer">
                      <div className="loading_reddy">
                        <img src={whitelogo} alt="loader" />
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                  <Modal.Header closeButton>
                    <Modal.Title>
                      {this.state.customerflagId > 0 ? "Edit" : "Add"}{" "}
                      User
                    </Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <div className="contBox">
                      <Row>
                        <Col xs={12} sm={6} md={6}>
                          <div className="form-group">
                            <label>
                              First Name
                              <span className="impField">*</span>
                            </label>
                            <Field
                              name="first_name"
                              type="text"
                              className={`form-control`}
                              placeholder="Enter first name"
                              autoComplete="off"
                            />
                            {errors.first_name &&
                              touched.first_name ? (
                              <span className="errorMsg">
                                {errors.first_name}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                        <Col xs={12} sm={6} md={6}>
                          <div className="form-group">
                            <label>
                              Last Name
                              <span className="impField">*</span>
                            </label>
                            <Field
                              name="last_name"
                              type="text"
                              className={`form-control`}
                              placeholder="Enter last name"
                              autoComplete="off"
                            />
                            {errors.last_name && touched.last_name ? (
                              <span className="errorMsg">
                                {errors.last_name}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <label>
                              Customer
                              <span className="impField">*</span>
                            </label>
                            {/* <Field
                              name="company_id"
                              component="select"
                              className={`selectArowGray form-control`}
                              autoComplete="off"
                              value={values.company_id}
                            >
                              <option key="-1" value="">
                                Select
                              </option>
                              {this.state.companyList.map(
                                (company, i) => (
                                  <option
                                    key={i}
                                    value={company.company_id}
                                  >
                                    {htmlDecode(company.company_name)}
                                  </option>
                                )
                              )}
                            </Field> */}
                            <Autosuggest
                              suggestions={suggestions}
                              onSuggestionsFetchRequested={
                                this.onSuggestionsFetchRequested
                              }
                              onSuggestionsClearRequested={
                                this.onSuggestionsClearRequested
                              }
                              getSuggestionValue={(suggestion) =>
                                this.setSuggesstionValue(
                                  suggestion,
                                  setFieldValue
                                )
                              }
                              renderSuggestion={this.renderSuggestion}
                              inputProps={{
                                placeholder: "Customer",
                                value: values.company_name,
                                type: "search",
                                onChange: (event, object) => {
                                  return this.onReferenceChange(
                                    event,
                                    object,
                                    setFieldValue
                                  );
                                },
                                onKeyPress: this.enterPressed.bind(this),
                                onBlur: () => setFieldTouched("company_name"),
                                className: "form-control customInput",
                              }}
                              name="company_name"
                            />
                            {
                              this.state.value && this.state.company_has_soldTo !== '' && displaySoldTo() ? (
                                this.state.company_has_soldTo ? (
                                  <span className="task-details-language"
                                    style={{
                                      color: "rgb(0, 86, 179)",
                                      fontWeight: "700",
                                    }} >To map new Sold To Party, click <button type="button" onClick={this.handleShowCustomerSoldToPopup}>here</button> if you choose to.</span>
                                ) : (
                                  <span className="task-details-language"
                                    style={{
                                      color: "rgb(0, 86, 179)",
                                      fontWeight: "700",
                                    }} >No Sold To Party mapped with above customer. Click <button type="button" onClick={this.handleShowCustomerSoldToPopup}>here</button> to map one if you choose to.</span>
                                )
                              ) : (null)
                            }
                            {errors.company_id ? (
                              <span className="errorMsg">
                                {errors.company_id}
                              </span>
                            ) : null}
                            {errors.company_name &&
                              touched.company_name ? (
                              <span className="errorMsg">
                                {errors.company_name}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>
                              Customer Email
                              <span className="impField">*</span>
                            </label>
                            <Field
                              name="email"
                              type="text"
                              className={`form-control`}
                              placeholder="Enter customer email"
                              autoComplete="off"
                            />
                            {errors.email && touched.email ? (
                              <span className="errorMsg">
                                {errors.email}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>
                              Phone Number
                              <span className="impField">*</span>
                            </label>
                            <Field
                              name="phone_no"
                              type="text"
                              className={`form-control`}
                              placeholder="Enter phone number"
                              autoComplete="off"
                            />
                            {errors.phone_no && touched.phone_no ? (
                              <span className="errorMsg">
                                {errors.phone_no}
                              </span>
                            ) : null}
                          </div>
                        </Col>

                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>
                              Country
                              <span className="impField">*</span>
                            </label>

                            <Field
                              name="country_id"
                              component="select"
                              className={`selectArowGray form-control`}
                              autoComplete="off"
                              value={values.country_id}
                              onChange={(e) => {
                                this.setCountry(e, setFieldValue);
                              }}
                              onBlur={() => setFieldTouched('country_id')}
                            >
                              <option key="-1" value="">
                                Select
                              </option>
                              {this.state.countryList.map(
                                (country, i) => (
                                  <option
                                    key={i}
                                    value={country.country_id}
                                  >
                                    {htmlDecode(country.country_name)}
                                  </option>
                                )
                              )}
                            </Field>
                            {errors.country_id &&
                              touched.country_id ? (
                              <span className="errorMsg">
                                {errors.country_id}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>

                      <Row>
                        <Col xs={12} sm={4} md={4}>
                          <div className="form-group">
                            <label>
                              Language
                              <span className="impField">*</span>
                            </label>

                            <Field
                              name="language_code"
                              component="select"
                              className={`selectArowGray form-control`}
                              autoComplete="off"
                              value={values.language_code}
                            >
                              <option key="-1" value="">
                                Select
                              </option>
                              {this.state.languageList.map(
                                (lang, i) => (
                                  <option
                                    key={i}
                                    value={lang.code}
                                  >
                                    {htmlDecode(lang.language)}
                                  </option>
                                )
                              )}
                            </Field>
                            {errors.language_code &&
                              touched.language_code ? (
                              <span className="errorMsg">
                                {errors.language_code}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                        <Col xs={12} sm={4} md={4}>
                          <div className="form-group">
                            <label>
                              Key Account
                              <span className="impField">*</span>
                            </label>
                            <Field
                              name="vip_customer"
                              component="select"
                              className={`selectArowGray form-control`}
                              autoComplete="off"
                              value={values.vip_customer}
                            >
                              <option key="-1" value="">
                                Select
                              </option>
                              {this.state.vipStat.map((status, i) => (
                                <option key={i} value={status.id}>
                                  {status.name}
                                </option>
                              ))}
                            </Field>
                            {errors.vip_customer &&
                              touched.vip_customer ? (
                              <span className="errorMsg">
                                {errors.vip_customer}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                        <Col xs={12} sm={4} md={4}>
                          <div className="form-group">
                            <label>
                              User Type
                              <span className="impField">*</span>
                            </label>

                            <Field
                              name="customer_type"
                              component="select"
                              className={`selectArowGray form-control`}
                              autoComplete="off"
                              value={values.customer_type}
                              disabled
                            >
                              {/* <option key="-1" value="">
                                Select
                              </option>
                              <option key="1" value="API">
                                API
                              </option>
                              <option key="2" value="CPS">
                                CPS
                              </option> */}
                              <option key="3" value="BOTH">
                                BOTH
                              </option>
                            </Field>
                            {errors.customer_type ? (
                              <span className="errorMsg">
                                {errors.customer_type}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                      {/* <Row>
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>SAP ref number</label>
                            <Field
                              name="sap_ref"
                              type="text"
                              className={`form-control`}
                              placeholder="Enter SAP reference"
                              autoComplete="off"
                            />
                            {errors.sap_ref && touched.sap_ref ? (
                              <span className="errorMsg">
                                {errors.sap_ref}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                        
                      </Row> */}
                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <label>
                              Assign Team
                              <span className="impField">*</span>
                            </label>

                            <Select
                              isMulti
                              name="teams"
                              options={this.state.teamList}
                              className="basic-multi-select"
                              classNamePrefix="select"
                              onChange={(evt) => this.selectTeam(evt, setFieldValue)}
                              placeholder="Teams"
                              onBlur={() => setFieldTouched("teams")}
                              value={
                                this.state.selectedTeamList
                              }
                            />
                            {errors.teams && touched.teams ? (
                              <span className="errorMsg">
                                {errors.teams}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>


                      <Row>
                        <Col xs={12} sm={4} md={4}>
                          <div className="form-group">
                            <label>
                              Is Admin
                              <span className="impField">*</span>
                            </label>
                            <Field
                              name="is_admin"
                              component="select"
                              className={`selectArowGray form-control`}
                              autoComplete="off"
                              value={values.is_admin}
                              onChange={(e) => {
                                handleChange(e);
                                this.onChangeAdminType(e);
                              }}
                            >
                              <option key="-1" value="">
                                Select
                              </option>
                              {this.state.isAdminType.map(
                                (status, i) => (
                                  <option key={i} value={status.id}>
                                    {status.name}
                                  </option>
                                )
                              )}
                            </Field>
                            {errors.is_admin && touched.is_admin ? (
                              <span className="errorMsg">
                                {errors.is_admin}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                        <Col xs={12} sm={4} md={4}>
                          <div className="form-group">
                            <label>
                              Disable Notifications
                              <span className="impField">*</span>
                            </label>
                            <Field
                              name="dnd"
                              component="select"
                              className={`selectArowGray form-control`}
                              autoComplete="off"
                              value={values.dnd}
                            >
                              <option key="3" value="">
                                Select
                              </option>
                              <option key="2" value="2">
                                No
                              </option>
                              <option key="1" value="1">
                                Yes
                              </option>
                            </Field>
                            {errors.dnd && touched.dnd ? (
                              <span className="errorMsg">
                                {errors.dnd}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                        <Col xs={12} sm={4} md={4}>
                          <div className="form-group">
                            <label>
                              Multi Language Access
                              <span className="impField">*</span>

                            </label>
                            <Field
                              name="multi_lang_access"
                              component="select"
                              className={`selectArowGray form-control`}
                              autoComplete="off"
                              value={values.multi_lang_access}
                            >
                              <option key="3" value="">
                                Select
                              </option>
                              <option key="2" value="2">
                                No
                              </option>
                              <option key="1" value="1">
                                Yes
                              </option>
                            </Field>
                            {errors.multi_lang_access && touched.multi_lang_access ? (
                              <span className="errorMsg">
                                {errors.multi_lang_access}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                      {this.state.isAdmin === true ? (
                        <div>
                          <Row>
                            <Col xs={12} sm={12} md={6}>
                              <div className="form-group">
                                <label>
                                  Role
                                  <span className="impField">*</span>
                                </label>
                                <Select
                                  isMulti
                                  name="role[]"
                                  options={this.state.roleList}
                                  className="basic-multi-select"
                                  classNamePrefix="select"
                                  onChange={(evt) =>
                                    setFieldValue(
                                      "role",
                                      [].slice
                                        .call(evt)
                                        .map((val) => val.value)
                                    )
                                  }
                                  placeholder="Role"
                                  onBlur={() =>
                                    setFieldTouched("role")
                                  }
                                  defaultValue={
                                    this.state.selectedRoleList
                                  }
                                />
                                {errors.role && touched.role ? (
                                  <span className="errorMsg">
                                    {errors.role}
                                  </span>
                                ) : null}
                              </div>
                              {/* <Field
                                  name="role"
                                  component="select"
                                  className={`selectArowGray form-control`}
                                  autoComplete="off"
                                  value={values.role}
                                >
                                  <option key="-1" value="">
                                    Select
                                  </option>
                                  {this.state.roleList.map(
                                    (role, i) => (
                                      <option
                                        key={i}
                                        value={role.role_id}
                                      >
                                        {role.role_name}
                                      </option>
                                    )
                                  )}
                                </Field> 
                                {errors.role && touched.role ? (
                                  <span className="errorMsg">
                                    {errors.role}
                                  </span>
                                ) : null}*/}
                            </Col>
                            <Col xs={12} sm={6} md={6}>
                              <div className="form-group">
                                <label>
                                  Role Type
                                  <span className="impField">*</span>
                                </label>
                                <Field
                                  name="role_type"
                                  component="select"
                                  className={`selectArowGray form-control`}
                                  autoComplete="off"
                                  value={values.role_type}
                                >
                                  <option key="-1" value="">
                                    Select
                                  </option>
                                  {this.state.roleType.map(
                                    (role_type, i) => (
                                      <option
                                        key={i}
                                        value={role_type.id}
                                      >
                                        {role_type.name}
                                      </option>
                                    )
                                  )}
                                </Field>
                                {errors.role_type &&
                                  touched.role_type ? (
                                  <span className="errorMsg">
                                    {errors.role_type}
                                  </span>
                                ) : null}
                              </div>
                            </Col>
                          </Row>
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                  </Modal.Body>
                  <Modal.Footer>
                    <button
                      className={`btn btn-fill ${isValid ? "btn-fill" : "btn-disable"
                        }`}
                      type="submit"
                      disabled={isValid ? false : true}
                    >
                      {this.state.customerflagId > 0
                        ? isSubmitting
                          ? "Updating..."
                          : "Update"
                        : isSubmitting
                          ? "Submitting..."
                          : "Submit"}
                    </button>
                    {/* <button
                      onClick={(e) => this.modalCloseHandler()}
                      className={`btn btn-danger btn-sm`}
                      type="button"
                    >
                      Close
                    </button> */}
                  </Modal.Footer>
                </Form>
              );
            }}
          </Formik>
        </Modal>
        {
          this.state.showCustomerSoldToPopup && (
            <CustomerSoldToPopup
              showCustomerSoldToPopup={this.state.showCustomerSoldToPopup}
              handleCloseCustomerSoldToPopup={this.handleCloseCustomerSoldToPopup}
              company_id={this.state.company_id}
              company_name={this.state.value}
            />
          )
        }
      </>
    );
  }
}

export default ReviewRequestPopup;
