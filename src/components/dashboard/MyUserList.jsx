import React, { Component } from "react";
import { getMyDrupalId } from "../../shared/helper";
import base64 from "base-64";
import "../../assets/css/MyCustomer.css";
import API from "../../shared/axios";
import commenLogo from "../../assets/images/image-edit.jpg";
import Select from "react-select";
import jsonpath from "jsonpath";
import { htmlDecode } from "../../shared/helper";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";

import searchIcon from "../../assets/images/search-icon.svg";
import userCalender from "../../assets/images/user-calender-icon.svg";
import useremail from "../../assets/images/email-calender-icon.svg";
import { showErrorMessageFront } from "../../shared/handle_error_front";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Modal,
  Alert,
  Tooltip,
  OverlayTrigger,
} from "react-bootstrap";

const portal_url = `${process.env.REACT_APP_PORTAL_URL}`; // SATYAJIT

class MyCustomer extends Component {
  state = {
    vipBlock: [],
    normalBlock: [],
    agenBlock: [],
    searchInitiated: false,
    apiCallDone: false,
    showModal: false,
    searchEmail:''
  };

  constructor(props) {
    super(props);
    this.textInput = React.createRef();
  }

  focusTextInput = () => {
    if (this.textInput.current.value) {
      let keyword = this.textInput.current.value;
      this.setState({
        vipBlock: this.state.vipBlock.filter((customer, m) => {
          return (
            customer.first_name === keyword || customer.last_name === keyword
          );
        }),
        normalBlock: this.state.normalBlock.filter((customer, m) => {
          return (
            customer.first_name === keyword || customer.last_name === keyword
          );
        }),
      });
    }
    //console.log(this.state.vipBlock)
    //console.log(this.state.normalBlock)
    //this.textInput.current.focus();
  };

  componentDidMount = () => {
    //console.log('company');
    var company_id = this.props.match.params.company_id;
    // console.log(company_id);
    if (company_id > 0) {
      API.get("/api/employees/company")
        .then((res) => {
          var myCompany = [];
          var selVal = {};
          var redirect = true;
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            if (parseInt(element["company_id"]) === parseInt(company_id)) {
              selVal = {
                value: element["company_id"],
                label: htmlDecode(element["company_name"]),
              };
              redirect = false;
            }
            myCompany.push({
              value: element["company_id"],
              label: htmlDecode(element["company_name"]),
            });
          }

          if (redirect === true) {
            this.props.history.push("/");
          }

          this.setState({ companyArr: myCompany, defVal: selVal });

          API.get(`/api/employees/active_customers/${company_id}`)
            .then((res) => {
              this.setState({
                vipBlock: res.data.data.filter((customer, m) => {
                  return customer.vip_customer === 1;
                }),
                normalBlock: res.data.data.filter((customer, m) => {
                  return customer.vip_customer === 0;
                }),
              });

              API.get(`/api/agents/company/${company_id}`)
                .then((res) => {
                  this.setState({
                    agenBlock: res.data.data,
                  });
                  this.setState({ oldState: this.state });
                  

                  if(this.props.location.state.email && this.props.location.state.email != ''){
                    this.setState({apiCallDone: true},this.initialSearchEmail(this.props.location.state.email));
                  }else{
                    this.setState({ apiCallDone: true });
                  }
                  
                })
                .catch((err) => {
                  console.log(err);
                });
            })
            .catch((err) => {
              console.log(err);
            });
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      this.props.history.push("/");
    }
  };

  searchCustomer = (e) => {
    e.preventDefault();
    // console.log(e);
  };

  changeCompany = (event) => {
    if (event === null) {
      //console.log('here');
      this.setState({
        isHidden: true,
      });
    } else {
      if (event.value > 0) {
        this.setState({ apiCallDone: false });
        API.get(`/api/employees/active_customers/${event.value}`)
          .then((res) => {
            this.setState({
              vipBlock: res.data.data.filter((customer, m) => {
                return customer.vip_customer === 1;
              }),
              normalBlock: res.data.data.filter((customer, m) => {
                return customer.vip_customer === 0;
              }),
              defVal: event,
            });

            API.get(`/api/agents/company/${event.value}`)
              .then((res) => {
                this.setState({
                  agenBlock: res.data.data,
                });
                this.setState({ oldState: this.state });
                this.setState({ apiCallDone: true });
              })
              .catch((err) => {
                console.log(err);
              });
          })
          .catch((err) => {
            console.log(err);
          });
      } else {
        this.setState({
          vipBlock: [],
          normalBlock: [],
          agenBlock: [],
        });
      }
    }
  };

  handleTabs = (event) => {
    if (event.currentTarget.className === "active") {
      //DO NOTHING
    } else {
      var elems = document.querySelectorAll('[id^="tab_"]');
      var elemsContainer = document.querySelectorAll('[id^="show_tab_"]');
      var currId = event.currentTarget.id;

      for (var i = 0; i < elems.length; i++) {
        elems[i].classList.remove("active");
      }

      for (var j = 0; j < elemsContainer.length; j++) {
        elemsContainer[j].style.display = "none";
      }

      event.currentTarget.classList.add("active");
      event.currentTarget.classList.add("active");
      document.querySelector("#show_" + currId).style.display = "block";
    }
    // this.tabElem.addEventListener("click",function(event){
    //     alert(event.target);
    // }, false);
  };

  filterSearch = (event) => {
    if (event.target.value !== "") {
      var filerAgent = jsonpath.query(
        this.state.oldState,
        `$.agenBlock..[?(@ && @.first_name && /^${event.target.value}/i.test(@.first_name) )]`
      );

      //console.log('filerAgent',filerAgent);

      var filerNormal = jsonpath.query(
        this.state.oldState,
        `$.normalBlock..[?(@ && @.first_name && /^${event.target.value}/i.test(@.first_name) )]`
      );

      //console.log('filerNormal',filerNormal);

      var filerVip = jsonpath.query(
        this.state.oldState,
        `$.vipBlock..[?(@ && @.first_name && /^${event.target.value}/i.test(@.first_name) )]`
      );

      //console.log('filerVip',filerVip);
      this.setState({
        normalBlock: filerNormal,
        agenBlock: filerAgent,
        vipBlock: filerVip,
        searchInitiated: true,
      });
    } else {
      this.setState({
        normalBlock: this.state.oldState.normalBlock,
        agenBlock: this.state.oldState.agenBlock,
        vipBlock: this.state.oldState.vipBlock,
        searchInitiated: false,
      });
    }
  };

  filterSearchEmail = (event) => {
    if (event.target.value !== "") {
      var filerAgent = jsonpath.query(
        this.state.oldState,
        `$.agenBlock..[?(@ && @.email && /^${event.target.value}/i.test(@.email) )]`
      );

      //console.log('filerAgent',filerAgent);

      var filerNormal = jsonpath.query(
        this.state.oldState,
        `$.normalBlock..[?(@ && @.email && /^${event.target.value}/i.test(@.email) )]`
      );

      //console.log('filerNormal',filerNormal);

      var filerVip = jsonpath.query(
        this.state.oldState,
        `$.vipBlock..[?(@ && @.email && /^${event.target.value}/i.test(@.email) )]`
      );

      //console.log('filerVip',filerVip);
      this.setState({
        normalBlock: filerNormal,
        agenBlock: filerAgent,
        vipBlock: filerVip,
        searchInitiated: true,
        searchEmail:event.target.value
      });
    } else {
      this.setState({
        normalBlock: this.state.oldState.normalBlock,
        agenBlock: this.state.oldState.agenBlock,
        vipBlock: this.state.oldState.vipBlock,
        searchInitiated: false,
        searchEmail:''
      });
    }
  };

  initialSearchEmail = (email) => {
      var filerAgent = jsonpath.query(
        this.state.oldState,
        `$.agenBlock..[?(@ && @.email && /^${email}/i.test(@.email) )]`
      );

      //console.log('filerAgent',filerAgent);

      var filerNormal = jsonpath.query(
        this.state.oldState,
        `$.normalBlock..[?(@ && @.email && /^${email}/i.test(@.email) )]`
      );

      //console.log('filerNormal',filerNormal);

      var filerVip = jsonpath.query(
        this.state.oldState,
        `$.vipBlock..[?(@ && @.email && /^${email}/i.test(@.email) )]`
      );

      if(filerNormal.length == 0 && filerVip.length == 0 && filerAgent.length > 0){
        var elems = document.querySelectorAll('[id^="tab_"]');
        var elemsContainer = document.querySelectorAll('[id^="show_tab_"]');
        var currId = `tab_2`;

        for (var i = 0; i < elems.length; i++) {
          elems[i].classList.remove("active");
        }

        for (var j = 0; j < elemsContainer.length; j++) {
          elemsContainer[j].style.display = "none";
        }

        document.querySelector(`#${currId}`).classList.add("active");
        document.querySelector("#show_" + currId).style.display = "block";
      }

      //console.log('filerVip',filerVip);
      this.setState({
        normalBlock: filerNormal,
        agenBlock: filerAgent,
        vipBlock: filerVip,
        searchInitiated: true,
        searchEmail:email
      });
    
  };

  render() {
    if (
      this.state.vipBlock.length > 0 ||
      this.state.normalBlock.length > 0 ||
      this.state.agenBlock.length > 0 ||
      this.state.searchInitiated === true
    ) {
      return (
        <>
          <div className="content-wrapper">
            {/*<!-- Content Header (Page header) -->*/}
            <section className="content-header">
              <div className="row">
                <div className="col-lg-3 col-sm-5 col-xs-12">
                  <div className="form-group has-feedback">
                    <Select
                      className="basic-single"
                      classNamePrefix="select"
                      defaultValue={this.state.defVal}
                      isClearable={false}
                      isSearchable={true}
                      name="employee_id"
                      options={this.state.companyArr}
                      onChange={(e) => {
                        this.changeCompany(e);
                      }}
                    />
                  </div>
                </div>
                <div className="col-lg-3 col-sm-5 col-xs-12 pull-right">
                  <div className="form-group has-feedback">
                    <div className="input-group my-user-btn">
                      <input
                        className="form-control"
                        placeholder="Search by Users/Agent Name"
                        onChange={this.filterSearch}
                      />
                      <input
                        className="form-control"
                        placeholder="Search by Email"
                        onChange={this.filterSearchEmail}
                        value={this.state.searchEmail}
                      />
                      {/* <div className="input-group-btn">
                        <button
                          className="serch-btn"
                          type="button"
                          onClick={this.focusTextInput}
                        >
                          <img src={searchIcon} alt="search" />
                        </button>
                      </div> */}
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section className="content">
              <div className="nav-tabs-custom">
                <ul className="nav nav-tabs">
                  <li
                    className="active"
                    onClick={(e) => this.handleTabs(e)}
                    id="tab_1"
                  >
                    My Users
                  </li>
                  <li onClick={(e) => this.handleTabs(e)} id="tab_2">
                    {" "}
                    My Agents
                  </li>
                </ul>
                <div className="tab-content my-customer-tab-content">
                  <div className="tab-pane active" id="show_tab_1">
                    {this.state.normalBlock.length > 0 ? (
                      <div className="row">
                        {this.state.normalBlock.map((customer, j) => (
                          <NormalAccount
                            key={j}
                            index={j}
                            first_name={customer.first_name}
                            last_name={customer.last_name}
                            profile_pic={customer.profile_pic}
                            email={htmlDecode(customer.email)}
                            company_name={htmlDecode(customer.company_name)}
                            customer_id={customer.customer_id}
                            activated={customer.activated}
                            status={customer.status}
                          />
                        ))}
                      </div>
                    ) : (
                      <div className="row">
                        <div className="col-md-12">No Users Found</div>
                      </div>
                    )}
                    {this.state.vipBlock.length > 0 ? (
                      <div className="row">
                        {this.state.vipBlock.map((customer, m) => (
                          <KeyAccounts
                            key={m}
                            index={m}
                            first_name={customer.first_name}
                            last_name={customer.last_name}
                            email={htmlDecode(customer.email)}
                            company_name={htmlDecode(customer.company_name)}
                            profile_pic={customer.profile_pic}
                            customer_id={customer.customer_id}
                            activated={customer.activated}
                            status={customer.status}
                          />
                        ))}
                      </div>
                    ) : (
                      <div className="row">
                        <div className="col-md-12">No Key Users Found</div>
                      </div>
                    )}
                  </div>
                  <div className="tab-pane" id="show_tab_2">
                    {this.state.agenBlock.length > 0 ? (
                      <div className="row">
                        {this.state.agenBlock.map((agent, k) => (
                          <AgentAccount
                            key={k}
                            index={k}
                            first_name={agent.first_name}
                            profile_pic={agent.profile_pic}
                            last_name={agent.last_name}
                            email={htmlDecode(agent.email)}
                            customer_id={agent.agent_id}
                          />
                        ))}
                      </div>
                    ) : (
                      <div className="row">
                        <div className="col-md-12">No Agents Found</div>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </section>
          </div>
        </>
      );
    } else {
      if (this.state.apiCallDone === true) {
        return (
          <>
            <>
              <div className="content-wrapper">
                {/*<!-- Content Header (Page header) -->*/}
                <section className="content-header">
                  <div className="row">
                    <div className="col-lg-3 col-sm-5 col-xs-12">
                      <div className="form-group has-feedback">
                        <Select
                          className="basic-single"
                          classNamePrefix="select"
                          defaultValue={this.state.defVal}
                          isClearable={false}
                          isSearchable={true}
                          name="employee_id"
                          options={this.state.companyArr}
                          onChange={(e) => {
                            this.changeCompany(e);
                          }}
                        />
                      </div>
                    </div>
                    <div className="col-lg-3 col-sm-5 col-xs-12 pull-right">
                      <div className="form-group has-feedback">
                        <div className="input-group my-user-btn">
                          <input
                            className="form-control"
                            placeholder="Search by Users/Agent Name"
                            onChange={this.filterSearch}
                          />
                          <input
                            className="form-control"
                            placeholder="Search by Email"
                            value={this.state.searchEmail}
                            onChange={this.filterSearchEmail}
                          />
                          {/* <div className="input-group-btn">
                            <button
                              className="serch-btn"
                              type="button"
                              onClick={this.focusTextInput}
                            >
                              <img src={searchIcon} alt="search" />
                            </button>
                          </div> */}
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                <section className="content">
                  <div className="nav-tabs-custom">
                    <ul className="nav nav-tabs">
                      <li
                        className="active"
                        onClick={(e) => this.handleTabs(e)}
                        id="tab_1"
                      >
                        My Users
                      </li>
                      <li onClick={(e) => this.handleTabs(e)} id="tab_2">
                        {" "}
                        My Agents
                      </li>
                    </ul>
                    <div className="tab-content my-customer-tab-content">
                      <div className="tab-pane active" id="show_tab_1">
                        {this.state.normalBlock.length > 0 ? (
                          <div className="row">
                            {this.state.normalBlock.map((customer, j) => (
                              <NormalAccount
                                key={j}
                                index={j}
                                first_name={customer.first_name}
                                profile_pic={customer.profile_pic}
                                last_name={customer.last_name}
                                email={htmlDecode(customer.email)}
                                company_name={htmlDecode(customer.company_name)}
                                customer_id={customer.customer_id}
                                activated={customer.activated}
                                status={customer.status}
                              />
                            ))}
                          </div>
                        ) : (
                          <div className="row">
                            <div className="col-md-12">No Users Found</div>
                          </div>
                        )}
                        {this.state.vipBlock.length > 0 ? (
                          <div className="row">
                            {this.state.vipBlock.map((customer, m) => (
                              <KeyAccounts
                                key={m}
                                index={m}
                                first_name={customer.first_name}
                                last_name={customer.last_name}
                                email={htmlDecode(customer.email)}
                                company_name={htmlDecode(customer.company_name)}
                                profile_pic={customer.profile_pic}
                                customer_id={customer.customer_id}
                                activated={customer.activated}
                                status={customer.status}
                              />
                            ))}
                          </div>
                        ) : (
                          <div className="row">
                            <div className="col-md-12">No Key Users Found</div>
                          </div>
                        )}
                      </div>
                      <div className="tab-pane" id="show_tab_2">
                        {this.state.agenBlock.length > 0 ? (
                          <div className="row">
                            {this.state.agenBlock.map((agent, k) => (
                              <AgentAccount
                                key={k}
                                index={k}
                                first_name={agent.first_name}
                                profile_pic={agent.profile_pic}
                                last_name={agent.last_name}
                                email={htmlDecode(agent.email)}
                                customer_id={agent.agent_id}
                              />
                            ))}
                          </div>
                        ) : (
                          <div className="row">
                            <div className="col-md-12">No Agents Found</div>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </>
          </>
        );
      } else {
        return (
          <>
            <div className="loderOuter">
              <div className="loader">
                <img src={loaderlogo} alt="logo" />
                <div className="loading">Loading...</div>
              </div>
            </div>
          </>
        );
      }
    }
  }
}

class KeyAccounts extends Component {
  state = {
    showModal: false,
  };
  redirectUrl = (event, id) => {
    event.preventDefault();
    //http://reddy.indusnet.cloud/customer-dashboard?source=Mi02NTE=
    // var emp_drupal_id = getMyDrupalId(localStorage.token);
    // var base_encode   = base64.encode(`${id}-${emp_drupal_id}`);
    // window.open( portal_url + "customer-dashboard?source="+base_encode, '_blank');
    API.post(`/api/employees/shlogin`, { customer_id: parseInt(id) })
      .then((res) => {
        if (res.data.shadowToken) {
          var url =
            process.env.REACT_APP_CUSTOMER_PORTAL +
            `setToken/` +
            res.data.shadowToken;
          window.open(url, "_blank");
        }
      })
      .catch((error) => {
        showErrorMessageFront(error, 0, this.props);
      });
  };

  modalShowHandler = (event) => {
    event.preventDefault();
    this.setState({ showModal: true });
  };
  modalCloseHandler = () => {
    this.setState({ showModal: false });
  };

  render() {
    return (
      <>
        <div key={this.props.index} className="col-lg-6 col-sm-6 col-xs-12">
          <div className="customer-box">
            <div className="customer-key">Key Users</div>
            <div className="row clearfix">
              <div className="col-xs-10">
                {this.props.activated === 1 && this.props.status === 1 ? (
                  <>
                    <div className="customer-img-are">
                      <div className="customer-image">
                        {" "}
                        <span
                          onClick={(e) =>
                            this.redirectUrl(e, this.props.customer_id)
                          }
                          style={{ cursor: "pointer" }}
                        >
                          {" "}
                          <img src={this.props.profile_pic} />
                        </span>{" "}
                      </div>
                      {/* <div className="customer-reivew"> <i className="fa fa-star"></i><i className="fa fa-star"></i><i className="fa fa-star"></i> </div> */}
                    </div>
                    <div className="customer-content col-border">
                      <h5>
                        <span
                          onClick={(e) =>
                            this.redirectUrl(e, this.props.customer_id)
                          }
                          style={{ cursor: "pointer" }}
                        >
                          {this.props.company_name}
                        </span>
                      </h5>
                      <p
                        onClick={(e) =>
                          this.redirectUrl(e, this.props.customer_id)
                        }
                        style={{ cursor: "pointer" }}
                        className="cus-id"
                      >
                        <strong>Name:</strong> {this.props.first_name}{" "}
                        {this.props.last_name}
                      </p>
                      <p className="cus-id">
                        <strong>Email:</strong> {this.props.email}
                      </p>
                    </div>
                  </>
                ) : (
                  <>
                    <div className="customer-img-are">
                      <div className="customer-image">
                        {" "}
                        <span
                          onClick={(e) => this.modalShowHandler(e)}
                          style={{ cursor: "pointer" }}
                        >
                          {" "}
                          <img src={this.props.profile_pic} />
                        </span>
                        {this.props.status === 1 && this.props.activated === 0 && (
                          <p className="tielsInactive" style={{ color: "red" }}>
                            In-Active
                          </p>
                        )}
                      </div>
                      {/* <div className="customer-reivew"> <i className="fa fa-star"></i><i className="fa fa-star"></i><i className="fa fa-star"></i> </div> */}
                    </div>
                    <div className="customer-content col-border">
                      <h5>
                        <span
                          onClick={(e) => this.modalShowHandler(e)}
                          style={{ cursor: "pointer" }}
                        >
                          {this.props.company_name}
                        </span>
                      </h5>
                      <p
                        onClick={(e) => this.modalShowHandler(e)}
                        style={{ cursor: "pointer" }}
                        className="cus-id"
                      >
                        <strong>Name:</strong> {this.props.first_name}{" "}
                        {this.props.last_name}
                      </p>
                      <p className="cus-id">
                        <strong>Email:</strong> {this.props.email}
                      </p>
                    </div>
                  </>
                )}
              </div>
              <div className="col-xs-2">
                {" "}
                <div className="customer-icon-holder">
                  <img src={userCalender} alt="calendar" />
                  <img src={useremail} alt="envelope" />
                </div>
              </div>
            </div>
          </div>
        </div>
        <Modal
          show={this.state.showModal}
          onHide={() => this.modalCloseHandler()}
          backdrop="static"
        >
          <Modal.Header>
            <Modal.Title>My Customers</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <div className="contBox">
              <span>
                You cannot access this account as the user is yet to activate
                his/her account.
              </span>
            </div>
          </Modal.Body>

          <Modal.Footer>
            <button
              className={`btn-fill m-r-10`}
              type="button"
              onClick={(e) => this.modalCloseHandler()}
            >
              Close
            </button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

class NormalAccount extends Component {
  state = {
    showModal: false,
  };
  redirectUrl = (event, id) => {
    event.preventDefault();
    //http://reddy.indusnet.cloud/customer-dashboard?source=Mi02NTE=
    API.post(`/api/employees/shlogin`, { customer_id: parseInt(id) })
      .then((res) => {
        if (res.data.shadowToken) {
          var url =
            process.env.REACT_APP_CUSTOMER_PORTAL +
            `setToken/` +
            res.data.shadowToken;
          window.open(url, "_blank");
        }
      })
      .catch((error) => {
        showErrorMessageFront(error, 0, this.props);
      });
  };

  modalShowHandler = (event) => {
    event.preventDefault();
    this.setState({ showModal: true });
  };
  modalCloseHandler = () => {
    this.setState({ showModal: false });
  };

  render() {
    return (
      <>
        <div key={this.props.index} className="col-lg-4 col-sm-6 col-xs-12">
          <div className="customer-box customer-box-small">
            <div className="row clearfix">
              <div className="col-xs-10">
                {this.props.activated === 1 && this.props.status === 1 ? (
                  <>
                    <div className="customer-img-are">
                      <div className="customer-image">
                        {" "}
                        <span
                          onClick={(e) =>
                            this.redirectUrl(e, this.props.customer_id)
                          }
                          style={{ cursor: "pointer" }}
                        >
                          {" "}
                          <img src={this.props.profile_pic} />
                        </span>{" "}
                      </div>
                    </div>
                    <div className="customer-content col-border">
                      <h5>
                        <span
                          onClick={(e) =>
                            this.redirectUrl(e, this.props.customer_id)
                          }
                          style={{ cursor: "pointer" }}
                        >
                          {this.props.company_name}
                        </span>
                      </h5>
                      <p
                        onClick={(e) =>
                          this.redirectUrl(e, this.props.customer_id)
                        }
                        style={{ cursor: "pointer" }}
                        className="cus-id"
                      >
                        <strong>Name:</strong> {this.props.first_name}{" "}
                        {this.props.last_name}
                      </p>
                      <p className="cus-id">
                        <strong>Email:</strong> {this.props.email}
                      </p>
                    </div>
                  </>
                ) : (
                  <>
                    <div className="customer-img-are">
                      <div className="customer-image">
                        {" "}
                        <span
                          onClick={(e) => this.modalShowHandler(e)}
                          style={{ cursor: "pointer" }}
                        >
                          {" "}
                          <img src={this.props.profile_pic} />
                        </span>{" "}
                        {this.props.status === 1 && this.props.activated === 0 && (
                          <p className="tielsInactive" style={{ color: "red" }}>
                            In-Active
                          </p>
                        )}
                      </div>
                    </div>
                    <div className="customer-content col-border">
                      <h5>
                        <span
                          onClick={(e) => this.modalShowHandler(e)}
                          style={{ cursor: "pointer" }}
                        >
                          {this.props.company_name}
                        </span>
                      </h5>
                      <p
                        onClick={(e) => this.modalShowHandler(e)}
                        style={{ cursor: "pointer" }}
                        className="cus-id"
                      >
                        <strong>Name:</strong> {this.props.first_name}{" "}
                        {this.props.last_name}
                      </p>
                      <p className="cus-id">
                        <strong>Email:</strong> {this.props.email}
                      </p>
                    </div>
                  </>
                )}
              </div>
              <div className="col-xs-2">
                {" "}
                <div className="customer-icon-holder">
                  <img src={userCalender} alt="calendar" />
                  <img src={useremail} alt="envelope" />
                </div>
              </div>
            </div>
          </div>
        </div>
        <Modal
          show={this.state.showModal}
          onHide={() => this.modalCloseHandler()}
          backdrop="static"
        >
          <Modal.Header>
            <Modal.Title>My Customers</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <div className="contBox">
              <span>
                You cannot access this account as the user is yet to activate
                his/her account.
              </span>
            </div>
          </Modal.Body>

          <Modal.Footer>
            <button
              className={`btn-fill m-r-10`}
              type="button"
              onClick={(e) => this.modalCloseHandler()}
            >
              Close
            </button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

class AgentAccount extends Component {
  redirectUrl = (event, id) => {
    event.preventDefault();
    //http://reddy.indusnet.cloud/customer-dashboard?source=Mi02NTE=
    API.post(`/api/employees/shlogin`, { customer_id: parseInt(id) })
      .then((res) => {
        if (res.data.shadowToken) {
          var url =
            process.env.REACT_APP_CUSTOMER_PORTAL +
            `setToken/` +
            res.data.shadowToken;
          window.open(url, "_blank");
        }
      })
      .catch((error) => {
        showErrorMessageFront(error, 0, this.props);
      });
  };

  render() {
    return (
      <div key={this.props.index} className="col-lg-4 col-sm-6 col-xs-12">
        <div className="customer-box customer-box-small">
          <div className="row clearfix">
            <div className="col-xs-10">
              <div className="customer-img-are">
                <div className="customer-image">
                  {" "}
                  <span
                    onClick={(e) => this.redirectUrl(e, this.props.customer_id)}
                    style={{ cursor: "pointer" }}
                  >
                    {" "}
                    <img src={this.props.profile_pic} />
                  </span>{" "}
                </div>
              </div>
              <div className="customer-content col-border">
                <h5>
                  <span
                    onClick={(e) => this.redirectUrl(e, this.props.customer_id)}
                    style={{ cursor: "pointer" }}
                  >
                    {this.props.company_name}
                  </span>
                </h5>
                <p
                  onClick={(e) => this.redirectUrl(e, this.props.customer_id)}
                  style={{ cursor: "pointer" }}
                  className="cus-id"
                >
                  <strong>Name:</strong> {this.props.first_name}{" "}
                  {this.props.last_name}
                </p>
                <p className="cus-id">
                  <strong>Email:</strong> {this.props.email}
                </p>
              </div>
            </div>
            <div className="col-xs-2">
              {" "}
              <div className="customer-icon-holder">
                <img src={userCalender} alt="calendar" />
                <img src={useremail} alt="envelope" />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default MyCustomer;
