import React, { Component } from 'react';
import { getMyDrupalId } from '../../shared/helper';
import base64 from 'base-64';
import '../../assets/css/MyCustomer.css';
import Loader from 'react-loader-spinner';
import API from "../../shared/axios";
import commenLogo from '../../assets/images/image-edit.jpg';
import Select from "react-select";

const portal_url = `${process.env.REACT_APP_PORTAL_URL}`; // SATYAJIT

class MyCustomer extends Component {

  constructor(props) {
    super(props);
    this.textInput = React.createRef();

    this.state = {
      vipBlock: [],
      normalBlock: [],
      isHidden: true
    };
  }

  focusTextInput = () => {
    if (this.textInput.current.value) {
      let keyword = this.textInput.current.value;
      this.setState({
        vipBlock: this.state.vipBlock.filter((customer, m) => {
          return (customer.first_name === keyword || customer.last_name === keyword);
        }),
        normalBlock: this.state.normalBlock.filter((customer, m) => {
          return (customer.first_name === keyword || customer.last_name === keyword);
        })
      });
    }
    //console.log(this.state.vipBlock)
    //console.log(this.state.normalBlock)
    //this.textInput.current.focus();
  }

  componentDidMount = () => {
    //console.log('company');
    API.get('/api/employees/company')
      .then(res => {
       
        var myCompany = [];
        for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            myCompany.push({
              value: element["company_id"],
              label: element["company_name"]
            });
        }

        this.setState({companyArr:myCompany});

      })
      .catch(err => {
        console.log(err);
      });
  }

  searchCustomer = (e) => {
    e.preventDefault();
    console.log(e);
  }

  changeCompany = (event) => {
    if(event === null){
      //console.log('here');
      this.setState({
        isHidden: true
      });
    }else{
      if (event.value > 0) {
  
        API.get(`/api/employees/customers/${event.value}`)
        .then(res => {
  
          this.setState({
            vipBlock: res.data.data.filter((customer, m) => {
              return customer.vip_customer === 1;
            }),
            normalBlock: res.data.data.filter((customer, m) => {
              return customer.vip_customer === 0;
            }),
            defVal: event
          });

          API.get(`/api/agents/company/${event.value}`)
          .then(res => {
            this.setState({
              isHidden  : false,
              agenBlock : res.data.data
            });
          })
          .catch(err => {
            console.log(err);
          });
          
        })
        .catch(err => {
          console.log(err);
        });
  
      } else{
        this.setState({
          isHidden: true
        });
      }
    }
  }

  handleTabs = (event) =>{
        
    if(event.currentTarget.className === "active" ){
        //DO NOTHING
    }else{

        var elems          = document.querySelectorAll('[id^="tab_"]');
        var elemsContainer = document.querySelectorAll('[id^="show_tab_"]');
        var currId         = event.currentTarget.id;

        for (var i = 0; i < elems.length; i++){
            elems[i].classList.remove('active');
        }

        for (var j = 0; j < elemsContainer.length; j++){
            elemsContainer[j].style.display = 'none';
        }

        event.currentTarget.classList.add('active');
        event.currentTarget.classList.add('active');
        document.querySelector('#show_'+currId).style.display = 'block';
    }
    // this.tabElem.addEventListener("click",function(event){
    //     alert(event.target);
    // }, false);
  }

  render() {
    //console.log("checking hidden", this.state.isHidden);
    //console.log(this.state.normalBlock);
    if (!this.state.isHidden === true) {
      if (this.state.vipBlock.length > 0 || this.state.normalBlock.length > 0) {
        return (
          <>
            <div className="content-wrapper">
              {/*<!-- Content Header (Page header) -->*/}
              <section className="content-header">
                <div className="row">
                  <div className="col-lg-3 col-sm-6 col-xs-12">
                    <div className="form-group has-feedback">
                      <Select
                        className="basic-single"
                        classNamePrefix="select"
                        defaultValue={this.state.defVal}
                        isClearable={true}
                        isSearchable={true}
                        name="employee_id"
                        options={this.state.companyArr}
                        onChange={e => { this.changeCompany(e) }}
                    />
                    </div>
                  </div>
                  <div className="col-lg-3 col-sm-6 col-xs-12">
                    <div className="form-group has-feedback">
                      <div className="input-group">
                        <input
                          className="form-control"
                          placeholder="Search by Customer Name..."
                          ref={this.textInput}
                        />
                        <div className="input-group-btn">
                          <button
                            className="btn btn-default"
                            type="button"
                            onClick={this.focusTextInput}
                          >
                            <i className="glyphicon glyphicon-search"></i>
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="nav-tabs-custom">
                    <ul className="nav nav-tabs">
                        <li className="active" onClick = {(e) => this.handleTabs(e)} id="tab_1" >My Customers</li>
                        <li onClick = {(e) => this.handleTabs(e)}  id="tab_2"> My Agents</li>
                    </ul>
                    <div className="tab-content">
                      <div className="tab-pane active" id="show_tab_1">
                        <div className="row">
                          {this.state.normalBlock.map((customer, j) =>
                              <NormalAccount
                                key={j}
                                index={j}
                                first_name={customer.first_name}
                                last_name={customer.last_name}
                                email={customer.email}
                                company_name={customer.company_name}
                                drupal_id={customer.drupal_id}
                              />
                            )
                          }
                        </div>
                        <div className="row">
                          {this.state.vipBlock.map((customer, m) =>
                              <KeyAccounts
                                key={m}
                                index={m}
                                first_name={customer.first_name}
                                last_name={customer.last_name}
                                email={customer.email}
                                company_name={customer.company_name}
                                drupal_id={customer.drupal_id}
                              />
                            )
                          }
                        </div>
                      </div>
                      <div className="tab-pane" id="show_tab_2">
                        <div className="row">
                          {this.state.agenBlock && this.state.agenBlock.map((agent, k) =>                     
                              <AgentAccount
                                key={k}
                                index={k}
                                first_name={agent.first_name}
                                last_name={agent.last_name}
                                email={agent.email}
                                drupal_id={agent.drupal_id}
                              />
                            )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section className="content">
                
                
              </section>
            </div>
          </>
        );

      } else {
        return (
          <>
            <div className="loading_reddy_outer">
              <div className="loading_reddy" >
                <Loader
                  type="Puff"
                  color="#00BFFF"
                  height="50"
                  width="50"
                  verticalAlign="middle"
                />
              </div>
            </div>
          </>
        );
      }
    } else {
      return (
        <>
          <div className="content-wrapper">
            <section className="content customer-center-main">
              <div className="boxPapanel content-padding customer-center">
                <div className="form-group has-feedback">
                  <label>Select Company name form list below</label>
                  {/* <select className="form-control">
                            <option value="-1">Company name</option>
                        </select> */}
                  {/* <select className="form-control" onChange={e => { this.changeCompany(e) }}>
                    <option value="">Select</option>
                    <option value="1">INT</option>
                    <option value="2">INDUS</option>
                  </select> */}

                    <Select
                        className="basic-single"
                        classNamePrefix="select"
                        defaultValue={0}
                        isClearable={true}
                        isSearchable={true}
                        name="employee_id"
                        options={this.state.companyArr}
                        onChange={e => { this.changeCompany(e) }}
                    />

                </div>
              </div>
            </section>
          </div>
        </>
      );
    }
  }

}


class KeyAccounts extends Component {

  redirectUrl = (event, id) => {
    event.preventDefault();
    //http://reddy.indusnet.cloud/customer-dashboard?source=Mi02NTE=
    var emp_drupal_id = getMyDrupalId(localStorage.token);
    var base_encode   = base64.encode(`${id}-${emp_drupal_id}`); 
    window.open( portal_url + "customer-dashboard?source="+base_encode, '_blank');
  }

  render() {
    return (

      <div key={this.props.index} className="col-lg-6 col-sm-6 col-xs-12">
        <div className="customer-box">
          <div className="customer-key">Key Account</div>
          <div className="row clearfix">
            <div className="col-xs-10">
              <div className="customer-img-are">
                <div className="customer-image"> <span
                  onClick={e => this.redirectUrl(e, this.props.drupal_id)} style={{ cursor: "pointer" }} 
                > <img src={commenLogo} alt="User Image" /></span> </div>
                {/* <div className="customer-reivew"> <i className="fa fa-star"></i><i className="fa fa-star"></i><i className="fa fa-star"></i> </div> */}
              </div>
              <div className="customer-content col-border">
                <h5><span
                  onClick={e => this.redirectUrl(e, this.props.drupal_id)}
                  style={{ cursor: "pointer" }}
                >{this.props.company_name}</span></h5>
                <p onClick={e => this.redirectUrl(e, this.props.drupal_id)} style={{ cursor: "pointer" }} className="cus-id"><strong>Name:</strong> {this.props.first_name} {this.props.last_name}</p>
                <p className="cus-id"><strong>Email:</strong> {this.props.email}</p>
              </div>
            </div>
            <div className="col-xs-2"> <div className="customer-icon-holder"><i className="far fa-calendar-check"></i> <i className="far fa-envelope"></i> </div></div>
          </div>
        </div>
      </div>
    );
  }
}

class NormalAccount extends Component {

  redirectUrl = (event, id) => {
    event.preventDefault();
    //http://reddy.indusnet.cloud/customer-dashboard?source=Mi02NTE=
    var emp_drupal_id = getMyDrupalId(localStorage.token);
    var base_encode   = base64.encode(`${id}-${emp_drupal_id}`); 
    window.open( portal_url + "customer-dashboard?source="+base_encode, '_blank');
  }

  render() {
    return (
      <div key={this.props.index} className="col-lg-4 col-sm-6 col-xs-12">
        <div className="customer-box customer-box-small">
          <div className="row clearfix">
            <div className="col-xs-10">
              <div className="customer-img-are">
                <div className="customer-image"> <span
                  onClick={e => this.redirectUrl(e, this.props.drupal_id)} style={{ cursor: "pointer" }} 
                > <img src={commenLogo} alt="User Image" /></span> </div>
              </div>
              <div className="customer-content col-border">
                <h5><span
                  onClick={e => this.redirectUrl(e, this.props.drupal_id)}
                  style={{ cursor: "pointer" }}
                >{this.props.company_name}</span></h5>
                <p onClick={e => this.redirectUrl(e, this.props.drupal_id)} style={{ cursor: "pointer" }} className="cus-id"><strong>Name:</strong> {this.props.first_name} {this.props.last_name}</p>
                <p className="cus-id"><strong>Email:</strong> {this.props.email}</p>
              </div>
            </div>
            <div className="col-xs-2"> <div className="customer-icon-holder"><i className="far fa-calendar-check"></i> <i className="far fa-envelope"></i> </div></div>
          </div>
        </div>
      </div>
    );
  }
}

class AgentAccount extends Component {

  redirectUrl = (event, id) => {
    event.preventDefault();
    //http://reddy.indusnet.cloud/customer-dashboard?source=Mi02NTE=
    var emp_drupal_id = getMyDrupalId(localStorage.token);
    var base_encode   = base64.encode(`${id}-${emp_drupal_id}`); 
    window.open( portal_url + "customer-dashboard?source="+base_encode, '_blank');
  }

  render() {
    return (
      <div key={this.props.index} className="col-lg-4 col-sm-6 col-xs-12">
        <div className="customer-box customer-box-small">
          <div className="row clearfix">
            <div className="col-xs-10">
              <div className="customer-img-are">
                <div className="customer-image"> <span
                  onClick={e => this.redirectUrl(e, this.props.drupal_id)} style={{ cursor: "pointer" }} 
                > <img src={commenLogo} alt="User Image" /></span> </div>
              </div>
              <div className="customer-content col-border">
                <h5><span
                  onClick={e => this.redirectUrl(e, this.props.drupal_id)}
                  style={{ cursor: "pointer" }}
                >{this.props.company_name}</span></h5>
                <p onClick={e => this.redirectUrl(e, this.props.drupal_id)} style={{ cursor: "pointer" }} className="cus-id"><strong>Name:</strong> {this.props.first_name} {this.props.last_name}</p>
                <p className="cus-id"><strong>Email:</strong> {this.props.email}</p>
              </div>
            </div>
            <div className="col-xs-2"> <div className="customer-icon-holder"><i className="far fa-calendar-check"></i> <i className="far fa-envelope"></i> </div></div>
          </div>
        </div>
      </div>
    );
  }
}

export default MyCustomer;