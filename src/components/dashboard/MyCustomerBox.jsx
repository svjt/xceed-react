import React, { Component } from 'react';
import customersIcon from '../../assets/images/my-customer-icon.svg';
import keyAccoutIcon from '../../assets/images/key-accounts-icon.svg';
import API from "../../shared/axios";
import { Link } from 'react-router-dom';
import MyCompanyPopup from './MyCompanyPopup';

class MyCustomerBox extends Component {

    state = {
        custTile: {},
        showCompanyPopup:false,
        met_sla: '',
        missed_sla: '',
        met_num: '',
        missed_num: '',
        class_met_sla: '',
        class_missed_sla: '',
    };

    componentDidMount = () => {

        let query_params = '';
        //console.log('Customer Box Query String',this.props.queryString);

        if(this.props.queryString != ''){
            query_params = `?${this.props.queryString}`;
        }
            
        API.get(`/api/tasks/customer_tiles${query_params}`)
        .then(res => {
            // this.setupCustomerSla(res);
            this.setState({
                custTile : res.data.data
            });
        })
        .catch(err => {
            console.log(err);
        });
    }

    // setupCustomerSla = (res) => {
    //     let met_sla = '';
    //     let missed_sla = '';
    //     let met_num = '';
    //     let missed_num = '';
    //     let class_met_sla = '';
    //     let class_missed_sla = '';

    //     if(res.data.data.total_customer_sla > 0){
    //         met_sla = Math.ceil((res.data.data.total_met_customer_sla/res.data.data.total_customer_sla) * 100); 
    //         //missed_sla = Math.ceil((res.data.data.total_missed_customer_sla/res.data.data.total_customer_sla) * 100);
    //         missed_sla = 100-met_sla;
            
    //         met_num          = res.data.data.total_met_customer_sla;
    //         missed_num       = res.data.data.total_missed_customer_sla;

    //         class_met_sla    = met_sla;
    //         class_missed_sla = missed_sla;
    //     }else{

    //         met_num = 0;
    //         missed_num = 0;

    //         met_sla = 0;
    //         missed_sla = 0;
    //         class_met_sla = 1;
    //         class_missed_sla = 1;
    //     }
    //     this.setState({
    //         met_sla          : met_sla,
    //         missed_sla       : missed_sla,
    //         met_num          : met_num,
    //         missed_num       : missed_num,
    //         class_met_sla    : class_met_sla,
    //         class_missed_sla : class_missed_sla
    //     });
    // }

    selectComapny = () =>{
        this.setState({showCompanyPopup:true});
    }

    closePopup = () => {
        this.setState({showCompanyPopup:false});
    };

    render(){

        const blinker = this.state.custTile.customer_new_tile > 0 ? 'blinking' : '';

        return(
            <>
           {this.state.custTile && <div className="col-lg-3 col-sm-6 col-xs-12">
                <div className="small-box bg-Blue clearfix">
                    <div className="left-panel">
                        <div className="inner clearfix" style={{cursor:'pointer'}} onClick={()=>this.selectComapny()} >
                            <div className="icon-panel">
                                <div className="icon"><img src={customersIcon} alt="My Customers" /></div>
                                <h3>{this.state.custTile.company_tile}</h3>
                            </div>
                            <span>
                                {this.state.custTile.company_tile === 1 && <p><strong>My Customer</strong></p>}
                                {this.state.custTile.company_tile !== 1 && <p><strong>My Customers</strong></p>}
                            </span>
                            {/* Commented By AKJ on 27-12-2021 */}
                            {/* <div className="d-flex customerGraph">
                                <div className="">
                                    <p><span className="roundShape green"></span>Met  SLA</p>                           
                                    <p><span className="roundShape red"></span>Missed SLA  </p>
                                </div>
                                <div className="graphChart">
                                    {this.state.met_sla > 0 && 
                                    <div className="progress-main">
                                        <div className="progress vertical">
                                            <div className="progress-bar progress-bar-green" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style={{height: `${this.state.class_met_sla}%`}}> </div> 
                                        </div>
                                        <span className="sr-only-new">{`${this.state.met_num}`}</span>
                                    </div>}
                                    
                                    {this.state.missed_sla > 0 && 
                                    <div className="progress-main">
                                        <div className="progress vertical">
                                            <div className="progress-bar progress-bar-red" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style={{height: `${this.state.class_missed_sla}%`}}> </div> 
                                        </div>
                                        <span className="sr-only-new">{`${this.state.missed_num}`}</span>
                                    </div>}
                                </div>
                            </div> */}
                            {/* <Link to='/user/my_customer'> */}

                            {/* </Link> */}
                        </div>
                        <div className="footer-inner clearfix">
                            <div className="panel-01">
                                Open Requests <span>{this.state.custTile.customer_open_tile}</span>
                            </div>
                            <div className="panel-02 col-border">
                                New Requests <span className={blinker}>{this.state.custTile.customer_new_tile}</span>
                            </div>
                        </div>
                    </div>
                    <div className="right-panel">
                        <div className="keyAccounts">
                            <div className="icon-panel">
                                <div className="icon"><img src={keyAccoutIcon} alt="Key Account" /></div>
                                <h3>{this.state.custTile.key_customer_tile}</h3>
                            </div>
                            {this.state.custTile.key_customer_tile !== 1 && <p><strong>Key Users</strong></p>}
                            {this.state.custTile.key_customer_tile === 1 && <p><strong>Key User</strong></p>}
                        </div>
                        <div className="footer-inner clearfix">
                            <div className="panel-01 col-border">
                                Open <span>{this.state.custTile.key_open_customer_tile}</span>
                            </div>
                            <div className="panel-02 col-border">
                                New<span className={blinker}>{this.state.custTile.key_new_customer_tile}</span>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.showCompanyPopup === true && <MyCompanyPopup closePopup={this.closePopup} {...this.props} />}
            </div>}
            </>
        );
    }
}

export default MyCustomerBox;