import React, { Component } from 'react';
import taskIcon from '../../assets/images/my-tasks-icon.svg';
import API from "../../shared/axios";
import { getUserDisplayName, inArray,isSPOC } from "../../shared/helper";
class MyTaskBox extends Component {

    state = {
        taskTile: {}
    };

    componentDidMount = () => {
            
        let query_params = '';
        //console.log('Task Box Query String',this.props.queryString);

        if(this.props.queryString != ''){
            query_params = `?${this.props.queryString}`;
        }

        API.get(`/api/tasks/task_tiles${query_params}`)
        .then(res => {
            this.setState({
                taskTile : res.data.data
            });
        })
        .catch(err => {
            console.log(err);
        });
    }

    render(){
        //console.log('rendering', this.state.taskTile, this.state.taskTile.length);
        const { designation } = getUserDisplayName(localStorage.token);
        return(
            <>
        
            {this.state.taskTile && <div className="col-lg-3 col-sm-6 col-xs-12">
                <div className="small-box bg-Red">
                    <div className={`inner ${isSPOC() == 1 && this.state.taskTile.pending_reviews > 0 && 'col-lg-9 col-sm-6 col-xs-12'}`}>                   
                        <div className="icon-panel">
                            <div className="icon task-icon"><img src={taskIcon} alt="My Tasks" /></div>
                            <h3>{this.state.taskTile.total_tasks}</h3>
                        </div>
                        {this.state.taskTile.total_tasks === 1 && <p><strong>My Task</strong></p>}
                        {this.state.taskTile.total_tasks !== 1 && <p><strong>My Tasks</strong></p>}
                    </div>

                    {isSPOC() == 1 && this.state.taskTile.pending_reviews > 0 &&  <>
                    <div className="inner">
                            <div className="icon-panel">
                                <div className="icon task-icon"><img src={taskIcon} alt="My Tasks" /></div>
                                <h3>{this.state.taskTile.pending_reviews}</h3>
                            </div>
                            {this.state.taskTile.pending_reviews === 1 && <p><strong>Pending Review</strong></p>}
                            {this.state.taskTile.pending_reviews !== 1 && <p><strong>Pending Reviews</strong></p>}
                            </div>
                        </>}

                    <div className="small-box-footer clearfix">
                        <div className="col-3 red-color">Overdue <span>{this.state.taskTile.overdue_tasks}</span></div>
                        <div className="col-3 col-border">Due Today <span>{this.state.taskTile.overdue_today}</span></div>
                        <div className="col-3 col-border"><div className="row">Due this Week <span>{this.state.taskTile.overdue_week}</span></div></div>
                    </div>
                </div>
            </div> }

            
            </>
        );
    }
}

export default MyTaskBox;