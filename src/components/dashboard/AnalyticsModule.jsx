import React, { Component } from "react";
import API from "../../shared/axios";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
import customersIcon from "../../assets/images/my-customer-icon.svg";
import keyAccoutIcon from "../../assets/images/key-accounts-icon.svg";
import taskIcon from "../../assets/images/my-tasks-icon.svg";
import totalTaskImage from "../../assets/images/totaltask-icon.svg";
import potentialAlertImage from "../../assets/images/potential-alert-icon.svg";
import potentialNotificationImage from "../../assets/images/potential-notification-icon.svg";
import { Bar, Doughnut } from "react-chartjs-2";
import { Chart } from "react-google-charts";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Tooltip,
  OverlayTrigger,
  Modal,
  Table,
} from "react-bootstrap";

import { Link } from "react-router-dom";

import MyCompanyPopup from "./MyCompanyPopup";

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="left"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
      style={{
        backgroundColor: "rgba(0, 0, 0, 0.85)",
        padding: "2px 10px",
        color: "white",
        borderRadius: 3,
      }}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

class AnalyticsModule extends Component {
  state = {
    custTiles: {},
    taskTiles: {},
    showCompanyPopup: false,
    emp_task_distribution_google: [],
    reqTypeTaskDistribution_google: [],
    show_data: false,
    met_sla: "",
    missed_sla: "",
    distribution_total: 0,
    distribution_open: 0,
    distribution_close: 0,
    distribution_breach: 0,
    tasks_positive:0,
    total_riview:0,
    task_neutral:0,
    task_negative:0
  };

  checkHandler = (event) => {
    event.preventDefault();
  };

  componentDidMount = () => {
    API.get(
      `/api/reports/my_analytics/open_tasks?${this.props.queryString}`
    )
    .then((res) => {
      //console.log('open_tasks',res.data.open);
      this.setState({
        open_google: res.data.open,
        openTotalTasks: res.data.openTotalTasks,
      });
    })
    .catch((err) => {
      console.log(err);
    });

    API.get(
      `/api/reports/my_analytics/allocated_tasks?${this.props.queryString}`
    )
    .then((res) => {
      //console.log('allocated_tasks',res.data.data);
      this.setState({
        assign_google: res.data.assign,
        assignTotalTasks: res.data.assignTotalTasks,
      });
    })
    .catch((err) => {
      console.log(err);
    });

    API.get(
      `/api/reports/my_analytics/closed_tasks?${this.props.queryString}`
    )
    .then((res) => {
      //console.log('closed_tasks',res.data.data);
      this.setState({
        close_google: res.data.close,
        closeTotalTasks: res.data.closeTotalTasks,
      });
    })
    .catch((err) => {
      console.log(err);
    });

    API.get(
      `/api/reports/my_analytics/task_overdue_analysis?${this.props.queryString}`
    )
    .then((res) => {
      this.setState({
        overdue_google: res.data.overdueAnalysis,
        overdueTotalTasks: res.data.overdueTotalTasks,
      });
    })
    .catch((err) => {
      console.log(err);
    });

    API.get(
      `/api/reports/my_analytics/sla_met_analysis?${this.props.queryString}`
    )
    .then((res) => {
      this.setState({
        met_google: res.data.metAnalysis,
        metTotalTasks: res.data.metTotalTasks,
      });
    })
    .catch((err) => {
      console.log(err);
    });

    API.get(
      `/api/reports/my_analytics/company_task_distribution?${this.props.queryString}`
    )
    .then((res) => {
      //console.log('sla_met_analysis',res.data.data);
      this.setState({
        emp_task_distribution_google: res.data.emp_task_distribution_google
      });
    })
    .catch((err) => {
      console.log(err);
    });  

    API.get(
      `/api/reports/my_analytics/request_task_distribution?${this.props.queryString}`
    )
    .then((res) => {
      //console.log('sla_met_analysis',res.data.data);
      this.setState({
        req_task_distribution_google: res.data.req_task_distribution_google
      });
    })
    .catch((err) => {
      console.log(err);
    });

    API.get(
      `/api/reports/my_analytics/get_task_ratings_review?${this.props.queryString}`
    )
    .then((res) => {
      this.setState({
        total_riview:
          res.data.tasks_positive +
          res.data.task_neutral +
          res.data.task_negative,
        tasks_positive: res.data.tasks_positive,
        task_neutral: res.data.task_neutral,
        task_negative: res.data.task_negative,
      });
    })
    .catch((err) => {
      console.log(err);
    });


    this.setState({show_data: true});
  };

  selectComapny = () => {
    this.setState({ showCompanyPopup: true });
  };

  closePopup = () => {
    this.setState({ showCompanyPopup: false });
  };

  render() {
    if (this.state.isLoading) {
      return (
        <>
          <div className="loderOuter">
            <div className="loader">
              <img src={loaderlogo} alt="logo" />
              <div className="loading">Loading...</div>
            </div>
          </div>
        </>
      );
    } else {
      let prevHist = this.props.history;
      let urlStr = this.props.queryString;
      return (
        <>
          <div className="dashboard-top">
            {this.state.show_data && (
              <div className="row">
                <div className="col-lg-7 col-sm-7 col-xs-12">
                  <div className="analytics-moudle">
                    <div className="inner height-01 clearfix">
                      <div className="inner-head">
                        <h5>Number of Tasks</h5>
                      </div>
                      <div className="row">

                        <div className="col-md-4 col-sm-4 col-border">
                          <div className="analytics-moudle-inner">
                            <p className="chart-heading">Open Tasks</p>
                            <div className="analytics-moudle-chart">
                              <Chart
                                chartType="PieChart"
                                loader={<div>Loading Chart</div>}
                                data={this.state.open_google}
                                options={{
                                  chartArea: { width: "100%" },
                                  pieHole: 0.65,
                                  sliceVisibilityThreshold: 0,
                                  legend: { position: "none" },
                                  colors: [
                                    "#facf5a",
                                    "#ff5959",
                                    "#e8e8e8",
                                    "#D691AF",
                                  ],
                                  pieSliceText: "none",
                                }}
                                rootProps={{ "data-testid": "3" }}
                                chartEvents={[
                                  {
                                    eventName: "select",
                                    callback({ chartWrapper }) {
                                      let selected = chartWrapper
                                        .getChart()
                                        .getSelection();
                                      let selectedGraph = 0;
                                      if (selected != "") {
                                        if (selected[0].row === 0) {
                                          selectedGraph = 1;
                                        } else if (selected[0].row === 1) {
                                          selectedGraph = 2;
                                        } else if (selected[0].row === 2) {
                                          selectedGraph = 3;
                                        } else if (selected[0].row === 3) {
                                          selectedGraph = 17;
                                        }

                                        if (urlStr === "") {
                                          urlStr = "q=no";
                                        }

                                        prevHist.push({
                                          pathname: `/user/my_analytics_details/${selectedGraph}/1`,
                                          state: {
                                            urlStr: urlStr,
                                            selectedTab: 1,
                                          },
                                        });
                                      }
                                    },
                                  },
                                ]}
                              />
                              <div className="chart-count">
                                {this.state.openTotalTasks}
                                <span>Total</span>
                              </div>
                            </div>
                            <ul className="chart-description">
                              <li>
                                <span
                                  style={{ backgroundColor: "#facf5a" }}
                                ></span>
                                SLA Near Breach (>80%)
                              </li>
                              <li>
                                <span
                                  style={{ backgroundColor: "#ff5959" }}
                                ></span>
                                SLA Breached
                              </li>
                              <li>
                                <span
                                  style={{ backgroundColor: "#D691AF" }}
                                ></span>
                                SLA Breached With Reason
                              </li>
                              <li>
                                <span
                                  style={{ backgroundColor: "#e8e8e8" }}
                                ></span>
                                Within SLA
                              </li>
                            </ul>
                          </div>
                        </div>
                        <div className="col-md-4 col-sm-4 col-border">
                          <div className="analytics-moudle-inner">
                            <p className="chart-heading">Allocated Tasks</p>
                            <div className="analytics-moudle-chart">
                              <Chart
                                chartType="PieChart"
                                loader={<div>Loading Chart</div>}
                                data={this.state.assign_google}
                                options={{
                                  chartArea: { width: "100%" },
                                  pieHole: 0.65,
                                  sliceVisibilityThreshold: 0,
                                  legend: { position: "none" },
                                  colors: [
                                    "#facf5a",
                                    "#ff5959",
                                    "#e8e8e8",
                                    "#D691AF",
                                  ],
                                  pieSliceText: "none",
                                }}
                                rootProps={{ "data-testid": "3" }}
                                chartEvents={[
                                  {
                                    eventName: "select",
                                    callback({ chartWrapper }) {
                                      let selected = chartWrapper
                                        .getChart()
                                        .getSelection();
                                      let selectedGraph = 0;
                                      if (selected != "") {
                                        if (selected[0].row === 0) {
                                          selectedGraph = 4;
                                        } else if (selected[0].row === 1) {
                                          selectedGraph = 5;
                                        } else if (selected[0].row === 2) {
                                          selectedGraph = 6;
                                        } else if (selected[0].row === 3) {
                                          selectedGraph = 18;
                                        }

                                        if (urlStr === "") {
                                          urlStr = "q=no";
                                        }

                                        prevHist.push({
                                          pathname: `/user/my_analytics_details/${selectedGraph}/1`,
                                          state: {
                                            urlStr: urlStr,
                                            selectedTab: 2,
                                          },
                                        });
                                      }
                                    },
                                  },
                                ]}
                              />
                              <div className="chart-count">
                                {this.state.assignTotalTasks}
                                <span>Total</span>
                              </div>
                            </div>
                            <ul className="chart-description">
                              <li>
                                <span
                                  style={{ backgroundColor: "#facf5a" }}
                                ></span>
                                SLA Near Breach (>80%)
                              </li>
                              <li>
                                <span
                                  style={{ backgroundColor: "#ff5959" }}
                                ></span>
                                SLA Breached
                              </li>
                              <li>
                                <span
                                  style={{ backgroundColor: "#D691AF" }}
                                ></span>
                                SLA Breached With Reason
                              </li>
                              <li>
                                <span
                                  style={{ backgroundColor: "#e8e8e8" }}
                                ></span>
                                Within SLA
                              </li>
                            </ul>
                          </div>
                        </div>
                        <div className="col-md-4 col-sm-4">
                          <div className="analytics-moudle-inner">
                            <p className="chart-heading">Closed Tasks</p>
                            <div className="analytics-moudle-chart">
                              <Chart
                                chartType="PieChart"
                                loader={<div>Loading Chart</div>}
                                data={this.state.close_google}
                                options={{
                                  chartArea: { width: "100%" },
                                  pieHole: 0.65,
                                  sliceVisibilityThreshold: 0,
                                  legend: { position: "none" },
                                  colors: ["#15cda8", "#ff5959", "#D691AF"],
                                  pieSliceText: "none",
                                }}
                                rootProps={{ "data-testid": "3" }}
                                chartEvents={[
                                  {
                                    eventName: "select",
                                    callback({ chartWrapper }) {
                                      let selected = chartWrapper
                                        .getChart()
                                        .getSelection();
                                      let selectedGraph = 0;

                                      if (selected != "") {
                                        if (selected[0].row === 0) {
                                          selectedGraph = 7;
                                        } else if (selected[0].row === 1) {
                                          selectedGraph = 8;
                                        } else if (selected[0].row === 2) {
                                          selectedGraph = 19;
                                        }

                                        if (urlStr === "") {
                                          urlStr = "q=no";
                                        }

                                        prevHist.push({
                                          pathname: `/user/my_analytics_details/${selectedGraph}/1`,
                                          state: {
                                            urlStr: urlStr,
                                            selectedTab: 3,
                                          },
                                        });
                                      }
                                    },
                                  },
                                ]}
                              />
                              <div className="chart-count">
                                {this.state.closeTotalTasks}
                                <span>Total</span>
                              </div>
                            </div>
                            <ul className="chart-description">
                              <li>
                                <span
                                  style={{ backgroundColor: "#15cda8" }}
                                ></span>
                                SLA Met
                              </li>
                              <li>
                                <span
                                  style={{ backgroundColor: "#ff5959" }}
                                ></span>
                                SLA Breached
                              </li>
                              <li>
                                <span
                                  style={{ backgroundColor: "#D691AF" }}
                                ></span>
                                SLA Breached With Reason
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-lg-5 col-sm-5 col-xs-12">
                  <div className="analytics-moudle">
                    <div className="inner height-01 clearfix">
                      <div className="inner-head">
                        <h5>Breakdown- Open &amp; Closed Tasks</h5>
                      </div>

                      <div className="row col-border">
                        <div className="col-md-6 col-sm-6 col-border">
                          <div className="analytics-moudle-inner">
                            <p className="chart-heading">
                              Task Overdue Analysis
                            </p>
                            <div className="analytics-moudle-chart">
                              <Chart
                                height={"200px"}
                                chartType="PieChart"
                                loader={<div>Loading Chart</div>}
                                data={this.state.overdue_google}
                                options={{
                                  chartArea: { width: "100%" },
                                  pieHole: 0.65,
                                  sliceVisibilityThreshold: 0,
                                  legend: { position: "none" },
                                  colors: ["#CC4747", "#E65050", "#802d2d"],
                                  pieSliceText: "none",
                                }}
                                rootProps={{ "data-testid": "3" }}
                                chartEvents={[
                                  {
                                    eventName: "select",
                                    callback({ chartWrapper }) {
                                      let selected = chartWrapper
                                        .getChart()
                                        .getSelection();
                                      let selectedGraph = 0;
                                      if (selected != "") {
                                        if (selected[0].row === 0) {
                                          selectedGraph = 9;
                                        } else if (selected[0].row === 1) {
                                          selectedGraph = 10;
                                        } else if (selected[0].row === 2) {
                                          selectedGraph = 11;
                                        }

                                        if (urlStr === "") {
                                          urlStr = "q=no";
                                        }

                                        prevHist.push({
                                          pathname: `/user/my_analytics_details/${selectedGraph}/1`,
                                          state: { urlStr: urlStr },
                                        });

                                      }
                                    },
                                  },
                                ]}
                              />
                              <div className="chart-count">
                                {this.state.overdueTotalTasks}
                                <span>Total</span>
                              </div>
                            </div>

                            <ul className="chart-description">
                              <li>
                                <span
                                  style={{ backgroundColor: "#cc4747" }}
                                ></span>
                                Delayed By Days (1-4)
                              </li>
                              <li>
                                <span
                                  style={{ backgroundColor: "#e65050" }}
                                ></span>
                                Delayed By Week (>4 days)
                              </li>
                              <li>
                                <span
                                  style={{ backgroundColor: "#802d2d" }}
                                ></span>
                                Delayed By Month (>22 days)
                              </li>
                            </ul>

                            <div className="task-right-top">
                              <LinkWithTooltip
                                tooltip={`Displaying breakdown of all overdue (open,allocated & closed) tasks.`}
                                href="#"
                                id="tooltip-1"
                                clicked={(e) => this.checkHandler(e)}
                              >
                                <img src={totalTaskImage} alt="exclamation" />
                              </LinkWithTooltip>
                            </div>
                          </div>
                        </div>

                        <div className="col-md-6 col-sm-6">
                          <div className="analytics-moudle-inner">
                            <p className="chart-heading">SLA Met Analysis</p>
                            <div className="analytics-moudle-chart">
                              <Chart
                                chartType="PieChart"
                                loader={<div>Loading Chart</div>}
                                data={this.state.met_google}
                                options={{
                                  chartArea: { width: "100%" },
                                  pieHole: 0.65,
                                  tooltip: { trigger: "hover" },

                                  sliceVisibilityThreshold: 0,
                                  legend: { position: "none" },
                                  colors: ["#12B392", "#0E9673", "#084D3F"],
                                  pieSliceText: "none",
                                }}
                                rootProps={{ "data-testid": "3" }}
                                chartEvents={[
                                  {
                                    eventName: "select",
                                    callback({ chartWrapper }) {
                                      let selected = chartWrapper
                                        .getChart()
                                        .getSelection();
                                      let selectedGraph = 0;
                                      if (selected != "") {
                                        if (selected[0].row === 0) {
                                          selectedGraph = 12;
                                        } else if (selected[0].row === 1) {
                                          selectedGraph = 13;
                                        } else if (selected[0].row === 2) {
                                          selectedGraph = 14;
                                        }

                                        if (urlStr === "") {
                                          urlStr = "q=no";
                                        }

                                        prevHist.push({
                                          pathname: `/user/my_analytics_details/${selectedGraph}/1`,
                                          state: {
                                            urlStr: urlStr,
                                            selectedTab: 3,
                                          },
                                        });
                                      }
                                    },
                                  },
                                ]}
                              />
                              <div className="chart-count">
                                {this.state.metTotalTasks}
                                <span>Total</span>
                              </div>
                            </div>

                            <ul className="chart-description">
                              <li>
                                <span
                                  style={{ backgroundColor: "#12B392" }}
                                ></span>
                                Before 1 day
                              </li>
                              <li>
                                <span
                                  style={{ backgroundColor: "#0E9673" }}
                                ></span>
                                Before 2-3 days
                              </li>
                              <li>
                                <span
                                  style={{ backgroundColor: "#084d3f" }}
                                ></span>
                                Before 4 days
                              </li>
                            </ul>

                            <div className="task-right-top">
                              <LinkWithTooltip
                                tooltip={`Displaying breakdown of all tasks which were closed within SLA.`}
                                href="#"
                                id="tooltip-1"
                                clicked={(e) => this.checkHandler(e)}
                              >
                                <img src={totalTaskImage} alt="exclamation" />
                              </LinkWithTooltip>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <div className="analytics-moudle bg-Green">
                    <div className="inner height-02 clearfix">
                      <div className="inner-head">
                        <h5>Customer Wise Task Distribution</h5>
                      </div>

                      <div className="bar-canvus-area">
                        <div className="canvus-side-head">
                          <p>No. Of Tasks</p>
                        </div>
                        <div className="canvus-area">
                          <Chart
                            width={"100%"}
                            height={"300px"}
                            chartType="Bar"
                            loader={<div>Loading Chart</div>}
                            data={this.state.emp_task_distribution_google}
                            options={{
                              colors: ["#fad264", "#15cda8", "#ff5959"],
                              legend: { position: "none" },
                            }}
                            className={"reddyTaskDistribution"}
                            rootProps={{ "data-testid": "2" }}
                            // chartEvents={[
                            //   {
                            //     eventName: "select",
                            //     callback({ chartWrapper }) {
                            //       let selected = chartWrapper
                            //         .getChart()
                            //         .getSelection();

                            //         console.log('selected',selected);

                            //       let selectedGraph = 0;
                            //       let barGraph = "";
                            //       if (selected != "") {
                            //         selectedGraph = 15;

                            //         barGraph = `${selected[0].row},${selected[0].column}`;
                            //         if (urlStr === "") {
                            //           urlStr = "q=no";
                            //         }

                            //         let selTab = selected[0].column;

                            //         prevHist.push({
                            //           pathname: `/user/my_analytics_details/${selectedGraph}/${barGraph}`,
                            //           state: {
                            //             urlStr: urlStr,
                            //             selectedTab: selTab,
                            //           },
                            //         });
                            //       }
                            //     },
                            //   },
                            // ]}
                          />
                        </div>
                      </div>

                      <div className="bar-description-area">
                        <ul className="bar-description">
                          <li>
                            <span style={{ backgroundColor: "#fad264" }}></span>
                            Open
                          </li>
                          <li>
                            <span style={{ backgroundColor: "#15cda8" }}></span>
                            Allocated
                          </li>
                          <li>
                            <span style={{ backgroundColor: "#ff5959" }}></span>
                            Closed
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <div className="analytics-moudle bg-Green">
                    <div className="inner height-02 clearfix">
                      <div className="inner-head">
                        <h5>Distribution of Request Types</h5>
                        {/* Total:{" "}
                        <strong>{this.state.distribution_total} </strong>
                        (Open: <strong>
                          {this.state.distribution_open}
                        </strong>{" "}
                        Allocated:{" "}
                        <strong>{this.state.distribution_close}</strong>)
                        Closed:{" "}
                        <strong>{this.state.distribution_breach}</strong>{" "} */}
                      </div>
                      <div className="bar-canvus-area">
                        <div className="canvus-side-head">
                          <p>No. Of Requests</p>
                        </div>
                        <div className="canvus-area">
                          <Chart
                            width={"100%"}
                            height={"300px"}
                            chartType="Bar"
                            loader={<div>Loading Chart</div>}
                            data={this.state.req_task_distribution_google}
                            options={{
                              colors: ["#fad264", "#15cda8", "#ff5959"],
                              legend: { position: "none" },
                            }}
                            // For tests
                            rootProps={{ "data-testid": "2" }}
                            // chartEvents={[
                            //   {
                            //     eventName: "select",
                            //     callback({ chartWrapper }) {
                            //       let selected = chartWrapper
                            //         .getChart()
                            //         .getSelection();
                            //       let selectedGraph = 0;
                            //       let barGraph = 0;

                            //       if (selected != "") {
                            //         selectedGraph = 16;
                            //         barGraph = `${selected[0].row},${selected[0].column}`;

                            //         //console.log(this.state.req_task_distribution_google.getValue(selected[0].row,0));

                            //         if (urlStr === "") {
                            //           urlStr = "q=no";
                            //         }

                            //         let selTab = 1;

                            //         if (selected[0].column == 2) {
                            //           selTab = 2;
                            //         }

                            //         if (selected[0].column == 3) {
                            //           selTab = 3;
                            //         }

                            //         //console.log("Selected ",chartWrapper.chart.getSelection());

                            //         prevHist.push({
                            //           pathname: `/user/my_analytics_details/${selectedGraph}/${barGraph}`,
                            //           state: {
                            //             urlStr: urlStr,
                            //             selectedTab: selTab,
                            //           },
                            //         });
                            //       }
                            //     },
                            //   },
                            // ]}
                          />
                        </div>
                      </div>

                      <div className="bar-description-area">
                        <ul className="bar-description">
                          <li>
                            <span style={{ backgroundColor: "#facf5a" }}></span>
                            Open
                          </li>
                          <li>
                            <span style={{ backgroundColor: "#15cda8" }}></span>
                            Closed
                          </li>
                          <li>
                            <span style={{ backgroundColor: "#ff5959" }}></span>
                            Breached
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <div className="analytics-moudle bg-Green">
                    <div className="chartbg">
                      <div className="d-flex justify-content-center align-item-center midhead">
                        <strong> CSAT score </strong> {""} ={" "}
                        {this.state.tasks_positive > 0 &&
                          this.state.total_riview > 0 ? (
                            <div>
                              {(
                                (this.state.tasks_positive /
                                  this.state.total_riview) *
                                100
                              ).toFixed(2)}
                            %
                            </div>
                          ) : (
                            <div>0%</div>
                          )}
                      </div>
                      <Row>
                        <Col sm={12} md={2}></Col>
                        <Col sm={12} md={8}>
                          <Col sm={12} md={4}>
                            <div className="smilebox">
                              <img
                                src={require("../../assets/images/smile-1.svg")}
                                alt=""
                              />
                              <h3>{this.state.tasks_positive}</h3>
                              {this.state.tasks_positive > 0 &&
                                this.state.total_riview > 0 ? (
                                  <span>
                                    {(
                                      (this.state.tasks_positive /
                                        this.state.total_riview) *
                                      100
                                    ).toFixed(2)}
                                  %
                                  </span>
                                ) : (
                                  <span>0%</span>
                                )}
                              <p>Satisfied Customer</p>
                            </div>
                          </Col>

                          <Col sm={12} md={4}>
                            <div className="smilebox">
                              <img
                                src={require("../../assets/images/smile-2.svg")}
                                alt=""
                              />
                              <h3>{this.state.task_neutral}</h3>
                              {this.state.task_neutral > 0 &&
                                this.state.total_riview > 0 ? (
                                  <span>
                                    {(
                                      (this.state.task_neutral /
                                        this.state.total_riview) *
                                      100
                                    ).toFixed(2)}
                                  %
                                  </span>
                                ) : (
                                  <span>0%</span>
                                )}
                              <p>Neutral Customer</p>
                            </div>
                          </Col>

                          <Col sm={12} md={4}>
                            <div className="smilebox">
                              <img
                                src={require("../../assets/images/smile-3.svg")}
                                alt=""
                              />
                              <h3>{this.state.task_negative}</h3>
                              {this.state.task_negative > 0 &&
                                this.state.total_riview > 0 ? (
                                  <span>
                                    {(
                                      (this.state.task_negative /
                                        this.state.total_riview) *
                                      100
                                    ).toFixed(2)}
                                  %
                                  </span>
                                ) : (
                                  <span>0%</span>
                                )}
                              <p>Unsatisfied Customer</p>
                            </div>
                          </Col>
                        </Col>
                        <Col sm={12} md={2}></Col>
                      </Row>

                      <div className="ratingtabl">
                        <div className="bggrey">
                          <span className="greenclr m-r-10">5 Satisfied</span>
                          <span className="orangeclr m-r-10">4 Neutral</span>
                          <span className="redclr">3 Unsatisfied</span>
                        </div>
                      </div>

                      <div className="ratingtabl">
                        <Table striped bordered hover>
                          <thead>
                            <tr>
                              <th>Unsatisfied</th>
                              <th>Neutral</th>
                              <th>Satisfied</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td className="redclr">
                                <strong>
                                  {" "}
                                  <span className="mr-10"> 1 </span>
                                  <span className="mr-10"> 2 </span>
                                  <span className="mr-10"> 3 </span>
                                </strong>
                              </td>

                              <td className="orangeclr">
                                <strong> 4 </strong>
                              </td>

                              <td className="greenclr">
                                <strong> 5</strong>
                              </td>
                            </tr>
                          </tbody>
                        </Table>
                      </div>
                    </div>
                  </div>
                </div>

              </div>

            )}

            {this.state.showCompanyPopup === true && (
              <MyCompanyPopup closePopup={this.closePopup} {...this.props} />
            )}
          </div>
        </>
      );
    }
  }
}

export default AnalyticsModule;
