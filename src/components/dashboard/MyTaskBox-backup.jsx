import React, { Component } from 'react';

class MyTaskBox extends Component {
    render(){
        return(
            <>
            <div className="col-lg-3 col-sm-6 col-xs-12">
                <div className="small-box bg-Red">
                    <div className="inner">
                    <div className="row clearfix">
                        <div className="col-xs-8">
                        <h3>18</h3>
                        <p><strong>My Tasks</strong></p>
                        </div>
                        <div className="col-xs-4">
                        <div className="icon"><i className="fas fa-tasks"></i></div>
                        </div>
                    </div>
                    </div>
                    <div className="small-box-footer clearfix">
                        <div className="row clearfix">
                            <div className="col-xs-4 col-border">Overdue <span>1</span></div>
                            <div className="col-xs-4 col-border">Due Today <span>4</span></div>
                            <div className="col-xs-4 col-border"><div className="row">Due this Week <span>12</span></div></div>
                        </div>
                    </div>
                </div>
            </div>
            </>
        );
    }
}

export default MyTaskBox;