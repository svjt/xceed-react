import React, { Component } from 'react';
import { Row, Col, ButtonToolbar, Button, Modal } from "react-bootstrap";
import Select from "react-select";
import API from "../../shared/axios";
import { htmlDecode } from '../../shared/helper';
import { showErrorMessageFront } from "../../shared/handle_error_front";

class MyCompanyPopup extends Component {

    state = {
        companyArr: [],
        email_search:'',
        user_details:'',
        popup_loader:false,
        cust_email_errors:''
    }

    constructor(props){
        super(props);
    }

    componentWillMount = () => {
        API.get('/api/employees/company')
        .then(res => {
            var myCompany = [];
            for (let index = 0; index < res.data.data.length; index++) {
                const element = res.data.data[index];
                myCompany.push({
                    value: element["company_id"],
                    label: htmlDecode(element["company_name"])
                });
            }
            this.setState({companyArr:myCompany});
        })
        .catch(err => {
            console.log(err);
        });
    }

    changeCompany = (event) =>{
        // console.log(event.value);
        this.props.history.push(`/user/my_user/${event.value}`);
    }

    handleClose = () => {
        var close = {
            companyArr:[],
            email_search:'',
            user_details:'',
            popup_loader:false
        };
        this.setState(close);
        this.props.closePopup();
    };

    redirectUrl = (event, user_details) => {
        event.preventDefault();

        let id;

        if(user_details.type == 'c'){
            id = user_details.customer_id;
        }else{
            id = user_details.agent_id;
        }

        API.post(`/api/employees/shlogin`, { customer_id: parseInt(id) })
        .then((res) => {
            if (res.data.shadowToken) {
                var url =
                process.env.REACT_APP_CUSTOMER_PORTAL +
                `setToken/` +
                res.data.shadowToken;
                window.open(url, "_blank");
            }
        })
        .catch((error) => {
            showErrorMessageFront(error, 0, this.props);
        });
    };

    render() {
        return (
            <>
                {this.state.companyArr.length > 0 && <Modal show={true} onHide={() => this.handleClose()} backdrop="static">
                   <div className="customer-modal-edit">
                    <Modal.Header closeButton>
                        <Modal.Title>
                        Select a customer from the list below
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="contBox">
                            <Row>
                                <Col xs={12} sm={12} md={12}>
                                <div className="form-group">
                                    <label>Select Customer</label>
                                    <Select
                                        className="basic-single"
                                        classNamePrefix="select"
                                        defaultValue={0}
                                        isClearable={false}
                                        isSearchable={true}
                                        name="employee_id"
                                        options={this.state.companyArr}
                                        onChange={e => { this.changeCompany(e) }}
                                    />
                                </div>
                                </Col>
                            </Row>

                            <Row>
                                <Col xs={12} sm={12} md={9}>
                                <div className="form-group">
                                    <label>Search By User Email</label>
                                    <input 
                                        type="text" 
                                        className="form-control" 
                                        placeholder="User Email" 
                                        value={this.state.email_search} 
                                        onChange={(e)=>{
                                            this.setState({email_search:e.target.value});
                                        }} 
                                        onFocus={(e)=>{
                                            this.setState({cust_email_errors:''})
                                        }}
                                    />
                                </div> 
                                </Col>
                                <Col xs={12} sm={12} md={3}>
                                    <div className="form-group">
                                        <label>{`.`}</label>
                                        <button type="submit" className="btn-fill ml-10" onClick={(e)=>{
                                            e.preventDefault();
                                            this.setState({ popup_loader: true,user_details:''});

                                            const request_body = {
                                                cust_email: this.state.email_search
                                            };

                                            API.post(`/api/employees/search_email`, request_body)
                                            .then((res) => {
                                                this.setState({ popup_loader: false});
                                                //console.log('res',res);
                                                if(res.data.company_id > 0 && res.data.permission == 1){
                                                    this.props.history.push({
                                                        pathname : `/user/my_user/${res.data.company_id}`,
                                                        state :{
                                                            email : this.state.email_search
                                                        }
                                                    });
                                                }else if(res.data.company_id > 0 && res.data.permission == 0){
                                                    this.setState({user_details:res.data.details[0]});
                                                }else{
                                                    this.setState({cust_email_errors:'Sorry No Result Found.'});
                                                }
                                            }).catch((err) => {
                                                //console.log(err);
                                                //console.log(err.data.errors.cust_email);
                                                if(err && err.data){
                                                    this.setState({cust_email_errors:err.data.errors.cust_email});
                                                }
                                            });
                                        }} >
                                            {" "}Search{" "}
                                        </button>
                                    </div>
                                </Col>

                                {this.state.cust_email_errors != '' && <Col xs={12} sm={12} md={12}>
                                <div className="form-group">
                                    <span className="errorMsg">{this.state.cust_email_errors}</span>
                                </div> 
                                </Col>}

                                {this.state.user_details != '' && <Col xs={12} sm={12} md={12}>
                                <div className="form-group searchResult">
                                    <h6><span> {this.state.user_details.user_type_name} Details</span></h6>
                                    <h5><span 
                                    //style={{ cursor: "pointer" }} 
                                    // onClick={(e) =>
                                    //     this.redirectUrl(e, this.state.user_details)
                                    // } 
                                    >Customer: {this.state.user_details.company_name}</span></h5>
                                    <label>User: {this.state.user_details.first_name} {this.state.user_details.last_name}</label>
                                    <label>
                                        <span
                                            // onClick={(e) =>
                                            //     this.redirectUrl(e, this.state.user_details)
                                            // }
                                            // style={{ cursor: "pointer" }}
                                        >
                                            Email: {this.state.user_details.email}
                                        </span>
                                    </label>
                                    {this.state.user_details.type === 'c' && 
                                    <>
                                    {this.state.user_details.status == '0' ? <h4>This customer has not yet been activated by XCEED Admin</h4> : <h4>This customer is not mapped with your account. It is managed by</h4> }
                                    
                                    <Row>
                                    {this.state.user_details.team.map((team)=>{
                                        return (
                                            <>
                                            <Col xs={12} sm={4} md={4} className={`company-popup-email-search`} >
                                                <div class="team-member">
                                                    <div class="name">{team.first_name} {team.last_name}</div>
                                                    <div class="designation">{team.desig_name}</div>
                                                    <p>{team.dept_name}</p>
                                                    <div class="social-link">
                                                        <a href={"mailto:" + team.email} ><i class="fa fa-envelope" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </Col>    
                                            </>
                                        );
                                    })}
                                    </Row>
                                    </>
                                    }
                                </div> 
                                </Col>}

                            </Row>
                            
                        </div>
                    </Modal.Body>
                    </div>
                    </Modal>
                    }
                </>
        );
    }
}

export default MyCompanyPopup;