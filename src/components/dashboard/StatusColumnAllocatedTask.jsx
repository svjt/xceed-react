import React, { Component } from 'react';
import { localDate,inArray } from '../../shared/helper';

import {
    Row,
    Col,
    Tooltip,
    OverlayTrigger
  } from "react-bootstrap";

import { Link } from "react-router-dom";

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
    return (
      <OverlayTrigger
        overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
        placement="top"
        delayShow={300}
        delayHide={150}
        trigger={["hover"]}
      >
        <Link to={href} onClick={clicked}>
          {children}
        </Link>
      </OverlayTrigger>
    );
}

//import {Tooltip,OverlayTrigger} from 'react-bootstrap';

class StatusColumn extends Component {


    isDue = () =>{
        
        var selDueDate = this.props.rowData.due_date;
        var dueDate    = localDate(selDueDate);
        var today      = new Date();
        var timeDiff   = dueDate.getTime() - today.getTime();

        if(timeDiff > 0 ){
            return false;
        }else{
            return true;
        }
    }

    render() {

        //STATUS    
        // var tick,in_progress,exclaim = false;

        // if(this.props.rowData.status === 3 || this.props.rowData.status === 4){
        //     tick = true;
        //     if(this.checkDueDate()){
        //         if(this.props.rowData.vip_customer === 1){
        
        //         }else{
        //             exclaim = true;
        //         }
        //     }

        //     if(this.props.rowData.status == 3){
        //         var tooltip = (
        //             <Tooltip id="tooltip">
        //                 Responded.
        //             </Tooltip>
        //         );
        //     }else if(this.props.rowData.status == 4){
        //         var tooltip = (
        //             <Tooltip id="tooltip">
        //                 Responded To Customer.
        //             </Tooltip>
        //         );
        //     }

        // }else{
        //     if(this.props.rowData.status === 2){
        //         in_progress = true;
        //         if(this.checkDueDate()){
        //             if(this.props.rowData.vip_customer === 1){
            
        //             }else{
        //                 exclaim = true;
        //             }
        //         }
        //     }else if(this.props.rowData.status === 1){
        //         if(this.checkDueDate()){
        //             if(this.props.rowData.vip_customer === 1){
            
        //             }else{
        //                 exclaim = true;
        //             }
        //         }
        //     }
        // }

        return (
            <>
                {inArray(this.props.rowData.cust_resp_status,['Closed','Payment Received','Dispatched'])?
                <div className="actionStyle">
                    <LinkWithTooltip
                        tooltip={`Closure Pending`}
                        href="#"
                        id="tooltip-1"
                        clicked={e => this.checkHandler(e)}
                        >
                        <i>Closure Pending</i>
                    </LinkWithTooltip>
                </div>
                :
                <div className="actionStyle">
                    {this.isDue() && <i>Due</i> }
                    {this.props.rowData.status === 1 && !this.isDue() && <i>Assigned</i> }
                    {this.props.rowData.status === 2 && !this.isDue() && <i>In Progress</i> }
                    {this.props.rowData.status === 3 && !this.isDue() && <i>Responded</i> }
                    {this.props.rowData.order_verified_status == 0 && this.props.rowData.request_type == 23 && <LinkWithTooltip
                          tooltip={`Yet to set Product Code`}
                          href="#"
                          id="tooltip-1"
                          clicked={e => this.checkHandler(e)}
                          >
                          {`  `}<i class="fa fa-exclamation-triangle" aria-hidden="true" style={{color:"#ffcc5f"}} ></i>
                      </LinkWithTooltip> }
                    {this.props.rowData.rdd_pending && this.props.rowData.request_type == 23 && <LinkWithTooltip
                          tooltip={`Approve / Reject changes in RDD`}
                          href="#"
                          id="tooltip-1"
                          clicked={e => this.checkHandler(e)}
                          >
                          {`  `}<i class="fa fa-exclamation-triangle" aria-hidden="true" style={{color:"#ff4c4c"}} ></i>
                      </LinkWithTooltip> }
                </div>}
            </>
        );
    }
}

export default StatusColumn;
