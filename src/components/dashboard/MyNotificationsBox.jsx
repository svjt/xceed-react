import React, { Component } from 'react';
import { Button, ButtonToolbar, Modal } from "react-bootstrap";

class MyNotificationsBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal : false
        };
      }      
    
    modalShowHandler = () => {       
        this.setState({ showModal: true });
    };
    modalCloseHandler = () => {
        this.setState({ showModal: false });
    };

    render(){
        return(
            <>
            <div className="col-lg-3 col-sm-6 col-xs-12">
                <div className="small-box bg-OffGreen">
                    <div className="inner">
                    <div className="row clearfix">
                        <div className="col-xs-8">
                            <h3>0</h3>
                            <p onClick={e => this.modalShowHandler()}><strong>My Notifications</strong></p>
                        </div>
                        <div className="col-xs-4">
                            <div className="icon"><i className="far fa-bell"></i></div>
                        </div>
                    </div>
                    </div>
                    <div className="small-box-footer clearfix">
                        <div className="row clearfix">
                            <div className="col-xs-4 col-border">New <span>0</span></div>
                            <div className="col-xs-4 col-border">Unread <span>0</span></div>
                            <div className="col-xs-4 col-border">Actions <span>0</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <Modal show={this.state.showModal} onHide={() => this.modalCloseHandler()} backdrop="static">
                <Modal.Header>
                    <Modal.Title>&nbsp;</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                <div className="contBox">
                    <span>Coming soon.</span>
                </div>
                </Modal.Body>

                <Modal.Footer>
                        <button 
                        className={`btn btn-danger btn-sm m-r-10`} 
                        type="button" 
                        onClick={e => this.modalCloseHandler()}>Close</button>
                    
                </Modal.Footer>
            </Modal>
            </>
        );
    }
}

export default MyNotificationsBox;