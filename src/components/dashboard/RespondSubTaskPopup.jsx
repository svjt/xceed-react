import React, { Component } from 'react';
import { Row, Col, ButtonToolbar, Button, Modal } from "react-bootstrap";
import axios from "axios";
import { Formik, Field, Form } from "formik"; 
import * as Yup from "yup";
import Loader from 'react-loader-spinner';
import dateFormat from 'dateformat';

const initialValues = {
    description   : "",
    attachment    : ""
  };

class RespondSubTaskPopup extends Component {    

    constructor(props){
        super(props);
        this.state = {
            task_id        : 0,
            showRespondSubTask     : false
        };
    }

    componentWillReceiveProps = (nextProps) => {
        if(nextProps.showRespondSubTask === true && nextProps.task_id > 0){
            var id =1;
            this.setState({showRespondSubTask: nextProps.showRespondSubTask,task_id:nextProps.task_id});
  
            axios.get(`http://10.0.10.222/project/dev/tty_portal/react_services/get_task_details/task_id/${nextProps.task_id}/emp_id/${id}`)
            .then(res => {
                this.setState({ 
                    taskDetails:res.data
                });
            })
            .catch(err => {

            });
        }
    }

    handleClose = () => {
        var close = {showRespondSubTask: false,task_id:0};
        this.setState(close);
        this.props.handleClose(close);
    };

    handleRespond = (values, actions) => {
        const formData = new FormData();
        
        for (const key in values) {
            formData.append(key, values[key]);
        }
    }

    showDueDate = () =>{
        
        return dateFormat(this.state.taskDetails[0].due_date, "dd/mm/yyyy");
    }

    render(){

        const FILE_SIZE = 160 * 1024;
        const SUPPORTED_FORMATS = [
        "image/jpg",
        "image/jpeg",
        "image/gif",
        "image/png"
        ];
        
        const validateStopFlag = Yup.object().shape({
            description : Yup.string().trim()
                .required("Please enter description"),
            attachment  : Yup.mixed()
                .required("A file is required")
                .test(
                  "fileSize",
                  "File too large",
                  value => value && value.size <= FILE_SIZE
                )
                .test(
                  "fileFormat",
                  "Unsupported Format",
                  value => value && SUPPORTED_FORMATS.includes(value.type)
                )
        });

        const newInitialValues = Object.assign(initialValues, {
            description   : "",
            attachment    : ""
          });

        if(this.state.task_id === 0)return null;

        if(typeof this.state.taskDetails === 'undefined' && this.state.showRespondSubTask === true ){
            return(
                <>
                    <Loader 
                        type="Puff"
                        color="#00BFFF"
                        height="100"	
                        width="100"
                        verticalAlign="middle"
                    />
                </>
            );
        }else{
            return(
                <>
                <Modal show={this.state.showRespondSubTask} onHide={() => this.handleClose()} backdrop="static">
                    <Formik
                        initialValues    = {newInitialValues}
                        validationSchema = {validateStopFlag}
                        onSubmit         = {this.handleSubmitAssignTask}
                    >
                        {({ values, errors, touched, isValid, isSubmitting, setFieldValue }) => {
                        return (
                            <Form>
                            <Modal.Header closeButton>
                                <Modal.Title>
                                Respond Sub Task
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div className="contBox">
                                <Row>
                                    <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                        <label>
                                            Task 
                                        </label>
                                        <p>
                                            {this.state.taskDetails[0].task_ref_id}
                                        </p>
                                    </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                        <label>
                                            Description 
                                        </label>
                                        <p>
                                            {this.state.taskDetails[0].description}
                                        </p>
                                    </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                        <label>
                                            Assigned To  
                                        </label>
                                        <p>
                                            {this.state.taskDetails[0].assigned_to}
                                        </p>
                                    </div>
                                    </Col>
                                    <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                        <label>
                                            Due Date  
                                        </label>
                                        <p>
                                            {this.showDueDate()}
                                        </p>
                                    </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                        <Field
                                        name="description"
                                        component="textarea"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.description}
                                        >
                                        </Field>
                                        {errors.description && touched.description ? (
                                        <span className="errorMsg">
                                            {errors.description}
                                        </span>
                                        ) : null}
                                    </div>
                                    </Col>
                                    <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                        <Field
                                        name="attachment"
                                        type="file"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.attachment}
                                        >
                                        </Field>
                                        {errors.attachment && touched.attachment ? (
                                        <span className="errorMsg">
                                            {errors.attachment}
                                        </span>
                                        ) : null}
                                    </div>
                                    </Col>
                                </Row>
                                </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <button
                                    className={`btn btn-success btn-sm ${
                                    isValid ? "btn-custom-green" : "btn-disable"
                                    } m-r-10`}
                                    type="submit"
                                    disabled={isValid ? false : true}
                                >
                                    {this.state.stopflagId > 0
                                    ? isSubmitting
                                        ? "Updating..."
                                        : "Update"
                                    : isSubmitting
                                    ? "Submitting..."
                                    : "Submit"}
                                </button>
                                <button
                                    onClick={this.handleClose}
                                    className={`btn btn-danger btn-sm`}
                                    type="button"
                                >
                                    Close
                                </button>
                            </Modal.Footer>
                            </Form>
                        );
                        }}
                    </Formik>
                    </Modal>
                </>
            );
        }
    }
}

export default RespondSubTaskPopup;