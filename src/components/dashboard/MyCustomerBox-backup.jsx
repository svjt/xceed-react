import React, { Component } from 'react';

class MyCustomerBox extends Component {

    componentDidMount = () => {
            
        API.get(`/api/tasks/customer_tiles`)
        .then(res => {
            
            this.setState({
                custTile : res.data.data
            });
        })
        .catch(err => {
            console.log(err);
        });
    }

    render(){
        return(
            <>
           <div className="col-lg-3 col-sm-6 col-xs-12">
                <div className="small-box bg-Blue">
                    <div className="inner">
                    <div className="row clearfix">
                        <div className="col-xs-4">
                            <h3>25</h3>
                            <p><strong>My Customers</strong></p>
                        </div>
                        <div className="col-xs-4">
                        <div className="icon"><i className="fas fa-users"></i></div>
                        </div>
                        <div className="col-xs-4 keyAccounts">
                            <i className="fa fa-star"></i>
                            <h3>4</h3>
                            <p><strong>Key  Accounts</strong></p>
                        </div>
                    </div>
                    </div>
                    <div className="small-box-footer clearfix">
                        <div className="row clearfix">
                            <div className="col-xs-4 "><div className="row">Open Requests <span>8</span></div></div>
                            <div className="col-xs-4  col-border"><div className="row">New Requests <span>14</span></div></div>
                            <div className="col-xs-2  col-border"><div className="row">Open <span>1</span></div></div>
                            <div className="col-xs-2  col-border"><div className="row">New<span>3</span></div></div>
                        </div>
                    </div>
                </div>
            </div>
            </>
        );
    }
}

export default MyCustomerBox;