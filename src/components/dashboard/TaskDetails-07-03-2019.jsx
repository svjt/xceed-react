import React, { Component } from "react";
import { Row, Col, Button,Panel,PanelGroup } from "react-bootstrap";
import dateFormat from "dateformat";
import API from "../../shared/axios";

import * as Yup from "yup";
import { Formik, Field, Form } from "formik";
import swal from "sweetalert";

import { Redirect, Link } from "react-router-dom";
//import Loader from "react-loader-spinner";
import { getDashboard } from "../../shared/helper";
import commenLogo from "../../assets/images/drreddylogosmall.png";
import pdfIcon from '../../assets/images/pdf-icon.png';

import { getMyId } from '../../shared/helper';

import whitelogo from '../../assets/images/drreddylogo_white.png';

const path = `${process.env.REACT_APP_API_URL}/api/tasks/download/`; // SATYAJIT
const download_path = `${process.env.REACT_APP_API_URL}/api/drl/download/`; // SATYAJIT

// initialize form and their validation
const initialValues = {
	comment: ""
};

const commentSchema = Yup.object().shape({
	comment: Yup.string()
			.trim("Please remove whitespace")
			.strict().required("Please enter your comment")
});


const initialSLAValues = {
	slaStatus: ""
};

const SLASchema = Yup.object().shape({
	slaStatus: Yup.string().required("Please enter SLA status")
});


const priority_arr = [
  { priority_id: 1, priority_value: "Low" },
  { priority_id: 2, priority_value: "Medium" },
  { priority_id: 3, priority_value: "High" }
];

const req_category_arr = [
  { request_category_id: 1, request_category_value: "New Order" },
  { request_category_id: 2, request_category_value: "Other" },
  { request_category_id: 3, request_category_value: "Complaint" },
  { request_category_id: 4, request_category_value: "Forecast" },
  { request_category_id: 5, request_category_value: "Payment" },
  { request_category_id: 6, request_category_value: "Notification" }
];

class TaskDetails extends Component {
  constructor(props) {
    super(props);

    //const {changeStatus} = this.props.location.state;

    //console.log(this.props);
    this.state = {
      taskDetails: [],
      CQT_details: [],
      CQT_loader : false,
      files: [],
      order_number: "",
      task_id: this.props.match.params.id,
      assign_id:this.props.match.params.assign_id
    };
  }

  setCreateDate = date_added => {
    var mydate = new Date(date_added);
    return dateFormat(mydate, "dd/mm/yyyy");
  };

  setDaysPending = due_date => {
    var dueDate = new Date(due_date);
    var today = new Date();
    var timeDiff = dueDate.getTime() - today.getTime();
    if (timeDiff > 0) {
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
      return diffDays;
    } else {
      return 0;
    }
  };

  setPriorityName = priority_id => {
    var ret = "Not Set";
    for (let index = 0; index < priority_arr.length; index++) {
      const element = priority_arr[index];
      if (element["priority_id"] === priority_id) {
        ret = element["priority_value"];
      }
    }

    return ret;
  };

  getSLAStatus = (sla_status) => {
    if(sla_status === 1){
      return "Need Further Info";
    }else if(sla_status === 2){
      return "Closed";      
    }
  }

  setReqCategoryName = req_cate_id => {
    var ret = "Not Set";
    for (let index = 0; index < req_category_arr.length; index++) {
      const element = req_category_arr[index];
      if (element["request_category_id"] === req_cate_id) {
        ret = element["request_category_value"];
      }
    }

    return ret;
  };

  checkPharmacopoeia(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (
        (request_type === 22 ||
          request_type === 21 ||
          request_type === 19 ||
          request_type === 18 ||
          request_type === 17 ||
          request_type === 2 ||
          request_type === 3 ||
          request_type === 4 ||
          request_type === 10 ||
          request_type === 12 ||
          request_type === 1) &&
        parent_id === 0
      ) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkPolymorphicForm(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if ((request_type === 2 || request_type === 3) && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkStabilityDataType(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 13 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkAuditSiteName(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 9 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkAuditVistDate(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 9 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkQuantity(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if ((request_type === 7 || request_type === 23 || request_type === 25) && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkBatchNo(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 7 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkNatureOfIssue(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 7 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkRDD(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 23 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkSamplesWorkingImpureRequest(request_type, parent_id){
    if(request_type === null){
        return false;
    }else{
        if(request_type === 1 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkProductRequest(request_type, parent_id) {
    if(request_type === null){
      return false;
    }else{
        if((request_type !== 24) && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkMarketRequest(request_type, parent_id) {
    if(request_type === null){
      return false;
    }else{
        if((request_type !== 24 && request_type !== 25 && request_type !== 26) && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkForecastNotificationDate(request_type, parent_id) {
    if(request_type === null){
      return false;
    }else{
        if(request_type === 24 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkAmountPending( request_type, parent_id ) {
    if(request_type === null){
      return false;
    }else{
        if(request_type === 25 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkDaysRemaining( request_type, parent_id ) {
    if(request_type === null){
      return false;
    }else{
        if(request_type === 25 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkNotificationRequestType( request_type, parent_id ) {
    if(request_type === null){
      return false;
    }else{
        if(request_type === 26 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkNotificationTargetApproval( request_type, parent_id ) {
    if(request_type === null){
      return false;
    }else{
        if(request_type === 26 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkNotificationTargetImplementation ( request_type, parent_id ){
    if(request_type === null){
      return false;
    }else{
        if(request_type === 26 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkPaymentStatus( request_type, parent_id ) {
    if(request_type === null){
      return false;
    }else{
        if(request_type === 25 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkPaymentDueDate( request_type, parent_id ) {
    if(request_type === null){
      return false;
    }else{
        if(request_type === 25 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  setDateFormat(date_value) {
    var mydate = new Date(date_value);
    //console.log(mydate);
    return dateFormat(mydate, "dd/mm/yyyy");
  }

  getTaskCommentDetails = () => {
    API.get(`/api/tasks/${this.state.task_id}/${this.state.assign_id}`)
      .then(res => {

        if(res.data.data.taskDetails.cqt > 0){

            this.setState({ CQT_loader : true });

            API.get(`/api/drl/${res.data.data.taskDetails.cqt}`)
            .then(res => {
                this.setState({
                    CQT_details : res.data.data,
                    CQT_loader : false
                })                       
            })
            .catch(err => {
              console.log(err);
            });
        }

        this.setState({
          taskDetails: res.data.data.taskDetails,
          files: res.data.data.files,
          subTasks:res.data.data.subTasks,
          comments: res.data.data.comments,
          has_discussion: res.data.data.hasDiscussion
        });

        console.log('taskDetails',this.state.taskDetails);

        //STOP THIS CALL FOR ALLOCATED TASK
        //if(res.data.data.status === 1){
        var post_data = {};
        API.put(`/api/tasks/status/${this.state.task_id}`, post_data)
          .then(response => {
            //console.log(response.data.data);
            //nextProps.refreshTableStatus(nextProps.currRow);
          })
          .catch(err => {
            console.log(err);
          });
        //}
      })
      .catch(err => {
        console.log(err);
      });
  }

  componentDidMount = () => {
    if (this.state.task_id > 0) {
      this.getTaskCommentDetails();
    }
  };

  // componentWillReceiveProps = (nextProps) => {

  //     console.log("next props", nextProps );

  //     if(nextProps.currRow.task_id > 0){
  //         //console.log(nextProps.currRow.task_id);
  //         API.get(`/api/tasks/${nextProps.currRow.task_id}`)
  //         .then(res => {
  //             console.log('MY TASKS');
  //             console.log(res.data.data);
  //             this.setState({
  //                 taskDetails: res.data.data
  //             });

  //             if(typeof nextProps.allocated !== 'undefined' && nextProps.allocated == 1){

  //             }else{
  //                 if(res.data.data.status === 1){
  //                     var post_data = {};
  //                     API.put(`/api/tasks/status/${nextProps.currRow.task_id}`,post_data)
  //                     .then(res => {
  //                         //console.log(res.data.data);
  //                         nextProps.refreshTableStatus(nextProps.currRow);
  //                     })
  //                     .catch(err => {
  //                         console.log(err);
  //                     });
  //                 }
  //             }
  //         })
  //         .catch(err => {
  //             console.log(err);
  //         });
  //     }
  // }

  /* handleClose = () => {
        var close = {showTaskDetails: false};
        this.setState(close);
        this.props.handleClose(close);
    }; */

  createAccordion = () => {

      let accordion = [];
      var sub_tasks = this.state.subTasks;

      for (let index = 0; index < sub_tasks.length; index++) {

          accordion.push(
              <Panel eventKey={sub_tasks[index].task_id} key={index}>
                  <Panel.Heading>
                  <Panel.Title toggle>{sub_tasks[index].task_ref}</Panel.Title>
                  </Panel.Heading>
                  <Panel.Body collapsible>
                      <div className="form-group">
                          <label>
                              Task: 
                          </label>
                          {sub_tasks[index].discussion === 1 &&
                              <Link to={{ pathname: '/user/discussion_details/'+sub_tasks[index].parent_id }} target="_blank" style={{cursor:'pointer'}}>{sub_tasks[index].task_ref}</Link>
                          }
                          {sub_tasks[index].discussion !== 1 &&
                              <Link to={{ pathname: `/user/task_details/${sub_tasks[index].task_id}/${sub_tasks[index].assignment_id}` }} target="_blank" style={{cursor:'pointer'}}>{sub_tasks[index].task_ref}</Link>
                          }
                      </div>
                      
                      <div className="form-group">
                          <label>
                              Description:
                          </label>

                          {sub_tasks[index].parent_id > 0 && sub_tasks[index].title }
                          {sub_tasks[index].parent_id === 0 && sub_tasks[index].req_name }
                      </div>
                      <div className="form-group">
                          <label>
                              Created:
                          </label>
                              {dateFormat(sub_tasks[index].date_added, "dd/mm/yyyy")}
                      </div>
                      {sub_tasks[index].first_name !== '' && <div className="form-group">
                          <label>
                              Customer Name:
                          </label>
                            {`${sub_tasks[index].first_name + " " + sub_tasks[index].last_name}`}
                      </div>}
                      {sub_tasks[index].assigned_by === -1 && <div className="form-group">
                          <label>
                              Assigned By:
                          </label>
                              System
                      </div>}
                      {sub_tasks[index].assigned_by > 0 && <div className="form-group">
                          <label>
                              Assigned By:
                          </label>
                              {`${sub_tasks[index].assigned_by_first_name+' '+sub_tasks[index].assigned_by_last_name+' ('+sub_tasks[index].assign_by_desig_name+')'}`}
                      </div>}
                      <div className="form-group">
                          <label>
                              Assigned To:
                          </label>
                              {`${sub_tasks[index].assigned_to_first_name+' '+sub_tasks[index].assigned_to_last_name+' ('+sub_tasks[index].assign_to_desig_name+')'}`}
                      </div>
                      
                  </Panel.Body>
              </Panel>
          );
      }

      return accordion;
  }

  openCQTattachment = ( filepath ) => {
          API.get(filepath)
          .then(res => {
              window.open( res.data.data, '_blank');
          })
          .catch(err => {
              console.log(err);
          });
  }

  getComments = () => {
    if (this.state.comments.length > 0) {
      const comment_html = this.state.comments.map((comm, i) => (
        <div className="comment-list-wrap" key={i}>
          <div className="comment">
            <div className="row">
              <div className="col-md-10 col-sm-10 col-xs-9">
                <div className="imageArea">
                  <div className="imageBorder">
                    <img alt="noimage" src={commenLogo} />
                  </div>
                </div>
                <div className="conArea">
                  <p>
                    {comm.first_name} {comm.last_name}
                  </p>
                  <span>{comm.comment}</span>
                </div>
                <div className="clearfix" />
              </div>
              <div className="col-md-2 col-sm-2 col-xs-3">
                <div className="dateArea">
                  <p>{this.setDateFormat(comm.post_date)}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      ));

      return (
        <>
          <div className="disHead">
            <label>Task Comments:</label>
          </div>
          <div className="comment-list-main-wrapper">{comment_html}</div>
        </>
      );
    }
  };

  getCQT = () => {

    if(this.state.CQT_details && this.state.CQT_details !== "" && this.state.CQT_loader === false){

        const { CQT_details } = this.state;

        return (
          <>
            <div className="disHead">
              <label>CQT Details:</label>
            </div>
            <div className="row cqtDetailsDeta-btm">
                
                <div className="col-md-4 col-sm-6 col-xs-12">
                  <div className="form-group">
                    <label>Title :</label>
                      {CQT_details.title}
                  </div>
                </div>

                <div className="col-md-4 col-sm-6 col-xs-12">
                  <div className="form-group">
                    <label>Product Name :</label>
                      {CQT_details.product_name}
                  </div>
                </div>

                <div className="col-md-4 col-sm-6 col-xs-12">
                  <div className="form-group">
                    <label>Country Name :</label>
                      {CQT_details.country_name}
                  </div>
                </div>

                <div className="col-md-4 col-sm-6 col-xs-12">
                  <div className="form-group">
                    <label>Query Category :</label>
                      {CQT_details.query_category}
                  </div>
                </div>

                <div className="col-md-4 col-sm-6 col-xs-12">
                  <div className="form-group">
                    <label>Query Description :</label>
                      { CQT_details.query_description }
                  </div>
                </div>

                <div className="col-md-4 col-sm-6 col-xs-12">
                  <div className="form-group">
                    <label>Criticality :</label>
                      {CQT_details.criticality}
                  </div>
                </div>
               

                <div className="col-md-4 col-sm-6 col-xs-12">
                  <div className="form-group">
                    <label>Assigned To :</label>
                      {CQT_details.assigned_to}
                  </div>
                </div>

                <div className="col-md-4 col-sm-6 col-xs-12">
                  <div className="form-group">
                    <label>Status :</label>
                      {CQT_details.status}
                  </div>
                </div>


            </div>

                  <div className="disHead">
                    <label>CQT Attachment: ({CQT_details.files.length})</label>
                  </div>   
                  
                  {CQT_details.files.length > 0 &&                     
                        <div className="cqtDetailsDeta-btm">
                          <ul className='cqtAttachment'>

                                {CQT_details.files.map((file, p) => (                            
                                  <li key={p}>
                                    <div className="file-base">
                                      <div className="file-baseName">{file.BaseName} </div>
                                      <div className="file-attactment">
                                        <small>| {file.File_x0020_Type} |</small>
                                        {/* <a href="" onClick={this.openCQTattachment(`${download_path}${file.ID}`)} download rel="noopener noreferrer"> */}

                                          <span onClick={(e) => this.openCQTattachment(`${download_path}${file.ID}`)}>
                                              <i className="fa fa-download"></i>
                                          </span>

                                        {/* </a> */}
                                      </div>
                                    </div>
                                  </li> 
                                  ))
                                }

                          </ul>
                      </div>                    
                  }

          </>
        );
    } else {
          return (
              <div className="loderOuter">
                <div className="loading_reddy_outer">
                    <div className="loading_reddy" >
                        <img src={whitelogo} alt="logoImage"/>
                    </div>
                </div>
              </div>
          );
      }
  } 

  handleSapChange = (e) => {
    this.setState({
      order_number : e.target.value
    }) 
  }

  getSalesOrder = () => {
    this.setState({
      sales_result : ""
    })

    API
      .post(`api/bundle/salesorder`, { 'order_number' : this.state.order_number })
      .then(res => {
          this.setState({
            sales_result : res.data.data
          })
      })
      .catch(err => {
          console.log(err);
      });
  }

  getSAP = () => {

    return (
      <>
        <div className="disHead">
          <label>SAP</label>
        </div>
        <div className="row cqtDetailsDeta-btm">
            
            <div className="col-md-4 col-sm-6 col-xs-12">
              <div className="form-group">
                <label>Enter the sales order number ( 0000000001, 0000000002, 0000000003, 0000000006, 0000000009 ) :</label>
                <input type="text" name="order_number" onChange={(e) => this.handleSapChange(e)}/>
                <input type="submit" onClick={this.getSalesOrder}/>
              </div>
            </div>

               {this.state.sales_result && 
                  <div>
                    <p>PO Order Status : <strong> {this.state.sales_result.OrderStatus}</strong></p>
                    <p>Sales Order Number : <strong> {this.state.sales_result.OrderNumber}</strong></p>
                    <p>LC status : <strong> {this.state.sales_result.LCStatus}</strong></p>
                    <p>CoA : <strong> {this.state.sales_result.CoA}</strong></p>
                    <p>Credit Block Status : <strong> {this.state.sales_result.BlockStatus}</strong></p>
                  </div>
               }

        </div>
      </>
    )
  }

  handleChange = (e, field) => {   
      this.setState({
          [field]: e.target.value
      });
  };

  splitQuantity = (quantity) => {
    if(quantity){
      return quantity.split(" ");
    }
  }

  submitComment = (values, actions) => {
      let tid = this.props.match.params.id;
      let aid = this.props.match.params.assign_id;
      if( tid ) {
          var postData = {comment:values.comment,assign_id:aid};
          API
          .post(`api/tasks/respond_back/${tid}`,postData)
          .then(res => {
              //this.getCommentList( tid );
              values.comment ='';
              swal("Comment posted successfully!", {
                  icon: "success"
              })
              this.getTaskCommentDetails();
          })
          .catch(err => {
              console.log(err);
          });
      }
  }

  submitSLA = (values, actions) => {
    let tid = this.props.match.params.id;
    var postData = {sla_status:values.slaStatus};
    API
    .post(`api/tasks/update_sla/${tid}`,postData)
    .then(res => {
        //this.getCommentList( tid );
        values.slaStatus ='';
        swal("SLA Status Updated!", {
            icon: "success"
        })
        this.getTaskCommentDetails();
    })
    .catch(err => {
        console.log(err);
    });

}

  closeBrowserWindow = () => {
    window.close()
  } 

  // downloadFile = (e,file_id) => {
  //   e.preventDefault();
  //   if(file_id){
  //       API
  //       .get(`api/tasks/download/${file_id}`)
  //       .then(res => {
  //           return true;
  //       })
  //       .catch(err => {
  //           console.log(err);
  //       });
  //   }
  // }

  render() {
    //console.log(window.location.origin)
    //SVJT
    var my_id = getMyId();
    // SATYAJIT
    if (this.props.match.params.id === 0 || this.props.match.params.id === "") {
      return <Redirect to="/user/dashboard" />;
    }

    const { taskDetails } = this.state;

    //console.log("task details =====",  taskDetails );
    
    if (taskDetails.task_id > 0) {      
      if (this.state.files.length > 0) {
        var fileList = this.state.files.map((file,k) => (
          <li key={k}>
            <img alt="noimage" src={pdfIcon}/>
            <a href={`${path}${file.upload_id}`} target="_blank" download rel="noopener noreferrer">
              {file.actual_file_name}
            </a>
            {file.source === 1 ? <small> (Uploaded by Employee)</small> : ""}
          </li>
        ));
      }
      return (
        <>
          <div className="content-wrapper">
          <section className="content-header"><h1>{taskDetails.req_name}</h1></section>
            <section className="content">
            {taskDetails.parent_id > 0 && taskDetails.discussion !== 1 && taskDetails.sla_status === 0 &&
              <div className="boxPapanel content-padding">
                <div className="taskdetails">
                  {/* taskdetails Panel Header*/}
                  <div className="taskdetailsHeader">

                        <Formik
                        initialValues={initialSLAValues}
                        validationSchema={SLASchema}
                        onSubmit={this.submitSLA}
                        >
                        {({
                          values,
                          errors,
                          touched,
                          handleChange,
                          handleBlur,
                          setFieldValue
                        }) => {

                          return (
                            
                              <div className="clearfix tdBtmlist">
                              <Form>
                                  <div className="commentBox1">
                                    <div className="row">
                                      <div className="col-sm-8 col-xs-6">
                                      {/* taskdetails Panel Top Listing*/}
                                        <div className="tdTplist">
                                          <ul>
                                            <li>
                                              <div className="form-group">
                                                <label>Task:</label> {taskDetails.task_ref}
                                              </div>
                                            </li>
                                            <li>
                                              <div className="form-group">
                                                <label>Created On:</label>{" "}
                                                {this.setCreateDate(
                                                  taskDetails.date_added
                                                )}
                                              </div>
                                            </li>
                                            <li>
                                              <div className="form-group">
                                                <label>Days Pending:</label>{" "}
                                                {this.setDaysPending(taskDetails.due_date)}
                                              </div>
                                            </li>
                                            <li>
                                              <div className="form-group">
                                                <label>Priority:</label>{" "}
                                                {this.setPriorityName(
                                                  taskDetails.priority
                                                )}
                                              </div>
                                            </li>
                                            {taskDetails.parent_id > 0 && taskDetails.discussion !== 1 && taskDetails.sla_status !== 0 &&
                                              <li>
                                                <div className="form-group">
                                                  <label>SLA Status:</label> {this.getSLAStatus(taskDetails.sla_status)}
                                                </div>
                                              </li>
                                            }
                                          </ul>
                                        </div>
                                        {/* taskdetails Panel Top Listing*/}
                                      </div>
                                      <div className="col-sm-4 col-xs-6">
                                      <div className="row">
                                        <div className="col-sm-8 col-xs-8">
                                          <div className="form-group">
                                              <Field
                                                  name="slaStatus"
                                                  component="select"
                                                  className="form-control"
                                                  autoComplete="off"
                                                  value={values.slaStatus}
                                              >
                                                  <option value="0" key="0" >Select</option>
                                                  <option value="1" key="1" >Need Further Info</option>
                                                  <option value="2" key="2" >Closed</option>
                                              </Field>
                                              {errors.slaStatus && touched.slaStatus && <label className="redError">{errors.slaStatus}</label>}
                                          </div>
                                        </div>
                                        <div className="col-sm-2 col-xs-2">
                                          <div className="button-footer buttonSmall">
                                              <Button 
                                                  type="submit" 
                                                  className="btn btn-danger btn-flat"
                                              > SAVE </Button>
                                          </div>
                                        </div>
                                    </div>
                                  </div>
                                  </div>
                                </div>
                              </Form>
                              </div>
                              );
                        }}
                        </Formik>

                  </div>
                  {/* taskdetails Panel Header*/}

                  
                </div>
              </div>}


                <div className="boxPapanel content-padding">
                <div className="taskdetails">
                  {/* taskdetails Panel Btm Listing Start*/}

                  <div className="clearfix tdBtmlist">                    
                    <Row className="task_details_complete">

                    {taskDetails.parent_id === 0 &&
                      taskDetails.request_type !== 25 && 
                      taskDetails.request_type !== 26 && 
                      taskDetails.closed_status !== 1 &&
                      (getDashboard() === "BM" ||
                        getDashboard() === "CSC") && (
                        <Link
                          to={`/user/edit_task_details/${taskDetails.task_id}/${taskDetails.assignment_id}`}
                          className="plus-request edit-button"
                        >
                          <i className="far fa-edit" aria-hidden="true" />
                          Edit
                        </Link>
                      )}  
                      

                      {(taskDetails.first_name !== "" ||
                        taskDetails.last_name !== "") && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Customer Name:</label>{" "}
                            {taskDetails.first_name}{" "}
                            {taskDetails.last_name}
                          </div>
                        </Col>
                      )}

                      {this.checkProductRequest(taskDetails.request_type,
                      taskDetails.parent_id) 
                       &&
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Product:</label>{" "}
                            {taskDetails.product_name}
                          </div>
                        </Col>
                      }

                      {this.checkMarketRequest(taskDetails.request_type,
                      taskDetails.parent_id) 
                        && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Market:</label>{" "}
                            {taskDetails.country_name}
                          </div>
                        </Col>
                      )}

                      {this.checkSamplesWorkingImpureRequest(
                        taskDetails.request_type,
                        taskDetails.parent_id
                        ) === true && taskDetails.service_request_type.indexOf('Samples') !== -1 && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                          
                            <label>Samples:</label>
                            <p>Number of batches : {taskDetails.number_of_batches}</p>
                            {taskDetails.quantity && 
                              <p>Quantity : {this.splitQuantity(taskDetails.quantity)[0]} {this.splitQuantity(taskDetails.quantity)[1]}</p>
                            }
                            
                          </div>
                        </Col>
                      )}

                      {this.checkSamplesWorkingImpureRequest(
                        taskDetails.request_type,
                        taskDetails.parent_id
                        ) === true && taskDetails.service_request_type.indexOf('Working Standards') !== -1 && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Working Standards:</label>
                            {taskDetails.working_quantity && 
                              <p>Quantity : {this.splitQuantity(taskDetails.working_quantity)[0]} {this.splitQuantity(taskDetails.working_quantity)[1]} </p>
                            }
                          </div>
                        </Col>
                      )}

                      {this.checkSamplesWorkingImpureRequest(
                        taskDetails.request_type,
                        taskDetails.parent_id
                        ) === true && taskDetails.service_request_type.indexOf('Impurities') !== -1 && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Impurities:</label>
                            <p>Required impurity : {taskDetails.specify_impurity_required}</p>

                            {taskDetails.impurities_quantity && 
                              <p>Quantity : {this.splitQuantity(taskDetails.impurities_quantity)[0]} {this.splitQuantity(taskDetails.impurities_quantity)[1]} </p>
                            }
                          </div>
                        </Col>
                      )}

                      {this.checkSamplesWorkingImpureRequest(
                        taskDetails.request_type,
                        taskDetails.parent_id
                        ) === true && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Shipping Address:</label>
                            <p>{taskDetails.shipping_address}</p>
                          </div>
                        </Col>
                      )}

                      {/* post */}
                      {this.checkPharmacopoeia(
                        taskDetails.request_type,
                        taskDetails.parent_id
                      ) === true && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Pharmacopoeia:</label>{" "}
                            {taskDetails.pharmacopoeia}
                          </div>
                        </Col>
                      )}

                      {/* post */}
                      {this.checkPolymorphicForm(
                        taskDetails.request_type,
                        taskDetails.parent_id
                      ) === true && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Polymorphic Form:</label>{" "}
                            {taskDetails.polymorphic_form}
                          </div>
                        </Col>
                      )}

                      {taskDetails.parent_id === 0 && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Request Category:</label>{" "}
                            {this.setReqCategoryName(
                              taskDetails.request_category
                            )}
                          </div>
                        </Col>
                      )}

                      
                      {/* post */}
                      {this.checkStabilityDataType(
                        taskDetails.request_type,
                        taskDetails.parent_id
                      ) === true && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Stability Data Type:</label>{" "}
                            {taskDetails.stability_data_type}
                          </div>
                        </Col>
                      )}

                      {/* post */}
                      {this.checkAuditSiteName(
                        taskDetails.request_type,
                        taskDetails.parent_id
                      ) === true && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Site Name:</label>{" "}
                            {taskDetails.audit_visit_site_name}
                          </div>
                        </Col>
                      )}

                      {/* post */}
                      {this.checkAuditVistDate(
                        taskDetails.request_type,
                        taskDetails.parent_id
                      ) === true && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Audit/Visit Date:</label>{" "}
                            {this.setDateFormat(
                              taskDetails.request_audit_visit_date
                            )}
                          </div>
                        </Col>
                      )}

                      {this.checkQuantity(
                        taskDetails.request_type,
                        taskDetails.parent_id
                      ) === true && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Quantity:</label>{" "}
                            {taskDetails.quantity}
                          </div>
                        </Col>
                      )}

                      {this.checkBatchNo(
                        taskDetails.request_type,
                        taskDetails.parent_id
                      ) === true && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Batch No:</label>{" "}
                            {taskDetails.batch_number}
                          </div>
                        </Col>
                      )}

                      {this.checkForecastNotificationDate(
                        taskDetails.request_type,
                        taskDetails.parent_id
                      ) === true && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Notification Date:</label>{" "}
                            {this.setCreateDate(
                              taskDetails.date_added
                            )}
                          </div>
                        </Col>
                      )}

                      {this.checkNatureOfIssue(
                        taskDetails.request_type,
                        taskDetails.parent_id
                      ) === true && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Nature Of Issue:</label>{" "}
                            {taskDetails.nature_of_issue}
                          </div>
                        </Col>
                      )}      

                      {this.checkRDD(
                        taskDetails.request_type,
                        taskDetails.parent_id
                      ) === true && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Required Date Of Delivery:</label>{" "}
                            {taskDetails.rdd !== null && this.setCreateDate(taskDetails.rdd) }
                          </div>
                        </Col>
                      )}                

                      {taskDetails.parent_id > 0 && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Title:</label> {taskDetails.title}
                          </div>
                        </Col>
                      )}

                      {taskDetails.parent_id > 0 && (
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <label>Content:</label>{" "}
                            {taskDetails.content}
                          </div>
                        </Col>
                      )}

                      {this.checkAmountPending(
                        taskDetails.request_type,
                        taskDetails.parent_id
                      ) === true && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Amount Pending:</label>{" "}
                            {taskDetails.payment_pending}
                          </div>
                        </Col>
                      )}

                      {this.checkDaysRemaining(
                        taskDetails.request_type,
                        taskDetails.parent_id
                      ) === true && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Days Remaining:</label>{" "}
                            {taskDetails.days_remaining}
                          </div>
                        </Col>
                      )}

                      {this.checkPaymentStatus(
                        taskDetails.request_type,
                        taskDetails.parent_id
                      ) === true && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>status:</label>{" "}
                            {taskDetails.payment_status}
                          </div>
                        </Col>
                      )}


                      {this.checkPaymentDueDate(
                        taskDetails.request_type,
                        taskDetails.parent_id
                      ) === true && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Due Date:</label>{" "}
                            {this.setCreateDate(
                              taskDetails.due_date
                            )}
                          </div>
                        </Col>
                      )}

                      {this.checkNotificationRequestType(
                        taskDetails.request_type,
                        taskDetails.parent_id
                      ) === true && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Request Type:</label>{" "}
                            {taskDetails.notification_type}
                          </div>
                        </Col>
                      )}
                      
                      {this.checkNotificationTargetApproval(
                        taskDetails.request_type,
                        taskDetails.parent_id
                      ) === true && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Target for Approval/Feedback:</label>{" "}
                            {this.setCreateDate( taskDetails.target_for_approval)}
                          </div>
                        </Col>
                      )}

                      {this.checkNotificationTargetImplementation(
                        taskDetails.request_type,
                        taskDetails.parent_id
                      ) === true && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Target for Implementation:</label>{" "}
                            {this.setCreateDate(taskDetails.target_for_implementation)}
                          </div>
                        </Col>
                      )}

                      {/* post */}
                      {taskDetails.parent_id === 0 && (
                        <Col xs={12} sm={6} md={4}>
                          <div className="form-group">
                            <label>Requirement:</label>{" "}
                            {taskDetails.content}
                          </div>
                        </Col>
                      )}

                        {this.state.files.length > 0 &&
                          <Col xs={12}>
                              <div className="mb-20"> 
                                  <label>Attachment</label>
                                  <div className="cqtDetailsDeta-btm">
                                    <ul className='conDocList'>{fileList}</ul>  
                                   </div>                                                                        
                              </div>
                          </Col>
                        }
                    </Row>
                    
                                    
                    {this.state.subTasks.length > 0 && 
                      <>
                      <label>Sub Tasks:</label>

                      {taskDetails.parent_id === 0 && 
                       this.state.has_discussion === 1 &&
                       <Row>
                         
                       <Col xs={12} sm={6} md={12}>
                          <div className="form-group">
                          <Link
                            to={{ pathname: '/user/discussion_details/'+taskDetails.task_id }} target="_blank" style={{cursor:'pointer'}}
                            className="plus-request edit-button"
                          >
                            <i className="far fa-edit" aria-hidden="true" />
                            Discussions
                          </Link>
                          </div>
                        </Col>
                        </Row>
                        
                      }   
                      <br></br>
                      
                      <PanelGroup accordion id="task_panel" defaultActiveKey={this.state.subTasks[0].task_id} >
                          {this.createAccordion()}
                      </PanelGroup>
                      </>
                    }     

                            

                  </div>

                  {taskDetails.cqt > 0 ? this.getCQT() : ""}

                  {taskDetails.request_type === 23 ? this.getSAP() : ""}
                  
                  {/* taskdetails Panel Btm Listing End*/}

                  {this.getComments()}

                  {taskDetails.assigned_to === my_id && taskDetails.assigned_by > 0 && taskDetails.responded === 0 && 
                  <Formik
                        initialValues={initialValues}
                        validationSchema={commentSchema}
                        onSubmit={this.submitComment}
                  >
                      {({
                          values,
                          errors,
                          touched,
                          handleChange,
                          handleBlur,
                          setFieldValue
                      }) => {

                          return (
                              <div className="clearfix tdBtmlist">
                              <Form>
                                  <div className="commentBox1">
                                      <div className="form-group">
                                          <label>Enter your comment</label>
                                          
                                          <Field
                                              name="comment"
                                              component='textarea'
                                              className="form-control"
                                              autoComplete="off"
                                              placeholder="Comment here"
                                              onChange={handleChange}
                                          />
                                          {errors.comment && touched.comment && <label className="redError">{errors.comment}</label>}
                                      </div>
                                      <div className="button-footer">
                                          <Button 
                                              type="submit" 
                                              className="btn btn-success btn-flat"
                                          > Submit </Button>
                                      
                                          <Button
                                              onClick={this.closeBrowserWindow}
                                              className={`btn btn-danger btn-flat`}
                                              type="button"
                                          >
                                              Cancel
                                          </Button>
                                      </div>
                                  </div>
                              </Form>
                              </div>
                              );
                      }}
                    </Formik>}
                </div>
              </div>
            </section>
          </div>
        </>
      );
    } else {
      return (
        <>
          <div className="loderOuter">
              <div className="loading_reddy_outer">
                  <div className="loading_reddy" >
                      <img src={whitelogo} alt="loaderImage"/>
                  </div>
              </div>
            </div>
        </>
      );
    }
  }
}

export default TaskDetails;
