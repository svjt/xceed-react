import React, { Component } from "react";
import { Button } from "react-bootstrap";
import dateFormat from "dateformat";
import API from "../../shared/axios";

//import TinyMCE from 'react-tinymce';
import { FilePond } from "react-filepond";
import "filepond/dist/filepond.min.css";

//import Dropzone from 'react-dropzone'
//import classNames from 'classnames'

import { Redirect } from "react-router-dom";
//import Loader from "react-loader-spinner";
import commenLogo from "../../assets/images/drreddylogosmall.png";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
//import paperClip from '../../assets/images/paper-clip.jpg';
//import pdfIcon from '../../assets/images/pdf-icon.png';
//import docIcon from '../../assets/images/doc-icon.png';
//import imgIcon from '../../assets/images/img-icon.png';
//import {Link} from "react-router-dom"

import { 
  getMyDrupalId,
  localDate,
  localDateTime,
  htmlDecode 
} from "../../shared/helper";

import * as Yup from "yup";
import { Formik, Field, Form } from "formik";
import swal from "sweetalert";

import ReactHtmlParser from "react-html-parser";

const path = `${process.env.REACT_APP_API_URL}/api/tasks/download_discussion/`; // SATYAJIT

// initialize form and their validation
const initialValues = {
  comment: "",
  file: "",
};

const commentSchema = Yup.object().shape({
  comment: Yup.string().trim()
    .trim("Please remove whitespace")
    .strict()
    .required("Please enter your comment"),
});
// end here

class DiscussionDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      comment: "",
      discussDetails: [],
      task_id: this.props.match.params.did,
      isLoading: true,
      loadDiscussion: false,
      files: [],
    };
  }

  handleChange = (e, field) => {
    this.setState({
      [field]: e.target.value,
    });
  };

  getCommentList = (id) => {
    API.get(`api/tasks/discussions/${id}`)
      .then((res) => {
        //console.log("download list", res);

        this.setState({
          discussDetails: res.data.data.discussion,
          discussStatus: res.data.data.status,
          discussTaskRef: res.data.data.task_ref,
          discussReqName: res.data.data.req_name,
          discussFile: res.data.data.files,
          isLoading: false,
        });
        //console.log("state",  this.state );
      })
      .catch((err) => {
        console.log(err);
      });
  };

  componentDidMount = () => {
    if (this.state.task_id > 0) {
      this.getCommentList(this.state.task_id);
      initialValues.comment = this.state.comment;
    }
  };

  submitComment = (values, actions) => {
    this.setState({ loadDiscussion: true });
    let tid = this.props.match.params.did;
    //console.log('files',this.state.files);
    var formData = new FormData();

    if (this.state.files && this.state.files.length > 0) {
      for (const file of this.state.files) {
        formData.append("file", file);
      }
      //formData.append('file', this.state.file);
    } else {
      formData.append("file", "");
    }

    formData.append("comment", values.comment);

    const config = {
      headers: {
        "content-type": "multipart/form-data",
      },
    };
    //console.log('tid', values.comment);return false;
    if (tid) {
      API.post(`api/tasks/discussions/${tid}`, formData, config)
        .then((res) => {
          this.getCommentList(tid);
          values.comment = "";
          this.setState({
            files: [],
          });

          this.setState({ loadDiscussion: false });
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Comment posted successfully.",
            icon: "success",
          });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  closeBrowserWindow = () => {
    window.close();
  };

  handleTabs = (event) => {
    if (event.currentTarget.className === "active") {
      //DO NOTHING
    } else {
      var elems = document.querySelectorAll('[id^="tab_"]');
      var elemsContainer = document.querySelectorAll('[id^="show_tab_"]');
      var currId = event.currentTarget.id;

      for (var i = 0; i < elems.length; i++) {
        elems[i].classList.remove("active");
      }

      for (var j = 0; j < elemsContainer.length; j++) {
        elemsContainer[j].style.display = "none";
      }

      event.currentTarget.classList.add("active");
      event.currentTarget.classList.add("active");
      document.querySelector("#show_" + currId).style.display = "block";
    }
    // this.tabElem.addEventListener("click",function(event){
    //     alert(event.target);
    // }, false);
  };

  getCommentFile = (uploads) => {
    if (typeof uploads === "undefined") {
      return "";
    } else {
      return (
        <ul className="conDocList">
          {uploads.map((files, j) => (
            <li key={j}>
              <a
                href={`${path}${files.upload_id}`}
                target="_blank"
                download
                rel="noopener noreferrer"
              >
                {files.actual_file_name}
              </a>
            </li>
          ))}
        </ul>
      );
    }
  };

  getDiscussionForm = () => {
    return (
      <Formik
        initialValues={initialValues}
        validationSchema={commentSchema}
        onSubmit={this.submitComment}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          setFieldValue,
        }) => {
          //console.log(errors);

          return (
            <div className="clearfix tdBtmlist">
              <Form encType="multipart/form-data">
                <div className="commentBox">
                  <div className="form-group">
                    {/* <TinyMCE
                          name="comment"
                          ref={ref => this.tyneMceBox = ref}
                          content={values.comment}
                          config={{
                            menubar: false,
                            branding: false,
                              toolbar: 'undo redo | bold italic | alignleft aligncenter alignright'
                          }}
                          className={`selectArowGray form-control`}
                          autoComplete="off"
                          onChange={value =>
                              setFieldValue("comment", value.level.content)
                          }
                      /> */}

                    <Field
                      name="comment"
                      component="textarea"
                      className={`selectArowGray form-control`}
                      autoComplete="off"
                      onChange={(value) =>
                        setFieldValue("comment", value.level.content)
                      }
                    />
                    {errors.comment && touched.comment ? (
                      <span className="errorMsg">{errors.comment}</span>
                    ) : null}
                  </div>
                  <div
                    className="form-group"
                    style={{ borderStyle: "dashed", borderWidth: "1px" }}
                  >
                    <FilePond
                      ref={(ref) => (this.pond = ref)}
                      allowMultiple={true}
                      maxFiles={10}
                      onupdatefiles={(fileItems) => {
                        // Set currently active file objects to this.state
                        this.setState({
                          files: fileItems.map((fileItem) => fileItem.file),
                        });
                      }}
                    ></FilePond>
                  </div>

                  <div className="button-footer commentBox-footer">
                    <Button type="submit" className="btn btn-danger btn-flat">
                      {" "}
                      Send{" "}
                    </Button>
                  </div>
                </div>
              </Form>
            </div>
          );
        }}
      </Formik>
    );
  };

  render() {
    //SATYAJIT
    if (
      this.props.match.params.did === 0 ||
      this.props.match.params.did === ""
    ) {
      return <Redirect to="/user/dashboard" />;
    }

    if (this.state.isLoading === true) {
      return (
        <>
          <div className="loderOuter">
            <div className="loader">
              <img src={loaderlogo} alt="logo" />
              <div className="loading">Loading...</div>
            </div>
          </div>
        </>
      );
    } else {
      if (this.state.task_id > 0) {
        return (
          <>
            <div className="content-wrapper">
              <section className="content">
                <div className="row">
                  <div className="col-xs-12">
                    <div className="nav-tabs-custom">
                      <ul className="nav nav-tabs">
                        <li
                          className="active"
                          onClick={(e) => this.handleTabs(e)}
                          id="tab_1"
                        >
                          DISCUSSION DETAILS
                        </li>
                        <li onClick={(e) => this.handleTabs(e)} id="tab_2">
                          FILES
                        </li>
                      </ul>

                      <div className="tab-content">
                        <div className="tab-pane active" id="show_tab_1">
                          <section className="content-header">
                            <h1>Discussion Details</h1>
                          </section>
                          <section className="content">
                            <div className="boxPapanel content-padding">
                              <div className="disHead">
                                <h3>
                                  {this.state.discussReqName} |{" "}
                                  {this.state.discussTaskRef}
                                </h3>
                              </div>

                              <div className="comment-list-main-wrapper">
                                {this.state.discussDetails.map(
                                  (comments, i) => (
                                    <div key={i} className="comment">
                                      <div className="row">
                                        <div className="col-md-10 col-sm-10 col-xs-9">
                                          <div className="imageArea">
                                            <div className="imageBorder">
                                              <img
                                                alt="noimage"
                                                src={commenLogo}
                                              />
                                            </div>
                                          </div>
                                          <div className="conArea">
                                            <p>
                                              {comments.added_by_type ===
                                                "C" && (
                                                <p>{`${comments.cust_fname} ${comments.cust_lname}`}</p>
                                              )}

                                              {comments.added_by_type ===
                                                "E" && (
                                                <p>{`${comments.emp_fname} ${comments.emp_lname} (DRL)`}</p>
                                              )}

                                              {comments.added_by_type ===
                                                "A" && (
                                                <p>{`${comments.agnt_first_name} ${comments.agnt_last_name} (Agent)`}</p>
                                              )}
                                            </p>

                                            <span>
                                              {ReactHtmlParser(
                                                htmlDecode(comments.comment)
                                              )}
                                            </span>
                                            {this.getCommentFile(
                                              comments.uploads
                                            )}
                                          </div>
                                          <div className="clearfix" />
                                        </div>

                                        <div className="col-md-2 col-sm-2 col-xs-3">
                                          <div className="dateArea">
                                            <p>
                                              {dateFormat(
                                                localDate(comments.date_added),
                                                "ddd, mmm dS, yyyy"
                                              )}
                                            </p>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  )
                                )}
                              </div>
                            </div>
                          </section>
                        </div>

                        <div className="tab-pane" id="show_tab_2">
                          <section className="content-header">
                            <h1>File Details</h1>
                          </section>
                          <section className="content">
                            <div className="boxPapanel content-padding">
                              <div className="disHead">
                                <h3>
                                  {this.state.discussReqName} |{" "}
                                  {this.state.discussTaskRef}
                                </h3>
                              </div>

                              <div className="comment-list-main-wrapper">
                                {this.state.discussFile &&
                                  this.state.discussFile !== "" &&
                                  this.state.discussFile.map((file, i) => (
                                    <div key={i} className="comment">
                                      <div className="row">
                                        <div className="col-md-10 col-sm-10 col-xs-9">
                                          <div className="imageArea">
                                            <div className="imageBorder">
                                              <img
                                                alt="noimage"
                                                src={commenLogo}
                                              />
                                            </div>
                                          </div>
                                          <div className="conArea">
                                            <a
                                              href={`${path}${file.upload_id}`}
                                              target="_blank"
                                              download
                                              rel="noopener noreferrer"
                                            >
                                              {file.actual_file_name}
                                            </a>
                                          </div>
                                          <div className="clearfix" />
                                        </div>

                                        <div className="col-md-2 col-sm-2 col-xs-3">
                                          <div className="dateArea">
                                            <p>
                                              {dateFormat(
                                                localDate(file.date_added),
                                                "ddd, mmm dS, yyyy"
                                              )}
                                            </p>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  ))}
                              </div>
                            </div>
                          </section>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </>
        );
      }
    }
  }
}

export default DiscussionDetails;
