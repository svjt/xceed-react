import React, { Component } from "react";

import { Row, Col, ButtonToolbar, Button, Modal } from "react-bootstrap";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
//import dateFormat from "dateformat";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";

import API from "../../shared/axios";

const initialValues = {
  employee_id: "",
};

class CQTReAssignPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showCQTReAssign: false,
      showCQTLoader: false,
      employeeArr: [],
      employee_id: "",
      defaultValue: "",
      cqtTaskId: 0,
    };
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.showCQTReAssign === true && nextProps.currRow.task_id > 0) {
      this.setState({
        currRow: nextProps.currRow,
        showCQTLoader: true,
      });

      if (nextProps.currRow && nextProps.currRow.cqt > 0) {
        API.get(`/api/drl/employeelist`)
          .then((res) => {
            API.post(`/api/drl/getassigned`, {
              id: nextProps.currRow.cqt,
            }).then((result) => {
              this.setState({
                employeeArr: res.data.data,
                cqtTaskId: result.data.data.cqtTaskId,
              });

              //set assigned to in the dropdown
              let loop = res.data.data.length;
              for (var i = 0; i < loop; i++) {
                if (res.data.data[i].value === result.data.data.cqtAssignedId) {
                  var defValue = {
                    value: res.data.data[i].value,
                    label: res.data.data[i].label,
                  };

                  this.setState({
                    defaultValue: defValue,
                    showCQTReAssign: nextProps.showCQTReAssign,
                    showCQTLoader: false,
                  });

                  initialValues.employee_id = defValue;
                }
              }
            });
          })
          .catch((err) => {
            console.log(err);
          });
      }
    }
  };

  handleClose = () => {
    var close = { showCQTReAssign: false, showCQTLoader: false, cqtTaskId: 0 };
    this.setState(close);
    this.props.handleClose(close);
  };

  handleSubmitAssignTask = (values, actions) => {
    API.post(`/api/drl/reassign/`, {
      cqtTaskId: this.state.cqtTaskId,
      assignTo: values.employee_id.value,
    })
      .then((res) => {
        this.handleClose();
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: "Task has been reassigned.",
          icon: "success",
        }).then(() => {
          this.props.reloadTask();
        });
      })
      .catch((error) => {
        actions.setErrors(error.data.data);
        actions.setSubmitting(false);
      });
  };

  render() {
    //var currRow = this.state.currRow;

    const validateStopFlag = Yup.object().shape({
      employee_id: Yup.string().trim().required("Please enter employee name"),
    });

    return (
      <>
        {this.state.cqtTaskId === 0 && this.state.showCQTLoader === true && (
          <>
            <div className="loderOuter">
              <div className="loader">
                <img src={loaderlogo} alt="logo" />
                <div className="loading">Loading...</div>
              </div>
            </div>
          </>
        )}

        <Modal
          show={this.state.showCQTReAssign}
          onHide={() => this.handleClose()}
          backdrop="static"
          dialogClassName="compressPopup"
        >
          <Formik
            initialValues={initialValues}
            validationSchema={validateStopFlag}
            onSubmit={this.handleSubmitAssignTask}
          >
            {({
              values,
              errors,
              touched,
              isValid,
              isSubmitting,
              setFieldValue,
            }) => {
              // console.log("===========", this.state.defaultValue);

              return (
                <Form encType="multipart/form-data">
                  <Modal.Header closeButton>
                    <Modal.Title>Re-Assign A Task</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <div className="contBox">
                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              defaultValue={this.state.defaultValue}
                              isClearable={true}
                              isSearchable={true}
                              name="employee_id"
                              options={this.state.employeeArr}
                              onChange={(value) =>
                                setFieldValue("employee_id", value)
                              }
                            />

                            {errors.employee_id && touched.employee_id ? (
                              <span className="errorMsg">
                                {errors.employee_id}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </Modal.Body>
                  <Modal.Footer>
                    <button
                      onClick={this.handleClose}
                      className={`btn-line`}
                      type="button"
                    >
                      Close
                    </button>
                    <button
                      className={`btn-fill ${
                        isValid ? "btn-custom-green" : "btn-disable"
                      } m-r-10`}
                      type="submit"
                      disabled={isValid ? false : true}
                    >
                      {this.state.stopflagId > 0
                        ? isSubmitting
                          ? "Updating..."
                          : "Update"
                        : isSubmitting
                        ? "Assigning..."
                        : "Assign"}
                    </button>
                  </Modal.Footer>
                </Form>
              );
            }}
          </Formik>
        </Modal>
      </>
    );
  }
}

export default CQTReAssignPopup;
