import React, { Component } from 'react';

import { Row, Col, ButtonToolbar, Button, Modal } from "react-bootstrap";
import axios from "axios";
import { Formik, Field, Form } from "formik"; 
import * as Yup from "yup"; 
import swal from "sweetalert";
import Loader from 'react-loader-spinner';

import DatePicker from 'react-date-picker';

import jp from 'jsonpath';

import ReactHtmlParser from 'react-html-parser';

const initialValues = {
    employeeId   : "",
    deptId : "",
    designationId : "",
    dueDate      : ""
  };

const onlyUnique = (value, index, self) => { 
    return self.indexOf(value) === index;
}  

class AssignSubTaskPopup extends Component {    

    constructor(props){
        super(props);
        this.state = {
            task_id           : 0,
            showAssignSubTask : false,
            employeeId        : '-1',
            deptId            : '-1',
            designationId     : '-1'
        };
    }

    componentWillReceiveProps = (nextProps) => {
        if(nextProps.showAssignSubTask == true && nextProps.task_id > 0){
            var id =1;
            this.setState({showAssignSubTask: nextProps.showAssignSubTask,task_id:nextProps.task_id});
  
            // axios.get(`http://10.0.10.222/project/dev/tty_portal/react_services/get_employees/id/${id}`)
            // .then(res => {
            //     this.setState({ 
            //         employeeArr:res.data,
            //         employeeDisplayArr:res.data
            //     });
            //     //console.log(res.data);
            // })
            // .catch(err => {

            // });

            // axios.get(`http://10.0.10.222/project/dev/tty_portal/react_services/get_department/id/${id}`)
            // .then(res => {
            //     this.setState({ 
            //         deptArr:res.data,
            //         deptDisplayArr:res.data
            //     });
            //     //console.log(res.data);
            // })
            // .catch(err => {

            // });

            // axios.get(`http://10.0.10.222/project/dev/tty_portal/react_services/get_designation/id/${id}`)
            // .then(res => {
            //     this.setState({
            //         designationArr:res.data,
            //         designationDisplayArr:res.data
            //     });
            //     //console.log(res.data);
            // })
            // .catch(err => {});
            
        }
    }

    handleClose = () => {
        var close = {showAssignSubTask:false,task_id:0};
        this.setState(close);
        this.props.handleClose(close);
    };

    handleSubmitAssignTask = (values, actions) => {
        const formData = new FormData();
        
        for (const key in values) {
            formData.append(key, values[key]);
        }
        
        //console.log(formData);
        //this.props.assignNewTasks(formData);
    }

    changeDueDate = (value) =>{
        initialValues.dueDate = value; 
    }

    handleEmployeeChange = (event) =>{
        var selectedIndex = event.target.value;
        if(selectedIndex > 0){
            var empArr        = jp.query(this.state.employeeArr,'$[?(@.id==='+selectedIndex+')]');
            this.setState({employeeId:event.target.value,deptId:empArr[0].dept_id,designationId:empArr[0].desig_id});
        }else{
            this.setState({employeeId:'-1',deptId:'-1',designationId:'-1'});
        }
    }

    handleDepartmentChange = (event) =>{
        var selectedIndex = event.target.value;
        if(selectedIndex > 0){
            var empArr         = jp.query(this.state.employeeArr,'$[?(@.dept_id==='+selectedIndex+')]');
            //console.log(empArr);
            //var designationArr = jp.query(empArr,'$[*].desig_id');
            //var uniqueDesigArr =  designationArr.filter(onlyUnique);

            this.setState({employeeDisplayArr:empArr,employeeId:'-1',deptId:selectedIndex,designationId:'-1'});
        }else{
            this.setState({employeeId:'-1',deptId:'-1',designationId:'-1',employeeDisplayArr:this.state.employeeArr});
        }
    }

    handleDesignationChange = (event) =>{
        var selectedIndex = event.target.value;
        if(selectedIndex > 0){
            var empArr        = jp.query(this.state.employeeArr,'$[?(@.desig_id==='+selectedIndex+')]');

            this.setState({employeeDisplayArr:empArr,employeeId:'-1',deptId:'-1',designationId:selectedIndex});
        }else{
            this.setState({employeeId:'-1',deptId:'-1',designationId:'-1',employeeDisplayArr:this.state.employeeArr});
        }
    }

    render(){
        //console.log('++++');
        //console.log(this.props);
        //console.log(this.state.deptArr);

        const validateStopFlag = Yup.object().shape({
            employeeId: Yup.string().trim()
                .required("Please enter employee name"),
            deptId: Yup.string().trim()
                .required("Please enter member department"),
            designationId: Yup.string().trim()
                .required("Please enter member designation"),
            dueDate: Yup.string().trim()
                .required("Please enter due date")
        });

        const newInitialValues = Object.assign(initialValues, {
            employeeId    : this.state.employeeId,
            deptId        : this.state.deptId,
            designationId : this.state.designationId,
            dueDate       : new Date()
          });

        if(this.state.task_id == 0)return null;

        if((typeof this.state.employeeDisplayArr === 'undefined' || typeof this.state.deptDisplayArr === 'undefined' || typeof this.state.designationDisplayArr === 'undefined') && this.state.showAssignSubTask === true ){
            return(
                <>
                    <Loader 
                        type="Puff"
                        color="#00BFFF"
                        height="100"	
                        width="100"
                        verticalAlign="middle"
                    />
                </>
            );
        }else{
            return(
                <>
                <Modal show={this.state.showAssignSubTask} onHide={() => this.handleClose()}  backdrop="static">
                    <Formik
                        initialValues    = {newInitialValues}
                        validationSchema = {validateStopFlag}
                        onSubmit         = {this.handleSubmitAssignTask}
                    >
                        {({ values, errors, touched, isValid, isSubmitting, setFieldValue }) => {
                        return (
                            <Form>
                            <Modal.Header closeButton>
                                <Modal.Title>
                                Assign A Sub Task
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div className="contBox">
                                <Row>
                                    <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                        <Field
                                        name="employeeId"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.employeeId}
                                        onChange={this.handleEmployeeChange}
                                        >
                                        <option key='-1' value="" >Employee Name</option>
                                            {this.state.employeeDisplayArr.map((emp, i) => (
                                                <option key={i} value={emp.id}>
                                                    {emp.name}
                                                </option>
                                            ))}
                                        </Field>
                                        {errors.employeeId && touched.employeeId ? (
                                        <span className="errorMsg">
                                            {errors.employeeId}
                                        </span>
                                        ) : null}
                                    </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                        <Field
                                        name="deptId"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.deptId}
                                        onChange={this.handleDepartmentChange}
                                        >
                                            <option key='-1' value="" >Department Name</option>
                                            {this.state.deptDisplayArr.map((dept, i) => (
                                                <option key={i} value={dept.id}>
                                                    {dept.name}
                                                </option>
                                            ))}
                                        </Field>
                                        {errors.deptId && touched.deptId ? (
                                        <span className="errorMsg">
                                            {errors.deptId}
                                        </span>
                                        ) : null}
                                    </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                        <Field
                                        name="designationId"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.designationId}
                                        onChange={this.handleDesignationChange}
                                        >
                                            <option key='-1' value="" >Designation</option>
                                            {this.state.designationDisplayArr.map((desig, i) => (
                                                <option key={i} value={desig.id}>
                                                    {desig.name}
                                                </option>
                                            ))}
                                        </Field>
                                        {errors.designationId && touched.designationId ? (
                                        <span className="errorMsg">
                                            {errors.designationId}
                                        </span>
                                        ) : null}
                                    </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                        
                                        <DatePicker
                                            name="dueDate"
                                            onChange={value =>
                                                setFieldValue("dueDate", value)
                                            }
                                            value={values.dueDate}
                                        />

                                        {errors.dueDate && touched.dueDate ? (
                                        <span className="errorMsg">
                                            {errors.dueDate}
                                        </span>
                                        ) : null}
                                    </div>
                                    </Col>
                                </Row>
                                </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <button
                                    onClick={this.handleClose}
                                    className={`btn-line`}
                                    type="button"
                                >
                                    Close
                                </button>
                                <button
                                    className={`btn-fill ${
                                    isValid ? "btn-custom-green" : "btn-disable"
                                    } m-r-10`}
                                    type="submit"
                                    disabled={isValid ? false : true}
                                >
                                    {this.state.stopflagId > 0
                                    ? isSubmitting
                                        ? "Updating..."
                                        : "Update"
                                    : isSubmitting
                                    ? "Submitting..."
                                    : "Submit"}
                                </button>
                            </Modal.Footer>
                            </Form>
                        );
                        }}
                    </Formik>
                    </Modal>
                </>
            );
        }
    }
}

export default AssignSubTaskPopup;