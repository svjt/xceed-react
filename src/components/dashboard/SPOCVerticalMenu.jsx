import React, { Component } from 'react';
import { getDashboard,getMyId,htmlDecode, inArray } from '../../shared/helper';

import ellipsisImage from '../../assets/images/ellipsis-icon.svg';
import exclamationImage from '../../assets/images/exclamation-icon.svg';
import peopleImage from '../../assets/images/people-icon.svg';

//import { Link } from 'react-router-dom';
import {
    Tooltip,
    OverlayTrigger
  } from "react-bootstrap";

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
    return (
      <OverlayTrigger
        overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
        placement="left"
        delayShow={300}
        delayHide={150}
        trigger={["hover"]}
      >
        {children}
      </OverlayTrigger>
    );
  }
  /*For Tooltip*/

class SPOCVerticalMenu extends Component {

    constructor(){
        super();
        document.querySelector('body').addEventListener('click',function(event){
            var elems = document.querySelectorAll('.btn-group');
            for (var i = 0; i < elems.length; i++){
                elems[i].classList.remove('open');
            }
        });
    }

    handleVerticalMenu = (event) => {
        var elems = document.querySelectorAll('.btn-group');
        for (var i = 0; i < elems.length; i++){
            elems[i].classList.remove('open');
        }

        if(event.target.parentNode.classList.contains("btn-group")){
            event.target.parentNode.classList.add("open");
        }else if(event.target.parentNode.classList.contains("dropdown-toggle")){
            event.target.parentNode.parentNode.classList.add("open");
        }
        
    }

    render() {
        

        return (
            <>

            {this.props.currRow.comment_status == 1 ?'':<div className="actionStyle">
                <div className="btn-group">
                    <button type="button" className="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" onClick = {(e) => this.handleVerticalMenu(e)} >
                        <img src={ellipsisImage} alt="ellipsisImage" />
                    </button>
                    <ul className="dropdown-menu pull-right" role="menu">

                        <li><span onClick={()=>this.props.showApprovalPopup(this.props.currRow)} style={{cursor:'pointer'}} >
                            <i className="fa fa-check" />Approve</span>
                        </li>
                        {this.props.t === 'T' && <li><span onClick={()=>this.props.showTranslateCommentPopup(this.props.currRow)} style={{cursor:'pointer'}} >
                            <i className="fas fa-pencil-alt" />Review Comment</span>
                        </li>}

                        {this.props.t === 'O' && <li><span onClick={()=>this.props.showOriginalCommentPopup(this.props.currRow)} style={{cursor:'pointer'}} >
                            <i className="fas fa-pencil-alt" />Review Comment</span>
                        </li>}

                        {<li><span onClick={()=>this.props.showTranslateCommentPopup(this.props.currRow)} style={{cursor:'pointer'}} >
                                <i className="fas fa-pencil-alt" />Review Comment</span>
                            </li>}  
                               
                    </ul>
                </div>
            </div>}

            
            </>
        );
    }
}

export default SPOCVerticalMenu;
