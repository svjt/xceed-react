import React, { Component } from 'react';

import BMOverview from "../bmoverview/BMOverview";
import CSCOverview from "../cscoverview/CSCOverview";
import RAOverview from "../raoverview/RAOverview";
import OTHROverview from "../othroverview/OTHROverview";
import { getDashboard } from '../../shared/helper';

class Dashboard extends Component {
    
    render(){
        if( getDashboard() === 'BM' || getDashboard() === 'SPOC' ) {
            return <BMOverview {...this.props} />;
        } else if( getDashboard() === 'CSC' ) {
            return <CSCOverview {...this.props} />;
        } else if( getDashboard() === 'RA' ) {
            return <RAOverview {...this.props} />;

        } else {
            return <OTHROverview {...this.props} />;
        } 
    }
}

export default Dashboard;