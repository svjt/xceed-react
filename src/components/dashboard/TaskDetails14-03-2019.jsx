import React, { Component } from "react";
import { Row, Col, Button,Panel,PanelGroup } from "react-bootstrap";
import dateFormat from "dateformat";
import API from "../../shared/axios";

import * as Yup from "yup";
import { Formik, Field, Form } from "formik";
import swal from "sweetalert";

import { Redirect, Link } from "react-router-dom";
//import Loader from "react-loader-spinner";
import { getDashboard, htmlDecode } from "../../shared/helper";
import commenLogo from "../../assets/images/drreddylogosmall.png";
import pdfIcon from '../../assets/images/pdf-icon.png';

import { getMyId } from '../../shared/helper';

import whitelogo from '../../assets/images/drreddylogo_white.png';
import Select from "react-select";

const path          = `${process.env.REACT_APP_API_URL}/api/tasks/download/`; // SATYAJIT
const comm_path     = `${process.env.REACT_APP_API_URL}/api/tasks/download_comment/`; // SVJT
const diss_path     = `${process.env.REACT_APP_API_URL}/api/tasks/download_discussion/`; // SVJT
const download_path = `${process.env.REACT_APP_API_URL}/api/drl/download/`; // SATYAJIT

// initialize form and their validation
const initialValues = {
	comment: ""
};

const sapInitialValues = {
  order_number: "",
  invoice_number: ""
}

const commentSchema = Yup.object().shape({
	comment: Yup.string()
			.trim("Please remove whitespace")
			.strict().required("Please enter your comment")
});

const sapSchema = Yup.object().shape({
	order_number: Yup.string()
      .required("Please enter sales order number"),
  invoice_number: Yup.object().shape({
      isOrderNumber: Yup.string(),
      value : Yup.string().when("isOrderNumber", {
        is: true,
        then: Yup.string()                    
          .required("Please select invoice number")
    })
  })   
});


const priority_arr = [
  { priority_id: 1, priority_value: "Low" },
  { priority_id: 2, priority_value: "Medium" },
  { priority_id: 3, priority_value: "High" }
];

const req_category_arr = [
  { request_category_id: 1, request_category_value: "New Order" },
  { request_category_id: 2, request_category_value: "Other" },
  { request_category_id: 3, request_category_value: "Complaint" },
  { request_category_id: 4, request_category_value: "Forecast" },
  { request_category_id: 5, request_category_value: "Payment" },
  { request_category_id: 6, request_category_value: "Notification" }
];

class TaskDetails extends Component {
  constructor(props) {
    super(props);

    //const {changeStatus} = this.props.location.state;

    //console.log(this.props);
    this.state = {
      taskDetails: [],
      CQT_details: [],
      discussDetails: [],
      CQT_loader : false,
      sap_loader : false,
      no_sales_record: false,
      files: [],
      order_number: "",
      invoice_number: "",
      sales_order: "",
      invoice_list: [],
      task_id: this.props.match.params.id,
      assign_id:this.props.match.params.assign_id
    };
  }

  setCreateDate = date_added => {
    var mydate = new Date(date_added);
    return dateFormat(mydate, "dd/mm/yyyy");
  };

  setDaysPending = due_date => {
    var dueDate = new Date(due_date);
    var today = new Date();
    var timeDiff = dueDate.getTime() - today.getTime();
    if (timeDiff > 0) {
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
      return diffDays;
    } else {
      return 0;
    }
  };

  setPriorityName = priority_id => {
    var ret = "Not Set";
    for (let index = 0; index < priority_arr.length; index++) {
      const element = priority_arr[index];
      if (element["priority_id"] === priority_id) {
        ret = element["priority_value"];
      }
    }

    return ret;
  };

  getSLAStatus = (sla_status) => {
    if(sla_status === 1){
      return "Need Further Info";
    }else if(sla_status === 2){
      return "Closed";      
    }
  }

  setReqCategoryName = req_cate_id => {
    var ret = "Not Set";
    for (let index = 0; index < req_category_arr.length; index++) {
      const element = req_category_arr[index];
      if (element["request_category_id"] === req_cate_id) {
        ret = element["request_category_value"];
      }
    }

    return ret;
  };

  checkPharmacopoeia(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (
        (request_type === 22 ||
          request_type === 21 ||
          request_type === 19 ||
          request_type === 18 ||
          request_type === 17 ||
          request_type === 2 ||
          request_type === 3 ||
          request_type === 4 ||
          request_type === 10 ||
          request_type === 12 ||
          request_type === 1) &&
        parent_id === 0
      ) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkPolymorphicForm(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if ((request_type === 2 || request_type === 3) && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkStabilityDataType(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 13 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkAuditSiteName(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 9 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkAuditVistDate(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 9 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkQuantity(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if ((request_type === 7 || request_type === 23 || request_type === 25) && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkBatchNo(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 7 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkNatureOfIssue(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 7 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkRDD(request_type, parent_id) {
    if (request_type === null) {
      return false;
    } else {
      if (request_type === 23 && parent_id === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkSamplesWorkingImpureRequest(request_type, parent_id){
    if(request_type === null){
        return false;
    }else{
        if(request_type === 1 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkProductRequest(request_type, parent_id) {
    if(request_type === null){
      return false;
    }else{
        if((request_type !== 24) && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkMarketRequest(request_type, parent_id) {
    if(request_type === null){
      return false;
    }else{
        if((request_type !== 24 && request_type !== 25 && request_type !== 26) && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkForecastNotificationDate(request_type, parent_id) {
    if(request_type === null){
      return false;
    }else{
        if(request_type === 24 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkAmountPending( request_type, parent_id ) {
    if(request_type === null){
      return false;
    }else{
        if(request_type === 25 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkDaysRemaining( request_type, parent_id ) {
    if(request_type === null){
      return false;
    }else{
        if(request_type === 25 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkNotificationRequestType( request_type, parent_id ) {
    if(request_type === null){
      return false;
    }else{
        if(request_type === 26 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkNotificationTargetApproval( request_type, parent_id ) {
    if(request_type === null){
      return false;
    }else{
        if(request_type === 26 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkNotificationTargetImplementation ( request_type, parent_id ){
    if(request_type === null){
      return false;
    }else{
        if(request_type === 26 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkPaymentStatus( request_type, parent_id ) {
    if(request_type === null){
      return false;
    }else{
        if(request_type === 25 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  checkPaymentDueDate( request_type, parent_id ) {
    if(request_type === null){
      return false;
    }else{
        if(request_type === 25 && parent_id === 0){
            return true;
        }else{
            return false;
        }
    }
  }

  setDateFormat(date_value) {
    var mydate = new Date(date_value);
    //console.log(mydate);
    return dateFormat(mydate, "dd/mm/yyyy");
  }

  getTaskCommentDetails = () => {
    API.get(`/api/tasks/${this.state.task_id}/${this.state.assign_id}`)
      .then(res => {

        if(res.data.data.discussionExists){

          API.get(`api/tasks/discussions/${this.state.task_id}`)
          .then(res => {
            this.setState({
              discussDetails: res.data.data.discussion,
              discussTaskRef: res.data.data.task_ref,
              discussReqName: res.data.data.req_name,
              discussFile: res.data.data.files
            });
            //console.log("state",  this.state );
          })
          .catch(err => {
            console.log(err);
          });
        }

        if(res.data.data.taskDetails.cqt > 0){

            this.setState({ CQT_loader : true });

            API.get(`/api/drl/${res.data.data.taskDetails.cqt}`)
            .then(res => {
                this.setState({
                    CQT_details : res.data.data,
                    CQT_loader : false
                })                       
            })
            .catch(err => {
              console.log(err);
            });
        }

        this.setState({
          taskDetails     : res.data.data.taskDetails,
          taskDetailsFiles: res.data.data.taskDetailsFiles,
          comments        : res.data.data.comments,
          subTasks        : res.data.data.subTasks,
          commentExists   : res.data.data.commentExists,
          discussionExists: res.data.data.discussionExists
        });

       // console.log('taskDetails',this.state.taskDetails);

        // SATYAJIT -- Call to SAP if request_type = 'New Order Request' (23)
        API.get(`/api/bundle/getsalesorder/${res.data.data.taskDetails.task_id}`)
          .then(res => {
              const salesData = res.data.data
              this.setState({
                sales_order : salesData
              })
          })
          .catch(err => {
            console.log(err);
          });
        // SATYAJIT

        //STOP THIS CALL FOR ALLOCATED TASK
        //if(res.data.data.status === 1){
        var post_data = {};
        API.put(`/api/tasks/status/${this.state.task_id}`, post_data)
          .then(response => {
            //console.log(response.data.data);
            //nextProps.refreshTableStatus(nextProps.currRow);
          })
          .catch(err => {
            console.log(err);
          });
        //}
      })
      .catch(err => {
        console.log(err);
      });
  }

  componentDidMount = () => {   
    if (this.state.task_id > 0) {
      this.getTaskCommentDetails();
    }
  };

  createAccordion = () => {

      let accordion = [];
      var sub_tasks = this.state.subTasks;

      for (let index = 0; index < sub_tasks.length; index++) {

          accordion.push(
              <Panel eventKey={sub_tasks[index].task_id} key={index}>
                  <Panel.Heading>
                  <Panel.Title toggle>{sub_tasks[index].task_ref}</Panel.Title>
                  </Panel.Heading>
                  <Panel.Body collapsible>
                      <div className="form-group">
                          <label>
                              Task: 
                          </label>
                          {sub_tasks[index].discussion === 1 &&
                              <Link to={{ pathname: '/user/discussion_details/'+sub_tasks[index].parent_id }} target="_blank" style={{cursor:'pointer'}}>{sub_tasks[index].task_ref}</Link>
                          }
                          {sub_tasks[index].discussion !== 1 &&
                              <Link to={{ pathname: `/user/task_details/${sub_tasks[index].task_id}/${sub_tasks[index].assignment_id}` }} target="_blank" style={{cursor:'pointer'}}>{sub_tasks[index].task_ref}</Link>
                          }
                      </div>
                      
                      <div className="form-group">
                          <label>
                              Description:
                          </label>

                          {sub_tasks[index].parent_id > 0 && sub_tasks[index].title }
                          {sub_tasks[index].parent_id === 0 && sub_tasks[index].req_name }
                      </div>
                      <div className="form-group">
                          <label>
                              Created:
                          </label>
                              {dateFormat(sub_tasks[index].date_added, "dd/mm/yyyy")}
                      </div>
                      {sub_tasks[index].first_name !== '' && <div className="form-group">
                          <label>
                              Customer Name:
                          </label>
                            {`${sub_tasks[index].first_name + " " + sub_tasks[index].last_name}`}
                      </div>}
                      {sub_tasks[index].assigned_by === -1 && <div className="form-group">
                          <label>
                              Assigned By:
                          </label>
                              System
                      </div>}
                      {sub_tasks[index].assigned_by > 0 && <div className="form-group">
                          <label>
                              Assigned By:
                          </label>
                              {`${sub_tasks[index].assigned_by_first_name+' '+sub_tasks[index].assigned_by_last_name+' ('+sub_tasks[index].assign_by_desig_name+')'}`}
                      </div>}
                      <div className="form-group">
                          <label>
                              Assigned To:
                          </label>
                              {`${sub_tasks[index].assigned_to_first_name+' '+sub_tasks[index].assigned_to_last_name+' ('+sub_tasks[index].assign_to_desig_name+')'}`}
                      </div>
                      
                  </Panel.Body>
              </Panel>
          );
      }

      return accordion;
  }

  openCQTattachment = ( filepath ) => {
          API.get(filepath)
          .then(res => {
              window.open( res.data.data, '_blank');
          })
          .catch(err => {
              console.log(err);
          });
  }

  getComments = () => {
    if (this.state.comments.length > 0) {
      const comment_html = this.state.comments.map((comm, i) => (
        <div className="comment-list-wrap" key={i}>
          <div className="comment">
            <div className="row">
              <div className="col-md-10 col-sm-10 col-xs-9">
                <div className="imageArea">
                  <div className="imageBorder">
                    <img alt="noimage" src={commenLogo} />
                  </div>
                </div>
                <div className="conArea">
                  <p>
                    {comm.first_name} {comm.last_name}
                  </p>
                  <span>{comm.comment}</span>
                </div>
                <div className="clearfix" />
              </div>
              <div className="col-md-2 col-sm-2 col-xs-3">
                <div className="dateArea">
                  <p>{this.setDateFormat(comm.post_date)}</p>
                </div>
              </div>
            </div>
            {this.getCommentFile(comm.uploads)}
          </div>
        </div>
      ));

      return (
        <>
          <div className="disHead">
            <label>Task Comments:</label>
          </div>
          <div className="comment-list-main-wrapper">{comment_html}</div>
        </>
      );
    }
  };

  getCommentFile = (uploads) => {
    if(typeof uploads === 'undefined'){
      return '';
    }else{
      return (
        <ul className="conDocList">
          {uploads.map((files, j) => (
                <li key={j} ><a href={`${comm_path}${files.tcu_id}`} target="_blank" download rel="noopener noreferrer">
                {files.actual_file_name}
              </a></li>
          ))}
        </ul>
      );
    }
  }

  getDiscussionFile = (uploads) => {
    if(typeof uploads === 'undefined'){
      return '';
    }else{
      return (
        <ul className="conDocList">
          {uploads.map((files, j) => (
                <li key={j} ><a href={`${diss_path}${files.upload_id}`} target="_blank" download rel="noopener noreferrer">
                {files.actual_file_name}
              </a></li>
          ))}
        </ul>
      );
    }
  }

  getCQT = () => {

    if(this.state.CQT_details && this.state.CQT_details !== "" && this.state.CQT_loader === false){

        const { CQT_details } = this.state;

        return (
          <>
            <div className="disHead">
              <label>CQT Details:</label>
            </div>
            <div className="row cqtDetailsDeta-btm">
                
                <div className="col-md-4 col-sm-6 col-xs-12">
                  <div className="form-group">
                    <label>Title :</label>
                      {CQT_details.title}
                  </div>
                </div>

                <div className="col-md-4 col-sm-6 col-xs-12">
                  <div className="form-group">
                    <label>Product Name :</label>
                      {CQT_details.product_name}
                  </div>
                </div>

                <div className="col-md-4 col-sm-6 col-xs-12">
                  <div className="form-group">
                    <label>Country Name :</label>
                      {CQT_details.country_name}
                  </div>
                </div>

                <div className="col-md-4 col-sm-6 col-xs-12">
                  <div className="form-group">
                    <label>Query Category :</label>
                      {CQT_details.query_category}
                  </div>
                </div>

                <div className="col-md-4 col-sm-6 col-xs-12">
                  <div className="form-group">
                    <label>Query Description :</label>
                      { CQT_details.query_description }
                  </div>
                </div>

                <div className="col-md-4 col-sm-6 col-xs-12">
                  <div className="form-group">
                    <label>Criticality :</label>
                      {CQT_details.criticality}
                  </div>
                </div>
               

                <div className="col-md-4 col-sm-6 col-xs-12">
                  <div className="form-group">
                    <label>Assigned To :</label>
                      {CQT_details.assigned_to}
                  </div>
                </div>

                <div className="col-md-4 col-sm-6 col-xs-12">
                  <div className="form-group">
                    <label>Status :</label>
                      {CQT_details.status}
                  </div>
                </div>


            </div>

                  <div className="disHead">
                    <label>CQT Attachment: ({CQT_details.files.length})</label>
                  </div>   
                  
                  {CQT_details.files.length > 0 &&                     
                        <div className="cqtDetailsDeta-btm">
                          <ul className='cqtAttachment'>

                                {CQT_details.files.map((file, p) => (                            
                                  <li key={p}>
                                    <div className="file-base">
                                      <div className="file-baseName">{file.BaseName} </div>
                                      <div className="file-attactment">
                                        <small>| {file.File_x0020_Type} |</small>
                                        {/* <a href="" onClick={this.openCQTattachment(`${download_path}${file.ID}`)} download rel="noopener noreferrer"> */}

                                          <span onClick={(e) => this.openCQTattachment(`${download_path}${file.ID}`)}>
                                              <i className="fa fa-download"></i>
                                          </span>

                                        {/* </a> */}
                                      </div>
                                    </div>
                                  </li> 
                                  ))
                                }

                          </ul>
                      </div>                    
                  }

          </>
        );
    } else {
          return (
              <div className="loderOuter">
                <div className="loading_reddy_outer">
                    <div className="loading_reddy" >
                        <img src={whitelogo} alt="logoImage"/>
                    </div>
                </div>
              </div>
          );
      }
  } 

  handleOrderChange = (e,setFieldValue) => {
    this.setState({order_number : e.target.value});
    setFieldValue('order_number', e.target.value)
  }

  cancelSalesOrder = () => {
    this.order_number_field.value = "";
  }

  getInvoiceList = () => {
    this.setState({
      sap_loader : true            
    })
    if(this.state.order_number) {
        API
        .post(`api/bundle/search`, {
          'order_number' : this.state.order_number
        })
        .then(res => {
            if(res.data.data === 'Not processed'){
              this.setState({
                  no_invoice_list: 'Not processed',
                  sap_loader : false,
                  no_sales_record: false              
              })
            }else{
              this.setState({
                  invoice_list: res.data.data,
                  sap_loader : false,
                  no_sales_record: false              
              })
            }
        })
        .catch(err => {
            this.setState({
              sap_loader : false,
              no_sales_record : true         
            })
        });
    }
  }

  getSalesRecord = ( values, actions ) => {

    const invoice_no = values.invoice_number.value;
    const order_no = values.order_number;

    if(invoice_no && order_no && this.state.task_id){

      this.setState({
        sap_loader : true            
      })
      
      API
        .post(`api/bundle/view`, {
          'order_number'    :  order_no,
          'invoice_number'  :  invoice_no,
          'task_id' : this.state.task_id
        })
        .then(res => {
           this.setState({
              sales_order: res.data.data,
              sap_loader : false,
              no_sales_record: false              
          })
        })
        .catch(err => {
            this.setState({
              sap_loader : false,
              no_sales_record : true         
            })
        });
    }
  }

  getSAP = () => {

    return (
      <>
        <div className="disHead">
          <label>SAP</label>
        </div>
        <div className="row cqtDetailsDeta-btm">

          <Formik
              sapInitialValues    = {sapInitialValues}
              validationSchema    = {sapSchema}
              onSubmit            = {this.getSalesRecord}
          >
          {({ values, errors, touched, isValid, isSubmitting, setFieldValue }) => {
            //console.log("err==========",  errors );
            //console.log("touch===========", touched );
            //console.log("values============", values);
            return(
              <Form>
                <div className="cqtDetailsDeta-serach form-inline">
                    <div className="form-group">
                      <label>Sales Order Number:</label>

                        <Field
                          name="order_number"
                          type="text"
                          className={`form-control`}
                          placeholder="order number"
                          autoComplete="off"
                          value={values.order_number}
                          onChange={(e) => this.handleOrderChange(e, setFieldValue)}
                        />
                        {errors.order_number &&
                        touched.order_number ? (
                          <span className="errorMsg">
                            {errors.order_number}
                          </span>
                        ) : null}

                      {this.state.invoice_list.length > 0 &&

                          <Select
                            className="basic-single"
                            classNamePrefix="select"
                            defaultValue={values.invoice_number}
                            isClearable={true}
                            isSearchable={true}
                            name="invoice_number"
                            options={this.state.invoice_list}
                            onChange={value =>
                                setFieldValue("invoice_number", value)
                            }
                          />
                      }                      

                      {errors.invoice_number && touched.invoice_number !== '' ? (
                        <span className="errorMsg">
                            {errors.invoice_number}
                        </span>
                        ) : null}

                      {this.state.invoice_list.length === 0 && 
                        <input className="btn btn-success btn-sm btn" type="button" value="Get Invoice List" onClick={this.getInvoiceList}/>
                      }
                      
                      {this.state.invoice_list.length > 0 && 
                        <input className="btn btn-success btn-sm btn" type="submit" value="View"/>
                      }
                      
                      <input className="btn btn-danger btn-sm btn" type="button" value="Cancel" onClick={this.cancelSalesOrder}/>
                    </div>
                    
                    {
                      this.state.no_invoice_list && 
                      <div className="form-group">
                        <label>{this.state.no_invoice_list}</label>
                      </div>
                    }

                  </div>
              </Form>
            )
          }}  
        </Formik>

            {this.state.sap_loader === true && 
                <div className="loderOuter">
                  <div className="loading_reddy_outer">
                      <div className="loading_reddy" >
                          <img src={whitelogo} alt="loaderImage"/>
                      </div>
                  </div>
                </div>
            }
            
            <div className="row cqtDetailsDeta-result">
               {this.state.sales_order && 
                  this.state.sales_order.map((sale, i) => {
                    let productList = JSON.parse( sale.product )
                    return (
                      <div className="col-md-3 col-sm-6 col-xs-12" key={i}>
                          <p>PO Order Status:<strong> {sale.order_status === 'A' ? "Not yet processed" : sale.order_status === 'B' ? "Partially processed" : "Completely processed"}</strong></p>
                          <p>Sale Order Number : <strong> {sale.order_number}</strong></p>
                          <p>Invoice Number : <strong> {sale.invoice_number}</strong></p>
                          <ul>
                            <li>PI (Pre Invoice) : <strong> {sale.pre_invoice}</strong></li>
                            <li>LC status : <strong> {sale.lc_status}</strong></li>
                            <li>CoA : <strong> {sale.coa}</strong></li>
                          </ul>                    
                          <p>Credit Block Status : <strong> {sale.block_status}</strong></p>
                          <p>Requested Delivery Date : <strong> {sale.delivery_date}</strong></p>    
                          <p>Payment Status : <strong> {sale.payment_status}</strong></p>
                          <p>Product : </p>
                          
                          <ul>
                              {productList.map( (product, p)=> {
                                  return (
                                    <li key={p}><strong> {product.name} </strong></li>
                                  )
                              })}
                          </ul>
                                       
                          <p>Dispatch Date : <strong> {sale.dispatch_date}</strong></p>
                          <p>Days Remaining : <strong> {sale.remaining_days} </strong></p>
                          <p>Amount Pending : <strong> {sale.amount_pending} {sale.code}</strong></p>
                          <p>Due Date : <strong> {sale.due_date}</strong></p>
                      </div>
                    )
                  })                  
               }
               </div>

            {this.state.no_sales_record &&
                <div className="col-md-12 col-sm-12 col-xs-12"> No record found </div>
            }
        </div>
      </>
    )
  }

  handleChange = (e, field) => {   
      this.setState({
          [field]: e.target.value
      });
  };

  splitQuantity = (quantity) => {
    if(quantity){
      return quantity.split(" ");
    }
  }

  submitComment = (values, actions) => {
      let tid = this.props.match.params.id;
      let aid = this.props.match.params.assign_id;
      if( tid ) {
          //var postData = {comment:values.comment,assign_id:aid};

          var formData = new FormData();

          formData.append('comment', values.comment);
          formData.append('assign_id', aid);

          if(this.state.file){
              formData.append('file', this.state.file);            
          }else{
              formData.append('file', "");
          }

          const config = {
              headers: {
                  'content-type': 'multipart/form-data'
              }
          };

          API
          .post(`api/tasks/respond_back_comment/${tid}`,formData,config)
          .then(res => {
              values.comment ='';
              this.fileInput.value = "";
              swal("Comment posted successfully!", {
                  icon: "success"
              })
              this.getTaskCommentDetails();
          })
          .catch(err => {
              console.log(err);
          });
      }
  }

  submitDiscussion = (values, actions) => {
    let tid = this.props.match.params.id;
    //console.log('files',this.state.files);
    var formData = new FormData();

    if(this.state.file){
        formData.append('file', this.state.file);            
    }else{
        formData.append('file', "");
    }

    formData.append('comment', values.comment);

    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    };

    if (tid) {
      API.post(`api/tasks/discussions/${tid}`,formData,config)
        .then(res => {
          this.getTaskCommentDetails(tid);
          values.comment = "";
          values.file    = "";

          this.setState({
            comment: "",
            file: []
          });

          this.fileInputDiss.value = "";

          swal("Comment posted successfully!", {
            icon: "success"
          });
        })
        .catch(err => {
          console.log(err);
        });
    }
  };

  closeBrowserWindow = () => {
    window.close()
  } 

  handleTabs = (event) =>{
        
    if(event.currentTarget.className === "active" ){
        //DO NOTHING
    }else{

        var elems          = document.querySelectorAll('[id^="tab_"]');
        var elemsContainer = document.querySelectorAll('[id^="show_tab_"]');
        var currId         = event.currentTarget.id;

        for (var i = 0; i < elems.length; i++){
            elems[i].classList.remove('active');
        }

        for (var j = 0; j < elemsContainer.length; j++){
            elemsContainer[j].style.display = 'none';
        }

        event.currentTarget.classList.add('active');
        event.currentTarget.classList.add('active');
        document.querySelector('#show_'+currId).style.display = 'block';
    }
    // this.tabElem.addEventListener("click",function(event){
    //     alert(event.target);
    // }, false);
  }

  handleTabs2 = (event) =>{
        
    if(event.currentTarget.className === "active" ){
        //DO NOTHING
    }else{

        var elems          = document.querySelectorAll('[id^="d_tab_"]');
        var elemsContainer = document.querySelectorAll('[id^="show_d_tab_"]');
        var currId         = event.currentTarget.id;

        for (var i = 0; i < elems.length; i++){
            elems[i].classList.remove('active');
        }

        for (var j = 0; j < elemsContainer.length; j++){
            elemsContainer[j].style.display = 'none';
        }

        event.currentTarget.classList.add('active');
        event.currentTarget.classList.add('active');
        document.querySelector('#show_'+currId).style.display = 'block';
    }
    // this.tabElem.addEventListener("click",function(event){
    //     alert(event.target);
    // }, false);
  }


  // downloadFile = (e,file_id) => {
  //   e.preventDefault();
  //   if(file_id){
  //       API
  //       .get(`api/tasks/download/${file_id}`)
  //       .then(res => {
  //           return true;
  //       })
  //       .catch(err => {
  //           console.log(err);
  //       });
  //   }
  // }

  render() {
    //console.log(window.location.origin)
    //SVJT
    var my_id = getMyId();
    // SATYAJIT
    if (this.props.match.params.id === 0 || this.props.match.params.id === "") {
      return <Redirect to="/user/dashboard" />;
    }

    const { taskDetails } = this.state;

    //console.log("task details =====",  taskDetails );
    
    if (taskDetails.task_id > 0){
      if (this.state.taskDetailsFiles.length > 0) {
        var fileList = this.state.taskDetailsFiles.map((file,k) => (
          <li key={k}>
            <img alt="noimage" src={pdfIcon}/>
            <a href={`${path}${file.upload_id}`} target="_blank" download rel="noopener noreferrer">
              {file.actual_file_name}
            </a>
          </li>
        ));
      }
      return (
        <>
          <div className="content-wrapper">
          <section className="content-header"><h1>{taskDetails.req_name}</h1></section>

            <section className="content">
              <div className="nav-tabs-custom">
                <ul className="nav nav-tabs">
                    <li className="active" onClick = {(e) => this.handleTabs(e)} id="tab_1" >TASK DETAILS</li>
                    {this.state.commentExists && <li onClick = {(e) => this.handleTabs(e)}  id="tab_2">COMMENTS</li>}
                    {this.state.discussionExists &&<li onClick = {(e) => this.handleTabs(e)}  id="tab_3">DISCUSSION</li>}
                </ul>
              </div>
              <div className="tab-content">
                <div className="tab-pane active" id="show_tab_1">
                  {taskDetails.parent_id > 0 &&
                  <div className="boxPapanel content-padding">
                    <div className="taskdetails">
                      <div className="taskdetailsHeader">
                          <div className="commentBox1">
                            <div className="row">
                              <div className="col-sm-8 col-xs-6">
                              {/* taskdetails Panel Top Listing*/}
                                <div className="tdTplist">
                                  <ul>
                                    <li>
                                      <div className="form-group">
                                        <label>Task:</label> {taskDetails.task_ref}
                                      </div>
                                    </li>
                                    <li>
                                      <div className="form-group">
                                        <label>Created On:</label>{" "}
                                        {this.setCreateDate(
                                          taskDetails.date_added
                                        )}
                                      </div>
                                    </li>
                                    <li>
                                      <div className="form-group">
                                        <label>Days Pending:</label>{" "}
                                        {this.setDaysPending(taskDetails.due_date)}
                                      </div>
                                    </li>
                                    <li>
                                      <div className="form-group">
                                        <label>Priority:</label>{" "}
                                        {this.setPriorityName(
                                          taskDetails.priority
                                        )}
                                      </div>
                                    </li>
                                    {taskDetails.parent_id > 0 && taskDetails.discussion !== 1 && taskDetails.sla_status !== 0 &&
                                      <li>
                                        <div className="form-group">
                                          <label>SLA Status:</label> {this.getSLAStatus(taskDetails.sla_status)}
                                        </div>
                                      </li>
                                    }
                                  </ul>
                                </div>
                                {/* taskdetails Panel Top Listing*/}
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>}


                  <div className="boxPapanel content-padding">
                    <div className="taskdetails">
                    {/* taskdetails Panel Btm Listing Start*/}

                    <div className="clearfix tdBtmlist">                    
                      <Row className="task_details_complete">

                      {taskDetails.parent_id === 0 &&
                        taskDetails.request_type !== 25 && 
                        taskDetails.request_type !== 26 && 
                        taskDetails.closed_status !== 1 &&
                        (getDashboard() === "BM" ||
                          getDashboard() === "CSC") && (
                          <Link
                            to={`/user/edit_task_details/${taskDetails.task_id}/${taskDetails.assignment_id}`}
                            className="plus-request edit-button"
                          >
                            <i className="far fa-edit" aria-hidden="true" />
                            Edit
                          </Link>
                        )}  
                        

                        {(taskDetails.first_name !== "" ||
                          taskDetails.last_name !== "") && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Customer Name:</label>{" "}
                              {taskDetails.first_name}{" "}
                              {taskDetails.last_name}
                            </div>
                          </Col>
                        )}

                        {this.checkProductRequest(taskDetails.request_type,
                        taskDetails.parent_id) 
                        &&
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Product:</label>{" "}
                              {taskDetails.product_name}
                            </div>
                          </Col>
                        }

                        {this.checkMarketRequest(taskDetails.request_type,
                        taskDetails.parent_id) 
                          && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Market:</label>{" "}
                              {taskDetails.country_name}
                            </div>
                          </Col>
                        )}

                        {this.checkSamplesWorkingImpureRequest(
                          taskDetails.request_type,
                          taskDetails.parent_id
                          ) === true && taskDetails.service_request_type.indexOf('Samples') !== -1 && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                            
                              <label>Samples:</label>
                              <p>Number of batches : {taskDetails.number_of_batches}</p>
                              {taskDetails.quantity && 
                                <p>Quantity : {this.splitQuantity(taskDetails.quantity)[0]} {this.splitQuantity(taskDetails.quantity)[1]}</p>
                              }
                              
                            </div>
                          </Col>
                        )}

                        {this.checkSamplesWorkingImpureRequest(
                          taskDetails.request_type,
                          taskDetails.parent_id
                          ) === true && taskDetails.service_request_type.indexOf('Working Standards') !== -1 && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Working Standards:</label>
                              {taskDetails.working_quantity && 
                                <p>Quantity : {this.splitQuantity(taskDetails.working_quantity)[0]} {this.splitQuantity(taskDetails.working_quantity)[1]} </p>
                              }
                            </div>
                          </Col>
                        )}

                        {this.checkSamplesWorkingImpureRequest(
                          taskDetails.request_type,
                          taskDetails.parent_id
                          ) === true && taskDetails.service_request_type.indexOf('Impurities') !== -1 && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Impurities:</label>
                              <p>Required impurity : {taskDetails.specify_impurity_required}</p>

                              {taskDetails.impurities_quantity && 
                                <p>Quantity : {this.splitQuantity(taskDetails.impurities_quantity)[0]} {this.splitQuantity(taskDetails.impurities_quantity)[1]} </p>
                              }
                            </div>
                          </Col>
                        )}

                        {this.checkSamplesWorkingImpureRequest(
                          taskDetails.request_type,
                          taskDetails.parent_id
                          ) === true && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Shipping Address:</label>
                              <p>{taskDetails.shipping_address}</p>
                            </div>
                          </Col>
                        )}

                        {/* post */}
                        {this.checkPharmacopoeia(
                          taskDetails.request_type,
                          taskDetails.parent_id
                        ) === true && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Pharmacopoeia:</label>{" "}
                              {htmlDecode(taskDetails.pharmacopoeia)}
                            </div>
                          </Col>
                        )}

                        {/* post */}
                        {this.checkPolymorphicForm(
                          taskDetails.request_type,
                          taskDetails.parent_id
                        ) === true && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Polymorphic Form:</label>{" "}
                              {htmlDecode(taskDetails.polymorphic_form)}
                            </div>
                          </Col>
                        )}

                        {taskDetails.parent_id === 0 && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Request Category:</label>{" "}
                              {this.setReqCategoryName(
                                taskDetails.request_category
                              )}
                            </div>
                          </Col>
                        )}

                        
                        {/* post */}
                        {this.checkStabilityDataType(
                          taskDetails.request_type,
                          taskDetails.parent_id
                        ) === true && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Stability Data Type:</label>{" "}
                              {taskDetails.stability_data_type}
                            </div>
                          </Col>
                        )}

                        {/* post */}
                        {this.checkAuditSiteName(
                          taskDetails.request_type,
                          taskDetails.parent_id
                        ) === true && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Site Name:</label>{" "}
                              {taskDetails.audit_visit_site_name}
                            </div>
                          </Col>
                        )}

                        {/* post */}
                        {this.checkAuditVistDate(
                          taskDetails.request_type,
                          taskDetails.parent_id
                        ) === true && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Audit/Visit Date:</label>{" "}
                              {this.setDateFormat(
                                taskDetails.request_audit_visit_date
                              )}
                            </div>
                          </Col>
                        )}

                        {this.checkQuantity(
                          taskDetails.request_type,
                          taskDetails.parent_id
                        ) === true && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Quantity:</label>{" "}
                              {taskDetails.quantity}
                            </div>
                          </Col>
                        )}

                        {this.checkBatchNo(
                          taskDetails.request_type,
                          taskDetails.parent_id
                        ) === true && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Batch No:</label>{" "}
                              {taskDetails.batch_number}
                            </div>
                          </Col>
                        )}

                        {this.checkForecastNotificationDate(
                          taskDetails.request_type,
                          taskDetails.parent_id
                        ) === true && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Notification Date:</label>{" "}
                              {this.setCreateDate(
                                taskDetails.date_added
                              )}
                            </div>
                          </Col>
                        )}

                        {this.checkNatureOfIssue(
                          taskDetails.request_type,
                          taskDetails.parent_id
                        ) === true && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Nature Of Issue:</label>{" "}
                              {taskDetails.nature_of_issue}
                            </div>
                          </Col>
                        )}      

                        {this.checkRDD(
                          taskDetails.request_type,
                          taskDetails.parent_id
                        ) === true && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Required Date Of Delivery:</label>{" "}
                              {taskDetails.rdd !== null && this.setCreateDate(taskDetails.rdd) }
                            </div>
                          </Col>
                        )}                

                        {taskDetails.parent_id > 0 && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Title:</label> {taskDetails.title}
                            </div>
                          </Col>
                        )}

                        {taskDetails.parent_id > 0 && (
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>Content:</label>{" "}
                              {taskDetails.content}
                            </div>
                          </Col>
                        )}

                        {this.checkAmountPending(
                          taskDetails.request_type,
                          taskDetails.parent_id
                        ) === true && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Amount Pending:</label>{" "}
                              {taskDetails.payment_pending}
                            </div>
                          </Col>
                        )}

                        {this.checkDaysRemaining(
                          taskDetails.request_type,
                          taskDetails.parent_id
                        ) === true && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Days Remaining:</label>{" "}
                              {taskDetails.days_remaining}
                            </div>
                          </Col>
                        )}

                        {this.checkPaymentStatus(
                          taskDetails.request_type,
                          taskDetails.parent_id
                        ) === true && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>status:</label>{" "}
                              {taskDetails.payment_status}
                            </div>
                          </Col>
                        )}


                        {this.checkPaymentDueDate(
                          taskDetails.request_type,
                          taskDetails.parent_id
                        ) === true && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Due Date:</label>{" "}
                              {this.setCreateDate(
                                taskDetails.due_date
                              )}
                            </div>
                          </Col>
                        )}

                        {this.checkNotificationRequestType(
                          taskDetails.request_type,
                          taskDetails.parent_id
                        ) === true && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Request Type:</label>{" "}
                              {taskDetails.notification_type}
                            </div>
                          </Col>
                        )}
                        
                        {this.checkNotificationTargetApproval(
                          taskDetails.request_type,
                          taskDetails.parent_id
                        ) === true && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Target for Approval/Feedback:</label>{" "}
                              {this.setCreateDate( taskDetails.target_for_approval)}
                            </div>
                          </Col>
                        )}

                        {this.checkNotificationTargetImplementation(
                          taskDetails.request_type,
                          taskDetails.parent_id
                        ) === true && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Target for Implementation:</label>{" "}
                              {this.setCreateDate(taskDetails.target_for_implementation)}
                            </div>
                          </Col>
                        )}

                        {/* post */}
                        {taskDetails.parent_id === 0 && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Requirement:</label>{" "}
                              {htmlDecode(taskDetails.content)}
                            </div>
                          </Col>
                        )}

                        {(taskDetails.submitted_by > 0) && (
                          <Col xs={12} sm={6} md={4}>
                            <div className="form-group">
                              <label>Posted By Agent:</label>{" "}
                              {taskDetails.agnt_first_name}{" "}
                              {taskDetails.agnt_last_name}{" "}
                            </div>
                          </Col>
                        )}

                          {this.state.taskDetailsFiles.length > 0 &&
                            <Col xs={12}>
                                <div className="mb-20"> 
                                    <label>Attachment</label>
                                    <div className="cqtDetailsDeta-btm">
                                      <ul className='conDocList'>{fileList}</ul>  
                                    </div>                                                                        
                                </div>
                            </Col>
                          }
                      </Row>
                      
                                      
                      {this.state.subTasks.length > 0 && 
                        <>
                        <label>Sub Tasks:</label>

                        {taskDetails.parent_id === 0 && 
                        this.state.has_discussion === 1 &&
                        <Row>
                          
                        <Col xs={12} sm={6} md={12}>
                            <div className="form-group">
                            <Link
                              to={{ pathname: '/user/discussion_details/'+taskDetails.task_id }} target="_blank" style={{cursor:'pointer'}}
                              className="plus-request edit-button"
                            >
                              <i className="far fa-edit" aria-hidden="true" />
                              Discussions
                            </Link>
                            </div>
                          </Col>
                          </Row>
                          
                        }   
                        <br></br>
                        
                        <PanelGroup accordion id="task_panel" defaultActiveKey={this.state.subTasks[0].task_id} >
                            {this.createAccordion()}
                        </PanelGroup>
                        </>
                      }     

                              

                    </div>

                    {taskDetails.cqt > 0 ? this.getCQT() : ""}

                    {taskDetails.request_type === 23 ? this.getSAP() : ""}
                    
                    {/* taskdetails Panel Btm Listing End*/}

                    </div>
                  </div>
                </div>
                {this.state.commentExists && <div className="tab-pane" id="show_tab_2">
                  <div className="boxPapanel content-padding">
                    <div className="taskdetails">
                    {this.getComments()}

                    {(taskDetails.assigned_to === my_id || taskDetails.assigned_by === my_id) && taskDetails.responded === 0 && 
                    <Formik
                        initialValues={initialValues}
                        validationSchema={commentSchema}
                        onSubmit={this.submitComment}
                    >
                        {({
                            values,
                            errors,
                            touched,
                            handleChange,
                            handleBlur,
                            setFieldValue
                        }) => {

                            return (
                                <div className="clearfix tdBtmlist">
                                <Form encType="multipart/form-data">
                                    <div className="commentBox1">
                                        <div className="form-group">
                                            <label>Enter your comment</label>
                                            
                                            <Field
                                                name="comment"
                                                component='textarea'
                                                className="form-control"
                                                autoComplete="off"
                                                placeholder="Comment here"
                                                onChange={handleChange}
                                            />
                                            {errors.comment && touched.comment && <label className="redError">{errors.comment}</label>}
                                        </div>
                                        <div className="form-group">
                                            <label>Attachments</label>
                                            <input 
                                                id="file" 
                                                name="file" 
                                                type="file" 
                                                onChange={(event) => {                                            
                                                    this.setState({ file : event.target.files[0] })
                                                    setFieldValue("file", event.target.files[0]);
                                                }}
                                                ref={ref=> this.fileInput = ref}
                                            />

                                            {errors.file && touched.file ? (
                                            <span className="errorMsg">
                                                {errors.file}
                                            </span>
                                            ) : null}
                                        </div>
                                        <div className="button-footer">
                                            <Button 
                                                type="submit" 
                                                className="btn btn-success btn-flat"
                                            > Submit </Button>
                                        
                                            <Button
                                                onClick={this.closeBrowserWindow}
                                                className={`btn btn-danger btn-flat`}
                                                type="button"
                                            >
                                                Cancel
                                            </Button>
                                        </div>
                                    </div>
                                </Form>
                                </div>
                                );
                        }}
                    </Formik>}
                    </div>
                  </div>
                </div>}
                {this.state.discussionExists && this.state.discussDetails.length > 0 && <div className="tab-pane" id="show_tab_3">
                    <div className="row">
                    <div className="col-xs-12">

                      <div className="nav-tabs-custom">

                        <ul className="nav nav-tabs">
                          <li className="active" onClick={(e) => this.handleTabs2(e)} id="d_tab_1" >DISCUSSION DETAILS</li>
                          <li onClick={(e) => this.handleTabs2(e)} id="d_tab_2">FILES</li>
                        </ul>

                        <div className="tab-content">

                          <div className="tab-pane active" id="show_d_tab_1">
                            <section className="content-header">
                              <h1>Discussion Details</h1>
                            </section>
                            <section className="content">
                              <div className="boxPapanel content-padding">
                                <div className="disHead">
                                  <h3>
                                    {this.state.discussReqName} | {this.state.discussTaskRef}
                                  </h3>
                                </div>

                                <div className="comment-list-main-wrapper">
                                  {this.state.discussDetails.map((comments, i) => (
                                    <div key={i} className="comment">
                                      <div className="row">
                                        <div className="col-md-10 col-sm-10 col-xs-9">
                                          <div className="imageArea">
                                            <div className="imageBorder">
                                              <img alt="noimage" src={commenLogo} />
                                            </div>
                                          </div>
                                          <div className="conArea">
                                            <p>
                                            {comments.added_by_type === "C" && <p>{`${comments.cust_fname} ${comments.cust_lname}`}</p> }

                                            {comments.added_by_type === "E" && <p>{`${comments.emp_fname} ${comments.emp_lname} (DRL)`}</p> }

                                            {comments.added_by_type === "A" && <p>{`${comments.agnt_first_name} ${comments.agnt_last_name} (Agent)`}</p> }
                                            </p>
                                            <span>{comments.comment}</span>
                                            {this.getCommentFile(comments.uploads)}
                                          </div>
                                          <div className="clearfix" />
                                        </div>

                                        <div className="col-md-2 col-sm-2 col-xs-3">
                                          <div className="dateArea">
                                            <p>
                                              {dateFormat(
                                                new Date(comments.date_added),
                                                "ddd, mmm dS, yyyy"
                                              )}
                                            </p>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  ))}
                                </div>

                                <Formik
                                  initialValues={initialValues}
                                  validationSchema={commentSchema}
                                  onSubmit={this.submitDiscussion}
                                >
                                  {({
                                    values,
                                    errors,
                                    touched,
                                    handleChange,
                                    handleBlur,
                                    setFieldValue
                                  }) => {
                                    console.log(errors);

                                    return (
                                      <div className="clearfix tdBtmlist">
                                        <Form encType="multipart/form-data">
                                          <div className="commentBox">
                                            <div className="form-group">
                                              {/*<label>Enter your comment</label>*/}

                                              <Field
                                                name="comment"
                                                component="textarea"
                                                className="form-control"
                                                autoComplete="off"
                                                placeholder="Enter your comment"
                                                onChange={handleChange}
                                              />
                                              {errors.comment && touched.comment && (
                                                <label className="redError">
                                                  {errors.comment}
                                                </label>
                                              )}
                                            </div>
                                            <div className="form-group">
                                              <input 
                                                  id="file" 
                                                  name="file" 
                                                  type="file" 
                                                  values=""
                                                  onChange={(event) => {                                            
                                                      this.setState({ 
                                                        file : event.target.files[0]
                                                      })
                                                      setFieldValue("file", event.target.files[0]);
                                                  }} 
                                                  ref={ref=> this.fileInputDiss = ref}
                                              />
                                            </div>

                                            <div className="button-footer commentBox-footer">
                                              <Button
                                                type="submit"
                                                className="btn btn-danger btn-flat"
                                              >
                                                {" "}
                                                Send{" "}
                                              </Button>
                                            </div>
                                          </div>
                                        </Form>
                                      </div>
                                    );
                                  }}
                                </Formik>
                              </div>
                            </section>
                          </div>

                          <div className="tab-pane" id="show_d_tab_2">
                            <section className="content-header">
                              <h1>File Details</h1>
                            </section>
                            <section className="content">
                              <div className="boxPapanel content-padding">
                                <div className="disHead">
                                  <h3>
                                    {this.state.discussReqName} | {this.state.discussTaskRef}
                                  </h3>
                                </div>

                                <div className="comment-list-main-wrapper">

                                  {this.state.discussFile &&
                                    this.state.discussFile !== '' &&
                                    this.state.discussFile.map((file, i) => (
                                      <div key={i} className="comment">
                                        <div className="row">
                                          <div className="col-md-10 col-sm-10 col-xs-9">
                                            <div className="imageArea">
                                              <div className="imageBorder">
                                                <img alt="noimage" src={commenLogo} />
                                              </div>
                                            </div>
                                            <div className="conArea">


                                              <a href={`${path}${file.upload_id}`} target="_blank" download rel="noopener noreferrer">
                                                {file.actual_file_name}
                                              </a>

                                            </div>
                                            <div className="clearfix" />
                                          </div>

                                          <div className="col-md-2 col-sm-2 col-xs-3">
                                            <div className="dateArea">
                                              <p>
                                                {dateFormat(
                                                  new Date(file.date_added),
                                                  "ddd, mmm dS, yyyy"
                                                )}
                                              </p>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    ))}
                                </div>
                              </div>
                            </section>

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>}
              </div>
            </section>
          </div>
        </>
      );
    } else {
      return (
        <>
          <div className="loderOuter">
              <div className="loading_reddy_outer">
                  <div className="loading_reddy" >
                      <img src={whitelogo} alt="loaderImage"/>
                  </div>
              </div>
            </div>
        </>
      );
    }
  }
}

export default TaskDetails;
