import React, { Component } from 'react';

import { Row, Col, ButtonToolbar, Button, Modal } from "react-bootstrap";
import axios from "axios";
import { Formik, Field, Form } from "formik"; 
import * as Yup from "yup"; 
import swal from "sweetalert";
import Loader from 'react-loader-spinner';

//import DatePicker from 'react-date-picker';

import dateFormat from 'dateformat';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
//import TinyMCE from 'react-tinymce';
import Select from "react-select";

import API from "../../shared/axios";

//import jp from 'jsonpath';

const initialValues = {
    title            : "",
    content             : "",
    employee_list_id : "",
    authorization    : "",
    dueDate          : ""
  };

class CreateSubTaskPopup extends Component {    

    constructor(props){
        super(props);

        this.state = {
            showCreateSubTask : false,
            task_id           : 0
        };
    }

    componentWillReceiveProps = (nextProps) => {
        if(nextProps.showCreateSubTask == true && nextProps.currRow.task_id > 0){

            var id = 1;
            this.setState({showCreateSubTask: nextProps.showCreateSubTask,currRow:nextProps.currRow});
            
            API.get(`/api/employees/other`)
            .then(res => {
                //console.log(res.data.data);

                var myTeam = [];
                for (let index = 0; index < res.data.data.length; index++) {
                    const element = res.data.data[index];
                    myTeam.push({
                    value: element["employee_id"],
                    label: element["first_name"] +" "+ element["last_name"] +" ("+ element["desig_name"] +")"
                    });
                }

                this.setState({employeeArr:myTeam});
            })
            .catch(err => {
                console.log(err);
            });
        }

    }

    handleClose = () => {
        var close = {showCreateSubTask: false,task_id:0};
        this.setState(close);
        this.props.handleClose(close);
    };

    handleSubmitCreateTask = (values, actions) => {
        var due_date = dateFormat(values.dueDate, "yyyy-mm-dd");
        const postData = {
            title:values.title,
            assigned_to:values.employee_id.value,
            content:values.content,
            need_authorization:values.authorization,
            due_date:due_date
        };

        //console.log(postData);
        

        API.post(`/api/tasks/create_sub/${this.state.currRow.task_id}`,postData).then(res => {
            this.handleClose();
            swal("Sub Task created successfully!", {
                icon: "success"
            }).then(() => {
                this.props.reloadTask();
            });
        });
    }

    changeDueDate = (value) =>{
        initialValues.dueDate = value; 
    }

    getDueDate = () =>{
        if(typeof this.state.currRow !== 'undefined'){
            //console.log(this.state.currRow.due_date);
            var due_date = dateFormat(this.state.currRow.due_date, "yyyy-mm-dd");
            //var replacedDate = this.state.currRow.due_date.split('T');
            //var dateFormat = replacedDate[0];
            return new Date(due_date);
        }
    }

    render(){

        const validateStopFlag = Yup.object().shape({
            title       : Yup.string().trim()
                .required("Please enter title"),
            content        : Yup.string().trim()
                .required("Please give description"),
            employee_id : Yup.string().trim()
                .required("Please select employee"),   
            dueDate     : Yup.string().trim()
                .required("Please enter due date")
        });

        const newInitialValues = Object.assign(initialValues, {
            title            : "",
            content          : "",
            employee_id      : "",
            authorization    : 0,
            dueDate          : this.getDueDate()
          });
          
            return(
                <>
                {typeof this.state.employeeArr !== 'undefined' && 
                <Modal show={this.state.showCreateSubTask} onHide={() => this.handleClose()} backdrop="static">
                    <Formik
                        initialValues    = {newInitialValues}
                        validationSchema = {validateStopFlag}
                        onSubmit         = {this.handleSubmitCreateTask}
                    >
                        {({ values, errors, touched, isValid, isSubmitting, setFieldValue }) => {
                        return (
                            <Form>
                            <Modal.Header closeButton>
                                <Modal.Title>
                                Create Sub Task
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div className="contBox">
                                <Row>
                                    <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                        Title
                                        <Field
                                        name="title"
                                        type="text"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.title}
                                        > 
                                        </Field>
                                        {errors.title && touched.title ? (
                                        <span className="errorMsg">
                                            {errors.title}
                                        </span>
                                        ) : null}
                                    </div>
                                    </Col>
                                    <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                        Employee
                                        {/* <Field
                                        name="employee_id"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.employee_id}
                                        >
                                        <option key='-1' value="" >Employee</option>
                                            {this.state.employeeArr.map((emp, i) => (
                                                <option key={i} value={emp.employee_id}>
                                                    {emp.first_name} {emp.last_name} ({emp.desig_name})
                                                </option>
                                            ))}
                                        </Field> */}

                                        <Select
                                            className="basic-single"
                                            classNamePrefix="select"
                                            defaultValue={values.employee_id}
                                            isClearable={true}
                                            isSearchable={true}
                                            name="employee_id"
                                            options={this.state.employeeArr}
                                            onChange={value =>
                                                setFieldValue("employee_id", value)
                                            }
                                        />

                                        {errors.employee_id && touched.employee_id ? (
                                        <span className="errorMsg">
                                            {errors.employee_id}
                                        </span>
                                        ) : null}
                                    </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col xs={12} sm={12} md={12}>
                                    <div className="form-group">
                                        Description
                                        <Field
                                        name="content"
                                        component="textarea"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.content}
                                        > 
                                        </Field>

                                        {/* <TinyMCE
                                            name="content"
                                            content={values.comment}
                                            config={{
                                                branding: false,
                                                toolbar: 'undo redo | bold italic | alignleft aligncenter alignright'
                                            }}
                                            className={`selectArowGray form-control`}
                                            autoComplete="off"
                                            onChange={value =>
                                                setFieldValue("content", value.level.content)
                                            }
                                        /> */}

                                        {errors.content && touched.content ? (
                                        <span className="errorMsg">
                                            {errors.content}
                                        </span>
                                        ) : null}
                                    </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                        Due Date
                                        {/* <DatePicker
                                            name="dueDate"
                                            onChange={value =>
                                                setFieldValue("dueDate", value)
                                            }
                                            value={values.dueDate}
                                        /> */}
                                        <div className="form-control">
                                            <DatePicker
                                                name={'dueDate'}
                                                className="borderNone"
                                                selected={values.dueDate ? values.dueDate : null}
                                                onChange={value => setFieldValue('dueDate', value)}
                                                dateFormat='dd/MM/yyyy'
                                            />

                                            {errors.dueDate && touched.dueDate ? (
                                            <span className="errorMsg">
                                                {errors.dueDate}
                                            </span>
                                            ) : null}
                                        </div>
                                    </div>
                                    </Col>
                                    <Col xs={12} sm={6} md={6}>
                                    <div className="form-group">
                                        Need Approval
                                        <Field
                                        name="authorization"
                                        component="select"
                                        className={`selectArowGray form-control`}
                                        autoComplete="off"
                                        value={values.authorization}
                                        >
                                        <option key='0' value="0" >No</option>
                                        <option key='1' value="1" >Yes</option>
                                        </Field>
                                    </div>
                                    </Col>
                                </Row>

                                </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <ButtonToolbar>
                                <Button
                                    className={`btn btn-success btn-sm ${
                                    isValid ? "btn-custom-green" : "btn-disable"
                                    } m-r-10`}
                                    type="submit"
                                    disabled={isValid ? false : true}
                                >
                                    {this.state.stopflagId > 0
                                    ? isSubmitting
                                        ? "Updating..."
                                        : "Update"
                                    : isSubmitting
                                    ? "Submitting..."
                                    : "Submit"}
                                </Button>
                                <Button
                                    onClick={this.handleClose}
                                    className={`btn btn-danger btn-sm`}
                                    type="button"
                                >
                                    Close
                                </Button>
                                </ButtonToolbar>
                            </Modal.Footer>
                            </Form>
                        );
                        }}
                    </Formik>
                    </Modal>}
                </>
            );
    }
}

export default CreateSubTaskPopup;