import React, { Component } from 'react';
import { Row, Col, Button } from "react-bootstrap";
import dateFormat from 'dateformat';

import API from "../../shared/axios";

import { Redirect } from 'react-router-dom';
import { Formik, Field, Form } from "formik"; 
import * as Yup from "yup";
import Loader from 'react-loader-spinner';
import DatePicker from 'react-date-picker';
import swal from "sweetalert";
import { localDate } from '../../shared/helper';

const initialValues = {
    country_id      : 0,
    product_id      : 0,
    content         : '',
    pharmacopoeia   : '',
    polymorphic_form : '',
    nature_of_issue:'',
    batch_number:'',
    quantity:'',
    units:'',
    rdd:'',
    stability_data_type : '',
    audit_visit_site_name: '',
    request_audit_visit_date : ''
};

const validateEditTask = Yup.object().shape({
    country_id      : Yup.string()
        .required("Please select country"),
    product_id      : Yup.string()
        .required("Please select product")
});

class EditTaskDetails extends Component {

    constructor(props){
        super(props);

        //const {changeStatus} = this.props.location.state;

        //console.log(this.props);
        this.state = {
            taskDetails: [],
            country : [],
            product : [],
            task_id : this.props.match.params.id,
            assign_id: this.props.match.params.assign_id
        };
    }

    fetchTaskList =  ( id,assign_id ) => (API.get( `/api/tasks/${id}/${assign_id}` ));

    fetchCountryList =  () => (API.get( `/api/feed/country` ));

    fetchProductList =  () => (API.get( `/api/feed/products` ));

    componentDidMount = () => {

        if(this.state.task_id > 0){

            Promise.all([this.fetchTaskList(this.state.task_id,this.state.assign_id), this.fetchCountryList(), this.fetchProductList()])
                .then((data) => {

                    //console.log( data );

                    if(data[0].status === 200){
                        
                        this.setState({
                            taskDetails: data[0].data.data.taskDetails
                        });

                        var quantity = [];

                        if(this.state.taskDetails.request_type === 7 || this.state.taskDetails.request_type === 23){
                            quantity = this.state.taskDetails.quantity.split(" ");
                        }

                        initialValues.country_id            = this.state.taskDetails.country_id
                        initialValues.product_id            = this.state.taskDetails.product_id
                        initialValues.content               = this.state.taskDetails.content
                        initialValues.pharmacopoeia         = this.state.taskDetails.pharmacopoeia
                        initialValues.polymorphic_form      = this.state.taskDetails.polymorphic_form
                        initialValues.nature_of_issue       = this.state.taskDetails.nature_of_issue
                        initialValues.batch_number          = this.state.taskDetails.batch_number

                        initialValues.quantity              = quantity[0]
                        initialValues.units                 = quantity[1]
                        initialValues.rdd                   = localDate(this.state.taskDetails.rdd)
                        
                        initialValues.stability_data_type   = this.state.taskDetails.stability_data_type
                        initialValues.audit_visit_site_name = this.state.taskDetails.audit_visit_site_name
                        initialValues.request_audit_visit_date  = localDate(this.state.taskDetails.request_audit_visit_date)
                    }
                    //console.log(initialValues)

                    if(data[1].status === 200){
                        this.setState({
                            country: data[1].data.data
                        });
                    }

                    if(data[2].status === 200){
                        this.setState({
                            product: data[2].data.data
                        });
                    }
                })
                .catch((err) => {
                    console.log(err)
                });
        }        
    }

    closeBrowserWindow = () => {
        window.close()
    }    

    checkPharmacopoeia(request_type, parent_id){

        if(request_type === null){
            return false;
        }else{
            if((request_type === 22 || request_type === 21 || request_type === 19 || request_type === 18 || request_type === 17 || request_type === 2 || request_type === 3 || request_type === 4 || request_type === 10 || request_type === 12) && parent_id === 0){
                return true;
            }else{
                return false;
            }
        }
        
    }

    checkPolymorphicForm(request_type, parent_id){
        if(request_type === null){
            return false;
        }else{
            if((request_type === 2 || request_type === 3) && parent_id === 0){
                return true;
            }else{
                return false;
            }
        }
    }

    checkStabilityDataType(request_type, parent_id){
        if(request_type === null){
            return false;
        }else{
            if(request_type === 13 && parent_id === 0){
                return true;
            }else{
                return false;
            }
        }
    }

    checkAuditSiteName(request_type, parent_id){
        if(request_type === null){
            return false;
        }else{
            if(request_type === 9 && parent_id === 0){
                return true;
            }else{
                return false;
            }
        }
    }

    checkAuditVistDate(request_type, parent_id){
        if(request_type === null){
            return false;
        }else{
            if(request_type === 9 && parent_id === 0){
                return true;
            }else{
                return false;
            }
        }
    }

    checkQuantity(request_type, parent_id) {
        if (request_type === null) {
          return false;
        } else {
          if ((request_type === 7 || request_type === 23) && parent_id === 0) {
            return true;
          } else {
            return false;
          }
        }
      }
    
      checkBatchNo(request_type, parent_id) {
        if (request_type === null) {
          return false;
        } else {
          if (request_type === 7 && parent_id === 0) {
            return true;
          } else {
            return false;
          }
        }
      }
    
      checkNatureOfIssue(request_type, parent_id) {
        if (request_type === null) {
          return false;
        } else {
          if (request_type === 7 && parent_id === 0) {
            return true;
          } else {
            return false;
          }
        }
      }
    
      checkRDD(request_type, parent_id) {
        if (request_type === null) {
          return false;
        } else {
          if (request_type === 23 && parent_id === 0) {
            return true;
          } else {
            return false;
          }
        }
      }

    setDateFormat(date_value){
        var mydate = localDate(date_value);
        return dateFormat(mydate, "dd/mm/yyyy");
    }

    submitEditTask = (values, actions) => {
        
        let 
            req_type = this.state.taskDetails.request_type, 
            pid = this.state.taskDetails.parent_id,
            postObject = {};

            postObject.product_id = values.product_id;
            postObject.country_id = values.country_id;

        if(this.checkPharmacopoeia(req_type, pid) === true) {
            postObject.pharmacopoeia = values.pharmacopoeia;
        }

        if(this.checkPolymorphicForm(req_type, pid) === true) {            
            postObject.polymorphic_form = values.polymorphic_form;
        }

        if(this.checkNatureOfIssue(req_type, pid) === true) {            
            postObject.nature_of_issue = values.nature_of_issue;
        }

        if(this.checkBatchNo(req_type, pid) === true) {            
            postObject.batch_number = values.batch_number;
        }

        if(this.checkQuantity(req_type, pid) === true) {            
            postObject.quantity = values.quantity +' '+values.units;
        }

        if(this.checkRDD(req_type, pid) === true) {       
            var rdd_format = dateFormat(values.rdd, "yyyy-mm-dd");     
            postObject.rdd = rdd_format;
        }
        
        if(this.checkAuditVistDate(req_type, pid) === true) {            
            postObject.request_audit_visit_date = values.request_audit_visit_date;
        }

        if(this.checkStabilityDataType(req_type, pid) === true) {            
            postObject.stability_data_type = values.stability_data_type;
        }        

        if(this.checkAuditSiteName(req_type, pid) === true) {
            postObject.audit_visit_site_name = values.audit_visit_site_name;
        }

        if(pid === 0){
            postObject.content = values.content;
        }

        API.put(`/api/tasks/${this.state.task_id}`,postObject)
        .then(response => {
            swal({
                closeOnClickOutside: false,
                title: "Success",
                text: "Task has been updated successfully.",
                icon: "success"
              }).then(() => {
                this.props.history.push(`/user/task_details/${this.state.task_id}/${this.state.assign_id}`)
            })
        })
        .catch(err => {
            console.log(err);
        });

        //console.log( postObject );
    }

    render(){

        // SATYAJIT
        if(this.props.match.params.id === 0 || this.props.match.params.id === ""){
            return <Redirect to='/user/dashboard'/>
        }
        
        console.log("task id =====", this.state.taskDetails )

        if(this.state.taskDetails.task_id > 0) {

            return(
                <>
                    <div className="content-wrapper">
                        <section className="content">
                        <div className="boxPapanel content-padding">

                            <div className="taskdetails">
                                
                                <div className="taskdetailsHeader">
                                    <h3>{this.state.taskDetails.req_name}</h3>     
                                </div>
                                
                                
                                <div className="clearfix tdBtmlist">

                                <Formik
                                    initialValues    = {initialValues}
                                    validationSchema = {validateEditTask}
                                    onSubmit         = {this.submitEditTask}
                                >
                            {({ values, errors, touched, isValid, isSubmitting, setFieldValue }) => {
                            return (
                                    <Form>
                                        <Row>

                                        <Col xs={12} sm={6} md={3}>
                                            <div className="form-group  has-feedback">
                                                <label>Product</label>
                                                
                                                <Field
                                                    name="product_id"
                                                    component="select"
                                                    className="form-control"
                                                    autoComplete="off"
                                                    value={values.product_id}
                                                >
                                                <option key='-1' value="" >Select Product</option>
                                                    {this.state.product.map((products, i) => (
                                                        <option key={i} value={products.product_id}>
                                                            {products.product_name}
                                                        </option>
                                                    ))}
                                                </Field>
                                                {errors.product_id && touched.product_id ? (
                                                <span className="errorMsg">
                                                    {errors.product_id}
                                                </span>
                                                ) : null}

                                            </div>
                                        </Col>

                                        <Col xs={12} sm={6} md={3}>
                                            <div className="form-group  has-feedback">
                                                <label>Market</label>

                                                <Field
                                                    name="country_id"
                                                    component="select"
                                                    className="form-control"
                                                    autoComplete="off"
                                                    value={values.country_id}
                                                >
                                                <option key='-1' value="" >Select Country</option>
                                                    {this.state.country.map((countries, k) => (
                                                        <option key={k} value={countries.country_id}>
                                                            {countries.country_name}
                                                        </option>
                                                    ))}
                                                </Field>
                                                {errors.country_id && touched.country_id ? (
                                                <span className="errorMsg">
                                                    {errors.country_id}
                                                </span>
                                                ) : null}

                                            </div>
                                        </Col>
                                        
                                        {
                                            this.checkPharmacopoeia(this.state.taskDetails.request_type, this.state.taskDetails.parent_id) === true && 
                                            
                                            <Col xs={12} sm={6} md={3}>
                                                <div className="form-group">
                                                    <label>Pharmacopoeia:</label> 
                                                    <Field
                                                        name="pharmacopoeia"
                                                        type="text"
                                                        className="form-control"
                                                        autoComplete="off"
                                                        value={(values.pharmacopoeia !== null && values.pharmacopoeia !== "") ? values.pharmacopoeia : ''}
                                                    > 
                                                    </Field>
                                                    {errors.pharmacopoeia && touched.pharmacopoeia ? (
                                                    <span className="errorMsg">
                                                        {errors.pharmacopoeia}
                                                    </span>
                                                    ) : null}
                                                </div>
                                            </Col>
                                        }

                                        {
                                            this.checkNatureOfIssue(this.state.taskDetails.request_type,this.state.taskDetails.parent_id) === true && 
                                            <Col xs={12} sm={6} md={3}>
                                                <div className="form-group">
                                                    <label>Nature Of Issue:</label>

                                                    <Field
                                                        name="nature_of_issue"
                                                        type="text"
                                                        className="form-control"
                                                        autoComplete="off"
                                                        value={(values.nature_of_issue !== null && values.nature_of_issue !== "") ? values.nature_of_issue : ''}                                                    
                                                    >
                                                    </Field>
                                                    {errors.nature_of_issue && touched.nature_of_issue ? (
                                                    <span className="errorMsg">
                                                        {errors.nature_of_issue}
                                                    </span>
                                                    ) : null}

                                                </div>
                                            </Col>
                                        }

                                        {
                                            this.checkBatchNo(this.state.taskDetails.request_type,this.state.taskDetails.parent_id) === true && 
                                            <Col xs={12} sm={6} md={3}>
                                                <div className="form-group">
                                                    <label>Batch No:</label>

                                                    <Field
                                                        name="batch_number"
                                                        type="text"
                                                        className="form-control"
                                                        autoComplete="off"
                                                        value={(values.batch_number !== null && values.batch_number !== "") ? values.batch_number : ''}                                                    
                                                    >
                                                    </Field>
                                                    {errors.batch_number && touched.batch_number ? (
                                                    <span className="errorMsg">
                                                        {errors.batch_number}
                                                    </span>
                                                    ) : null}

                                                </div>
                                            </Col>
                                        }

                                        {
                                            this.checkQuantity(this.state.taskDetails.request_type,this.state.taskDetails.parent_id) === true && 
                                            <>
                                            <Col xs={12} sm={6} md={3}>
                                                <div className="form-group">
                                                    <label>Quantity:</label>

                                                    <Field
                                                        name="quantity"
                                                        type="text"
                                                        className="form-control"
                                                        autoComplete="off"
                                                        value={(values.quantity !== null && values.quantity !== "") ? values.quantity : ''}                                                    
                                                    >
                                                    </Field>
                                                    {errors.quantity && touched.quantity ? (
                                                    <span className="errorMsg">
                                                        {errors.quantity}
                                                    </span>
                                                    ) : null}

                                                </div>
                                            </Col>
                                            <Col xs={12} sm={6} md={3}>
                                                <div className="form-group">
                                                    <label>Units:</label>

                                                    <Field
                                                    name="units"
                                                    component="select"
                                                    className={`selectArowGray form-control`}
                                                    autoComplete="off"
                                                    value={values.units}
                                                    >
                                                    <option key='-1' value="" >Select</option>
                                                    <option key='1' value="Mgs" >Mgs</option>
                                                    <option key='2' value="Grams" >Grams</option>
                                                    <option key='3' value="Kgs" >Kgs</option>
                                                    
                                                    </Field>
                                                    {errors.units && touched.units ? (
                                                    <span className="errorMsg">
                                                        {errors.units}
                                                    </span>
                                                    ) : null}

                                                </div>
                                            </Col>
                                            </>
                                        }        


                                        {
                                            this.checkRDD(this.state.taskDetails.request_type,this.state.taskDetails.parent_id) === true && 
                                            <Col xs={12} sm={6} md={3}>
                                                <div className="form-group">
                                                    <label>Request Date Of Delivery:</label>
                                                    <div className="form-control">
                                                        <DatePicker
                                                            name="rdd"
                                                            onChange={value =>
                                                                setFieldValue("rdd",value)
                                                            }
                                                            value={values.rdd}
                                                        />

                                                        {errors.rdd && touched.rdd ? (
                                                        <span className="errorMsg">
                                                            {errors.rdd}
                                                        </span>
                                                        ) : null}
                                                    </div>
                                                </div>
                                            </Col>
                                        }                                


                                        {
                                            this.checkPolymorphicForm(this.state.taskDetails.request_type,this.state.taskDetails.parent_id) === true && 
                                            <Col xs={12} sm={6} md={3}>
                                                <div className="form-group">
                                                    <label>Polymorphic Form:</label>

                                                    <Field
                                                        name="polymorphic_form"
                                                        type="text"
                                                        className="form-control"
                                                        autoComplete="off"
                                                        value={(values.polymorphic_form !== null && values.polymorphic_form !== "") ? values.polymorphic_form : ''}                                                    
                                                    >
                                                    </Field>
                                                    {errors.polymorphic_form && touched.polymorphic_form ? (
                                                    <span className="errorMsg">
                                                        {errors.polymorphic_form}
                                                    </span>
                                                    ) : null}

                                                </div>
                                            </Col>
                                        }

                                        {
                                            this.checkStabilityDataType(this.state.taskDetails.request_type,this.state.taskDetails.parent_id) === true && 
                                            <Col xs={12} sm={6} md={3}>
                                                <div className="form-group">
                                                    <label>Stability Data Type:</label>

                                                    <Field
                                                        name="stability_data_type"
                                                        component="select"
                                                        className="form-control"
                                                        autoComplete="off"
                                                        value={values.stability_data_type}
                                                    >
                                                        <option key='-1' value="" >Select Type</option>
                                                        <option key='1' value='ACC'>ACC</option>
                                                        <option key='2' value='Long Term'>Long Term</option>
                                                        <option key='3' value='Others'>Others</option>
                                                    </Field>
                                                    {errors.stability_data_type && touched.stability_data_type ? (
                                                    <span className="errorMsg">
                                                        {errors.stability_data_type}
                                                    </span>
                                                    ) : null}

                                                </div>
                                            </Col>
                                        }

                                        {
                                            this.checkAuditSiteName(this.state.taskDetails.request_type,this.state.taskDetails.parent_id) === true && 
                                            <Col xs={12} sm={6} md={3}>
                                                <div className="form-group">
                                                    <label>Site Name:</label>

                                                    <Field
                                                        name="audit_visit_site_name"
                                                        type="text"
                                                        className="form-control"
                                                        autoComplete="off"
                                                        value={(values.audit_visit_site_name !== null && values.audit_visit_site_name !== "") ? values.audit_visit_site_name : ''}
                                                    > 
                                                    </Field>

                                                    {errors.audit_visit_site_name && touched.audit_visit_site_name ? (
                                                    <span className="errorMsg">
                                                        {errors.audit_visit_site_name}
                                                    </span>
                                                    ) : null}

                                                </div>
                                            </Col>
                                        }


                                        {
                                            this.checkAuditVistDate(this.state.taskDetails.request_type,this.state.taskDetails.parent_id) === true && 
                                            <Col xs={12} sm={6} md={3}>
                                                <div className="form-group">
                                                    <label>Audit/Visit Date:</label>
                                                    <div className="form-control">
                                                        <DatePicker
                                                            name="request_audit_visit_date"
                                                            onChange={value =>
                                                                setFieldValue("request_audit_visit_date",value)
                                                            }
                                                            value={values.request_audit_visit_date}
                                                        />

                                                        {errors.request_audit_visit_date && touched.request_audit_visit_date ? (
                                                        <span className="errorMsg">
                                                            {errors.request_audit_visit_date}
                                                        </span>
                                                        ) : null}
                                                    </div>
                                                </div>
                                            </Col>
                                        }

                                        {this.state.taskDetails.parent_id === 0 && 
                                        <Col xs={12}>
                                            <div className="form-group">
                                                <label>Requirement:</label> 
                                                    <Field
                                                        name="content"
                                                        component="textarea"
                                                        className="form-control"
                                                        autoComplete="off"
                                                        value={(values.content !== null && values.content !== "") ? values.content : ''}
                                                    > 
                                                    </Field>
                                                    {errors.content && touched.content ? (
                                                    <span className="errorMsg">
                                                        {errors.content}
                                                    </span>
                                                    ) : null}
                                            </div>
                                        </Col>}
                                    

                                        <Col xs={12}>
                                            <div className="input-group-btn">
                                                
                                                <Button
                                                    className="btn btn-success btn-flat"
                                                    type="submit"
                                                >
                                                    Update
                                                </Button>

                                                <Button
                                                    onClick={this.closeBrowserWindow}
                                                    className={`btn btn-danger btn-flat`}
                                                    type="button"
                                                >
                                                    Cancel
                                                </Button>
                                            </div>
                                        </Col>
                                        </Row>
                                    </Form>
                                );
                            }}
                                </Formik>

                                </div>
                            </div>

                            </div>
                        </section>
                    </div>
                </>    
            );
        } else {
            
            return (
                <>
                    <div className="loading_reddy_outer">
                        <div className="loading_reddy" >
                            <Loader 
                                type="Puff"
                                color="#00BFFF"
                                height="50"	
                                width="50"
                                verticalAlign="middle"
                            />
                        </div>
                    </div>
                </>
            );
        }
        
    }
}

export default EditTaskDetails;