import React, { Component } from "react";
import { Row, Col, ButtonToolbar, Button, Modal, Alert } from "react-bootstrap";

//import { FilePond } from 'react-filepond';
//import 'filepond/dist/filepond.min.css';
import Dropzone from "react-dropzone";

import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import dateFormat from "dateformat";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";
import API from "../../shared/axios";
import { showErrorMessageFront } from "../../shared/handle_error_front";

import swal from "sweetalert";

import { localDate, trimString } from "../../shared/helper";

import { Editor } from "@tinymce/tinymce-react";
//import uploadAxios from 'axios';

import TinyMCE from "react-tinymce";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";

const initialValues = {
  employeeId      : "",
  comment         : "",
  breach_reason   : "",
  file            : "",
  dueDate         : "",
  showModalLoader : false,
  file_name       : [],
  case_id         : ""
};

//console.log( TinyMCE );

// const onlyUnique = (value, index, self) => {
//     return self.indexOf(value) === index;
// }

const removeDropZoneFiles = (fileName, objRef, setErrors) => {
  var newArr = [];
  for (let index = 0; index < objRef.state.files.length; index++) {
    const element = objRef.state.files[index];

    if (fileName === element.name) {
    } else {
      newArr.push(element);
    }
  }

  var fileListHtml = newArr.map((file) => (
    <Alert key={file.name}>
      <span onClick={() => removeDropZoneFiles(file.name, objRef, setErrors)}>
        <i className="far fa-times-circle"></i>
      </span>{" "}
      {file.name}
    </Alert>
  ));
  setErrors({ file_name: "" });
  objRef.setState({
    files: newArr,
    filesHtml: fileListHtml,
  });
};

class RequestCaseChanges extends Component {
  constructor(props) {
    super(props);
    this.state = {
      requestCaseChanges: false,
      requestCaseChangesAfterSO:false,
      files: [],
      sla_breach: false,
      showModalLoader: false,
      acceptNewEmp: false,
      sales_order_number:'',
      acceptNewEmpMsg: "",
      filesHtml: "",
    };
  }

  handleChange = (e, field) => {
    this.setState({
      [field]: e.target.value,
    });
  };

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.requestCaseChanges === true && nextProps.currRow.task_id > 0) {
      this.setState({
        requestCaseChanges: nextProps.requestCaseChanges,
        currRow: nextProps.currRow,
        caseData:nextProps.caseData
      });
    }else if(nextProps.requestCaseChangesAfterSO === true && nextProps.currRow.task_id > 0){
      this.setState({
        requestCaseChangesAfterSO: nextProps.requestCaseChangesAfterSO,
        currRow: nextProps.currRow,
        sales_order_number: nextProps.sales_order_number,
        caseData:[]
      });
    }
  };

  handleClose = () => {
    var close = {
      sla_breach: false,
      new_employee: false,
      msg_new_employee: "",
      showModalLoader: false,
      requestCaseChanges: false,
      requestCaseChangesAfterSO:false,
      sales_order_number:'',
      files: [],
      filesHtml: "",
    };
    this.setState(close);
    this.props.handleClose(close);
  };

  handleRequestToReopen = (values, actions) => {
    //console.log(values);
    this.setState({ showModalLoader: true, msg_new_employee: false });
    // console.log(this.state.currRow);
    var formData = new FormData();
    formData.append("comment", values.comment.trim());
    if(this.state.requestCaseChanges){
      formData.append("case_id", values.case_id.value);
    }

    if(this.state.requestCaseChangesAfterSO){
      formData.append("so_number", values.so_number);
    }

    formData.append("assign_id", this.state.currRow.assignment_id);

    if(this.state.files && this.state.files.length > 0){
      for(let index = 0; index < this.state.files.length; index++){
        const element = this.state.files[index];
        formData.append("file", element);
      }
    }else{
      formData.append("file", "");
    }

    let url = '';

    if(this.state.requestCaseChanges){
      url = `/api/add_task/request_case_changes/${this.state.currRow.task_id}`;
    }else{
      url = `/api/add_task/request_case_changes_so/${this.state.currRow.task_id}`;
    }
    
    API.post(
      url,
      formData
    )
      .then((res) => {
        this.handleClose();
        swal({
          closeOnClickOutside: false,
          title: "Success",
          text: "Comment posted successfully.",
          icon: "success",
        }).then(() => {
          this.setState({ showModalLoader: false });
          this.props.reloadTask();
        });
      })
      .catch((error) => {
        this.setState({ showModalLoader: false });
        if (error.data.status === 3) {
          var token_rm = 2;
          showErrorMessageFront(error, token_rm, this.props);
          this.handleClose();
        } else {
          actions.setErrors(error.data.errors);
          actions.setSubmitting(false);
        }
      });
  };

  setDropZoneFiles = (acceptedFiles, setErrors, setFieldValue) => {
    //console.log(acceptedFiles);
    setErrors({ file_name: false });
    setFieldValue(this.state.files);
    var prevFiles = this.state.files;
    var newFiles;
    if (prevFiles.length > 0) {
      //newFiles = newConcatFiles = acceptedFiles.concat(prevFiles);

      for (let index = 0; index < acceptedFiles.length; index++) {
        var remove = 0;

        for (let index2 = 0; index2 < prevFiles.length; index2++) {
          if (acceptedFiles[index].name === prevFiles[index2].name) {
            remove = 1;
            break;
          }
        }

        //console.log('remove',acceptedFiles[index].name,remove);

        if (remove === 0) {
          prevFiles.push(acceptedFiles[index]);
        }
      }
      //console.log('acceptedFiles',acceptedFiles);
      //console.log('prevFiles',prevFiles);
      newFiles = prevFiles;
    } else {
      newFiles = acceptedFiles;
    }

    var fileListHtml = newFiles.map((file) => (
      <Alert key={file.name}>
        <span onClick={() => removeDropZoneFiles(file.name, this, setErrors)}>
          <i className="far fa-times-circle"></i>
        </span>{" "}
        {trimString(25, file.name)}
      </Alert>
    ));

    this.setState({
      files: newFiles,
      filesHtml: fileListHtml,
    });
  };

  render() {
    const newInitialValues = Object.assign(initialValues, {
      comment: "",
      case_id:""
    });

    const newInitialValuesSO = Object.assign(initialValues, {
      comment: "",
      so_number:this.state.sales_order_number
    });

    const validateStopFlag = Yup.object().shape({
      comment: Yup.string().trim().required("Please enter your comment."),
      case_id: Yup.string().trim().required("Please select case ID."),
    });

    const validateStopFlagSO = Yup.object().shape({
      comment: Yup.string().trim().required("Please enter your comment."),
      so_number: Yup.number().required(`Please enter SO/STO Number.`)
    });


    return (
      <>
        <Modal
          show={this.state.requestCaseChanges || this.state.requestCaseChangesAfterSO}
          onHide={() => this.handleClose()}
          backdrop="static"
        >
          <Formik
            initialValues={this.state.requestCaseChanges?newInitialValues:newInitialValuesSO}
            validationSchema={this.state.requestCaseChanges?validateStopFlag:validateStopFlagSO}
            onSubmit={this.handleRequestToReopen}
          >
            {({
              values,
              errors,
              touched,
              isValid,
              isSubmitting,
              handleChange,
              setFieldValue,
              setFieldTouched,
              setErrors,
            }) => {
              /* console.log('values', values)
                      console.log('errors', errors)
                      console.log('touched', touched)
                      console.log('isValid', isValid) */
              return (
                <Form encType="multipart/form-data">
                  {this.state.showModalLoader === true ? (
                    <div className="loderOuter">
                      <div className="loader">
                        <img src={loaderlogo} alt="logo" />
                        <div className="loading">Loading...</div>
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                  <Modal.Header closeButton>
                    <Modal.Title>
                     {this.state.requestCaseChanges && `Request Case Changes`} {this.state.requestCaseChangesAfterSO && `Request Case Changes After ${this.state.currRow.genpact_type == 1?"SO":"STO"} Generation`} | {this.state.currRow.task_ref}
                    </Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <div className="contBox">
                      {this.state.requestCaseChanges && <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <label>
                              Case ID<span className="required-field">*</span>
                            </label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              value={values.case_id}
                              defaultValue={values.case_id}
                              isSearchable={true}
                              name="case_id"
                              options={this.state.caseData}
                              onChange={(e) => {
                                console.log(e);
                                setFieldValue("case_id",e)
                              }}
                            />

                            {errors.case_id && touched.case_id ? (
                              <span style={{ color: "#ffae42" }}>
                                {errors.case_id}
                              </span>
                            ) : null}

                          </div>
                        </Col>
                      </Row>}
                      {this.state.requestCaseChangesAfterSO && 

                        <Row>
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>
                              {this.state.currRow.genpact_type == 1?"SO":"STO"} Number <span className="required-field">*</span>
                              </label>
                              <Field
                                name="so_number"
                                type="text"
                                className={`selectArowGray form-control`}
                                autoComplete="off"
                                value={values.so_number}
                              ></Field>
                              {errors.so_number && touched.so_number ? (
                                <span className="errorMsg">{errors.so_number}</span>
                              ) : null}
                            </div>
                          </Col>
                        </Row>

                      }
                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <label>
                              Comment{" "}
                              <span className="required-field">*</span>
                            </label>

                            <Editor
                                name="content"
                                className={`selectArowGray form-control`}
                                value={
                                  values.comment !== null &&
                                  values.comment !== ""
                                    ? values.comment
                                    : ""
                                }
                                content={
                                  values.comment !== null &&
                                  values.comment !== ""
                                    ? values.comment
                                    : ""
                                }
                                init={{
                                  menubar: false,
                                  branding: false,
                                  placeholder: "Enter comments",
                                  plugins:
                                    "link table hr visualblocks code placeholder lists autoresize textcolor",
                                  toolbar:
                                    "bold italic strikethrough superscript subscript | forecolor backcolor | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | visualblocks code | fontselect",
                                  font_formats:
                                    "Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats",
                                  color_map: [
                                    "000000",
                                    "Black",
                                    "808080",
                                    "Gray",
                                    "FFFFFF",
                                    "White",
                                    "FF0000",
                                    "Red",
                                    "FFFF00",
                                    "Yellow",
                                    "008000",
                                    "Green",
                                    "0000FF",
                                    "Blue",
                                  ],
                                }}
                                onEditorChange={(value) =>
                                  setFieldValue("comment", value)
                                }
                              />


                            {/* <Field
                              name="comment"
                              component="textarea"
                              className="form-control"
                              autoComplete="off"
                              placeholder="Comment here"
                              onChange={handleChange}
                              style={{ height: "130px" }}
                            /> */}

                            {errors.comment && touched.comment ? (
                              <span className="errorMsg">
                                {errors.comment}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>

                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group custom-file-upload">
                            
                            <Dropzone
                              onDrop={(acceptedFiles) =>
                                this.setDropZoneFiles(
                                  acceptedFiles,
                                  setErrors,
                                  setFieldValue
                                )
                              }
                            >
                              {({ getRootProps, getInputProps }) => (
                                <section>
                                  <div
                                    {...getRootProps()}
                                    className="custom-file-upload-header"
                                  >
                                    <input {...getInputProps()} />
                                    <p>Upload file</p>
                                  </div>
                                  <div className="custom-file-upload-area">
                                    {this.state.filesHtml}
                                  </div>
                                </section>
                              )}
                            </Dropzone>
                            {errors.file_name || touched.file_name ? (
                              <span className="errorMsg errorExpandView">
                                {errors.file_name}
                              </span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>

                    </div>
                  </Modal.Body>
                  <Modal.Footer>
                    <button
                      onClick={this.handleClose}
                      className={`btn-line`}
                      type="button"
                    >
                      Cancel
                    </button>
                    <button
                      className={`btn-fill ${
                        isValid ? "btn-custom-green" : "btn-disable"
                      } m-r-10`}
                      type="submit"
                      disabled={isValid ? false : true}
                    >
                      {this.state.stopflagId > 0
                        ? isSubmitting
                          ? "Updating..."
                          : "Update"
                        : isSubmitting
                        ? "Submitting..."
                        : "Submit"}
                    </button>
                  </Modal.Footer>
                </Form>
              );
            }}
          </Formik>
        </Modal>
      </>
    );
  }
}

export default RequestCaseChanges;
