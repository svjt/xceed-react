import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

class CustomerDashboard extends Component {

    render(){

        if(this.props.match.params.id > 0) {

            //console.log(this.props.match.params.id);

            const dashboard_url = 'http://drreddy.indusnet.cloud/customer-dashboard/' + this.props.match.params.id;

            //const dashboard_url = 'http://www.youtube.com/embed/xDMP3i36naA';

            return(
                <>
                    <div className="content-wrapper">
                        <div className="iframe-container">
                            <iframe 
                                title="customer-dashboard"
                                src={dashboard_url}
                                allowFullScreen
                            ></iframe>
                        </div>
                    </div>
                </>    
            );
        } else {
            
            return (
                <Redirect to='/user/dashboard'/>
            );
        }        
    }
}

export default CustomerDashboard;