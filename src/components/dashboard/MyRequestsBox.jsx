import React, { Component } from 'react';

class MyRequestsBox extends Component {
    render(){
        return(
            <>
            <div className="col-lg-3 col-sm-6 col-xs-12">
                <div className="small-box bg-OffBlue">
                    <div className="inner">
                    <div className="row clearfix">
                        <div className="col-xs-8">
                            <h3>12</h3>
                            <p><strong>My Requests</strong></p>
                        </div>
                        <div className="col-xs-4">
                            <div className="icon"><i className="far fa-comments"></i></div>
                        </div>
                    </div>
                    </div>
                    <div className="small-box-footer clearfix">
                        <div className="row clearfix">
                            <div className="col-xs-4"></div>
                            <div className="col-xs-4 col-border">Open Requests <span>8</span></div>
                            <div className="col-xs-4 col-border">New Requests <span>14</span></div>
                        </div>
                    </div>
                </div>
            </div>
            </>
        );
    }
}

export default MyRequestsBox;