import React, { Component } from 'react';
import { Row, Col, ButtonToolbar, Button, Modal,Panel } from "react-bootstrap";
import dateFormat from 'dateformat';
import API from "../../shared/axios";

import { Redirect } from 'react-router-dom';
import Loader from 'react-loader-spinner';

class DiscussionDetails extends Component {

    constructor( props ) {
        super( props );

        this.state = {
            discussDetails: [],
            task_id : this.props.match.params.did
        }
    }

    componentDidMount = () => { 
        if(this.state.task_id > 0){
            API.get(`api/tasks/discussions/${this.state.task_id}`)
            .then(res => {
                this.setState({
                    discussDetails: res.data.data
                });
                //console.log(res.data.data);
            })
            .catch(err => {
                console.log(err);
            });
        }
    }

    submitComment = () => {

        let tid = this.props.match.params.did;
        
        API
            .get(`api/tasks/discussions/${tid}`)
            .then(res => {
                alert('Axios success, please change the endpoint')
            })
            .catch(err => {
                console.log(err);
            });

    }

    render(){

        //SATYAJIT
        if(this.props.match.params.did === 0 || this.props.match.params.did === ""){
            return <Redirect to='/user/dashboard'/>
        }

        if(this.state.task_id > 0) {

            return(
                <>
                    <div className="content-wrapper">
                        <section className="content-header">
                            <h1>
                                Discussion Details
                            </h1>
                        </section>
                        <section className="content">
                            <div className="boxPapanel content-padding">

                            {this.state.discussDetails.map(
                                (comments, i) => 
                                    <div key={i} className="row">

                                        <img 
                                        alt="noimage"
                                        height="80"
                                        width="60"
                                        src="https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg"/>
                                        
                                        <span>
                                            {comments.posted_by_type === 'C' ? 
                                                comments.cust_first_name+' '+comments.cust_last_name + '(Customer)' :
                                                comments.emp_first_name+' '+comments.emp_last_name + '(Employee)'
                                            }
                                        </span>
                                        <p>{comments.comment}</p>

                                        <p>{dateFormat(new Date(comments.date_added), "dd/mm/yyyy")}</p>        

                                    </div>
                            )}

                            <div className="commentBox">
                                <div className="form-group">
                                    <label>Enter your comment</label>
                                    <textarea
                                        name="commentText"
                                    />
                                </div>
                                <div className="row">
                                    <button onClick={this.submitComment}> Submit </button>
                                </div>
                            </div>

                        </div>
                        </section>
                    </div>

                </>    
            );
        } else {
            
            return (
                <>
                    <div className="loading_reddy_outer">
                        <div className="loading_reddy" >
                            <Loader 
                                type="Puff"
                                color="#00BFFF"
                                height="50"	
                                width="50"
                                verticalAlign="middle"
                            />
                        </div>
                    </div>
                </>
            );
        }
        
    }
}

export default DiscussionDetails;