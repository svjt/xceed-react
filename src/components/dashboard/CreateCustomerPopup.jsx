import React, { Component } from "react";
import Autosuggest from "react-autosuggest";

import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Modal,
  Alert,
  Tooltip,
  OverlayTrigger,
} from "react-bootstrap";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";
import API from "../../shared/axios";
import { Redirect, Link } from "react-router-dom";
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";

import { showErrorMessageFront } from "../../shared/handle_error_front";

import { displaySoldTo } from "../../shared/helper";
import swal from "sweetalert";

import {
  localDate,
  trimString,
  inArray,
  htmlDecode,
} from "../../shared/helper";
import CustomerSoldToPopup from './CustomerSoldToPopup';
import Dropzone from "react-dropzone";
import exclamationImage from "../../assets/images/exclamation-icon-black.svg";
import { Editor } from "@tinymce/tinymce-react";
import ReactHtmlParser from "react-html-parser";
//import { url } from 'inspector';

//import jp from 'jsonpath';

const initialValues = {
  customer_type: { value: "BOTH", label: "BOTH" }
};

/*For Tooltip*/
function LinkWithTooltipText({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

class CreateCustomerPopup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showCreateCustomer: false,
      createCustomerLoader: false,
      isAdmin: false,
      countryList: [],
      country_id: '',
      ot_id: '',
      companyList: [],
      company_id: '',
      languageList: [],
      language_id: '',
      vipList: [
        { value: "0", label: "No" },
        { value: "1", label: "Yes" },
      ],
      vip_customer: '',
      statusList: [
        { value: "0", label: "Inactive" },
        { value: "1", label: "Active" },
        { value: "3", label: "Dummy" },
      ],
      status: '',
      customerTypeList: [
        // { value: "API", label: "API" },
        // { value: "CPS", label: "CPS" },
        { value: "BOTH", label: "BOTH" },
      ],
      customer_type: '',
      fa_access: '',
      is_admin: '',
      teamList: [],
      selTeam: [],
      productList: [],
      selProducts: [],
      dnd: '',
      dndList: [
        { value: "2", label: "No" },
        { value: "1", label: "Yes" },
      ],
      ignore_spoc: '',
      multi_lang_access: '',
      roleList: [],
      selRole: [],
      roleTypeList: [
        { value: "1", label: "Admin" },
        { value: "2", label: "Regular" },
      ],
      role_type: '',
      value: "", // for auto-suggest
      suggestions: [], // for auto-suggest
      company_has_soldTo: '',
      showCustomerSoldToPopup: false,
    };
  }

  componentDidMount() {
    if (this.props.showCreateCustomer === true) {

      if (this.props.ot_id && this.props.ot_id != '') {
        this.setState({ ot_id: this.props.ot_id });
        initialValues.email = this.props.cust_company_details[0].email
      }

      this.setState({ showCreateCustomer: this.props.showCreateCustomer });

      API.get("/api/employees/company")
        .then((res) => {
          var company = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            company.push({
              value: element["company_id"],
              label: htmlDecode(element["company_name"]),
            });
          }
          this.setState({ companyList: company });
        })
        .catch((err) => {
          showErrorMessageFront(err, this.props);
        });

      API.get("/api/feed/country")
        .then((res) => {
          var country = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            country.push({
              value: element["country_id"],
              label: htmlDecode(element["country_name"]),
            });
          }
          this.setState({ countryList: country });
        })
        .catch((err) => {
          showErrorMessageFront(err, this.props);
        });

      API.get("/api/feed/language")
        .then((res) => {
          var language = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            language.push({
              value: element["id"],
              label: htmlDecode(element["language"]),
              code: element["code"]
            });
          }
          this.setState({ languageList: language });
        })
        .catch((err) => {
          showErrorMessageFront(err, this.props);
        });

      API.get(`/api/feed/products`)
        .then((res) => {
          var products = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            products.push({
              value: element["product_id"],
              label: htmlDecode(element["product_name"]),
            });
          }
          this.setState({ productList: products });
        })
        .catch((err) => {
          console.log(err);
        });

      API.get("/api/employees/all_emp")
        .then((res) => {
          var myTeam = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];

            if (element["desig_name"] == 'SPOC') {
              let prefix = '';
              if (element["lang_code"] === 'zh') {
                prefix = 'M-'
              } else if (element["lang_code"] === 'es') {
                prefix = 'S-'
              } else if (element["lang_code"] === 'pt') {
                prefix = 'P-'
              } else if (element["lang_code"] === 'ja') {
                prefix = 'J-'
              }
              myTeam.push({
                value: element["employee_id"],
                label:
                  element["first_name"] +
                  " " +
                  element["last_name"] +
                  " (" +
                  prefix +
                  element["desig_name"] +
                  ")",
              });
            } else {
              myTeam.push({
                value: element["employee_id"],
                label:
                  element["first_name"] +
                  " " +
                  element["last_name"] +
                  " (" +
                  element["desig_name"] +
                  ")",
              });
            }

          }
          this.setState({
            teamList: myTeam,
          });
        })
        .catch((err) => {
          showErrorMessageFront(err, this.props);
        });

      API.get("/api/customers/role_all")
        .then((res) => {
          var myRole = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            myRole.push({
              value: element["role_id"],
              label: element["role_name"],
            });
          }
          this.setState({
            roleList: myRole,
          });
        })
        .catch((err) => {
          showErrorMessageFront(err, this.props);
        });
    }
  }

  getCountryTeam = (country_id) => {
    API.get(`/api/employees/country_team/${country_id}`)
      .then((res) => {
        if (res.data && res.data.data && res.data.data.length > 0) {
          const { emp_bm, emp_cscc, emp_csct, emp_ra } = res.data.data[0];
          const team_ids = [emp_bm, emp_cscc, emp_csct, emp_ra];
          const { teamList } = this.state;
          if (teamList && teamList.length > 0) {
            const country_team = [];
            team_ids.forEach(id => {
              const team = teamList.find((team) => id && team.value == id);
              if (team && Object.keys(team).length > 0) {
                country_team.push(team);
              }
            });
            this.setState({ selTeam: country_team });
          }
        }
      })
      .catch((err) => {
        showErrorMessageFront(err, this.props);
      });
  }

  handleClose = () => {
    var close = {
      showCreateCustomer: false,
      outlookShowCustomerAdd: false,
      createCustomerLoader: false,
      filesHtml: "",
      value: '',
    };

    this.setState({
      isAdmin: false,
      country_id: '',
      value: '',
      selTeam: [],
      teamList: [],
      company_id: '',
      selRole: [],
      company_has_soldTo: '',
      suggestions: [],
      role_type: '',
      showCustomerSoldToPopup: false,
      showCreateCustomer: false,
      createCustomerLoader: false,
    });

    initialValues.email = '';

    this.props.handleClose(close);
  };
  handleSAPUser = (values) => {
    //console.log("this is values : ",values);
    if ((values.company_name == '' || values.company_name == undefined) ||
      (values.country_id == '' || values.country_id == undefined) ||
      (values.email == '' || values.email == undefined) ||
      (values.phone_no == '' || values.phone_no == undefined)) {

      swal({
        closeOnClickOutside: false,
        title: "Error!",
        text: "Please enter proper data.",
        icon: "error",
      }).then(() => {
        // callback
      });

    } else {
      this.setState({ createCustomerLoader: true })
      const post_data = {
        company_name : values.company_name,
        country_id : values.country_id.value,
        email : values.email,
        phone_no : values.phone_no
      }
      API.post("/api/feed/add_sap_user", post_data)
      .then((res) => {
        let res_data = res.data ;
        if(res_data.status){
          this.setState({ createCustomerLoader: false })
          window.open("https://bpconnect-accp.mendixcloud.com/");
        }else{
          this.setState({ createCustomerLoader: false })
          swal({
            closeOnClickOutside: false,
            title: "Error!",
            text: "Error in addition.",
            icon: "error",
          }).then(() => {
            // callback
          });
        }
        
         
        
      })
      .catch((err) => {
        this.setState({ createCustomerLoader: false });
        if (err.data.status === 3) {
          this.setState({
            showCreateCustomer: false,
          });
          showErrorMessageFront(err, this.props);
        } else {
         
        }
      });
    }
  }

  handleSubmitCustomer = (values, actions) => {
    // console.log("values", values);

    // var err_cnt = 0;

    // if ( values.employee_id === undefined || values.employee_id === "") {
    //   err_cnt++;
    //   actions.setErrors({ employee_id: "Please select team." });
    //   actions.setSubmitting(false);
    //   this.setState({ createCustomerLoader: false });
    // }
    // if ( values.is_admin.value === undefined || values.is_admin.value === "No") {
    //   if (values.role_type.value === null || values.role_type.value === "") {
    //     err_cnt++;
    //     actions.setErrors({ role_type: "Please select role type." });
    //     actions.setSubmitting(false);
    //     this.setState({ createCustomerLoader: false });
    //   }else if (values.role_id.value === null || values.role_id.value === "") {
    //     err_cnt++;
    //     actions.setErrors({ role_id: "Please select role." });
    //     actions.setSubmitting(false);
    //     this.setState({ createCustomerLoader: false });
    //   }
    // } 

    // if (err_cnt === 0) {
    this.setState({ createCustomerLoader: true });

    let role_type;
    if (values.role_type && values.role_type.value > 0) {
      role_type = values.role_type.value;
    } else {
      role_type = "0";
    }

    let selectedTeamIds = [];
    this.state.selTeam.forEach((team) => {
      selectedTeamIds.push(team.value);
    })

    let selectedRoleIds = [];

    if (values.role_id && values.role_id.length > 0) {
      values.role_id.forEach((role) => {
        selectedRoleIds.push(role.value);
      })
    }

    const post_data = {
      first_name: values.first_name,
      last_name: values.last_name,
      country_id: values.country_id.value,
      language_code: values.language_id.code,
      company_name: this.state.value, //from autosuggest
      status: "0",
      email: values.email,
      vip_customer: values.vip_customer.value,
      fa_access: "0",
      phone_no: values.phone_no,
      teams: selectedTeamIds,
      products: [],
      sap_ref: values.sap_ref,
      customer_type: values.customer_type.value,
      is_admin: values.is_admin.value,
      role: selectedRoleIds,
      role_type: role_type,
      dnd: values.dnd.value,
      ignore_spoc: "2",
      multi_lang_access: values.multi_lang_access.value
    };

    API.post("/api/customers/add_customer_by_employee", post_data)
      .then((res) => {
        if (this.state.ot_id != '') {
          this.props.openOutlook(this.state.ot_id);
        } else {
          this.setState({ createCustomerLoader: false });
          this.handleClose();
          swal({
            closeOnClickOutside: false,
            title: "Success",
            text: "Record added successfully.",
            icon: "success",
          }).then(() => {
            // callback
          });
        }
      })
      .catch((err) => {
      
        this.setState({ createCustomerLoader: false });
        if (err.data.status === 3) {
          this.setState({
            showCreateCustomer: false,
          });
          showErrorMessageFront(err, this.props);
        } else {
          console.log("hello am in !!");
          actions.setErrors(err.data.errors);
          actions.setSubmitting(false);
        }
      });
    // }
  };

  setCountry = (event, setFieldValue) => {
    if (event.length == 0) {
      setFieldValue("country_id", "");
      this.setState({ country_id: "" });
    } else {
      const { value } = event;
      if (value) this.getCountryTeam(value);
      setFieldValue("country_id", event);
      this.setState({ country_id: event });
    }
  };

  // setCustomer = (event, setFieldValue) => {
  //   if (event.length == 0) {
  //     setFieldValue("company_id", "");
  //     this.setState({ company_id: "" });
  //   } else {
  //     setFieldValue("company_id", event);
  //     this.setState({ company_id: event });
  //   }
  // };

  setLanguage = (event, setFieldValue) => {
    if (event.length == 0) {
      setFieldValue("language_id", "");
      this.setState({ language_id: "" });
    } else {
      setFieldValue("language_id", event);
      this.setState({ language_id: event });
    }
  };

  setVip = (event, setFieldValue) => {
    if (event.length == 0) {
      setFieldValue("vip_customer", "");
      this.setState({ vip_customer: "" });
    } else {
      setFieldValue("vip_customer", event);
      this.setState({ vip_customer: event });
    }
  };

  setStatus = (event, setFieldValue) => {
    if (event.length == 0) {
      setFieldValue("status", "");
      this.setState({ status: "" });
    } else {
      setFieldValue("status", event);
      this.setState({ status: event });
    }
  };

  setUserType = (event, setFieldValue) => {
    if (event.length == 0) {
      setFieldValue("customer_type", "");
      this.setState({ customer_type: "" });
    } else {
      setFieldValue("customer_type", event);
      this.setState({ customer_type: event });
    }
  };

  // setFaAccess = (event, setFieldValue) => {
  //   if (event.length == 0) {
  //     setFieldValue("fa_access", "");
  //     this.setState({ fa_access: "" });
  //   } else {
  //     setFieldValue("fa_access", event);
  //     this.setState({ fa_access: event });
  //   }
  // };

  setIsAdmin = (event, setFieldValue) => {
    if (event.length == 0) {
      setFieldValue("is_admin", "");
      this.setState({ is_admin: "" });
    } else {
      setFieldValue("is_admin", event);
      this.setState({ is_admin: event });
    }
    // console.log(event.value);
    if (event.value === "0") {
      this.setState({ isAdmin: true });
    } else {
      this.setState({ isAdmin: false });
      this.setState({ selRole: [], role_type: "" });
    }
  };

  setDND = (event, setFieldValue) => {
    if (event.length == 0) {
      setFieldValue("dnd", "");
      this.setState({ dnd: "" });
    } else {
      setFieldValue("dnd", event);
      this.setState({ dnd: event });
    }
  };

  setIgnoreSpoc = (event, setFieldValue) => {
    if (event.length == 0) {
      setFieldValue("ignore_spoc", "");
      this.setState({ ignore_spoc: "" });
    } else {
      setFieldValue("ignore_spoc", event);
      this.setState({ ignore_spoc: event });
    }
  };

  setMultiLang = (event, setFieldValue) => {
    if (event.length == 0) {
      setFieldValue("multi_lang_access", "");
      this.setState({ multi_lang_access: "" });
    } else {
      setFieldValue("multi_lang_access", event);
      this.setState({ multi_lang_access: event });
    }
  };

  setRoleType = (event, setFieldValue) => {
    if (event.length == 0) {
      setFieldValue("role_type", "");
      this.setState({ role_type: "" });
    } else {
      setFieldValue("role_type", event);
      this.setState({ role_type: event });
    }
  };

  selectMultiProducts = (event, setFieldValue) => {
    if (event && (event.length === 0 || event === null)) {
      setFieldValue("product_id", []);
      this.setState({ selProducts: null });
    } else {
      if (event.length > 5) {
      } else {
        setFieldValue("product_id", event);

        let selProducts = [];
        let prev_state =
          this.state.selProducts != null ? this.state.selProducts : [];
        //ADD NEW PRODUCTS
        for (let index = 0; index < event.length; index++) {
          const element = event[index];
          selProducts.push(element.value);
        }

        //REMOVE EXCESS PRODUCTS
        for (let index = 0; index < prev_state.length; index++) {
          const element = prev_state[index];
          if (!inArray(element.product_id, selProducts)) {
            prev_state.splice(index, 1);
          }
        }
        this.setState({ selProducts: prev_state });
      }
    }
  };

  checkCompanySoldto = (company_name) => {
    API
      .get(`/api/employees/company_has_soldto/${company_name}`)
      .then((res) => {
        if (res.data && Object.keys(res.data).length > 0) {
          const { company_id, has_soldto } = res.data;
          this.setState({ company_has_soldTo: has_soldto, company_id: company_id });
        } else {
          this.setState({ company_has_soldTo: '', company_id: '' });
        }
      })
      .catch((err) => {
        this.setState({ company_has_soldTo: '' });
        console.log("==========", err);
      });
  }

  selectTeam = (event, setFieldValue, setFieldTouched) => {
    if (event && (event.length === 0 || event === null)) {
      setFieldValue("employee_id", []);
      setFieldTouched("employee_id");
      this.setState({ selTeam: null });
    } else {
      if (event.length > 5) {
      } else {
        setFieldValue("employee_id", event);
        setFieldTouched("employee_id");
        this.setState({ selTeam: event });
      }
    }
  };

  selectMultiRole = (event, setFieldValue, setFieldTouched) => {
    if (event && (event.length === 0 || event === null)) {
      setFieldValue("role_id", []);
      setFieldTouched("role_id");
      this.setState({ selRole: null });
    } else {
      if (event.length > 5) {
      } else {
        setFieldValue("role_id", event);
        setFieldTouched("role_id");

        let selRoles = [];
        let prev_state =
          this.state.selRole != null ? this.state.selRole : [];
        //ADD NEW TEAM MEMBER
        for (let index = 0; index < event.length; index++) {
          const element = event[index];
          selRoles.push(element.value);
        }

        //REMOVE EXCESS TEAM MEMBER
        for (let index = 0; index < prev_state.length; index++) {
          const element = prev_state[index];
          if (!inArray(element.product_id, selRoles)) {
            prev_state.splice(index, 1);
          }
        }
        this.setState({ selRole: prev_state });
      }
    }
  };

  enterPressed = (event) => {
    var code = event.keyCode || event.which;
    if (code === 13) {
      //13 is the enter keycode

      this.setState({
        searchkey: event.target.value,
      });
      //this.getRequests(1, event.target.value);
    }
  };

  onReferenceChange = (event, { newValue }, setFieldValue) => {
    this.setState({ value: newValue }, () =>
      setFieldValue("company_name", newValue)
    );
  };

  onSuggestionsFetchRequested = ({ value, reason }) => {
    const inputValue = value.toLowerCase();
    const inputLength = inputValue.length;

    if (inputLength === 0) {
      this.setState({ company_has_soldTo: '', company_id: '' })
      return [];
    } else {
      if (inputLength > 0) {
        this.checkCompanySoldto(value);
        API
          .get(`/api/employees/company-search/${value}`)
          .then((res) => {
            this.setState({
              suggestions: res.data.data,
            });
            return this.state.suggestions;
          })
          .catch((err) => {
            console.log("==========", err);
          });
      }
    }
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
      // company_has_soldTo: '',
    });
  };

  setSuggesstionValue = (suggestion, setFieldValue) => {
    setFieldValue("company_name", suggestion.company_name);
    this.checkCompanySoldto(suggestion.company_name);
    // console.log({suggestion});
    return suggestion.company_name;
  };

  renderSuggestion = (suggestion) => {
    return (
      <div>
        <Link to="#">{suggestion.company_name}</Link>
      </div>
    );
  };

  handleShowCustomerSoldToPopup = () => {
    this.setState({ showCustomerSoldToPopup: true });
  }

  handleCloseCustomerSoldToPopup = () => {
    const company_name = this.state.value;
    this.checkCompanySoldto(company_name);
    this.setState({ showCustomerSoldToPopup: false });
  }


  render() {
    const { suggestions, value } = this.state;

    const newInitialValues = Object.assign(initialValues);

    const validateAddFlag = Yup.object().shape({
      first_name: Yup.string().trim().required("Please enter first name"),
      last_name: Yup.string().trim().required("Please enter last name"),
      company_id: Yup.string().trim().test("customer", "Please select customer", () => this.state.value !== ''),
      employee_id: Yup.array()
        .of(
          Yup.object().shape({
            label: Yup.string(),
            value: Yup.string()
          })
        )
        .test("teamSelected", "Please select team", () => this.state.selTeam && this.state.selTeam.length > 0),
      country_id: Yup.string().trim().required("Please select country"),
      email: Yup.string().trim().required("Please enter email").email("Invalid email").max(80, "Email can be maximum 80 characters long"),
      phone_no: Yup.string().trim().required("Please enter phone number").max(30, "Phone number cannot be more than 30 characters long"),
      //.matches(/^[0-9]$/, "Invalid phone number")
      language_id: Yup.string().trim().required("Please select language"),
      vip_customer: Yup.string().trim().required("Please select key account"),
      // status: Yup.string().trim().required("Please select status"),
      customer_type: Yup.object().required("Please select user type"),
      // fa_access: Yup.string().trim().required("Please select formulation assistant access"),
      is_admin: Yup.object().required("Please select admin status"),
      role_type: Yup.string().when('is_admin', {
        is: (is_admin) => is_admin === undefined || is_admin.value == "0",
        then: Yup.string().required('Please select role type'),
      }),
      role_id: Yup.string().when('is_admin', {
        is: (is_admin) => is_admin === undefined || is_admin.value == "0",
        then: Yup.string().required('Please select role'),
      }),
    });

    return (
      <>
        <Modal
          show={this.state.showCreateCustomer}
          className={`create-cust-popup`}
          onHide={() => this.handleClose()}
          backdrop="static"
        >
          <Formik
            initialValues={newInitialValues}
            validationSchema={validateAddFlag}
            onSubmit={this.handleSubmitCustomer}
          >
            {({
              values,
              errors,
              touched,
              isValid,
              isSubmitting,
              setFieldValue,
              setFieldTouched,
              setErrors,
            }) => {
              return (
                <Form>
                  {/* {console.log({errors})} */}
                  {/* {console.log("values",values)} */}
                  {/* {console.log("sel team",this.state.selTeam)} */}
                  {this.state.createCustomerLoader && (
                    <div className="loderOuter">
                      <div className="loderOuter">
                        <div className="loader">
                          <img src={loaderlogo} alt="logo" />
                          <div className="loading">Loading...</div>
                        </div>
                      </div>
                    </div>
                  )}
                  <Modal.Header closeButton>
                    <Modal.Title>Create User</Modal.Title>
                    {this.state.ot_id != '' && <p className={`cust-outlook-message`}>User doesn't exist in XCEED, continue to create a new user.</p>}
                  </Modal.Header>
                  <Modal.Body>

                    <div className="contBox taskcontBox">
                      {this.state.ot_id != '' && <button
                        onClick={() => {
                          this.setState({ createCustomerLoader: true });
                          this.props.openOutlookExplicit(this.state.ot_id)
                        }}
                        className={`btn link-btn text-right`}
                        type="button"
                      >
                        Skip and Continue
                      </button>}
                      <Row>
                        <Col xs={12} sm={6} md={6}>
                          <div className="form-group">
                            <label>
                              First Name{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Field
                              name="first_name"
                              type="text"
                              className="form-control"
                              autoComplete="off"
                            ></Field>
                            {errors.first_name && touched.first_name ? (
                              <span className="errorMsg">{errors.first_name}</span>
                            ) : null}
                          </div>
                        </Col>
                        <Col xs={12} sm={6} md={6}>
                          <div className="form-group">
                            <label>
                              Last Name{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Field
                              name="last_name"
                              type="text"
                              className="form-control"
                              autoComplete="off"
                            ></Field>
                            {errors.last_name && touched.last_name ? (
                              <span className="errorMsg">{errors.last_name}</span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs={12} sm={6} md={6}>
                          <div className="form-group">
                            <label>
                              Customer{" "}
                              <span className="required-field">*</span>
                            </label>
                            {/* <Select
                              className="basic-single"
                              classNamePrefix="select"
                              name="company_id"
                              options={this.state.companyList}
                              onChange={(e) => {
                                this.setCustomer(e, setFieldValue);
                              }}
                              onBlur={() => setFieldTouched("company_id")}
                            /> */}
                            <Autosuggest
                              suggestions={suggestions}
                              onSuggestionsFetchRequested={
                                this.onSuggestionsFetchRequested
                              }
                              onSuggestionsClearRequested={
                                this.onSuggestionsClearRequested
                              }
                              getSuggestionValue={(suggestion) =>
                                this.setSuggesstionValue(
                                  suggestion,
                                  setFieldValue
                                )
                              }
                              renderSuggestion={this.renderSuggestion}
                              inputProps={{
                                placeholder: "",
                                value,
                                type: "search",
                                onChange: (event, object) => {
                                  return this.onReferenceChange(
                                    event,
                                    object,
                                    setFieldValue
                                  );
                                },
                                // onBlur: this.checkCompanySoldto(values.company_name),
                                onKeyPress: this.enterPressed.bind(this),
                                className: "form-control customInput",
                              }}
                              name="company_name"
                            />

                            {
                              this.state.value && this.state.company_has_soldTo !== '' && displaySoldTo() ? (
                                this.state.company_has_soldTo ? (
                                  <span className="task-details-language"
                                    style={{
                                      color: "rgb(0, 86, 179)",
                                      fontWeight: "700",
                                    }} >To map new Sold To Party, click <button type="button" onClick={this.handleShowCustomerSoldToPopup}>here</button> if you choose to.</span>
                                ) : (
                                  <span className="task-details-language"
                                    style={{
                                      color: "rgb(0, 86, 179)",
                                      fontWeight: "700",
                                    }} >No Sold To Party mapped with above customer. Click <button type="button" onClick={this.handleShowCustomerSoldToPopup}>here</button> to map one if you choose to.</span>
                                )
                              ) : (null)
                            }

                            {errors.company_id ? (
                              <span className="errorMsg">{errors.company_id}</span>
                            ) : null}
                          </div>
                        </Col>
                        <Col xs={12} sm={6} md={6}>
                          <div className="form-group">
                            <label>
                              User Email{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Field
                              name="email"
                              type="text"
                              className="form-control"
                              autoComplete="off"
                            ></Field>
                            {errors.email && touched.email ? (
                              <span className="errorMsg">{errors.email}</span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs={12} sm={6} md={6}>
                          <div className="form-group">
                            <label>
                              Phone No{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Field
                              name="phone_no"
                              type="text"
                              className="form-control"
                              autoComplete="off"
                            ></Field>
                            {errors.phone_no && touched.phone_no ? (
                              <span className="errorMsg">{errors.phone_no}</span>
                            ) : null}
                          </div>
                        </Col>
                        <Col xs={12} sm={6} md={6}>
                          <div className="form-group">
                            <label>
                              Country{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              name="country_id"
                              options={this.state.countryList}
                              onChange={(e) => {
                                this.setCountry(e, setFieldValue);
                              }}
                              onBlur={() => setFieldTouched("country_id")}
                            />
                            {errors.country_id && touched.country_id ? (
                              <span className="errorMsg">{errors.country_id}</span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs={12} sm={6} md={6}>
                          <div className="form-group">
                            <label>
                              Language{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              name="language_id"
                              options={this.state.languageList}
                              onChange={(e) => {
                                this.setLanguage(e, setFieldValue);
                              }}
                              onBlur={() => setFieldTouched("language_id")}
                            />
                            {errors.language_id && touched.language_id ? (
                              <span className="errorMsg">{errors.language_id}</span>
                            ) : null}
                          </div>
                        </Col>
                        <Col xs={12} sm={6} md={6}>
                          <div className="form-group">
                            <label>
                              Key Account{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              name="vip_customer"
                              options={this.state.vipList}
                              onChange={(e) => {
                                this.setVip(e, setFieldValue);
                              }}
                              onBlur={() => setFieldTouched("vip_customer")}
                            />
                            {errors.vip_customer && touched.vip_customer ? (
                              <span className="errorMsg">{errors.vip_customer}</span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        {/* <Col xs={12} sm={6} md={6}>
                          <div className="form-group">
                            <label>
                              Status{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              name="status"
                              options={this.state.statusList}
                              onChange={(e) => {
                                this.setStatus(e, setFieldValue);
                              }}
                              onBlur={() => setFieldTouched("status")}
                            />
                            {errors.status && touched.status ? (
                              <span className="errorMsg">{errors.status}</span>
                            ) : null}
                          </div>
                        </Col> */}
                        <Col xs={12} sm={6} md={6}>
                          <div className="form-group">
                            <label>
                              User Type{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              name="customer_type"
                              options={this.state.customerTypeList}
                              // onChange={(e) => {
                              //   this.setUserType(e, setFieldValue);
                              // }}
                              value={values.customer_type}
                              onBlur={() => setFieldTouched("customer_type")}
                              isDisabled
                            />
                            {errors.customer_type ? (
                              <span className="errorMsg">{errors.customer_type}</span>
                            ) : null}
                          </div>
                        </Col>

                        <Col xs={12} sm={6} md={6}>
                          <div className="form-group">
                            <label>
                              Is Admin{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              name="is_admin"
                              options={this.state.vipList}
                              onChange={(e) => {
                                this.setIsAdmin(e, setFieldValue);
                              }}
                              onBlur={() => setFieldTouched("is_admin")}
                            />
                            {errors.is_admin && touched.is_admin ? (
                              <span className="errorMsg">{errors.is_admin}</span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <label>
                              Assign Team{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Select
                              isMulti
                              className="basic-single"
                              classNamePrefix="select"
                              isSearchable={true}
                              isClearable={true}
                              name="employee_id"
                              value={this.state.selTeam}
                              // value={values.employee_id}
                              options={this.state.teamList}
                              onChange={(e) => {
                                this.selectTeam(e, setFieldValue, setFieldTouched);
                              }}
                            />
                            {errors.employee_id && touched.employee_id ? (
                              <span className="errorMsg">{errors.employee_id}</span>
                            ) : null}
                          </div>
                        </Col>
                      </Row>
                      {/* <Row>
                        <Col xs={12} sm={12} md={12}>
                          <div className="form-group">
                            <label>
                              Product
                            </label>
                            <Select
                              isMulti
                              className="basic-single"
                              classNamePrefix="select"
                              options={this.state.productList}
                              isSearchable={true}
                              isClearable={true}
                              name="product_id"
                              value={values.product_id}
                              onChange={(e) => {
                                this.selectMultiProducts(e, setFieldValue);
                              }}
                            />
                            {errors.product_id && touched.product_id ? (
                              <span className="errorMsg">{errors.product_id}</span>
                            ) : null}
                          </div>
                        </Col>
                      </Row> */}
                      {/* <Row>
                        <Col xs={12} sm={6} md={6}>
                          <div className="form-group">
                            <label>
                              Formulation Assistant Access{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              name="fa_access"
                              options={this.state.vipList}
                              onChange={(e) => {
                                this.setFaAccess(e, setFieldValue);
                              }}
                              onBlur={() => setFieldTouched("fa_access")}
                            />
                            {errors.fa_access && touched.fa_access ? (
                              <span className="errorMsg">{errors.fa_access}</span>
                            ) : null}
                          </div>
                        </Col>
                      </Row> */}
                      <Row>
                        <Col xs={12} sm={6} md={6}>
                          <div className="form-group">
                            <label>
                              Disable Notifications{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              name="dnd"
                              options={this.state.dndList}
                              onChange={(e) => {
                                this.setDND(e, setFieldValue);
                              }}
                              onBlur={() => setFieldTouched("dnd")}
                            />
                            {errors.dnd && touched.dnd ? (
                              <span className="errorMsg">{errors.dnd}</span>
                            ) : null}
                          </div>
                        </Col>
                        <Col xs={12} sm={6} md={6}>
                          <div className="form-group">
                            <label>
                              Multi Language Access{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              name="multi_lang_access"
                              options={this.state.dndList}
                              onChange={(e) => {
                                this.setMultiLang(e, setFieldValue);
                              }}
                              onBlur={() => setFieldTouched("multi_lang_access")}
                            />
                            {errors.multi_lang_access && touched.multi_lang_access ? (
                              <span className="errorMsg">{errors.multi_lang_access}</span>
                            ) : null}
                          </div>
                        </Col>
                        {/* <Col xs={12} sm={6} md={6}>
                          <div className="form-group">
                            <label>
                              Exclude SPOC{" "}
                              <span className="required-field">*</span>
                            </label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              name="ignore_spoc"
                              options={this.state.dndList}
                              onChange={(e) => {
                                this.setIgnoreSpoc(e, setFieldValue);
                              }}
                              onBlur={() => setFieldTouched("ignore_spoc")}
                            />
                            {errors.ignore_spoc && touched.ignore_spoc ? (
                              <span className="errorMsg">{errors.ignore_spoc}</span>
                            ) : null}
                          </div>
                        </Col> */}
                      </Row>
                      {/* <Row>
                        <Col xs={12} sm={6} md={6}>
                          <button
                            onClick={() => { this.handleSAPUser(values) }}

                            type="button"
                          >
                            Add SAP user
                          </button>
                        </Col>
                      </Row> */}
                      <Row>
                        <Col xs={12} sm={6} md={6}>
                          {this.state.isAdmin == true &&
                            <div className="form-group">
                              <label>
                                Role Type{" "}
                                <span className="required-field">*</span>
                              </label>
                              <Select
                                className="basic-single"
                                classNamePrefix="select"
                                name="role_type"
                                options={this.state.roleTypeList}
                                onChange={(e) => {
                                  this.setRoleType(e, setFieldValue);
                                }}
                                onBlur={() => setFieldTouched("role_type")}
                              />
                              {errors.role_type && touched.role_type ? (
                                <span className="errorMsg">{errors.role_type}</span>
                              ) : null}
                            </div>
                          }
                        </Col>
                      </Row>
                      {this.state.isAdmin == true &&
                        <Row>
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>
                                role{" "}
                                <span className="required-field">*</span>
                              </label>
                              <Select
                                isMulti
                                className="basic-single"
                                classNamePrefix="select"
                                options={this.state.roleList}
                                isSearchable={true}
                                isClearable={true}
                                name="role_id"
                                value={values.role_id}
                                onChange={(e) => {
                                  this.selectMultiRole(e, setFieldValue, setFieldTouched);
                                }}
                              />
                              {errors.role_id && touched.role_id ? (
                                <span className="errorMsg">{errors.role_id}</span>
                              ) : null}
                            </div>
                          </Col>
                        </Row>
                      }
                    </div>
                  </Modal.Body>
                  <Modal.Footer>
                    <button
                      onClick={this.handleClose}
                      className={`btn-line`}
                      type="button"
                    >
                      Close
                    </button>
                    <button
                      className={`btn-fill ${isValid ? "btn-custom-green" : "btn-disable"
                        } m-r-10`}
                      type="submit"
                      disabled={isValid ? false : true}
                    >
                      {this.state.stopflagId > 0
                        ? isSubmitting
                          ? "Updating..."
                          : "Update"
                        : isSubmitting
                          ? "Submitting..."
                          : "Submit"}
                    </button>
                  </Modal.Footer>
                </Form>
              );
            }}
          </Formik>
        </Modal>
        {
          this.state.showCustomerSoldToPopup && (
            <CustomerSoldToPopup
              showCustomerSoldToPopup={this.state.showCustomerSoldToPopup}
              handleCloseCustomerSoldToPopup={this.handleCloseCustomerSoldToPopup}
              company_id={this.state.company_id}
              company_name={this.state.value}
            />
          )
        }
      </>
    );
  }
}

export default CreateCustomerPopup;
