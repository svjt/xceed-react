import React, { Component } from "react";
import {
  Row,
  Col,
  ButtonToolbar,
  Button,
  Modal,
  Tooltip,
  OverlayTrigger,
} from "react-bootstrap";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { Link } from "react-router-dom";
import { Formik, Field, Form, FieldArray, isEmptyChildren } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import API from "../../shared/axios";
import Select from "react-select";
//import whitelogo from '../../assets/images/drreddylogo_white.png';
import loaderlogo from "../../assets/images/Xceed_Logo-animated.gif";
import editImage from "../../assets/images/edit-icon.svg";
import { showErrorMessageFront } from "../../shared/handle_error_front";
import DatePicker from "react-datepicker";
import { isBoolean } from "util";
import dateFormat from "dateformat";
import ReactHtmlParser from "react-html-parser";

import { htmlDecode } from "../../shared/helper";

/*For Tooltip  */
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
      placement="top"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}
/*For Tooltip*/

//=========
const leaveInitialValues = {
  owner: "",
  fromDate: "",
  toDate: "",
  reason: "",
};

const setNumberOfLeaves = (refOBj) => (cell, row) => {
  console.log(cell);
  let ret_str = '';

  if(cell && cell.days && cell.months){
    ret_str = `${cell.days} Day${cell.days > 1?"s":""} ${cell.months} Month${cell.months > 1?"s":""}`;
  }else if(cell && cell.days){
    ret_str = `${cell.days} Day${cell.days > 1?"s":""}`;
  }else{
    ret_str = `1 Day`;
  }

  return ret_str;
  //return cell && cell.days ? cell.days : 1;
};

const setLeaveStatus = (refObj) => (cell, row) => {
  let leaveText = "";
  if (row.date_from_original > row.today_date && cell === 1) {
    leaveText = "Yet To Start";
  } else {
    if (cell === 1) {
      leaveText = "On Leave";
    } else if (cell === 2) {
      leaveText = "Completed";
    } else if (cell === 3) {
      leaveText = "Resumed";
    }
  }
  return leaveText;
};

const validateLeave = Yup.object().shape({
  owner: Yup.string().required("Please select an employee"),
  fromDate: Yup.string().required("Please select from date"),
  toDate: Yup.string().required("Please select to date"),
  checkAll: Yup.boolean(),
  assignAllCustomerTo: Yup.string().when("checkAll", {
    is: true,
    then: Yup.string().required("Please assign an employee"),
  }),

  // .object().shape({
  //     value : Yup.string().required(),
  //     label: Yup.string().required()
  // })
  // .when("checkAll", {
  //     is: true,
  //     then: Yup.object().required("Please assign an employee")
  // })

  // checkAll: Yup.boolean().test('checkAllValidate', function(obj){
  //     return obj.checkAll
  // })
});

const actionFormatter = (refObj) => (cell, row) => {
  if (row.status === 1) {
    return (
      <div className="actionStyle">
        <LinkWithTooltip
          tooltip="Click to Edit"
          href="#"
          clicked={(e) => refObj.updateLeave(e, cell)}
          id="tooltip-1"
        >
          <img src={editImage} alt="Edit" />
        </LinkWithTooltip>
      </div>
    );
  } else {
    return "-";
  }
};

class Leave extends Component {
  owner_emp = null;
  to_date_emp = null;
  from_date_emp = null;
  constructor(props) {
    super(props);

    this.state = {
      showLoader: false,
      showModalForm: false,
      employeeArr: [],
      customer_list: [],
      selectChkBoxAll: false,
      selectedOwner: "",
      show_all: false,
      headingText: "Create Leave",
      update_id: 0,
      default_status_value: {},
      //assignOtherCustomerTo : '',
      assignAllCustomerTo: "",
      status_arr: [
        { label: "On Leave", value: 1 },
        { label: "Resumed", value: 3 },
      ],
    };
  }

  componentDidMount = () => {
    // API.get(`/api/employees/all_employees`)
    // .then(res => {
    //     var myTeam = [];
    //     for (let index = 0; index < res.data.data.length; index++) {
    //         const element = res.data.data[index];
    //         var leave = '';
    //         if(element.on_leave == 1){
    //             leave = '[On Leave]';
    //         }
    //         myTeam.push({
    //             value: element["employee_id"],
    //             label: element["first_name"] +" "+ element["last_name"] +" ("+ element["desig_name"] +") " + leave
    //         });
    //     }
    //     this.setState({employeeArr:myTeam});
    //     this.getLeaveData();
    // })
    // .catch(err => {
    //     console.log(err);
    // });

    this.setState({ employeeArr: [] });

    API.get(`/api/team/my_team_members`)
      .then((res) => {
        var myTeam = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          var leave = "";
          if (element.on_leave == 1) {
            leave = "[On Leave]";
          }
          myTeam.push({
            value: element["employee_id"],
            label:
              element["first_name"] +
              " " +
              element["last_name"] +
              " (" +
              element["desig_name"] +
              ") " +
              leave,
          });
        }

        this.setState({ employeeArr: myTeam });
        this.getLeaveData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getLeaveData = () => {
    API.get(`/api/leave/`)
      .then((res) => {
        this.setState({
          tableData: res.data.data,
        });
        // console.log('res.data.data',res.data.data);
      })
      .catch((error) => {
        if (error.data.status === 3) {
          var token_rm = 2;
          showErrorMessageFront(error, token_rm, this.props);
        } else {
          console.log("error in fetching leave record", error);
        }
      });
  };

  updateLeave = (event, id) => {
    this.owner_emp = null;
    this.from_date_emp = null;
    this.to_date_emp = null;

    this.setState({
      owner_emp_err: null,
      to_date_emp_err: null,
      from_date_emp_err: null,
    });

    this.setState({ employeeArr: [] });

    API.get(`/api/team/my_team_members`)
      .then((res) => {
        var myTeam = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          var leave = "";
          if (element.on_leave == 1) {
            leave = "[On Leave]";
          }
          myTeam.push({
            value: element["employee_id"],
            label:
              element["first_name"] +
              " " +
              element["last_name"] +
              " (" +
              element["desig_name"] +
              ") " +
              leave,
          });
        }

        this.setState({ employeeArr: myTeam });
        this.getLeaveData();
      })
      .catch((err) => {
        console.log(err);
      });

    if (id) {
      event.preventDefault();
      API.get(`/api/leave/get_leave/${id}`)
        .then((response) => {
          let serverResp = response.data.data;

          //  update formik
          leaveInitialValues.owner = {
            value: serverResp.employee_id,
            label: `${serverResp.first_name} ${serverResp.last_name} (${serverResp.desig_name})`,
          };
          leaveInitialValues.fromDate = new Date(serverResp.date_from);
          leaveInitialValues.toDate = new Date(serverResp.date_to);
          leaveInitialValues.reason = serverResp.reason;
          leaveInitialValues.leave_status = serverResp.leave_status;

          this.owner_emp = {
            value: serverResp.employee_id,
            label: `${serverResp.first_name} ${serverResp.last_name} (${serverResp.desig_name})`,
          };
          this.from_date_emp = dateFormat(
            new Date(serverResp.date_from),
            "yyyy-mm-dd"
          );
          this.to_date_emp = dateFormat(
            new Date(serverResp.date_to),
            "yyyy-mm-dd"
          );

          // if(serverResp.other_employee_id > 0){
          //     leaveInitialValues.assignOtherCustomerTo = {
          //         value: serverResp.other_employee_id,
          //         label: `${serverResp.other_first_name} ${serverResp.other_last_name} (${serverResp.other_desig_name})`
          //     }
          // } else {
          //     leaveInitialValues.assignOtherCustomerTo = {
          //         value: 0,
          //         label: ''
          //     }
          // }

          if (serverResp.status === 1) {
            this.setState({
              default_status_value: { label: "On Leave", value: 1 },
            });
          } else {
          }

          if (serverResp.all_checked === 1) {
            leaveInitialValues.checkAll = true;

            let customArr = [];
            serverResp.assign_list.map((b) => {
              b.new_emp_id > 0
                ? (b.empObj = {
                    value: b.new_emp_id,
                    label: `${b.e_first_name} ${b.e_last_name} (${b.desig_name})`,
                  })
                : (b.empObj = "");

              customArr.push(b);
            });

            // set state
            this.setState({
              customer_list: customArr,
              selectChkBoxAll: true,
              headingText: "Update Leave",
              showModalForm: true,
              update_id: serverResp.id,
              assignAllCustomerTo: {
                value: serverResp.assign_list[0].new_emp_id,
                label: `${serverResp.assign_list[0].e_first_name} ${serverResp.assign_list[0].e_last_name} (${serverResp.assign_list[0].desig_name})`,
              },
              //assignOtherCustomerTo   : leaveInitialValues.assignOtherCustomerTo
            });

            leaveInitialValues.assignAllCustomerTo = {
              value: serverResp.assign_list[0].new_emp_id,
              label: `${serverResp.assign_list[0].e_first_name} ${serverResp.assign_list[0].e_last_name} (${serverResp.assign_list[0].desig_name})`,
            };
          } else {
            leaveInitialValues.checkAll = false;

            let editCustArr = [];
            serverResp.assign_list.map((rows) => {
              rows.checked = rows.checked;

              rows.new_emp_id > 0
                ? (rows.empObj = {
                    value: rows.new_emp_id,
                    label: `${rows.e_first_name} ${rows.e_last_name} (${rows.desig_name})`,
                  })
                : (rows.empObj = "");

              editCustArr.push(rows);
            });

            // set state
            this.setState({
              customer_list: editCustArr,
              selectChkBoxAll: false,
              headingText: "Update Leave",
              showModalForm: true,
              update_id: serverResp.id,
              assignAllCustomerTo: "",
              //assignOtherCustomerTo   : leaveInitialValues.assignOtherCustomerTo
            });
          }
        })
        .catch((err) => {
          showErrorMessageFront(err, 2, this.props);
        });
    } else {
      showErrorMessageFront("Error in page", 2, this.props);
    }
  };

  applyLeave = (values, actions) => {
    if (!values.owner || values.owner.value === "") {
      actions.setErrors({ owner: "Please select employee" });
      actions.setFieldTouched("owner");
      actions.setSubmitting(false);
    } else if (!values.fromDate || values.fromDate === "") {
      actions.setErrors({ fromDate: "Please select from date" });
      actions.setFieldTouched("fromDate");
      actions.setSubmitting(false);
    } else if (!values.toDate || values.toDate === "") {
      actions.setErrors({ toDate: "Please select to date" });
      actions.setFieldTouched("toDate");
      actions.setSubmitting(false);
    } else if (
      (this.state.selectChkBoxAll === true &&
        this.state.assignAllCustomerTo === "") ||
      (this.state.selectChkBoxAll === false &&
        this.state.assignAllCustomerTo !== "")
    ) {
      actions.setErrors({ assignedEmployee: "Invalid selection." });
      actions.setFieldTouched("assignedEmployee");
      actions.setSubmitting(false);
    }
    // else if(this.state.assignOtherCustomerTo === ''){
    //     actions.setErrors({errAssignOther:'Please select employee for others.'});
    //     actions.setFieldTouched('assignOtherCustomerTo');
    //     actions.setSubmitting(false);
    // }
    else {
      actions.setSubmitting(true);

      let assignToArr = [];
      this.state.customer_list.map((rows, k) => {
        if (rows.checked === true && typeof rows.empObj === "object") {
          assignToArr.push({
            customer_id: rows.customer_id,
            employee_id: rows.empObj.value,
          });
        }
      });

      let from_date = dateFormat(values.fromDate, "yyyy-mm-dd");
      let to_date = dateFormat(values.toDate, "yyyy-mm-dd");
      let employee_id = values.owner.value;
      let assign_to = assignToArr;
      let reason = values.reason;
      let status = this.state.default_status_value.value;
      let all_check = this.state.selectChkBoxAll;
      //let others      = values.assignOtherCustomerTo.value

      // UPDATE REQUEST
      if (this.state.update_id > 0) {
        // console.log(this.state.default_status_value);
        const request_body = {
          from_date: from_date,
          to_date: to_date,
          assign_to: assign_to,
          reason: reason,
          status: status,
          all_check: all_check,
          //others      : others
        };
        // console.log(request_body);
        API.put(`/api/leave/assign/${this.state.update_id}`, request_body)
          .then((res) => {
            this.closeModalForm();
            swal({
              title: "Success",
              text: "Your leave request has been updated successfully!",
              icon: "success",
            }).then(() => {
              this.setState({ update_id: 0 });
              this.getLeaveData();
            });
          })
          .catch((error) => {
            this.setState({ showLoader: false });
            if (error.data.status === 3) {
              var token_rm = 2;
              showErrorMessageFront(error, token_rm, this.props);
            } else {
              actions.setErrors(error.data.errors);
              actions.setSubmitting(false);
            }
          });
      } else {
        // ADD REQUEST
        const request_body = {
          employee_id: employee_id,
          from_date: from_date,
          to_date: to_date,
          assign_to: assign_to,
          reason: reason,
          all_check: all_check,
          //others      : others
        };
        //console.log( request_body )
        API.post(`/api/leave/assign/`, request_body)
          .then((res) => {
            this.closeModalForm();
            swal({
              title: "Success",
              text: "Your leave request has been submitted successfully!",
              icon: "success",
            }).then(() => {
              this.getLeaveData();
            });
          })
          .catch((error) => {
            this.setState({ showLoader: false });
            if (error.data.status === 3) {
              var token_rm = 2;
              showErrorMessageFront(error, token_rm, this.props);
            } else {
              actions.setErrors(error.data.errors);
              actions.setSubmitting(false);
            }
          });
      }
    }
  };

  changeOwner = (event, setFieldValue) => {
    if (event === null) {
      this.owner_emp = null;
      setFieldValue("owner", "");
    } else {
      this.owner_emp = event;
      setFieldValue("owner", event);
    }
    this.getCustomersData();
  };

  changeFromDate = (event, setFieldValue) => {
    if (event === null) {
      this.from_date_emp = null;
      setFieldValue("fromDate", "");
    } else {
      this.from_date_emp = event;
      setFieldValue("fromDate", event);
    }
    this.getCustomersData();
  };

  changeToDate = (event, setFieldValue) => {
    if (event === null) {
      this.to_date_emp = null;
      setFieldValue("toDate", "");
    } else {
      this.to_date_emp = event;
      setFieldValue("toDate", event);
    }
    this.getCustomersData();
  };

  getCustomersData = () => {
    this.setState({
      owner_emp_err: null,
      to_date_emp_err: null,
      from_date_emp_err: null,
    });

    if (
      this.owner_emp !== null &&
      this.from_date_emp !== null &&
      this.to_date_emp !== null
    ) {
      var post_date = {
        owner: this.owner_emp.value,
        from_date: dateFormat(this.from_date_emp, "yyyy-mm-dd"),
        to_date: dateFormat(this.to_date_emp, "yyyy-mm-dd"),
      };

      if (this.state.update_id > 0) {
        post_date.leave_id = this.state.update_id;
        API.post(`/api/leave/employee_customer_update`, post_date)
          .then((res) => {
            //console.log('________>', res.data.data );

            let response = res.data.data;
            let tempArr = [];

            response.assign_list.map((v) => {
              v.new_emp_id > 0
                ? (v.empObj = {
                    value: v.new_emp_id,
                    label: `${v.e_first_name} ${v.e_last_name} (${v.desig_name})`,
                  })
                : (v.empObj = "");

              tempArr.push(v);
            });

            // set state
            this.setState({
              customer_list: tempArr,
              selectChkBoxAll: response.all_checked === 1 ? true : false,
              selectedOwner: this.owner_emp,
              assignAllCustomerTo:
                response.all_checked === 1
                  ? {
                      value: response.employee_id,
                      label: `${response.first_name} ${response.last_name} (${response.desig_name})`,
                    }
                  : "",
            });
          })
          .catch((err) => {
            console.log(err);
          });
      } else {
        API.post(`/api/leave/employee_customers`, post_date)
          .then((res) => {
            let tempArr = [];
            res.data.data.map((v) => {
              v.empObj = "";
              tempArr.push(v);
            });

            // set state
            this.setState({
              customer_list: tempArr,
              selectChkBoxAll: false,
              selectedOwner: this.owner_emp,
              assignAllCustomerTo: "",
            });
          })
          .catch((err) => {
            console.log("=============", err.data.errors);
            if (err.data.errors.fromDate !== "") {
              this.setState({ from_date_emp_err: err.data.errors.fromDate });
              //errors.fromDate = err.data.errors.fromDate;
              //setFieldTouched('fromDate');
            }
            if (err.data.errors.toDate !== "") {
              this.setState({ to_date_emp_err: err.data.errors.toDate });

              //errors.toDate = err.data.errors.toDate;
              //setFieldTouched('toDate');
            }
            if (err.data.errors.owner !== "") {
              this.setState({ owner_emp_err: err.data.errors.owner });

              //errors.owner = err.data.errors.owner;
              //setFieldTouched('owner');
            }
            //console.log(err);
          });
      }
    }
  };

  onChangeAllEmployee = (event, setFieldValue) => {
    let tempArr = [];

    if (event === null) {
      setFieldValue("assignAllCustomerTo", "");
      this.setState({ assignAllCustomerTo: "" });

      this.state.customer_list.map((cust, i) => {
        cust.empObj = "";
        tempArr.push(cust);
      });
      this.setState({ customer_list: tempArr });
    } else {
      setFieldValue("assignAllCustomerTo", event);
      this.setState({ assignAllCustomerTo: event });

      this.state.customer_list.map((cust) => {
        cust.empObj = {
          value: event.value,
          label: event.label,
        };
        tempArr.push(cust);
      });
      this.setState({ customer_list: tempArr });
    }
  };

  // onChangeOtherEmployee = (event,setFieldValue) => {
  //     if(event === null){
  //         setFieldValue('assignOtherCustomerTo', '')
  //         this.setState({assignOtherCustomerTo: ''});
  //     }else{
  //         setFieldValue('assignOtherCustomerTo', event)
  //         this.setState({assignOtherCustomerTo: event});
  //     }
  // }

  openLeaveForm = () => {
    this.owner_emp = null;
    this.from_date_emp = null;
    this.to_date_emp = null;
    this.owner_emp_err = null;
    this.to_date_emp_err = null;
    this.from_date_emp_err = null;
    this.setState({
      showModalForm: true,
      owner_emp_err: null,
      to_date_emp_err: null,
      from_date_emp_err: null,
      //assignOtherCustomerTo : ''
    });

    this.setState({ employeeArr: [] });

    API.get(`/api/team/my_team_members`)
      .then((res) => {
        var myTeam = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          var leave = "";
          if (element.on_leave == 1) {
            leave = "[On Leave]";
          }
          myTeam.push({
            value: element["employee_id"],
            label:
              element["first_name"] +
              " " +
              element["last_name"] +
              " (" +
              element["desig_name"] +
              ") " +
              leave,
          });
        }

        this.setState({ employeeArr: myTeam });
        this.getLeaveData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  closeModalForm = () => {
    this.setState({
      showModalForm: false,
      customer_list: [],
      selectChkBoxAll: false,
      selectedOwner: "",
      //assignOtherCustomerTo : '',
      headingText: "Create Leave",
      default_status_value: {},
      update_id: null,
    });
    leaveInitialValues.owner = {
      value: 0,
      label: "",
    };
    //leaveInitialValues.assignOtherCustomerTo = '';
    leaveInitialValues.fromDate = "";
    leaveInitialValues.toDate = "";
    leaveInitialValues.reason = "";
  };

  changeStatus = (event, setFieldValue) => {
    if (event === null) {
      setFieldValue("leave_status", "");
      this.setState({ default_status_value: "" });
    } else {
      setFieldValue("leave_status", event);
      this.setState({ default_status_value: event });
    }
  };

  selectAllCustomer = (event, setFieldValue, setFieldTouched) => {
    let checkedStatus = event.target.checked;
    // console.log(event.target.checked);
    // update all checkbox field
    setFieldValue("checkAll", checkedStatus);
    setFieldTouched("checkAll");

    // assign an array to hold updated checked status & push into it
    var tempCustArr = [];
    this.state.customer_list.map((cust) => {
      if (checkedStatus) {
        cust.checked = true;
      } else {
        cust.checked = checkedStatus;
        cust.empObj = null;
      }
      tempCustArr.push(cust);
    });

    //leaveInitialValues.checkAll     = checkedStatus;

    if (checkedStatus === false) {
      //leaveInitialValues.assignAllCustomerTo = '';
      setFieldValue("assignAllCustomerTo", "");
      this.setState({ assignAllCustomerTo: "" });
    }

    this.setState({
      customer_list: tempCustArr,
      selectChkBoxAll: checkedStatus,
    });
  };

  selectCustomer = (event) => {
    let tempCustArr = [];

    // update checked property to customer list
    this.state.customer_list.map((cust, i) => {
      if (cust.customer_id === event.target.value) {
        cust.checked = event.target.checked;
        cust.empObj = "";
      }
      tempCustArr.push(cust);
    });

    // // update state
    this.setState({
      customer_list: tempCustArr,
      selectChkBoxAll: false,
      assignAllCustomerTo: "",
    });
  };

  onChangeEmployee = (event, customer_id) => {
    if (event === null) {
      let tempArr = [];
      this.state.customer_list.map((val) => {
        if (val.customer_id === customer_id) {
          val.empObj = "";
        }
        tempArr.push(val);
      });

      this.setState({ customer_list: tempArr });
    } else {
      let tempArr = [];
      this.state.customer_list.map((val) => {
        if (val.customer_id === customer_id) {
          val.empObj = event;
        }
        tempArr.push(val);
      });

      this.setState({ customer_list: tempArr });
    }
  };

  render() {
    // console.log('rendering ===>', leaveInitialValues);

    if (this.state.showLoader) {
      return (
        <div className="loderOuter">
          <div className="loader">
            <img src={loaderlogo} alt="logo" />
            <div className="loading">Loading...</div>
          </div>
        </div>
      );
    } else {
      return (
        <>
          <div className="content-wrapper leave-management">
            <section className="content-header">
              <h1>Leaves Management</h1>
              <div
                className="plus-sign btn-fill"
                onClick={() => this.openLeaveForm()}
              >
                <i className="fas fa-plus"></i>
              </div>
            </section>
            <section className="content">
              <div className="nav-tabs-custom">
                <BootstrapTable data={this.state.tableData} striped hover>
                  <TableHeaderColumn isKey dataField="employee_name">
                    Employee
                  </TableHeaderColumn>
                  <TableHeaderColumn dataField="date_added">
                    Request Date
                  </TableHeaderColumn>
                  <TableHeaderColumn dataField="date_from">
                    From
                  </TableHeaderColumn>
                  <TableHeaderColumn dataField="date_to">To</TableHeaderColumn>
                  <TableHeaderColumn
                    dataField="status"
                    dataFormat={setLeaveStatus(this)}
                  >
                    Status
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    dataField="no_of_leaves"
                    dataFormat={setNumberOfLeaves(this)}
                  >
                    No of Leaves
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    dataField="id"
                    dataFormat={actionFormatter(this)}
                  >
                    Action
                  </TableHeaderColumn>
                </BootstrapTable>
              </div>
            </section>
          </div>

          <Modal
            show={this.state.showModalForm}
            onHide={() => this.closeModalForm()}
            backdrop="static"
          >
            <Formik
              initialValues={leaveInitialValues}
              validationSchema={validateLeave}
              onSubmit={this.applyLeave}
              enableReinitialize={true}
            >
              {({
                values,
                errors,
                touched,
                isValid,
                isSubmitting,
                setFieldValue,
                setFieldTouched,
                setValues,
                setErrors,
              }) => {
                // {
                //   console.log("validate ===>", validateLeave);
                // }
                // {
                //   console.log("errors ===>", errors);
                // }

                return (
                  <Form>
                    <Modal.Header closeButton>
                      <Modal.Title>{this.state.headingText}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <div className="contBox leave-contBox">
                        <Row>
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label>
                                Select Employee{" "}
                                <span className="required-field">*</span>
                              </label>
                              <Select
                                className="basic-single"
                                classNamePrefix="select"
                                defaultValue={values.owner}
                                isClearable={true}
                                isSearchable={true}
                                name="owner"
                                value={values.owner}
                                options={this.state.employeeArr}
                                onChange={(e) => {
                                  this.changeOwner(e, setFieldValue);
                                }}
                                onBlur={() => setFieldTouched("owner")}
                                isDisabled={
                                  this.state.update_id > 0 ? true : false
                                }
                              />

                              {errors.owner || touched.owner ? (
                                <span className="errorMsg">{errors.owner}</span>
                              ) : null}

                              {console.log(
                                "from_date_emp_err",
                                this.state.from_date_emp_err
                              )}

                              {this.state.owner_emp_err !== null ||
                              this.state.to_date_emp_err !== null ||
                              this.state.from_date_emp_err !== null ? (
                                <span className="errorMsg errorleqave">
                                  {this.state.owner_emp_err}
                                  {this.state.to_date_emp_err}
                                  {this.state.from_date_emp_err}
                                </span>
                              ) : null}
                            </div>
                          </Col>
                        </Row>

                        <Row>
                          <Col xs={6} sm={6} md={6}>
                            <div className="form-group react-date-picker">
                              <label>From</label>
                              <div className="form-control">
                                <DatePicker
                                  name={"fromDate"}
                                  className="borderNone"
                                  selected={
                                    values.fromDate ? values.fromDate : null
                                  }
                                  //onChange={value => setFieldValue('dueDate', value)}
                                  dateFormat="dd/MM/yyyy"
                                  autoComplete="off"
                                  onChange={(e) => {
                                    this.changeFromDate(e, setFieldValue);
                                  }}
                                  onBlur={() => setFieldTouched("fromDate")}
                                />
                                {errors.fromDate && touched.fromDate ? (
                                  <span className="errorMsg errorleqave">
                                    {errors.fromDate}
                                  </span>
                                ) : null}
                              </div>
                            </div>
                          </Col>
                          <Col xs={6} sm={6} md={6}>
                            <div className="form-group react-date-picker">
                              <label>To</label>
                              <div className="form-control">
                                <DatePicker
                                  name={"toDate"}
                                  className="borderNone"
                                  selected={
                                    values.toDate ? values.toDate : null
                                  }
                                  //onChange={value => setFieldValue('dueDate', value)}
                                  dateFormat="dd/MM/yyyy"
                                  autoComplete="off"
                                  onChange={(e) => {
                                    this.changeToDate(e, setFieldValue);
                                  }}
                                  onBlur={() => setFieldTouched("toDate")}
                                />
                                {errors.toDate && touched.toDate ? (
                                  <span className="errorMsg errorleqave">
                                    {errors.toDate}
                                  </span>
                                ) : null}
                              </div>
                            </div>
                          </Col>
                        </Row>

                        <Row>
                          <Col xs={12} sm={12} md={12}>
                            <div className="form-group">
                              <label> Reason </label>
                              <Field
                                name="reason"
                                component="textarea"
                                className={`selectArowGray form-control`}
                                autoComplete="off"
                                value={values.reason}
                                onBlur={() => setFieldTouched("reason")}
                              >
                                {errors.reason || touched.reason ? (
                                  <p>{errors.reason}</p>
                                ) : null}
                              </Field>
                            </div>
                          </Col>
                        </Row>

                        {this.state.customer_list.length > 0 && (
                          <div className="leave-man-form-group-first">
                            <Row>
                              <Col xs={12} sm={6} md={6}>
                                <div className="form-group checText">
                                  <input
                                    type="checkbox"
                                    name="checkAll"
                                    value={values.checkAll}
                                    checked={this.state.selectChkBoxAll}
                                    onChange={(e) =>
                                      this.selectAllCustomer(
                                        e,
                                        setFieldValue,
                                        setFieldTouched
                                      )
                                    }
                                  />
                                  <label> Assign All Customers </label>
                                </div>
                              </Col>

                              <Col xs={12} sm={6} md={6}>
                                <div className="leave-man-form-group">
                                  <label> Assign To </label>

                                  <Select
                                    className="leave-single"
                                    classNamePrefix="select"
                                    isClearable={true}
                                    isSearchable={true}
                                    defaultValue={
                                      this.state.assignAllCustomerTo
                                    }
                                    value={this.state.assignAllCustomerTo}
                                    name="assignAllCustomerTo"
                                    options={this.state.employeeArr.filter(
                                      (x) => {
                                        return (
                                          values.owner.value.toString() !==
                                          x.value.toString()
                                        );
                                      }
                                    )}
                                    onChange={(e) => {
                                      this.onChangeAllEmployee(
                                        e,
                                        setFieldValue
                                      );
                                    }}
                                    onBlur={() =>
                                      setFieldTouched("assignAllCustomerTo")
                                    }
                                  />
                                </div>
                              </Col>
                            </Row>
                            <div className="clearFix"></div>
                          </div>
                        )}

                        {/* {console.log('===>', this.state.customer_list)} */}

                        {this.state.customer_list.length > 0 &&
                          this.state.customer_list.map((customer, index) => {
                            return (
                              <div
                                key={index}
                                className="leave-man-form-group-sub"
                              >
                                <Row>
                                  <Col xs={6} sm={6} md={6} className="mobFull">
                                    <div className="form-group checText">
                                      <input
                                        type="checkbox"
                                        name="selectedCustomerArr"
                                        value={customer.customer_id}
                                        checked={customer.checked}
                                        onChange={(e) => {
                                          this.selectCustomer(e);
                                        }}
                                      />
                                      <label>
                                        {htmlDecode(customer.company_name)} (
                                        {htmlDecode(customer.first_name)}{" "}
                                        {htmlDecode(customer.last_name)})
                                      </label>
                                    </div>
                                  </Col>

                                  <Col xs={6} sm={6} md={6} className="mobFull">
                                    <Select
                                      className="leave-single"
                                      classNamePrefix="select"
                                      isClearable={true}
                                      isSearchable={true}
                                      defaultValue={customer.empObj}
                                      value={customer.empObj}
                                      name={`selectedEmployee${index}`}
                                      options={this.state.employeeArr.filter(
                                        (x) => {
                                          return (
                                            values.owner.value.toString() !==
                                            x.value.toString()
                                          );
                                        }
                                      )}
                                      onChange={(e) => {
                                        this.onChangeEmployee(
                                          e,
                                          customer.customer_id
                                        );
                                      }}
                                      onBlur={() =>
                                        setFieldTouched(
                                          `selectedEmployee${index}`
                                        )
                                      }
                                    />
                                  </Col>
                                </Row>
                                <div className="clearFix"></div>
                              </div>
                            );
                          })}

                        {errors.assignedEmployee || touched.assignedEmployee ? (
                          <span className="errorMsg">
                            {errors.assignedEmployee}
                          </span>
                        ) : null}

                        {/* start other customer block */}
                        {/* {this.owner_emp !== null && this.from_date_emp !== null && this.to_date_emp !== null &&
                                            <Row>
                                                <Col xs={12} sm={12} md={6}>
                                                    <div className="form-group checText">
                                                        <label> Assign Other Customers </label>
                                                    </div>
                                                </Col>
                                                
                                                <Col xs={12} sm={12} md={6}>
                                                    <div className="leave-man-form-group">
                                                        <label> Assign To </label>                          
                                                        <Select
                                                            className="leave-single"
                                                            classNamePrefix="select"
                                                            isClearable={true}
                                                            isSearchable={true}
                                                            defaultValue={this.state.assignOtherCustomerTo}
                                                            value={this.state.assignOtherCustomerTo}
                                                            name="assignOtherCustomerTo"
                                                            options={
                                                                this.state.employeeArr.filter(x => {
                                                                    return values.owner.value.toString() !== x.value.toString()
                                                                })
                                                            }
                                                            onChange={e => {this.onChangeOtherEmployee(e,setFieldValue)}}
                                                            onBlur={() => setFieldTouched("assignOtherCustomerTo")}  
                                                        />
                                                        
                                                        {errors.errAssignOther || touched.errAssignOther ? (
                                                        <span className="errorMsg">
                                                            {errors.errAssignOther}
                                                        </span>
                                                        ) : null}
                                                    </div>
                                                </Col>                                       
                                            </Row>
                                            } */}
                        {/* end other customer block */}

                        {this.state.update_id > 0 && (
                          <Row>
                            <Col xs={6} sm={6} md={6}>
                              <div className="leave-man-status">
                                <label> Status </label>
                                <Select
                                  className="leave-single"
                                  classNamePrefix="select"
                                  isSearchable={true}
                                  defaultValue={this.state.default_status_value}
                                  name={`leave_status`}
                                  options={this.state.status_arr}
                                  onChange={(e) => {
                                    this.changeStatus(e, setFieldValue);
                                  }}
                                />
                              </div>
                            </Col>
                          </Row>
                        )}
                      </div>
                    </Modal.Body>
                    <Modal.Footer>
                      <button
                        className={`btn-fill ${
                          this.state.update_id > 0
                            ? "btn-custom-green"
                            : isValid
                            ? "btn-custom-green"
                            : "btn-disable"
                        } m-r-10`}
                        type="submit"
                        //disabled={isValid ? false : true}
                      >
                        {this.state.stopflagId > 0
                          ? isSubmitting
                            ? "Updating..."
                            : "Update"
                          : isSubmitting
                          ? "Submitting..."
                          : "Submit"}
                      </button>
                    </Modal.Footer>
                  </Form>
                );
              }}
            </Formik>
          </Modal>
        </>
      );
    }
  }
}

export default Leave;
