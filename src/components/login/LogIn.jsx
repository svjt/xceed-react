import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";

// redux store
import { connect } from "react-redux";
import { frontEndLogin } from "../../store/actions/auth";

//import { Button } from "react-bootstrap";
// form, fields, validation, schemas
import * as Yup from "yup";
import { Formik, Field, Form } from "formik";

// Loader
import LoadingSpinner from "../loader/loadingSpinner";
//import LoadingPage from '../loader/LoadingPage';

import drreddylogo from "../../assets/images/drreddylogo_white.png";
import exceedlogo from "../../assets/images/Xceed_Logo_Purple_RGB.png";

import { getLPlusOne } from "../../shared/helper";

import "./LogIn.css";

// initialize form and their validation
const initialValues = {
  email: "",
  password: "",
};

const loginSchema = Yup.object().shape({
  email: Yup.string().trim()
    .trim("Please remove whitespace")
    .email("E-mail is not valid!")
    .strict()
    .required("Please enter email"),
  password: Yup.string().trim()
    .trim("Please remove whitespace")
    .strict()
    .required("Please enter password"),
});
// end here

class Login extends Component {
  constructor() {
    super();
    this.state = {
      loading: false,
      errors: null,
      enableFocus: false,
    };
  }

  handleSubmit = (values, { setErrors }) => {
    // SATYAJIT

    this.setState({ loading: true }, () => {
      this.props.submitLogin(
        values,
        () => {

          var ot = localStorage.getItem("ot");
          console.log('ot',ot);

          
          var level = getLPlusOne();
          if (level === 3 || level === 2) {
            this.props.history.push("/user/my_team");
          } else {
            this.props.history.push("/user/dashboard");
          }
        },
        setErrors
      );
    });
  };

  handleChange = (e, field) => {
    this.setState({
      [field]: e.target.value,
      enableFocus: true,
    });
  };

  render() {
    //console.log( this.props );

    let token = localStorage.getItem("token");
    if (token) {
      return <Redirect to="/user/dashboard" />; // Redirecting the user to dashboard if already logged in
      //return <Redirect to="/" />;
    }

    const { loading } = this.props;

    return (
      <>
        <div className="login-box">
          {loading ? (
            <LoadingSpinner />
          ) : (
            <Formik
              initialValues={initialValues}
              validationSchema={loginSchema}
              onSubmit={this.handleSubmit}
            >
              {({
                values,
                errors,
                touched,
                handleChange,
                isValid,
                handleBlur,
                setFieldValue,
              }) => {
                //console.log( errors );
                return (
                  <Form>
                    <div className="login-logo">
                      <Link to="/" className="logo">
                        <span className="logo-mini">
                          <img src={drreddylogo} alt="Dr.Reddy's" />
                        </span>
                      </Link>
                      <br />
                      <span className="logo-mini">
                        <img src={exceedlogo} alt="Dr.Reddy's" height="36px" />
                      </span>
                    </div>

                    <div className="login-box-body">
                      {/* invalid token format */}
                      {/* {this.props.auth.data.message && 
										<p><label>{this.props.auth.data.message}</label></p>} */}

                      <h2>{errors.process}</h2>

                      <p className="login-box-msg">Login to Your Account</p>
                      <div className="form-group has-feedback m-b-25">
                        <Field
                          name="email"
                          type="text"
                          className="form-control"
                          autoComplete="off"
                          placeholder="email"
                          onChange={handleChange}
                          autoFocus={this.state.enableFocus}
                        />

                        {errors.email && touched.email && (
                          <label>{errors.email}</label>
                        )}

                        {/* <span className="glyphicon glyphicon-user form-control-feedback"></span> */}
                        {/* <span class="help-block with-errors">Please correct the error</span> */}
                      </div>
                      <div className="form-group has-feedback">
                        <Field
                          name="password"
                          type="password"
                          className="form-control"
                          autoComplete="off"
                          placeholder="Password"
                          onChange={handleChange}
                          autoFocus={this.state.enableFocus}
                        />
                        {errors.password && touched.password && (
                          <label>{errors.password}</label>
                        )}
                        {/* <span className="glyphicon glyphicon-lock form-control-feedback"></span> */}
                        {/* <span class="help-block with-errors">Please correct the error</span> */}
                      </div>
                      <div className="row">
                        <div className="col-xs-8" />
                        <div className="col-xs-4">
                          <button
                            className="btn-fill"
                            type="submit"
                            disabled={isValid ? false : true}
                          >
                            Login
                          </button>
                        </div>
                      </div>
                    </div>
                  </Form>
                );
              }}
            </Formik>
          )}{" "}
          {/* end of loader */}
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.auth.loading,
    errors: state.auth.errors,
  };
};

const mapDispatchToProps = (dispatch) => {
  //console.log("dispatch called")
  return {
    submitLogin: (data, onSuccess, setErrors) =>
      dispatch(frontEndLogin(data, onSuccess, setErrors)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
