import React from 'react';
import Loader from 'react-loader-spinner'

const LoadingPage = () => (
    <div className="loading_reddy_outer">
        <div className="loading_reddy" >
            <Loader 
                type="Puff"
                color="#00BFFF"
                height="50"	
                width="50"
                verticalAlign="middle"
            />
        </div>
    </div>
);

export default LoadingPage;