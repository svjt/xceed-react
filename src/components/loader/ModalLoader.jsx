import React, { Component } from 'react';

import whitelogo from '../../assets/images/drreddylogo_white.png';

class ModalLoader extends Component {
    render() {
        return (
            this.props.isLoading && (
                <div className='loading_reddy_outer'>
                    <div className='loading_reddy'>
                        <img src={whitelogo} alt='loader' />
                    </div>
                </div>
            )
        );
    }
}

export default ModalLoader;
