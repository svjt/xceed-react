import React, { Component } from "react";
import loader from '../../assets/images/logo.png';

class Loader extends Component {
  render(){
    return(
      <div className="loading"><img src={loader} alt="loader"/></div>
    )
  }
}
export default Loader;