import React, { Component } from 'react';
import { getDashboard,getMyId,htmlDecode, inArray } from '../../shared/helper';

import ellipsisImage from '../../assets/images/ellipsis-icon.svg';
import exclamationImage from '../../assets/images/exclamation-icon.svg';
import peopleImage from '../../assets/images/people-icon.svg';

import exclamationImage2 from "../../assets/images/exclamation-icon-black.svg";

//import { Link } from 'react-router-dom';
import {
    Tooltip,
    OverlayTrigger
  } from "react-bootstrap";

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
    return (
      <OverlayTrigger
        overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
        placement="left"
        delayShow={300}
        delayHide={150}
        trigger={["hover"]}
      >
        {children}
      </OverlayTrigger>
    );
  }
  /*For Tooltip*/

class BMVerticalMenu extends Component {

    constructor(){
        super();
        document.querySelector('body').addEventListener('click',function(event){
            var elems = document.querySelectorAll('.btn-group');
            for (var i = 0; i < elems.length; i++){
                elems[i].classList.remove('open');
            }
        });
    }

    assign = () => {
        this.props.showAssignPopup(this.props.currRow);
    }

    re_assign = () => {       
        this.props.showReAssignPopup(this.props.currRow);
    }

    re_assign_cqt = () => {       
        this.props.showReAssignCQTPopup(this.props.currRow);
    }

    subTask = () => {
        this.props.showSubTaskPopup(this.props.currRow);
    }

    poke = () => {
        this.props.poke(this.props.currRow);
    }

    respondBack = () => {
        this.props.showRespondPopup(this.props.currRow);
    }

    createProforma = () => {
        this.props.showProforma(this.props.currRow);
    }

    respondCustomer = () => {
        this.props.showRespondCustomerPopup(this.props.currRow);
    }

    closeTask = () => {
        this.props.showCloseTaskPopup(this.props.currRow);
    }

    deleteTask = () => {
        this.props.showDeleteTaskPopup(this.props.currRow);
    }

    authorizeTask = () => {
        this.props.showAuthorizeTaskPopup(this.props.currRow);
    }

    increaseSla = () => {
        this.props.showIncreaseSlaPopup(this.props.currRow);
    }

    cloneMainTask = () => {
        this.props.showCloneMainTaskPopup(this.props.currRow);
    }

    clone = () => {
        this.props.clone(this.props.currRow);
    }

    cloneSubTask = () => {
        this.props.cloneSubTask(this.props.currRow);
    }

    handleVerticalMenu = (event) => {
        var elems = document.querySelectorAll('.btn-group');
        for (var i = 0; i < elems.length; i++){
            elems[i].classList.remove('open');
        }

        if(event.target.parentNode.classList.contains("btn-group")){
            event.target.parentNode.classList.add("open");
        }else if(event.target.parentNode.classList.contains("dropdown-toggle")){
            event.target.parentNode.parentNode.classList.add("open");
        }
        
    }

    getCurrentOwnerText = () => {
        if(this.props.currRow.cqt > 0 ){
            return (
                <LinkWithTooltip
                    tooltip={`The task is in CQT`}
                    href="#"
                    id={`tooltip-menu-${this.props.currRow.task_id}${this.props.currRow.current_ownership}`}
                    clicked={e => this.checkHandler(e)}
                >
                    <img src={peopleImage} alt="exclamation" />
                </LinkWithTooltip>

            );
        }else if(this.props.currRow.genpact > 0 ){
            return (
                <LinkWithTooltip
                    tooltip={`This feature is currently unavailable`}
                    href="#"
                    id={`tooltip-menu-${this.props.currRow.task_id}${this.props.currRow.current_ownership}`}
                    clicked={e => this.checkHandler(e)}
                >
                    <img src={peopleImage} alt="exclamation" />
                </LinkWithTooltip>

            );
        }else{
            return (
                <LinkWithTooltip
                    tooltip={`The task is with ${this.props.currRow.curr_owner_fname} ${this.props.currRow.curr_owner_lname} (${this.props.currRow.curr_owner_desig})`}
                    href="#"
                    id={`tooltip-menu-${this.props.currRow.task_id}${this.props.currRow.current_ownership}`}
                    clicked={e => this.checkHandler(e)}
                >
                    <img src={peopleImage} alt="exclamation" />
                </LinkWithTooltip>
            );
        }
    }

    getGenpactText = () => {
        return (
            <LinkWithTooltip
                tooltip={this.props.currRow.genpact_type == 1?`Sales Order`:`Stock Transfer Order`}
                href="#"
                id={`tooltip-menu-${this.props.currRow.task_id}${this.props.currRow.genpact_type}`}
                clicked={e => this.checkHandler(e)}
            >
                <img src={exclamationImage2} alt="exclamation" />
            </LinkWithTooltip>

        );
    }

    render() {

        var assign_task,create_sub_task,respond_to_customer,close_task,delete_task,respond_back,poke,authorize,re_assign_task,re_assign_cqt,breach,current_ownership,hide_menu,proforma,increase_sla,clone,sub_task_clone,clone_main_task,genpact_menu,approveComment,EditTranslatedComment,approveTaskPopup,EditTranslatedTask = false;
        var my_id        = getMyId();
        var request_type = this.props.currRow.request_type;

        if(this.props.approvalTab == 1){
            if(this.props.currRow.review_status == true){
                if(this.props.currRow.review_comment_id === 0){
                    approveTaskPopup = true;
                    EditTranslatedTask = true;
                }else{
                    approveComment        = true;
                    EditTranslatedComment = true;
                }
            }else{
                hide_menu = true;
            }
        }else if(this.props.approvalTabTaskDetails == 1){
            if(this.props.taskReview > 0){
                approveTaskPopup = true; 
            }else{
                hide_menu = true;
            }
        }else{
            if(this.props.currRow.parent_id === 0){

                let dashboard_type = getDashboard();
    
                //MAIN TASK
                if( dashboard_type === 'BM' ||
                    dashboard_type === 'SPOC' ||
                    dashboard_type === 'CSC' || 
                    dashboard_type === 'RA' ){
                    close_task          = true;
                    respond_to_customer = true;
    
                    if(dashboard_type === 'BM'){
                        delete_task = true;
                    }
                }else{
                    delete_task = false;
                }
                assign_task     = true;
                create_sub_task = true;
                sub_task_clone  = true;
    
                if(this.props.currRow.assigned_by > 0){
                    respond_back = true;
                }
    
                if( (request_type === 23 || request_type === 41) && (
                    (this.props.currRow.current_ownership !== null && this.props.currRow.current_ownership === my_id) 
                    || 
                    (this.props.currRow.owner !== null && this.props.currRow.owner === my_id) ) ){
                    proforma = true;
                }
    
                if(!inArray(request_type,[24,25,26]) && (
                    (this.props.currRow.current_ownership !== null && this.props.currRow.current_ownership === my_id) 
                || 
                    (this.props.currRow.owner !== null && this.props.currRow.owner === my_id)
                ) && this.props.currRow.pause_sla === 0){
                    increase_sla = true;
                }
    
                // this.props.currRow.pause_sla  -- SATYAJIT (22/01/2020)
    
                clone_main_task = true;

                if(this.props.for_review_task > 0 && this.props.is_spoc_review === true){
                    approveTaskPopup = true;
                }
    
            }else{
                //SUB TASK
    
                if(this.props.currRow.cqt && this.props.currRow.cqt > 0 && this.props.currRow.cqt_status && this.props.currRow.cqt_status === 1 ){
                    //DO NOTHING
                    hide_menu = true;
                }else if(this.props.currRow.genpact && this.props.currRow.genpact == 1){
                    //DO NOTHING
                    genpact_menu = true;
        
                    if(this.props.currRow.current_ownership !== null && this.props.currRow.current_ownership > 0){
                        current_ownership = true;
                    }
    
                }else{
    
                    if(this.props.currRow.close_status === 1){
                        hide_menu = true;
                    }else{
                        if(this.props.currRow.assigned_by === my_id){
                        
                            if(this.props.currRow.cqt !== null && this.props.currRow.cqt > 0){
                                //re_assign_cqt = true;
                            }else{
                                if(getDashboard() !== 'SPOC'){
                                    re_assign_task = true;
                                }
                                poke = true;
                            }
                        }else{
                            assign_task = true;
                            
                            if(this.props.currRow.assigned_by > 0){
                                //create_sub_task = true;
                                if(this.props.currRow.need_authorization === 1){
                                    authorize  = true;   
                                }else{
                                    respond_back = true;
                                }
                            }
                        }
            
                        if(this.props.currRow.owner === my_id){
                            close_task  = true;
                        }
                    }
        
                    if(this.props.currRow.current_ownership !== null && this.props.currRow.current_ownership > 0){
                        current_ownership = true;
                    }
    
                    if(!inArray(request_type,[24,25,26]) && (
                        (this.props.currRow.current_ownership !== null && this.props.currRow.current_ownership === my_id) 
                    || 
                        (this.props.currRow.owner !== null && this.props.currRow.owner === my_id)
                    ) && this.props.currRow.pause_sla === 0){
                        increase_sla = true;
                    }
                    
                    if(this.props.currRow.owner !== null && this.props.currRow.owner == my_id){
                        clone = true;
                    }
                    
                }
                
            }

            if(this.props.currRow.breach_reason !== null && this.props.currRow.breach_reason !== ''){
                breach = true;
            }
        }
        
        if(hide_menu){
            return null;
        }else if(genpact_menu){
            return (
                <>
                <div className="actionStyle">
                    <div className="btn-group">
                        <button type="button" className="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" onClick = {(e) => this.handleVerticalMenu(e)} >
                            <img src={ellipsisImage} alt="ellipsisImage" />
                        </button>
                        
                        {this.getGenpactText()}
                        
                        <ul className="dropdown-menu pull-right" role="menu">
                            
                            {<li><span onClick={()=>this.props.requestCaseChanges(this.props.currRow)} style={{cursor:'pointer'}}><i className="fas fa-hand-point-up" />Request Case Changes</span></li>}

                            {<li><span onClick={()=>this.props.requestCaseChangesAfterSO(this.props.currRow)} style={{cursor:'pointer'}}><i className="fas fa-hand-point-up" />Request Case Changes After {this.props.currRow.genpact_type == 1?`SO`:`STO`} Generation</span></li>}

                            

                        </ul>
                    </div>
                </div>
                </>
            );
        }else{
            return (

                <>
                <div className="actionStyle">
                    <div className="btn-group">
                        <button type="button" className="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" onClick = {(e) => this.handleVerticalMenu(e)} >
                            <img src={ellipsisImage} alt="ellipsisImage" />
                        </button>

                        {current_ownership && this.getCurrentOwnerText()}

                        &nbsp;

                        {breach && 
                        <LinkWithTooltip
                            tooltip={`${htmlDecode(this.props.currRow.breach_reason)}`}
                            href="#"
                            id={`tooltip-menu-${this.props.currRow.task_id}`}
                            clicked={e => this.checkHandler(e)}
                        >
                            <img src={exclamationImage} alt="exclamation" />
                        </LinkWithTooltip>}
                        
                        <ul className="dropdown-menu pull-right" role="menu">
                            {poke && <li><span onClick={()=>this.poke()} style={{cursor:'pointer'}} ><i className="fas fa-hand-point-up" />Follow Up</span></li>}

                            {this.props.currRow.language !== 'en' && this.props.currRow.is_spoc === false && this.props.currRow.parent_id === 0 && this.props.currRow.on_review === true && <li><span onClick={()=>this.props.poke_spoc(this.props.currRow)} style={{cursor:'pointer'}} ><i className="fas fa-pencil-alt" />Follow Up SPOC</span></li> }

                            {this.props.currRow.language !== 'en' && this.props.currRow.is_spoc === false && this.props.currRow.parent_id === 0 && <li><span onClick={()=>this.props.review_task(this.props.currRow)} style={{cursor:'pointer'}} ><i className="fas fa-pencil-alt" />Request Review From Regional SPOC</span></li> }

                            {this.props.currRow.reclassification === 1 && this.props.currRow.is_spoc === false && this.props.currRow.request_type != 43 && <li><span onClick={()=>this.props.reclassify(this.props.currRow)} style={{cursor:'pointer'}} ><i className="fas fa-sync-alt" />Reclassify Task</span></li>}

                            {assign_task && <li><span onClick={()=>this.assign()} style={{cursor:'pointer'}} ><i className="fas fa-pencil-alt" />Assign</span></li>}
                            
                            {re_assign_task && <li><span onClick={()=>this.re_assign()} style={{cursor:'pointer'}} ><i className="fas fa-pencil-alt" />Re-Assign</span></li>}
    
                            {re_assign_cqt && <li><span onClick={()=>this.re_assign_cqt()} style={{cursor:'pointer'}} ><i className="fas fa-pencil-alt" />Re-Assign</span></li> }
                            
                            {create_sub_task &&  <li><span onClick={()=>this.subTask()} style={{cursor:'pointer'}} ><i className="fas fa-user-plus" />Create Sub Task</span></li>}
    
                            {respond_back && <li><span onClick={()=>this.respondBack()} style={{cursor:'pointer'}} ><i className="fas fa-file-prescription" />Respond to Assigner</span></li>}
    
                            {respond_to_customer && <li><span onClick={()=>this.respondCustomer()} style={{cursor:'pointer'}} ><i className="fas fa-arrow-left" />Respond To Customer</span></li>}
    
                            {close_task &&  <li><span onClick={()=>this.closeTask()} style={{cursor:'pointer'}} ><i className="fas fa-window-close" />Close Task</span></li>}

                            {delete_task &&  <li><span onClick={()=>this.deleteTask()} style={{cursor:'pointer'}} ><i className="fas fa-user-minus" />Cancel Request</span></li>}
                            
                            {authorize &&  <li><span onClick={()=>this.authorizeTask()} style={{cursor:'pointer'}} ><i className="fas fa-calendar-check" />Approval</span></li>}
                            
                            {proforma && <li><span onClick={()=>this.createProforma()} style={{cursor:'pointer'}} >
                                    <i className="fas fa-file-invoice" />Create Proforma Invoice</span>
                                </li>}
                            {increase_sla && <li><span onClick={()=>this.increaseSla()} style={{cursor:'pointer'}} >
                                    <i className="fas fa-chart-line" />Increase SLA</span>
                                </li>}    

                            {clone && <li><span onClick={()=>this.clone()} style={{cursor:'pointer'}} >
                                    <i className="fas fa-chart-line" />Copy sub-task</span>
                                </li>}    

                            {sub_task_clone &&  <li><span onClick={()=>this.cloneSubTask()} style={{cursor:'pointer'}}><i className="fas fa-user-plus" />Copy And Create Sub Task</span></li>}

                            {clone_main_task && <li><span onClick={()=>this.cloneMainTask()} style={{cursor:'pointer'}} >
                                    <i className="fas fa-chart-line" />Copy And Create Task</span>
                                </li>}   

                            {approveComment && <li><span onClick={()=>this.props.showApprovalPopup(this.props.currRow)} style={{cursor:'pointer'}} >
                                    <i className="fa fa-check" />Approve & Send To Customer</span>
                                </li>} 

                            {EditTranslatedComment && <li><span onClick={()=>this.props.showTranslateCommentPopup(this.props.currRow)} style={{cursor:'pointer'}} >
                                <i className="fas fa-pencil-alt" />Review Comment</span>
                            </li>}  

                            {approveTaskPopup && <li><span onClick={()=>this.props.showApproveTaskPopup(this.props.currRow)} style={{cursor:'pointer'}} >
                                <i className="fa fa-check" />Mark As Done</span>
                            </li>}  

                            {EditTranslatedTask && <li><span onClick={()=>this.props.showEditTaskPopup(this.props.currRow)} style={{cursor:'pointer'}} >
                                <i className="fas fa-pencil-alt" />Review Task</span>
                            </li>}

                            {/* {approveTaskPopup && <li><span onClick={()=>this.props.showApproveTaskPopup(this.props.currRow)} style={{cursor:'pointer'}} >
                                <i className="fas fa-pencil-alt" />Review Task</span>
                            </li>} */}

                                 
                        </ul>
                    </div>
                </div>
                </>
            );
        }
    }
}

export default BMVerticalMenu;
