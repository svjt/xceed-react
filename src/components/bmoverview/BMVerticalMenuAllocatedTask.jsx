import React, { Component } from 'react';
import { getDashboard,getMyId,htmlDecode,localDate } from '../../shared/helper';

import ellipsisImage from '../../assets/images/ellipsis-icon.svg';
import exclamationImage from '../../assets/images/exclamation-icon.svg';
import peopleImage from '../../assets/images/people-icon.svg';

//import { Link } from 'react-router-dom';
import {
    Tooltip,
    OverlayTrigger
  } from "react-bootstrap";

/*For Tooltip*/
function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
    // console.log( id );
    // console.log( children );
    // console.log( href );
    // console.log( tooltip );
    // console.log( clicked );
    
    return (
      <OverlayTrigger
        overlay={<Tooltip id={id}>{tooltip}</Tooltip>}
        placement="left"
        delayShow={300}
        delayHide={150}
        trigger={["hover"]}
      >
        {children}
      </OverlayTrigger>
    );
  }
  /*For Tooltip*/

class BMVerticalMenuAllocatedTask extends Component {

    constructor(){
        super();
        document.querySelector('body').addEventListener('click',function(event){
            var elems = document.querySelectorAll('.btn-group');
            for (var i = 0; i < elems.length; i++){
                elems[i].classList.remove('open');
            }
        });
    }

    re_assign = () => {
        this.props.showReAssignPopup(this.props.currRow);
    }

    re_assign_cqt = () => {       
        this.props.showReAssignCQTPopup(this.props.currRow);
    }

    poke = () => {
        this.props.poke(this.props.currRow);
    }

    respondCustomer = () => {
        this.props.showRespondCustomerPopup(this.props.currRow);
    }

    closeTask = () => {
        this.props.showCloseTaskPopup(this.props.currRow);
    }

    deleteTask = () => {
        this.props.showDeleteTaskPopup(this.props.currRow);
    }

    respondBackAssigned = () => {
        this.props.showRespondAssignPopup(this.props.currRow);
    }

    authorizeTask = () => {
        this.props.showAuthorizeTaskPopup(this.props.currRow);
    }

    clone = () => {
        this.props.clone(this.props.currRow);
    }

    handleVerticalMenu = (event) => {
        var elems = document.querySelectorAll('.btn-group');
        for (var i = 0; i < elems.length; i++){
            elems[i].classList.remove('open');
        }

        if(event.target.parentNode.classList.contains("btn-group")){
            event.target.parentNode.classList.add("open");
        }else if(event.target.parentNode.classList.contains("dropdown-toggle")){
            event.target.parentNode.parentNode.classList.add("open");
        }
        
    }

    checkDueDate = () =>{
        //var replacedDate = this.props.currRow.due_date.split('T');
        var dueDate = localDate(this.props.currRow.due_date);
        var today  = new Date();
        var timeDiff = dueDate.getTime() - today.getTime();
        if(timeDiff > 0 ){
            return false;
        }else{
            return true;
        }
    }

    render() {

        var respond_to_customer,close_task,poke,respond_back,authorize,re_assign_task,delete_task,re_assign_cqt,breach,current_ownership,hide_menu,clone,approveTaskPopup,genpact_menu = false;
        var my_id = getMyId();
        if(this.props.currRow.parent_id === 0){
            poke = true;

            if(this.props.for_review_task > 0 && this.props.is_spoc_review === true){
                approveTaskPopup = true;
            }

            if(this.props.currRow.cqt !== null && this.props.currRow.cqt > 0){
                //re_assign_cqt = true;
            }else{
                if(getDashboard() !== 'SPOC'){
                    re_assign_task = true;
                }
            }
            if( getDashboard() === 'BM' || 
                getDashboard() === 'SPOC' || 
                getDashboard() === 'CSC' || 
                getDashboard() === 'RA' ){

                close_task          = true;
                respond_to_customer = true;

                if(getDashboard() === 'BM'){
                    delete_task = true;
                }
            }else{
                delete_task = false;
            }
            if(this.props.currRow.assigned_by > 0 && this.props.currRow.owner !== my_id){
                //respond_back = true;
            }
        }else{

            if(this.props.currRow.cqt !== null && this.props.currRow.cqt > 0 && this.props.currRow.cqt_status === 1 ){
                //DO NOTHING
                hide_menu = true;
            }else if(this.props.currRow.genpact && this.props.currRow.genpact == 1){
                genpact_menu = true;
            }else{

                
                if(this.props.currRow.cqt !== null && this.props.currRow.cqt > 0){
                    //re_assign_cqt = true;
                }else{
                    poke = true;
                    if(getDashboard() !== 'SPOC'){
                        re_assign_task = true;
                    }
                }
                if(this.props.currRow.assigned_by > 0 && this.props.currRow.owner !== my_id){
                    if(this.props.currRow.need_authorization === 1){
                        //authorize  = true;   
                    }else{
                        //respond_back = true;
                    }
                }
                if(this.props.currRow.owner === my_id){
                    close_task  = true;
                }

                if(this.props.currRow.owner !== null && this.props.currRow.owner == my_id){
                    clone = true;
                }
            }
        }

        if(this.props.currRow.breach_reason !== null && this.props.currRow.breach_reason !== ''){
            breach = true;
        }
 
        if(this.props.currRow.current_ownership !== null && this.props.currRow.current_ownership > 0){
            current_ownership = true;
        }

        if(hide_menu){
            return null;
        }else if(genpact_menu){
            return null;
        }else{
            return (
                <>
                <div className="actionStyle">
                    <div className="btn-group">
                        <button type="button" className="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" onClick = {(e) => this.handleVerticalMenu(e)} >
                            <img src={ellipsisImage} alt="ellipsisImage" />
                        </button>
    
                        {breach && <LinkWithTooltip
                            tooltip={`${htmlDecode(this.props.currRow.breach_reason)}`}
                            href="#"
                            id={`tooltip-menu-${this.props.currRow.task_id}`}
                            clicked={e => this.checkHandler(e)}
                        >
                        <img src={exclamationImage} alt="exclamation" />
                        </LinkWithTooltip>}
                        &nbsp;
                        {current_ownership && <LinkWithTooltip
                            tooltip={`The task is with ${this.props.currRow.curr_owner_fname} ${this.props.currRow.curr_owner_lname} (${this.props.currRow.curr_owner_desig})`}
                            href="#"
                            id={`tooltip-menu-${this.props.currRow.task_id}${this.props.currRow.current_ownership}`}
                            clicked={e => this.checkHandler(e)}
                        >
                        <img src={peopleImage} alt="exclamation" />
                        </LinkWithTooltip>}
    
                        <ul className="dropdown-menu pull-right" role="menu">
    
                            {poke && <li><span onClick={()=>this.poke()} style={{cursor:'pointer'}} ><i className="fas fa-hand-point-up" />Follow Up</span></li>}

                            {this.props.currRow.language !== 'en' && this.props.currRow.is_spoc === false && this.props.currRow.parent_id === 0 && this.props.currRow.on_review === true && <li><span onClick={()=>this.props.poke_spoc(this.props.currRow)} style={{cursor:'pointer'}} ><i className="fas fa-pencil-alt" />Follow Up SPOC</span></li> }

                            {this.props.currRow.language !== 'en' && this.props.currRow.is_spoc === false &&  this.props.currRow.parent_id === 0 && <li><span onClick={()=>this.props.review_task(this.props.currRow)} style={{cursor:'pointer'}} ><i className="fas fa-pencil-alt" />Request Review From Regional SPOC</span></li> }
    
                            {this.props.currRow.reclassification === 1 && this.props.currRow.is_spoc === false && this.props.currRow.request_type != 43 && <li><span onClick={()=>this.props.reclassify(this.props.currRow)} style={{cursor:'pointer'}} ><i className="fas fa-sync-alt" />Reclassify Task</span></li>}
                            

                            {re_assign_task && <li><span onClick={()=>this.re_assign()} style={{cursor:'pointer'}} ><i className="fas fa-pencil-alt" />Re-Assign</span></li>}
    
                            {re_assign_cqt && <li><span onClick={()=>this.re_assign_cqt()} style={{cursor:'pointer'}} ><i className="fas fa-pencil-alt" />Re-Assign</span></li> }
    
                            {respond_to_customer && <li><span onClick={()=>this.respondCustomer()} style={{cursor:'pointer'}}><i className="fas fa-arrow-left" />Respond To Customer</span></li>}
    
                            {respond_back && <li><span onClick={()=>this.respondBackAssigned()} style={{cursor:'pointer'}}><i className="fas fa-file-prescription" />Respond to Assigner</span></li>}
    
                            {authorize &&  <li><span onClick={()=>this.authorizeTask()} style={{cursor:'pointer'}}><i className="fas fa-calendar-check" />Approval</span></li>}

                            {delete_task &&  <li><span onClick={()=>this.deleteTask()} style={{cursor:'pointer'}} ><i className="fas fa-user-minus" />Cancel Request</span></li>}
    
                            {close_task &&  <li><span onClick={()=>this.closeTask()} style={{cursor:'pointer'}}><i className="fas fa-window-close" />Close Task</span></li>}

                            {approveTaskPopup && <li><span onClick={()=>this.props.showApproveTaskPopup(this.props.currRow)} style={{cursor:'pointer'}} >
                                <i className="fa fa-check" />Mark As Done</span>
                            </li>}  



                            {clone && <li><span onClick={()=>this.clone()} style={{cursor:'pointer'}} >
                                    <i className="fas fa-chart-line" />Copy sub-task</span>
                                </li>}

                        </ul>
                    </div>
                </div>
                </>
            );
        }
    }
}

export default BMVerticalMenuAllocatedTask;
