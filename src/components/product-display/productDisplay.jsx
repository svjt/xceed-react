import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from "../../shared/axios";
import { TabContent, TabPane, Nav, NavItem, NavLink, Row, Col } from 'reactstrap';
import Loader from 'react-loader-spinner';
import whitelogo from '../../assets/images/drreddylogo_white.png';

import classnames from 'classnames';

class productDisplay extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            activeTab: '1',
            productDetail: {},
            documentsArray: [],
            documentsDescriptionArray: [],
            redirect: false,
        };
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    componentDidMount() {
        this.getProductDetails()
    }
    getProductDetails() {
        // axios.get(process.env.REACT_APP_PORTAL_URL + 'ext/api/get-product-details/' + this.props.match.params.id + '?_format=json')
        axios.get('api/products/' + this.props.match.params.id)
            .then(res => {
                if (res.data.data.length > 0) {
                    this.setState({ productDetail: res.data.data[0] });
                    this.setState({
                        productDetail: res.data.data[0]
                    }, () => {
                        if (this.state.productDetail.documents != '') {
                            var documents = this.state.productDetail.documents.split("~");
                            var documents_description = this.state.productDetail.documents_description.split("~");
                            this.setState({ documentsArray: documents });
                            this.setState({ documentsDescriptionArray: documents_description });
                        }

                    });
                }
                else {
                    this.props.history.push('/')
                    //return <Redirect to='/' />
                }

            }).catch(err => {
            })
    }
    noData = () => {
        return <><div className="documents-box" key={1}>
            <div className="documents-box-left"><b>TEST</b></div>

        </div></>;
    }
    render() {
        if ((JSON.stringify(this.state.productDetail) != '{}') && this.state.productDetail) {
            return (
                <>
                    <div className="content-wrapper">
                        <section className="content">
                            <div className="dashboard-content-sec">
                                <div className="breadcrumb">
                                    <ul>
                                        <li><Link to="/user/product_catalogue">Product Catalogue</Link></li>
                                        <li>{this.state.productDetail.title}</li>
                                    </ul>
                                </div>
                                <div className="product-details-box clearfix">
                                    <figure>
                                        <img src={process.env.REACT_APP_PRODUCT_URL + this.state.productDetail.image} alt={this.state.productDetail.title} typeof="Image" className="img-fluid" width="480" height="354" />
                                    </figure>
                                    <div className="product-details-content">
                                        <h2>{this.state.productDetail.title}</h2>
                                        <ul className="status">
                                            <li><h3>CAS No.</h3>{this.state.productDetail.cas_no}</li>
                                            <li><h3>Innovator Brand (USA)</h3>{this.state.productDetail.innovator_brand}</li>
                                            <li><h3>Therapy Area</h3>{this.state.productDetail.therapy_area}</li>
                                            <li><h3>Therapeutic Category</h3>{this.state.productDetail.therapeutic_category}</li>
                                            <li><h3>API Technology</h3>{this.state.productDetail.api_technology}</li>
                                            <li><h3>Dose From</h3>{this.state.productDetail.dose_form}</li>
                                            <li><h3>Dr Reddy's Development status</h3>{this.state.productDetail.development_status}</li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="quicktabs-main">
                                    <Nav tabs>
                                        <NavItem>
                                            <NavLink
                                                className={classnames({ active: this.state.activeTab === '1' })}
                                                onClick={() => { this.toggle('1'); }}
                                            >
                                                Details
                                </NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink
                                                className={classnames({ active: this.state.activeTab === '2' })}
                                                onClick={() => { this.toggle('2'); }}
                                            >
                                                Documents
                            </NavLink>
                                        </NavItem>
                                    </Nav>
                                    <TabContent activeTab={this.state.activeTab}>
                                        <TabPane tabId="1">
                                            <Row>
                                                <Col sm="12">
                                                    <h3>Mechanism of Action</h3>
                                                    {this.state.productDetail.mechanism}
                                                    <h3>Indication</h3>
                                                    <p>{this.state.productDetail.indication}</p>
                                                    <h3>Available Regulatory Filing: </h3>
                                                    {this.state.productDetail.available_regulatory}
                                                </Col>
                                            </Row>
                                        </TabPane>
                                        <TabPane tabId="2">
                                            <Row>
                                                <Col sm="12">
                                                    {(this.state.documentsArray && this.state.documentsArray.length > 0) ?
                                                        (
                                                            this.state.documentsArray.map((document, key) => (
                                                                <div className="documents-box" key={key}>
                                                                    <div className="documents-box-left"><b>{this.state.documentsDescriptionArray[key]}</b></div>
                                                                    <div className="row clearfix">
                                                                        <div className="documents-box-right">
                                                                            <div className="download-btn"> <a href={process.env.REACT_APP_PRODUCT_URL + document} target="_blank" type="application/pdf; length=36922" >Download</a> </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            ))
                                                        )
                                                        :
                                                        <><div className="documents-box" key={1}>
                                                            <div className="">No documents available.</div>

                                                        </div></>
                                                    }
                                                </Col>
                                            </Row>
                                        </TabPane>
                                    </TabContent>
                                </div>
                            </div>
                        </section>
                    </div>
                </>
            );
        }
        else {
            return (
                <div className="loderOuter">
                    <div className="loading_reddy_outer">
                        <div className="loading_reddy" >
                            <img src={whitelogo} alt="loaderImage"/>
                        </div>
                    </div>
                </div>
            );
        }

    }
}

export default productDisplay;