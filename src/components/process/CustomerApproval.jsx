import React, { Component } from 'react';
import Axios from 'axios';
import { Row, Col, Button, ButtonToolbar } from 'react-bootstrap';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import Select from 'react-select';
import swal from 'sweetalert';

import { htmlDecode } from "../../shared/helper";
import Loader from '../loader/loader';

let initialValues = {
    assign_to: '',
    employee_id: '',
};

const validationSchema = (refObj) =>
    Yup.object().shape({
        employee_id: Yup.object().test('isother', 'Please select an employee',
            (value) => refObj.state.assignTo == 'others' && value && Object.keys(value).length > 0),
    });

class CustomerApproval extends Component {
    state = {
        showLoader: false,
        token: '',
        errMsg: null,
        successMsg: '',
        type: '', // 1 = Approval, 2 = Assignment
        customerDetails: {},
        employeeList: [],
        assignTo: '', // admin / others
    };

    componentDidMount() {
        //this.handleValidateToken();
        this.handleApproval();
    }

    handleValidateToken = () => {
        const config = {
            baseURL: process.env.REACT_APP_API_URL,
            headers: { Authorization: `Bearer ${this.props.match.params.token}` },
        };

        Axios.get('api/customers/process_validate_token', config)
            .then((res) => {
                if (res.data && res.data.status === 1) {
                    // console.log('data', res.data.customer_details);
                    let employee_list = [];
                    if (res.data.employee_list && res.data.employee_list.length > 0) {
                        for (let index = 0; index < res.data.employee_list.length; index++) {
                            const element = res.data.employee_list[index];
                            employee_list.push({
                                value: element["employee_id"],
                                label: htmlDecode(element["employee_name"]),
                            });
                        }
                    }
                    this.setState({
                        showLoader: false,
                        type: res.data.type,
                        customerDetails: res.data.customer_details,
                        employeeList: employee_list,
                        assignTo: res.data.assign_to ? res.data.assign_to : '',
                        token: res.data.token,
                    });
                } else {
                    this.setState({
                        showLoader: false,
                        errMsg:
                            res.data && res.data.message
                                ? res.data.message
                                : 'Something went wrong, please try again later!',
                    });
                }
            })
            .catch((err) => {
                if (err.response && err.response.status === 401) {
                    this.setState({
                        showLoader: false,
                        errMsg: err.data && err.data.message ? err.data.message : 'Invalid Access.',
                    });
                } else {
                    this.setState({
                        showLoader: false,
                        errMsg:
                            err.data && err.data.message
                                ? err.data.message
                                : 'Something went wrong, please try again later!',
                    });
                }
                console.log({ err });
            });
    };

    handleConfirmApproval = (values, actions) => {
        swal({
            closeOnClickOutside: false,
            title: 'Request review',
            text: 'Are you sure you want to approve this user ?',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        }).then((didApprove) => {
            if (didApprove) {
                this.setState({ showLoader: true });
                this.handleApproval();
            }
        });
    };

    handleApproval = () => {
        if (this.props.match.params.token) {
            const config = {
                baseURL: process.env.REACT_APP_API_URL,
                headers: { Authorization: `Bearer ${this.props.match.params.token}` },
            };

            // assign_id is in token, no need to send again
            // API CALL HERE
            Axios.get('api/customers/process_customer_approval', config)
                .then((res) => {
                    //console.log(res.data.status);
                    if (res.data && res.data.status == 1) {
                        this.setState({
                            showLoader: false,
                            type: '',
                            successMsg: res.data.message ? res.data.message : 'Successfully Approved!',
                        });
                    } else {
                        this.setState({
                            showLoader: false,
                            type: '',
                            errMsg:
                                res.data && res.data.message
                                    ? res.data.message
                                    : 'Something went wrong, please try again later!',
                        });
                    }
                })
                .catch((err) => {   //console.log("ERR", err.response);
                    if (err.response && err.response.status === 401 ) {
                        if(err.response.data.status == 2){
                            this.setState({
                                showLoader: false,
                                type: '',
                                errMsg: err.response.data && err.response.data.errors.message ? err.response.data.errors.message : 'Invalid Access.',
                            });
                        }else{
                            this.setState({
                                showLoader: false,
                                type: '',
                                errMsg: err.data && err.data.message ? err.data.message : 'Invalid Access.',
                            });
                        }
                    } else {
                        this.setState({
                            showLoader: false,
                            type: '',
                            errMsg:
                                err.data && err.data.message
                                    ? err.data.message
                                    : 'Something went wrong, please try again later!',
                        });
                    }                    
                });
        }
    };

    handleAssignment = (values, actions) => {
        if (this.props.match.params.token) {
            const config = {
                baseURL: process.env.REACT_APP_API_URL,
                headers: { Authorization: `Bearer ${this.props.match.params.token}` },
            };

            const payload = {
                employee_id: values.employee_id,
                assign_to: values.assign_to,
            };

            // API CALL HERE
            Axios.post('/process_customer_assignment', payload, config)
                .then((res) => {
                    if (res.data && res.data.status === 1) {
                        this.setState({
                            showLoader: false,
                            successMsg: res.data.message ? res.data.message : 'Successfully Assigned!',
                        });
                    } else {
                        this.setState({
                            showLoader: false,
                            errMsg:
                                res.data && res.data.message
                                    ? res.data.message
                                    : 'Something went wrong, please try again later!',
                        });
                    }
                })
                .catch((err) => {
                    if (err.response && err.response.status === 401) {
                        this.setState({
                            showLoader: false,
                            errMsg: err.data && err.data.message ? err.data.message : 'Invalid Access.',
                        });
                    } else {
                        this.setState({
                            showLoader: false,
                            errMsg:
                                err.data && err.data.message
                                    ? err.data.message
                                    : 'Something went wrong, please try again later!',
                        });
                    }
                    console.log({ err });
                });
        }
    };

    setEmployee = (event, setFieldValue) => {
        if (event.length == 0) {
            setFieldValue("employee_id", "");
            this.setState({ employee_id: "" });
        } else {
            setFieldValue("employee_id", event);
            this.setState({ employee_id: event });
        }
    };

    customerAssignForm = (customerDetails) => {
        return (
            <div className='newFormSection'>
                <section className='content'>
                    <div className='boxPapanel content-padding form-min-hight'>
                        <div className='taskdetails'>
                            {this.showCustomerDetails(customerDetails)}
                            <div className='clearfix tdBtmlist'>
                                <Formik
                                    initialValues={initialValues}
                                    validationSchema={validationSchema(this)}
                                    onSubmit={this.handleAssignment}
                                >
                                    {({ values, errors, touched, setFieldValue }) => {
                                        return (
                                            <div className='contBox'>
                                                <Row>
                                                    <Col xs={12} sm={12} md={12}>
                                                        <div className='form-group'>
                                                            <label>
                                                                Assign to Employee
                                                                <span className='required-field'>*</span>
                                                            </label>
                                                            <Select
                                                                className='basic-single'
                                                                classNamePrefix='select'
                                                                name='employee_id'
                                                                options={this.state.employeeList}
                                                                isDisabled={this.state.assignTo === 'admin'}
                                                                onChange={(e) => {
                                                                    this.setEmployee(e, setFieldValue);
                                                                }}
                                                            />
                                                            {errors.employee_id || touched.employee_id ? (
                                                                <span className='errorMsg'>{errors.employee_id}</span>
                                                            ) : null}
                                                        </div>
                                                    </Col>
                                                    <Col xs={12} sm={12} md={12}>
                                                        <button
                                                            className='btn m-r-10'
                                                            type='submit'
                                                            disabled={this.state.assignTo === 'admin'}
                                                            onClick={(e) => {
                                                                setFieldValue('assign_to', 'employee');
                                                            }}
                                                        >
                                                            Assign to Selected Employee
                                                        </button>
                                                        <button
                                                            className='btn m-r-10'
                                                            type='submit'
                                                            onClick={(e) => {
                                                                setFieldValue('assign_to', 'admin');
                                                            }}
                                                        >
                                                            Assign to Admin
                                                        </button>
                                                    </Col>
                                                </Row>
                                            </div>
                                        );
                                    }}
                                </Formik>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    };

    customerApproveForm = (customerDetails) => {
        return (
            <div className='newFormSection'>
                <section className='content'>
                    <div className='boxPapanel content-padding form-min-hight'>
                        <div className='taskdetails'>
                            {this.showCustomerDetails(customerDetails)}
                            <div className='clearfix tdBtmlist'>
                                <Formik
                                    initialValues={{}}
                                    validationSchema={{}}
                                    onSubmit={this.handleConfirmApproval}>
                                    {({ errors, isSubmitting }) => {
                                        return (
                                            <div className='messageBox rejectionForm'>
                                                <p>To approved this user, click approve.</p>
                                                <Form>
                                                    <Row>
                                                        <Col>
                                                            {errors.message ? (
                                                                <span className='errorMsg'>{errors.message}</span>
                                                            ) : null}
                                                        </Col>
                                                    </Row>
                                                    <ButtonToolbar>
                                                        <Button className='btn btn-fill' type='submit'>
                                                            Approve
                                                        </Button>
                                                    </ButtonToolbar>
                                                </Form>
                                            </div>
                                        );
                                    }}
                                </Formik>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    };

    customerErrorMessage = (errMsg) => {
        return (
            <div className='messageBox'>
                <img src={require('../../assets/images/invalid-file.png')} /> <h1>{errMsg}</h1>
            </div>
        );
    };

    customerSuccessMessage = (successMsg) => {
        return (
            <div className='messageBox successBox'>
                <img src={require('../../assets/images/successful.png')} /> <h1>{successMsg}</h1>
            </div>
        );
    };

    showCustomerDetails = (customerDetails) => {
        if (customerDetails && Object.keys(customerDetails).length > 0) {
            return (
                <div className='contBox'>
                    <Row>
                        <Col xs={12} sm={12} md={12}>
                            <div className='form-group'>
                                <table className='table table-bordered'>
                                    <tr>
                                        <td>User Name</td>
                                        <td>
                                            {customerDetails.first_name} {customerDetails.last_name}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Customer Name</td>
                                        <td>{customerDetails.company_name}</td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>{customerDetails.email}</td>
                                    </tr>
                                    <tr>
                                        <td>Phone no</td>
                                        <td>{customerDetails.phone_no}</td>
                                    </tr>
                                    <tr>
                                        <td>Country</td>
                                        <td>{customerDetails.country_name}</td>
                                    </tr>
                                    <tr>
                                        <td>Language</td>
                                        <td>{customerDetails.language}</td>
                                    </tr>
                                    <tr>
                                        <td>Key Account</td>
                                        <td>{customerDetails.vip_customer == 1 ? 'Yes' : 'No'}</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>{customerDetails.status == 1 ? 'Active' : 'Inactive'}</td>
                                    </tr>
                                    {customerDetails.customer_type != '' && customerDetails.customer_type != null && (
                                        <tr>
                                            <td>User Type</td>
                                            <td>{customerDetails.customer_type}</td>
                                        </tr>
                                    )}
                                    <tr>
                                        <td>Assign Team</td>
                                        <td>
                                            {customerDetails.teams &&
                                                customerDetails.teams.map((answer, i) => {
                                                    return (
                                                        <p>
                                                            {answer.first_name} {answer.last_name} ({answer.desig_name}){' '}
                                                        </p>
                                                    );
                                                })}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Is Admin</td>
                                        <td>{(customerDetails.user_is_admin == 1 && customerDetails.role_type == 0) ? 'Yes' : 'No'}</td>
                                    </tr>
                                    <tr>
                                        <td>Disable Notifications</td>
                                        <td>{customerDetails.dnd == 1 ? 'Yes' : 'No'}</td>
                                    </tr>
                                    <tr>
                                        <td>Multi Language Access</td>
                                        <td>{customerDetails.multi_lang_access == 1 ? 'Yes' : 'No'}</td>
                                    </tr>
                                    {customerDetails.role_type != 0 && (
                                        <>
                                            <tr>
                                                <td>Role</td>
                                                <td>
                                                    {customerDetails.role &&
                                                        customerDetails.role.map((rl, j) => {
                                                            return <p>{rl.role_name} </p>;
                                                        })}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Role Type</td>
                                                <td>{customerDetails.role_type == 1 ? 'Admin' : 'Regular'}</td>
                                            </tr>
                                        </>
                                    )}
                                </table>
                            </div>
                        </Col>
                    </Row>
                </div>
            );
        }
        return <div className='contBox'>Invalid Data Provided</div>;
    };

    render() {
        const { showLoader, successMsg, errMsg, type, customerDetails } = this.state;

        return (
            <div className='newFormSection'>
                <div className='logoWrapper'>
                    <a href={process.env.REACT_APP_PORTAL_URL} target='_blank'>
                        <img src={require('../../assets/images/logo-blue.jpg')} />
                    </a>
                </div>

                {showLoader && <Loader />}

                {errMsg && this.customerErrorMessage(errMsg)}

                {successMsg && this.customerSuccessMessage(successMsg)}

                {type == 1 && this.customerApproveForm(customerDetails)}

                {type == 2 && this.customerAssignForm(customerDetails)}

                <div className='footerCopy'>
                    <p>© 2021 Dr. Reddy’s Laboratories Ltd. All rights reserved(v2.9.0)</p>
                </div>
            </div>
        );
    }
}

export default CustomerApproval;
