import React, { Component, Fragment } from 'react';
//import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Header from "../header/Header";
import Sidebar from '../sidebar/Sidebar';
import Footer from '../footer/Footer';
import jwt_decode from 'jwt-decode';

import { Link,withRouter } from 'react-router-dom';

// connect to store
import { connect } from 'react-redux';
import { authLogout,getNotifications,getNotificationCount } from "../../store/actions/auth";

import './Layout.css';


const pathIgnoreList = ['process_customer_approval'];
const currentPath = window.location ? window.location.pathname.split('/')[1] : '';

class Layout extends Component {

    constructor( props ) {
        super( props );
    }

    componentDidMount() {
        if(!pathIgnoreList.includes(currentPath)){
            document.body.classList.add('skin-blue');
            document.body.classList.add('sidebar-mini');
        }

        if(localStorage.getItem('token') !== "" && localStorage.getItem('token') !== null){
            this.props.getNotificationCount();
        }
        //document.body.classList.add('sidebar-collapse');
        //console.log("Layout Load");
    }

    render() {

        const isLoggedIn = 
            (localStorage.getItem('token') !== "" && localStorage.getItem('token') !== null) 
            ? true 
            : false;

        //const path_name = this.props.history.location.pathname;
        const path_name = '';
       
        return pathIgnoreList.includes(currentPath) ? (
            this.props.children
         ) 
        : (
            <Fragment>
                <Header isLoggedIn={isLoggedIn}/> 
                <Sidebar isLoggedIn={isLoggedIn} path_name={path_name}/> 
                    {this.props.children}
                <Footer isLoggedIn={isLoggedIn}/>               
            </Fragment>
        )
         
    }
}

const mapStateToProps = state => {
    //console.log('state',state);
  return {
    ...state
  };
  };
  
  const mapDispatchToProps = dispatch => {
  //console.log("dispatch called")
  return {
        clickOpenNotification: (data, onSuccess, setErrors) => dispatch(getNotifications(data, onSuccess, setErrors)),
        getNotificationCount: (data, onSuccess, setErrors) => dispatch(getNotificationCount(data, onSuccess, setErrors)),
        logMeOut: () => dispatch(authLogout())
        
  };
  };
  
  export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Layout));
  
//export default Layout;
  